/***
 **  $Date: 2014-04-25 10:51:28 +0700 (Fri, 25 Apr 2014) $
 **  $Revision: 5271 $
 **  $Author: CHINHND $
 **  $HeadURL: https://scs01.wisekey.ch:8443/svn/wiseid/iphone/trunk/SRC/Libs/WISeKey/Common/Extensions/NSString+Extensions.h $
 **  $Id: NSString+Extensions.h 5271 2014-04-25 03:51:28Z CHINHND $
***/
//
//  NSString_Extensions.h
//  WISeID
//
//  Created by wisekey on 4/21/11.
//  Copyright 2011 WISeKey SA, All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (NSStringFunctions)

-(NSString*) NullToBlank;
-(NSString*)substringBetween:(NSString*)fromStr and:(NSString*)toStr;
-(BOOL)containsAnyOf:(NSString *)Strings ;
-(BOOL)containsString:(NSString *)str;
-(BOOL)containsExactString:(NSString *)str;
-(BOOL)containsStringWithPattern:(NSString*)regEx;
-(BOOL)isEmpty;
-(NSInteger)numberOfLinesInString;	//returns how many lines does targeted string has.
+(NSString*)stringWithFloat:(float)value maxDecimal:(int)decimalLimit;
+(CGFloat)getHeightOfString:(NSString *)acontent width:(float)width withFont:(UIFont*)font;
+ (NSString *)stringFromFileSize:(double)theSize;
- (BOOL)equalsAnyOf:(NSString *)Strings ;
-(BOOL) hasSubString:(NSString *) str;
-(BOOL) hasSubStringWithPattern:(NSString*) regEx;
+ (NSString *)GetUUID;

@end