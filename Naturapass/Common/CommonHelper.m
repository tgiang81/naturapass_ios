//
//  CommonHelper.m
//  Naturapass
//
//  Created by Giang on 8/25/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CommonHelper.h"
#import <QuartzCore/QuartzCore.h>
#import "ASSharedTimeFormatter.h"
#import <CommonCrypto/CommonDigest.h>

@implementation CommonHelper

static CommonHelper *sharedInstance = nil;

+ (CommonHelper *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

//2015-09-01T04:02:52+0200
//"yyyy-MM-dd hh:mm:ss"

//f94e007f369851443cf6e23e0d132227
//https:/maps.googleapis.com/maps/api/place/autocomplete/json?input=ha&key= AIzaSyBhYj6mC9U81_meIvppiFcysfo5bO2nNVc&types=establishment|geocode&radius=500&language=fr

-(NSString*) getRandKeyGoogleSearch
{
    NSArray *arrRan = @[/*@"AIzaSyC21pAQ94efbsY_uNpUjej9PjmHJcCAsnk",*/

                        @"AIzaSyAyZkjQbl2HA72X0DUcu2wITsswwUP_UEc",
                        @"AIzaSyBMdefJenNpeWcbIlBgSYLrOHF57-Kr3q4",
                        @"AIzaSyDy48M81Hbcj5b614QjWiR_UsJemnHbUaM",
                        @"AIzaSyDjirS7Px4xRWxASuJHt9Xq4Sd-xhE0fdg",
                        @"AIzaSyAMZkV6-MErckmX9muMQiQexHb2ZjPOM6g",
                        @"AIzaSyB8ejqJsZOdNL_v1xh7kxg_iXG8AgNjGKw",
                        @"AIzaSyB67S1qDBaORHGbm9SmIYVFFnphS6KqJkc",
                        @"AIzaSyBO-C8wd6LQgG0qPgUh-nIfCWeXcDvznCM",
                        @"AIzaSyB67S1qDBaORHGbm9SmIYVFFnphS6KqJkc",
                        @"AIzaSyA6rcNd7doSoNKJdRmHbad7av50YUk7vps",
                        @"AIzaSyDk3IvEbZg94XjiulSsSOyICeP4utQSYKQ",
                        @"AIzaSyA0f-wV9enFuT0m8Ji3ZjM2hzH-7eHF4vo",
                        @"AIzaSyBPyYK-AvZP7cxvzr3dtkZdXwnt4Bi4JX4",
                        @"AIzaSyDeIvu2biFIy2RYBzDkoVi7dWnneXOD7Fg",
                        @"AIzaSyBqiFBc8P3RMRWBrIGOKEQGIsUqB5DldEY",
                        @"AIzaSyDHA9bK6IqLvkqmW2lWnNTu5JuU8Ya2u0c",
                        @"AIzaSyA9SLCLXYgO3Sv0g8NOcmEFr9R455ESAeI",
                        @"AIzaSyAujb96CALlxy-qhsBWmmqX3SasRDgsZ8I",
                        @"AIzaSyBL3nKuok3d6u7Nn4fBarLcvJUyR5DYKb4",
                        @"AIzaSyDpHgvJ7urHoHqJKcpZ9neQywYvwTcHX1Y"
                        ];
    int x = arc4random() % arrRan.count;
    
    return arrRan[x];
}


-(NSString*) getRandKey
{

    NSArray *arrRan = @[@"58680a03d6443e62b641828bc7a9d421",
                        @"9dcb5d311913d815d3869b7141180b1d",
                        @"100311cc1ea03f4b04054b4f40fe46b1",
                        @"0bb78e257b1c4cf31b2077b06bb6e299",
                        @"c3b1a8514cff0b7866177a9b0a95beed",
                        @"a9c05913a3c2d61aaa86bff3992fc6ff",
                        @"7204313c52ac846b0f63002326279a24",
                        @"79eb0893a499800c30a167da7dc63094",
                        @"9d92bbc927616dd0f1e798ccdfb50241",
                        @"b4cadaa3d7b22f314b8e0dc71236c9c2",
                        @"8f9aefadfac5a49ac653f23d3793ff63"
                        ];
    int x = arc4random() % 10;
    return arrRan[x];
}


-(NSString*) timeLeftSinceDate: (NSString *) dateStr
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd-MM-YYYY HH:mm:ss"];
    
    
    NSDate *dateT = [[NSDate alloc] init];
    // voila!
    dateT = [dateFormatter dateFromString:dateStr];
    
    
    NSString *timeLeft;
    
    NSDate *today10am =[NSDate date];
    
    NSInteger seconds = [today10am timeIntervalSinceDate:dateT];
    
    NSInteger days = (int) (floor(seconds / (3600 * 24)));
    if(days) seconds -= days * 3600 * 24;
    
    NSInteger hours = (int) (floor(seconds / 3600));
    if(hours) seconds -= hours * 3600;
    
    NSInteger minutes = (int) (floor(seconds / 60));
    if(minutes) seconds -= minutes * 60;
    
    if(days) {
        timeLeft = [NSString stringWithFormat:@"Il y a %ld jours", (long)days*-1];
    }
    else if(hours) { timeLeft = [NSString stringWithFormat: @"Il y a %ld heures", (long)hours*-1];
    }
    else if(minutes) { timeLeft = [NSString stringWithFormat: @"Il y a %ld minutes", (long)minutes*-1];
    }
    else if(seconds)
        timeLeft = [NSString stringWithFormat: @"A l'instant"];
    
    return timeLeft;
}

-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
{
//    CGPoint saveCenter = roundedView.center;
//    CGRect newFrame = CGRectMake(roundedView.frame.origin.x, roundedView.frame.origin.y, newSize, newSize);
//    roundedView.frame = newFrame;
//    roundedView.layer.masksToBounds = YES;
//    roundedView.layer.cornerRadius = newSize;
//    roundedView.center = saveCenter;
}


+ (NSString *)generatorString {
    
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    NSString *uuidString = (NSString *)CFBridgingRelease(CFUUIDCreateString(NULL,uuidRef));
    CFRelease(uuidRef);
    
    return uuidString;
}
+(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatterBis];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"d MMM yyyy à H:mm" forFormatter: outputFormatterBis];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}
#pragma mark -- Thumbnail
- (UIImage *)buildThumbnailImage:(CGPDFDocumentRef)pdfDocument
{
    BOOL hasRetinaDisplay = FALSE;  // by default
    CGFloat pixelsPerPoint = 1.0;  // by default (pixelsPerPoint is just the "scale" property of the screen)
    
    if ([UIScreen instancesRespondToSelector:@selector(scale)])  // the "scale" property is only present in iOS 4.0 and later
    {
        // we are running iOS 4.0 or later, so we may be on a Retina display;  we need to check further...
        if ((pixelsPerPoint = [[UIScreen mainScreen] scale]) == 1.0)
            hasRetinaDisplay = FALSE;
        else
            hasRetinaDisplay = TRUE;
    }
    else
    {
        // we are NOT running iOS 4.0 or later, so we can be sure that we are NOT on a Retina display
        pixelsPerPoint = 1.0;
        hasRetinaDisplay = FALSE;
    }
    
    size_t imageWidth = 320;  // width of thumbnail in points
    size_t imageHeight = 460;  // height of thumbnail in points
    
    if (hasRetinaDisplay)
    {
        imageWidth *= pixelsPerPoint;
        imageHeight *= pixelsPerPoint;
    }
    
    size_t bytesPerPixel = 4;  // RGBA
    size_t bitsPerComponent = 8;
    size_t bytesPerRow = bytesPerPixel * imageWidth;
    
    void *bitmapData = malloc(imageWidth * imageHeight * bytesPerPixel);
    
    // in the event that we were unable to mallocate the heap memory for the bitmap,
    // we just abort and preemptively return nil:
    if (bitmapData == NULL)
        return nil;
    
    // remember to zero the buffer before handing it off to the bitmap context:
    bzero(bitmapData, imageWidth * imageHeight * bytesPerPixel);
    
    CGContextRef theContext = CGBitmapContextCreate(bitmapData, imageWidth, imageHeight, bitsPerComponent, bytesPerRow,
                                                    CGColorSpaceCreateDeviceRGB(), kCGImageAlphaPremultipliedLast);
    
    CGPDFPageRef pdfPage = CGPDFDocumentGetPage(pdfDocument, 1);  // get the first page for your thumbnail
    
    CGAffineTransform shrinkingTransform =
    CGPDFPageGetDrawingTransform(pdfPage, kCGPDFMediaBox, CGRectMake(0, 0, imageWidth, imageHeight), 0, YES);
    
    CGContextConcatCTM(theContext, shrinkingTransform);
    
    CGContextDrawPDFPage(theContext, pdfPage);  // draw the pdfPage into the bitmap context
    CGPDFDocumentRelease(pdfDocument);
    
    //
    // create the CGImageRef (and thence the UIImage) from the context (with its bitmap of the pdf page):
    //
    CGImageRef theCGImageRef = CGBitmapContextCreateImage(theContext);
    free(CGBitmapContextGetData(theContext));  // this frees the bitmapData we malloc'ed earlier
    CGContextRelease(theContext);
    
    UIImage *theUIImage;
    
    // CAUTION: the method imageWithCGImage:scale:orientation: only exists on iOS 4.0 or later!!!
    if ([UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)])
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef scale:pixelsPerPoint orientation:UIImageOrientationUp];
    }
    else
    {
        theUIImage = [UIImage imageWithCGImage:theCGImageRef];
    }
    
    CFRelease(theCGImageRef);
    return theUIImage;
}

+ (NSString *) stringToMD5:(NSString *)key {
    const char *str = [key UTF8String];
    if (str == NULL) {
        str = "";
    }
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    NSString *filename = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%@",
                          r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10],
                          r[11], r[12], r[13], r[14], r[15], [[key pathExtension] isEqualToString:@""] ? @"" : [NSString stringWithFormat:@".%@", [key pathExtension]]];
    
    return filename;
}

@end
