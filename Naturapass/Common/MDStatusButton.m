//
//  MDCheckBox.m
//  Naturapass
//
//  Created by Manh on 7/30/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDStatusButton.h"
#import "Define.h"
@implementation MDStatusButton
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    _colorDisEnable = [UIColor lightGrayColor];
    _colorEnable =self.backgroundColor;
    _imgEnable =self.currentBackgroundImage;
    _imgDisEnable = [UIImage imageNamed:@"gray_btn_bg"];
}
-(void)setEnabled:(BOOL)enabled{
    [super setEnabled:enabled];
    if (enabled) {
        self.backgroundColor = _colorEnable;
        [self  setBackgroundImage:_imgEnable forState:UIControlStateNormal];
    }
    else
    {
        self.backgroundColor = _colorDisEnable;
        [self  setBackgroundImage:_imgDisEnable forState:UIControlStateNormal];

    }
}
@end
