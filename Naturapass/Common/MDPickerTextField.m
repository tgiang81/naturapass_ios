//
//  DPTextField.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDPickerTextField.h"
#import "HSDatePickerViewController.h"
@interface MDPickerTextField ()<HSDatePickerViewControllerDelegate>
{
}
@property (strong, nonatomic)  UIButton *btnClick;

@end
@implementation MDPickerTextField
{
}

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnClick = [UIButton new];
    [self addContraintSupview:_btnClick];
    [self.btnClick addTarget:self action:@selector(btnTimeClick:) forControlEvents:UIControlEventTouchUpInside];

}
-(void)addContraintSupview:(UIView*)view
{
    UIView *viewSuper = self;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.frame = viewSuper.frame;
    [viewSuper addSubview:view];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(-5)-[view]-(-5)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(-5)-[view]-(-5)-|"
                                                                      options:0
                                                                      metrics:nil
                               
                                                                        views:NSDictionaryOfVariableBindings(view)]];
}
-(void) btnTimeClick:(UIButton*) sender
{
    HSDatePickerViewController *hsdpvc = [[HSDatePickerViewController alloc] init];
    hsdpvc.confirmButtonTitle = @"OUI";
    hsdpvc.backButtonTitle = @"NON";
    
    hsdpvc.delegate = self;
    if (_date) {
        hsdpvc.date =_date;
    }
    [_parentVC presentViewController:hsdpvc animated:YES completion:nil];
}
#pragma mark -date picker
- (void)hsDatePickerPickedDate:(NSDate *)date
{
    _date = date;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd/MM/yyyy HH:mm"];
    self.text = [NSString stringWithFormat:@"%@",
                 [df stringFromDate:date]];
    if (self.CallBack) {
        self.CallBack();
    }
}

- (void)hsDatePickerWillDismissWithQuitMethod:(HSDatePickerQuitMethod)method
{
}

- (void)hsDatePickerDidDismissWithQuitMethod:(HSDatePickerQuitMethod)method
{    

}

@end
