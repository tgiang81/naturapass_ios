//
//  UIImage+CheckBox.m
//  Naturapass
//
//  Created by macbook on 18/01/2019.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import "UIImage+CheckBox.h"

@implementation UIImage (CheckBox)
+ (UIImage *)imageCheckBoxWithBackground:(UIColor*)colorBackground colorActive:(UIColor*)colorActive colorBorder:(UIColor*)colorBorder isRadio:(BOOL)isRadio
{
    UIView *view_active = [UIView new];
    view_active.frame = CGRectMake(0, 0, 40, 40);
    view_active.layer.masksToBounds = YES;
    view_active.layer.borderWidth = 3;
    view_active.backgroundColor = [UIColor clearColor];
    
    UIView *view_sub_active = [UIView new];
    view_sub_active.frame = CGRectMake(7, 7, 26, 26);
    view_sub_active.layer.masksToBounds = YES;
    if (isRadio) {
        view_active.layer.cornerRadius = 20;
        view_sub_active.layer.cornerRadius = 13;
    }
    else
    {
        view_active.layer.cornerRadius = 4;
        view_sub_active.layer.cornerRadius = 2;
    }
    view_active.backgroundColor = colorBackground;
    view_active.layer.borderColor = colorBorder.CGColor;
    view_sub_active.backgroundColor = colorActive;

    [view_active addSubview:view_sub_active];
    UIImage *imgActive = [UIImage imageFromWithView:view_active];
    return imgActive;
}
+ (UIImage *)imageFromWithView:(UIView*)vView
{
    UIView *view = vView;
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end
