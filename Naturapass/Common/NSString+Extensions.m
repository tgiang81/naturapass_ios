/***
 **  $Date: 2014-04-25 10:51:28 +0700 (Fri, 25 Apr 2014) $
 **  $Revision: 5271 $
 **  $Author: CHINHND $
 **  $HeadURL: https://scs01.wisekey.ch:8443/svn/wiseid/iphone/trunk/SRC/Libs/WISeKey/Common/Extensions/NSString+Extensions.m $
 **  $Id: NSString+Extensions.m 5271 2014-04-25 03:51:28Z CHINHND $
***/
//
//  NSString_Extensions.m
//  WISeID
//
//  Created by wisekey on 4/21/11.
//  Copyright 2011 WISeKey SA, All rights reserved.
//

#import "NSString+Extensions.h"

#include <stdlib.h>

#define DEFAULT_LENGTH  8

@implementation NSString (NSStringFunctions)

+ (NSString *)GetUUID
{
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge NSString *)string;
}

-(BOOL) hasSubStringWithPattern:(NSString*) regEx
{
	NSRange r = [self rangeOfString:regEx options:NSRegularExpressionSearch];
    return (r.location != NSNotFound);
}

- (BOOL) hasSubString:(NSString *) str
{
	return [self length] != 0 && [self rangeOfString:str].location != NSNotFound;
}

- (BOOL)containsAnyOf:(NSString *)Strings
{
	for(NSString *str in [Strings componentsSeparatedByString:@","])
	{
		if([self containsString:str])
			return YES;
	}
	return NO;
}

- (BOOL)equalsAnyOf:(NSString *)Strings
{
	for(NSString *str in [Strings componentsSeparatedByString:@","])
	{
		if([self isEqualToString:str])
			return YES;
	}
	return NO;
}


-(NSString*)NullToBlank
{
	if((self==nil)||[self isEqual:[NSNull null]]||[self isEqualToString:@"<null>"])
		return @"";
	else
	{
		return self;
	}
}

-(NSString*)substringBetween:(NSString*)fromStr and:(NSString*)toStr
{
	if(self.length!=0)
	{
		NSRange fromRange = [self rangeOfString:fromStr];
		NSRange toRange = [self rangeOfString:toStr];
		if(fromRange.location != NSNotFound && toRange.location != NSNotFound)
		{
			if(toRange.location>fromRange.location)
			{
				NSInteger x=fromRange.location+fromStr.length;
				NSRange desiredRange=NSMakeRange(x,toRange.location - x);
				NSString *retVal = [self substringWithRange:desiredRange];
				return retVal;
			}
		}
	}
	
	return nil;
}

-(BOOL)containsString:(NSString *)str caseSensitive:(BOOL)caseSensitive
{
	if([self length]!=0)
	{
		NSRange textRange;
		if(caseSensitive)
		{
			textRange =[self rangeOfString:str];
		}
		else
		{
			textRange =[self rangeOfString:str options:NSCaseInsensitiveSearch];
		}
		
		return (textRange.location != NSNotFound);
	}
	
	return NO;
}

-(BOOL)containsString:(NSString *)str
{
	return [self containsString:str caseSensitive:NO];
}

-(BOOL)containsExactString:(NSString *)str
{
	return [self containsString:str caseSensitive:YES];
}

-(BOOL)containsStringWithPattern:(NSString*)regEx
{
	NSRange r = [self rangeOfString:regEx options:NSRegularExpressionSearch];
    return (r.location != NSNotFound);
}

-(BOOL)isEmpty
{
	return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""];
}

-(NSInteger)numberOfLinesInString
{
	return [[self componentsSeparatedByString:@"\n"] count];
}

+(NSString*)stringWithFloat:(float)value maxDecimal:(int)decimalLimit
{
	NSNumber *numVal=[NSNumber numberWithFloat:value];
	NSString *numString=[numVal stringValue];
	
	NSArray *comps=[numString componentsSeparatedByString:@"."];
	if(comps.count>1)
	{
		NSString *pre=[comps objectAtIndex:0];
		NSString *post=[comps objectAtIndex:1];
		if(post.length>decimalLimit)
		{
			post=[post substringToIndex:decimalLimit];
		}
		numString=[NSString stringWithFormat:@"%@.%@",pre,post];
	}
	
	return numString;
}


+ (CGFloat) getHeightOfString:(NSString *)acontent width:(float)width withFont:(UIFont*)font
{
    CGSize size = [acontent sizeWithFont:font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    return size.height;
}

+ (NSString *)stringFromFileSize:(double)theSize
{
    int multiplyFactor = 0;
    
    NSArray *tokens = [NSArray arrayWithObjects:@"bytes",@"KB",@"MB",@"GB",@"TB",nil];
    
    while (theSize > 1024) {
        theSize /= 1024;
        multiplyFactor++;
    }
    
    return [NSString stringWithFormat:@"%4.2f %@",theSize, [tokens objectAtIndex:multiplyFactor]];
}

@end