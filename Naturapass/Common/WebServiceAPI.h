//
//  WebserviceServiceAPI.h
//  PDVLOreal
//
//  Created by William Antwi on 04/01/13.
//  Copyright (c) 2013 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Config.h"
#import "Define.h"

@protocol WebServiceAPIDelegate;

@interface WebServiceAPI : NSObject
{
    NSURLSessionDataTask *task;
}

//initializes it delegate
@property (assign) BOOL bNoNeedResponse;

@property (nonatomic, weak) id <WebServiceAPIDelegate> delegate;

//fix multi response
@property(nonatomic, copy) void (^onComplete)(NSDictionary*, int errCode);

- (NSString *) getSenderId;
- (NSString *) getPHPSESSID;
+ (NSString *) getPHPSESSID;

-(void) resetPHPSESSID;

// Loads the Connection data from the web service
- (void) getRequest:(NSString *)url;

- (void) getRequestForURL:(NSString *)url withTimeOut:(float) iTimeOut;

- (void) putRequest:(NSURL *) putURL;
- (void) putRequestLounge:(NSURL *) putURL withParametersDic:(NSDictionary *)postDict;
 
- (void) postRequest:(NSURL *)postUrl withParametersDic:(NSDictionary *)postDict;
- (void) postRequestData:(NSURL *)postUrl withParametersDic:(NSDictionary *)dict withMedia:(NSData *)mediaData;

- (void) postRequestForURL:(NSString *)urlString;
-(void)postGroupAction:(NSDictionary *)postDict withAttachmentMediaDic:(NSDictionary *) mediaDic;

// Get string as JsonStructure Using NSDictionary 
- (NSString *)getJsonDataFromDictionary:(NSDictionary *)dictionary;
 
-(void) fmGetEmailStatusFor :(GROUP_TYPE) type;
-(void) fmGetPushNotificationStatusFor :(GROUP_TYPE) type;

-(void) fmSetPushNotificationStatusFor :(GROUP_TYPE) type withValue:(NSDictionary*) param;
-(void) fmSetEmailStatusFor :(GROUP_TYPE) type withValue:(NSDictionary*) param;


-(void) getUserFriendsAction;
- (void) postUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDic;

// Publication Actions
- (void) postPublicationCommentAction:(NSString *) publication_id withParametersDic:(NSDictionary *)postDict;
- (void) postMessageData:(NSDictionary *)postDict withMedia:(NSDictionary *) mediaDic;
- (void) postLoungeJoinAction:(NSString *)postID;
- (void) postLoungeMessageAction:(NSString *)strloungeID withParametersDic:(NSDictionary *)postDict;
- (void) postLoungeInviteGroupAction:(NSString *)strlounge_id group:(NSString *)strgroup_id withParametersDic:(NSDictionary *)postDict;
- (void) postLoungeInviteGroup:(NSString *)strlounge_id group:(NSString *)strgroup_id;
- (void) postUserGeolocationAction:(NSString *)struserID withParametersDic:(NSDictionary *)postDict;
- (void) postUpdateUserGeolocationAction:(NSDictionary *)postDict;

- (void) postUserFriendShipAction:(NSString *)struserID;
- (void) postAvatarAction:(NSDictionary *)postDict;
- (void) postPublicationSignalAction:(NSString *)post_ID withParametersDic:(NSDictionary *)postDict;
- (void) postSignalerRequest:(NSURL *)postUrl withParametersDic:(NSDictionary *)postDict;
-(void) postUserFriendsAction:(NSString *)strLounge_id andFriend:(NSString*)friendID;
- (void) postPublicationRequestData:(NSURL *)postUrl withParametersDic:(NSDictionary *)dict;
- (void) postUserProfilePictureActionwithAttachmentImage:(NSDictionary *) imageDict;
- (void) postUserDeviceAction:(NSString *)strType identifier:(NSString *)strIdentifier;
- (void) postFacebookUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDic;
- (void) putFacebookEmailAction:(NSDictionary *)postDict;
-(void) postRequestUploadPhoto: (NSString *)url attachment:(NSDictionary *)attachment forGroup:(BOOL) isGroup;

#pragma publication action
- (void) putPublicationLikeAction:(NSString *)publication_id;
- (void) putPublicationUnLikeAction:(NSString *)publication_id;
- (void) putPublicationCommentLikeAction:(NSString *)comment_id;
- (void) putPublicationCommentUnLikeAction:(NSString *)comment_id;
- (void) putLoungessuscriberParticipateAction:(NSString *)loungesid user_id:(NSString*)user_id withParametersDic:(NSDictionary *)postDict;
- (void) putLoungeGeolocationAction:(NSString *)loungesid  location:(NSString *)strGeolocation withParametersDic:(NSDictionary *)postDict;
- (void) putLoungeSuscriberQuietAction:(NSString *)loungesid withParametersDic:(NSDictionary *)postDict;
- (void) putLoungeSuscriberAdminAction:(NSString *)loungesid suscriber_id:(NSString*)suscriber_id;
- (void) putLoungeSuscriberGeolocationAction:(NSString *)loungesid withParametersDic:(NSDictionary *)postDict;
- (void) putUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDict;
- (void) postChangePasswordAction:(NSDictionary *)postDict;
- (void) putPersonalInformationRequestWithURL:(NSURL *)url attachment:(NSDictionary *)attachment;
- (void) putUserParameterAction:(NSString *)loungesid withParametersDic:(NSDictionary *)postDict;
- (void) putLoungegeolocationActionAdmin:(NSString *)loungesid geolocations:(NSString *)stradminGeolocation;
- (void) putLoungeAction:(NSString *)lounge_id withParametersDic:(NSDictionary *)params imageattachment:(NSDictionary *)attachment;
- (void) putLoungesuscriberPubliccommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict;
- (void) putLoungesuscriberParticipationCommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict;
- (void) putLoungesuscriberPrivatecommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict;
- (void) putRequestPublicPrivate:(NSString *) putURL withParametersDic:(NSDictionary *)postDict;

-(void)putUserFriendshipConfirmAction:(NSString *)strID;
-(void)delUserFriendshipConfirmAction:(NSString *)strID;

- (void) putUserReadNotificationAction:(NSString *)notification_id;
- (void) putReadAllNotification;
- (void) putUserResetPasswordAction:(NSDictionary *)postDict;
- (void) putUpdateGroupItem:(NSDictionary *)postDict group_id:(NSString*)group_id;


- (void) getPublicationsAction:(NSString *)strLimit forSharing:(NSString *)strShareType withOffset:(NSString *)strOffset withGroups:(NSString*) strGroupAppend;
- (void) getPublicationsPrivite:(NSString *)strLimit withOffset:(NSString *)strOffset withFilter:(NSString*) strFilter;
- (void) getPublicationsPublic:(NSString *)strLimit withOffset:(NSString *)strOffset withFilter:(NSString*) strFilter;


- (void) getPublicationsActionItem:(NSString *)publication_id;
- (void) getPublicationsUserAction:(NSString *)strLimit forUserId:(NSString *)strUserId withOffset:(NSString *)strOffset;
- (void) getPublicationCommentsAction:(NSString *)publication_id;
#pragma Lounges Action
- (void) postLoungeInvitationByEmailWithLounge: (NSString *) LoungeID subject:(NSString*)subject toEmails: (NSString *) emailsString withContent: (NSString *) emailContent;
- (void) postGroupInvitationByEmailWithGroupID: (NSString *) groupID subject:(NSString*)subject toEmails: (NSString *) emailsString withContent: (NSString *) emailContent;
- (void) getLoungesAction:(NSString *)strLimit withOffset:(NSString *)strOffset andQuery:(NSString*)query;

- (void) getSearchAction:(NSString *)_strSearch kind:(NSString*)mykind limit:(NSString*)mylimit offset:(NSString*)myoffset;
- (void) getLoungesSearch:(NSString *)search limit:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getLoungeMessageAction:(NSString *)_strSearch limit:(NSString*)limit loaded:(NSString*)loaded;
- (void) getLoungeMessageOffset:(NSString *)_strSearch limit:(NSString*)limit offset:(NSString*)offset;
- (void) getLoungeSubscribeAction:(NSString *)lounge_id;
- (void) getLoungeSubscribeFriendAction:(NSString *)lounge_id;
- (void) getLoungeSuscriberGeolocationAction:(NSString *)lounge_id;
- (void) getUserLoungesAction:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getUserGroupsAction;
- (void) getUserAction;
- (void) getFriendAction:(NSString *)friendId;
- (void) getUserParameterAction;
- (void) getLounge:(NSString *)strlounge_id;
- (void) getUsersSearchAction:(NSString *)_strSearch;
- (void) getLoungeAskAction:(NSString *)_strID;
- (void) getUserWaitingAction;
- (void) getUserNotificationAction;
- (void) getNotificationStatusWithLimit:(NSString*)strLimit offset:(NSString*)strOffset;
- (void) getPushNotificationPublicationAction:(NSString *)object_id;
- (void) getFacebookUserAction:(NSString *)facebook_ID;

- (void) getUserParam;
- (void) deletePublicationActionAction:(NSString *)publication_id;
- (void) deleteCommentPublicationAction:(NSString *)comment_id;
- (void) deletePublicationAction:(NSString *)publication_id;

- (void) deletePublicationCommentActionAction:(NSString *)comment_id;
- (void) deleteLoungeAction:(NSString *)lounge_id;
- (void) deleteLoungeJoinAction:(NSString *)lounge_id;
- (void) deleteUserDeviceAction:(NSString *)type identifier:(NSString *)_identifier;

//LoungeActions
-(void)postLoungePhotoAction:(NSMutableArray *)dicPhoto withAttachmentMediaDic:(NSDictionary *) mediaDic;
- (void) postLoungePhoto:(NSString *)loungeID withData:(NSDictionary*)data;

#pragma Group Action
//Groups Action
- (void) getMyGroupsAction:(NSString *)strLimit forFilter:(NSString *)strFilter withOffset:(NSString *)strOffset;
- (void) deleteGroupAction:(NSString *)groupID;
- (void) unsubscribeGroupAction :(NSString *)userID :(NSString *)groupID;
- (void) getGroupAsks:(NSString*)group_id;
- (void) getGroupAction:(NSString*) group_id;
-(void) getGroupToutesSubscribeAction:(NSString *)group_id;
-(void) getGroupToutesSubscribeFriendAction:(NSString *)group_id;
-(void) getPublicationLikesAction:(NSString *)publication_id;
- (void) getGroupToutesAction:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getGroupToutesSearch:(NSString *)search limit:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getGroupToutesSearchAction:(NSString *)_strSearch;
- (void) getUserGroupToutesAction:(NSString *)strLimit withOffset:(NSString *)strOffset;

- (void) postGroupJoinAction:(NSString *)postID;

-(void) putMailable:(NSString*) group_id mailable:(int) is_mailable  mykind:(NSString *)mykind;

- (void) postGroupPhoto:(NSString *)groupID withData:(NSDictionary*)data;

-(void) postGroupInviteFriendNaturapass:(NSString *)groupID andFriend:(NSString*)friendID;

-(void) postGroupInviteOtherGroup:(NSString *)groupID otherGroup:(NSString*)otherGroupID;

-(void)putGroupSubscribers:(NSString*)group_id subcribers_id:(NSString*)subcribers_id;
//manhld
- (void) getGroupSearch:(NSString *)_strSearch offset:(NSString*)offset;

-(void) getFriendsRecommendations;
#pragma mark - VOIS CHIENS
- (void) getVoisChiensTypeWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getVoisChiensBreedWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;
#pragma mark - WEAPON
- (void) getProfileAllWithKind:(NSString*)kind withLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;

- (void) getWeaponsBrandsWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;

- (void) getWeaponsCalibresWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;

- (void) getProfileItemWithKind:(NSString*)kind withkindID:(NSString *)kind_id withLimit:(NSString *)strLimit withOffset:(NSString *)strOffset;

-(void)postProfileWithParam:(NSDictionary*)params attachment:(NSDictionary*)attachment withKind:(int)type_view;
-(void)putProfileWithKindID:(NSString*)kind_id withKind:(int)type_view  withParams:(NSDictionary*)params attachment:(NSDictionary*)attachment;
- (void) deleteProfileWithKind:(NSString*)kind kindID:(NSString*)kind_id;

- (void) deleteProfileMediaWithKind:(NSString*)kind WithmediaID:(NSString*)media_id;
#pragma mark - V2
//get
- (void) getItemWithKind:(NSString *)mykind myid:(NSString*)myid;
-(void) getCategories;
-(void) getSearchAnimal;
-(void)getUserParametersFriendsAction;
- (void) getAsksWithKind:(NSString*)mykind mykind_id:(NSString*)mykind_id;
- (void) getPublicationsAction:(NSString *)my_id forSharing:(NSString *)strLimit withOffset:(NSString *)strOffset kind:(NSString*) myKind;

- (void) getPublicationMapAction:(NSDictionary*)dicMap listGroup:(NSString *)strGroup_id;

- (void) getMessageActionWithMykind:(NSString *)mykind mykind_id:(NSString*)mykind_id limit:(NSString*)limit loaded:(NSString*)loaded previous:(NSString*)previous;
- (void) getMessageOffsetActionWithMykind:(NSString *)mykind mykind_id:(NSString*)mykind_id limit:(NSString*)limit offset:(NSString*)offset;
-(void)getPublicationFilterOffWithLimit:(NSString*)limit offset:(NSString*)offset  withGroups:(NSString*) strGroupAppend;
- (void) getPublicationMapFilterOff:(NSDictionary*)dicMap;
- (void) getLoungeOldOwner:(NSString *)strLimit withOffset:(NSString *)strOffset;
- (void) getGroupAgendaWithGroupID:(NSString*)group_id ;
- (void) getAllHuntsAdmin;
- (void) getAllGroupsAdmin;
-(void)getLoungeSubscriberNotMember:(NSString*)lounge_id;
-(void)getCategoriesGeolocated:(NSString*)strLat strLng:(NSString*)strLng;
//post
-(void)postInvitionEmailWithDictionary: (NSDictionary *) postDictionary;
- (void) postPublicationLikeAction:(NSString *)publication_id;
- (void) postPublicationCommentLikeAction:(NSString *)comment_id;
- (void) postMessageActionWithKind:(NSString *)mykind mykind_id:(NSString*)mykind_id withParametersDic:(NSDictionary *)postDict;
- (void) postGroupAgendaWithGroupId:(NSString *)group_id  withParametersDic:(NSDictionary *)postDict;
- (void) postLoungeNotMemberWithLoungId:(NSString *)lounge_id  withParametersDic:(NSDictionary *)postDict;
- (void) postUserLock:(NSDictionary *)postDict;
//put
-(void)putAdminSubscribersWithKind:(NSString*)mykind group_id:(NSString*)group_id subcribers_id:(NSString*)subcribers_id;
-(void)putUserParametersFriendsActionWithParametersDic:(NSDictionary *)postDict;
- (void) putJoinWithKind:(NSString*)mykind mykind_id:(NSString *)mykind_id strUserID:(NSString *)strUserID;
- (void) putLoungesuscriberNonmembersParticipion:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict;
- (void) putLoungesuscriberPubliccomment_Nonmembers:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict;
//delete
- (void) deleteJoinWithKind :(NSString*)mykind UserID:(NSString *)userID mykind_id:(NSString *)mykind_id;
- (void) deleteMessageWithKind :(NSString*)mykind mykind_id:(NSString *)mykind_id;
- (void) deleteNonMembers :(NSString*)lounge_id nonmembers_id:(NSString *)nonmembers_id;
- (void) deleteLoungeSubcriberJoin:(NSString*)lounge_id members_id:(NSString *)members_id;
- (void) deleteUserFriendShipAction:(NSString *)struserID;
- (void) deleteUserLock:(NSString *)user_id;
//put
-(void) putEmailNotificationSetting:(NSDictionary*) param;
-(void) putSmartphoneNotificationSetting:(NSDictionary*) param;
-(void) putGroupSmartphoneNotificationSetting:(NSDictionary*) param;
- (void) PUT_COMMENT_PUBLICATION_ACTION:(NSString *) comment_id withParametersDic:(NSDictionary *)postDict;

-(void) fnGET_GET_NEWS;


-(void) fnGET_ALL_NOTIFICATION;

-(void) fnGET_GROUP_PENDING_INVITATION:(NSDictionary*)param;
-(void) fnV2_GET_GROUP_INVITATION;
-(void) fnGET_HUNT_PENDING_INVITATION:(NSDictionary*)param;

-(void) fnGETUSERPROFILE_API;

-(void) fnPOST_MESSAGE_DISCUSSION:(NSDictionary*)param;
-(void) fnPOST_PUBLICATION_FAVORITES:(NSDictionary*)param;
-(void) fnPUT_READ_MESSAGE:(NSDictionary *)postDict;

-(void)fnPOST_JOIN_USER_GROUP:(NSDictionary*) param groupID:(NSString*)groupid;
-(void)fnPOST_JOIN_USER_LOUNGE:(NSDictionary*) param loungeID:(NSString*)loungeid;


-(void)fnPOST_JOIN_AGENDA_GROUP_TO_HUNT:(NSDictionary*) param loungeID:(NSString*)loungeid;

-(void) fnDELETE_GROUP_INVITATION:(NSDictionary*) param;

-(void) fnDELETE_HUNT_INVITATION:(NSDictionary*) param;
-(void)fnPUT_PUBLICATION_EDIT:(NSDictionary*) param;
-(void)fnPUT_UPDATE_PUBLICATION:(NSDictionary*) param withType:(NSString*)type;
-(void)fnPUT_UPDATE_PUBLICATION_ALL:(NSDictionary*) param;
-(void) fnPUT_SQLITE_GET_ALL_PUBLICATION:(NSDictionary*) param;

-(void)fnPUT_OBSERVATION_EDIT:(NSString*)observationID param:(NSDictionary*) param;
- (void) fnPOST_MEDIA_EDIT:(NSString *)object_id withAttachmentMediaDic:(NSDictionary *) mediaDic;

-(void) fnGET_USER_ADDRESS;
-(void) fnPOST_USER_ADDRESS:(NSDictionary*) param;
-(void) fnDELETE_USER_ADDRESS:(NSDictionary*) param;
-(void) fnGET_NOTIFICATION_GLOBAL_PARAMETERS;
-(void) fnGET_EMAIL_PARAMETERS;
-(void) fnGET_NOTI_PARAMETERS;
-(void) fnGET_GROUPS_PARAMETERS_MOBILE:(NSString*)group_id mykind:(NSString *)mykind;
-(void) fnPOST_GROUP_MULTIPLE_JOIN:(NSDictionary*) param;

-(void) fnGET_MES_GROUP_HUNT;
- (void) putRequestWithoutCookie: (NSDictionary*) postDict;

-(void) fnGET_DASHBOARD_LIVE_HUNT;

//FAVORITE PUBLICATION
-(void) fnDELETE_FAVORITE_PUBLICATION:(NSString*) favorite_id;
-(void) fnGET_FAVORITE_PUBLICATION:(NSString*) favorite_id;
-(void) fnPUT_FAVORITE_PUBLICATION:(NSString*) favorite_id wwithParam:(NSDictionary*)param;
-(void) fnGET_FAVORITE_PUBLICATIONLIST:(NSString*)limit offset:(NSString*)offset;

-(void) fnGET_USER_CONVERSATIONS;
-(void) fnGET_CONVERSATIONS_MESSAGE:(NSString *)conversationID limit:(NSString*)strLimit offset:(NSString*)strOffset;
//get discustion
-(void) fnGET_ALL_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset limit_hunt:(NSString*)limit_hunt offset_hunt:(NSString*)offset_hunt limit_group:(NSString*)limit_group offset_group:(NSString*)offset_group;
-(void) fnGET_DISCUSSTION_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
-(void) fnDELETE_GENERAL_CHAT_MESSAGE:(NSString*)msg_id;
-(void) fnGET_LOUNGE_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
-(void) fnGET_GROUP_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
-(void)fnPOST_CREATE_DISCUSSION:(NSDictionary*) param;

-(void)fnGET_CATEGORIES_BY_RECEIVER:(NSString*)strReciever;
-(void)fnGET_LIVE_HUNT_PUBLICATION_DISCUSSION:(NSString*)hunt_id limit:(NSString*)strlimit offset:(NSString*)strOffset;
-(void) getInfoLocation:(NSString *)lat withLng:(NSString*)lng;
-(void) getInfoWeatherMap:(NSString *)lat withLng:(NSString*)lng withKey:(NSString*)key;
-(void) getInfoWindMap:(NSString *)lat withLng:(NSString*)lng withKey:(NSString*)key;
-(void) getUserNotificationStatus:(NSString *)strCount;
-(void) getInfoInterstice;
-(void) getToCacheSystem;
-(void) getPublicationColor;
-(void) getUserLock;
-(void)fnPOST_JOIN_GROUP_TO_HUNT:(NSDictionary*) param loungeID:(NSString*)loungeid;

//V2 GET
- (void) getTheFriends:(BOOL) isFriend withID:(NSString*)strID;

- (void) postAjouterFriendShips:(NSString *)the_id;

-(void) fnGET_HUNT_LIST_CITIES:(NSDictionary*)param;
-(void) fnGET_HUNT_LIST_COUNTRIES:(NSDictionary*)param;
-(void) fnGET_HUNT_TYPES:(NSDictionary*)param;

-(void) fnGET_HUNT_CITY_LOCATION;
- (void) fnADD_HUNT_CITY_LOCATION:(NSString *)cityID;
- (void) fnDEL_HUNT_CITY_LOCATION:(NSString *)cityID;

-(void) fnGET_HUNT_COUNTRY_LOCATION;
- (void) fnADD_HUNT_COUNTRY_LOCATION:(NSString *)countryID;
- (void) fnDEL_HUNT_COUNTRY_LOCATION:(NSString *)countryID;

-(void) fnGET_HUNT_PRACTICE;
- (void) fnADD_HUNT_PRACTICE:(NSString *)huntTypeID;
- (void) fnDEL_HUNT_PRACTICE:(NSString *)huntTypeID;

- (void) PUT_SQLITE_ALL_POINTS_IN_MAP_USING_DEFAULT_DB:(NSDictionary *)param;

-(void) fnGET_HUNT_LIKED;
- (void) fnADD_HUNT_LIKED:(NSString *)huntTypeID;
- (void) fnDEL_HUNT_LIKED:(NSString *)huntTypeID;

-(void) fnPUT_GROUP_allow_show_EDIT:(NSString*)group_id withParam:(NSDictionary*)param;
-(void) fnPUT_GROUP_allow_add_EDIT:(NSString*)group_id withParam:(NSDictionary*)param;

-(void) fnPUT_HUNT_allowaddshow_Publication_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param;
-(void) fnPUT_HUNT_allowaddshow_Chat_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param;

-(void) fnPUT_GROUP_allowaddshow_Publication_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param;
-(void) fnPUT_GROUP_allowaddshow_Chat_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param;


- (void) PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:(NSDictionary *)param;
- (void) fnGET_MES_GROUP_SEARCH:(NSString *)_strSearch offset:(NSString*)offset;

-(void) fnGET_HUNT_INFORMATION: (NSString*)huntID;

-(void) fnDELETE_MESSAGE_HUNT_CHAT:(NSString*)strMsg;

-(void) fnDELETE_MESSAGE_GROUP_CHAT:(NSString*)strMsg;

-(void) cancelTask;

-(void) fnGET_CHECK_LINKTO_RECEIVER;

-(void)putProfileFilesWithKindID:(NSString*)kind_id attachment:(NSDictionary*)attachment withParam:(NSDictionary*)params  withKind:(int)type_view;
-(void) fnPUT_USER_IDENTIFIER_WITHPARAM:(NSDictionary *)param;
-(void)fnPOST_SHAPES:(NSDictionary*) param;
-(void)fnPUT_SHAPES:(NSDictionary*) param withShapeID:(NSString*)shapeID;
-(void)fnDELETE_SHAPES:(NSString*) shapeID;
@end
#pragma mark ----
@protocol WebServiceAPIDelegate <NSObject>

@optional
- (void)webServiceDidSucceedSync:(WebServiceAPI *)webService;
- (void)webServiceDidFailSync:(WebServiceAPI *)webService error:(NSError *)error;

- (void)webServiceDidStartSync:(WebServiceAPI *)webService;
- (void)webServiceDidSucceedSync:(WebServiceAPI *)webService responseDict:(id)response requestUrl:(NSString *)url;



@end
