//
//  ASImageView.m
//  Naturapass
//
//  Created by Clément Padovani on 10/6/14.
//  Copyright (c) 2014 Appsolute. All rights reserved.
//

#import "ASImageView.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import "Config.h"
#import "Define.h"

@interface ASImageView ()

@property (nonatomic, weak) UIActivityIndicatorView *spinnerView;

@property (nonatomic, weak) UIImageView *imageView;

@end

@implementation ASImageView

- (instancetype) initWithFrame: (CGRect) frame forImagePath: (NSString *) imagePath
{
    if (!imagePath) {
        return nil;
    }
    
	NSParameterAssert(imagePath);
	
	self = [super initWithFrame: frame];
	
	if (self)
	{
		_imagePath = [imagePath copy];
		
		UIImageView *imageView = [[UIImageView alloc] initWithFrame: [self bounds]];
		
		[imageView setHidden: YES];
		
		UIActivityIndicatorView *spinnerView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
		
		[spinnerView setHidesWhenStopped: YES];
		
		[spinnerView startAnimating];
		
		[spinnerView setFrame: [self bounds]];
		
		[self addSubview: imageView];
		
		[self addSubview: spinnerView];
		
		[self setImageView: imageView];
		
		[self setSpinnerView: spinnerView];
		
        NSString *fullPath = IMAGE_ROOT_API;
        fullPath = [fullPath stringByAppendingString: imagePath];
        NSURL *imageURL = [NSURL URLWithString: fullPath];
        
		__weak ASImageView *weakSelf = self;
		
        [imageView setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            __strong ASImageView *strongSelf = weakSelf;
            if (!strongSelf)
                ASLog(@"nil strong self");
            [[strongSelf imageView] setImage: image];
            [[strongSelf spinnerView] stopAnimating];
            [[strongSelf imageView] setHidden: NO];
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            __strong ASImageView *strongSelf = weakSelf;
            if (!strongSelf)
                ASLog(@"nil strong self");
            [[strongSelf spinnerView] stopAnimating];
            [[strongSelf imageView] setHidden: NO];
        }];
	}
	
	return self;
}

- (void) loadImageComplete
{
    
}

- (void) setContentMode: (UIViewContentMode) contentMode
{
	[super setContentMode: contentMode];
	
	[[self imageView] setContentMode: contentMode];
}

- (void) setImagePath: (NSString *) imagePath
{
	NSParameterAssert(imagePath);
	
//	if ([imagePath isEqualToString: [self imagePath]])
//		return;
	
	[[self spinnerView] startAnimating];
	
	[[self imageView] setHidden: YES];
	
	_imagePath = [imagePath copy];
	
    
    NSString *fullPath = IMAGE_ROOT_API;
    fullPath = [fullPath stringByAppendingString: _imagePath];
    NSURL *imageURL = [NSURL URLWithString: fullPath];
    
	__weak ASImageView *weakSelf = self;
	
    [[self imageView] setImageWithURLRequest:[NSURLRequest requestWithURL:imageURL] placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        __strong ASImageView *strongSelf = weakSelf;
        if (!strongSelf)
            ASLog(@"nil strong self");
        [[strongSelf imageView] setImage: image];
        [[strongSelf spinnerView] stopAnimating];
        [[strongSelf imageView] setHidden: NO];
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        __strong ASImageView *strongSelf = weakSelf;
        if (!strongSelf)
            ASLog(@"nil strong self");
        [[strongSelf spinnerView] stopAnimating];
        [[strongSelf imageView] setHidden: NO];
    }];
}

@end
