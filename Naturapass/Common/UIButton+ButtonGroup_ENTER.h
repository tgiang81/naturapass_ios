//
//  UIButton+ButtonGroup_ENTER.h
//  Naturapass
//
//  Created by Giang on 8/20/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (ButtonGroup_ENTER)
-(void)setImage_Mur;
-(void)setImage_Carte;
-(void)setImage_Discuss;
-(void)setImage_Agenda;
-(void)setImage_Setting;

-(void)setImage_GroupMes;
-(void)setImage_GroupSearch;
-(void)setImage_GroupSetting;

-(void)setImage_MUR_mur;
-(void)setImage_MUR_filter;
-(void)setImage_MUR_sestting;

-(void)setImage_CHASSE_WALL_MUR;
-(void)setImage_CHASSE_WALL_SEARCH;
-(void)setImage_CHASSE_HISTORY;
-(void)setImage_CHASSE_WALL_SETTING;

-(void)setImage_Amis_List;
-(void)setImage_Amis_Add;
-(void)setImage_Amis_Demand_Invitation;
-(void)setImage_Amis_Search;
-(void)setImageWithImgStateNomal:(NSString*)imgStateNomal ImgStateSelected:(NSString*)ImgStateSelected;
@end
