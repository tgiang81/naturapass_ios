
//
//  NSDate+Extensions.h
//  demo
//
//  Created by Admin on 3/29/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"

typedef enum {
	WKDateFormatFull        = 0,
	WKDateFormatMonthYear	= 1,
	WKDateFormatYear		= 2,
	WKDateFormatMonthDay	= 3
} WKDateFormatType;

@interface NSDate (NSDateForNSString)

+ (NSDate*)dateFor:(DateType) dateType;

+(NSString*)currentGMTZeroTime;

+(NSString*)convertToFranceTime :(NSDate*) inDate;


+(NSString *)currentRFC822TimeYear:(NSString*)dInput;
+(NSString *)currentRFC822TimeNoYear:(NSString*)dInput;

+(NSDate*)GMTZeroDateForString:(NSString*)utcDateString;

+(NSDate *)convertNSStringDateToNSDate:(NSString *)string;

+(NSString*)convertNSDateToNSString:(NSDate*)date;

+(int)numberOfDaysFromDate:(NSDate*)fromDate ToDate:(NSDate*)toDate;

+(NSString*)generalDateFromUTCDate:(NSString*)utcDate;

+(NSString*)UTCDateFromGeneralDate:(NSString*)generalDate;

+(NSString*)currentStandardUTCTime;

+(NSString*)currentLocalUTCTime;

+(NSString*)generalDatefromUTCMonDayFormat:(NSString*)utcDate;

+(NSString*)UTCDatefromGeneralMonDayFormat:(NSString*)generalDate;

+ (NSString *) localTimeFromStandardUTCTime:(NSString *)timeStr;

+(NSString*)currentStandardUTCTimeGMTPLUS :(NSString*) strGMT_2;


+(NSDate *)convertNSStringDateToNSDateX1:(NSString *)string;

+(NSDate *)convertNSStringDateToNSDatePicker:(NSString *)string;

-(NSString *)timeAgo;

+(NSString*)convertToStandardUTCTimeGMTPLUS :(NSString*) strGMT_2 withDate:(NSDate*)myDate;
+(NSString *)convertTimeIntervalToNSTring:(NSTimeInterval )interval;
+(NSString *)convertTimeInterval:(NSTimeInterval )interval withFormat:(NSString*)strFormat;
+(NSDate*)convertDateToDateGMT2 :(NSDate*) dateLocal;
 +(NSTimeInterval)getBeginToday;
 +(NSTimeInterval)getEndToday;
 +(NSTimeInterval)getBeginYesterday;
 +(NSTimeInterval)getEndYesterday;
 +(NSTimeInterval)getBeginThisWeek;
 +(NSTimeInterval)getEndThisWeek;
 +(NSTimeInterval)getBeginLastWeek;
 +(NSTimeInterval)getEndLastWeek;
 +(NSTimeInterval)getBeginThisMonth;
 +(NSTimeInterval)getEndThisMonth;
 +(NSTimeInterval)getBeginLastMonth;
 +(NSTimeInterval)getEndLastMonth;
 +(NSTimeInterval)getBeginThisYear;
 +(NSTimeInterval)getEndThisYear;
 +(NSTimeInterval)getBeginLastYear;
 +(NSTimeInterval)getEndLastYear;
+(NSString*)convertDateHeaderAgenda:(NSString*)inputDate;
+(NSTimeInterval)getBeginLast30;
+(NSTimeInterval)getEndLast30;
+(NSTimeInterval)getBeginWithStringDate:(NSString*)dateString;
+(NSTimeInterval)getEndWithStringDate:(NSString*)dateString;
+(NSString *)convertAgendaToNSTring:(NSString *)string;
+(NSString *)convertDateGmt2ToSystem:(NSString *)string withInputFormat:(NSString *)inputFormat withOupuFormat:(NSString*)outputFormat;
+(NSComparisonResult)compareLocalDateWithInput:(NSString*)stringDate;
@end
