//
//  CommonHelper.h
//  Naturapass
//
//  Created by Giang on 8/25/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonHelper : NSObject
+ (CommonHelper *) sharedInstance;
@property (assign) BOOL isActiveOverlay;


-(void)setRoundedView:(UIView *)roundedView toDiameter:(float)newSize;
-(NSString*) timeLeftSinceDate: (NSString *) dateStr;
+ (NSString *)generatorString;
+(NSString*)convertDate:(NSString*)date;
-(NSString*) getRandKey;
-(NSString*) getRandKeyGoogleSearch;
- (UIImage *)buildThumbnailImage:(CGPDFDocumentRef)pdfDocument;
+ (NSString *) stringToMD5:(NSString *)key;

@end
