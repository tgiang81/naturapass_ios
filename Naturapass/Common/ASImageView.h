//
//  ASImageView.h
//  Naturapass
//
//  Created by Clément Padovani on 10/6/14.
//  Copyright (c) 2014 Appsolute. All rights reserved.
//

@import UIKit;

@interface ASImageView : UIView

@property (nonatomic, copy) NSString *imagePath;

- (instancetype) initWithFrame: (CGRect) frame forImagePath: (NSString *) imagePath NS_DESIGNATED_INITIALIZER;

@end
