//
//  UIImage+alpha.h
//  Naturapass
//
//  Created by Giang on 10/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (alpha)
- (UIImage *)imageByApplyingAlpha:(CGFloat) alpha;

@end
