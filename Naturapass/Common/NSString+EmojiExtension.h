//
//  NSString+EmojiExtension.h
//  Naturapass
//
//  Created by manh on 11/18/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EmojiExtension)
- (NSString*)removeEmoji;
@end
