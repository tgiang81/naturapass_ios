//
//  MDCheckBox.m
//  Naturapass
//
//  Created by Manh on 7/30/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDCheckBox.h"
#import "Define.h"
@implementation MDCheckBox
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        
        [self setImage: [UIImage imageNamed:@"checkbox_active"] forState:UIControlStateSelected];
        [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    [self setImage: [UIImage imageNamed:@"checkbox_active"] forState:UIControlStateSelected];
    [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
}

-(void)setTypeCheckBox:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        case UI_GROUP_MUR_NORMAL:
        {
            
            [self setImage: [UIImage imageNamed:@"ic_check_active_blue"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;

        case UI_GROUP_TOUTE:
        {
            [self setImage: [UIImage imageNamed:@"ic_check_active_blue"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        case UI_CHASSES_MUR_NORMAL:
        {
            [self setImage: [UIImage imageNamed:@"ic_chasse_checkbox_active"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"ic_chasse_checkbox_inactive"] forState:UIControlStateNormal];
        }
            break;

        case UI_CHASSES_TOUTE:
        {
            [self setImage: [UIImage imageNamed:@"checkbox_active"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;
        case UI_CARTE:
        {
            [self setImage: [UIImage imageNamed:@"ic_check_active_pink"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;
        case UI_AMIS_DELETE:
        {
            [self setImage: [UIImage imageNamed:@"ic_remove_contact"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;
        case UI_DISCUSSION:
        {
            [self setImage: [UIImage imageNamed:@"discussion_bg_checkbox_selected"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        }
            break;

        default:
            break;
    }

}
@end
