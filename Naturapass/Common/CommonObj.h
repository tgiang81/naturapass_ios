//
//  CommonObj.h
//  Naturapass
//
//  Created by Giang on 9/23/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommonObj : NSObject
{
}
@property (assign) int nbChasseInvitation;
@property (assign) int nbGroupInvitation;
@property (assign) int nbUnreadMessage;
@property (assign) int nbUnreadNotification;
@property (assign) int nbUserWaiting;

//
@property (assign) int nbLiveMur;
@property (assign) int nbLiveChat;

+ (CommonObj *) sharedInstance;

@end
