//
//  AppCommon.m
//  Arcatime
//
//  Created by ocsdeveloper12 on 16/07/13.
//  Copyright (c) 2013 ocsdeveloper12. All rights reserved.
//

#import "AppCommon.h"
#import <CommonCrypto/CommonDigest.h>
#import "Config.h"
#include <sys/xattr.h>
#import "UIImage+ProportionalFill.h"
#import "FileHelper.h"
#import "Define.h"
#import "ServiceHelper.h"

AppCommon *SharedCommon = nil;
static int countNetworkIssue;

@implementation AppCommon

+ (AppCommon *) common
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		SharedCommon = [[self alloc] init];
        countNetworkIssue = 0;
	});
	
	return SharedCommon;
}

//+(AppCommon *) common {
//    if(!SharedCommon) SharedCommon = [[self alloc] init];
//    return SharedCommon;
//}

- (void) setPushAreEnabled:(BOOL)pushAreEnabled
{
//	#if (TARGET_IPHONE_SIMULATOR)
//	
//		_pushAreEnabled = YES;
//	
//		return;
//	
//	#endif
//	
//	_pushAreEnabled = pushAreEnabled;
//	
//	ASLog(@"push are enabled: %@", pushAreEnabled ? @"YES" : @"NO");
}

//- (id)init {
//	return self;
//}
//


#pragma mark - Get App Name

- (NSString *)getAppName{
    NSString *strAppName = @"";
    strAppName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    
    return strAppName;
}

#pragma mark - Reachable
-(BOOL) isReachable {
    
    if(!([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) || !([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == NotReachable)) {
        countNetworkIssue = 0;
        
          return YES;
    }else{
        countNetworkIssue += 1;
        
        if (countNetworkIssue > 3) {
            //Only notify 3 times....with Nework issue.
            return NO;
        }
        
        if (!alert) {
            alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                               message:str(strNETWORK)
                                              delegate:self
                                     cancelButtonTitle:str(strOK)
                                     otherButtonTitles:nil];
        }
        
        if (!self.isShowned) {
            [alert show];
            [alert setTag:10];
            alert.delegate = self;
            
        }
            return NO;

    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    self.isShowned = NO;
}

- (BOOL) isReachableCheck {
     if(!([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable) || !([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == NotReachable))
        return YES;
    else
        return NO;
}

#pragma mark - Alert Functions

- (void) showErrorAlert:(NSString *)strMessage {
    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                         message:strMessage
                                                        delegate:self
                                               cancelButtonTitle:str(strOK)
                                               otherButtonTitles:nil];
    [errorAlert show];
}


#pragma mark - Loading

-(void) LoadIconinView:(UIView *)view
{
    if (loadingView) 
        [self RemoveLoading];
   
    loadingView = [[UIView alloc] initWithFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width -80)/2, ([UIScreen mainScreen].bounds.size.height-160)/2, 80, 80)];
	[loadingView setBackgroundColor:[UIColor blackColor]];
//    [loadingView setAlpha:0.5];
    //Enable maskstobound so that corner radius would work.
	[loadingView.layer setMasksToBounds:YES];
    //Set the corner radius
	[loadingView.layer setCornerRadius:10.0];    
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[activityView setFrame:CGRectMake(21, 21, 37, 37)];
	[activityView setHidesWhenStopped:YES];
	[activityView startAnimating];
	[loadingView addSubview:activityView];
    [view addSubview:loadingView];
}

- (void) upLoadIconinView:(UIWindow *) view{
    if (loadingView)
        [self RemoveLoading];
    
    if ([COMMON isiPhone]) {
           loadingView = [[UIView alloc] initWithFrame:CGRectMake((view.frame.size.width-100)/2, (view.frame.size.height-100)/2, 100, 100)];
    }else{
           loadingView = [[UIView alloc] initWithFrame:CGRectMake(334, 462, 100, 100)];
    }
 
	[loadingView setBackgroundColor:[UIColor blackColor]];
    [loadingView setAlpha:0.8];
    //Enable maskstobound so that corner radius would work.
	[loadingView.layer setMasksToBounds:YES];
    //Set the corner radius
	[loadingView.layer setCornerRadius:10.0];
    activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	[activityView setFrame:CGRectMake(31, 31, 38, 38)];
	[activityView setHidesWhenStopped:YES];
	[activityView startAnimating];
	[loadingView addSubview:activityView];
        
    [view addSubview:loadingView];
}

-(void) RemoveLoading {
    [loadingView removeFromSuperview];
}
#pragma mark Loading

- (void)addLoading:(UIViewController *)controller
{
    [self removeProgressLoading];
    progressHUD = [[MBProgressHUD alloc] init];
    progressHUD = [[MBProgressHUD alloc] initWithView:controller.view];
    [controller.view addSubview:progressHUD];
    [progressHUD show:YES];
}

- (void)addLoadingForView:(UIView *)parent
{
    [self removeProgressLoading];
    progressHUD = [[MBProgressHUD alloc] init];
    progressHUD = [[MBProgressHUD alloc] initWithView:parent];
    [parent addSubview:progressHUD];
    [progressHUD show:YES];
}
-(void)removeProgressLoading
{
    [progressHUD hide:YES];
    [progressHUD removeFromSuperview];
}


#pragma mark - UserInterfaceIdiom Functions

- (BOOL) isiPhone{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
	    
	    //ASLog(@"is iphone");
	    
        return YES;
    }
	
	//ASLog(@"not iphone");
	
    return NO;
}

- (BOOL) isiPhone4{
    if ([UIScreen mainScreen].bounds.size.height == 480) {
	    
	    //ASLog(@"is iphone 4");
	    
        return YES;
    }
	
	//ASLog(@"not iphone 4");
	
    return NO;
}

- (BOOL) isIOS6
{
	//NSString *versionString = [[UIDevice currentDevice] systemVersion];
	
    if ([[[UIDevice currentDevice] systemVersion] integerValue] < 7) {
	    
	    //NSLog(@"is ios 6");
	    
        return YES;
    }

	//NSLog(@"is ios 7");
	
    return NO;
}

#pragma mark - Is Logged in

- (BOOL)isLoggedIn{
    NSString *strUserId = [NSString stringWithFormat:@"%@",[self getUserId]];
    
    if ([strUserId isEqualToString:@""] || [strUserId isEqualToString:@"(null)"] || [strUserId isEqualToString:@"nil"]) {
        return NO;
    }
    
    return YES;
}

- (void) fbDidLogout
{
	[self doLogout];
}

- (void) doLogout
{
    [[ServiceHelper sharedInstance] cancelAllPerform];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"sender_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"my_email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserPassword"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FB_UserDetails"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FBAccessTokenKey"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FBExpirationDateKey"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Filter_deal"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Radius_KM"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Selected_Categories"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SLIDEGROUP"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"GROUPSHARINGS"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"TICKBOX"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SETGROUPS"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SLIDEVALUE"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PHPSESSID" ];

    [[NSUserDefaults standardUserDefaults] synchronize];
    

}

- (NSString *)getUserId{
    NSString *strUserId= @"";
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] != NULL) {
        strUserId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"]];
    }
    return strUserId;
}

-(NSString *)characterTrimming:(NSString *)str{
   str = [[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return str;
}

- (NSString *)getIdiomImage:(NSString *)imgName{
    if ([self isiPhone]) {
        return imgName;
    }
    return [NSString stringWithFormat:@"%@_ipad",imgName];
}

//- (NSString *)removeAccentsfromString:(NSString *)text{
//    NSData *asciiEncoded = [text dataUsingEncoding:NSASCIIStringEncoding
//                              allowLossyConversion:YES];
//    
//    // take the data object and recreate a string using the lossy conversion
//    NSString *other = [[NSString alloc] initWithData:asciiEncoded
//                                            encoding:NSASCIIStringEncoding];
//    
//    return other;
//    
//}
#pragma Mark -ImageResize

- (UIImage *)resizeImage:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize{
    return [imageToCrop imageCroppedToFitSize:_targetSize];
}

- (UIImage *)resizeimageScaled:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize{
    return [imageToCrop imageScaledToFitSize:_targetSize];
}

#pragma mark - TypeOFUser

-(void)settypeOfUser:(BOOL)userType{
    //@1 - Admin --// @0 - User
    [[NSUserDefaults standardUserDefaults]setBool:userType forKey:@"AdminOrUser"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)returnTypeOfUser{
    BOOL userType = [[[NSUserDefaults standardUserDefaults] valueForKey:@"AdminOrUser"] boolValue];
    return userType;
}

// TODO: REWRITE ME

- (NSString *) makeItSecret: (NSString *) aSecret
{
	NSString *theSecret;
	
	const char *something = [aSecret UTF8String];
	
	uint8_t anotherThing[CC_SHA1_DIGEST_LENGTH];
	
	CC_SHA1(something, (CC_LONG) strlen(something), anotherThing);
	
	NSMutableString *somethingElse = [NSMutableString stringWithCapacity: CC_SHA1_DIGEST_LENGTH * 2];
	
	for (NSUInteger i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
	{
		[somethingElse appendFormat: @"%02x", anotherThing[i]];
	}
	
	theSecret = [somethingElse copy];
	
	
	
	return theSecret;
}

- (NSString *) encrypt: (NSString *) input
{
	NSString *secret = [self makeItSecret: input];
	
	[self setUserPasswordTextValue: secret];
	
	return secret;
}

//-(NSString*) encrypt:(NSString*)input
//{
//    NSData *data = [input dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
//    CC_SHA1(data.bytes, data.length, digest);
//    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
//    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
//        [output appendFormat:@"%02x", digest[i]];
//    [self setUserPasswordTextValue:output];
//    
//    return output;
//    
//}
- (void)setUserPasswordTextValue:(NSString *)strPassword{
    if (![strPassword isEqualToString:@""] && ![strPassword isEqualToString:@"(null)"]) {
        [[NSUserDefaults standardUserDefaults] setValue:strPassword forKey:@"UserPassword"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
#pragma  mark - Cell Height

- (CGSize)dataSize:(NSString *)string withFontName:(NSString *)fontName ofSize:(NSInteger)size withSize:(CGSize)LabelWidth {
    CGSize maxSize = LabelWidth;
    CGSize dataHeight;
    
    UIFont *font = [UIFont fontWithName:fontName size:size];
    
    NSString *version = [[UIDevice currentDevice] systemVersion];
    if ([version floatValue]>=7.0) {
	    
        CGRect textRect = [string boundingRectWithSize:maxSize
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                            attributes:@{NSFontAttributeName:font}
                                               context:nil];
	    
        dataHeight = CGSizeMake(textRect.size.width , textRect.size.height);
        
    }else{
	    
        dataHeight = [string sizeWithFont:font constrainedToSize:maxSize lineBreakMode:NSLineBreakByCharWrapping];
    }
    
    return CGSizeMake(ceilf((float) dataHeight.width), ceil((float) dataHeight.height + 15));
}

#pragma mark - keychain store

- (void) storeKeychain {
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"ID" accessGroup:nil];
    //ASLog(@"username =%@",[keychain objectForKey:(__bridge id)kSecAttrAccount]);
    //ASLog(@"textFieldPassword4 =%@",[keychain objectForKey:(__bridge id)kSecValueData]);
    
}
#pragma mark - Set and Get Fecebook User Details

- (void)setFacebookUserDetails:(NSMutableDictionary *)dictUserDetails{
    if (dictUserDetails != NULL) {
        [[NSUserDefaults standardUserDefaults] setObject:dictUserDetails forKey:@"FB_UserDetails"];
        //[[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"FBLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
	    
	    NSString *passwordString = dictUserDetails[@"id"];
	    
	    if (passwordString && [passwordString length])
		    [self makeItSecret: passwordString];
	    
        NSString *strPassword = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"id"]];
        if (![strPassword isEqualToString:@""] && ![strPassword isEqualToString:@"(null)"])
            [self encrypt:strPassword];
    }
}

- (NSString *) keyChainUserID{
    [self storeKeychain];
    NSString *strUserName = [keychain objectForKey:(__bridge id)kSecAttrAccount];
    return strUserName;
}

- (NSString *) keyChainPasswordID{
    [self storeKeychain];
    NSString *strPasswordName = [keychain objectForKey:(__bridge id)kSecValueData];
    return strPasswordName;
}


- (NSString *)getFBUserNickName{
    NSString *userFbName = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"name"]];
    if ([userFbName isEqualToString:@"(null)"] || [userFbName isEqualToString:@""]) {
        return @"";
    }
    return userFbName;
}

- (NSString *)getFBUserFirstName{
    NSString *userFbName = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"first_name"]];
    if ([userFbName isEqualToString:@"(null)"] || [userFbName isEqualToString:@""]) {
        return @"";
    }
    return userFbName;
}

- (NSString *)getFBUserLastName{
    NSString *userFbLastName = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"last_name"]];
    if ([userFbLastName isEqualToString:@"(null)"] || [userFbLastName isEqualToString:@""]) {
        return @"";
    }
    
    return userFbLastName;
    
}

- (NSString *)getFBUserId{
    NSString *userFbUserId = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"id"]];
    if ([userFbUserId isEqualToString:@"(null)"] || [userFbUserId isEqualToString:@""]) {
        return @"";
    }
    
    return userFbUserId;
}

- (NSString *)getFBUserDob{
    NSString *userFbUserDob = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"birthday"]];
    if ([userFbUserDob isEqualToString:@"(null)"] || [userFbUserDob isEqualToString:@""]) {
        return @"";
    }
    
    return userFbUserDob;
}


- (NSString *)getFBUserEmail{
    NSString *userFbEmail = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"email"]];
    if ([userFbEmail isEqualToString:@"(null)"] || [userFbEmail isEqualToString:@""]) {
        return @"";
    }
    return userFbEmail;
}

- (NSString *)getFBUserGender{
    NSString *userFbGender = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] valueForKey:@"gender"]];
    if ([userFbGender isEqualToString:@"(null)"] || [userFbGender isEqualToString:@""]) {
        return @"";
    }
    return userFbGender;
}

- (NSString *)getFBUserImage{
    NSString *userFbImage = [NSString stringWithFormat:@"%@",[[[[[NSUserDefaults standardUserDefaults] objectForKey:@"FB_UserDetails"] objectForKey:@"picture"] objectForKey:@"data"] valueForKey:@"url"]];
    if ([userFbImage isEqualToString:@"(null)"] || [userFbImage isEqualToString:@""]) {
        return @"";
    }
    return userFbImage;
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark - FB
- (void)saveFBDetails:(NSDictionary *)_dic
{
    [[NSUserDefaults standardUserDefaults] setValue:_dic forKey:@"FB_UserDetails"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void)listSubviewsOfView:(UIView *)view {
    
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
                
        // List the subviews of subview
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *lb = (UILabel*)subview;

            if ([self isGrayColor:lb.textColor withTolerance:0.02]) {
                lb.textColor = [UIColor blackColor];
            }
        }
//        else if ([subview isKindOfClass:[UIButton class]]) {
//            UIButton *btn = (UIButton*)subview;
//            if ([self isGrayColor:[btn titleColorForState:UIControlStateNormal] withTolerance:1]) {
//                
//                [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                
//            }
//        }
        else if ([subview isKindOfClass:[UITextView class]]) {
            UITextView *tv = (UITextView*)subview;
            if ([self isGrayColor:tv.textColor withTolerance:0.02]) {
                tv.textColor = [UIColor blackColor];
            }
            
        }
        else if ([subview isKindOfClass:[UITextField class]]) {
            UITextField *tf = (UITextField*)subview;
            if ([self isGrayColor:tf.textColor withTolerance:0.02]) {
                tf.textColor = [UIColor blackColor];
            }
        }
        
        [self listSubviewsOfView:subview];
    }
}
- (BOOL)isGrayColor:(UIColor *)color1
      withTolerance:(CGFloat)tolerance {
    CGFloat r1, g1, b1, a1;
    [color1 getRed:&r1 green:&g1 blue:&b1 alpha:&a1];
    return
    fabs(r1 - g1) <= tolerance &&
    fabs(g1 - b1) <= tolerance &&
    fabs(b1 - r1) <= tolerance &&
    b1<=0.667;
}
@end
