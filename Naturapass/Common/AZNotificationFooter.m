//
//  AZNotification.m
//  Helping
//
//  Created by Mohammad Azam on 3/27/14.
//  Copyright (c) 2014 AzamSharp Consulting LLC. All rights reserved.
//

#import "AZNotificationFooter.h"

@implementation AZNotificationFooter

-(void) showNotificationWithTitle:(NSString *)title controller:(UIViewController *)controller notificationType:(AZNotificationType)notificationType shouldShowNotificationUnderNavigationBar:(BOOL)shouldShowNotificationUnderNavigationBar
{
    notificationView = [[AZNotificationView alloc] initWithTitleFooter:title referenceView:controller.view notificationType:(AZNotificationType) notificationType showNotificationUnderNavigationBar:YES];
    
    [controller.view addSubview:notificationView];
}

-(void) showNotificationWithTitle:(NSString *)title controller:(UIViewController *)controller notificationType:(AZNotificationType)notificationType
{
    controller = controller.navigationController ? controller.navigationController : controller;
    
    notificationView = [[AZNotificationView alloc] initWithTitleFooter:title referenceView:controller.view notificationType:(AZNotificationType) notificationType];
    
    [controller.view addSubview:notificationView];
}
-(void)hidenNotification
{
    [notificationView removeFromSuperview];
}
-(void)setTitleNotif:(NSString*)title
{
    [notificationView setTitle:title];
}
@end
