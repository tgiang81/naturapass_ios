//
//  BaseDesignableView.h
//  Naturapass
//
//  Created by giangtu on 10/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BaseDesignableView : UIImageView{
}

@property (assign, nonatomic) IBInspectable CGFloat borderWidth;
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;
@property (strong, nonatomic) IBInspectable UIColor *borderColor;
@property (assign, nonatomic) IBInspectable BOOL isRounded;
@property (assign, nonatomic) IBInspectable BOOL isHeighChange;

@end
