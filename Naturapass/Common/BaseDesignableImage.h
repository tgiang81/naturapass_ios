//
//  BaseDesignableImage.h
//  Naturapass
//
//  Created by Giang on 8/16/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface BaseDesignableImage : UIImageView{
}

@property (assign, nonatomic) IBInspectable CGFloat borderWidth;
@property (assign, nonatomic) IBInspectable CGFloat cornerRadius;
@property (strong, nonatomic) IBInspectable UIColor *borderColor;
@property (assign, nonatomic) IBInspectable BOOL isRounded;
@end


