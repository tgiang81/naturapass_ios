//
//  CommonObj.m
//  Naturapass
//
//  Created by Giang on 9/23/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "CommonObj.h"

@implementation CommonObj

static CommonObj *sharedInstance = nil;


+ (CommonObj *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}


@end
