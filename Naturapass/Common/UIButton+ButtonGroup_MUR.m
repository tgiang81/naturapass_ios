//
//  UIButton+ButtonGroup_MUR.m
//  Naturapass
//
//  Created by Giang on 8/20/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "UIButton+ButtonGroup_MUR.h"

@implementation UIButton (ButtonGroup_MUR)

-(void)setImageState
{
    [self setImage: [UIImage imageNamed:@"checkbox_active"] forState:UIControlStateSelected];
    [self setImage: [UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];

}
@end
