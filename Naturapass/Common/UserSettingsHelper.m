//
//  UserSettingsHelper.m
//  Naturapass
//
//  Created by Admin on 3/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "UserSettingsHelper.h"

#define strFetchIntersticeTime                  @"FetchIntersticeTime"
#define strDisplayIntersticeTime                @"DisplayIntersticeTime"
#define strIntersticeJsonData                   @"IntersticeJsonData"

#define strGameFairTime                         @"GameFairTime"

@implementation UserSettingsHelper

#pragma mark - fetch interstice time

+(NSString*)getFetchIntersticeTime
{
	return [[NSUserDefaults standardUserDefaults] objectForKey:strFetchIntersticeTime];
}

+(void)setFetchIntersticeTime:(NSString*)time
{
	[[NSUserDefaults standardUserDefaults] setObject:time forKey:strFetchIntersticeTime];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - advertising display time

+(NSString*)getIntersticeDisplayTime
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:strDisplayIntersticeTime];
}

+(void)setIntersticeDisplayTime:(NSString*)time
{
    [[NSUserDefaults standardUserDefaults] setObject:time forKey:strDisplayIntersticeTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark - GAMEFAIR display time

+(NSNumber*)getGameFairDisplayTime
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:strGameFairTime];
}

+(void)setGameFairDisplayTime:(NSNumber*)time
{
    [[NSUserDefaults standardUserDefaults] setObject:time forKey:strGameFairTime];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - advertising display time

+(NSDictionary*)getIntersticeJsonData
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:strIntersticeJsonData];
}

+(void)setIntersticeJsonData:(NSDictionary*)dictionary
{
    [[NSUserDefaults standardUserDefaults] setObject:dictionary forKey:strIntersticeJsonData];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -

@end