//
//  MDRadioButton.h
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDRadioButton : UIButton
{
    UIView *view_normal;
    UIView *view_sub_normal;
    UIView *view_active;
    UIView *view_sub_active;
    
}
-(void)fnColorNormal:(UIColor*)colorNormal withColorActive:(UIColor*)colorActive;
-(void)fnColor:(UIColor*)colorNormal withColorBoderNormal:(UIColor*)colorBorderNormal withColorActive:(UIColor*)colorActive  withColorBoderActive:(UIColor*)colorBorderActive;
-(void)setTypeButton:(BOOL)isRadio;
@end
