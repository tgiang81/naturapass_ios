//
//  WebserviceServiceAPI.m
//  PDVLOreal
//
//  Created by William Antwi on 04/01/13.
//  Copyright (c) 2013 Appsolute. All rights reserved.
//

#import "WebServiceAPI.h"
#import "ErrorViewController.h"
#import "AppDelegate.h"
#import "OpenUDID.h"
#import "AppCommon.h"
#import "ServiceHelper.h"
#import "Define.h"
#import "AFNetworking.h"
#import "KSToastView.h"


@interface WebServiceAPI () <NSURLSessionDelegate, UIApplicationDelegate> {
}

@end

@implementation WebServiceAPI
@synthesize delegate;

#pragma mark - SharedInstance

-(id)init{
    if (self = [super init]){
        self.bNoNeedResponse = NO;
    }
    return self;
}

-(BOOL)fnCheckResponse:(NSDictionary*)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        if ([response[@"message"] isKindOfClass:[NSString class]]) {
            //            [KSToastView ks_showToast: response[@"message"] duration:2.0f completion: ^{
            //            }];
            return YES;
        }
    }
    return NO;
}

-(void) resetPHPSESSID
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PHPSESSID" ];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *) getPHPSESSID {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"PHPSESSID"];
}

-(NSString *) getPHPSESSID {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"PHPSESSID"];
}

- (NSString *) getSenderId {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
}

// FOR LOGIN

- (void) getRequest:(NSString *)url {
    NSURL *URL = [NSURL URLWithString:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:15];
    
    [self processingRequest: request];
}

- (void) getRequestForURL:(NSString *)url withTimeOut:(float) iTimeOut
{
    NSURL *URL = [NSURL URLWithString:url];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:iTimeOut];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    [self processingRequest: request];
}


- (void) postRequestForURL:(NSString *)urlString {
    NSURL *URL = [NSURL URLWithString:urlString];
    
    NSData *postData = [urlString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    
    NSString *postDataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    [request setURL:[NSURL URLWithString:urlString]];
    ASLog(@"urlString=%@",urlString);
    ASLog(@"sid = %@",[self getPHPSESSID]);
    
    [request setHTTPMethod:@"POST"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [self processingRequest: request];
}

- (void) putRequestTimeOut:(NSURL *) postUrl timeout:(CGFloat) time{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:time];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [self processingRequest: request];
}

- (void) putRequest:(NSURL *) putURL {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:putURL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    [self processingRequest: request];
}

- (void) putRequestWithoutCookie: (NSDictionary*) postDict {
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: [NSURL URLWithString:PUTFACEBOOKUSERACTION_API ]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    NSData *jsData = [NSJSONSerialization dataWithJSONObject:postDict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *dataMain = [[NSString alloc] initWithData:jsData encoding:NSUTF8StringEncoding];
    
    NSString *postDataLength;
    
    NSMutableData *postData = [NSMutableData data]; //[NSMutableData dataWithCapacity:[data length] + 512];
    
    [postData appendData:[NSData dataWithData:[dataMain dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]]];
    
    [request setHTTPBody:postData];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    
    [self processingRequest: request];
}
- (void) putRequestURL:(NSString*)url withParam:(NSDictionary*)param timeout:(CGFloat) time{
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:jsonString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:time];
    [request setURL:[NSURL URLWithString:url ]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"PHPSESSID"]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    
    [self processingRequest: request];
}

- (void) deleteRequest:(NSString *)strURL {
    NSURL *deleteURL = [[NSURL alloc] initWithString:strURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: deleteURL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MIN_TIMEOUT];
    
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [self processingRequest: request];
}


-(void)postSignalerRequest:(NSURL *)postUrl withParametersDic:(NSMutableDictionary *)dict{
    
    NSData *jsData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *dataMain = [[NSString alloc] initWithData:jsData encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MIN_TIMEOUT];
    
    NSString *postDataLength;
    
    NSMutableData *postData = [NSMutableData data]; //[NSMutableData dataWithCapacity:[data length] + 512];
    
    [postData appendData:[NSData dataWithData:[dataMain dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]]];
    
    postDataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    [self processingRequest: request];
}


-(void)postAvatarRequest:(NSURL *)postUrl withParametersDic:(NSDictionary *)postDict{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"}" withString:@""];
    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"," withString:@"& "];
    jsonString=[jsonString stringByReplacingOccurrencesOfString:@"{" withString:@""];
    jsonString=[jsonString stringByReplacingOccurrencesOfString:@":" withString:@"="];
    
    //ASLog(@"postDict = %@",jsonString);
    
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
}

- (void)sendRequestWithURL:(NSString *)url
                    params:(NSDictionary *)params
                attachment:(NSDictionary *)attachment{
    
    
    
    NSString *fileName = [attachment objectForKey:@"kFileName"];
    NSData *fileData = [attachment objectForKey:@"kFileData"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:MAX_TIMEOUT] ;//30 minutes timeout
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
    
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"publication[media][file]\"; filename=\"%@\"\r\n", fileName]
                      dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[NSData dataWithData:fileData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    [self processingRequest: request];
}


- (void)sendGroupRequestWithURL:(NSString *)url
                         params:(NSDictionary *)params
                     attachment:(NSDictionary *)attachment{
    
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                           timeoutInterval:MEDIUM_TIMEOUT] ;
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
        
        NSMutableData *body = [NSMutableData data];
        
        for (NSString *param in params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        if (attachment != NULL) {
        
            NSString *fileName = [attachment objectForKey:@"kFileName"];
            NSData *fileData = [attachment objectForKey:@"kFileData"];
        
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"group[photo][file]\"; filename=\"%@\"\r\n", fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
            [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        
            if([fileData length])
            [body appendData:[NSData dataWithData:fileData]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }

        [request setHTTPBody:body];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        
        [self processingRequest: request];
}

- (void)postProfileRequestWithURL:(NSString *)url
                           params:(NSDictionary *)params
                       attachment:(NSDictionary *)attachment{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MEDIUM_TIMEOUT] ;
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    //
    if (attachment != NULL) {
        if (attachment[@"photo"]) {
            NSString *fileName = [attachment[@"photo"] objectForKey:@"kFileName"];
            NSString *strname = [attachment[@"photo"] objectForKey:@"name"];
            
            NSData *fileData = [attachment[@"photo"] objectForKey:@"kFileData"];
            
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",strname, fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
            
            if([fileData length])
                [body appendData:[NSData dataWithData:fileData]];
            
        }
        if (attachment[@"media"]) {
            NSArray *arrMedia =attachment[@"media"];
            for (int i =0 ; i<arrMedia.count; i++) {
                
                NSDictionary *dic = arrMedia[i];
                NSString *fileName = dic[@"kFileName"];
                int count = 1000+i;
                
                NSString *strName = [ dic[@"name"] stringByReplacingOccurrencesOfString:@"count"
                                                                             withString:[NSString stringWithFormat:@"%d",count]];
                
                NSData *fileData = dic[@"kFileData"];
                
                [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",strName,fileName]
                                  dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                
                [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
                
                if([fileData length])
                    [body appendData:[NSData dataWithData:fileData]];
                
            }
            
        }
    }
    ///
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    [self processingRequest: request];
}

- (void)postAgendaRequestWithURL:(NSString *)url params:(NSDictionary *)putDic{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:putDic options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //ASLog(@"postDict = %@",jsonString);
    
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MIN_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody:postData];
    [self processingRequest: request];
}


- (void)sendLougeRequestWithURL:(NSString *)url
                         params:(NSMutableArray *)params
                     attachment:(NSDictionary *)attachment{
    NSString *fileName = [attachment objectForKey:@"kFileName"];
    NSData *fileData = [attachment objectForKey:@"kFileData"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:MEDIUM_TIMEOUT] ;//30 minutes timeout
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    //    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSDictionary *param in params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param[@"path"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", param[@"value"]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    //ASLog(@"PostDict=%@",params);
    if (fileData) {
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"lounge[photo][file]\"; filename=\"%@\"\r\n", fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        [body appendData:[NSData dataWithData:fileData]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self processingRequest: request];
}


- (void)sendRegistrationRequestWithURL:(NSString *)url
                                params:(NSDictionary *)params
                            attachment:(NSDictionary *)attachment{
    
    
    NSString *fileName = [attachment objectForKey:@"kFileName"];
    NSData *fileData = [attachment objectForKey:@"kFileData"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MEDIUM_TIMEOUT] ;
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    /*user[photo][file]*/
    if (fileData) {
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user[photo][file]\"; filename=\"%@\"\r\n", fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        
        [body appendData:[NSData dataWithData:fileData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [request setHTTPBody:body];
    
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    
    [self processingRequest: request];
}


- (void)putPersonalInformationRequestWithURL:(NSURL *)url
                                  attachment:(NSDictionary *)attachment{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MEDIUM_TIMEOUT] ;
    
    if (attachment != NULL) {
        
        NSString *fileName = [attachment objectForKey:@"kFileName"];
        NSData *fileData = [attachment objectForKey:@"kFileData"];
        
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
        
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"user[photo]\"; filename=\"%@\"\r\n", fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        
        if([fileData length])
            [body appendData:[NSData dataWithData:fileData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        [self processingRequest: request];
        
    }
}

-(void) postRequestUploadPhoto: (NSString *)url attachment:(NSDictionary *)attachment forGroup:(BOOL) isGroup

{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MAX_TIMEOUT] ;
    
    if (attachment != NULL) {
        
        NSString *fileName = [attachment objectForKey:@"kFileName"];
        NSData *fileData = [attachment objectForKey:@"kFileData"];
        
        
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
        
        NSMutableData *body = [NSMutableData data];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        if (isGroup) {
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"group[photo]\"; filename=\"%@\"\r\n", fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }else{
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"lounge[photo]\"; filename=\"%@\"\r\n", fileName]
                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        
        if([fileData length])
            [body appendData:[NSData dataWithData:fileData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request setHTTPBody:body];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
        
        [self processingRequest: request];
    }
    
}

- (void)putPersonalInformation:(NSURL *)url
                        params:(NSDictionary *)params
                    attachment:(NSDictionary *)attachment{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MIN_TIMEOUT];
    [request setHTTPMethod:@"PUT"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *mKey in params.allKeys) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", mKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params valueForKey:mKey] ] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [self processingRequest: request];
}

- (void)postPersonalInformation:(NSURL *)url
                         params:(NSDictionary *)params{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MIN_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *mKey in params.allKeys) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", mKey] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params valueForKey:mKey] ] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
}

- (void) postRequestData:(NSURL *)postUrl withParametersDic:(NSDictionary *)dict withMedia:(NSData *)mediaData {
    
    NSData *jsData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    
    NSString *dataMain = [[NSString alloc] initWithData:jsData  encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    NSString *postDataLength;
    
    NSMutableData *postData = [NSMutableData data]; //[NSMutableData dataWithCapacity:[data length] + 512];
    
    [postData appendData:[NSData dataWithData:[dataMain dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]]];
    
    postDataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    
    [self processingRequest: request];
}

- (void) putRequestPublication:(NSURL *) putURL withParametersDic:(NSDictionary *)postDict {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    ASLog(@"postDict =%@",jsonString);
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:putURL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
}

- (void) putRequestLounge:(NSURL *) putURL withParametersDic:(NSDictionary *)postDict {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:putURL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
    
}

- (void)putUpdateGroup:(NSURL *)url
                params:(NSDictionary *)params{
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //ASLog(@"postDict = %@",jsonString);
    
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
    
}

-(void)postRequest:(NSURL *)postUrl withParametersDic:(NSMutableDictionary *)postDict{
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //ASLog(@"postDict = %@",jsonString);
    
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
}

- (void) putRequestLoungeAPI:(NSURL *) putUrl withParametersDic:(NSDictionary *)putDic attachment:(NSDictionary *)attachment{
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:putDic options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    //ASLog(@"postDict = %@",jsonString);
    
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:putUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
}

- (void) putRequestPublicPrivate:(NSString *) putURL withParametersDic:(NSDictionary *)postDict {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:postDict options:0 error:NULL];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //ASLog(@"postDict =%@",jsonString);
    NSData *postData = [jsonString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:jsonString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    [request setURL:[NSURL URLWithString:putURL]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request addValue: @"application/json; charset=utf-8" forHTTPHeaderField: @"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    [request setHTTPBody:postData];
    
    [self processingRequest: request];
}

- (void) postPublicationRequestData:(NSURL *)postUrl withParametersDic:(NSDictionary *)dict {
    
    NSData *jsData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSString *dataMain = [[NSString alloc] initWithData:jsData                                                                                 encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:postUrl
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:MEDIUM_TIMEOUT];
    NSString *postDataLength;
    
    NSMutableData *postData = [NSMutableData data]; //[NSMutableData dataWithCapacity:[data length] + 512];
    
    [postData appendData:[NSData dataWithData:[dataMain dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]]];
    
    postDataLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postDataLength forHTTPHeaderField:@"Content-Length"];
    [self processingRequest: request];
}

#pragma mark - GET

-(void) fmGetEmailStatusFor :(GROUP_TYPE) type{
//type: Group/Agenda
    [self getRequestForURL:GET_EMAIL_PARAMETERS withTimeOut:MEDIUM_TIMEOUT];

}

-(void) fmGetPushNotificationStatusFor :(GROUP_TYPE) type{
    [self getRequestForURL:GET_EMAIL_PARAMETERS withTimeOut:MEDIUM_TIMEOUT];

}

-(void) fmSetEmailStatusFor :(GROUP_TYPE) type withValue:(NSDictionary*) param{
    if (type == TYPE_GROUP) {
        [self putRequest:[NSURL URLWithString: PUT_EMAIL_NOTI(@"group", param[@"value"]) ]];

    }else{
        [self putRequest:[NSURL URLWithString: PUT_EMAIL_NOTI(@"hunt", param[@"value"]) ]];
    }

}

-(void) fmSetPushNotificationStatusFor :(GROUP_TYPE) type withValue:(NSDictionary*) param{

    if (type == TYPE_GROUP) {
        [self putRequest:[NSURL URLWithString: PUT_NOTIFICATION_NOTI(@"group", param[@"value"]) ]];
        
    }else{
        [self putRequest:[NSURL URLWithString: PUT_NOTIFICATION_NOTI(@"hunt", param[@"value"]) ]];
    }
}

//
-(void) getUserFriendsAction{
    [self getRequestForURL:GETUSERFRIENDSACTION_API([self getSenderId]) withTimeOut:MEDIUM_TIMEOUT];
}


- (void) postUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDic{
    [self sendRegistrationRequestWithURL:REGISTER_API params:postDict attachment:imageDic];
}

-(void) fnDELETE_MESSAGE_GROUP_CHAT:(NSString*)strMsg
{
    [self deleteRequest:DELETE_MESSAGE_GROUP_CHAT(strMsg)];
}

-(void) fnDELETE_MESSAGE_HUNT_CHAT:(NSString*)strMsg
{
    [self deleteRequest:DELETE_MESSAGE_HUNT_CHAT(strMsg)];
}

#pragma mark - Hunt CITY + COUNTRY

-(void) fnGET_HUNT_LIKED
{
    [self getRequestForURL:GET_HUNT_LIKED withTimeOut:MEDIUM_TIMEOUT];
}

- (void) fnADD_HUNT_LIKED:(NSString *)huntTypeID{
    [self postRequestForURL:ADD_HUNT_LIKED(huntTypeID)];
}

- (void) fnDEL_HUNT_LIKED:(NSString *)huntTypeID{
    [self deleteRequest:DEL_HUNT_LIKED(huntTypeID)];
    
}

-(void) fnGET_HUNT_PRACTICE
{
    [self getRequestForURL:GET_HUNT_PRACTICE withTimeOut:MEDIUM_TIMEOUT];
}

- (void) fnADD_HUNT_PRACTICE:(NSString *)huntTypeID{
    [self postRequestForURL:ADD_HUNT_PRACTICE(huntTypeID)];
}

- (void) fnDEL_HUNT_PRACTICE:(NSString *)huntTypeID{
    [self deleteRequest:DEL_HUNT_PRACTICE(huntTypeID)];
}

-(void) fnGET_HUNT_TYPES:(NSDictionary*)param
{
    [self getRequestForURL:GET_HUNT_TYPES(param[@"limit"],param[@"offset"]) withTimeOut:MIN_TIMEOUT];
}

/**/
-(void) fnGET_HUNT_LIST_COUNTRIES:(NSDictionary*)param
{
    [self getRequestForURL:GET_HUNT_LIST_COUNTRIES(param[@"limit"],param[@"offset"],param[@"filter"]) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_HUNT_COUNTRY_LOCATION
{
    [self getRequestForURL:GET_HUNT_COUNTRY_LOCATION withTimeOut:MEDIUM_TIMEOUT];
}

- (void) fnADD_HUNT_COUNTRY_LOCATION:(NSString *)countryID{
    [self postRequestForURL:ADD_HUNT_COUNTRY_LOCATION(countryID)];
}

- (void) fnDEL_HUNT_COUNTRY_LOCATION:(NSString *)countryID{
    [self deleteRequest:DEL_HUNT_COUNTRY_LOCATION(countryID)];
    
}


-(void) fnGET_CHECK_LINKTO_RECEIVER
{
    [self getRequestForURL:GET_CHECK_LINKTO_RECEIVER withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_HUNT_INFORMATION: (NSString*)huntID
{
    [self getRequestForURL:GET_HUNT_INFORMATION(huntID) withTimeOut:MEDIUM_TIMEOUT];
}
/**/

-(void) fnGET_HUNT_LIST_CITIES:(NSDictionary*)param
{
    [self getRequestForURL:GET_HUNT_LIST_CITIES(param[@"limit"],param[@"offset"],param[@"filter"]) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_HUNT_CITY_LOCATION
{
    [self getRequestForURL:GET_HUNT_CITY_LOCATION withTimeOut:MEDIUM_TIMEOUT];
}

- (void) fnADD_HUNT_CITY_LOCATION:(NSString *)cityID{
    [self postRequestForURL:ADD_HUNT_CITY_LOCATION(cityID)];
}

- (void) fnDEL_HUNT_CITY_LOCATION:(NSString *)cityID{
    [self deleteRequest:DEL_HUNT_CITY_LOCATION(cityID)];
    
}
/**/
#pragma mark - getPublication Action

- (void) getPublicationsAction:(NSString *)strLimit forSharing:(NSString *)strShareType withOffset:(NSString *)strOffset withGroups:(NSString*) strGroupAppend{
    NSString *str =@"";
    if (strShareType != nil) {
        str =[NSString stringWithFormat:@"&sharing=%@",strShareType];
    }
    
    [self getRequestForURL:GETPUBLICATION(strLimit,str,strOffset, strGroupAppend) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getPublicationsPrivite:(NSString *)strLimit withOffset:(NSString *)strOffset withFilter:(NSString*) strFilter{
    [self getRequestForURL:GET_PUBLICATION_PRIVATE(strLimit,strOffset,strFilter) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getPublicationsPublic:(NSString *)strLimit withOffset:(NSString *)strOffset withFilter:(NSString*) strFilter{
    [self getRequestForURL:GET_PUBLICATION_PUBLIC(strLimit,strOffset,strFilter) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getPublicationsActionItem:(NSString *)publication_id {
    [self getRequestForURL:GETPUBLICATIONITEM(publication_id) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getPublicationsUserAction:(NSString *)strLimit forUserId:(NSString *)strUserId withOffset:(NSString *)strOffset {
    [self getRequestForURL:GETPUBLICATIONUSER(strUserId, strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getPublicationCommentsAction:(NSString *)publication_id {
    [self getRequestForURL:GETCOMMENT_API(publication_id) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getLoungeMessageAction:(NSString *)_strSearch limit:(NSString*)limit loaded:(NSString*)loaded {
    [self getRequestForURL:GETLOUNGEMESSAGEACTION(_strSearch, limit, loaded) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getLoungeMessageOffset:(NSString *)_strSearch limit:(NSString*)limit offset:(NSString*)offset{
    [self getRequestForURL:GETLOUNGEMESSAGEOFFSET(_strSearch,limit,offset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getLoungeSuscriberGeolocationAction:(NSString *)lounge_id {
    [self getRequestForURL:GETLOUNGESUBSCRIBERGEOLOCATION_API(lounge_id, [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"]) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getUserGroupsAction {
    [self getRequestForURL:GETUSERGROUPSACTION_API([self getSenderId]) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getUserAction {
    [self getRequestForURL:GETUSERACTION_API([self getSenderId]) withTimeOut:MIN_TIMEOUT];
}
- (void) getFriendAction:(NSString *)friendId {
    [self getRequestForURL:GETUSERACTION_API(friendId) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getUserParam{
    [self getRequestForURL:GET_USER_PARAM withTimeOut:MIN_TIMEOUT];
}
- (void) getGroupAction:(NSString*) group_id {
    [self getRequestForURL:GET_GROUP_ACTION(group_id) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getUserParameterAction {
    [self getRequestForURL:GETUSERPARAMETERACTION_API([self getSenderId]) withTimeOut:MIN_TIMEOUT];
}

- (void) getLoungeAskAction:(NSString *)_strID{
    [self getRequestForURL:GETLOUNGEASKACTION_API(_strID) withTimeOut:MIN_TIMEOUT];
}

- (void) getUserNotificationAction {
    [self getRequestForURL:GETUSERNOTIFICATIONACTION_API withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getNotificationStatusWithLimit:(NSString*)strLimit offset:(NSString*)strOffset {
    [self getRequestForURL:GET_USER_NOTIFICATION_STATUS_API(strLimit,strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getUserWaitingAction{
    [self getRequestForURL:GETUSERWAITINGACTION_API withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getPushNotificationPublicationAction:(NSString *)object_id {
    [self getRequestForURL:GETPUSHNOTIFICATIONPUBLICATIONID(object_id) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getFacebookUserAction:(NSString *)facebook_ID{
    [self getRequest:GETFACEBOOKUSERACTION_API(facebook_ID)];
    
}

-(void)putUserFriendshipConfirmAction:(NSString *)strID{
    [self putRequest:[NSURL URLWithString:USERFRIENDSHIPCONFIRMACTION_API(strID)]];
}

-(void)delUserFriendshipConfirmAction:(NSString *)strID{
    [self deleteRequest: USERFRIENDSHIPCONFIRMACTION_API(strID)];
}


- (void) putUpdateFacebookUserInfo:(NSDictionary *)myDic  {
    //ASLog(@"Publication Id = %@",publication_id);
    [self putRequestWithoutCookie:myDic];
}

- (void) putPublicationLikeAction:(NSString *)publication_id  {
    //ASLog(@"Publication Id = %@",publication_id);
    [self putRequest:[NSURL URLWithString:LIKE_API(publication_id)]];
}

- (void) putPublicationUnLikeAction:(NSString *)publication_id  {
    //ASLog(@"Publication Id = %@",publication_id);
    [self putRequest:[NSURL URLWithString:UNLIKE_API(publication_id)]];
}

- (void) putPublicationCommentLikeAction:(NSString *)comment_id {
    [self putRequest:[NSURL URLWithString:PUTCOMMENTLIKE_API(comment_id)]];
}

- (void) putPublicationCommentUnLikeAction:(NSString *)comment_id {
    [self putRequest:[NSURL URLWithString:PUTCOMMENTUNLIKE_API(comment_id)]];
}

- (void) putLoungessuscriberParticipateAction:(NSString *)loungesid user_id:(NSString*)user_id withParametersDic:(NSDictionary *)postDict{
    [self putRequestLounge:[NSURL URLWithString:PUTLOUNGESUSCRIBERPARTICIPATIONACTION(loungesid, user_id)]withParametersDic:postDict];
}

- (void) putLoungeGeolocationAction:(NSString *)loungesid  location:(NSString *)userid withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestLounge:[NSURL URLWithString:PUTLOUNGEGEOLOCATION_API(loungesid, userid)] withParametersDic:postDict];
    //ASLog(@"url: %@",[NSURL URLWithString:PUTLOUNGEGEOLOCATION_API(loungesid, userid)] );
}
- (void) putLoungeSuscriberQuietAction:(NSString *)loungesid withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestLounge:[NSURL URLWithString:PUTLOUNGESUSCRIBERQUIETACTION(loungesid, [self getSenderId])] withParametersDic:postDict];
}
- (void) putLoungeSuscriberAdminAction:(NSString *)loungesid suscriber_id:(NSString*)suscriber_id{
    //    ASLog(@"%@",PUT_API(loungesid,[self getSenderId]));
    [self putRequest:[NSURL URLWithString:PUTLOUNGESUSCRIBERADMIN_API(loungesid, suscriber_id)]];
}
- (void) putLoungeSuscriberGeolocationAction:(NSString *)loungesid withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestLounge:[NSURL URLWithString:PUTLOUNGESUSCRIBERGEOLOCATIONACTION(loungesid,[self getSenderId])] withParametersDic:postDict];
}

- (void) postUserProfilePictureActionwithAttachmentImage:(NSDictionary *) imageDict {
    [self putPersonalInformationRequestWithURL:[NSURL URLWithString:POSTUSERPROFILEPICTURE] attachment:imageDict];
}
- (void) putUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDict {
    [self putPersonalInformation:[NSURL URLWithString:PUTUSERACTION_API] params:postDict attachment:imageDict];
}
- (void) postChangePasswordAction:(NSDictionary *)postDict {
    //    [self postPersonalInformation:[NSURL URLWithString:PUT_CHANGE_PASSWORD_API] params:postDict];
    [self postRequestData:[NSURL URLWithString:PUT_CHANGE_PASSWORD_API] withParametersDic:postDict withMedia:NULL];
}

- (void) putUserParameterAction:(NSString *)loungesid withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestLounge:[NSURL URLWithString:PUTUSERPARAMETERACTION_API] withParametersDic:postDict];
}
- (void) putLoungegeolocationActionAdmin:(NSString *)loungesid geolocations:(NSString *)stradminGeolocation {
    [self putRequest:[NSURL URLWithString:PUTLOUNGEGEOLOCATIONACTION_ADMIN_API(loungesid, stradminGeolocation)]];
}
- (void) putLoungeAction:(NSString *)lounge_id withParametersDic:(NSDictionary *)params imageattachment:(NSDictionary *)attachment {
    [self putRequestLoungeAPI:[NSURL URLWithString:PUTLOUNGEACTION_API(lounge_id)] withParametersDic:params attachment:attachment];
}

//https://www.naturapass.com/api/v1/lounges/313/notmembers/120/publiccomment
- (void) putLoungesuscriberPubliccomment_Nonmembers:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict {
    [self putRequestPublicPrivate:PUTLOUNGE_SUSCRIBER_PUBLICCOMMENT_NONMEMBER(lounge_id,strID) withParametersDic:postDict];
}

- (void) putLoungesuscriberPubliccommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestPublicPrivate:PUTLOUNGESUSCRIBERPUBLICCOMMENTACTION_API(lounge_id,strID) withParametersDic:postDict];
}
- (void) putLoungesuscriberParticipationCommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict {
    [self putRequestPublicPrivate:PUT_LOUNGE_SUSCRIBER_PARTICIPATIONCOMMENT_API(lounge_id,strID) withParametersDic:postDict];
}

- (void) putLoungesuscriberNonmembersParticipion:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSDictionary *)postDict {
    [self putRequestPublicPrivate:PUTLOUNGE_SUSCRIBER_NONMEMBERS_PARTICIPATION(lounge_id,strID) withParametersDic:postDict];
}

- (void) putLoungesuscriberPrivatecommentAction:(NSString *)lounge_id strID:(NSString *)strID withParametersDic:(NSMutableDictionary *)postDict {
    [self putRequestPublicPrivate:PUTLOUNGESUSCRIBERPRIVATECOMMENTACTION_API(lounge_id,strID) withParametersDic:postDict];
}

- (void) putUserReadNotificationAction:(NSString *)notification_id  {
    [self putRequest:[NSURL URLWithString:PUTUSERREADNOTIFICATIONACTION_API(notification_id)]];
}
- (void) putReadAllNotification  {
    [self putRequest:[NSURL URLWithString:PUT_USER_READ_ALL_NOTIFICATION]];
}

- (void) putUserResetPasswordAction:(NSDictionary *)postDict{
    [self putRequestLounge:[NSURL URLWithString:PUTUSERRESETPASSWORDACTION_API] withParametersDic:postDict ];
}
- (void) putUpdateGroupItem:(NSDictionary *)postDict group_id:(NSString*)group_id {
    [self putUpdateGroup:[NSURL URLWithString:PUT_GROUP_UPDATE_ITEM(group_id)] params:postDict];}

-(void) putMailable:(NSString*) group_id mailable:(int) is_mailable  mykind:(NSString *)mykind
{
    [self putRequest:[NSURL URLWithString:GROUP_MAILABLE(mykind,group_id, is_mailable)]];
}

#pragma mark - deletePublication Action

- (void) deletePublicationCommentActionAction:(NSString *)comment_id{
    [self deleteRequest:DELETECOMMENT_API(comment_id)];
}

- (void) deletePublicationActionAction:(NSString *)publication_id{
    [self deleteRequest:DELETEPUBLICATION_API(publication_id)];
}
- (void) deleteCommentPublicationAction:(NSString *)comment_id{
    [self deleteRequest:DELETE_COMMENT_PUBLICATION_API(comment_id)];
}

- (void) deletePublicationAction:(NSString *)publication_id {
    [self deleteRequest:DELETEPUBLICATIONACTION_API(publication_id)];
    
}

- (void) deleteLoungeAction:(NSString *)lounge_id {
    [self deleteRequest:DELETELOUNGEACTION(lounge_id)];
    
}
- (void) deleteLoungeJoinAction:(NSString *)lounge_id {
    [self deleteRequest:DELETELOUNGEJOINACTION([self getSenderId],lounge_id)];
    
}
- (void) deleteUserDeviceAction:(NSString *)type identifier:(NSString *)_identifier {
    [self deleteRequest:DELETEUSERDEVICEACTION(type, _identifier)];
    
}

#pragma mark - postLouge Action

-(void)postLoungePhotoAction:(NSMutableArray *)dicPhoto withAttachmentMediaDic:(NSDictionary *) mediaDic {
    [self sendLougeRequestWithURL:POSTLOUNGEACTION_API params:dicPhoto attachment:mediaDic];
}

- (void) postLoungeInvitationByEmailWithLounge: (NSString *) LoungeID subject:(NSString*)subject toEmails: (NSString *) emailsString withContent: (NSString *) emailContent
{
    
    NSDictionary *emailDictionary = @{@"to" : emailsString,
                                      @"subject": @"Invitation",
                                      @"body" : emailContent};
    NSDictionary *postDictionary = emailDictionary;
    [self postRequestData:[NSURL URLWithString:POSTLOUNGEINVITEEMAILACTION(LoungeID)] withParametersDic:postDictionary withMedia:NULL];
}
- (void) postGroupInvitationByEmailWithGroupID: (NSString *) groupID subject:(NSString*)subject toEmails: (NSString *) emailsString withContent: (NSString *) emailContent
{
    
    NSDictionary *emailDictionary = @{@"to" : emailsString,
                                      @"subject": @"Invitation",
                                      @"body" : emailContent};
    NSDictionary *postDictionary = emailDictionary;
    [self postRequestData:[NSURL URLWithString:GROUP_INVITE_BY_EMAIL(groupID)] withParametersDic:postDictionary withMedia:NULL];
}
#pragma mark - postPublication Action

- (void) postPublicationCommentAction:(NSString *) publication_id withParametersDic:(NSDictionary *)postDict {
    [self postRequestData:[NSURL URLWithString:POSTCOMMENT_API(publication_id)] withParametersDic:postDict withMedia:NULL];
}


-(void) postMessageData:(NSDictionary *)postDict withMedia:(NSDictionary *) mediaDic{
    [self postPublicationRequestData:[NSURL URLWithString:POST_PUBLICATION_MUR] withParametersDic:postDict];
}

- (void) postLoungeJoinAction:(NSString *)postID{
    [self postRequestForURL:POSTLOUNGEJOINACTION_API([self getSenderId], postID)];
}

- (void) postLoungeMessageAction:(NSString *)strloungeID withParametersDic:(NSDictionary *)postDict {
    [self postRequest:[NSURL URLWithString:POSTLOUNGESMESSAGEACTION_API(strloungeID)] withParametersDic:postDict];
}

- (void) postLoungeInviteGroupAction:(NSString *)strlounge_id group:(NSString *)strgroup_id withParametersDic:(NSDictionary *)postDict{
    [self postRequestData:[NSURL URLWithString:POSTLOUNGEINVITEGROUPACTION_API(strlounge_id, strgroup_id)] withParametersDic:postDict withMedia:NULL];
    
}
- (void) postLoungeInviteGroup:(NSString *)strlounge_id group:(NSString *)strgroup_id{
    [self postRequestForURL:POSTLOUNGEINVITEGROUPACTION_API(strlounge_id, strgroup_id)];
    
}

- (void) postUserGeolocationAction:(NSString *)struserID withParametersDic:(NSDictionary *)postDict {
    [self postRequest:[NSURL URLWithString:POSTUSERGEOLOCATIONACTION_API([self getSenderId])] withParametersDic:postDict];
}
- (void) postUpdateUserGeolocationAction:(NSDictionary *)postDict
{
    [self postRequest:[NSURL URLWithString:POSTUPDATEUSERGEOLOCACATION_API] withParametersDic:postDict];
}


- (void) postUserFriendShipAction:(NSString *)struserID {
    [self postRequestForURL:POST_USER_FRIENDSHIP_API(struserID)];
}

- (void) postAvatarAction:(NSDictionary *)postDict {
    [self postAvatarRequest:[NSURL URLWithString:REGISTER_API] withParametersDic:postDict];
    
}
- (void) postPublicationSignalAction:(NSString *)post_ID withParametersDic:(NSDictionary *)postDict{
    [self postSignalerRequest:[NSURL URLWithString:POSTPUBLICATIONSIGNALACTION_API(post_ID)] withParametersDic:postDict];
    
}
-(void) postUserFriendsAction:(NSString *)strLounge_id andFriend:(NSString*)friendID{
    [self postRequestForURL:POSTUSERFRIENDSHIP_API(strLounge_id, friendID)];
}
-(void) postUserDeviceAction:(NSString *)strType identifier:(NSString *)strIdentifier{
    [self postRequestForURL:POSTUSERDEVICEACTION(strType, strIdentifier)];
}
- (void) postFacebookUserAction:(NSDictionary *)postDict withAttachmentImage:(NSDictionary *) imageDic{
    [self sendRegistrationRequestWithURL:POSTFACEBOOKUSERACTION_API params:postDict attachment:imageDic];
}
- (void) putFacebookEmailAction:(NSDictionary *)postDict{
    [self putRequestPublicPrivate:PUTFACEBOOKUSERACTION_API withParametersDic:postDict];
    
}


-(void) getLoungeSubscribeAction:(NSString *)lounge_id {
    [self getRequestForURL:GETLOUNGESUBSCRIBER_API(lounge_id) withTimeOut:MEDIUM_TIMEOUT];
    
}
//manhld
-(void) getLoungeSubscribeFriendAction:(NSString *)lounge_id {
    [self getRequestForURL:GETLOUNGESUBSCRIBERFRIEND_API(lounge_id) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getLoungesAction:(NSString *)strLimit withOffset:(NSString *)strOffset andQuery:(NSString*)query{
    [self getRequestForURL:LOUNGES_API(strLimit, strOffset, query) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getLoungesSearch:(NSString *)search limit:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:LOUNGESEARCH_LIMIT_API(search, strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getSearchAction:(NSString *)_strSearch kind:(NSString*)mykind limit:(NSString*)mylimit offset:(NSString*)myoffset{
    NSString *escapedString = [_strSearch stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    
    [self getRequestForURL:GET_SEARCH_API(mykind,escapedString,mylimit,myoffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getUserLoungesAction:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:GETUSERLOUNGESACTION_API(strLimit, strOffset) withTimeOut:MIN_TIMEOUT];
}

//#define GET_MUR_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,ishareing,strGroups,strHunts)
- (void) getMediaMapAction:(NSDictionary*)dicMap sharings:(NSString *)strSharing  groups:(NSString *)strGroup {
    
    [self getRequestForURL:GET_MUR_MAP(dicMap[@"southWestLatitude"],
                                       dicMap[@"southWestLongitude"],
                                       dicMap[@"northEastLatitude"],
                                       dicMap[@"northEastLongitude"],
                                       strSharing,strGroup ) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getUsersSearchAction:(NSString *)_strSearch {
    NSString *escapedString = [_strSearch stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    [self getRequestForURL:GETUSERSEACHACTION_API(escapedString) withTimeOut:MIN_TIMEOUT];
}

- (void) getLounge:(NSString *)strLounge_id {
    [self getRequestForURL:GETLOUNGE_API(strLounge_id) withTimeOut:MIN_TIMEOUT];
}

- (void) getTheFriends:(BOOL) isFriend withID:(NSString*)strID
{
    if (strID) {
        //friends of someone
        [self getRequestForURL:GET_MY_FRIENDS(strID, !isFriend) withTimeOut:MEDIUM_TIMEOUT];
        
    }else{
        //ME
        [self getRequestForURL:GET_MY_FRIENDS([self getSenderId], !isFriend) withTimeOut:MEDIUM_TIMEOUT];
    }
    
}

- (void) postLoungePhoto:(NSString *)loungeID withData:(NSDictionary*)data {
    
    [self postRequestUploadPhoto:POST_LOUNGES_PHOTO(loungeID) attachment:data forGroup: NO];
}

-(void)fnPOST_JOIN_GROUP_TO_HUNT:(NSDictionary*) param loungeID:(NSString*)loungeid
{
    [self postRequestData:[NSURL URLWithString:POST_JOIN_GROUP_TO_HUNT(loungeid)] withParametersDic:param withMedia:NULL];
    
}

#pragma mark - Groups Action

-(void)fnPOST_JOIN_USER_GROUP:(NSDictionary*) param groupID:(NSString*)groupid
{
    [self postRequestData:[NSURL URLWithString:POST_JOIN_USER_GROUP(groupid)] withParametersDic:param withMedia:NULL];
    
}

-(void)fnPOST_JOIN_AGENDA_GROUP_TO_HUNT:(NSDictionary*) param loungeID:(NSString*)loungeid
{
    [self postRequestData:[NSURL URLWithString:POST_JOIN_AGENDA_GROUP_TO_HUNT(loungeid)] withParametersDic:param withMedia:NULL];
    
}

-(void)fnPUT_PUBLICATION_EDIT:(NSDictionary*) param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_PUBLICATION_EDIT( param[@"publication_id"])] withParametersDic:param[@"value"] ];
}
-(void)fnPUT_UPDATE_PUBLICATION:(NSDictionary*) param withType:(NSString*)type
{
    [self putRequestPublication:[NSURL URLWithString:PUT_UPDATE_PUBLICATION( param[@"publication_id"],type)] withParametersDic:param[@"value"] ];
}
-(void)fnPUT_UPDATE_PUBLICATION_ALL:(NSDictionary*) param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_UPDATE_PUBLICATION_ALL( param[@"publication_id"])] withParametersDic:param[@"value"] ];
}


//* get Sqlite of all points in map of the current user
//1433:      *
//1434:      * PUT /v2/publication/sqlite/refresh
//1435:      *
//1436:      * {
//1437:      *  "publications":"1,2,3,4,5",
//1438:      *  "updated":"1450860029"
//1439:      * }
-(void) fnPUT_HUNT_allowaddshow_Publication_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_HUNT_allowaddshow_Publication_EDIT(hunt_id)] withParametersDic:param];
    
}

-(void) fnPUT_HUNT_allowaddshow_Chat_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_HUNT_allowaddshow_Chat_EDIT(hunt_id)] withParametersDic:param];
    
}


/*GROUP*/

-(void) fnPUT_GROUP_allowaddshow_Publication_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_GROUP_allowaddshow_Publication_EDIT(hunt_id)] withParametersDic:param];
    
}

-(void) fnPUT_GROUP_allowaddshow_Chat_EDIT:(NSString*)hunt_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_GROUP_allowaddshow_Chat_EDIT(hunt_id)] withParametersDic:param];
    
}


/**/

-(void) fnPUT_GROUP_allow_add_EDIT:(NSString*)group_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_GROUP_allow_add_EDIT(group_id)] withParametersDic:param];
    
}

-(void) fnPUT_GROUP_allow_show_EDIT:(NSString*)group_id withParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_GROUP_allow_show_EDIT(group_id)] withParametersDic:param];
    
}

-(void) fnPUT_SQLITE_GET_ALL_PUBLICATION:(NSDictionary*) param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_SQLITE_GET_ALL_PUBLICATION] withParametersDic:param ];
    
}

-(void)fnPUT_OBSERVATION_EDIT:(NSString*)observationID param:(NSDictionary*) param
{
    [self putRequestPublication:[NSURL URLWithString:PUT_OBSERVATION_EDIT(observationID)] withParametersDic:param];
}


- (void) fnPOST_MEDIA_EDIT:(NSString *)object_id withAttachmentMediaDic:(NSDictionary *) mediaDic {
    [self sendRequestWithURL:POST_MEDIA_EDIT(object_id) params:NULL attachment:mediaDic];
}
-(void) fnDELETE_GROUP_INVITATION:(NSDictionary*) param
{
    [self deleteRequest:DELETE_GROUP_INVITATION([self getSenderId], param[@"groupid"])];
    
}

-(void) fnDELETE_HUNT_INVITATION:(NSDictionary*) param
{
    [self deleteRequest:DELETE_HUNT_INVITATION([self getSenderId], param[@"huntid"])];
    
}

-(void)fnPOST_JOIN_USER_LOUNGE:(NSDictionary*) param loungeID:(NSString*)loungeid
{
    [self postRequestData:[NSURL URLWithString:POST_JOIN_USER_LOUNGE(loungeid)] withParametersDic:param withMedia:NULL];
    
}
- (void) getMyGroupsAction:(NSString *)strLimit forFilter:(NSString *)strFilter withOffset:(NSString *)strOffset {
    [self getRequestForURL:GET_MY_GROUPSACTION_API(strFilter, strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

-(void)postGroupAction:(NSDictionary *)postDict withAttachmentMediaDic:(NSDictionary *) mediaDic {
//    [self sendGroupRequestWithURL:POST_CREATE_GROUP_API params:postDict attachment:mediaDic];
    
    //Support Notification Parameters... enable new default Notification Push/Email.
    [self sendGroupRequestWithURL:POST_CREATE_GROUP_API_WITH_NOTIFICATION params:postDict attachment:mediaDic];

}

- (void) deleteGroupAction:(NSString *)groupID
{
    [self deleteRequest:DELETE_GROUP_API(groupID)];
}

- (void) unsubscribeGroupAction :(NSString *)userID :(NSString *)groupID
{
    [self deleteRequest:DELETE_USER_GROUP_API(userID,groupID)];
}

- (void) getGroupAsks:(NSString*)group_id
{
    [self getRequestForURL:GET_GROUPS_ASKS(group_id) withTimeOut:MIN_TIMEOUT];
}
-(void)putGroupSubscribers:(NSString*)group_id subcribers_id:(NSString*)subcribers_id
{
    [self putRequest:[NSURL URLWithString:PUT_GROUPS_SUBCRIBERS(group_id,subcribers_id)]];
}

- (void) getGroupSearch:(NSString *)_strSearch offset:(NSString*)offset {
    [self getRequestForURL:GET_GROUP_SEARCH(_strSearch,offset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) fnGET_MES_GROUP_SEARCH:(NSString *)_strSearch offset:(NSString*)offset {
    [self getRequestForURL:GET_MES_GROUP_SEARCH(_strSearch,offset) withTimeOut:MEDIUM_TIMEOUT];
}

#pragma mark - Groups Toutes
-(void) getGroupToutesSubscribeAction:(NSString *)group_id {
    [self getRequestForURL:GETGROUPSUBSCRIBER_API(group_id) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) getGroupToutesSubscribeFriendAction:(NSString *)group_id {
    [self getRequestForURL:GETGROUPSUBSCRIBERFRIEND_API(group_id) withTimeOut:MEDIUM_TIMEOUT];
}
-(void) getPublicationLikesAction:(NSString *)publication_id {
    [self getRequestForURL:GET_PUBLICATION_LIKES_API(publication_id) withTimeOut:MIN_TIMEOUT];
}

- (void) getGroupToutesAction:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:GROUP_TOUTES_API(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getGroupToutesSearch:(NSString *)search limit:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:LOUNGESEARCH_LIMIT_API(search, strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getGroupToutesSearchAction:(NSString *)_strSearch {
    [self getRequestForURL:LOUNGESEARCH_API(_strSearch) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getUserGroupToutesAction:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:GETUSERLOUNGESACTION_API(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) postGroupJoinAction:(NSString *)postID{
    [self postRequestForURL:POST_GROUP_JOIN_ACTION_API([self getSenderId], postID)];
}
-(void) postGroupInviteFriendNaturapass:(NSString *)groupID andFriend:(NSString*)friendID{
    [self postRequestForURL:GROUP_INVITE_FRIEND_NATURAPASS(groupID, friendID)];
}

-(void) postGroupInviteOtherGroup:(NSString *)groupID otherGroup:(NSString*)otherGroupID{
    [self postRequestForURL:GROUP_INVITE_GROUPE(groupID, otherGroupID)];
}

//POST_GROUP_PHOTO

- (void) postGroupPhoto:(NSString *)groupID withData:(NSDictionary*)data {
    
    [self postRequestUploadPhoto:POST_GROUP_PHOTO(groupID) attachment:data forGroup:YES];
}

#pragma mark - FOR V2
//FAVORITE PUBLICATION
-(void) fnDELETE_FAVORITE_PUBLICATION:(NSString*) favorite_id
{
    [self deleteRequest:FAVORITE_PUBLICATION_URL(favorite_id)];
}

-(void) fnGET_FAVORITE_PUBLICATION:(NSString*) favorite_id
{
    [self getRequestForURL:FAVORITE_PUBLICATION_URL(favorite_id) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnPUT_FAVORITE_PUBLICATION:(NSString*) favorite_id wwithParam:(NSDictionary*)param
{
    [self putRequestPublication:[NSURL URLWithString:FAVORITE_PUBLICATION_URL(favorite_id)] withParametersDic:param];
}

//PUBLICATION
-(void) getCategories
{
    [self getRequestForURL:GET_CATEGORIES withTimeOut:MEDIUM_TIMEOUT];
}

-(void) getSearchAnimal
{
    [self getRequestForURL:GET_SEARCH_ANIMAL withTimeOut:MEDIUM_TIMEOUT];
}

-(void) getFriendsRecommendations
{
    [self getRequestForURL:GET_FRIEND_RECOMMENDATION(@"9") withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getVoisChiensTypeWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_VOIS_CHIENS_TYPE(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getVoisChiensBreedWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_VOIS_CHIENS_BREED(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getProfileAllWithKind:(NSString*)kind withLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_WITH_KIND(kind,strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getWeaponsBrandsWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_WEAPONS_BRANDS(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getWeaponsCalibresWithLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_WEAPONS_CALIBRES(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getProfileItemWithKind:(NSString*)kind withkindID:(NSString *)kind_id withLimit:(NSString *)strLimit withOffset:(NSString *)strOffset{
    [self getRequestForURL:GET_PROFILE_ITEM_WITH_KIND(kind,kind_id,strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

-(void)postProfileWithParam:(NSDictionary*)params attachment:(NSDictionary*)attachment withKind:(int)type_view
{
    switch (type_view) {
        case TYPE_DOG:
        {
            [self postProfileRequestWithURL:POST_PROFILE_WITH_KIND(PROFILE_MULTI_DOGS) params:params attachment:attachment];
        }
            break;
        case TYPE_WEAPONS:
        {
            [self postProfileRequestWithURL:POST_PROFILE_WITH_KIND(PROFILE_MULTI_WEAPONS) params:params attachment:attachment];
        }
            break;
        case TYPE_PAPER:
        {
            [self postProfileRequestWithURL:POST_PROFILE_WITH_KIND(PROFILE_MULTI_PAPERS) params:params attachment:attachment];
        }
            break;
        default:
            break;
    }
}

-(void)putProfileFilesWithKindID:(NSString*)kind_id attachment:(NSDictionary*)attachment withParam:(NSDictionary*)params  withKind:(int)type_view
{
    NSString *srtUrl = @"";
    switch (type_view) {
        case TYPE_DOG:
        {
            srtUrl = POST_PROFILE_FILES_WITH_KIND(kind_id);
        }
            break;
        case TYPE_WEAPONS:
        {
            srtUrl = POST_PROFILE_FILES_WITH_WEAPON_MEDIA(kind_id);
        }
            break;
        case TYPE_PAPER:
        {
        }
            break;
        default:
            break;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:srtUrl]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:VIDEO_TIMEOUT] ;
    
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    //
    if (attachment != NULL) {
        
        NSString *fileName = attachment[@"kFileName"];
        
        NSString *strName = attachment[@"name"];
        
        NSData *fileData = attachment[@"kFileData"];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",strName,fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        
        if([fileData length])
            [body appendData:[NSData dataWithData:fileData]];
        
    }
    ///
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[body length]] forHTTPHeaderField:@"Content-Length"];
    
    [self processingRequest: request];
    
}

-(void)putProfileWithKindID:(NSString*)kind_id withKind:(int)type_view  withParams:(NSDictionary*)params attachment:(NSDictionary*)attachment
{
    switch (type_view) {
        case TYPE_DOG:
        {
            [self postProfileRequestWithURL:POST_PROFILE_MEDIA_WITH_KIND(PROFILE_MULTI_DOGS, kind_id) params:params attachment:attachment];
        }
            break;
        case TYPE_WEAPONS:
        {
            [self postProfileRequestWithURL:POST_PROFILE_MEDIA_WITH_KIND(PROFILE_MULTI_WEAPONS, kind_id) params:params attachment:attachment];
        }
            break;
        case TYPE_PAPER:
        {
            [self postProfileRequestWithURL:POST_PROFILE_MEDIA_WITH_KIND(PROFILE_MULTI_PAPERS, kind_id) params:params attachment:attachment];
        }
            break;
        default:
            break;
    }
}

- (void) deleteProfileWithKind:(NSString*)kind kindID:(NSString*)kind_id
{
    [self deleteRequest:DELETE_PROFILE_WITH_KIND(kind,kind_id)];
}

- (void) deleteProfileMediaWithKind:(NSString*)kind WithmediaID:(NSString*)media_id
{
    [self deleteRequest:DELETE_PROFILE_MEDIA_WITH_KIND(kind,media_id)];
}

//get
- (void) getItemWithKind:(NSString *)mykind myid:(NSString*)myid {
    [self getRequestForURL:GET_ITEM_API(mykind,myid) withTimeOut:MEDIUM_TIMEOUT];
    
}

-(void)getUserParametersFriendsAction
{
    [self getRequestForURL:GET_USER_PARAMETERS_FRIEND withTimeOut:MIN_TIMEOUT];
}

- (void) getAsksWithKind:(NSString*)mykind mykind_id:(NSString*)mykind_id
{
    [self getRequestForURL:GET_ASKS_KIND(mykind,mykind_id) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getPublicationsAction:(NSString *)my_id forSharing:(NSString *)strLimit withOffset:(NSString *)strOffset kind:(NSString*) myKind{
    [self getRequestForURL:Get_Publication(myKind,my_id,strLimit,strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

//#define GET_PUBLICATION_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,group_id)

- (void) getPublicationMapAction:(NSDictionary*)dicMap listGroup:(NSString *)strGroup_id
{
    [self getRequestForURL:GET_PUBLICATION_MAP(dicMap[@"southWestLatitude"],
                                               dicMap[@"southWestLongitude"],
                                               dicMap[@"northEastLatitude"],
                                               dicMap[@"northEastLongitude"],
                                               strGroup_id) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getMessageActionWithMykind:(NSString *)mykind mykind_id:(NSString*)mykind_id limit:(NSString*)limit loaded:(NSString*)loaded previous:(NSString*)previous{
    [self getRequestForURL:GET_MESSAGE_ACTION(mykind,mykind_id,limit,loaded,previous) withTimeOut:MEDIUM_TIMEOUT];
}

- (void) getMessageOffsetActionWithMykind:(NSString *)mykind mykind_id:(NSString*)mykind_id limit:(NSString*)limit offset:(NSString*)offset{
    [self getRequestForURL:GET_MESSAGE_OFFSET_ACTION(mykind,mykind_id,limit,offset) withTimeOut:MEDIUM_TIMEOUT];
}

-(void)getPublicationFilterOffWithLimit:(NSString*)limit offset:(NSString*)offset withGroups:(NSString*) strGroupAppend
{
    [self getRequestForURL:GET_PUBLICATION_FILTEROFF(limit, offset,strGroupAppend) withTimeOut:VIDEO_TIMEOUT];
}

- (void) getPublicationMapFilterOff:(NSDictionary*)dicMap {
    [self getRequestForURL:GET_PUBLICATION_MAP_FILTEROFF(dicMap[@"southWestLatitude"],
                                                         dicMap[@"southWestLongitude"],
                                                         dicMap[@"northEastLatitude"],
                                                         dicMap[@"northEastLongitude"],@"") withTimeOut:VIDEO_TIMEOUT];
    
}

-(void)fnGET_FAVORITE_PUBLICATIONLIST:(NSString*)limit offset:(NSString*)offset
{
    [self getRequestForURL:GET_FAVORITE_PUBLICATION(limit, offset) withTimeOut:VIDEO_TIMEOUT];
}

- (void) getLoungeOldOwner:(NSString *)strLimit withOffset:(NSString *)strOffset {
    [self getRequestForURL:GET_LOUNGES_OLD_OWNER(strLimit, strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
- (void) getGroupAgendaWithGroupID:(NSString*)group_id {
    [self getRequestForURL:GET_GROUP_AGENDA(group_id) withTimeOut:MEDIUM_TIMEOUT];
    
}
- (void) getAllHuntsAdmin {
    [self getRequestForURL:GET_ALL_HUNTS_ADMIN withTimeOut:MEDIUM_TIMEOUT];
    
}
- (void) getAllGroupsAdmin {
    [self getRequestForURL:GET_ALL_GROUPS_ADMIN withTimeOut:MEDIUM_TIMEOUT];
    
}
-(void)getLoungeSubscriberNotMember:(NSString*)lounge_id
{
    [self getRequestForURL:GET_LOUNGE_SUBCRIBER_NOTMEMBER(lounge_id) withTimeOut:MIN_TIMEOUT];
}
-(void)getCategoriesGeolocated:(NSString*)strLat strLng:(NSString*)strLng
{
    [self getRequestForURL:GET_CATEGORIES_GEOLOCATED(strLat, strLng) withTimeOut:MEDIUM_TIMEOUT];
}

-(void)fnGET_CATEGORIES_BY_RECEIVER:(NSString*)strReciever
{
    [self getRequestForURL:GET_CATEGORIES_BY_RECEIVER(strReciever) withTimeOut:MIN_TIMEOUT];
}
-(void)fnGET_LIVE_HUNT_PUBLICATION_DISCUSSION:(NSString*)hunt_id limit:(NSString*)strlimit offset:(NSString*)strOffset
{
    [self getRequestForURL:GET_LIVE_HUNT_PUBLICATION_DISCUSSION(hunt_id,strlimit,strOffset) withTimeOut:MEDIUM_TIMEOUT];
}
-(void) getInfoLocation:(NSString *)lat withLng:(NSString*)lng{
    [self getRequest:GET_INFO_LOCATION(lat,lng)];
}
-(void) getInfoWeatherMap:(NSString *)lat withLng:(NSString*)lng withKey:(NSString*)key{
    [self getRequest:GET_INFO_WEATHER_MAP(lat,lng,key)];
}
-(void) getInfoWindMap:(NSString *)lat withLng:(NSString*)lng withKey:(NSString*)key{
    [self getRequest:GET_INFO_WIND_MAP(lat,lng,key) ];
}
-(void) getUserNotificationStatus:(NSString *)strCount{
    [self getRequestForURL:GET_USER_NOTIFICATION_STATUS_API(@"30",strCount) withTimeOut:MAX_TIMEOUT];
}
-(void) getInfoInterstice{
    [self getRequestForURL:GET_INTERSTICE_API withTimeOut:MIN_TIMEOUT];
}
-(void) getToCacheSystem{
    [self getRequestForURL:GET_TOCACHESYSTEM withTimeOut:MAX_TIMEOUT];
}
-(void) getPublicationColor{
    [self getRequestForURL:GET_PUBLICATION_COLOR withTimeOut:MEDIUM_TIMEOUT];
}
-(void) getUserLock{
    [self getRequestForURL:GET_USER_LOCK withTimeOut:MIN_TIMEOUT];
}

//delete
- (void) deleteMessageWithKind :(NSString*)mykind mykind_id:(NSString *)mykind_id
{
    [self deleteRequest:DELETE_MESSAGE_ACTION(mykind, mykind_id)];
}

- (void) deleteUserFriendShipAction:(NSString *)struserID  {
    [self deleteRequest:DELETE_USER_FRIENDSHIP_API(struserID)];
}
- (void) deleteUserLock:(NSString *)user_id  {
    [self deleteRequest:DELETE_USER_LOCK(user_id)];
}
//post
-(void)postInvitionEmailWithDictionary: (NSDictionary *) postDictionary
{
    [self postRequest:[NSURL URLWithString:POST_INVITATION_EMAIL_API] withParametersDic:postDictionary];
}

-(void) fnPOST_MESSAGE_DISCUSSION:(NSDictionary*)param
{
    [self postRequest:[NSURL URLWithString:POST_MESSAGE_DISCUSSION] withParametersDic:param];
    
}

-(void) fnPOST_PUBLICATION_FAVORITES:(NSDictionary*)param
{
    [self postRequest:[NSURL URLWithString:POST_PUBLICATION_FAVORITES] withParametersDic:param];
    
}

- (void) postPublicationLikeAction:(NSString *)publication_id  {
    //ASLog(@"Publication Id = %@",publication_id);
    [self postRequestForURL:LIKE_API(publication_id)];
}
- (void) postPublicationCommentLikeAction:(NSString *)comment_id {
    [self postRequestForURL:POSTCOMMENTLIKE_API(comment_id)];
}

- (void) postAjouterFriendShips:(NSString *)the_id {
    [self postRequestForURL:POST_ADD_FRIENDSHIP(the_id)];
}

- (void) postMessageActionWithKind:(NSString *)mykind mykind_id:(NSString*)mykind_id withParametersDic:(NSDictionary *)postDict {
    [self postRequest:[NSURL URLWithString:POST_MESSAGE_ACTION(mykind,mykind_id)] withParametersDic:postDict];
}
- (void) postGroupAgendaWithGroupId:(NSString *)group_id  withParametersDic:(NSDictionary *)postDict {
    [self postAgendaRequestWithURL:POST_GROUP_AGENDA(group_id) params:postDict];
}
- (void) postLoungeNotMemberWithLoungId:(NSString *)lounge_id  withParametersDic:(NSDictionary *)postDict {
    [self postAgendaRequestWithURL:POST_LOUNGE_NOTMEMBER(lounge_id) params:postDict];
}
- (void) postUserLock:(NSDictionary *)postDict {
    [self postAgendaRequestWithURL:POST_USER_LOCK params:postDict];
}

//put
-(void)putAdminSubscribersWithKind:(NSString*)mykind group_id:(NSString*)group_id subcribers_id:(NSString*)subcribers_id
{
    [self putRequest:[NSURL URLWithString:PUT_ADMIN_SUBCRIBERS(mykind,group_id,subcribers_id)]];
}

-(void) putEmailNotificationSetting:(NSDictionary*) param
{
    [self putRequest:[NSURL URLWithString: PUT_EMAIL_OPTION(param[@"option"], param[@"value"]) ]];
}
-(void) putSmartphoneNotificationSetting:(NSDictionary*) param
{
    [self putRequest:[NSURL URLWithString: PUT_SMARTPHONE_OPTION(param[@"option"], param[@"value"]) ]];
}
-(void) putGroupSmartphoneNotificationSetting:(NSDictionary*) param
{
    [self putRequest:[NSURL URLWithString: PUT_GROUP_SMARTPHONE_OPTION(param[@"option"], param[@"value"],param[@"group_id"])]];
}

-(void) fnGET_NOTIFICATION_GLOBAL_PARAMETERS
{
    [self getRequestForURL:GET_NOTIFICATION_GLOBAL_PARAMETERS withTimeOut:MEDIUM_TIMEOUT];
}
-(void) fnGET_EMAIL_PARAMETERS
{
    [self getRequestForURL:GET_EMAIL_PARAMETERS withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_NOTI_PARAMETERS
{
    [self getRequestForURL:GET_NOTI_PARAMETERS withTimeOut:MEDIUM_TIMEOUT];
}
-(void) fnGET_GROUPS_PARAMETERS_MOBILE:(NSString*)group_id mykind:(NSString *)mykind
{
    [self getRequestForURL:GET_GROUPS_PARAMETERS_MOBILE(mykind,group_id) withTimeOut:MEDIUM_TIMEOUT];
}
-(void) PUT_COMMENT_PUBLICATION_ACTION:(NSDictionary*) param
{
    [self putRequest:[NSURL URLWithString: PUT_COMMENT_PUBLICATION( param[@"value"]) ]];
}
- (void) PUT_COMMENT_PUBLICATION_ACTION:(NSString *) comment_id withParametersDic:(NSDictionary *)postDict {
    [self putRequestPublication:[NSURL URLWithString:PUT_COMMENT_PUBLICATION(comment_id)] withParametersDic:postDict ];
}
- (void) PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:(NSDictionary *)param {
    [self putRequestURL:GET_SQLITE_ALL_POINTS_IN_MAP withParam:param timeout:300];
}

- (void) PUT_SQLITE_ALL_POINTS_IN_MAP_USING_DEFAULT_DB:(NSDictionary *)param {
    [self putRequestURL:GET_SQLITE_ALL_POINTS_IN_MAP_USING_DEFAULT_DB withParam:param timeout:300];
}

-(void) fnPUT_USER_IDENTIFIER_WITHPARAM:(NSDictionary *)param
{
    [self putRequestURL:PUT_USER_IDENTIFIER withParam:param timeout:MIN_TIMEOUT];
    
}
-(void) fnGET_USER_ADDRESS
{
    [self getRequestForURL:GET_USER_ADDRESS withTimeOut:MIN_TIMEOUT];
}

-(void) fnGET_MES_GROUP_HUNT
{
    [self getRequestForURL:GET_MES_GROUP_HUNT([self getSenderId]) withTimeOut:MEDIUM_TIMEOUT];
}
//get conversations
-(void) fnGET_USER_CONVERSATIONS;
{
    [self getRequestForURL:GET_USER_CONVERSATIONS withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_CONVERSATIONS_MESSAGE:(NSString *)conversationID limit:(NSString*)strLimit offset:(NSString*)strOffset
{
    [self getRequestForURL:GET_CONVERSATIONS_MESSAGE(conversationID,strLimit,strOffset) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_ALL_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset limit_hunt:(NSString*)limit_hunt offset_hunt:(NSString*)offset_hunt limit_group:(NSString*)limit_group offset_group:(NSString*)offset_group;
{
    [self getRequestForURL:GET_ALL_CONVERSATIONS(limit, offset, limit_hunt, offset_hunt, limit_group, offset_group) withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_DISCUSSTION_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
{
    [self getRequestForURL:GET_DISCUSSTION_CONVERSATIONS(limit, offset) withTimeOut:MEDIUM_TIMEOUT];
}
-(void) fnGET_LOUNGE_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
{
    [self getRequestForURL:GET_LOUNGE_CONVERSATIONS(limit, offset) withTimeOut:MEDIUM_TIMEOUT];
}
-(void) fnGET_GROUP_CONVERSATIONS:(NSString *)limit offset:(NSString*)offset;
{
    [self getRequestForURL:GET_GROUP_CONVERSATIONS(limit, offset) withTimeOut:MEDIUM_TIMEOUT];
}
-(void)fnPOST_CREATE_DISCUSSION:(NSDictionary*) param
{
    [self postRequestData:[NSURL URLWithString:POST_CREATE_DISCUSSION] withParametersDic:param withMedia:NULL];
}

-(void) fnPOST_USER_ADDRESS:(NSDictionary*) param
{
    [self postRequestData:[NSURL URLWithString:POST_USER_ADDRESS] withParametersDic:param withMedia:NULL];
}

-(void) fnDELETE_USER_ADDRESS:(NSDictionary*) param
{
    [self deleteRequest:DELETE_USER_ADDRESS(param[@"id"])];
}
-(void) fnDELETE_GENERAL_CHAT_MESSAGE:(NSString*)msg_id
{
    [self deleteRequest:DELETE_GENERAL_CHAT_MESSAGE(msg_id)];
}

-(void) fnPOST_GROUP_MULTIPLE_JOIN:(NSDictionary*) param
{
    [self postRequestData: [NSURL URLWithString: POST_GROUP_MULTIPLE_JOIN(param[@"group_id"])]  withParametersDic:param[@"data"] withMedia:NULL];
}

-(void) fnGET_GET_NEWS
{
    [self getRequestForURL:GET_NEWS withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_ALL_NOTIFICATION
{
    [self getRequestForURL:GET_ALL_NOTIFICATION withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_GROUP_PENDING_INVITATION:(NSDictionary*)param
{
    [self getRequestForURL:GET_GROUP_PENDING_INVITATION(param[@"limit"],param[@"offset"]) withTimeOut:MEDIUM_TIMEOUT];
    
}
-(void) fnV2_GET_GROUP_INVITATION
{
    [self getRequestForURL:V2_GET_GROUP_INVITATION withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_DASHBOARD_LIVE_HUNT
{
    [self getRequestForURL:GET_DASHBOARD_LIVE_HUNT withTimeOut:MEDIUM_TIMEOUT];
}

-(void) fnGET_HUNT_PENDING_INVITATION:(NSDictionary*)param
{
    [self getRequestForURL:GET_HUNT_PENDING_INVITATION(param[@"limit"],param[@"offset"]) withTimeOut:MEDIUM_TIMEOUT];
    
}

-(void) fnGETUSERPROFILE_API
{
    [self getRequestForURL:GETUSERPROFILE_API withTimeOut:MIN_TIMEOUT];
    
}

-(void) fnPUT_READ_MESSAGE:(NSDictionary *)postDict
{
    [self putRequestLounge:[NSURL URLWithString:PUT_READ_MESSAGE] withParametersDic:postDict];
    
}

-(void)putUserParametersFriendsActionWithParametersDic:(NSDictionary *)postDict{
    [self putRequestLounge:[NSURL URLWithString:PUT_USER_PARAMETERS_FRIEND] withParametersDic:postDict];
}
- (void) putJoinWithKind:(NSString*)mykind mykind_id:(NSString *)mykind_id strUserID:(NSString *)strUserID {

        [self putRequest:[NSURL URLWithString:PUT_JOIN_KIND_NEW(mykind,mykind_id,strUserID)]];

}

//delete
- (void) deleteJoinWithKind :(NSString*)mykind UserID:(NSString *)userID mykind_id:(NSString *)mykind_id
{
    [self deleteRequest:DELETE_JOIN_KIND(mykind,userID,mykind_id)];
}
- (void) deleteNonMembers :(NSString*)lounge_id nonmembers_id:(NSString *)nonmembers_id
{
    [self deleteRequest:DELETE_LOUNGE_NOTMEMBER(lounge_id, nonmembers_id)];
}
- (void) deleteLoungeSubcriberJoin:(NSString*)lounge_id members_id:(NSString *)members_id
{
    [self deleteRequest:DELETE_LOUNGE_SUBCRIBER_JOINS(lounge_id, members_id)];
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {
        NSString *part = [NSString stringWithFormat: @"%@=%@", key, [dictionary valueForKey:key]];
        [parts addObject:part];
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}


- (NSString *)getJsonDataFromDictionary:(NSMutableDictionary *)dictionary {
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:nil] ;
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}

-(void)fnPOST_SHAPES:(NSDictionary*) param
{
    [self postRequestData:[NSURL URLWithString:POST_SHAPE] withParametersDic:param withMedia:NULL];
    
}
-(void)fnPUT_SHAPES:(NSDictionary*) param withShapeID:(NSString*)shapeID
{
    [self putRequestURL:PUT_SHAPE(shapeID) withParam:param timeout:300];

    
}
-(void)fnDELETE_SHAPES:(NSString*) shapeID
{
    
    [self deleteRequest:PUT_SHAPE(shapeID)];
    
}
//MARK:- REQUES
/****************************************************************************************************************/

-(void) cancelTask
{
    [task cancel];
}

-(void) processingRequest :(NSURLRequest*)request
{
    NSURLSession *session = [NSURLSession sharedSession];
    
    task = [session dataTaskWithRequest:request
                      completionHandler:
            ^(NSData *data, NSURLResponse *response, NSError *error) {
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                ASLog(@"[REQUEST URL] :>>> %@", [[request URL] absoluteString]);

                dispatch_async(dispatch_get_main_queue(), ^{
                    id jsonData = nil;
                    int code = (int)error.code;
                    //check error
                    
                    if (error) {
                        
                        //                                              [KSToastView ks_showToast:NSLocalizedString(@"INVALID_ERROR", @"")  duration:2.0f completion: ^{
                        //                                              }];
                        
                        if (self.onComplete) {
                            self.onComplete(nil, code);
                        }
                        return ;
                    }
                    
                    //don't want error processing:
                    if (self.bNoNeedResponse == YES) {
                        id jsonDict =[NSJSONSerialization JSONObjectWithData: data options: 0 error: NULL];
                        if ([jsonDict isKindOfClass: [NSDictionary class]])
                        {
                            jsonData = (NSDictionary*)jsonDict;
                            if (self.onComplete) {
                                
                                self.onComplete(jsonData, code);
                            }
                            
                        }
                        else if ([jsonDict isKindOfClass: [NSDictionary class]])
                        {
                            jsonData = (NSArray*)jsonDict;
                            
                            if (((NSArray*)jsonData).count>0) {
                                if (self.onComplete) {
                                    self.onComplete(jsonData, code);
                                }
                            }
                            else
                            {
                                if (self.onComplete) {
                                    self.onComplete(nil, code);
                                }
                            }
                        }
                        else
                        {
                            if (self.onComplete) {
                                self.onComplete(nil, code);
                            }
                            
                        }
                        
                        return;
                    }
                    
                    
                    
                    code = (int)((NSHTTPURLResponse *)response).statusCode;
                    if (data == nil) {
                        
                        if (self.onComplete) {
                            self.onComplete(nil, code);
                        }
                        
                        return;
                    }
                    
                    
                    id jsonDict =[NSJSONSerialization JSONObjectWithData: data options: 0 error: NULL];
                    
                    if ([jsonDict isKindOfClass: [NSArray class]]) {
                        jsonData = (NSArray*)jsonDict;
                        
                        if (((NSArray*)jsonData).count>0) {
                            if (self.onComplete) {
                                self.onComplete(jsonDict, code);
                            }
                        }
                        else
                        {
                            if (self.onComplete) {
                                self.onComplete(nil, code);
                            }
                        }
                        return;
                        
                    }
                    else if ([jsonDict isKindOfClass: [NSDictionary class]])
                    {
                        jsonData = (NSDictionary*)jsonDict;
                    }
                    else{
                        
                        if (self.onComplete) {
                            self.onComplete(nil, code);
                        }
                        
                        return;
                    }
                    
                    if (jsonData[@"code"]) {
                        code = (int)[jsonData[@"code"] intValue];
                    }
                    
                    BOOL hasIssue = YES;
                    
                    //Show Dog
                    if(code == 503)
                    {
                        if (self.onComplete) {
                            self.onComplete(nil,503);
                        }
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName: KILL_TEXT_FOCUS object:nil];
                        
                        [appDelegate showErrorViewController];
                        
                        hasIssue = NO;
                    }
                    //Internal error
                    
                    else if (code == 500)
                    {
                        NSString *strSpecialChecking = [[request URL] absoluteString];
                        
                        ASLog(@"ISSUE-URL:>>> %@", [[request URL] absoluteString]);
                        ASLog(@"%@",response);
                        
                        //Special case groups/hunts sometime 500 when init the app, forget it.
                        if ([strSpecialChecking containsString:@"groups/hunts" ]) {
                            return;
                        }
                        
                        if ([[jsonData valueForKey:@"message"] isKindOfClass:[NSString class]]) {
                            
                            if (self.onComplete) {
                                self.onComplete(nil,500);
                            }
                            
                            [appDelegate showErrMsg:jsonData[@"message"] ];
                            hasIssue = NO;
                            
                        }
                        else
                        {
                            NSArray *errorString = [jsonData valueForKey:@"message"];
                            NSError *error = nil;
                            
                            if (self.onComplete) {
                                self.onComplete(nil,500);
                            }
                            if (errorString) {
                                NSData *data = [NSJSONSerialization dataWithJSONObject:errorString options:kNilOptions error:&error];
                                NSString *str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                
                                
                                [appDelegate showErrMsg:str ];
                                hasIssue = NO;
                                
                            }
                        }
                    }
                    else if (code == 404)
                    {
                        
                        if (self.onComplete) {
                            self.onComplete(jsonData,404);
                        }
                        [COMMON removeProgressLoading];
                        
                        
                    }
                    //un authorize
                    //  if 401 (un authorized) => check if user login was saved => auto login again…if fail => show Login Screen
                    

                    else if(code == 401)
                    {
                        //tri 1 time
                       
                        WebServiceAPI *serviceAPIx =[WebServiceAPI new];
                        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
                        
                        if (pushToken)
                        {
                            [serviceAPIx getRequest: REAL_LOGIN([COMMON keyChainUserID], [COMMON keyChainPasswordID], pushToken)];
                        }
                        else
                        {
                            [serviceAPIx getRequest: REAL_LOGIN_NO_PUSH([COMMON keyChainUserID], [COMMON keyChainPasswordID])];
                        }

                        serviceAPIx.onComplete =^(id response, int errCode)
                        {
                            NSMutableDictionary *userDictionary = [response valueForKey:@"user"];
                            if ([userDictionary isKindOfClass:[NSDictionary class]] && userDictionary != NULL) {
                                NSString *user_id   = [userDictionary valueForKey:@"id"];
                                NSString *user_sid  = [userDictionary valueForKey:@"sid"];
                                if (user_id != NULL && user_sid != NULL) {
                                    [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:@"sender_id"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue: userDictionary[@"usertag"] forKey:@"user_tag"];
                                    
                                    [[NSUserDefaults standardUserDefaults] setValue:user_sid forKeyPath:@"PHPSESSID"];
                                    [[NSUserDefaults standardUserDefaults] synchronize];
                                }
                                else
                                {
                                    [appDelegate forceLogout];
                                }
                            }
                            else
                            {
                                [appDelegate forceLogout];
                            }
                        };
                        
                        return;
                        
                    }
                    //session timeout
                    else if (code == 403)
                    {
                        [COMMON removeProgressLoading];
                        
                        NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: data options: 0 error: NULL];
                        
                        if (self.onComplete) {
                            self.onComplete(jsonData, code);
                        }
                        
                        
                        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
                        
                        if (pushToken)
                        {
                            [self getRequest: REAL_LOGIN([COMMON keyChainUserID], [COMMON keyChainPasswordID], pushToken)];
                        }
                        else
                        {
                            [self getRequest: REAL_LOGIN_NO_PUSH([COMMON keyChainUserID], [COMMON keyChainPasswordID])];
                        }
                    }
                    else {
                        
                        //200...201 success response from RESTFUL
                        
                        hasIssue = NO;
                        
                        if (self.onComplete) {
                            self.onComplete(jsonData, code);
                        }
                        
                    }
                    
                    // ... push error alert
                    if (hasIssue) {
                        [self fnCheckResponse:jsonData];
                    }
                    
                });
                
                
            }];
    
    [task resume];
}

#pragma mark - continue


@end
