//
//  ImageViewAction.h
//  Naturapass
//
//  Created by Manh on 11/26/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^onCallBack)(NSInteger index);

@interface ImageViewAction : UIImageView
@property (nonatomic, copy) onCallBack oncallback;
@property (nonatomic, assign) NSInteger indexPath;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, copy) UITapGestureRecognizer *singleTap;
-(void)fnRemoveClick;
@end
