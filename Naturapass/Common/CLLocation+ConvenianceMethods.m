//
//  CLLocation+ConvenianceMethods.m
//  Naturapass
//
//  Created by Clément Padovani on 9/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "CLLocation+ConvenianceMethods.h"

@implementation CLLocation (ConvenianceMethods)

- (BOOL) locationExceedsDistanceDelta: (CLLocation *) aLocation
{
	static const CLLocationDistance kDistanceDelta = 15;
	
	return ([self distanceFromLocation: aLocation] >= kDistanceDelta);
}

- (BOOL) locationExceedsDurationDelta: (CLLocation *) aLocation
{
	static const NSTimeInterval kDurationDelta = 20;
	
	return ([[aLocation timestamp] timeIntervalSinceDate: [self timestamp]] >= kDurationDelta);
}


@end
