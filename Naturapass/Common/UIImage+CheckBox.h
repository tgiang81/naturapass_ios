//
//  UIImage+CheckBox.h
//  Naturapass
//
//  Created by macbook on 18/01/2019.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImage (CheckBox)
+ (UIImage *)imageCheckBoxWithBackground:(UIColor*)colorBackground colorActive:(UIColor*)colorActive colorBorder:(UIColor*)colorBorder isRadio:(BOOL)isRadio;
@end

NS_ASSUME_NONNULL_END
