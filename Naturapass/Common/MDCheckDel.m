//
//  MDCheckBox.m
//  Naturapass
//
//  Created by Manh on 7/30/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDCheckDel.h"
#import "Define.h"
@implementation MDCheckDel
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        
        [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
        [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
    [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
}

-(void)setTypeCheckBox:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            
            [self setImage: [UIImage imageNamed:@"ic_add_member_group"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"ic_add_member_group"] forState:UIControlStateNormal];
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
            break;
        case UI_GROUP_TOUTE:
        {
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        }
            break;
        default:
            break;
    }

}
@end
