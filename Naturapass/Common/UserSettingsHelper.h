//
//  UserSettingsHelper.h
//  Naturapass
//
//  Created by Admin on 3/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserSettingsHelper : NSObject
{

}

#pragma mark - fetch interstice time

+(NSString*)getFetchIntersticeTime;

+(void)setFetchIntersticeTime:(NSString*)time;

#pragma mark - advertising display time

+(NSString*)getIntersticeDisplayTime;

+(void)setIntersticeDisplayTime:(NSString*)time;

#pragma mark - advertising display time

+(NSDictionary*)getIntersticeJsonData;

+(void)setIntersticeJsonData:(NSDictionary*)dictionary;

#pragma mark -
+(NSNumber*)getGameFairDisplayTime;
+(void)setGameFairDisplayTime:(NSNumber*)time;

@end