//
//  AppCommon.h
//  Arcatime
//
//  Created by ocsdeveloper12 on 16/07/13.
//  Copyright (c) 2013 ocsdeveloper12. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "Config.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "KeychainItemWrapper.h"
#import "WebServiceAPI.h"

@interface AppCommon : UIViewController<UIApplicationDelegate,WebServiceAPIDelegate> {
    
    UIView                  *loadingView;
    UIActivityIndicatorView *activityView;
    UILabel *lblMessage;
    MBProgressHUD           *progressHUD;

    //keychain
    KeychainItemWrapper         *keychain;
    //Facebook
    
    NSArray     *permissions;
    
    UIAlertView *alert;
}

+(AppCommon *) common;

@property (nonatomic) BOOL pushAreEnabled;

@property (nonatomic) BOOL isShowned;
//Reachability
-(BOOL) isReachable;
-(BOOL) isReachableCheck;

// App Name

- (NSString *)getAppName;

/// Is Logged in
- (BOOL)isLoggedIn;
- (void) doLogout;

// Set User Details
- (NSString *)getUserId;
// set Country Details

// TypeOFUser
-(void)settypeOfUser:(BOOL)userType;
-(BOOL)returnTypeOfUser;

// SetFecebookDetails

- (NSString *) makeItSecret: (NSString *) aSecret;

- (NSString *) encrypt: (NSString *) input;

// - (NSString*) encrypt:(NSString*)input;
- (NSString *) keyChainUserID;
- (NSString *) keyChainPasswordID;

//Alerts
- (void) showErrorAlert:(NSString *)strMessage;

//Load Icon
- (void) LoadIconinView:(UIView *) view;
- (void) upLoadIconinView:(UIWindow *) view;
- (void) RemoveLoading;
- (void)addLoading:(UIViewController *)controller;

- (void)addLoadingForView:(UIView *)parent;

-(void)removeProgressLoading;
- (CGSize)dataSize:(NSString *)string withFontName:(NSString *)fontName ofSize:(NSInteger)size withSize:(CGSize)LabelWidth;
-(UIImage *)resizeImage:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize;
-(UIImage *)resizeimageScaled:(UIImage *)imageToCrop  targetSize:(CGSize)_targetSize;

//CharacterTrimming
-(NSString *)characterTrimming:(NSString *)str;

//keychainAccess
- (void) storeKeychain;

//Device
- (BOOL) isiPhone;
- (BOOL) isiPhone4;

- (BOOL) isIOS6;

//Methods
- (NSString *)getIdiomImage:(NSString *)imgName;
//- (NSString *)removeAccentsfromString:(NSString *)text;

- (void)setFacebookUserDetails:(NSMutableDictionary *)dictUserDetails;
- (NSString *)getFBUserFirstName;
- (NSString *)getFBUserLastName;
- (NSString *)getFBUserId;
- (NSString *)getFBUserDob;
- (NSString *)getFBUserEmail;
- (NSString *)getFBUserGender;
- (NSString *)getFBUserImage;
- (NSString *)getFBUserNickName;

//Facebook
- (void)saveFBDetails:(NSDictionary *)_dic;
- (void)listSubviewsOfView:(UIView *)view;
@end

extern AppCommon *SharedCommon;
#define COMMON (SharedCommon? SharedCommon:[AppCommon common])

