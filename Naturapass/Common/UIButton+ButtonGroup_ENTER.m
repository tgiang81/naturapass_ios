//
//  UIButton+ButtonGroup_ENTER.m
//  Naturapass
//
//  Created by Giang on 8/20/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "UIButton+ButtonGroup_ENTER.h"

@implementation UIButton (ButtonGroup_ENTER)
-(void)setImageWithImgStateNomal:(NSString*)imgStateNomal ImgStateSelected:(NSString*)ImgStateSelected
{
    [self setBackgroundImage: [UIImage imageNamed:ImgStateSelected] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:imgStateNomal] forState:UIControlStateNormal];
    
}

//ENTER GROUP
-(void)setImage_Mur
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_mur_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_mur_inactive"] forState:UIControlStateNormal];
    
}

-(void)setImage_Carte
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_map_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_map_inactive"] forState:UIControlStateNormal];
    
}

-(void)setImage_Discuss
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_chat_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_chat_inactive"] forState:UIControlStateNormal];
    
}

-(void)setImage_Agenda
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_event_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_event_inactive"] forState:UIControlStateNormal];
    
}
-(void)setImage_Setting
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_setting_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_setting"] forState:UIControlStateNormal];
    
}


//MES GROUP

-(void)setImage_GroupMes
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_my_group"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_my_group_inactive"] forState:UIControlStateNormal];
    
}

-(void)setImage_GroupSearch
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_search_group_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_search_group"] forState:UIControlStateNormal];
    
}


-(void)setImage_GroupSetting
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_setting_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_group_setting"] forState:UIControlStateNormal];
    
}

//MUR wall
-(void)setImage_MUR_mur
{
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_mur_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_mur"] forState:UIControlStateNormal];
    
}

-(void)setImage_MUR_filter
{
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter"] forState:UIControlStateNormal];
    
}

-(void)setImage_MUR_sestting
{
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_setting_post"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_setting"] forState:UIControlStateNormal];
}

//CHASSE
-(void)setImage_CHASSE_WALL_MUR
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_mes_chasse_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_mes_chasse_inactive"] forState:UIControlStateNormal];
}

-(void)setImage_CHASSE_WALL_SEARCH
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_search_chasse_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_search_group"] forState:UIControlStateNormal];
}
-(void)setImage_CHASSE_HISTORY
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_chasse_history_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_chasse_history_inactive"] forState:UIControlStateNormal];
}

-(void)setImage_CHASSE_WALL_SETTING
{
    [self setBackgroundImage: [UIImage imageNamed:@"ic_chasse_setting_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"ic_chasse_setting_inactive"] forState:UIControlStateNormal];
}

//AMIS
-(void)setImage_Amis_List
{
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend"] forState:UIControlStateNormal];
    
}
-(void)setImage_Amis_Add
{
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend_add_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend_add"] forState:UIControlStateNormal];
    
}
-(void)setImage_Amis_Demand_Invitation
{
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend_added_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_friend_added"] forState:UIControlStateNormal];
    
}
-(void)setImage_Amis_Search
{
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_search_friend_active"] forState:UIControlStateSelected];
    [self setBackgroundImage: [UIImage imageNamed:@"amis_ic_search_friend"] forState:UIControlStateNormal];
    
}

@end
