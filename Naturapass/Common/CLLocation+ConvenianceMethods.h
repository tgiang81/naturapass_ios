//
//  CLLocation+ConvenianceMethods.h
//  Naturapass
//
//  Created by Clément Padovani on 9/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

@import CoreLocation;

@interface CLLocation (ConvenianceMethods)

/**
 *	@Author Clément Padovani, 14-09-29 11:09
 *
 *	@brief  Checks if @param aLocation exceeds the distance delta (currently 15m).
 *
 *	@param aLocation A CLLocation object.
 *
 *	@return If @param aLocation exceeds the distance delta.
 */

- (BOOL) locationExceedsDistanceDelta: (CLLocation *) aLocation;

/**
 *	@Author Clément Padovani, 14-09-29 11:09
 *
 *	@brief  Checks if @param aLocation exceeds the age delta (currently 20s).
 *
 *	@param aLocation A CLLocation object.
 *
 *	@return If @param aLocation exceeds the duration delta.
 */

- (BOOL) locationExceedsDurationDelta: (CLLocation *) aLocation;

@end
