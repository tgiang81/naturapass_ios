//
//  MDCheckBox.h
//  Naturapass
//
//  Created by Manh on 7/30/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MDStatusButton : UIButton
{
}
@property (nonatomic,strong) UIColor *colorEnable;
@property (nonatomic,strong) UIColor *colorDisEnable;

@property (nonatomic,strong) UIImage *imgEnable;
@property (nonatomic,strong) UIImage *imgDisEnable;
@end
