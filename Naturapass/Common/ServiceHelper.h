//
//  ServiceHelper.h
//  Naturapass
//
//  Created by Admin on 1/20/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

//#define strQueuePostingMessage         @"strQueuePostingMessage"
//#define strQueuePostingPhoto         @"strQueuePostingPhoto"
//#define strQueuePostingVideo         @"strQueuePostingVideo"

#define strMessageKey           @"Message"
#define strPhotoKey             @"Photo"
#define strVideoKey             @"Video"

#define strPostTypeKey          @"Type"
#define strPostDataKey          @"Data"
#define strPostAttachmentKey    @"Attachment"
#define strPostPublication          @"PostPublication"

@interface ServiceHelper : NSObject

#pragma mark - Singleton Thred-Safe Pattern

+ (ServiceHelper *) sharedInstance;

#pragma mark - post data

- (void) addNewPostingTask:(NSDictionary *)dicTask;

//- (void) handleNetworkStatus;
- (void) startSyncingPostingData;
-(void) resetStatus;
- (void)resetStatusPostingMessage;
-(void) cancelAllPerform;
#pragma mark - post data

- (void) getIntersticeAction;
#pragma mark -
- (NSString *) getPHPSESSID;

- (void) removePostingTask:(NSDictionary*)obj;
-(void) deleteAllPostPublication;

-(void) prepareSendingMessage:(NSDictionary*)inDic;

-(void) postingDiscussionMessageONLINE :(NSDictionary*)msgDic;
-(void) postingChatMessageONLINE :(NSDictionary*)msgDic;

- (void)resetPostingChatMessage;
#pragma mark - upload multi photo

@end
