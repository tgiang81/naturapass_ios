//
//  DPTextField.h
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^callBackPickerClick) ();

@interface MDPickerTextField : UITextField
@property (nonatomic, copy) callBackPickerClick CallBack;
@property (weak, nonatomic)  UIViewController *parentVC;
@property (nonatomic, strong) NSDate       *date;
@end
