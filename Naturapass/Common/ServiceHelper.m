//
//  ServiceHelper.m
//  Naturapass
//
//  Created by Admin on 1/20/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ServiceHelper.h"
#import <AFNetworking/AFNetworking.h>

#import "AppCommon.h"
#import "Config.h"
#import "UserSettingsHelper.h"
#import "NSDate+Extensions.h"
#import "FileHelper.h"
#import "Define.h"
#import "AZNotification.h"
#import "CommonHelper.h"

#import "MessageWaitToSend.h"
#import "NSDate+Extensions.h"
#import "MapDataDownloader.h"
#import "KSToastView.h"
#import "NSString+EMOEmoji.h"
//de qui...end

static ServiceHelper *sharedInstance = nil;

@interface ServiceHelper() {
    BOOL doingPublication, doingChatMessage;
    AFNetworkReachabilityStatus networkStatus;
    NSMutableData *_responseData;
    NSDictionary *tmpPhotoDicPost;
    NSMutableArray *dicPublicationMsg;
}

@end

@implementation ServiceHelper

#pragma mark Singleton Thred-Safe Pattern

+ (ServiceHelper *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

- (void) startSyncingPostingData
{
    [self resetPostingChatMessage];
    //Chat message
    [self resetStatusPostingMessage];
}

-(void) cancelAllPerform
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

-(void) resetStatus
{
    doingPublication = NO;
    doingChatMessage = NO;
}

#pragma mark - public

//- (void) handleNetworkStatus
//{
//    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
//        networkStatus = status;
//        switch (status) {
//            case AFNetworkReachabilityStatusReachableViaWWAN:
//            case AFNetworkReachabilityStatusReachableViaWiFi:
//                [[ServiceHelper sharedInstance] syncPostingData];
//                break;
//            case AFNetworkReachabilityStatusNotReachable:
//            default:
//                break;
//        }
//    }];
//
//    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
//}

#pragma mark - post data

- (void) addNewPostingTask:(NSDictionary *)newTask
{
    NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];

    NSString *guid  = newTask[strPostDataKey][@"publication"][@"guid"];
    
    NSMutableDictionary *dicTmp = [NSMutableDictionary new];
    if ([value isKindOfClass:[NSDictionary class]]) {
        [dicTmp addEntriesFromDictionary:value];
    }
    [dicTmp setObject:newTask forKey:guid];
    
    NSUserDefaults *myDefault = [NSUserDefaults standardUserDefaults];
    [myDefault setObject:dicTmp forKey:strQueueName];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [self syncPostingPublication];
}

//
- (void) removePostingTask:(NSDictionary*)obj
{
    NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];
    if ([value isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dicPosting = [value mutableCopy];
        NSString *guid  = obj[strPostDataKey][@"publication"][@"guid"];
        if (dicPosting[guid]) {
            [dicPosting removeObjectForKey:guid];
            [[NSUserDefaults standardUserDefaults] setObject:dicPosting forKey:strQueueName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}
- (void) failPostingTask:(NSDictionary*)obj
{
    NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];
    if (value && [value isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dicPosting = [value mutableCopy];
        NSString *guid  = obj[strPostDataKey][@"publication"][@"guid"];
        if (dicPosting[guid]) {
            NSMutableDictionary *dicTmp = [dicPosting[guid] mutableCopy];
            [dicTmp setObject:@(1) forKey:@"selected"];
            [dicPosting setObject:dicTmp forKey:guid];
            [[NSUserDefaults standardUserDefaults] setObject:dicPosting forKey:strQueueName];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }

}
- (void)resetStatusPostingMessage
{
    NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];
    if ([value isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dicPosting = [value mutableCopy];
        NSArray *arrAllKey = [dicPosting allKeys];
        for (NSString *key in arrAllKey) {
            NSMutableDictionary *dic = [dicPosting[key] mutableCopy];
            [dic removeObjectForKey:@"selected"];
            [dicPosting setValue:dic forKey:key];

        }
        [[NSUserDefaults standardUserDefaults] setObject:dicPosting forKey:strQueueName];

        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    [self performSelector:@selector(syncPostingPublication) withObject:nil afterDelay:2];
}
- (void)syncPostingPublication
{
    if ([COMMON isReachable] && (doingPublication == NO) && ([COMMON isLoggedIn]))
    {
        NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
        id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dicPosting = (NSDictionary*)value;
            NSArray *arrAllKey = [dicPosting allKeys];
            for (NSString *key in arrAllKey) {
                NSDictionary *dic = dicPosting[key];
                if (!dic[@"selected"]) {
                    tmpPhotoDicPost = dic;
                    doingPublication = YES;
                    [self processParsedObjectPublication:dic[strPostDataKey]];
                    break;
                }
            }
            return;
        }
    }
    
}

#pragma mark - posing Chat message
- (void)resetPostingChatMessage
{
    NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    NSPredicate *ind=  [NSPredicate predicateWithFormat:@"iduser == %@ && ischeck = NO", sender_id ];
    
    NSArray *arrWait = [MessageWaitToSend MR_findAllWithPredicate:ind ];
    
    for (MessageWaitToSend *obj in arrWait) {
        obj.ischeck = @YES;
    }
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    [localContext MR_saveToPersistentStoreAndWait];
    
    [self syncPostingChatMessage];
}
-(void) syncPostingChatMessage
{
    if ([COMMON isReachable] && (doingChatMessage == NO) && ([COMMON isLoggedIn]))
    {
        
        NSPredicate *ind=  [NSPredicate predicateWithFormat:@"iduser == %@ && ischeck = %@", [self getSenderId], [NSNumber numberWithBool: YES]];
        
        MessageWaitToSend *waitObj = [MessageWaitToSend MR_findFirstWithPredicate:ind sortedBy:@"timecreate" ascending:YES];
        
        if (waitObj != nil) {
            doingChatMessage = YES;
            
            if ([waitObj.type intValue] == ISDISCUSS) {
                
                [self postingDiscussionMessage: waitObj];
                
            }else{
                [self postingChatMessage: waitObj];
                
            }
            return;
        }
    }
    
}


-(void) prepareSendingMessage:(NSDictionary*)inDic
{
    //push in cache
    
    MessageWaitToSend *objCreate = [MessageWaitToSend MR_createEntity];
    
    objCreate.ischeck = @YES;
    
    objCreate.idgroup = inDic[@"idgroup"];
    
    objCreate.mycontent = inDic[@"message"];
    
    //update time create
    objCreate.timecreate = inDic[@"date"];
    
    objCreate.type = inDic[@"targetScreen"];
    
    objCreate.iduser = [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] stringValue];
    
    //Random
    objCreate.guid = [CommonHelper generatorString];
    
    objCreate.participant =  inDic[@"joinStr"];//nsarray to string...
    
    //save
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    [localContext MR_saveToPersistentStoreAndWait];
    
    [self syncPostingChatMessage];
}


-(void) postingDiscussionMessage :(MessageWaitToSend*)msgDic
{
    WebServiceAPI *serviceObj =[WebServiceAPI new];

    serviceObj.onComplete = ^(NSDictionary*response, int errCode) {

        doingChatMessage = NO;

        //? send successful but don't received success, bad network. Normally, sent...then bad network -> tick done ???
        // isReachable with bad network...?
        if (errCode == 404) {
            // error 404...not exist obj

            [msgDic MR_deleteEntity];
            
            if ([response isKindOfClass: [NSDictionary class]]) {
                if ([response[@"message"] isKindOfClass: [NSString class]]) {
                    [KSToastView ks_showToast: @"Objet non trouvé" duration:2.0f completion: ^{
                    }];
                    
                }
            }

        }else if (errCode == 500) {
            //server issue...don't delete
            msgDic.ischeck = @NO;
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        //if error 404...not exist obj  || SUCCESS-> remove from cache.

        if (!response ||[response isKindOfClass: [NSArray class] ]) {
        }
        
        if ([response[@"code"] isKindOfClass: [NSNumber class]]) {
        }
        if([response[@"message"] isKindOfClass:[NSDictionary class]] || (errCode == 201))
        {
            [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_CACHE_CHAT object:response];
            //Success -> delete from cache;
            [msgDic MR_deleteEntity];
            //notify update view...
        }

        
        [self performSelector:@selector(syncPostingChatMessage) withObject:nil afterDelay:2];

    };

    NSArray * array = [msgDic.participant componentsSeparatedByString:@","];

    NSString * timeStr = [NSDate convertToFranceTime:msgDic.timecreate];

    NSDictionary *dicPost = @{@"message":@{ @"conversationId":msgDic.idgroup,
                                            @"content":msgDic.mycontent,
                                            @"participants":array,
                                            @"created": timeStr,
                                            @"guid":msgDic.guid
                                            }};
    
    [serviceObj fnPOST_MESSAGE_DISCUSSION:dicPost];
}


-(void) postingChatMessage :(MessageWaitToSend*)msgDic
{
    WebServiceAPI *serviceObj =[WebServiceAPI new];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode) {
        
        doingChatMessage = NO;
        
        
        //if error 404...not exist obj  || SUCCESS-> remove from cache.

        if (errCode == 404) {
            
            // error 404...not exist obj
            [msgDic MR_deleteEntity];
            
            if ([response isKindOfClass: [NSDictionary class]]) {
                if ([response[@"message"] isKindOfClass: [NSString class]]) {
                    [KSToastView ks_showToast: @"Objet non trouvé" duration:2.0f completion: ^{
                    }];

                }
            }
            
            

        }else if (errCode == 403) {
            
            /*
             agenda's discussion work offline, so you are using cahce and background process at your side.
             If an admin change allowadd_chat, all user will be not notify about this change. if an user want to post a message and he have not right (not admin + allow_add_chat=0), server will responde 403 forbiden. so when you received 403, you will display an error message to inform user that he has not right to post a message : "Vous n'avez pas le droit de poster un message dans la discussion de cet événement". after that you have to remove this message in cache file and you have to update local database to update allow... and display or not new ui to disable user to add a new message in the discussion
             */
            //update db...
            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_hunt, kSQL_agenda]];
            
            //Notify delete chat id... + disable edit...
            
            [[NSNotificationCenter defaultCenter] postNotificationName: DISABLE_CHAT object:msgDic.timecreate];
            
        }else if (errCode == 500) {
            //server issue...don't delete
            msgDic.ischeck = @NO;
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        //bad network...Timeout
        if ((errCode == -1001) && !response ){
            //
        }
        
        if ([response isKindOfClass: [NSArray class] ]) {
            //avoid duplicate message
        }
        
        if ([response[@"code"] isKindOfClass: [NSNumber class]]) {
           
        }
        if([response[@"message"] isKindOfClass:[NSDictionary class]])
        {/*
            [KSToastView ks_showToast: @"[Test only] Post message successful!!!" duration:2.0f completion: ^{
            }];*/

            [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_CACHE_CHAT object:response];
            //Success -> delete from cache;
            [msgDic MR_deleteEntity];
            //notify update view...
        }else{/*
            [KSToastView ks_showToast: [NSString stringWithFormat:@"[Test only] Error: %d" ,errCode] duration:2.0f completion: ^{
            }];
               */
        }
        
        [self performSelector:@selector(syncPostingChatMessage) withObject:nil afterDelay:2];
        
    };
    
    //FIX GEAR

    NSString *strIDGroup_FIX = msgDic.idgroup;
    NSString *strGUID_FIX = msgDic.guid;

    
    //Check nil if old data...update new version...
    
    /*
    [KSToastView ks_showToast: [NSString stringWithFormat:@"[Test only] %@ -- %@", msgDic.mycontent,strGUID_FIX ] duration:2.0f completion: ^{
    }];
*/

    
    strIDGroup_FIX = CHECKSTRING(msgDic.idgroup);
    strGUID_FIX = CHECKSTRING(msgDic.guid);

    if ([msgDic.type intValue] == ISLOUNGE) {
        
        //        {"content":"test again ","create_time":"2015-12-03T16:35:52GMT+07:00"} cái này ok nhe
        
        NSString * timeStr = [NSDate convertToFranceTime:msgDic.timecreate];

        [serviceObj postLoungeMessageAction: strIDGroup_FIX  withParametersDic: @{@"content":msgDic.mycontent, @"create_time": timeStr,                                             @"guid":strGUID_FIX }];
        
    }else if ([msgDic.type intValue] == ISGROUP) {
        
        //        {"content":"test again ","create_time":"2015-12-03T16:35:52GMT+07:00"} cái này ok nhe
        
        NSString * timeStr = [NSDate convertToFranceTime:msgDic.timecreate];

        [serviceObj postMessageActionWithKind:MYGROUP mykind_id:strIDGroup_FIX withParametersDic:@{@"content":msgDic.mycontent, @"create_time": timeStr,                                             @"guid":strGUID_FIX }];
        
    }
}

-(void) postingChatMessageONLINE :(NSDictionary*)msgDic
{
    WebServiceAPI *serviceObj =[WebServiceAPI new];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode) {

        
        //if error 404...not exist obj  || SUCCESS-> remove from cache.
        
        if (errCode == 404) {

            if ([response isKindOfClass: [NSDictionary class]]) {
                if ([response[@"message"] isKindOfClass: [NSString class]]) {
                    [KSToastView ks_showToast: @"Objet non trouvé" duration:2.0f completion: ^{
                    }];
                    
                }
            }
            
            
            
        }else if (errCode == 403) {
            
        }else if (errCode == 500) {

        }
        
        //bad network...Timeout
        if ((errCode == -1001) && !response ){
            //
        }
        
        if ([response isKindOfClass: [NSArray class] ]) {
            //avoid duplicate message
        }
        
        if ([response[@"code"] isKindOfClass: [NSNumber class]]) {
            
        }
        if([response[@"message"] isKindOfClass:[NSDictionary class]])
        {/*
          [KSToastView ks_showToast: @"[Test only] Post message successful!!!" duration:2.0f completion: ^{
          }];*/
            
            [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_CACHE_CHAT object:response];

        }else{/*
               [KSToastView ks_showToast: [NSString stringWithFormat:@"[Test only] Error: %d" ,errCode] duration:2.0f completion: ^{
               }];
               */
        }
    };
    
    //FIX GEAR

    
    NSString * timeStr = [NSDate convertToFranceTime: msgDic[@"date"]];

    NSString *strIDGroup_FIX = msgDic[@"idgroup"];
    NSString *strGUID_FIX = [CommonHelper generatorString];
    
    
    //Check nil if old data...update new version...
    
    /*
     [KSToastView ks_showToast: [NSString stringWithFormat:@"[Test only] %@ -- %@", msgDic.mycontent,strGUID_FIX ] duration:2.0f completion: ^{
     }];
     */
    
    if ([msgDic[@"targetScreen"] intValue] == ISLOUNGE) {
        
        //        {"content":"test again ","create_time":"2015-12-03T16:35:52GMT+07:00"} cái này ok nhe
        
        
        [serviceObj postLoungeMessageAction: strIDGroup_FIX  withParametersDic: @{@"content":msgDic[@"message"], @"create_time": timeStr,                                             @"guid":strGUID_FIX }];
        
    }else if ([msgDic[@"targetScreen"] intValue] == ISGROUP) {
        
        //        {"content":"test again ","create_time":"2015-12-03T16:35:52GMT+07:00"} cái này ok nhe
        
        
        [serviceObj postMessageActionWithKind:MYGROUP mykind_id:strIDGroup_FIX withParametersDic:@{@"content":msgDic[@"message"], @"create_time": timeStr,                                             @"guid":strGUID_FIX }];
        
    }
}

-(void) postingDiscussionMessageONLINE :(NSDictionary*)msgDic
{

    WebServiceAPI *serviceObj =[WebServiceAPI new];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode) {

        if (errCode == 404) {
            // error 404...not exist obj
            
            if ([response isKindOfClass: [NSDictionary class]]) {
                if ([response[@"message"] isKindOfClass: [NSString class]]) {
                    [KSToastView ks_showToast: @"Objet non trouvé" duration:2.0f completion: ^{
                    }];
                    
                }
            }
            
        }else if (errCode == 500) {
            [KSToastView ks_showToast: @"Internal Server Error" duration:2.0f completion: ^{
            }];

        }
        
        //if error 404...not exist obj  || SUCCESS-> remove from cache.
        
        if (!response ||[response isKindOfClass: [NSArray class] ]) {
        }
        
        if ([response[@"code"] isKindOfClass: [NSNumber class]]) {
        }
        if([response[@"message"] isKindOfClass:[NSDictionary class]] || (errCode == 201))
        {
            [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_CACHE_CHAT object:response];
        }
    };
    
    NSArray * array = [msgDic[@"joinStr"] componentsSeparatedByString:@","];
    
    NSString * timeStr = [NSDate convertToFranceTime: msgDic[@"date"]];
    
    NSDictionary *dicPost = @{@"message":@{ @"conversationId":msgDic[@"idgroup"] ,
                                            @"content":msgDic[@"message"],
                                            @"participants":array,
                                            @"created": timeStr,
                                            @"guid": [CommonHelper generatorString]
                                            }};
    
    [serviceObj fnPOST_MESSAGE_DISCUSSION:dicPost];
}

-(void)saveCacheMur:(id)response attachment:(id)attachment
{
    NSDictionary *publicationDic =response[@"publication"];
    if (publicationDic) {
        
        NSString *valueEmoj = [publicationDic[@"content"] emo_emojiString];

        
        __weak  AppDelegate *appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        [appDelegate showPostMsgSuccess:[NSString stringWithFormat:@"Votre publication \"%@\" a été correctement publiée !", valueEmoj]];
        NSMutableArray *publicationArray= [NSMutableArray new];
        //load from cache
        NSString *strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILE_MUR_SAVE) ];
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        if (arrTmp.count > 0) {
            [publicationArray  addObjectsFromArray:arrTmp];
        }
        NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:publicationDic];
        if (attachment) {
            [dicMul setValue:attachment[@"kFileData"] forKey:@"kFileData"];
        }
        [publicationArray insertObject:dicMul atIndex:0];
        // Write array
        [publicationArray writeToFile:strPath atomically:YES];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_NEW object:publicationDic userInfo: nil];
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_UPDATING_UPLOAD_COUNT object:nil userInfo: nil];
}

#pragma mark - private methods

-(void)processParsedObjectPublication:(id)object{
    dicPublicationMsg = [NSMutableArray new];
    [self processParsedObjectPublication:object depth:0 parent:nil path:nil];
    [[ServiceHelper sharedInstance] postMediaData: tmpPhotoDicPost];
}

-(void)processParsedObjectPublication:(id)object depth:(int)depth parent:(id)parent path:(NSString*)strPath{
    
    if([object isKindOfClass:[NSDictionary class]]){
        
        for(NSString * key in [object allKeys]){
            id child = [object objectForKey:key];
            if (!strPath) {
                [self processParsedObjectPublication:child depth:depth+1 parent:object path: key];
            }else{
                [self processParsedObjectPublication:child depth:depth+1 parent:object path: [NSString stringWithFormat:@"%@[%@]",strPath,key ]];
            }
        }
    }else if([object isKindOfClass:[NSArray class]]){
        
        for(id child in object){
            
            [self processParsedObjectPublication:child depth:depth+1 parent:object path:strPath];
        }
    }
    else{
        [dicPublicationMsg addObject:@{@"path": strPath, @"value": [NSString stringWithFormat:@"%@",[object description]] }];
    }
}
#pragma mark - posing function
- (void) postMediaData:(NSDictionary *)dataDic
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    NSString *postType = [dataDic objectForKey:strPostTypeKey];
    
    NSString *fileName = [dataDic[strPostAttachmentKey] objectForKey:@"kFileName"];
    
    NSData *fileData = nil;
    
    NSString *pathFile = nil;
    if ([postType isEqualToString:strPhotoKey]) {
        pathFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:dataDic[strPostAttachmentKey][@"kFileData"]];
        UIImage *image = [UIImage imageWithContentsOfFile:pathFile];
        fileData = UIImageJPEGRepresentation(image, 0.5); //
        ASLog(@"File size is : %@",[NSByteCountFormatter stringFromByteCount:fileData.length countStyle:NSByteCountFormatterCountStyleFile]);
        
        
        
    }
   else if ([postType isEqualToString:strVideoKey])
    {
        pathFile = dataDic[strPostAttachmentKey][@"kFileData"];
        fileData = [NSData dataWithContentsOfFile:pathFile ]; //
        
    }
    
    
    //Catch try
    if (fileData == nil && ([postType isEqualToString:strPhotoKey]  || [postType isEqualToString:strVideoKey]))
    {
        //Delete this task.
        [[ServiceHelper sharedInstance] removePostingTask:dataDic];
        doingPublication = NO;
        [self performSelector:@selector(syncPostingPublication) withObject:nil afterDelay:2];
        return;
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:POST_PUBLICATION_MUR]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData
                                                       timeoutInterval:60*30];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:[NSString stringWithFormat:@"PHPSESSID=%@",[self getPHPSESSID]] forHTTPHeaderField:@"Cookie"];
    
    NSMutableData *body = [NSMutableData data];
    
    for (NSDictionary *param in dicPublicationMsg) {
        //            NSLog(@"%@ ___ %@",param[@"path"], param[@"value"]);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param[@"path"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", param[@"value"] ] dataUsingEncoding:NSUTF8StringEncoding]];
    }

    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    if (fileData) {
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"publication[media][file]\"; filename=\"%@\"\r\n", fileName]
                          dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [request addValue:[NSString stringWithFormat:@"%lu", (unsigned long)[fileData length]]  forHTTPHeaderField:@"Content-Length"];
        [body appendData:[NSData dataWithData:fileData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [request setHTTPBody:body];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                         // [progressView setProgress:uploadProgress.fractionCompleted];
                          
                          ASLog(@"%f",uploadProgress.fractionCompleted);
                          
                          float progress = uploadProgress.fractionCompleted;
                          if ([postType isEqualToString:strVideoKey]) {
                              if (progress >= 0.99) {
                                  
                                  [[ServiceHelper sharedInstance] removePostingTask:dataDic];
                                  
                                  //remove file temp
                                  NSString *pathFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:dataDic[strPostAttachmentKey][@"kFileData"]];
                                  
                                  [FileHelper removeFileAtPath:pathFile];
                                  
                                  
                                  doingPublication = NO;
                                  [self performSelector:@selector(syncPostingPublication) withObject:nil afterDelay:2];

                              }
                          }
                        //notify relative component
                          NSString *guid = dataDic[@"Data"][@"publication"][@"guid"];
                          [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_UPLOADING_PROGRESS object: @{@"id":guid?guid:@"",
                                                                                                                              @"type":postType,
                                                                                                                              @"text":  dataDic[@"Data"][@"publication"][@"content"],
                                                                                                                              
                                                                                                                              @"obj":dataDic,
                                                                                                                              @"kind":postType,
                                                                                                                              @"percentage": [NSNumber numberWithFloat:progress*100]
                                                                                                                              } userInfo: nil];
                          
                          
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                          [[ServiceHelper sharedInstance] failPostingTask:dataDic];
                          doingPublication = NO;
                          [self performSelector:@selector(syncPostingPublication) withObject:nil afterDelay:2];
                          
                      } else {
                          AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                          [app updateAllowShowAdd:responseObject];
                          
                          //server convert ok ... save id using local file...chu i nhieu case.
                          //ReLoad cache:
                          NSDictionary *attachment=nil;
                          if (dataDic[strPostAttachmentKey][@"kFileData"]) {
                              attachment = @{@"kFileName": @"image.png",
                                             @"kFileData": dataDic[strPostAttachmentKey][@"kFileData"]};
                              [self saveCacheMur:responseObject attachment:attachment];
                              
                              //remove file temp
                              NSString *pathFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:dataDic[strPostAttachmentKey][@"kFileData"]];
                              
                              [FileHelper removeFileAtPath:pathFile];
                          }else{
                              [self saveCacheMur:responseObject attachment:nil];

                          }
                          
                          [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                          [[ServiceHelper sharedInstance] removePostingTask:dataDic];


                          doingPublication = NO;
                          [self performSelector:@selector(syncPostingPublication) withObject:nil afterDelay:2];
                          
//                          [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_REFRESH_MES_NEW object:nil];
                      }
                  }];
    
    [uploadTask resume];
    
}


#pragma mark - post data
//JSON: {
//    displaytime = 0;
//    enable = 1;
//    link = "";
//    url = "/img/interstice-naturapass-concours.jpg";
//}
- (void) getIntersticeAction
{
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj getInfoInterstice];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        if (!response) {
            return ;
        }
        NSDictionary *dictionary = (NSDictionary *)response;
        
        NSString *intersticeUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[dictionary objectForKey:@"url"]];
        
//        {"enable":true,"link":"","url":"/img/slide-partenaires.jpg","displaytime":3}

        // save json data
        [UserSettingsHelper setIntersticeJsonData:dictionary];
        
        [self downloadAndSaveSecondSplash:intersticeUrl];
        
    };
}

- (void) downloadAndSaveSecondSplash:(NSString *)strUrl
{
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    
    
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response)
    {
      
        [UserSettingsHelper setFetchIntersticeTime:[NSDate currentGMTZeroTime]];
        NSString *path = [FileHelper pathForSecondSplashFile];

        return [NSURL fileURLWithPath:path];

    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        ASLog(@"File downloaded to: %@", filePath);
    }];
    [downloadTask resume];
    
    
    /*
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //        NSLog(@"Successfully downloaded file to %@", path);
        
        // save fetch time
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
    
    [operation start];
    */
}

#pragma mark - user token

- (NSString *) getPHPSESSID {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"PHPSESSID"];
}

- (NSString *) getSenderId {
    return [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
}

#pragma mark -
-(void) deleteAllPostPublication
{
    NSString *strQueueName = concatstring([COMMON getUserId],strPostPublication);
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:strQueueName];
    if ([value isKindOfClass:[NSDictionary class]]) {
        NSMutableDictionary *dicPosting = [value mutableCopy];
        NSArray *arrAllKey = [dicPosting allKeys];
        for (NSString *key in arrAllKey) {
            NSDictionary *dic = dicPosting[key];
            [self removePostingTask:dic];

        }        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
}

@end
