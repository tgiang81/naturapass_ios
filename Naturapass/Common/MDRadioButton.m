//
//  MDRadioButton.m
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import "MDRadioButton.h"
#import "Define.h"
@implementation MDRadioButton
-(instancetype)initWithFrame:(CGRect)frame
{
    self =[super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    
    view_normal = [UIView new];
    view_normal.frame = CGRectMake(0, 0, 40, 40);
    view_normal.layer.masksToBounds = YES;
    view_normal.layer.cornerRadius = 20;
    view_normal.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view_normal.layer.borderWidth = 3;
    view_normal.backgroundColor = [UIColor clearColor];
    
    view_sub_normal = [UIView new];
    view_sub_normal.frame = CGRectMake(7, 7, 26, 26);
    view_sub_normal.layer.masksToBounds = YES;
    view_sub_normal.layer.cornerRadius = 13;
    view_sub_normal.backgroundColor = [UIColor clearColor];
    
    [view_normal addSubview:view_sub_normal];

    
    view_active = [UIView new];
    view_active.frame = CGRectMake(0, 0, 40, 40);
    view_active.layer.masksToBounds = YES;
    view_active.layer.cornerRadius = 20;
    view_active.layer.borderColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR).CGColor;
    view_active.layer.borderWidth = 3;
    view_active.backgroundColor = [UIColor clearColor];
    
    view_sub_active = [UIView new];
    view_sub_active.frame = CGRectMake(7, 7, 26, 26);
    view_sub_active.layer.masksToBounds = YES;
    view_sub_active.layer.cornerRadius = 13;
    view_sub_active.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    
    [view_active addSubview:view_sub_active];
    
    UIImage *imgNormal = [self imageFromWithView:view_normal];
    UIImage *imgActive = [self imageFromWithView:view_active];
    [self setImage: imgActive forState:UIControlStateSelected];
    [self setImage: imgNormal forState:UIControlStateNormal];
    
}
-(void)setTypeButton:(BOOL)isRadio
{
    if (isRadio) {
        view_normal.layer.cornerRadius = 20;
        view_active.layer.cornerRadius = 20;
        view_sub_normal.layer.cornerRadius = 13;
        view_sub_active.layer.cornerRadius = 13;
    }
    else
    {
        view_normal.layer.cornerRadius = 10;
        view_active.layer.cornerRadius = 10;
        view_sub_normal.layer.cornerRadius = 4;
        view_sub_active.layer.cornerRadius = 4;
    }
    UIImage *imgNormal = [self imageFromWithView:view_normal];
    UIImage *imgActive = [self imageFromWithView:view_active];
    [self setImage: imgActive forState:UIControlStateSelected];
    [self setImage: imgNormal forState:UIControlStateNormal];

}
-(void)fnColorNormal:(UIColor*)colorNormal withColorActive:(UIColor*)colorActive
{
    view_normal.backgroundColor = colorNormal;
    view_active.backgroundColor = colorNormal;
    view_sub_active.backgroundColor = colorActive;
    view_active.layer.borderColor = colorActive.CGColor;
    UIImage *imgNormal = [self imageFromWithView:view_normal];
    UIImage *imgActive = [self imageFromWithView:view_active];
    [self setImage: imgActive forState:UIControlStateSelected];
    [self setImage: imgNormal forState:UIControlStateNormal];
}

-(void)fnColor:(UIColor*)colorNormal withColorBoderNormal:(UIColor*)colorBorderNormal withColorActive:(UIColor*)colorActive  withColorBoderActive:(UIColor*)colorBorderActive
{
    view_normal.backgroundColor = colorNormal;
    view_normal.layer.borderColor = colorBorderNormal.CGColor;
    view_sub_normal.backgroundColor = colorNormal;

    view_active.backgroundColor = colorNormal;
    view_active.layer.borderColor = colorBorderActive.CGColor;
    view_sub_active.backgroundColor = colorActive;
    UIImage *imgNormal = [self imageFromWithView:view_normal];
    UIImage *imgActive = [self imageFromWithView:view_active];
    [self setImage: imgActive forState:UIControlStateSelected];
    [self setImage: imgNormal forState:UIControlStateNormal];
}

- (UIImage *)imageFromWithView:(UIView*)vView
{
    UIView *view = vView;
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
-(void)addContraintSupview:(UIView*)view withSuperView:(UIView*)viewSuper
{
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeWidth
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:20]];
    [viewSuper addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeHeight
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:nil
                                                          attribute:NSLayoutAttributeNotAnAttribute
                                                         multiplier:1.0
                                                           constant:20]];
    
    [viewSuper addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeCenterX
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:viewSuper
                                                          attribute:NSLayoutAttributeCenterX
                                                         multiplier:1
                                                           constant:0]];
    [viewSuper addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                          attribute:NSLayoutAttributeCenterY
                                                          relatedBy:NSLayoutRelationEqual
                                                             toItem:viewSuper
                                                          attribute:NSLayoutAttributeCenterY
                                                         multiplier:1
                                                           constant:0]];
}

@end
