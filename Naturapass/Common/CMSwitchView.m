//
//  CMSwitchView.m
//
//  Created by Mycose on 31/03/2015.
//  Copyright (c) 2015 Mycose. All rights reserved.
//

#import "CMSwitchView.h"
#import "Define.h"

@interface UIView (CMSwitchView)
- (CGFloat)width;
- (CGFloat)semiWidth;
- (CGFloat)height;
- (CGFloat)semiHeight;
@end

@implementation UIView (CMSwitchView)
- (CGFloat)width
{
    return CGRectGetWidth(self.frame);
}

- (CGFloat)semiWidth
{
    return floorf(CGRectGetWidth(self.frame)/2.0f);
}

- (CGFloat)height
{
    return CGRectGetHeight(self.frame);
}

- (CGFloat)semiHeight
{
    return floorf(CGRectGetHeight(self.frame)/2.0f);
}
@end

@interface CMSwitchView()
@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, strong) UIView* switchView;
@property (nonatomic, strong) UIView* rectangle;


@property (nonatomic, strong) UIView* dotView;
@property (nonatomic, strong) UIImageView* dotImg;

@end

@implementation CMSwitchView

#pragma mark - LifeCycle

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    
    //Rectangle
    self.rectangle = [[UIView alloc] initWithFrame: CGRectMake(5.0f, 0.0f, [self width]-5.0f, [self height]/2) ];
    
    [self addSubview:self.rectangle];
    self.rectangle.center = CGPointMake([self width]/2, [self height]/2);
    
    
    _switchView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [self width], [self height])];
    [self addSubview:_switchView];
    _switchView.backgroundColor = [UIColor clearColor];
    
    
    
    //Circle...
    _dotView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [self height], [self height])];
    [_dotView.layer setMasksToBounds:YES];
    _dotView.layer.cornerRadius= _dotView.frame.size.height /2;
    _dotView.layer.borderWidth =0;
    
    _dotImg = [[UIImageView alloc] initWithFrame:CGRectMake([self height]/4, [self height]/4, [self height]/2, [self height]/2)];
    // add image
    [self.dotView addSubview:_dotImg];
    [_switchView addSubview:self.dotView];
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(switchClicked)];
    [_switchView addGestureRecognizer:tap];
    
//    UIPanGestureRecognizer* pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureDetected:)];
//    pan.delegate = self;
//    [_switchView addGestureRecognizer:pan];
    
    self.isSelected = NO;
    self.animDuration = 0.6;
    self.layer.masksToBounds = NO;
    
    //ON
    //    [self onSwitch];
    //    [self yesSelect];
    
    
    //OFF
    //    [self offSwitch];
    //    [self noSelect];
    
}

-(void) noSelect
{
    self.isSelected = NO;
    [self.dotView setFrame:CGRectMake([self semiHeight]-[self.dotView semiWidth], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
    
}
-(void) yesSelect
{
    self.isSelected = YES;
    [self.dotView setFrame:CGRectMake(([self width]-[self semiHeight])-[self.dotView semiHeight], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
    
}

-(void) onSwitch
{
    _dotImg.image=[UIImage imageNamed:@"ic_apply"];//close
    if (self.dotColorON) {
        _dotView.backgroundColor = self.dotColorON;
        self.rectangle.backgroundColor = self.dotColorON;
    }
    else
    {
        _dotView.backgroundColor = UIColorFromRGB(ON_SWITCH);
        self.rectangle.backgroundColor = UIColorFromRGB(ON_SWITCH);
    }
}

-(void) offSwitch
{
    _dotImg.image=[UIImage imageNamed:@"close_switch"];
    
    if (self.dotColorOFF) {
        _dotView.backgroundColor = self.dotColorOFF;
        
        self.rectangle.backgroundColor = self.dotColorOFF;
    }
    else
    {
        _dotView.backgroundColor = UIColorFromRGB(OFF_SWITCH);
        self.rectangle.backgroundColor = UIColorFromRGB(OFF_SWITCH);
    }
}
-(void)fnThemeWithColorON:(UIColor*)ColorON ColorOFF:(UIColor*)ColorOFF
{
    self.dotColorON=ColorON;
    self.dotColorOFF =ColorOFF;
}
#pragma mark - Gesture

- (void)panGestureDetected:(UIPanGestureRecognizer*)panGesture
{
    CGPoint translation = [panGesture translationInView:self.switchView];
    
    if (((CGRectGetMidX(self.dotView.frame) + translation.x) >= [self semiHeight]) &&
        ((CGRectGetMidX(self.dotView.frame) + translation.x) <= [self width] - [self semiHeight]))
        [self.dotView setFrame:CGRectMake(self.dotView.frame.origin.x + translation.x, self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
    
    if ((self.isSelected == NO && (CGRectGetMidX(self.dotView.frame) > [self semiWidth])) ||
        (self.isSelected == YES && (CGRectGetMidX(self.dotView.frame) < [self semiWidth]))) {
        self.isSelected = !self.isSelected;
        if (self.delegate)
            [self.delegate switchValueChanged:self andNewValue:self.isSelected];
    }
    
    if (panGesture.state == UIGestureRecognizerStateEnded) {
        if (self.isSelected == NO && (CGRectGetMidX(self.dotView.frame) > [self semiWidth])) {
            [self switchClicked];
        } else if (self.isSelected == NO && (CGRectGetMidX(self.dotView.frame) < [self semiWidth])) {
            
            [UIView animateWithDuration:self.animDuration animations:^{
                
                [self.dotView setFrame:CGRectMake([self semiHeight]-[self.dotView semiWidth], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
            }];
        } else if (self.isSelected == YES && (CGRectGetMidX(self.dotView.frame) < [self semiWidth])) {
            [self switchClicked];
        } else if (self.isSelected == YES && (CGRectGetMidX(self.dotView.frame) > [self semiWidth])) {
            [UIView animateWithDuration:self.animDuration animations:^{
                
                [self.dotView setFrame:CGRectMake(([self width]-[self semiHeight])-[self.dotView semiHeight], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
            }];
        }
    }
    [panGesture setTranslation:CGPointZero inView:self.switchView];
}

#pragma mark - Action

- (void)switchClicked
{
    if (self.isSelected == YES) {
        
        self.isSelected = NO;
        [UIView animateWithDuration:self.animDuration animations:^{
            [self.dotView setFrame:CGRectMake([self semiHeight]-[self.dotView semiWidth], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
            
            [self offSwitch];
            
        }];
        
    } else {
        
        self.isSelected = YES;
        [UIView animateWithDuration:self.animDuration animations:^{
            [self.dotView setFrame:CGRectMake([self width]-[self semiHeight]-[self.dotView semiWidth], self.dotView.frame.origin.y, self.dotView.frame.size.width, [self.dotView height])];
            [self onSwitch];
            
        }];
    }
    
    
    if (self.delegate)
        [self.delegate switchValueChanged:self andNewValue:self.isSelected];
}

- (void)setSelected:(BOOL)boolean animated:(BOOL)animated {
    NSTimeInterval duration = self.animDuration;
    if (animated == NO) {
        self.animDuration = 0.f;
        [self switchClicked];
        self.animDuration = duration;
    } else
        [self switchClicked];
}

@end
