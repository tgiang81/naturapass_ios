
//
//  NSDate+Extensions.m
//  demo
//
//  Created by Admin on 3/29/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "NSDate+Extensions.h"
//Dates


@implementation NSDate (NSDateForNSString)

+(NSDate *)beginningOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    
    return [cal dateFromComponents:components];
    
}

+(NSDate *)endOfDay:(NSDate *)date
{
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(  NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [cal dateFromComponents:components];
    
}

//get last day of month
+(NSDate*)lastDayOfYear:(NSDate*)fromDate
{
    NSInteger dayCount = 365;
    
    NSDateComponents *comp = [[self calendar] components:
                              NSCalendarUnitYear |
                              NSCalendarUnitMonth |
                              NSCalendarUnitDay fromDate:fromDate];
    
    [comp setDay:dayCount];
    
    return [[self calendar] dateFromComponents:comp];
}
+(NSDate*)lastDayOfMonth:(NSDate*)fromDate
{
    NSInteger dayCount = [self numberOfDaysInMonthCountFromDate:fromDate];
    
    NSDateComponents *comp = [[self calendar] components:
                              NSCalendarUnitYear |
                              NSCalendarUnitMonth |
                              NSCalendarUnitDay fromDate:fromDate];
    
    [comp setDay:dayCount];
    
    return [[self calendar] dateFromComponents:comp];
}

+(NSDate*)last30Days:(NSDate*)fromDate
{
    NSInteger dayCount = 30;
    
    NSDateComponents *comp = [[self calendar] components:
                              NSCalendarUnitYear |
                              NSCalendarUnitMonth |
                              NSCalendarUnitDay fromDate:fromDate];
    
    [comp setDay:dayCount];
    
    return [[self calendar] dateFromComponents:comp];
}


+(NSDate*)lastDayOfWeek:(NSDate*)fromDate
{
    NSInteger dayCount = 7;
    
    NSDateComponents *comp = [[self calendar] components:
                              NSCalendarUnitYear |
                              NSCalendarUnitMonth |
                              NSCalendarUnitDay fromDate:fromDate];
    
    [comp setDay:dayCount];
    
    return [[self calendar] dateFromComponents:comp];
}
+(NSInteger)numberOfDaysInMonthCountFromDate:(NSDate*)fromDate
{
    NSRange dayRange = [[self calendar] rangeOfUnit:NSCalendarUnitDay
                                             inUnit:NSCalendarUnitMonth
                                            forDate:fromDate];
    
    return dayRange.length;
}

+(NSCalendar*)calendar
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneWithName: stringUTCDateTimeFormat]];
    return calendar;
}

+(NSDate*)convertDateToDateGMT2 :(NSDate*) dateLocal
{
    NSDate *date= dateLocal;
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:stringUTCDateTimeFormat];

    NSString *date_Local = [formatter_local stringFromDate:date];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [formatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [formatter setDateFormat:stringUTCDateTimeFormat];
    NSDate *dateGMT2 = [formatter dateFromString:date_Local];
    //[formatter release];
    return dateGMT2;
}
+(NSComparisonResult)compareLocalDateWithInput:(NSString*)stringDate
{
    //Convert date local -> date Paris
    NSDate *date= [NSDate date];
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    
    NSString *date_Local = [formatter_local stringFromDate:date];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [formatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    NSDate *toDay = [formatter dateFromString:date_Local];
    //Convert date input -> date Paris
    NSDate* dateInput = [NSDate convertNSStringDateToNSDateX1: stringDate];
    return [toDay compare:dateInput];
}
//Main date function
+ (NSDate*)dateFor:(DateType) dateType {
    NSCalendar *cal = [NSCalendar currentCalendar];
    [cal setTimeZone: [NSTimeZone systemTimeZone]];
    NSDateComponents *components = [cal components:( NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate:[ NSDate new]];
    
    [components setHour:-[components hour]];
    [components setMinute:-[components minute]];
    [components setSecond:-[components second]];
    NSDate *today = [cal dateByAddingComponents:components toDate:[[NSDate alloc] init] options:0]; //This variable should now be pointing at a date object that is the start of today (midnight);
    //
    [components setHour:-23];
    [components setMinute:-59];
    [components setSecond:-59];
    NSDate *yesterday = [cal dateByAddingComponents:components toDate: today options:0];
    //this week
    components = [cal components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[ NSDate new]];
    //
    NSInteger dayofweek = [[[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:today] weekday];// this will give you current day of week
    [components setDay:([components day] - ((dayofweek) - 2))];//// for beginning of the week.
    
    NSDate *thisWeek  = [cal dateFromComponents:components];
    //last week
    [components setDay:([components day] - 7)];
    NSDate *lastWeek  = [cal dateFromComponents:components];
    //
    
    [components setDay:1];//fist day of month
    
    NSDate *thisMonth = [cal dateFromComponents:components];
    //
    [components setMonth:([components month] - 1)];
    NSDate *lastMonth = [cal dateFromComponents:components];
    //
    components = [cal components:NSCalendarUnitYear fromDate:[ NSDate new]];
    
    [components setYear:([components year])];
    NSDate *thisYear = [cal dateFromComponents:components];
    //
    [components setYear:([components year] - 1)];
    NSDate *lastYear = [cal dateFromComponents:components];
    //
    
    //last 30 days
    components = [cal components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[ NSDate new]];
    [components setDay:([components day] - 30)];
    
    NSDate *last30  = [cal dateFromComponents:components];
    
    
    
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    
//    [formatter setTimeZone: [NSTimeZone systemTimeZone]];
//    
//    [formatter setDateFormat:stringUTCDateTimeFormat];
//    NSString *strDate = [formatter stringFromDate:today];
//    NSDate *date_curent = [formatter dateFromString:strDate];
//    NSLog(@"today=%@",date_curent);
    NSLog(@"yesterday=%@",yesterday);
    
    NSLog(@"thisWeek=%@",thisWeek);
    NSLog(@"lastWeek=%@",lastWeek);
    
    NSLog(@"thisMonth=%@",thisMonth);
    NSLog(@"lastMonth=%@",lastMonth);
    
    NSLog(@"thisYear=%@",thisYear);
    NSLog(@"lastYear=%@",lastYear);
    switch (dateType) {
        case DateToday:
        {
            return [self convertDateToDateGMT2:today];
        }
            break;
        case DateYesterday:
        {
            
            return [self convertDateToDateGMT2:yesterday];
        }
            break;
        case DateThisWeek:
        {
            
            return [self convertDateToDateGMT2:thisWeek];

        }
            break;
        case DateLastWeek:
        {
            return [self convertDateToDateGMT2:lastWeek];
        }
            break;
        case DateThisMonth:
        {
            return [self convertDateToDateGMT2:thisMonth];
        }
            break;
        case DateLastMonth:
        {
            return [self convertDateToDateGMT2:lastMonth];
        }
            break;
        case DateThisYear:
        {
            return [self convertDateToDateGMT2:thisYear];
        }
            break;
        case DateLastYear:
        {
            return [self convertDateToDateGMT2:lastYear];
        }
            break;

        case DateLast30:
        {
            return [self convertDateToDateGMT2:last30];
        }
            break;
            
    }
    
//    [components setMonth:([components month] - 1)];
//    NSDate *testMonth = [cal dateFromComponents:components];
//    
//    //get start day_time of month
//    
//    NSDate *end_date = [self lastDayOfMonth:testMonth];
//    
//    //get end day_time of month
//    
//    NSTimeInterval ti = [end_date timeIntervalSince1970];
//    
//    
//    NSLog(@"test 3 month =%f",ti);
//    


    return nil;
}

+(NSTimeInterval)beginTodayToTimeInterval:(NSDate*)date
{
    NSDate *begin_today = [self beginningOfDay:date];
    NSTimeInterval ti = [begin_today timeIntervalSince1970];
    
    return ti;
}
+(NSTimeInterval)endTodayToTimeInterval:(NSDate*)date
{
    NSDate *end_today = [self endOfDay:date];
    NSTimeInterval ti = [end_today timeIntervalSince1970];
    
    return ti;
}
//today
+(NSTimeInterval)getBeginToday
{
    NSDate *date = [NSDate dateFor: DateToday];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndToday
{
    NSDate *date = [NSDate dateFor: DateToday];
    return [self endTodayToTimeInterval:date];
}
//yesterday
+(NSTimeInterval)getBeginYesterday
{
    NSDate *date = [NSDate dateFor: DateYesterday];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndYesterday
{
    NSDate *date = [NSDate dateFor: DateYesterday];
    return [self endTodayToTimeInterval:date];
}
//this Week
+(NSTimeInterval)getBeginThisWeek
{
    NSDate *date = [NSDate dateFor: DateThisWeek];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndThisWeek
{
    NSDate *date = [self lastDayOfWeek:[NSDate dateFor: DateThisWeek]];
    return [self endTodayToTimeInterval:date];
}
//last week
+(NSTimeInterval)getBeginLastWeek
{
    NSDate *date = [NSDate dateFor: DateLastWeek];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndLastWeek
{
    NSDate *date = [self lastDayOfWeek:[NSDate dateFor: DateLastWeek]];
    return [self endTodayToTimeInterval:date];
}

//last 30 days
+(NSTimeInterval)getBeginLast30
{
    NSDate *date = [NSDate dateFor: DateLast30];
    return [self beginTodayToTimeInterval:date];
}

+(NSTimeInterval)getEndLast30
{
    NSDate *date = [NSDate dateFor: DateToday];
    return [self endTodayToTimeInterval:date];
}

//this month
+(NSTimeInterval)getBeginThisMonth
{
    NSDate *date = [NSDate dateFor: DateThisMonth];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndThisMonth
{
    NSDate *date = [self lastDayOfMonth:[NSDate dateFor: DateThisMonth]];
    return [self endTodayToTimeInterval:date];
}
//last month
+(NSTimeInterval)getBeginLastMonth
{
    NSDate *date = [NSDate dateFor: DateLastMonth];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndLastMonth
{
    NSDate *date = [self lastDayOfMonth:[NSDate dateFor: DateLastMonth]];
    return [self endTodayToTimeInterval:date];
}
//last this year

+(NSTimeInterval)getBeginThisYear
{
    NSDate *date = [NSDate dateFor: DateThisYear];
    return [self beginTodayToTimeInterval:date];
}
+(NSTimeInterval)getEndThisYear
{
    NSDate *date = [self lastDayOfYear:[NSDate dateFor: DateThisYear]];
    return [self endTodayToTimeInterval:date];
}
+(NSTimeInterval)getBeginLastYear
{
    NSDate *date = [NSDate dateFor: DateLastYear];

    return [self beginTodayToTimeInterval:date];

}
+(NSTimeInterval)getEndLastYear
{
    NSDate *date = [self lastDayOfYear:[NSDate dateFor: DateLastYear]];

    return [self endTodayToTimeInterval:date];

}
+(NSTimeInterval)getBeginWithStringDate:(NSString*)dateString
{
    NSDate *date = [self convertNSStringDateToNSDate:dateString];
    
    return [self beginTodayToTimeInterval:date];
    
}
+(NSTimeInterval)getEndWithStringDate:(NSString*)dateString
{
    NSDate *date = [self convertNSStringDateToNSDate:dateString];
    
    return [self endTodayToTimeInterval:date];
    
}
+(NSDate *)convertNSStringDateToNSDatePicker:(NSString *)string
{
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:stringDatePicker];
    NSDate *convertedDate = [dateFormatter dateFromString:dateString];
    //[dateFormatter release];
    return convertedDate;
}


+(NSDate *)convertNSStringDateToNSDate:(NSString *)string 
{
	//- check parameter validity
	if( string == nil )
		return nil;
	
	//- process request
	NSString *dateString = string;
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	// this is imporant - we set our input date format to match our input string
	// if format doesn't match you'll get nil from your string, so be careful
	[dateFormatter setDateFormat:stringUTCDateTimeFormat];
	NSDate *convertedDate = [dateFormatter dateFromString:dateString];
	//[dateFormatter release];
	return convertedDate;
}

+(NSString *)convertTimeIntervalToNSTring:(NSTimeInterval )interval
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [dateFormatter setDateFormat: @"dd/MM/yyyy HH:mm"];
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    return convertedDate;
}
+(NSString *)convertDateGmt2ToSystem:(NSString *)string withInputFormat:(NSString *)inputFormat withOupuFormat:(NSString*)outputFormat
{
    
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    
    
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    
    [inputDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [inputDateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [inputDateFormatter setDateFormat: inputFormat];
    NSDate *date = [inputDateFormatter dateFromString:dateString];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone: [NSTimeZone systemTimeZone]];
    
    [dateFormatter setDateFormat: outputFormat];
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    return convertedDate;
}
+(NSString *)convertAgendaToNSTring:(NSString *)string
{
    
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    
    
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    
    [inputDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [inputDateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [inputDateFormatter setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    NSDate *date = [inputDateFormatter dateFromString:dateString];

    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [dateFormatter setDateFormat: @"dd/MM/yyyy à HH:mm"];
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    return convertedDate;
}
+(NSString *)convertTimeInterval:(NSTimeInterval )interval withFormat:(NSString*)strFormat
{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    if (strFormat.length> 0) {
        [dateFormatter setDateFormat:strFormat];
    }
    else
    {
        [dateFormatter setDateFormat: @"dd/MM/yyyy HH:mm"];
    }
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    return convertedDate;
}
+(NSDate *)convertNSStringDateToNSDateX1:(NSString *)string
{
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    
    //	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    //	[dateFormatter setDateFormat:stringUTCDateTimeFormat];
    NSDate *convertedDate = [dateFormatter dateFromString:dateString];
    //[dateFormatter release];
    return convertedDate;
}



+(NSString*)convertNSDateToNSString:(NSDate*)date
{
	//- check parameter validity
	if( date == nil )
		return nil;
	
	//- process request
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:stringUTCDateTimeFormat];
	NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
	//[dateFormatter release];
	return strDate;
}

+(int)numberOfDaysFromDate:(NSDate*)fromDate ToDate:(NSDate*)toDate
{
    //dates needed to be reset to represent only yyyy-mm-dd to get correct number of days between two days.
    unsigned int unitFlags = NSDayCalendarUnit;
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:fromDate  toDate:toDate options:0];
    int days = [comps day];
	
    //[gregorian release];
    return days;
}

+(NSString*)generalDateFromUTCDate:(NSString*)utcDate
{
	NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
	[formatter setDateFormat:stringUTCDateFormat];
	NSDate *date=[formatter dateFromString:utcDate];
	if(date==nil)
		return nil;
	[formatter setDateFormat:stringGeneralFullDateFormat];
	NSString *resultDate=[formatter stringFromDate:date];	
	//[formatter release];
	return resultDate;
}

+(NSString*)UTCDateFromGeneralDate:(NSString*)generalDate
{
	NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
	[formatter setDateFormat:stringGeneralFullDateFormat];
	NSDate *date=[formatter dateFromString:generalDate];
	NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
	[formatter setLocale:usLocale];
	[formatter setDateFormat:stringUTCDateFormat];	
	NSString *resultDate=[formatter stringFromDate:date];
    //[usLocale release];
	//[formatter release];
	
	return resultDate;
}

+(NSString*)currentGMTZeroTime
{
	NSDate *sourceDate = [NSDate date];
    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMT];
	sourceDate= [sourceDate dateByAddingTimeInterval:(currentGMTOffset*-1)];
	
	NSDateFormatter *formater=[[NSDateFormatter alloc] init];
	[formater setDateFormat:stringUTCDateTimeFormat];
	NSString *sourceDateStr=[formater stringFromDate:sourceDate];
	//[formater release];
	
	return sourceDateStr;
}

+(NSString *)currentRFC822TimeYear :(NSString*)dInput{
    //@"Sun, 06 Nov 1994 08:49:37 GMT";
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:stringRFC822DateTimeFormatYear];
    
//    "dt_txt" = "2015-12-04 00:00:00";

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *d1 = [df dateFromString:dInput];

    return [dateFormatter stringFromDate:d1];
}

+(NSString *)currentRFC822TimeNoYear:(NSString*)dInput{
    //@"Sun, 06 Nov 1994 08:49:37 GMT";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier:@"fr_FR"]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:stringRFC822DateTimeFormatNoYear];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *d1 = [df dateFromString:dInput];
    
    return [dateFormatter stringFromDate:d1];
}

+(NSDate*)GMTZeroDateForString:(NSString*)utcDateString
{
	NSDate *date=[self convertNSStringDateToNSDate:utcDateString];
	NSTimeZone *localTimeZone=[NSTimeZone localTimeZone];
	date=[date dateByAddingTimeInterval:[localTimeZone secondsFromGMT]];
	return date;
}

+(NSString*)currentStandardUTCTime
{
	NSDate *date=[NSDate date];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
//    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* currentTimeZone = [NSTimeZone localTimeZone];
//    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMT];
    [formatter setTimeZone:currentTimeZone];
	NSString *strDate = [formatter stringFromDate:date];
	//[formatter release];
	return strDate;
}
+(NSString*)convertDateHeaderAgenda:(NSString*)inputDate 
{
    if(inputDate.length == 0) return nil;
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSDate *date = [dateFormatter dateFromString:inputDate];
    [dateFormatter setDateFormat:@"dd/MM/yy"];
    NSString *contStr=[dateFormatter stringFromDate:date];
    //[dateFormatter release];
    return contStr;
}
+(NSString*)currentStandardUTCTimeGMTPLUS :(NSString*) strGMT_2
{
    NSDate *date=[NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [formatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [formatter setDateFormat:stringGMT_G_Format];
    //    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:strGMT_2];
    //    [formatter setTimeZone:gmt];
    NSString *strDate = [formatter stringFromDate:date];
    //[formatter release];
    return strDate;
}

+(NSString*)convertToFranceTime :(NSDate*) inDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [formatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [formatter setDateFormat:stringGMT_G_Format];
    //    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:strGMT_2];
    //    [formatter setTimeZone:gmt];
    NSString *strDate = [formatter stringFromDate:inDate];
    //[formatter release];
    return strDate;
}



+(NSString*)convertToStandardUTCTimeGMTPLUS :(NSString*) strGMT_2 withDate:(NSDate*)myDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:stringGMT_G_Format];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:strGMT_2];
    [formatter setTimeZone:gmt];
    NSString *strDate = [formatter stringFromDate:myDate];
    //[formatter release];
    return strDate;
}


+(NSString*)currentLocalUTCTime
{
	NSDate *date=[NSDate date];
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:stringUTCDateTimeFormat];
	NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
	[formatter setLocale:usLocale];
	NSString *strDate = [formatter stringFromDate:date];
	//[formatter release];
    //[usLocale release];
	return strDate;
}

+(NSString*)generalDatefromUTCMonDayFormat:(NSString *)utcDate
{
	NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
	[formatter setDateFormat:stringUTCMonthDayFormat];
	NSDate *date1=[formatter dateFromString:utcDate];
	[formatter setDateFormat:stringGeneralMonthDayFormat];
	NSString* datestr=[formatter stringFromDate:date1];
	//[formatter release];
	return datestr;
}

+(NSString*)UTCDatefromGeneralMonDayFormat:(NSString *)generalDate
{
	NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:stringGeneralMonthDayFormat];
	NSDate *date = [dateFormatter dateFromString:generalDate];
	[dateFormatter setDateFormat:stringUTCMonthDayFormat];
	NSString *contStr=[dateFormatter stringFromDate:date];
	//[dateFormatter release];
	return contStr;
}

+ (NSString *) localTimeFromStandardUTCTime:(NSString *)timeStr
{
    if (timeStr == nil) {
        timeStr = [NSDate currentStandardUTCTime];
    }
    
    //UTC time
    NSDateFormatter *utcDateFormatter = [[NSDateFormatter alloc] init];
    [utcDateFormatter setDateFormat:stringUTCDateTimeFormat];
    [utcDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: 0]];
    
    // utc format
    NSDate *dateInUTC = [utcDateFormatter dateFromString: timeStr];
    
    // offset second
    NSInteger seconds = [[NSTimeZone systemTimeZone] secondsFromGMT];
    
    // format it and send
    NSDateFormatter *localDateFormatter = [[NSDateFormatter alloc] init];
    [localDateFormatter setDateFormat:stringUTCDateTimeFormat];
    [localDateFormatter setTimeZone :[NSTimeZone timeZoneForSecondsFromGMT: seconds]];
    
    // formatted string
    NSString *localDate = [localDateFormatter stringFromDate: dateInUTC];
    
    return localDate;
}

#pragma mark - Time ago

-(NSString *)timeAgo
{
    NSDate *now = [NSDate date];
    double deltaSeconds = fabs([self timeIntervalSinceDate:now]) - 5*60 - 20;
    //    double deltaSeconds = fabs([self timeIntervalSinceDate:now]);
    double deltaMinutes = deltaSeconds / 60.0f;
    if(deltaSeconds < 5) {
        return @"A l'instant";
    } else if(deltaSeconds < 60) {
        return [NSString stringWithFormat:NSLocalizedString(@"%d seconds ago.", @""), (int)deltaSeconds];
    } else if(deltaSeconds < 120) {
        return @"Il y a 1 minute";
    } else if (deltaMinutes < 60) {
        return [NSString stringWithFormat:@"Il y a %d minutes", (int)deltaMinutes];
    } else if (deltaMinutes < 120) {
        return @"Il y a 1 heure";
    } else if (deltaMinutes < (24 * 60)) {
        return [NSString stringWithFormat:@"Il y a %d heures", (int)floor(deltaMinutes/60)];
    } else if (deltaMinutes < (24 * 60 * 2)) {
        return @"Hier";
    } else if (deltaMinutes < (24 * 60 * 7)) {
        return [NSString stringWithFormat:@"Il y a %d jours", (int)floor(deltaMinutes/(60 * 24))];
    } else if (deltaMinutes < (24 * 60 * 31)) {
        return [NSString stringWithFormat:@"Il y a %d semaine", (int)floor(deltaMinutes/(60 * 24 * 7))];
    } else if (deltaMinutes < (24 * 60 * 61)) {
        return @"Il y a 1 mois";
    } else if (deltaMinutes < (24 * 60 * 365.25)) {
        return [NSString stringWithFormat:@"Il y a %d mois", (int)floor(deltaMinutes/(60 * 24 * 30))];
    } else if (deltaMinutes < (24 * 60 * 731)) {
        return @"l'année dernière.";
    }
    return [NSString stringWithFormat: @"Il y a %d ans" , (int)floor(deltaMinutes/(60 * 24 * 365))];
}

#pragma mark - 

@end
