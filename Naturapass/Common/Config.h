//
//  Config.h
//
//  Created by ocsdeveloper3 on 11/07/13.
//  Copyright (c) 2013 Appsolute. All rights reserved.
//

/*
 strMessageUnicode
 socket trả về ok là vì nhận đc thế nào thì gửi trả lại như thế đó.ko qua db
 db mình lưu utf8 nên truyền vào raw emoji thì nó tự discard
 */
/////////////////////////**************** Colors
#define COLOR_LOGIN_TEXTFIELDS                         [UIColor colorWithRed: (140.f/255.f) green: (187.f/255.f) blue: (25.f/255.f) alpha: 1]
#define COLOR_SALON_TAB_UNSELECT                       [UIColor colorWithRed: (64.f/255.f) green: (64.f/255.f) blue: (64.f/255.f) alpha: 1]
#define COLOR_SALON_TAB_SELECT                         [UIColor colorWithRed: (140.f/255.f) green: (187.f/255.f) blue: (25.f/255.f) alpha: 1]
/////////////////////////**************** Colors

///////////////***************Custom Fonts
#define FONT_HELVETICANEUE_MEDIUM(FontSize)            [UIFont fontWithName:@"HelveticaNeue-Medium" size:FontSize]
#define FONT_HELVETICANEUE(FontSize)                   [UIFont fontWithName:@"HelveticaNeue" size:FontSize]
#define FONT_HELVETICANEUE_LIGHT(FontSize)             [UIFont fontWithName:@"HelveticaNeue-Light" size:FontSize]
#define FONT_HELVETICANEUE_BOLD(FontSize)            [UIFont fontWithName:@"HelveticaNeue-Bold" size:FontSize]
#define FONT_HELVETICANEUE_Italic(FontSize)            [UIFont fontWithName:@"HelveticaNeue-Italic" size:FontSize]

///////////////***************Custom Fonts

//////////////******* IPHONE5 Screen frame
//#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define IS_IPHONE5 (CGRectGetHeight([[UIScreen mainScreen] bounds]) >= 568)

//////////////******* IPHONE5 Screen frame

//////////////******* IOS VERSION Check
#define IS_GREATER_THAN_OR_EQUAL_IOS7  (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)?YES:NO)
//////////////******* IOS VERSION Check


/////**************** SHA1-Encryption **********************/////////////

#define SHA1_KEY                                    @"W881xNaXSaPbcdOtrRTaM5ZNCp90qc3ti104uLMF"

/////**************** SHA1-Encryption **********************/////////////

#define LIKETAG             1000
#define UNLIKETAG           2000
#define COMMENTBUTTONTAG    3000
#define LOCATIONBUTTONTAG   4000
#define INFOBUTTONTAG       5000
#define SETTINGBUTTONTAG    6000
#define ALLERBUTTONTAG      4001


#define PUBLICATION_RESPONSE    0
#define LIKE_RESPONSE           1
#define UNLIKE_RESPONSE         2
#define COMMENT_RESPONSE        3
#define PUBLICATION_OFFSET_RESPONSE     4
#define SEARCH_COUNT                    5
#define FRIENDS_COUNT                   6
#define SIGNAL_COUNT                    7
#define DELETEACTION                    8
#define SIGNAL_RESPONSE                 9
#define GETGROUPRESPONSE                10
#define POSTDEVICE                      11
#define SLIDETYPE                       12
#define GETGROUPASKS                    13
#define PUT_GROUP_USER_JOIN             14
#define PUT_GROUP_USER_JOIN_ALL         15

#define heightHeader  60
#define heightFooter  90

#define widthCell  231

#define widthFooterCell  80
#define heightFooterCell  80

#define heightPopOut  30
#define heightImageInPop  18

#define heightImageLogo  62
#define widthBrand  60

#define widthCollectionWithHeader 80
#define widthCollectionNoHeader 70

//groupes
#define GROUPS_ACTION_RESPONSE    0
#define GROUPS_ACTION_OFFSET_RESPONSE   1
#define GROUPS_ACTION_DELETE   2
#define GROUPS_ACTION_UNSUBSCIBE   3
#define groups_limit                       @"10"


/*Log flurry
 [Flurry logEvent:@"MurFragment" timed:YES];
 [Flurry logEvent:@"PostDetaiFragment" timed:YES];
 [Flurry logEvent:@"MesGroupFragment" timed:YES];
 [Flurry logEvent:@"carte.MapFragment" timed:YES];
 [Flurry logEvent:@"FriendMurFragment" timed:YES];
 [Flurry logEvent:@"discussion.DiscussionFragment" timed:YES];
 [Flurry logEvent:@"MesChasseFragment" timed:YES];
 [Flurry logEvent:@"AmisSearchFragment" timed:YES];
 [Flurry logEvent:@"MyProfileListFragment" timed:YES];
 [Flurry logEvent:@"AmisTagFragment" timed:YES];
 [Flurry logEvent:@"livecarte.HMapFragment" timed:YES];
 */


/*
 file info
 search ggtt in code.
 change Naturapass in localization string file.
 */
//must change ggtt
#ifdef DEBUG

#define  FLURRY_API_KEY  @""

#else

#define  FLURRY_API_KEY  @"JD5VB5V6WDTCTYJKC28V" //Naturapass

#endif

#define GOOGLE_PLACE_API @"AIzaSyBZyk1C4BckzqzvwjUiAoZTQxN_2TFu3N0"
#define schemeName @"Naturapass"
#define fbID @"fb178089399496278"
#define APP_ID_BUNDLE 883300083

//http://naturapass.phs.lan/app_dev.php/publications?limit=20&sharing=3&offset=0
static NSString *const kHNKDemoGooglePlacesAutocompleteApiKey = @"AIzaSyC21pAQ94efbsY_uNpUjej9PjmHJcCAsnk";

#define messageLoungeID                 @"1"
#define publication_limit               @"20"
#define REFRESH_HEADER_HEIGHT           64.0f
#define OffsetLimit                     @"0"
#define loadedCount                     @"0"
#define lounge_limit                    @"20"
#define loungeList_Limit                @"10"
#define mur_limit                       @"7"
#define group_limit                     @"30"
#define discustion_limit                @"10"
#define AppId                           @"1485047658419576"
#define App_Secret_Key                  @"5c6adb4a0d52e2bd4c4c20645ad2bf35"

#define MAX_DISTRIBUTION 500
#define ID_EVENT_SPECIAL @"516"
#define KEY_IGN @"zww19qlvqtv7w5r3m0u2rvo3"
//old key ign: zww19qlvqtv7w5r3m0u2rvo3
//
#define Appdel ((AppDelegate *)[UIApplication sharedApplication].delegate)
//

//**********************  WEB SERVICES  **********************************//

//ROOTURL (Base API)//////////////////////////////////////////////////////////////////////////////////////
////PROD
//https://github.com/mrcflorian/login-screen-swift
//remember reset for each apps.

#define SPECIAL_USER_ID 10454


//FCM push
//bundle fr.e-conception.naturadev ->rename file GoogleService-Info-DEV
//bundle fr.e-conception.naturapass ->rename file GoogleService-Info-PROD



//#define BASE_API                                        @"https://www.naturapass.com/api"
//#define IMAGE_ROOT_API                                  @"https://www.naturapass.com"
//#define LINK_SOCKET                                     @"https://www.naturapass.com"

#define BASE_API                                        @"https://naturapass.e-conception.fr/api"
#define IMAGE_ROOT_API                                  @"https://naturapass.e-conception.fr"
#define LINK_SOCKET                                      @"https://naturapass.e-conception.fr"



//Define shortcut tree path for CARTE publication making
//default
#define kPath_POSTE @"112/122"
#define kPath_MIRADOR_AFF @"112/121"
#define kPath_MIRADOR_BAT @"112/185"
#define kPath_CABANE @"112/177"
#define kPath_PIEGES @"112/586"
#define kPath_EQUIPEMENTS @"112"
#define kPath_HABITATS @"603"
#define kPath_ANIMAUX @"104"

#define pathANIMAL_TUE_OTHERS @"104/106"
#define pathANIMAL_TUE_SANGLIER @"104/106/350/144"
#define pathANIMAL_TUE_CHEVREUIL @"104/106/350/145"
#define pathANIMAL_TUE_RENARD @"104/106/350/148"
#define pathANIMAL_TUE_CERF @"104/106/350/146"
#define pathANIMAL_TUE_FAISAN @"104/106/352/149"
#define pathANIMAL_TUE_LIEVRE @"104/106/350/147"
#define pathANIMAL_TUE_COLVERT @"104/106/352/363"
#define pathANIMAL_TUE_PERDRIX @"104/106/352/151"
#define pathANIMAL_TUE_BECASSE @"104/106/352/617"
#define pathANIMAL_TUE_PIGEON @"104/106/352/143"
#define pathANIMAL_TUE_LAPIN @"104/106/350/150"
#define pathANIMAL_TUE_CORBEAU @"104/106/352/364"
#define pathANIMAL_TUE_CORNEILLE @"104/106/352/365"
#define pathANIMAL_TUE_CHAMOIS @"104/106/350/608"


#define pathANIMAL_VU_OTHERS @"104/111"
#define pathANIMAL_VU_SANGLIER @"104/111/319/135"
#define pathANIMAL_VU_CHEVREUIL @"104/111/319/136"
#define pathANIMAL_VU_RENARD @"104/111/319/139"
#define pathANIMAL_VU_CERF @"104/111/319/137"
#define pathANIMAL_VU_FAISAN @"104/111/320/377"
#define pathANIMAL_VU_LIEVRE @"104/111/319/138"
#define pathANIMAL_VU_COLVERT @"104/111/320/321"
#define pathANIMAL_VU_PERDRIX @"104/111/320/141"
#define pathANIMAL_VU_BECASSE @"104/111/320/381"
#define pathANIMAL_VU_PIGEON @"104/111/320/173"
#define pathANIMAL_VU_LAPIN @"104/111/319/860"
#define pathANIMAL_VU_CORBEAU @"104/111/320/861"
#define pathANIMAL_VU_CORNEILLE @"104/111/320/862"
#define pathANIMAL_VU_CHAMOIS @"104/111/319/376"


#define pathANIMAL_LOUPE_OTHERS @"104/105"
#define pathANIMAL_LOUPE_SANGLIER @"104/105/372/124"
#define pathANIMAL_LOUPE_CHEVREUIL @"104/105/372/125"
#define pathANIMAL_LOUPE_RENARD @"104/105/372/279"
#define pathANIMAL_LOUPE_CERF @"104/105/372/126"
#define pathANIMAL_LOUPE_FAISAN @"104/105/380/371"
#define pathANIMAL_LOUPE_LIEVRE @"104/105/372/127"
#define pathANIMAL_LOUPE_COLVERT @"104/105/380/864"
#define pathANIMAL_LOUPE_PERDRIX @"104/105/380/130"
#define pathANIMAL_LOUPE_BECASSE @"104/105/380/132"
#define pathANIMAL_LOUPE_PIGEON @"104/105/380/133"
#define pathANIMAL_LOUPE_LAPIN @"104/105/372/128"
#define pathANIMAL_LOUPE_CORBEAU @"104/105/380/865"
#define pathANIMAL_LOUPE_CORNEILLE @"104/105/380/866"
#define pathANIMAL_LOUPE_CHAMOIS @"104/105/372/863"



#define pathANIMAL_BLESSE_OTHERS @"104/107"
#define pathANIMAL_BLESSE_SANGLIER @"104/107/153"
#define pathANIMAL_BLESSE_CERF @"104/107/155"
#define pathANIMAL_BLESSE_CHEVREUIL @"104/107/154"
#define pathANIMAL_BLESSE_RENARD @"104/107/858"
#define pathANIMAL_BLESSE_CHIEN @"104/107/859"



#define pathANIMAL_TRACES_OTHERS @"104/623"
#define pathANIMAL_TRACES_SANGLIER @"104/623/628"
#define pathANIMAL_TRACES_CERF @"104/623/626"
#define pathANIMAL_TRACES_CHEVREUIL @"104/623/627"
#define pathANIMAL_TRACES_CHAMOIS @"104/623/625"
#define pathANIMAL_TRACES_BECASSE @"104/623/624"

#define pathJE_SUIS_ICI @"618"
#define pathSOS @"867"


#define kDatabaseFileName @"NaturaDB.db"

#define MYGROUP     @"groups"
#define MYCHASSE    @"lounges"
#define MYPUBLIC    @"publications"
#define ONE_GROUP     @"group"
#define ONE_CHASSE    @"lounge"
#define ONE_PUBLIC    @"publication"


#define LOGIN_API(email,password,deviceId,identifier)                   [NSString stringWithFormat:@"%@/user/login?email=%@&password=%@&device=%@&identifier=%@&name=iphone",BASE_API,email,password,deviceId,identifier]

#define REAL_LOGIN(email, password, pushToken)            [NSString stringWithFormat: @"%@/user/login?email=%@&password=%@&device=ios&identifier=%@&name=iOS&authorized=1", BASE_API, email, password, pushToken]

//445:      * GET /user/login?facebookid=100006772984423
//446:      *                  &device=[ios|android]
//447:      *                  &identifier=e0366ffd02fdd942a960119e71fce654c2ccf5
//448:      *                  &authorized=1
//449:      *                  &name=[iphone|ipod|ipad|...]
#define FB_LOGIN_API(facbook_id,identifier)                   [NSString stringWithFormat:@"%@/user/login?facebookid=%@&device=ios&identifier=%@&authorized=1&name=iphone",BASE_API,facbook_id,identifier]

#define REAL_LOGIN_NO_PUSH(email, password)            [NSString stringWithFormat: @"%@/user/login?email=%@&password=%@&device=ios&identifier=(null)&name=iOS&authorized=0", BASE_API, email, password]

#define REGISTER_API                                [NSString stringWithFormat:@"%@/users",BASE_API]

//http://naturapass-test.phs.lan/app_dev.php/api/v2/publications?limit=3&offset=0&reset=0&sharing=0

// GET /v2/publications?groups[]=8&groups[]=3&limit=20&sharing=4&offset=20


#define GETPUBLICATION(limit,share,offset, strGroupAppend)          [NSString stringWithFormat:@"%@/v2/publications?limit=%@%@&offset=%@%@",BASE_API,limit,share,offset,strGroupAppend]
#define GET_PUBLICATION_PRIVATE(limit,offset,strFilter)          [NSString stringWithFormat:@"%@/v2/publicationprivate?limit=%@&offset=%@%@",BASE_API,limit,offset,strFilter]

#define GET_PUBLICATION_PUBLIC(limit,offset,strFilter)  [NSString stringWithFormat:@"%@/v2/publicationpublic?limit=%@&offset=%@%@",BASE_API,limit,offset,strFilter]

//MON MUR: https://naturapass.e-conception.fr/api/v2/publicationprivate?limit=5&offset=0
//LE MUR: https://naturapass.e-conception.fr/api/v2/publicationpublic?limit=5&offset=0&filter=abc&startTime=&endTime=
#define GETPUBLICATIONITEM(publication)          [NSString stringWithFormat:@"%@/v2/publications/%@",BASE_API,publication]

//https://naturapass.e-conception.fr/api/v2/publications/213/user?limit=3&offset=0
#define GETPUBLICATIONUSER(user_id,limit,offset)    [NSString stringWithFormat:@"%@/v2/publications/%@/user?limit=%@&offset=%@",BASE_API,user_id,limit,offset]

//#define LIKE_API(publication_id)                    [NSString stringWithFormat:@"%@/publications/%@/like",BASE_API,publication_id]

#define UNLIKE_API(publication_id)                  [NSString stringWithFormat:@"%@/publications/%@/unlike",BASE_API,publication_id]

#define DELETEPUBLICATION_API(publication_id)       [NSString stringWithFormat:@"%@/publications/%@/action",BASE_API,publication_id]
///publications/{comment}/comment/action
#define DELETE_COMMENT_PUBLICATION_API(comment_id)       [NSString stringWithFormat:@"%@/publications/%@/comment",BASE_API,comment_id]

#define DELETEPUBLICATIONACTION_API(publication_id)       [NSString stringWithFormat:@"%@/publications/%@",BASE_API,publication_id]

//203:      * GET /v2/publication/{publication_id}/comments?limit=20&loaded=5

#define GETCOMMENT_API(publication_id)              [NSString stringWithFormat:@"%@/v2/publications/%@/comments?limit=%@&loaded=%@",BASE_API,publication_id,publication_limit,loadedCount]



#define POSTCOMMENT_API(publication_id)             [NSString stringWithFormat:@"%@/v2/publications/%@/comments",BASE_API,publication_id]

#define PUTCOMMENTLIKE_API(comment_id)              [NSString stringWithFormat:@"%@/publications/%@/comment/like",BASE_API,comment_id]

#define PUTCOMMENTUNLIKE_API(comment_id)            [NSString stringWithFormat:@"%@/publications/%@/comment/unlike",BASE_API,comment_id]

#define DELETECOMMENT_API(comment_id)               [NSString stringWithFormat:@"%@/publications/%@/comment/action",BASE_API,comment_id]

#define POSTPUBLICATION_API                         [NSString stringWithFormat:@"%@/publications",BASE_API]

#define LOUNGES_API(limit,offset,query)                           [NSString stringWithFormat:@"%@/lounges?limit=%@&offset=%@%@",BASE_API,limit,offset,query]

#define LOUNGESEARCH_LIMIT_API(searchtext,limit,offset)     [NSString stringWithFormat:@"%@/lounges/%@/search?limit=%@&offset=%@",BASE_API,searchtext,limit,offset]

#define LOUNGESEARCH_API(searchtext)                                [NSString stringWithFormat:@"%@/lounges/%@/search",BASE_API,searchtext]

#define POSTLOUNGEJOINACTION_API(userid,loungeid)                   [NSString stringWithFormat:@"%@/lounges/%@/joins/%@",BASE_API,userid,loungeid]

#define POSTLOUNGEINVITEEMAILACTION(loungeID)                    [NSString stringWithFormat: @"%@/lounges/%@/invites/mails", BASE_API, loungeID]

// ???

#define GETLOUNGEMESSAGEACTION(lounge_id,lounge_limit,loadedCount)                           [NSString stringWithFormat:@"%@/lounges/%@/messages?limit=%@&loaded=%@",BASE_API,lounge_id,lounge_limit,loadedCount]
#define GETLOUNGEMESSAGEOFFSET(lounge_id,discustion_limit,offset)                           [NSString stringWithFormat:@"%@/lounges/%@/message/offset?limit=%@&offset=%@",BASE_API,lounge_id,discustion_limit,offset]

//#define GETLOUNGESUBSCRIBER_API(lounge_id)                              [NSString stringWithFormat:@"%@/lounges/%@/subscribers",BASE_API,lounge_id]

#define GETLOUNGESUBSCRIBERFRIEND_API(lounge_id)                              [NSString stringWithFormat:@"%@/lounges/%@/subscribers/friend",BASE_API,lounge_id]
#define PUTLOUNGEGEOLOCATION_API(lounge_id,user_id)                 [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/geolocation",BASE_API,lounge_id,user_id]

#define GETLOUNGESUBSCRIBERGEOLOCATION_API(lounge_id, identifier)                   [NSString stringWithFormat:@"%@/lounges/%@/subscribers/geolocations?identifier=%@",BASE_API,lounge_id, identifier]

#define GETUSERFRIENDSACTION_API(user_id)                               [NSString stringWithFormat:@"%@/users/%@/friends",BASE_API,user_id]

#define GETUSERLOUNGESACTION_API(limit,offset)                          [NSString stringWithFormat:@"%@/lounges/owning?limit=%@&offset=%@",BASE_API,limit,offset]

#define GETUSERGROUPSACTION_API(user_id)                                [NSString stringWithFormat:@"%@/users/%@/groups",BASE_API,user_id]

#define DELETELOUNGEACTION(lounge_id)                                   [NSString stringWithFormat:@"%@/lounges/%@",BASE_API,lounge_id]

#define DELETEGEOLOCATIONLOUNGEACTION(lounge_id, push_identifier)    [NSString stringWithFormat: @"%@/lounges/%@/map?identifier=%@", BASE_API, lounge_id, push_identifier]

#define POSTLOUNGEINVITEGROUPACTION_API(lounge_id,group_id)             [NSString stringWithFormat:@"%@/lounges/%@/invites/%@/groups",BASE_API,lounge_id,group_id]

#define DELETELOUNGEJOINACTION(user_id,lounge_id)                       [NSString stringWithFormat:@"%@/lounges/%@/joinnews/%@",BASE_API,user_id,lounge_id]

#define PUTLOUNGESUSCRIBERPARTICIPATIONACTION(loungeid,subscribe_id)    [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/participation",BASE_API,loungeid,subscribe_id]

#define POSTLOUNGESMESSAGEACTION_API(lounge_id)                         [NSString stringWithFormat:@"%@/lounges/%@/messages",BASE_API,lounge_id]

#define PUTLOUNGESUSCRIBERQUIETACTION(lounge_id,suscriber_id)           [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/quiet",BASE_API,lounge_id,suscriber_id]

#define PUTLOUNGESUSCRIBERGEOLOCATIONACTION(lounge_id,suscriber_id)     [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/geolocation",BASE_API,lounge_id,suscriber_id]

#define GETUSERACTION_API(user_id)                                      [NSString stringWithFormat:@"%@/v2/users/%@",BASE_API,user_id]

#define PUTUSERACTION_API                                               [NSString stringWithFormat:@"%@/users",BASE_API]
#define PUT_CHANGE_PASSWORD_API                                               [NSString stringWithFormat:@"%@/v2/users/changepasswords",BASE_API]

#define POSTUSERPROFILEPICTURE                                          [NSString stringWithFormat:@"%@/users/profiles/pictures",BASE_API]


//https://naturapass.e-conception.fr/app_dev.php/api/users/manh-manhmanh/profile



#define GETUSERPARAMETERACTION_API(user_id)                             [NSString stringWithFormat:@"%@/users/%@/parameters",BASE_API,user_id]

#define PUTUSERPARAMETERACTION_API                                      [NSString stringWithFormat:@"%@/users/parameters",BASE_API]


#define GET_USER_PARAM                                                  [NSString stringWithFormat:@"%@/users/parameters/sharing",BASE_API]



#define PUTLOUNGEGEOLOCATIONACTION_ADMIN_API(lounge_id,geolocation)     [NSString stringWithFormat:@"%@/lounges/%@/geolocations/%@",BASE_API,lounge_id,geolocation]

#define GETLOUNGE_API(lounge_id)                                        [NSString stringWithFormat:@"%@/lounges/%@",BASE_API,lounge_id]

#define PUTLOUNGESUSCRIBERADMIN_API(lounge_id,suscriber_id)             [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/admin",BASE_API,lounge_id,suscriber_id]

#define POSTLOUNGEACTION_API                                            [NSString stringWithFormat:@"%@/v2/huntnews",BASE_API]

#define PUTLOUNGEACTION_API(lounge_id)                                  [NSString stringWithFormat:@"%@/lounges/%@",BASE_API, lounge_id]

#define POSTUSERGEOLOCATIONACTION_API(user_id)                          [NSString stringWithFormat:@"%@/users/%@/geolocations",BASE_API,user_id]

#define POSTUPDATEUSERGEOLOCACATION_API                                 [NSString stringWithFormat:@"%@/users/geolocations",BASE_API]

#define GETUSERSEACHACTION_API(searchtext)                              [NSString stringWithFormat:@"%@/users/search?q=%@",BASE_API,searchtext]

#define POSTUSERFRIENDSHIPASKACTION_API(user_id)                        [NSString stringWithFormat:@"%@/users/%@/friendships/asks",BASE_API,user_id]
//https://naturapass.e-conception.fr/app_dev.php/api/v2/hunts/499/subscribers/180/participationcomment
#define PUT_LOUNGE_SUSCRIBER_PARTICIPATIONCOMMENT_API(lounge_id,suscriber_id)   [NSString stringWithFormat:@"%@/v2/hunts/%@/subscribers/%@/participationcomment",BASE_API,lounge_id,suscriber_id]

#define PUTLOUNGESUSCRIBERPUBLICCOMMENTACTION_API(lounge_id,suscriber_id)   [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/publiccomment",BASE_API,lounge_id,suscriber_id]
//99:      * PUT /v2/hunts/{HUNT_ID}/notmembers/{NOT_MEMBER_ID}/participationcomment
#define PUTLOUNGE_SUSCRIBER_PUBLICCOMMENT_NONMEMBER(lounge_id,suscriber_id)   [NSString stringWithFormat:@"%@/v2/hunts/%@/notmembers/%@/participationcomment",BASE_API,lounge_id,suscriber_id]

//https://www.naturapass.com/api/v1/lounges/313/notmembers/119/participation
#define PUTLOUNGE_SUSCRIBER_NONMEMBERS_PARTICIPATION(lounge_id,suscriber_id)   [NSString stringWithFormat:@"%@/v1/lounges/%@/notmembers/%@/participation",BASE_API,lounge_id,suscriber_id]

#define PUTLOUNGESUSCRIBERPRIVATECOMMENTACTION_API(lounge_id,suscriber_id)  [NSString stringWithFormat:@"%@/lounges/%@/subscribers/%@/privatecomment",BASE_API,lounge_id,suscriber_id]

#define GETMEDIASMAPACTION_API(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset,sharing,group_id)                                   [NSString stringWithFormat:@"%@/publications/map?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=%@&sharing=%@&group=%@",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset,sharing,group_id]

#define POSTPUBLICATIONSIGNALACTION_API(publicationID)                      [NSString stringWithFormat:@"%@/publications/%@/signals",BASE_API,publicationID]

#define GETLOUNGEASKACTION_API(lounge_id)                                   [NSString stringWithFormat:@"%@/lounges/%@/asks",BASE_API,lounge_id]

#define POSTUSERFRIENDSHIP_API(lounge_id,user_id)                           [NSString stringWithFormat:@"%@/lounges/%@/invites/%@/users",BASE_API,lounge_id,user_id]

#define GETUSERWAITINGACTION_API                                            [NSString stringWithFormat:@"%@/users/waiting",BASE_API]

#define USERFRIENDSHIPCONFIRMACTION_API(user_id)                            [NSString stringWithFormat:@"%@/v2/users/%@/friendship",BASE_API,user_id]

#define GETUSERNOTIFICATIONACTION_API                                       [NSString stringWithFormat:@"%@/v2/notifications",BASE_API]
//v2/notification/all/status?limit=30&offset=0
#define GET_USER_NOTIFICATION_STATUS_API(limit,offset)                                       [NSString stringWithFormat:@"%@/v2/notification/all/status?limit=%@&offset=%@",BASE_API,limit,offset]

#define PUTUSERREADNOTIFICATIONACTION_API(notification_id)                  [NSString stringWithFormat:@"%@/users/%@/read/notification",BASE_API,notification_id]

//249:      * PUT /v2/notifications/readall
#define PUT_USER_READ_ALL_NOTIFICATION                  [NSString stringWithFormat:@"%@/v2/notifications/readall",BASE_API]

#define POSTUSERDEVICEACTION(type,identifier)                               [NSString stringWithFormat:@"%@/users/%@/devices/%@",BASE_API,type,identifier]

#define DELETEUSERDEVICEACTION(type,identifier)                             [NSString stringWithFormat:@"%@/users/%@/devices/%@",BASE_API,type,identifier]

#define GETPUSHNOTIFICATIONPUBLICATIONID(object_id)                         [NSString stringWithFormat:@"%@/publications/%@",BASE_API,object_id]

#define GETFACEBOOKUSERACTION_API(facebook_id)                              [NSString stringWithFormat:@"%@/facebook/user?fid=%@",BASE_API,facebook_id]

#define POSTFACEBOOKUSERACTION_API                                          [NSString stringWithFormat:@"%@/facebooks/users",BASE_API]

#define PUTFACEBOOKUSERACTION_API                                          [NSString stringWithFormat:@"%@/facebook/email",BASE_API]



#define PUTUSERRESETPASSWORDACTION_API                                      [NSString stringWithFormat:@"%@/user/reset/password",BASE_API]

//#define POST_INVITATION_EMAIL_API                                           [NSString stringWithFormat:@"%@/users/invitations",BASE_API]
#define GET_GROUP_SEARCH(name,offset)                                           [NSString stringWithFormat:@"%@/groups?limit=20&offset=%@&filter=%@",BASE_API,offset,name]



#define GET_MES_GROUP_SEARCH(name,offset)                                           [NSString stringWithFormat:@"%@/groups/owning?limit=20&offset=%@&filter=%@",BASE_API,offset,name]


#define GET_DISTRIBUTOR_FOR_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset) [NSString stringWithFormat:@"%@/admin/distributor/map?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=%d",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset]
//https://naturapass.e-conception.fr/api/v2/shapes?swLat=45.87938425471556&swLng=5.189093561071786&neLat=45.892886757450654&neLng=5.261191339392099&sharing=3&reset=1
#define GET_SHAPE_FOR_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,sharing,reset) [NSString stringWithFormat:@"%@/v2/shares?swLat=%@&swLng=%@&neLat=%@&neLng=%@&sharing=%@&reset=%D",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,sharing,reset]
#define POST_LOUNGES_PHOTO(lounge_id) [NSString stringWithFormat:@"%@/lounges/%@/photos",BASE_API,lounge_id]


// Groups action
///groups/owning?limit=10&offset=0&filter=test

// POST /groups/{group}/invites/mails

#define GROUP_INVITE_BY_EMAIL(group_id)                    [NSString stringWithFormat: @"%@/groups/%@/invites/mails", BASE_API, group_id]

#define GROUP_MAILABLE(mykind,group_id, is_mailable)               [NSString stringWithFormat:@"%@/%@/%@/subscribers/%d/mailable",BASE_API,mykind,group_id, is_mailable]


#define GET_MY_GROUPSACTION_API(filter,limit,offset)                        [NSString stringWithFormat:@"%@/groups/owning?limit=%@&offset=%@&filter=%@",BASE_API,limit,offset,filter]
#define POST_CREATE_GROUP_API                                               [NSString stringWithFormat:@"%@/v2/groups",BASE_API]

//New Group api to support Notification setting parameters.
#define POST_CREATE_GROUP_API_WITH_NOTIFICATION                             [NSString stringWithFormat:@"%@/v2/groupnews",BASE_API]

#define DELETE_GROUP_API(group_id)                                          [NSString stringWithFormat:@"%@/groups/%@",BASE_API,group_id]

#define DELETE_USER_GROUP_API(user_id,group_id)                             [NSString stringWithFormat:@"%@/groups/%@/joinnews/%@",BASE_API,user_id,group_id]

//GET /groups?limit=10&offset=0&filter=test
#define GROUP_TOUTES_API(limit,offset)                           [NSString stringWithFormat:@"%@/groups?limit=%@&offset=%@",BASE_API,limit,offset]

//#define GETGROUPSUBSCRIBER_API(group_id)                              [NSString stringWithFormat:@"%@/groups/%@/subscribers?all=1",BASE_API,group_id]
#define GETGROUPSUBSCRIBERFRIEND_API(group_id)                              [NSString stringWithFormat:@"%@/groups/%@/subscribers/friend",BASE_API,group_id]
#define GET_PUBLICATION_LIKES_API(publication_id)                              [NSString stringWithFormat:@"%@/v2/publications/%@/likes",BASE_API,publication_id]
#define POST_GROUP_JOIN_ACTION_API(userid,group_id)                   [NSString stringWithFormat:@"%@/groups/%@/joins/%@",BASE_API,userid,group_id]
#define PUT_GROUP_UPDATE_ITEM(group_id)                      [NSString stringWithFormat:@"%@/groups/%@",BASE_API,group_id]
//#define POSTUSER_GROUP_ASKACTION_API(user_id)                        [NSString stringWithFormat:@"%@/users/%@/friendships/asks",BASE_API,user_id]photosphotos
//manhld
#define GET_GROUPS_ASKS(group_id)   [NSString stringWithFormat:@"%@/groups/%@/asks",BASE_API,group_id]
#define PUT_GROUPS_SUBCRIBERS(group_id,subcribers_id)   [NSString stringWithFormat:@"%@/groups/%@/subscribers/%@/admin",BASE_API,group_id,subcribers_id]
#define GET_GROUP_API(group_id)                                          [NSString stringWithFormat:@"%@/groups/%@",BASE_API,group_id]
//548:      * POST /groups/{group}/photos

#define POST_GROUP_PHOTO(group_id) [NSString stringWithFormat:@"%@/groups/%@/photos",BASE_API,group_id]
// A user invites a friend to subscribe to a group
#define GROUP_INVITE_FRIEND_NATURAPASS(group_id,friend_id)                           [NSString stringWithFormat:@"%@/groups/%@/invites/%@/users",BASE_API,group_id,friend_id]

//582:      * POST /groups/{group}/invites/{group}/groups
#define GROUP_INVITE_GROUPE(group_id,other_group_id)                           [NSString stringWithFormat:@"%@/groups/%@/invites/%@/groups",BASE_API,group_id,other_group_id]



//- from version 1.2.5
#define GET_INTERSTICE_API                                                  [NSString stringWithFormat:@"%@/users/interstice",BASE_API]
#define GET_GROUP_ACTION(group_id)                                                  [NSString stringWithFormat:@"%@/groups/%@",BASE_API,group_id]


#define RegisterConnect                                     @"RegConnect"
#define POST_INVITATION_MAIL [NSString stringWithFormat:@"%@/%@",BASE_API,@"users/invitation"]
#define POST_INVITATION_EMAIL_API   [NSString stringWithFormat:@"%@/users/invitations",BASE_API]
//**********************  WEB SERVICES  **********************************//

#pragma mark - V2 API

#define Get_Publication(mykind,myid,limit,offset)                    [NSString stringWithFormat:@"%@/v2/%@/%@/publications?limit=%@&offset=%@",BASE_API,mykind,myid,limit,offset]


//#define Get_Item_Publication(mykind,myid)                                          [NSString stringWithFormat:@"%@/%@/%@",BASE_API,mykind,myid]
#define GET_SEARCH_API(mykind,searchtext,limit,offset)                                [NSString stringWithFormat:@"%@/%@/%@/search?limit=%@&offset=%@",BASE_API,mykind,searchtext,limit,offset]

#define GET_ITEM_API(mykind,myid)                                        [NSString stringWithFormat:@"%@/%@/%@",BASE_API,mykind,myid]

#define GET_USER_PARAMETERS_FRIEND [NSString stringWithFormat:@"%@/v2/user/parameters/friends",BASE_API]

#define GET_ASKS_KIND(mykind,mykind_id)   [NSString stringWithFormat:@"%@/%@/%@/asks",BASE_API,mykind,mykind_id]



#define GET_PUBLICATION_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,group_id)                                   [NSString stringWithFormat:@"%@/v2/publication/map?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=1%@",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,group_id]
//#define GET_PUBLICATION_FILTEROFF(limit,offset,strGroupAppend)  [NSString stringWithFormat:@"%@/v2/publication/filteroff?limit=%@&offset=%@%@",BASE_API,limit,offset,strGroupAppend]

#define GET_PUBLICATION_FILTEROFF(limit,offset,strGroupAppend)  [NSString stringWithFormat:@"%@/v2/publications?sharing=3&limit=%@&offset=%@%@",BASE_API,limit,offset,strGroupAppend]



#define GET_PUBLICATION_MAP_FILTEROFF(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,strCategories)                                   [NSString stringWithFormat:@"%@/v2/publication/map/mobile/filteroff?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=1%@",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,strCategories]
#define GET_GROUP_AGENDA(groups_id) [NSString stringWithFormat:@"%@/v2/groups/%@",BASE_API,groups_id]

#define GET_ALL_HUNTS_ADMIN [NSString stringWithFormat:@"%@/v2/users/hunts/admin",BASE_API]
#define GET_ALL_GROUPS_ADMIN [NSString stringWithFormat:@"%@/v2/users/groups/admin",BASE_API]

#define GET_USER_MYKIND(mykind,user_id)                                [NSString stringWithFormat:@"%@/users/%@/%@",BASE_API,user_id,mykind]
#define GET_LOUNGE_SUBCRIBER_NOTMEMBER(lounge_id) [NSString stringWithFormat:@"%@/lounges/%@/subscribersnotmember",BASE_API,lounge_id]

//157:      * GET /v2/user/allconversations?limit=5&offset=0&limit_hunt=5&offset_hunt=0&limit_group=5&offset_group=0
#define GET_ALL_CONVERSATIONS(limit,offset,limit_hunt,offset_hunt,limit_group,offset_group) [NSString stringWithFormat:@"%@/v2/user/allconversations?limit=%@&offset=%@&limit_hunt=%@&offset_hunt=%@&limit_group=%@&offset_group=%@",BASE_API,limit,offset,limit_hunt,offset_hunt,limit_group,offset_group]

//130:      * GET /v2/user/groupconversations?limit=5&offset=0
#define GET_GROUP_CONVERSATIONS(limit,offset) [NSString stringWithFormat:@"%@/v2/user/groupconversations?limit=%@&offset=%@",BASE_API,limit,offset]

//103:      * GET /v2/user/huntconversations?limit=5&offset=0
#define GET_LOUNGE_CONVERSATIONS(limit,offset) [NSString stringWithFormat:@"%@/v2/user/huntconversations?limit=%@&offset=%@",BASE_API,limit,offset]

//37:      * GET /v2/user/conversations?limit=5&offset=0
#define GET_DISCUSSTION_CONVERSATIONS(limit,offset) [NSString stringWithFormat:@"%@/v2/user/conversations?limit=%@&offset=%@",BASE_API,limit,offset]
// GET api/v2/publicationcolor
#define GET_PUBLICATION_COLOR [NSString stringWithFormat:@"%@/v2/publicationcolor",BASE_API]
//v2/hunts/791/publication/discussion?limit=20&offset=0
#define GET_LIVE_HUNT_PUBLICATION_DISCUSSION(hunt_id,limit,offset) [NSString stringWithFormat:@"%@/v2/hunts/%@/publication/discussion?limit=%@&offset=%@",BASE_API,hunt_id,limit,offset]

#define GET_INFO_LOCATION(lat,lng) [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=false",lat, lng]
#define GET_INFO_WEATHER_MAP(lat,lng,appid) [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/forecast?lat=%@&lon=%@&appid=%@&units=metric&lang=fr",lat, lng,appid]
#define GET_INFO_WIND_MAP(lat,lng,appid) [NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=%@",lat, lng,appid]
#define GET_USER_LOCK                               [NSString stringWithFormat:@"%@/v2/user/lock",BASE_API]
#define POST_USER_LOCK                               [NSString stringWithFormat:@"%@/v2/users/locks",BASE_API]
#define DELETE_USER_LOCK(user_id)                               [NSString stringWithFormat:@"%@/v2/users/%@/lock",BASE_API,user_id]

#define POST_GROUP_AGENDA(groups_id) [NSString stringWithFormat:@"%@/v2/groups/%@/hunts",BASE_API,groups_id]
#define POST_LOUNGE_NOTMEMBER(lounge_id) [NSString stringWithFormat:@"%@/v1/lounges/%@/notmembers",BASE_API,lounge_id]
#define POST_PUBLICATION_FAVORITES [NSString stringWithFormat:@"%@/v2/publications/favorites",BASE_API]

#define PUT_USER_PARAMETERS_FRIEND [NSString stringWithFormat:@"%@/v2/user/parameters/friends",BASE_API]

#define PUT_JOIN_KIND_NEW(mykind,group_id,user_id)                      [NSString stringWithFormat:@"%@/%@/%@/users/%@/joinnew",BASE_API,mykind,group_id,user_id]


#define PUT_COMMENT_PUBLICATION(comment_id)                      [NSString stringWithFormat:@"%@/v2/publications/%@/comment",BASE_API,comment_id]
#define DELETE_JOIN_KIND(mykind,user_id,mykind_id)                             [NSString stringWithFormat:@"%@/%@/%@/joins/%@",BASE_API,mykind,user_id,mykind_id]

#define DELETE_LOUNGE_NOTMEMBER(lounge_id,nonmembers_id) [NSString stringWithFormat:@"%@/lounges/%@/notmembers/%@",BASE_API,lounge_id,nonmembers_id]

//https://www.naturapass.com/api/v1/lounges/3530/joins/313
#define DELETE_LOUNGE_SUBCRIBER_JOINS(lounge_id,member_id) [NSString stringWithFormat:@"%@/v1/lounges/%@/joins/%@",BASE_API,member_id,lounge_id]
//https://naturapass.e-conception.fr/api/v2/users/184/friendship
#define POST_USER_FRIENDSHIP_API(user_id)                        [NSString stringWithFormat:@"%@/v2/users/%@/friendships",BASE_API,user_id]
#define DELETE_USER_FRIENDSHIP_API(user_id)                        [NSString stringWithFormat:@"%@/v2/users/%@/friendship",BASE_API,user_id]
#define DELETE_GENERAL_CHAT_MESSAGE(msg_id)                        [NSString stringWithFormat:@"%@/chats/%@/message",BASE_API,msg_id]
#pragma mark - vois chien

#define GET_PROFILE_VOIS_CHIENS(limit,offset)                                [NSString stringWithFormat:@"%@/v2/user/profile/dogs?limit=%@&offset=%@",BASE_API,limit,offset]

#define GET_PROFILE_VOIS_CHIENS_ITEM(dog_id)                                [NSString stringWithFormat:@"%@/v2/users/%@/profile/dog",BASE_API,dog_id]

#define GET_PROFILE_VOIS_CHIENS_TYPE(limit,offset)                                [NSString stringWithFormat:@"%@/v2/user/profile/dogs/type?limit=%@&offset=%@",BASE_API,limit,offset]

#define GET_PROFILE_VOIS_CHIENS_BREED(limit,offset)                                [NSString stringWithFormat:@"%@/v2/user/profile/dogs/breed?limit=%@&offset=%@",BASE_API,limit,offset]

#define POST_PROFILE_VOIS_CHIENS                        [NSString stringWithFormat:@"%@/v2/users/profiles/dogs",BASE_API]

#define PUT_PROFILE_VOIS_CHIENS(dog_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/dog",BASE_API,dog_id]
#define DELETE_PROFILE_VOIS_CHIENS(dog_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/dog",BASE_API,dog_id]
#define DELETE_PROFILE_VOIS_CHIENS_MEDIA(media_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/dog/media",BASE_API,media_id]
#pragma mark - vois weapons

#define GET_PROFILE_WITH_KIND(kind,limit,offset)[NSString stringWithFormat:@"%@/v2/user/profile/%@?limit=%@&offset=%@",BASE_API,kind,limit,offset]

#define GET_PROFILE_ITEM_WITH_KIND(kind,kind_id,limit,offset)[NSString stringWithFormat:@"%@/v2/users/%@/profile/%@?limit=%@&offset=%@",BASE_API,kind,kind_id,limit,offset]

#define GET_PROFILE_WEAPONS_CALIBRES(limit,offset)[NSString stringWithFormat:@"%@/v2/user/profile/weapons/calibre?limit=%@&offset=%@",BASE_API,limit,offset]

#define GET_PROFILE_WEAPONS_BRANDS(limit,offset)[NSString stringWithFormat:@"%@/v2/user/profile/weapons/brand?limit=%@&offset=%@",BASE_API,limit,offset]

#define POST_PROFILE_WITH_KIND(kind) [NSString stringWithFormat:@"%@/v2/users/profiles/%@",BASE_API,kind]

#define PUT_PROFILE_WITH_KIND(kind,kind_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/%@",BASE_API,kind_id,kind]
#define POST_PROFILE_MEDIA_WITH_KIND(kind,media_id) [NSString stringWithFormat:@"%@/v2/users/%@/profiles/%@/media",BASE_API,media_id,kind]

#define DELETE_PROFILE_WITH_KIND(kind,kind_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/%@",BASE_API,kind_id,kind]
#define DELETE_PROFILE_MEDIA_WITH_KIND(kind,media_id) [NSString stringWithFormat:@"%@/v2/users/%@/profile/%@/media",BASE_API,media_id,kind]

//after created a new profile dog...uploading files
#define POST_PROFILE_FILES_WITH_KIND(media_id) [NSString stringWithFormat:@"%@/v2/users/%@/profiles/addpicdogs/media",BASE_API,media_id]
#define POST_PROFILE_FILES_WITH_WEAPON_MEDIA(media_id) [NSString stringWithFormat:@"%@/v2/users/%@/profiles/addpicweapons/media",BASE_API,media_id]


#pragma mark - mykind

#define GET_MESSAGE_ACTION(mykind,mykind_id,limit,loaded,previous)                           [NSString stringWithFormat:@"%@/v2/%@/%@/messages?limit=%@&loaded=%@&previous=%@",BASE_API,mykind,mykind_id,limit,loaded,previous]

#define GET_MESSAGE_OFFSET_ACTION(mykind,mykind_id,limit,offset)                           [NSString stringWithFormat:@"%@/v2/%@/%@/message/offset?limit=%@&offset=%@",BASE_API,mykind,mykind_id,limit,offset]

#define GET_CATEGORIES                  [NSString stringWithFormat:@"%@/v2/categories/mobile",BASE_API]

#define GET_TOCACHESYSTEM                  [NSString stringWithFormat:@"%@/v2/categories/tocachesystem",BASE_API]
#define GET_CATEGORIES_GEOLOCATED(strLat,strLng)                  [NSString stringWithFormat:@"%@/v2/categories/geolocated?lat=%@&lng=%@",BASE_API,strLat,strLng]

#define GET_CATEGORIES_BY_RECEIVER(strReceiver)                  [NSString stringWithFormat:@"%@/v2/categories/tocachesystem/withoutanimals?%@",BASE_API,strReceiver]


#define GET_SEARCH_ANIMAL         [NSString stringWithFormat:@"%@/v2/publication/observation/animals?limit=10000&offset=0",BASE_API]
#define GETGROUPSUBSCRIBER_API(group_id)                              [NSString stringWithFormat:@"%@/v2/groups/%@/subscribers",BASE_API,group_id]
#define GETLOUNGESUBSCRIBER_API(lounge_id)                              [NSString stringWithFormat:@"%@/v2/hunts/%@/subscribers",BASE_API,lounge_id]
#define GET_FRIEND_RECOMMENDATION(limit)              [NSString stringWithFormat:@"%@/graph/friends/recommendations?limit=%@",BASE_API,limit]
#define GET_LOUNGES_OLD_OWNER(limit,offset)                           [NSString stringWithFormat:@"%@/lounges/old/owning?limit=%@&offset=%@",BASE_API,limit,offset]
//post
#define POSTCOMMENTLIKE_API(comment_id)              [NSString stringWithFormat:@"%@/v2/publications/%@/comments/likes",BASE_API,comment_id]

#define POST_PUBLICATION_MUR          [NSString stringWithFormat:@"%@/v2/publications/observations/sharings",BASE_API]

#define POST_MESSAGE_ACTION(mykind,mykind_id)                         [NSString stringWithFormat:@"%@/v2/%@/%@/messages",BASE_API,mykind,mykind_id]

//delete
#define DELETE_JOIN_KIND(mykind,user_id,mykind_id)                             [NSString stringWithFormat:@"%@/%@/%@/joins/%@",BASE_API,mykind,user_id,mykind_id]

#define DELETE_MESSAGE_ACTION(mykind,mykind_id)                         [NSString stringWithFormat:@"%@/v2/%@/%@/message",BASE_API,mykind,mykind_id]
//put
#define LIKE_API(publication_id)                    [NSString stringWithFormat:@"%@/v2/publications/%@/likes",BASE_API,publication_id]

#define PUT_USER_PARAMETERS_FRIEND [NSString stringWithFormat:@"%@/v2/user/parameters/friends",BASE_API]

#define PUT_ADMIN_SUBCRIBERS(mykind,group_id,subcribers_id)   [NSString stringWithFormat:@"%@/%@/%@/subscribers/%@/admin",BASE_API,mykind,group_id,subcribers_id]

#define GET_FRIEND_RECOMMENDATION(limit)              [NSString stringWithFormat:@"%@/graph/friends/recommendations?limit=%@",BASE_API,limit]

#define PUT_EMAIL_OPTION(email_option, value)   [NSString stringWithFormat:@"%@/users/%@/emails/%@/wanted",BASE_API,email_option, value]
#define PUT_SMARTPHONE_OPTION(smartphone_option, value)   [NSString stringWithFormat:@"%@/users/%@/notifications/%@/wanted",BASE_API,smartphone_option, value]
#define PUT_GROUP_SMARTPHONE_OPTION(smartphone_option, value,group_id)   [NSString stringWithFormat:@"%@/users/%@/notifications/%@/wanteds/%@/object",BASE_API,smartphone_option, value,group_id]
//INPROGRESS
#define PUT_EMAIL_NOTI(group_type, value)   [NSString stringWithFormat:@"%@/users/%@/emails/%@/wanted",BASE_API,group_type, value]
#define PUT_NOTIFICATION_NOTI(group_type, value)   [NSString stringWithFormat:@"%@/users/%@/emails/%@/wanted",BASE_API,group_type, value]


//setting get email/notification
#define GET_NOTIFICATION_GLOBAL_PARAMETERS                    [NSString stringWithFormat:@"http://www.mocky.io/v2/589c33412500001a02805528"]
#define GET_EMAIL_PARAMETERS                    [NSString stringWithFormat:@"%@/v2/user/profile/email/parametersbygroup",BASE_API]
#define GET_NOTI_PARAMETERS                     [NSString stringWithFormat:@"%@/v2/user/profile/notification/parametersbygroup",BASE_API]
#define GET_GROUPS_PARAMETERS_MOBILE(mykind,group_id)                     [NSString stringWithFormat:@"%@/v2/%@/%@/parameters/user",BASE_API,mykind,group_id]

//setting put notification
#define GET_USER_ADDRESS  [NSString stringWithFormat:@"%@/v1/user/addresses",BASE_API]

#define POST_USER_ADDRESS  [NSString stringWithFormat:@"%@/v1/users/addresses",BASE_API]

#define DELETE_USER_ADDRESS(my_id)  [NSString stringWithFormat:@"%@/v1/users/%@/address",BASE_API, my_id]

#define GET_MUR_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,ishareing,strGroupsHuntsCategories) [NSString stringWithFormat:@"%@/v2/publication/map?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=1&sharing=%@%@",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,ishareing,strGroupsHuntsCategories]
#define PUT_USER_IDENTIFIER                     [NSString stringWithFormat:@"%@/v1/user/identifier",BASE_API]



/*
 Retourne les amis d'un utilisateur
 30:      *
 31:      * GET /v2/users/{user}/friends?mutual=1
 */

//mutual = 0 => Mes amis
//Mutual = 1 => Amis en commun

#define GET_MY_FRIENDS(my_id,bIS)  [NSString stringWithFormat:@"%@/v2/users/%@/friends?mutual=%d",BASE_API, my_id, bIS]

/*
 https://naturapass.e-conception.fr/api/v2/users/42/friendships
 */
#define POST_ADD_FRIENDSHIP(the_id)  [NSString stringWithFormat:@"%@/v2/users/%@/friendships",BASE_API, the_id]
/*
 * EN : Connects one or more users to a group
 942:      *
 943:      * POST /api/groups/{group}/joins/multiples
 944:      *
 945:      * JSON DATA:
 946:      * {"groupe": {"subscribers": [1,3,10]}}
 */


#define POST_GROUP_MULTIPLE_JOIN(group_id)  [NSString stringWithFormat:@"%@/groups/%@/joins/multiples",BASE_API, group_id]

//api/v1/users/184/groups/hunts
#define GET_MES_GROUP_HUNT(the_id)  [NSString stringWithFormat:@"%@/users/%@/groups/hunts",BASE_API, the_id]

//https://naturapass.e-conception.fr/app_dev.php/api/user/conversations
#define GET_USER_CONVERSATIONS  [NSString stringWithFormat:@"%@/user/conversations",BASE_API]


#define GET_CONVERSATIONS_MESSAGE(conversationID,limit,offset)  [NSString stringWithFormat:@"%@/v1/conversation/messages?conversationId=%@&reverse=0&limit=%@&offset=%@",BASE_API,conversationID,limit,offset]


//https://naturapass.e-conception.fr/api/v1/messages
#define POST_CREATE_DISCUSSION  [NSString stringWithFormat:@"%@/v1/messages",BASE_API]


//NewS
//https://naturapass.e-conception.fr/app_dev.php/api/v2/user/news
#define GET_NEWS  [NSString stringWithFormat:@"%@/v2/user/news",BASE_API]

#define POST_MESSAGE_DISCUSSION  [NSString stringWithFormat:@"%@/messages",BASE_API]

//api/v2/notification/all
/*
 https://naturapass.e-conception.fr/api/v2/notification/all
 
 phat [11:03 AM]
 cai nay de get tat ca cac notificaiton
 */

#define GET_ALL_NOTIFICATION  [NSString stringWithFormat:@"%@/v2/notification/all",BASE_API]

//https://naturapass.e-conception.fr/api/v1/read/message
//{"conversation":{"id":31}}
#define PUT_READ_MESSAGE  [NSString stringWithFormat:@"%@/v1/read/message",BASE_API]

#define POST_JOIN_USER_GROUP(groupid)  [NSString stringWithFormat:@"%@/groups/%@/joins/multiples",BASE_API,groupid]


#define POST_JOIN_USER_LOUNGE(loungeid)  [NSString stringWithFormat:@"%@/lounges/%@/joins/multiples",BASE_API,loungeid]

#define POST_JOIN_GROUP_TO_HUNT(hunt_id)  [NSString stringWithFormat:@"%@/v2/hunts/%@/publications/bygroups",BASE_API,hunt_id]

#define POST_JOIN_AGENDA_GROUP_TO_HUNT(hunt_id)  [NSString stringWithFormat:@"%@/v2/hunts/%@/groups",BASE_API,hunt_id]

#define GET_GROUP_PENDING_INVITATION(limit,offset)  [NSString stringWithFormat:@"%@/v1/groups/pending?limit=%@&offset=%@",BASE_API,limit,offset]
#define V2_GET_GROUP_INVITATION  [NSString stringWithFormat:@"%@/v2/group/invitation",BASE_API]

//delete {"success":true}
//https://naturapass.e-conception.fr/api/v1/groups/160/joins/304
#define DELETE_GROUP_INVITATION(my_id,group_id)  [NSString stringWithFormat:@"%@/v1/groups/%@/joins/%@",BASE_API,my_id,group_id]



//get invitation hunt
//https://naturapass.e-conception.fr/api/v1/lounges/pending?limit=3&offset=0&filter=
#define GET_HUNT_PENDING_INVITATION(limit,offset)  [NSString stringWithFormat:@"%@/v1/lounges/pending?limit=%@&offset=%@",BASE_API,limit,offset]

//https://naturapass.e-conception.fr/api/v1/lounges/192/joins/651
#define DELETE_HUNT_INVITATION(my_id,hunt_id)  [NSString stringWithFormat:@"%@/v1/lounges/%@/joins/%@",BASE_API,my_id,hunt_id]



//Profile
//https://naturapass.e-conception.fr/api/v2/user/connected
#define GETUSERPROFILE_API                                     [NSString stringWithFormat:@"%@/v2/user/connected",BASE_API]


//https://www.naturapass.com/api/v1/lounges/3529/joins/313

//delete user in a hunt.
//{"success":true}

//https://naturapass.e-conception.fr/api/v1/publications/3497
#define PUT_PUBLICATION_EDIT(object_id)                         [NSString stringWithFormat:@"%@/publications/%@",BASE_API,object_id]
#define PUT_OBSERVATION_EDIT(object_id)                         [NSString stringWithFormat:@"%@/v2/publications/%@/observation",BASE_API,object_id]
#define PUT_OBSERVATION_EDIT(object_id)                         [NSString stringWithFormat:@"%@/v2/publications/%@/observation",BASE_API,object_id]
#define POST_MEDIA_EDIT(object_id)                         [NSString stringWithFormat:@"%@/v2/publications/%@/media",BASE_API,object_id]

#define POST_OBSERVATION_CREATE(object_id)                         [NSString stringWithFormat:@"%@/v2/publications/%@/observations",BASE_API,object_id]

#define PUT_UPDATE_PUBLICATION(object_id, type) [NSString stringWithFormat:@"%@/v2/publications/%@/%@",BASE_API,object_id,type]
#define PUT_UPDATE_PUBLICATION_ALL(object_id) [NSString stringWithFormat:@"%@/v2/publications/%@/all",BASE_API,object_id]
//1434:      * PUT /v2/publication/sqlite/refresh
#define PUT_SQLITE_GET_ALL_PUBLICATION [NSString stringWithFormat:@"%@/v2/publication/sqlite/refresh",BASE_API]

#define GET_DASHBOARD_LIVE_HUNT [NSString stringWithFormat:@"%@/v2/hunt/live",BASE_API]

#define GET_SQLITE_PUBLICATION_CARTE [NSString stringWithFormat:@"%@/v2/publication/sqlite",BASE_API]

#define GET_SQLITE_PUBLICATION_CARTE_NEW(the_pub_id,updatedtime) [NSString stringWithFormat:@"%@/v2/publication/sqlite?id=%@&updated=%@",BASE_API,the_pub_id,updatedtime]

#define PUT_SQLITE_SHAPE_CARTE [NSString stringWithFormat:@"%@/v2/shape/sqlite/refresh",BASE_API]

//    https://naturapass.e-conception.fr/api/v2/publication/sqlite?id=3970&updated=1450776883

#define GET_SHAPE_MAP(leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset,sharing) [NSString stringWithFormat:@"%@/v2/shapes/mobile?swLat=%@&swLng=%@&neLat=%@&neLng=%@&reset=%@&sharing=%@",BASE_API,leftLatitude,leftLoungitude,rightLatitude,rightLoungitude,reset,sharing]
#define POST_SHAPE                         [NSString stringWithFormat:@"%@/v2/shapes",BASE_API]

#define PUT_SHAPE(shape_id)                         [NSString stringWithFormat:@"%@/v2/shapes/%@",BASE_API,shape_id]

// 93:      * GET /v2/shapes/mobile?swLat=45.87938425471556&swLng=5.189093561071786&neLat=45.892886757450654&neLng=5.261191339392099&sharing=3&reset=1

#define GET_SQLITE_DISTRIBUTOR(lastUpdate) [NSString stringWithFormat:@"%@/v2/distributors/sqlite?updated=%@",BASE_API,lastUpdate]

//GET PUT DELETE
// v2/publications/{favorite_id}/favorite
#define FAVORITE_PUBLICATION_URL(favorite_id) [NSString stringWithFormat:@"%@/v2/publications/%@/favorite",BASE_API,favorite_id]

//  * GET /v2/publication/favorites?limit=20&offset=20
#define GET_FAVORITE_PUBLICATION(limit,offset)  [NSString stringWithFormat:@"%@/v2/publication/favorites?limit=%@&offset=%@",BASE_API,limit,offset]

// get Sqlite of all points in map of the current user
//PUT /v2/user/sqlite

#define GET_SQLITE_ALL_POINTS_IN_MAP  [NSString stringWithFormat:@"%@/v2/user/sqliteex",BASE_API]

#define GET_SQLITE_ALL_POINTS_IN_MAP_USING_DEFAULT_DB  [NSString stringWithFormat:@"%@/v2/user/sqliteex2",BASE_API]

#define MIN_TIMEOUT 150
#define MEDIUM_TIMEOUT 260
#define MAX_TIMEOUT 320
#define VIDEO_TIMEOUT 360


//HUNT CITY
#define GET_HUNT_CITY_LOCATION              [NSString stringWithFormat:@"%@/v2/user/profile/huntlocation/city",BASE_API]
#define ADD_HUNT_CITY_LOCATION(city_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profiles/huntlocations/cities",BASE_API,city_id]
#define DEL_HUNT_CITY_LOCATION(city_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profile/huntlocation/city",BASE_API,city_id]

//HUNT COUNTRY
#define GET_HUNT_COUNTRY_LOCATION              [NSString stringWithFormat:@"%@/v2/user/profile/huntlocation/country",BASE_API]
#define ADD_HUNT_COUNTRY_LOCATION(country_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profiles/huntlocations/countries",BASE_API,country_id]
#define DEL_HUNT_COUNTRY_LOCATION(country_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profile/huntlocation/country",BASE_API,country_id]


//hunt practice

#define GET_HUNT_PRACTICE              [NSString stringWithFormat:@"%@/v2/user/profile/hunttype/practiced",BASE_API]
#define ADD_HUNT_PRACTICE(hunttype_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profiles/hunttypes/practiceds",BASE_API,hunttype_id]
#define DEL_HUNT_PRACTICE(hunttype_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profile/hunttype/practiced",BASE_API,hunttype_id]

//hunt liked

#define GET_HUNT_LIKED              [NSString stringWithFormat:@"%@/v2/user/profile/hunttype/liked",BASE_API]
#define ADD_HUNT_LIKED(hunttype_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profiles/hunttypes/likeds",BASE_API,hunttype_id]
#define DEL_HUNT_LIKED(hunttype_id)  [NSString stringWithFormat:@"%@/v2/users/%@/profile/hunttype/liked",BASE_API,hunttype_id]



//GET CITIES/COUNTRIES/HUNT TYPE
#define GET_HUNT_LIST_CITIES(limit,offset,filter) [NSString stringWithFormat:@"%@/v2/user/profile/city?limit=%@&offset=%@&filter=%@",BASE_API,limit,offset,filter]
#define GET_HUNT_LIST_COUNTRIES(limit,offset,filter) [NSString stringWithFormat:@"%@/v2/user/profile/country?limit=%@&offset=%@&filter=%@",BASE_API,limit,offset,filter]

#define GET_HUNT_TYPES(limit,offset) [NSString stringWithFormat:@"%@/v2/user/profile/hunttype?limit=%@&offset=%@",BASE_API,limit,offset]


//                Get Hunt type
//
//                GET /v2/user/profile/hunttype?limit=20&offset=0
//https://naturapass.e-conception.fr/api/v2/users/1/profiles/hunttypes/practiceds
//https://naturapass.e-conception.fr/api/v2/users/1/profiles/hunttypes/likeds

#define PUT_GROUP_allow_add_EDIT(group_id)                         [NSString stringWithFormat:@"%@/v2/groups/%@/allowadd",BASE_API,group_id]
#define PUT_GROUP_allow_show_EDIT(group_id)                        [NSString stringWithFormat:@"%@/v2/groups/%@/allowshow",BASE_API,group_id]



#define PUT_GROUP_allowaddshow_Publication_EDIT(hunt_id)                         [NSString stringWithFormat:@"%@/v2/groups/%@/allowaddshow",BASE_API,hunt_id]

#define PUT_HUNT_allowaddshow_Publication_EDIT(hunt_id)                         [NSString stringWithFormat:@"%@/v2/hunts/%@/allowaddshow",BASE_API,hunt_id]

#define PUT_GROUP_allowaddshow_Chat_EDIT(hunt_id)                         [NSString stringWithFormat:@"%@/v2/groups/%@/allowaddshowchat",BASE_API,hunt_id]

#define PUT_HUNT_allowaddshow_Chat_EDIT(hunt_id)                         [NSString stringWithFormat:@"%@/v2/hunts/%@/allowaddshowchat",BASE_API,hunt_id]


#define GET_HUNT_INFORMATION(hunt_id)                        [NSString stringWithFormat:@"%@/v1/lounges/%@",BASE_API,hunt_id]

//Chat message
#define DELETE_MESSAGE_GROUP_CHAT(message_id)                        [NSString stringWithFormat:@"%@/v2/groups/%@/message",BASE_API,message_id]

//api/v1/lounges/1352/message
#define DELETE_MESSAGE_HUNT_CHAT(message_id)                         [NSString stringWithFormat:@"%@/v1/lounges/%@/message",BASE_API,message_id]

#define GET_CHECK_LINKTO_RECEIVER                         [NSString stringWithFormat:@"%@/v2/group/checkreceiver",BASE_API]



