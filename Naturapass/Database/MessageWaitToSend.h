//
//  MessageWaitToSend.h
//  Naturapass
//
//  Created by Giang on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface MessageWaitToSend : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "MessageWaitToSend+CoreDataProperties.h"
