//
//  NotifiEntity+CoreDataProperties.m
//  Naturapass
//
//  Created by Manh on 12/11/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotifiEntity+CoreDataProperties.h"

@implementation NotifiEntity (CoreDataProperties)

@dynamic notiID;
@dynamic notiContent;
@dynamic notiUpdate;
@dynamic notiRead;
@dynamic notiType;
@dynamic notiObjID;
@dynamic notiMyID;
@dynamic notiPhoto;
@dynamic notiSenderID;

@end
