//
//  NotifiEntity+CoreDataProperties.h
//  Naturapass
//
//  Created by Manh on 12/11/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "NotifiEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface NotifiEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *notiID;
@property (nullable, nonatomic, retain) NSString *notiContent;
@property (nullable, nonatomic, retain) NSString *notiUpdate;
@property (nullable, nonatomic, retain) NSNumber *notiRead;
@property (nullable, nonatomic, retain) NSString *notiType;
@property (nullable, nonatomic, retain) NSString *notiObjID;
@property (nullable, nonatomic, retain) NSString *notiMyID;
@property (nullable, nonatomic, retain) NSString *notiPhoto;
@property (nullable, nonatomic, retain) NSString *notiSenderID;

@end

NS_ASSUME_NONNULL_END
