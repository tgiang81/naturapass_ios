//
//  DatabaseManager.m
//  Naturapass
//
//  Created by Manh on 1/8/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "DatabaseManager.h"
#import "FileHelper.h"
#import "Config.h"

@implementation DatabaseManager

static NSOperationQueue * DatabaseQueue = nil;

//- (NSOperationQueue *)myDatabaseQueue {
//    return DatabaseQueue;
//}


+ (DatabaseManager*)sharedManager
{
    static id sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

-(void) swithDatabase
{
    [_queue close];
    
    NSString*path1 = [FileHelper pathForApplicationDataFile:kDatabaseFileName];;
    if (![FileHelper isfileExisting:path1]) {
        [FileHelper duplicateFiletoAppDataFromBundle:kDatabaseFileName toPath:path1 overwrite:YES];
    }
    
    ASLog(@"DB PATH >>>> %@", path1);
    
    _queue= [FMDatabaseQueue databaseQueueWithPath:path1];
    [_queue inDatabase:^(FMDatabase *db) {
        [db setKey:@"SECRET_KEYabc123"];
        FMResultSet *rs = [db executeQuery:@"PRAGMA cipher_migrate"]; // Required to migrate database from SQLCipher v2 to v3
        [rs close];
    }];

    
}

@end
