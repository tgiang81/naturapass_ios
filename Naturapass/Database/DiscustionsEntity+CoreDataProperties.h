//
//  DiscustionsEntity+CoreDataProperties.h
//  Naturapass
//
//  Created by Manh on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DiscustionsEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface DiscustionsEntity (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *disContent;
@property (nullable, nonatomic, retain) NSNumber *disID;
@property (nullable, nonatomic, retain) NSString *disCreateTime;
@property (nullable, nonatomic, retain) NSString *disFullName;
@property (nullable, nonatomic, retain) NSNumber *disOwnerID;
@property (nullable, nonatomic, retain) NSString *disImgUrl;
@property (nullable, nonatomic, retain) NSNumber *disMessageID;
@property (nullable, nonatomic, retain) NSString *disUserTag;
@property (nullable, nonatomic, retain) NSNumber *disMyID;
@property (nullable, nonatomic, retain) NSNumber *disType;

@end

NS_ASSUME_NONNULL_END
