//
//  DatabaseManager.h
//  Naturapass
//
//  Created by Manh on 1/8/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "FMResultSet.h"

@interface DatabaseManager : NSObject

@property (nonatomic, strong) FMDatabaseQueue *queue;
//@property (nonatomic, strong) FMDatabase *myDB;

+ (DatabaseManager*)sharedManager;
//- (NSOperationQueue *)myDatabaseQueue;
-(void) swithDatabase;

@end
