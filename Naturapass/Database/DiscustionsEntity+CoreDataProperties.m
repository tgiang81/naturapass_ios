//
//  DiscustionsEntity+CoreDataProperties.m
//  Naturapass
//
//  Created by Manh on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DiscustionsEntity+CoreDataProperties.h"

@implementation DiscustionsEntity (CoreDataProperties)

@dynamic disContent;
@dynamic disID;
@dynamic disCreateTime;
@dynamic disFullName;
@dynamic disOwnerID;
@dynamic disImgUrl;
@dynamic disMessageID;
@dynamic disUserTag;
@dynamic disMyID;
@dynamic disType;
@end
