//
//  MessageWaitToSend+CoreDataProperties.m
//  Naturapass
//
//  Created by Giang on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MessageWaitToSend+CoreDataProperties.h"

@implementation MessageWaitToSend (CoreDataProperties)

@dynamic mycontent;
@dynamic timecreate;
@dynamic idchat;
@dynamic type;
@dynamic idgroup;
@dynamic iduser;
@dynamic participant;
@dynamic guid;
@dynamic ischeck;
@end
