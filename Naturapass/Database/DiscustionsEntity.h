//
//  DiscustionsEntity.h
//  Naturapass
//
//  Created by Manh on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DiscustionsEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(BOOL)checkExitsWith:(NSString*)disID messageId:(int)messageId expectTarget:(int)expectTarget myID:(int)myID;
@end

NS_ASSUME_NONNULL_END

#import "DiscustionsEntity+CoreDataProperties.h"
