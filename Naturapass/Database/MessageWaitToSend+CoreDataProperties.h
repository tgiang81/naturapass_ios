//
//  MessageWaitToSend+CoreDataProperties.h
//  Naturapass
//
//  Created by Giang on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "MessageWaitToSend.h"

NS_ASSUME_NONNULL_BEGIN

@interface MessageWaitToSend (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *mycontent;
@property (nullable, nonatomic, retain) NSDate *timecreate;
@property (nullable, nonatomic, retain) NSString *idchat;
@property (nullable, nonatomic, retain) NSNumber *type;
@property (nullable, nonatomic, retain) NSString *idgroup;
@property (nullable, nonatomic, retain) NSString *iduser;
@property (nullable, nonatomic, retain) NSString *participant;
@property (nullable, nonatomic, retain) NSString *guid;
@property (nullable, nonatomic, retain) NSNumber *ischeck;

@end

NS_ASSUME_NONNULL_END
