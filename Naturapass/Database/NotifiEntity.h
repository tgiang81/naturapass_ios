//
//  NotifiEntity.h
//  Naturapass
//
//  Created by Manh on 12/11/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotifiEntity : NSManagedObject

// Insert code here to declare functionality of your managed object subclass
+(BOOL)checkExitsWith:(NSString*)notiID myID:(int)myID;
@end

NS_ASSUME_NONNULL_END

#import "NotifiEntity+CoreDataProperties.h"
