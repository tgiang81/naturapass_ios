//
//  AnimalsEntity.h
//  Naturapass
//
//  Created by Giang on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface AnimalsEntity : NSManagedObject
{}
@property (nonatomic, retain)  NSString *name;
@property (nonatomic, retain)  NSNumber *myid;

// Insert code here to declare functionality of your managed object subclass
+(BOOL)checkExits:(int)idm;
@end

