//
//  NotifiEntity.m
//  Naturapass
//
//  Created by Manh on 12/11/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "NotifiEntity.h"

@implementation NotifiEntity

// Insert code here to add functionality to your managed object subclass
+(BOOL)checkExitsWith:(NSString*)notiID myID:(int)myID
{
    
    NSPredicate *ind=  [NSPredicate predicateWithFormat:@"notiID=%d && notiMyID =%d",[notiID intValue],myID];
    NSArray *arr= [NotifiEntity MR_findAllWithPredicate:ind];
    if (arr.count > 0 ) {
        return YES;
    }
    return NO;
}
@end
