//
//  DiscustionsEntity.m
//  Naturapass
//
//  Created by Manh on 12/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "DiscustionsEntity.h"

@implementation DiscustionsEntity

// Insert code here to add functionality to your managed object subclass
+(BOOL)checkExitsWith:(NSString*)disID messageId:(int)messageId expectTarget:(int)expectTarget myID:(int)myID
{
    
    NSPredicate *ind=  [NSPredicate predicateWithFormat:@"disID=%d && disMessageID == %d &&  disType =%d && disMyID =%d",[disID intValue],messageId,expectTarget,myID];
    NSArray *arr= [DiscustionsEntity MR_findAllWithPredicate:ind];
    if (arr.count > 0 ) {
        return YES;
    }
    return NO;
}
@end
