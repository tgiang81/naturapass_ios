//
//  AnimalsEntity.m
//  Naturapass
//
//  Created by Giang on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AnimalsEntity.h"

@implementation AnimalsEntity

// Insert code here to add functionality to your managed object subclass
+(BOOL)checkExits:(int)idm
{
    NSPredicate *ind=  [NSPredicate predicateWithFormat:@"myid == %d", idm];
    NSArray *arr= [AnimalsEntity MR_findAllWithPredicate:ind];
    if (arr.count > 0 ) {
        return YES;
    }
    return NO;
}

@end
