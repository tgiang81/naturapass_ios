//
//  Publication_Carte.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Carte.h"
#import "Publication_Legende_Carte.h"

#import "BasicMapAnnotation.h"
#import "AppCommon.h"
#import "Publication_Specification.h"
#import "UIAlertView+Blocks.h"

#import <CoreLocation/CLPlacemark.h>
#import "HNKGooglePlacesAutocomplete.h"

#import <MapKit/MapKit.h>
#import "CLPlacemark+HNKAdditions.h"

//Chase create
#import "ChassesCreateOBJ.h"
#import "ChassesCreate_Step5.h"
#import "ChassesCreate_Step6.h"
#import "SubNavigationPRE_ANNULER.h"
#import "ChassesCreateOBJ.h"
#import "Publication_FavoriteAddress.h"
#import "MKMapView+ZoomLevel.h"

#import "NetworkCheckSignal.h"
#import "PublicationAddAddress.h"
#import "Publication_Color.h"
#import "AFNetworking.h"
#import "Publication_ChooseFavorite.h"
#import "DatabaseManager.h"
#import "MapDataDownloader.h"
#import "NSString+HTML.h"
#import "AlertVC.h"
#import "ChassesCreate_Step1.h"

@interface Publication_Carte () <MKMapViewDelegate , CLLocationManagerDelegate, UITextFieldDelegate>
{
    GMSMarker *pointMarker;
    
    NSString *strLatitude, *strLongitude, *strAltitude;
    IBOutlet UIButton*  btnSuivatnt;
    
    IBOutlet NSLayoutConstraint *constraintWidth;

    NSMutableArray *dicPhoto;
    
    MKPointAnnotation *markAnnotation;
}

@end

@implementation Publication_Carte

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set config for...
    
    
    [btnSuivatnt setTitle:str(strSUIVANT) forState:UIControlStateNormal ];
    
    //default publication create
    [self.btnFav setImage:  [UIImage imageNamed:@"ic_favarite_map_mur"] forState:UIControlStateNormal];
    
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
    
    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strMUR)];
    subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    
    [self addSubNav:@"SubNavigationPRE_ANNULER"];
    
    //coloring btn
    
    SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
    
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
    
    btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
    btn2.backgroundColor = UIColorFromRGB(MUR_CANCEL);
    vTmpMap.btnPrint.hidden = YES;
    
    //show button map type, change constraint
    vTmpMap.btnTypeMap.hidden = NO;
    vTmpMap.constraintLeftButtonLocation.constant = [UIScreen mainScreen].bounds.size.width - 105;

    
    
//    vTmpMap.mapView_.mapType = kGMSTypeSatellite;
//    iMapType = 11;
    iMapType = (int) [[NSUserDefaults standardUserDefaults] integerForKey:@"defaultMapType" ];
    [self doMapType:iMapType]; //

    switch (self.expectTarget) {
        case ISMUR:
        {
            if (self.isFromSetting) {
                //only PREV
                [self addSubNav:@"SubNavigation_PRE_General"];
                
                UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
                
                btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
            }
            if ([PublicationOBJ sharedInstance].isEditer) {
                [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
            }
        }
            break;
        case ISGROUP:
        {
        }
            break;
        case ISLOUNGE:
        {
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCHANTIERS)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationPRE_ANNULER"];
            
            
            SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
            
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
            
            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
            
            btn2.backgroundColor = UIColorFromRGB(CHASSES_CANCEL);
            //
            btnSuivatnt.backgroundColor = UIColorFromRGB(CHASSES_BACK);
            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
            }
            [self.btnFav setImage:  [UIImage imageNamed:@"ic_favorite_map_hunt"] forState:UIControlStateNormal];
        }
            break;
        case ISCARTE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCARTE)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.searchAddress setTintColor:UIColorFromRGB(CARTE_MAIN_BAR_COLOR)];
            
            [self addSubNav:nil];
        }
            break;
        case ISCREATEAGENDA:
        {
            [self.subview removeFromSuperview];
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview removeFromSuperview];
            [self addMainNav:@"MainNavChasses"];
            
            //Set title
            subview =  [self getSubMainView];
            [subview.myTitle setText:@"CRÉER UNE CHASSE"];
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
            
            
            [self addSubNav:nil];
            btnSuivatnt.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
            //From photo or has location data...
            if ( [[self.simpleDic objectForKey:@"latitude"] doubleValue] != 0 && [[self.simpleDic objectForKey:@"longitude"] doubleValue]!=0 )
            {
                CLLocationCoordinate2D simpleCoord;
                simpleCoord.latitude = [[self.simpleDic objectForKey:@"latitude"] doubleValue];
                simpleCoord.longitude= [[self.simpleDic objectForKey:@"longitude"] doubleValue];
                
                vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
                
            }else{
                if([self locationCheckStatusDenied])
                {
                    [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
                    return;
                }
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
                
            }
        }
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exitMapView) name:@"exitmapview" object:nil];
    
    
    if ([[Reachability reachabilityForLocalWiFi] currentReachabilityStatus] == NotReachable) {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strVotre_wifi_nest_pas_active) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [alert show];
    }
    
    self.tableControl.hidden=YES;
    self.tableControl.alpha = 0.75;
    
    [self getLocation];
    
    constraintWidth.constant = 38;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
            
        }
            break;
        case ISGROUP:
        {
        }
            break;
        case ISLOUNGE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCHANTIERS)];
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
            
            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
            
        }
            break;
        case ISCARTE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
            
        }
            break;
        case ISCREATEAGENDA:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR) ];
            
        }
            break;
        default:
            break;
    }
}
-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
        case 1://ANNULER
        {
            NSString *str=@"";
            Class Cclass= nil;
            
            if (self.expectTarget == ISLOUNGE)
            {
                str = str(strAAgenda);
                Cclass= [ChassesCreate_Step1 class];
            }
            else
            {
                str =str(strPublication);
                Cclass= [PublicationVC class];
            }
            NSString *strmessage= @"";
            if ([PublicationOBJ sharedInstance].isEditer) {
                strmessage = [NSString stringWithFormat:str(strEtesVousSurDeVouloiranuilervotresaisie)];
            }
            else
            {
                strmessage = [NSString stringWithFormat:str(strMessage10),str];
            }
            AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@",str(strAAnnuler),str] message:strmessage cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
            
            [vc doBlock:^(NSInteger index, NSString *str) {
                if (index==0) {
                    // NON
                }
                else if(index==1)
                {
                    //OUI
                    NSArray * controllerArray = [[self navigationController] viewControllers];
                    
                    for (int i=0; i < controllerArray.count; i++) {
                        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                            
                            [self.navigationController popToViewController:self.mParent animated:YES];
                            
                            return;
                        }
                    }
                    [self doRemoveObservation];
                    
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            [vc showAlert];
            
        }
            break;
            
        default:
            break;
    }
}

-(ISSCREEN) getTypeOfRequestScreen
{
    return ISMUR;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getLocation{
    
    if (self.expectTarget == ISLOUNGE)
    {
        self.photoLatitude = [ChassesCreateOBJ sharedInstance].latitude;
        self.photoLongitude = [ChassesCreateOBJ sharedInstance].longitude;
        
        //
        if ( [ChassesCreateOBJ sharedInstance].isModifi) {
            //Add a point
            CLLocationCoordinate2D coord = CLLocationCoordinate2DMake([self.photoLatitude floatValue], [self.photoLongitude floatValue]);
            
            vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:coord zoom: iZoomDefaultLevel];
            
            GMSMarker *marker = [GMSMarker markerWithPosition:coord];
            marker.title = @"";
            
            marker.icon = [UIImage imageNamed:@"red_pin"];
            marker.map = vTmpMap.mapView_;
        }
        
    }
    else { //publication map
        self.photoLatitude = [PublicationOBJ sharedInstance].latitude;
        self.photoLongitude = [PublicationOBJ sharedInstance].longtitude;
    }
    
    if (self.photoLatitude.length > 0 && self.photoLongitude.length > 0) {
        //will use photo's coordinate
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [self.photoLatitude doubleValue];
        simpleCoord.longitude= [self.photoLongitude doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        /*
         Vincent
         Yes, i Think you can delete this point when you create a new publication.
         */
        if (self.expectTarget != ISMUR) {
            pointMarker = [GMSMarker markerWithPosition:simpleCoord];
            pointMarker.title = @"";
            pointMarker.icon = [UIImage imageNamed:@"red_pin"];
            pointMarker.map = vTmpMap.mapView_;
        }
    }
    else if(self.isFromSetting)
    {
        
        self.photoLatitude = nil;
        self.photoLongitude = nil;
        if ([_dicCenterMap[@"lat"] doubleValue] != 0 && [_dicCenterMap[@"lon"] doubleValue]) {
            CLLocationCoordinate2D simpleCoord;
            simpleCoord.latitude = [_dicCenterMap[@"lat"] doubleValue];
            simpleCoord.longitude= [_dicCenterMap[@"lon"] doubleValue];
            
            [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:simpleCoord.latitude longitude:simpleCoord.longitude zoom:iZoomDefaultLevel]];
            
        }else{
            NSDictionary * theCenter = [[NSUserDefaults standardUserDefaults] objectForKey:concatstring([COMMON getUserId],@"CACHE_LAST_POINT")];
            if ( [theCenter[@"mapLat"] doubleValue] != 0 && [theCenter[@"mapLong"] doubleValue]!=0 )
            {
                CLLocationCoordinate2D simpleCoord;
                simpleCoord.latitude = [theCenter[@"mapLat"] doubleValue];
                simpleCoord.longitude= [theCenter[@"mapLong"] doubleValue];
                
                [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:simpleCoord.latitude longitude:simpleCoord.longitude zoom:iZoomDefaultLevel]];
                
            }else{
                if([self locationCheckStatusDenied])
                {
                    [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
                    return;
                }
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
            }
            
        }
        return;
        
    }
    
    [self loadLocations: self.photoLatitude andLong: self.photoLongitude];
}

-(void)loadLocations:(NSString *)newlatitude andLong:(NSString *)newlongitude
{
    NSString *latitude=@"";
    NSString *longitude=@"";
    
    if(newlatitude!=nil||newlongitude != nil){
        latitude  = newlatitude;
        longitude = newlongitude;
    }
    else {
        if([self locationCheckStatusDenied])
        {
            latitude  =  [NSString stringWithFormat:@"%f",LAT_DEFAULT];
            longitude  =  [NSString stringWithFormat:@"%f",LON_DEFAULT];
        }
        else
        {
            latitude  =  [NSString stringWithFormat:@"%f",vTmpMap.mapView_.myLocation.coordinate.latitude];
            longitude  =  [NSString stringWithFormat:@"%f",vTmpMap.mapView_.myLocation.coordinate.longitude];
        }
    }
    
    CLLocationCoordinate2D simpleCoord;
    simpleCoord.latitude = [latitude doubleValue];
    simpleCoord.longitude= [longitude doubleValue];
    
    vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
    
}

#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    
    NSString *strsearchValues=self.searchAddress.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.searchAddress.text=strsearchValues;
    
    [self handleSearchForSearchString:self.searchAddress.text];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self processLoungesSearchingURL:searchText];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.tableControl.hidden=YES;
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSString *strsearchValues=self.searchAddress.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.searchAddress.text=strsearchValues;
    [self handleSearchForSearchString:self.searchAddress.text];
    [theSearchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    [self clearSearchResults];
    self.tableControl.hidden=NO;
    [self.tableControl reloadData];
}

#pragma mark UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""]) {
        
        [self clearSearchResults];
        
    } else {
        [[HNKGooglePlacesAutocompleteQuery sharedQuery] resetAPIKEY:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
        
        [self.searchQuery fetchPlacesForSearchQuery:searchString
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 [KSToastView ks_showToast:str(strAdresseIntrouble) duration:2.0f completion: ^{
                                                 }];
                                             } else {
                                                 self.searchResults = places;
                                                 [self.tableControl reloadData];
                                             }
                                         }];
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kHNKDemoSearchResultsCellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HNKGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    
    [CLPlacemark hnk_placemarkFromGooglePlace:place
                                       apiKey:self.searchQuery.apiKey
                                   completion:^(CLLocation *m_Placemark, NSString *addressString, NSError *error) {
                                       if (error) {
                                           [KSToastView ks_showToast:str(strAdresseIntrouble) duration:2.0f completion: ^{
                                           }];
                                           
                                       } else if (m_Placemark) {
                                           if (self.searchResults.count > indexPath.row) {
                                               [self recenterMapToPlacemark:m_Placemark];
                                               
                                               if (self.searchResults.count > 0) {
                                                   self.searchAddress.text=  [self placeAtIndexPath:indexPath].name;
                                               }
                                               self.tableControl.hidden=YES;
                                               [self.searchAddress setShowsCancelButton:NO animated:YES];
                                               [self.searchAddress resignFirstResponder];
                                           }
                                       }
                                   }];
}

#pragma mark - Helpers

- (HNKGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.searchResults.count > indexPath.row) {
        return self.searchResults[indexPath.row];
        
    }else{
        return nil;
    }
}

#pragma mark Map Helpers

- (void)addPlacemarkAnnotationToMap:(CLPlacemark *)m_Placemark addressString:(NSString *)address
{
}

- (void)recenterMapToPlacemark:(CLLocation *)m_Placemark
{
    vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:m_Placemark.coordinate zoom: iZoomDefaultLevel];
    
    if (pointMarker.map) {
        pointMarker.map =  nil;
    }
    
    pointMarker = [GMSMarker markerWithPosition:m_Placemark.coordinate];
    pointMarker.title = @"";
    pointMarker.icon = [UIImage imageNamed:@"red_pin"];
    pointMarker.map = vTmpMap.mapView_;
}

#pragma mark Search Helpers

- (void)clearSearchResults
{
    self.searchResults = @[];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}

#pragma mark - OVERWRITE MAPVIEW

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = vTmpMap.mapView_.camera.target.latitude;
    float lng = vTmpMap.mapView_.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    if (self.expectTarget == ISLOUNGE) {
        [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
        [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
    }
    
}
#pragma mark - MAPVIEW

-(void) exitMapView
{
}


-(void) gotoChooseFavWithFavorites:(NSArray*)arrFavo
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopProgressLoading];
        if (arrFavo.count>0) {
            Publication_ChooseFavorite *viewController1 = [[Publication_ChooseFavorite alloc] initWithNibName:@"Publication_ChooseFavorite" bundle:nil];
            
            [PublicationOBJ sharedInstance].arrCacheFavorites =[arrFavo mutableCopy];
            
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
            
        }else{
            //seem offline....or no fav...
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                
                
                NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE ( c_user_id=%@ AND c_default=%@ ) ORDER BY c_id", sender_id, @"1"];
                FMResultSet *set_querry = [db  executeQuery:strQuerry];
                
                NSMutableArray*mutArr = [NSMutableArray new];
                
                while ([set_querry next])
                {
                    [mutArr addObject:@{ @"name": [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ? [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] : @"",
                                         @"id": [set_querry stringForColumn:@"c_id"] ? [set_querry stringForColumn:@"c_id"] : @""
                                         
                                         }];
                }
                
                
                if (mutArr.count > 0) {
                    Publication_ChooseFavorite *viewController1 = [[Publication_ChooseFavorite alloc] initWithNibName:@"Publication_ChooseFavorite" bundle:nil];
                    
                    [PublicationOBJ sharedInstance].arrCacheFavorites = mutArr;
                    
                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                }else{
                    Publication_Legende_Carte *viewController1 = [[Publication_Legende_Carte alloc] initWithNibName:@"Publication_Legende_Carte" bundle:nil];

                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                }
                
            }];
        }
    });
    
}

- (IBAction)onNext:(id)sender {
    
    if (self.expectTarget == ISCREATEAGENDA) {
        [self fnGetLocationCreateChasse];
        return;
    }
    //Online get location address
    
    if ([COMMON isReachable])
    {
        [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
            
            //i got the address
            [PublicationOBJ sharedInstance].location = mAddress;
            
        } withData:@{@"latitude" : strLatitude,
                     @"longitude" : strLongitude}];
    }
    
    //only long lat
    [self doWorkWithLocation:@""];
}

/*CREATE CHASSE*/
-(void) fnGetLocationCreateChasse
{
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.latitude] withLng:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.longitude]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        if ([response[@"results"] isKindOfClass: [NSArray class]])
        {
            
            NSArray *retArr = response[@"results"];
            
            NSString * strLocation = @"";
            
            BOOL alreadyGotAddress = NO;
            
            for (NSDictionary*mDic in retArr)
            {
                NSArray*tmpArr = mDic[@"types"];
                
                if ([tmpArr containsObject :@"administrative_area_level_1"])
                {
                    alreadyGotAddress = YES;
                    
                    strLocation =  mDic[@"formatted_address"];
                    
                    break;
                }
            }
            
            if (alreadyGotAddress == NO) {
                if (retArr.count > 0) {
                    NSDictionary*mDic = retArr[0];
                    strLocation =  mDic[@"formatted_address"];
                }
            }
            
            [ChassesCreateOBJ sharedInstance].address = strLocation;
            [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
            [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
            [self gotoback];
        }else{
            
            [self stopProgressLoading];
            
        }
        
    };
}
#pragma mark - CHASSE CREATE MAP


-(void)  doWorkWithLocation:(NSString*)str
{
    //From setting screens
    if (self.isFromSetting)
    {
        
        PublicationAddAddress *viewController1 = [[PublicationAddAddress alloc] initWithNibName:@"PublicationAddAddress" bundle:nil];
        
        viewController1.myDic = @{@"address":@"",
                                  @"latitude":strLatitude,
                                  @"longitude":strLongitude};
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        
        
        //            [COMMON addLoading:self];
        //            WebServiceAPI *serviceObj = [WebServiceAPI new];
        //             [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.latitude] withLng:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.longitude]];
        //
        //            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //                [self stopProgressLoading];
        //
        //                if (!response) {
        //                    return ;
        //                }
        //
        //                if ([response[@"results"] isKindOfClass: [NSArray class]]) {
        //
        //                    NSArray *retArr = response[@"results"];
        //
        //                    for (NSDictionary*mDic in retArr)
        //                    {
        //                        NSArray*tmpArr = mDic[@"types"];
        //
        //                        if ([tmpArr containsObject :@"administrative_area_level_1"])
        //                        {
        //
        //                            NSString * strLocation =  mDic[@"formatted_address"];
        //
        //                            PublicationAddAddress *viewController1 = [[PublicationAddAddress alloc] initWithNibName:@"PublicationAddAddress" bundle:nil];
        //
        //                            viewController1.myDic = @{@"address":strLocation,
        //                                                      @"latitude":strLatitude,
        //                                                      @"longitude":strLongitude};
        //                            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        //                            break;
        //                        }
        //                    }
        //
        //                }
        //
        //            };
        return;
    }
    
    if(self.expectTarget == ISLOUNGE)
    {
        [self publierAction:nil];
    }
    else{
        if ([PublicationOBJ sharedInstance].isEditer)
        {
            
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [WebServiceAPI new];
            [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.latitude] withLng:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.longitude]];
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [self stopProgressLoading];
                
                if (!response) {
                    return ;
                }
                if ([response[@"results"] isKindOfClass: [NSArray class]]) {
                    
                    NSArray *retArr = response[@"results"];
                    
                    for (NSDictionary*mDic in retArr)
                    {
                        NSArray*tmpArr = mDic[@"types"];
                        
                        if ([tmpArr containsObject :@"administrative_area_level_1"])
                        {
                            
                            NSString * strLocation =  mDic[@"formatted_address"];
                            
                            //send...new address
                            
                            [PublicationOBJ sharedInstance].latitude =strLatitude;
                            [PublicationOBJ sharedInstance].longtitude = strLongitude;
                            [PublicationOBJ sharedInstance].address =strLocation;
                            [[PublicationOBJ sharedInstance]  modifiPublication:self withType:EDIT_CARTE];
                            break;
                        }
                    }
                    
                }
            };
            return;
        }
        else
        {
            //Publication Treatment like offline
            [PublicationOBJ sharedInstance].latitude = strLatitude;
            [PublicationOBJ sharedInstance].longtitude = strLongitude;
            [PublicationOBJ sharedInstance].altitude = self.photoAltitude;
            
            //Offline
            //Only continue if Making publication...
            if (self.expectTarget != ISLOUNGE) {
                NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                
                [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                [self gotoChooseFavWithFavorites:nil];
            }else{
                //        strNETWORK
                //
                
                [AZNotification showNotificationWithTitle:str(strNETWORK) controller:self notificationType:AZNotificationTypeError];
            }

        }
        
    }
}

- (IBAction)publierAction:(id)sender {
    
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.latitude] withLng:[NSString stringWithFormat:@"%f",vTmpMap.mapView_.camera.target.longitude]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        if (!response) {
            [self stopProgressLoading];
            
            return ;
        }
        if ([response[@"results"] isKindOfClass: [NSArray class]])
        {
            
            NSArray *retArr = response[@"results"];
            
            NSString * strLocation = @"";
            
            BOOL alreadyGotAddress = NO;
            
            for (NSDictionary*mDic in retArr)
            {
                NSArray*tmpArr = mDic[@"types"];
                
                if ([tmpArr containsObject :@"administrative_area_level_1"])
                {
                    alreadyGotAddress = YES;
                    
                    strLocation =  mDic[@"formatted_address"];
                    
                    break;
                }
            }
            
            if (alreadyGotAddress == NO) {
                if (retArr.count > 0) {
                    NSDictionary*mDic = retArr[0];
                    strLocation =  mDic[@"formatted_address"];
                }
            }
            
            ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
            
            NSString *strAccess =[NSString stringWithFormat:@"%u",obj.accessKind];
            if (obj.imgData == nil) {
                UIImage *image=[UIImage imageNamed:@"img_cam"];
                obj.imgData = UIImageJPEGRepresentation(image, 0.5);
            }
            
            NSDictionary *dicGeo = nil;
            
            if (strLocation)
            {
                dicGeo =@{@"address":strLocation,
                          @"latitude":strLatitude,
                          @"longitude":strLongitude};
            }else{
                dicGeo =@{@"address":@"",
                          @"latitude":strLatitude,
                          @"longitude":strLongitude};
            }
            
            NSDictionary* postDict = @{@"lounge":   @{@"name":obj.strName,
                                                      @"description":obj.strComment,
                                                      @"geolocation":@"0", //1 option for admin to allow user select GEO or not... Now is disabled
                                                      @"access":strAccess,
                                                      @"meetingAddress":dicGeo,
                                                      @"meetingDate":obj.strDebut,
                                                      @"endDate":obj.strFin,
                                                      //ggtt
                                                      @"allow_add":  [NSNumber numberWithBool:obj.allow_add]  ,
                                                      @"allow_show": [NSNumber numberWithBool:obj.allow_show],
                                                      @"allow_add_chat":  [NSNumber numberWithBool:obj.allow_chat_add]  ,
                                                      @"allow_show_chat": [NSNumber numberWithBool:obj.allow_chat_show]
                                                      
                                                      }};
            UIImage *image = [UIImage imageWithData:obj.imgData];
            NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
            
            NSDictionary *attachment;
            attachment = @{@"kFileName": @"image.png",
                           @"kFileData": fileData};
            
            [ChassesCreateOBJ sharedInstance].attachment = [attachment copy];
            [ChassesCreateOBJ sharedInstance].address = strLocation;
            [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
            [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
            
            //update
            
            if (obj.isModifi) {
                //put as json...without image
                [[ChassesCreateOBJ sharedInstance]  modifiChassesWithVC:self];
            }else{
                //post multi-form
                
                dicPhoto =[NSMutableArray new];
                
                //SET DATA FORM
                [self processParsedObjectPhoto: postDict];
                
                //CREATE AGENDA
                ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
                UIImage *image = [UIImage imageWithData:obj.imgData];
                NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
                
                NSDictionary *attachment;
                attachment = @{@"kFileName": @"image.png",
                               @"kFileData": fileData};
                
                
                //create new
                
                WebServiceAPI *serviceAPI =[WebServiceAPI new];
                [serviceAPI postLoungePhotoAction:dicPhoto withAttachmentMediaDic:attachment];
                serviceAPI.onComplete =^(id response, int errCode)
                {
                    [self stopProgressLoading];
                    
                    if (!response || [response isKindOfClass: [NSArray class]]) {
                        
                        //                    [AZNotification showNotificationWithTitle:str(strINVALID_ERROR) controller:self notificationType:AZNotificationTypeError];
                        
                        return ;
                    }
                    if ( [response[@"hunt"] isKindOfClass:[NSDictionary class]]) {
                        
                        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                        //update DB.
                        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                        [app updateAllowShowAdd:response];
                        
                        [ChassesCreateOBJ sharedInstance].strID  = response[@"hunt"][@"id"];
                        
                        ChassesCreate_Step5 *viewController1 = [[ChassesCreate_Step5 alloc] initWithNibName:@"ChassesCreate_Step5" bundle:nil];
                        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
                    }
                    
                    
                };

            }
            
        }else{
            
            [self stopProgressLoading];
            
        }
        
    };
    
}

-(void)processParsedObjectPhoto:(id)object{
    [self processParsedObjectPhoto:object depth:0 parent:nil path:nil];
}

-(void)processParsedObjectPhoto:(id)object depth:(int)depth parent:(id)parent path:(NSString*)strPath{
    
    if([object isKindOfClass:[NSDictionary class]]){
        
        for(NSString * key in [object allKeys]){
            id child = [object objectForKey:key];
            if (!strPath) {
                [self processParsedObjectPhoto:child depth:depth+1 parent:object path: key];
            }else{
                [self processParsedObjectPhoto:child depth:depth+1 parent:object path: [NSString stringWithFormat:@"%@[%@]",strPath,key ]];
            }
        }
    }else if([object isKindOfClass:[NSArray class]]){
        
        for(id child in object){
            [self processParsedObjectPhoto:child depth:depth+1 parent:object path:strPath];
        }
        
    }
    else{
        
        [dicPhoto addObject:@{@"path": strPath, @"value": [NSString stringWithFormat:@"%@",[object description]] }];
        
    }
}

//LOUNGE
-(IBAction)fnAdressFavorite:(id)sender
{
    
    Publication_FavoriteAddress *viewController1 = [[Publication_FavoriteAddress alloc] initWithNibName:@"Publication_FavoriteAddress" bundle:nil];
    viewController1.isFromSetting = self.isFromSetting;
    
    [viewController1 setMyCallback: ^(NSDictionary*dicRet){
        
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [dicRet[@"latitude"] doubleValue];
        simpleCoord.longitude= [dicRet[@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        
        if (pointMarker.map) {
            pointMarker.map =  nil;
        }
        
        pointMarker = [GMSMarker markerWithPosition:simpleCoord];
        pointMarker.title = @"";
        pointMarker.icon = [UIImage imageNamed:@"red_pin"];
        pointMarker.map = vTmpMap.mapView_;
        
    }];
    
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
    
}

-(void)stopProgressLoading
{
    [COMMON removeProgressLoading];
}

#pragma mark - MAPKIT

-(IBAction)fnTracking_CurrentPos:(id)sender
{
    if([self locationCheckStatusDenied])
    {
        [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
        return;
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:vTmpMap.mapView_.camera.zoom]];
    
}

@end
