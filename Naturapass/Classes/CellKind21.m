//
//  CellKind21.m
//  Naturapass
//
//  Created by Giang on 11/18/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "CellKind21.h"
#import "AppCommon.h"

@implementation CellKind21

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.layer setMasksToBounds:YES];
    self.layer.cornerRadius= 4;
    self.layer.borderWidth =0;
    [COMMON listSubviewsOfView:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
