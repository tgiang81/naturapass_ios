//
//  ChatListe.m
//  Naturapass
//
//  Created by Giang on 9/21/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatListeViewFull.h"
#import "CellKind11.h"
#import "ChatVC.h"
#import "ChatNouveauVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *identifierSection0 = @"MyTableViewCell0";


@interface ChatListeViewFull ()
{
    NSMutableArray *arrDataDiscussions;
    IBOutlet UIButton *btnDiscussion;
    IBOutlet UILabel *lbTitle;
    NSArray * arrCategories;
    IBOutlet UILabel *lbDescription;
}
@end

@implementation ChatListeViewFull

- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription.text = str(strDISCUSSIONS);

    arrDataDiscussions = [NSMutableArray new];
    if (self.conversation_stype ==0) {
        btnDiscussion.hidden=NO;
        
    }
    else
    {
        btnDiscussion.hidden=YES;
        
    }
    arrCategories = @[str(strDISCUSSIONS), str(strDiscussionsGroupes), str(strDiscussionsChasses)];
    lbTitle.text =arrCategories[self.conversation_stype];
       //Refresh/loadmore
    [self initRefreshControl];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind11" bundle:nil] forCellReuseIdentifier:identifierSection0];
    
    [self startRefreshControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
#pragma mark - REFRESH/LOAD MORE
//overwrite
- (void)insertRowAtTop {
    switch (self.conversation_stype) {
        case 0:
        {
            [self getConversationsAction:YES];
        }
            break;
        case 1:
        {
            [self getGroupConversationAction:YES];
            
        }
            break;
        case 2:
        {
            [self getLoungeConversationAction:YES];
            
            
        }
            break;
        default:
            break;
    }
}

-(void)insertRowAtBottom
{
    switch (self.conversation_stype) {
        case 0:
        {
            [self getConversationsAction:NO];
        }
            break;
        case 1:
        {
            [self getGroupConversationAction:NO];
            
        }
            break;
        case 2:
        {
            [self getLoungeConversationAction:NO];
            
        }
            break;
        default:
            break;
    }}

-(void)getConversationsAction:(BOOL)isfresh
{
    NSString *strOffset = @"0";
    if (arrDataDiscussions.count>0 && isfresh ==NO) {
        strOffset =[NSString stringWithFormat:@"%d",(int)arrDataDiscussions.count];
    }
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnGET_DISCUSSTION_CONVERSATIONS:@"10" offset:strOffset];
    __weak typeof(self) wself = self;

    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        //ref
        if (isfresh) {
            [arrDataDiscussions removeAllObjects];
        }
        //List message
        if (response && [response[@"messages"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messages"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions addObject:dic];
            }
            [self.tableControl reloadData];
        }
    };
}

-(void)getLoungeConversationAction:(BOOL) isfresh
{
    NSString *strOffset = @"0";
    if (arrDataDiscussions.count>0 && isfresh ==NO) {
        strOffset =[NSString stringWithFormat:@"%d",(int)arrDataDiscussions.count];
    }
    [COMMON addLoading:self];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    __weak typeof(self) wself = self;
    [serviceObj fnGET_LOUNGE_CONVERSATIONS:@"10" offset:strOffset];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        if([wself fnCheckResponse:response]) return;

        if (isfresh) {
            [arrDataDiscussions removeAllObjects];
        }
        //List message hunt
        if (response && [response[@"messagesHunt"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messagesHunt"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions addObject:dic];
            }
            [self.tableControl reloadData];
        }
    };
}

-(void)getGroupConversationAction:(BOOL)isfresh
{
    NSString *strOffset = @"0";
    if (arrDataDiscussions.count>0 && isfresh ==NO) {
        strOffset =[NSString stringWithFormat:@"%d",(int)arrDataDiscussions.count];
    }
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    __weak typeof(self) wself = self;
    [serviceObj fnGET_GROUP_CONVERSATIONS:@"10" offset:strOffset];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        if([wself fnCheckResponse:response]) return;

        //ref
        if (isfresh) {
            [arrDataDiscussions removeAllObjects];
        }
        //List message group
        if (response && [response[@"messagesGroup"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messagesGroup"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions addObject:dic];
            }
        }
        [self.tableControl reloadData];
    };
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDataDiscussions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind11 *cell = nil;
    NSDictionary *dic = nil;
    
    switch (self.conversation_stype) {
        case 0:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
            dic = arrDataDiscussions[indexPath.row] ;
            if (dic)
            {

                NSString* imgUrl = nil;
                NSString * compountNames = nil;
                NSMutableArray*mutArray = [NSMutableArray new];
                
                
                NSArray *arrPartics =dic[@"conversation"][@"participants"];
/*
 conversation =     {
 id = 71;
 participants =         (
 );
 updated = "2015-08-28T09:51:42+02:00";
 };
 */
                
                
                
                if (   ([dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) ||
                    (int)arrPartics.count == 1)
                {
                    // Display all name in participants.
                    NSArray *arrParts =  dic[@"conversation"][@"participants"];
                    
                    if (arrParts.count > 0) {
                        NSDictionary*firstDic = dic[@"conversation"][@"participants"][0];
                        
                        if (firstDic[@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,firstDic[@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,firstDic [@"photo"]];
                        }
                        
                        
                        for (NSDictionary*Dic2 in dic[@"conversation"][@"participants"]) {
                            [mutArray addObject:Dic2[@"firstname"]];
                        }
                        
                        compountNames = [mutArray componentsJoinedByString:@", "];

                    }
                    
                }else{
                    int totalParticipants = (int) arrPartics.count - 1; // except latestUser;
                    
                    if (totalParticipants > 1) {
                        // Display "latestuser and totalParticipants members.
                        if (dic[@"owner"][@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
                        }
                        
                        compountNames = [NSString stringWithFormat:@"%@ et %d membres", dic[@"owner"][@"firstname"], (int)arrPartics.count-1];
                    } else {
                        // Display "latestuser and 1 member.
                        if (dic[@"owner"][@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
                        }
                        
                        compountNames = [NSString stringWithFormat:@"%@ %@", dic[@"owner"][@"firstname"],str(strEt_un_autre_membre)];
                    }
                    
                }
                
                if (imgUrl) {
                    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];

                }
                //FONT
                if (compountNames) {
                    cell.label1.text = compountNames;
                }
                
                NSString *valueEmoj = [dic[@"content"] emo_emojiString];
                
                cell.label2.text = valueEmoj;
                
                //timer
                NSDateFormatter *inputFormatter;
                
                NSDateFormatter *outputFormatterBis;
                
                //dd mmm .. this year...
                //hh mm.. today
                
                inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
                NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"updated"]];
                NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
                
                outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
                
                
                //"date": "2015-09-21T00:00:00+0200"
                cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
            }
            
            //set color
            if ([dic[@"unreadCount"] intValue] >0) {
                [cell.contentView setBackgroundColor:UIColorFromRGB(DISCUSSION_CELL_ACTIVE_COLOR)];
            }else{
                [cell.contentView setBackgroundColor:UIColorFromRGB(DISCUSSION_CELL_WHITE_COLOR)];
            }
            
        }
            break;
            
            
            /*
             {
             content = czv;
             created = "2015-10-22T10:06:29+02:00";
             group =     {
             }
             
             */
        case 1:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
            dic = arrDataDiscussions[indexPath.row] ;
            
            NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
            
            [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
            //FONT
            cell.label1.text = dic[@"group"][@"name"];
            
            NSString *valueEmoj = [dic[@"content"] emo_emojiString];
            
            cell.label2.text = valueEmoj;
            //timer
            NSDateFormatter *inputFormatter;
            
            NSDateFormatter *outputFormatterBis;
            
            //dd mmm .. this year...
            //hh mm.. today
            
            inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"created"]];
            NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
            
            outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
            
            
            //"date": "2015-09-21T00:00:00+0200"
            cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
        }
            break;
            
        case 2:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
            dic = arrDataDiscussions[indexPath.row] ;
            NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
            
            [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
            //FONT
            cell.label1.text = dic[@"hunt"][@"name"];
            
            NSString *valueEmoj = [dic[@"content"] emo_emojiString];
            
            cell.label2.text = valueEmoj;
            //timer
            NSDateFormatter *inputFormatter;
            
            NSDateFormatter *outputFormatterBis;
            
            //dd mmm .. this year...
            //hh mm.. today
            
            inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"created"]];
            NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
            
            outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
            
            
            //"date": "2015-09-21T00:00:00+0200"
            cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
        }
            break;
            
        default:
            break;
    }
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //cell
    ChatVC *viewController1 = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
    viewController1.mParent = self;
    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:YES completion:^ {
        NSLog(@"COMPLETED");
    }];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //request update counter...
        
    };
    NSDictionary*dic =    arrDataDiscussions[indexPath.row];
    switch (self.conversation_stype) {
        case 0:
        {
            [serviceObj fnPUT_READ_MESSAGE:@{@"conversation":@{@"id":dic[@"conversation"][@"id"]}}];
            viewController1.myConversationID = dic[@"conversation"][@"id"];
            viewController1.expectTarget =ISDISCUSS;
            
        }
            break;
        case 1:
        {
            viewController1.expectTarget =ISGROUP;
            [GroupEnterOBJ sharedInstance].dictionaryGroup = dic[@"group"];
            
        }
            break;
        case 2:
        {
            viewController1.expectTarget =ISLOUNGE;
            [GroupEnterOBJ sharedInstance].dictionaryGroup = dic[@"hunt"];
            
        }
            break;
        default:
            break;
    }
}

-(IBAction)fnCreateNewChasses:(id)sender
{
    ChatNouveauVC *viewController1 = [[ChatNouveauVC alloc] initWithNibName:@"ChatNouveauVC" bundle:nil];
    [self pushVC:viewController1 animate:NO expectTarget:ISDISCUSS];
}

@end
