//
//  GroupCreate_Step1.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step1.h"
#import "GroupCreate_Step2.h"
#import "PhotoSheetVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ImageWithCloseButton.h"
#import "GroupCreateOBJ.h"
#import "AlertVC.h"
#import "GroupCreate_Publication_Discussion_Allow.h"

@interface GroupCreate_Step1 ()
{
    NSDictionary *postDict;
    NSDictionary *attachment;
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnHelp;
}
@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPhotoSelected;
@end

@implementation GroupCreate_Step1

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strInformationSubLeGroupe);
    [btnHelp setTitle:  str(strENSAVOIRPLUS) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
    [self initialization];
    self.expectTarget =ISGROUP;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getAllHuntsAdmin];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        //or get from local save...
        if (response) {
            [GroupCreateOBJ sharedInstance].listHuntAdmin = response[@"hunts"];
        }
    };
}
-(void)initialization
{
    [labelChoosePhoto setText:str(strChoose_Photo)];
    [btnAvatar   setTitle:str(strChoose_Avatar) forState:UIControlStateNormal];
    [btnCamera   setTitle:str(strTake_Photo) forState:UIControlStateNormal];
    [btnLibrary  setTitle:str(strChoose_Library) forState:UIControlStateNormal];
    [btnCancel   setTitle:str(strCancel) forState:UIControlStateNormal];
    [nameTextField setAutocapitalizationType: UITextAutocapitalizationTypeWords];
    nameTextField.placeholder = str(strNomDuGroupe);
    descTitleLabel.text= str(strDescriptionDeVotreGroupe);
    descTitleLabel.textColor = [UIColor lightGrayColor];
    [self InitializeKeyboardToolBar];
    [nameTextField setInputAccessoryView:self.keyboardToolbar];
    [descTextView setInputAccessoryView:self.keyboardToolbar];
    
    self.viewPhotoSelected.hidden=YES;


    [self.viewPhotoSelected setMyCallback: ^(){
        //Close
        self.viewPhotoSelected.hidden=YES;
        self.viewPhotoSelected.imageContent.image = nil;
    }];
    //modifi
    
    if ([GroupCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
        GroupCreateOBJ *obj =[GroupCreateOBJ sharedInstance];
        nameTextField.text =obj.strName;
        descTextView.text =obj.strComment;
        if (obj.imgData) {
            self.viewPhotoSelected.hidden=NO;
            self.viewPhotoSelected.imageContent.image = [UIImage imageWithData:obj.imgData];

        }else{
            self.viewPhotoSelected.hidden=YES;
            self.viewPhotoSelected.imageContent.image = nil;
        }
        if([descTextView.text isEqual:@""]||descTextView ==nil)
            descTitleLabel.text= str(strDDescription);
        else
            descTitleLabel.text=@"";
    }
}


- (IBAction)profileImageOptions:(id)sender{
    [nameTextField resignFirstResponder];
    [descTextView resignFirstResponder];
    
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.viewPhotoSelected.imageContent.image = imgData;
        self.viewPhotoSelected.hidden = NO;
    }];
    
    [self presentViewController:vc animated:NO completion:^{
        
    }];
}


#pragma mark - action
-(IBAction)hellpAction:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strAnnuler_Groups) message:str(strDescHelpAgenda) cancelButtonTitle:nil otherButtonTitles:str(strOK)];
    
    [vc doBlock:^(NSInteger index, NSString *str) {
        if (index==0) {
            // NON
        }
        else if(index==1)
        {
            //OUI
        }
    }];
    [vc showAlert];
}
-(IBAction)inviteAction:(id)sender
{
    //Check input parameter
    if(![descTextView.text isEqualToString:@""] && ![nameTextField.text isEqualToString:@""])
    {
        //to UNICODE
        NSString *strName = [nameTextField.text emo_emojiString];
        NSString *strComment = [descTextView.text emo_emojiString];

        [GroupCreateOBJ sharedInstance].imgData = UIImageJPEGRepresentation([[AppCommon common] resizeImage:self.viewPhotoSelected.imageContent.image targetSize:CGSizeMake(128, 128)], 1.0);
        
        [GroupCreateOBJ sharedInstance].strName =strName;
        [GroupCreateOBJ sharedInstance].strComment =strComment;
        if ([GroupCreateOBJ sharedInstance].isModifi) {
            [[GroupCreateOBJ sharedInstance] modifiGroupWithVC:self];
        }
        else
        {
            
            
            GroupCreate_Publication_Discussion_Allow *viewController1 = [[GroupCreate_Publication_Discussion_Allow alloc] initWithNibName:@"GroupCreate_Publication_Discussion_Allow" bundle:nil];
            viewController1.myTypeView = PUBLICATION_ALLOW_AGENDA;
            
            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
            
            //            GroupCreate_Step2 *viewController1 = [[GroupCreate_Step2 alloc] initWithNibName:@"GroupCreate_Step2" bundle:nil];
            //            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }
    }
    else{
        [COMMON showErrorAlert:str(strRemplirtoutleschamps)];
    }
}

#pragma mark - delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if([descTextView.text isEqual:@""]||descTextView ==nil)
        descTitleLabel.text= str(strDDescription);
    else
        descTitleLabel.text=@"";
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 100) animated:YES];
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [nameTextField resignFirstResponder];
    [descTextView resignFirstResponder];
    
    return YES;
}
#pragma mark -keyboar tool bar
- (void)resignKeyboard:(id)sender
{
    [nameTextField resignFirstResponder];
    [descTextView resignFirstResponder];
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 150) animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}


@end
