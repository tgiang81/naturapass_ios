//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreaePersonneFictifParticipe.h"
#import "CellKind2.h"
#import "ChassesMemberOBJ.h"
#import "ChasseSettingMembres.h"
#import "ChassesCreate_Step5.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChassesCreaePersonneFictifParticipe ()
{
    NSArray * arrData;
    NSString *str_Owner;
}
@end

@implementation ChassesCreaePersonneFictifParticipe

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name":str(strJe_participe),
                                   @"image":@"ic_participate"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strJe_ne_sais_pas),
                                   @"image":@"ic_pre_participate"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strJe_ne_participe_pas),
                                   @"image":@"ic_not_participate"} copy];
    
    arrData =  [@[dic1,dic2,dic3] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    
    
}
#pragma mark - callback
-(void)setCallback:(Callback)callback
{
    _callback=callback;
}
-(void)doCallback:(Callback)callback
{
    self.callback=callback;
}
#pragma mark - table delegate
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.constraint_image_width.constant =40;
    cell.constraint_image_height.constant =40;
    cell.constraint_control_width.constant = 20;
    cell.constraintRight.constant = 5;
    btn.frame = CGRectMake(0, 0, btn.image.size.width , btn.image.size.height);
    [cell.viewControl addSubview:btn];
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *numberOption =@"2";
    switch (indexPath.row) {
        case 0:
        {
            numberOption=@"1";
            
        }
            break;
            
        case 1:
        {
            numberOption=@"2";
            
        }
            break;
        case 2:
        {
            numberOption=@"0";
            
        }
            break;
        default:
            break;
    }
    
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    NSDictionary * postDict = @{@"firstname" :obj.strFirtName,
                                @"lastname" :obj.strLastName,
                                @"participation" :numberOption,
                                @"publicComment" :obj.strCommentPublic,
                                @"privateComment" :obj.strComentPrive};
    
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI = [WebServiceAPI new];
    [serviceAPI postLoungeNotMemberWithLoungId:obj.strMyKindId withParametersDic:postDict];
    serviceAPI.onComplete =^(id response, int intCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if(response[@"success"]){
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (int i=0; i < controllerArray.count; i++) {
                if ([controllerArray[i] isKindOfClass: [ChassesCreate_Step5 class]]) {
                    
                    [self.navigationController popToViewController:controllerArray[i] animated:YES];
                    return;
                }
            }
            
            [self doRemoveObservation];

            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    };
}
@end
