//
//  CellBase.h
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLKMenuPopover.h"
#import "AppCommon.h"
#import "SettingActionView.h"

typedef void (^callBackGroup) (NSInteger);
typedef void (^callBackDismiss) (NSInteger);
@interface CellBase : UITableViewCell <MLKMenuPopoverDelegate>

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *btnHideMenu;

@property (weak, nonatomic)  UIView *viewMenu;

@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property (nonatomic, copy) callBackGroup CallBackGroup;
@property (nonatomic,strong) IBOutlet UIImageView *imgBackGroundSetting;
@property (nonatomic,strong) IBOutlet UIImageView *imgSettingSelected;
@property (nonatomic,strong) IBOutlet UIImageView *imgSettingNormal;
@property (nonatomic,strong) IBOutlet UIImageView *imgBgMore;
@property (nonatomic,strong) IBOutlet UIImageView *imgNormalMore;
@property (nonatomic,strong) IBOutlet UIView *viewGeoMore;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewGeoMore;

@property (nonatomic, strong) IBOutlet UIButton *btnSetting;
@property (nonatomic,strong) SettingActionView *settingView;

-(void) createMenuList:(NSArray*)arr;

-(void) show;
@property (nonatomic, copy) callBackDismiss CallBackdDismiss;
-(void) menuListwithPublication:(NSDictionary *)publicationDic;
@end
