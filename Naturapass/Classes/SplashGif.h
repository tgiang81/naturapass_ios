//
//  SplashGif.h
//  Naturapass
//
//  Created by GiangTT on 11/18/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^callBackClose) ();
@interface SplashGif : UIViewController
@property (nonatomic, copy) callBackClose CallBackClose;

@end
