//
//  ChassesCreateV2.m
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "PublicationNew_SelectAnimal.h"
#import "ChassesCreateHeaderCell.h"
#import "ChassesCreateBaseCell.h"
#import "ChassesCreateDateCell.h"
#import "ChassesCreateTextFieldCell.h"
#import "PhotoSheetVC.h"
#import "NSDate+Extensions.h"
#import "DPTextField.h"
#import "AlertSearchMapVC.h"
#import "Publication_Carte.h"
#import "AlertSuggestView.h"
#import "HNKGooglePlacesAutocomplete.h"
#import "ChassesCreateOBJ.h"
#import "MapLocationVC.h"
#import "Define.h"
#import "ChassesCreatePersonneCell.h"
#import "AlertSuggestPersoneView.h"
#import "ChassesCreateAccessCell.h"
#import "ChassesCreateDroitsCell.h"
#import "ChassesCreateLieuCell.h"
#import "ChassesCreateMapCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Naturapass-Swift.h"
#import "NetworkCheckSignal.h"
static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierDate = @"identifierDate";
static NSString *identifierTF = @"identifierTF";

static NSString *identifierPersonne = @"identifierPersonne";
static NSString *identifierAccess = @"identifierAccess";
static NSString *identifierDroits = @"identifierDroits";

static NSString *identifierLieu = @"identifierLieu";
static NSString *identifierMap = @"identifierMap";



typedef enum
{
    debut_date,
    fin_date,
    
}SELECT_DATE;

typedef enum
{
    FIELD_DATE,
    FIELD_PERSONE,
    FIELD_CARTE,
    FIELD_ACCESS,
    FIELD_DROITS,
    FIELD_LIEU
}CHASSES_FIELD_TYPE;
typedef enum
{
    DROITS_NONE,
    DROITS_TOUT,
    DROITS_ADMIN
}CHASSES_DROITS;

@interface PublicationNew_SelectAnimal ()<UITextFieldDelegate, GMSMapViewDelegate , WWCalendarTimeSelectorProtocol>
{
    SELECT_DATE iSelectDate;
    
    NSInteger indexExpand;
    NSData                      *imageData;
    AlertSuggestView *suggestView;
    AlertSuggestPersoneView *suggestViewPersonne;

    CLLocation *my_Placemark;
    NSMutableArray *arrPersonne;
    NSInteger typeSelected;
    BOOL inviterPresonne;
    ACCESS_KIND   m_accessKind;
    
    CHASSES_DROITS   m_droits_pub_add;
    CHASSES_DROITS   m_droits_pub_show;
    CHASSES_DROITS   m_droits_chat_add;
    CHASSES_DROITS   m_droits_chat_show;
    NSMutableArray *dicPhoto;
    NSString *strLatitude, *strLongitude, *strAltitude;
    BOOL isViewMap;
    WebServiceAPI *serviceAddress;
    NSString *strTmpDebut;
    NSString *strTmpFin;
}
@property (strong, nonatomic)  NSString *strAddress;
@property (strong, nonatomic)  NSString *strPersonne;
@property (nonatomic, strong) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (nonatomic, strong) ChassesCreateBaseCell *cellMap;
@end

@implementation PublicationNew_SelectAnimal

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //add sub navigation
    [self.subview removeFromSuperview];
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview removeFromSuperview];
    [self addMainNav:@"MainNavChasses"];
    
    //Set title
    subview =  [self getSubMainView];
    [subview.myTitle setText:@"SIGNALER QUOI ?"];
    //Change background color
    subview.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
    
    
    [self addSubNav:nil];
    
    [self.btnSAUVEGARDER.layer setMasksToBounds:YES];
    self.btnSAUVEGARDER.layer.cornerRadius= 20.0;
    
    [self.btnENVOYER.layer setMasksToBounds:YES];
    self.btnENVOYER.layer.cornerRadius= 25.0;
    self.vContainer.backgroundColor = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);
    self.viewTop.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateHeaderCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateDateCell" bundle:nil] forCellReuseIdentifier:identifierDate];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextFieldCell" bundle:nil] forCellReuseIdentifier:identifierTF];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreatePersonneCell" bundle:nil] forCellReuseIdentifier:identifierPersonne];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateAccessCell" bundle:nil] forCellReuseIdentifier:identifierAccess];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateDroitsCell" bundle:nil] forCellReuseIdentifier:identifierDroits];

    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateLieuCell" bundle:nil] forCellReuseIdentifier:identifierLieu];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateMapCell" bundle:nil] forCellReuseIdentifier:identifierMap];

    self.tableControl.estimatedRowHeight = 60;
    self.tableControl.estimatedSectionHeaderHeight = 40;
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    self.arrData = [NSMutableArray new];
    
    [_imgPhotoSelected.layer setMasksToBounds:YES];
    _imgPhotoSelected.layer.cornerRadius= 35.0;
    _imgPhotoSelected.image = [UIImage imageNamed:@"icon_chasses_placehoder"];
    NSString *currentDate = [NSDate currentStandardUTCTime];
    NSString *strDayMonth = @"";
    NSString *strHour = @"";

    //17/08/2018 21:13

    if (currentDate.length > 10) {
        strDayMonth = [currentDate substringWithRange:NSMakeRange(0, 10)];
        strHour = [currentDate substringWithRange:NSMakeRange(11, 2)];
    }
    _tfNom.text = [NSString stringWithFormat:@"CHASSE DU %@",[strDayMonth stringByReplacingOccurrencesOfString:@"/" withString:@"-"]];
    indexExpand = 0;
    arrPersonne = [NSMutableArray new];
    inviterPresonne = TRUE;
    serviceAddress = [WebServiceAPI new];

    [self InitializeKeyboardToolBar];
    if([strHour intValue] <= 19 )
        {
            strTmpDebut = [NSString stringWithFormat:@"%@ 08:00",[currentDate substringWithRange:NSMakeRange(0, 10)]];
            strTmpFin = [NSString stringWithFormat:@"%@ 19:00",[currentDate substringWithRange:NSMakeRange(0, 10)]];

        }
    else
    {
        strTmpDebut = currentDate;
        strTmpFin = currentDate;
    }
    m_accessKind = ACCESS_SEMIPRIVATE;
    NSDictionary *dicDate   = @{@"name":@"DATE ET HEURE",
                                @"icon":@"icon_chasses_date",
                                @"type":@(FIELD_DATE)};
    NSDictionary *dicPerson = @{@"name":@"INVITER DES PERSONNES",
                                @"icon":@"icon_chasses_persone",
                                @"type":@(FIELD_PERSONE),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_search_user"
                                };
    /*
    NSDictionary *dicCarte  = @{@"name":@"DÉFINIR LE TERRITOIRE",
                                @"icon":@"icon_chasses_carte",
                                @"type":@(FIELD_CARTE),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_address"
                                };
     */
    NSDictionary *dicShare  = @{@"name":@"GESTION DES ACCÈS",
                                @"icon":@"icon_chasses_access",
                                @"type":@(FIELD_ACCESS),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_address",
                                @"contents":@[
                                        @{@"name":@"PUBLIC",
                                          @"desc":@"Tout le monde peut rejoindre cet événement",
                                          @"icon":@"icon_chasses_lock_open",
                                          @"access":@(ACCESS_PUBLIC)
                                          },
                                        @{@"name":@"SEMI-PRIVÉ",
                                          @"desc":@"Tout le monde peut demander \nI'accès à cet événement",
                                          @"icon":@"icon_chasses_lock_close",
                                          @"access":@(ACCESS_SEMIPRIVATE)
                                          },
                                        @{@"name":@"PRIVÉ",
                                          @"desc":@"ll faut être invité pour avoir connaissance de cet événement",
                                          @"icon":@"icon_chasses_lock_close",
                                          @"access":@(ACCESS_PRIVATE)
                                          }
                                        ]
                                };
    NSDictionary *dicDroits  = @{@"name":@"GESTION DES DROITS",
                                @"icon":@"icon_chasses_share",
                                @"type":@(FIELD_DROITS),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_address",
                                @"contents":@[
                                        @{@"name":@"Discussions",
                                          @"access":@(0),
                                          },
                                        @{@"name":@"Publications",
                                          @"access":@(1),
                                          },
                                        ]
                                };
    //default
    m_droits_pub_add = DROITS_TOUT;
    m_droits_pub_show = DROITS_TOUT;
    m_droits_chat_add = DROITS_TOUT;
    m_droits_chat_show = DROITS_TOUT;
    NSDictionary *dicLieu  = @{@"name":@"LIEU DU RDV",
                               @"icon":@"icon_chasses_lieu",
                               @"type":@(FIELD_LIEU),
                               @"placehold":@"Entrez une adresse...",
                               @"icontf":@"icon_chasses_address"};
    [self.arrData addObject:dicDate];
    [self.arrData addObject:dicPerson];
    [self.arrData addObject:dicLieu];
//    [self.arrData addObject:dicCarte];
    [self.arrData addObject:dicDroits];
    [self.arrData addObject:dicShare];

    suggestView = [[AlertSuggestView alloc] initSuggestView];
    suggestViewPersonne = [[AlertSuggestPersoneView alloc] initSuggestView];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR) ];
    self.strAddress = [ChassesCreateOBJ sharedInstance].address;
    [self.tableControl reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//MARK: - TABLE HEADER
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dicData = self.arrData[section];
    ChassesCreateHeaderCell *cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
    cell.lbTitle.text = dicData[@"name"];
    cell.imgIcon.image = [UIImage imageNamed:dicData[@"icon"]];
    if (indexExpand == section) {
        cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_up"];
    }
    else
    {
        cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow"];
    }
    [cell.btnSelected addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnSelected.tag = section;
    cell.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.5;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    CGRect rect = tableView.frame;
    UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 0.5)];
    viewFooter.backgroundColor = [UIColor whiteColor];
    return viewFooter;
}


//Mark:- Delegate Calendar

-(void) WWCalendarTimeSelectorDone:(WWCalendarTimeSelector *)selector date:(NSDate *)date
{
    NSLog(@"%@",date);
    //    Sat Aug 18 15:00:00 2018
    
    //Convert date local -> date Paris
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSString *date_Local = [formatter_local stringFromDate:date];
    
    ASLog(@"%@", date_Local);
    
    switch (iSelectDate) {
        case debut_date:
        {
            strTmpDebut = date_Local;
            //Auto set for textField Fin if empty
            if (strTmpFin.length >0) {
                if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                {
                    strTmpFin = strTmpDebut;
                }
            }
            else
            {
                strTmpFin = strTmpDebut;
            }
            [self.tableControl reloadData];
        }
            break;
        default:
            {
                strTmpFin = date_Local;
                
                //Auto set for textField Fin if empty
                if (strTmpDebut.length >0) {
                    if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                    {
                        strTmpDebut = strTmpFin;
                    }
                }
                else
                {
                    strTmpDebut = strTmpFin;
                }
                [self.tableControl reloadData];
            }
            break;
    }
    
}
-(NSDate *)convertNSStringDateToNSDate:(NSString *)string
{
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSDate *convertedDate = [dateFormatter dateFromString:dateString];
    //[dateFormatter release];
    return convertedDate;
}
-(IBAction)fnDebutDate:(id)sender
{
    iSelectDate = debut_date;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
                                               }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = TRUE;
    viewController1.optionStyles.showTime = FALSE;
    if(strTmpDebut.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpDebut];

    }
}
-(IBAction)fnDebutHour:(id)sender
{
    iSelectDate = debut_date;

    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
    }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = FALSE;
    viewController1.optionStyles.showTime = TRUE;
    viewController1.optionStyles.showMonth = FALSE;
    viewController1.optionStyles.showYear = FALSE;
    if(strTmpDebut.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpDebut];
        
    }
}

-(IBAction)fnFinDate:(id)sender
{
    iSelectDate = fin_date;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
    }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = TRUE;
    viewController1.optionStyles.showTime = FALSE;
    if(strTmpFin.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpFin];
        
    }
}

-(IBAction)fnFinHour:(id)sender
{
    iSelectDate = fin_date;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
    }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = FALSE;
    viewController1.optionStyles.showTime = TRUE;
    viewController1.optionStyles.showMonth = FALSE;
    viewController1.optionStyles.showYear = FALSE;
    if(strTmpFin.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpFin];
        
    }
}


#pragma mark - TABLEVIEW
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[ChassesCreateBaseCell class]])
    {
        NSDictionary *dicData = self.arrData[indexPath.section];
        ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)cellTmp;
        cell.btnAddress.hidden = TRUE;
        cell.tfInput.text = @"";
        [cell.tfInput removeTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
        switch ([dicData[@"type"] intValue]) {
            case FIELD_DATE:
                {
                    NSDictionary *dicDebut = [self convertDateToNSTring:strTmpDebut];
                    NSDictionary *dicFin = [self convertDateToNSTring:strTmpFin];

                    cell.lbDebutDate.text = dicDebut[@"date"];
                    cell.lbDebutTime.text = dicDebut[@"time"];

                    cell.lbFinDate.text = dicFin[@"date"];
                    cell.lbFinTime.text = dicFin[@"time"];

                    [cell.btnDebutDate addTarget:self action:@selector(fnDebutDate:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnDebutHour addTarget:self action:@selector(fnDebutHour:) forControlEvents:UIControlEventTouchUpInside];

                    [cell.btnFinDate addTarget:self action:@selector(fnFinDate:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnFinHour addTarget:self action:@selector(fnFinHour:) forControlEvents:UIControlEventTouchUpInside];

                }
                break;
            case FIELD_PERSONE:
            {
                [cell addPersone:[arrPersonne copy]];
                [cell doBlockTokenDelete:^(NSInteger index) {
                    NSDictionary *dicTmp = [arrPersonne[index] copy];
                    
                    [arrPersonne removeObjectAtIndex:index];
                    [cell addPersone:[arrPersonne copy]];
                    [self.tableControl reloadData];
//                    [self updateSuggestView:cell.tfInput];
                    [suggestViewPersonne removeItemSelected:dicTmp];
                }];
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                cell.tfInput.text = self.strPersonne;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
//                cell.lbTitleAmis.text = [NSString stringWithFormat:@"Inviter tous mes amis (%lu):",(unsigned long)arrPersonne.count];
//                cell.swAmis.layer.cornerRadius = 16;
//                [cell.swAmis setBackgroundColor:UIColorFromRGB(OFF_SWITCH)];
//                cell.swAmis.on = inviterPresonne;
//                [cell.swAmis addTarget:self action:@selector(swChangeAction:) forControlEvents:UIControlEventValueChanged];
                cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];


            }
                break;
//            case FIELD_CARTE:
//            {
//            }
//                break;
            case FIELD_ACCESS:
            {
                NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
                cell.lbTitle.text = dicItem[@"name"];
                cell.lbDesc.text = dicItem[@"desc"];
                cell.imgIcon.image = [UIImage imageNamed:dicItem[@"icon"]];
                if([dicItem[@"access"] intValue] == m_accessKind)
                {
                    cell.imgCheckbox.image = [UIImage imageNamed:@"ic_chasse_create_checkbox_active"];
                }
                else
                {
                    cell.imgCheckbox.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                }

            }
                break;
            case FIELD_DROITS:
            {
                NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
                cell.lbTitle.text = dicItem[@"name"];
                cell.lbTout.text = @"Tout le\nmonde";
                cell.lbAdmin.text = @"Admin";
                cell.lbDiscuter.text = @"Publier";
                cell.lbConsulter.text = @"Consulter";
                [cell.btnToutDiscuter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];

                [cell.btnAdminDiscuter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];


                [cell.btnToutConsulter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];


                [cell.btnAdminConsulter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];

                
                cell.imgToutDiscuter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgAdminDiscuter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgToutConsulter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgAdminConsulter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                UIImage *imageActive =[UIImage imageNamed:@"ic_chasse_create_checkbox_active"];
                //Publication
                if([dicItem[@"access"] intValue] == 1)
                {
                    cell.btnToutDiscuter.tag = 5;
                    cell.btnAdminDiscuter.tag = 6;
                    cell.btnToutConsulter.tag = 7;
                    cell.btnAdminConsulter.tag = 8;
                    
                    cell.lbDiscuter.text = @"Publier";
                    //add
                    if(m_droits_pub_add == DROITS_TOUT)
                    {
                        cell.imgToutDiscuter.image = imageActive;
                    }
                    else if (m_droits_pub_add == DROITS_ADMIN)
                    {
                        cell.imgAdminDiscuter.image = imageActive;
                    }
                    
                    //Conssulter
                    if(m_droits_pub_show == DROITS_TOUT)
                    {
                        cell.imgToutConsulter.image = imageActive;

                    }
                    else if (m_droits_pub_show == DROITS_ADMIN)
                    {
                        cell.imgAdminConsulter.image = imageActive;
                    }
                }
                //Discussion
                else
                {
                    cell.lbDiscuter.text = @"Discuter";
                    cell.btnToutDiscuter.tag = 1;
                    cell.btnAdminDiscuter.tag = 2;
                    cell.btnToutConsulter.tag = 3;
                    cell.btnAdminConsulter.tag = 4;
                    //add
                    if(m_droits_chat_add == DROITS_TOUT)
                    {
                        cell.imgToutDiscuter.image = imageActive;
                    }
                    else if (m_droits_chat_add == DROITS_ADMIN)
                    {
                        cell.imgAdminDiscuter.image = imageActive;
                    }
                    
                    //Conssulter
                    if(m_droits_chat_show == DROITS_TOUT)
                    {
                        cell.imgToutConsulter.image = imageActive;
                        
                    }
                    else if (m_droits_chat_show == DROITS_ADMIN)
                    {
                        cell.imgAdminConsulter.image = imageActive;
                    }
                }
                
            }
                break;
            case FIELD_LIEU:
            {
                if(indexPath.row == 0)
                {
                cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                cell.tfInput.placeholder = dicData[@"placehold"];
                [cell.btnOpenMap addTarget:self action:@selector(openLieuMap:) forControlEvents:UIControlEventTouchUpInside];
                self.cellMap = cell;
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                cell.tfInput.text = self.strAddress;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                }
                else
                {
                    if (isViewMap) {
                        [cell.btnCloseMap addTarget:self action:@selector(closeLieuMap:) forControlEvents:UIControlEventTouchUpInside];
                        cell.mapView_.delegate = self;
                        [cell fnSetLocation:[ChassesCreateOBJ sharedInstance].latitude withLongitude:[ChassesCreateOBJ sharedInstance].longitude];

                    }
                    else
                    {
                        [cell.btnOpenMap addTarget:self action:@selector(openLieuMap:) forControlEvents:UIControlEventTouchUpInside];
                        self.cellMap = cell;
                    }
                }
            }
                break;
            default:
                cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                cell.tfInput.placeholder = dicData[@"placehold"];
                break;
        }
    }
    
}
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
        return self.arrData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        if (indexExpand == section)
        {
            NSDictionary *dicData = self.arrData[section];

            if([dicData[@"type"] intValue] == FIELD_ACCESS)
            {
                return 3;
            }
            if([dicData[@"type"] intValue] == FIELD_DROITS)
            {
                return 2;
            }
            if([dicData[@"type"] intValue] == FIELD_LIEU)
            {
                return 2;
            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 0;
        }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChassesCreateBaseCell *cell = nil;
    NSDictionary *dicData = self.arrData[indexPath.section];
    switch ([dicData[@"type"] intValue]) {
        case FIELD_DATE:
        {
            cell = (ChassesCreateDateCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierDate forIndexPath:indexPath];

        }
            break;
        case FIELD_PERSONE:
        {
            cell = (ChassesCreateDateCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPersonne forIndexPath:indexPath];
            
        }
            break;
        case FIELD_ACCESS:
        {
            cell = (ChassesCreateAccessCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierAccess forIndexPath:indexPath];
            
        }
            break;
        case FIELD_DROITS:
        {
            cell = (ChassesCreateDroitsCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierDroits forIndexPath:indexPath];
            
        }
            break;
        case FIELD_LIEU:
        {
            if(indexPath.row == 0)
            {
                cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            }
            else
            {
                if(isViewMap)
                {
                    cell = (ChassesCreateMapCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMap forIndexPath:indexPath];

                }
                else
                {
                    cell = (ChassesCreateLieuCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierLieu forIndexPath:indexPath];
                }

            }

        }
            break;
        default:
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            break;
    }

        if (cell) {
            [self configureCell:cell forRowAtIndexPath:indexPath];
        }
    cell.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self resignKeyboard:nil];
    NSDictionary *dicData = self.arrData[indexPath.section];
    if([dicData[@"type"] intValue] == FIELD_ACCESS)
    {
            NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
        m_accessKind = [dicItem[@"access"] intValue];
        [self.tableControl reloadData];
    }
    else if([dicData[@"type"] intValue] == FIELD_DROITS)
    {
    }
}
-(IBAction)expandAction:(id)sender
{
    [self resignKeyboard:nil];
    if (indexExpand == [sender tag]) {
        indexExpand = -1;
    }
    else
    {
        indexExpand = [sender tag];
    }
    [self.tableControl reloadData];
}
- (IBAction)profileImageOptions:(id)sender{
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    vc.expectTarget = ISCREATEAGENDA;
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.imgPhotoSelected.image = imgData;
        _btnClosePhoto.hidden = NO;
        _imgClosePhoto.hidden = NO;
        //resize image
        imageData = UIImageJPEGRepresentation([[AppCommon common] resizeImage:imgData targetSize:CGSizeMake(128, 128)], 1.0);
    }];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:vc animated:NO completion:^{}];
    
}
- (IBAction)closePhotoAction:(id)sender{
    _imgPhotoSelected.image = [UIImage imageNamed:@"icon_chasses_placehoder"];
    _btnClosePhoto.hidden = YES;
    _imgClosePhoto.hidden = YES;
    imageData = nil;
}
-(NSDictionary *)convertDateToNSTring:(NSString *)string
{
    
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    
    
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    
     [inputDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
     
     [inputDateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [inputDateFormatter setDateFormat: @"dd/MM/yyyy HH:mm"];
    NSDate *date = [inputDateFormatter dateFromString:dateString];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
     
     [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [dateFormatter setDateFormat: @"dd MMMM yyyy-HH:mm"];
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    if (convertedDate) {
        NSArray *arrDate = [convertedDate componentsSeparatedByString:@"-"];
        return @{@"date":arrDate[0],@"time":arrDate[1]};
    }
    return nil;
}
-(IBAction)showAlertSearch:(id)sender
{
    /*
    AlertSearchMapVC *vcSearch = [[AlertSearchMapVC alloc] init];
    
    [vcSearch doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
        //enable request
        self.strAddress = addressString;
        [self.tableControl reloadData];
    }];
    [vcSearch showAlert];
    */

}
-(void)createSuggestViewWithSuperView:(UIView*)view withRect:(CGRect)rect withIndexPath:(NSIndexPath*)indexPath
{
    switch (typeSelected) {
        case FIELD_CARTE:
        case FIELD_LIEU:
        {
            [suggestView fnDataInput:self.strAddress];
            [suggestView doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
                [self resignKeyboard:nil];
                if (m_Placemark) {
                    my_Placemark = m_Placemark;
                    self.strAddress = addressString;
                    
                    float lat = m_Placemark.coordinate.latitude;
                    float lng = m_Placemark.coordinate.longitude;
                    
                    [ChassesCreateOBJ sharedInstance].address = self.strAddress ;
                    [ChassesCreateOBJ sharedInstance].latitude =[NSString stringWithFormat:@"%f",lat];
                    [ChassesCreateOBJ sharedInstance].longitude = [NSString stringWithFormat:@"%f",lng];
                    
                    [self.tableControl reloadData];
                }
                
            }];
            [suggestView showAlertWithSuperView:view withRect:rect];
        }
        break;
        case FIELD_PERSONE:
        {
            [suggestViewPersonne fnDataInput:self.strPersonne withArrSelected:arrPersonne];
            [suggestViewPersonne doBlock:^(NSArray *arrResult) {
                [self resignKeyboard:nil];
                [arrPersonne removeAllObjects];
                if(arrResult)
                {
                    [arrPersonne addObjectsFromArray:arrResult];
                }
                [self.tableControl reloadData];
            }];
            [suggestViewPersonne showAlertWithSuperView:view withRect:rect];

        }
        break;
        default:
        break;
    }

}
-(void)updateSuggestView:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    
    [suggestView showAlertWithSuperView:self.tableControl withRect:rectSubView];
}
-(void)removeSuggestView
{
    if([suggestViewPersonne isDescendantOfView:self.tableControl]) {
        [suggestViewPersonne closeAction:nil];
    }
    if([suggestView isDescendantOfView:self.tableControl]) {
        [suggestView closeAction:nil];
    }
}
//MARK:- searBar
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    [self removeSuggestView];

}
    -(IBAction)swChangeAction:(UISwitch*)sender
    {
        inviterPresonne = sender.on;
        [self.tableControl reloadData];
    }
-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    NSString *strsearchValues= searchText.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    searchText.text=strsearchValues;
    [self processLoungesSearchingURL:searchText.text];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *strsearchValues= textField.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    textField.text=strsearchValues;
    [self processLoungesSearchingURL:strsearchValues];
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{    
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    NSDictionary *dicData = self.arrData[indexPath.section];
    typeSelected = [dicData[@"type"] intValue];
    [self createSuggestViewWithSuperView:self.tableControl withRect:rectSubView withIndexPath:indexPath];
    [self.tableControl bringSubviewToFront:cell.viewInput];

    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    [self removeSuggestView];
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    switch (typeSelected) {
        case FIELD_CARTE:
        case FIELD_LIEU:
        {
            self.strAddress = searchText;

            [suggestView processLoungesSearchingURL:searchText];
        }
        break;
        case FIELD_PERSONE:
        {
            self.strPersonne = searchText;
            [suggestViewPersonne processLoungesSearchingURL:searchText];
        }
        break;
        default:
        break;
    }
}
-(IBAction)openMapLocation:(id)sender
{
    Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
    if ([ChassesCreateOBJ sharedInstance].latitude && [ChassesCreateOBJ sharedInstance].longitude) {
        viewController1.simpleDic = @{@"latitude":  [ChassesCreateOBJ sharedInstance].latitude,
                                      @"longitude":  [ChassesCreateOBJ sharedInstance].longitude};
    }
    viewController1.expectTarget = ISCREATEAGENDA;
    [self pushVC:viewController1 animate:YES expectTarget:ISCREATEAGENDA iAmParent:NO];

}
-(IBAction)openLieuMap:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];

//    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:0 inSection:indexPath.section];

    isViewMap = !isViewMap;
    if (!isViewMap) {
        //hide suggest
        [self resignKeyboard:nil];
    }
    [self.tableControl reloadData];
//    [self.tableControl scrollToRowAtIndexPath:lastIndexPath
//                         atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
-(IBAction)closeLieuMap:(id)sender
{
    isViewMap = FALSE;
    [self.tableControl reloadData];
}
//MARK: - DROITS
-(IBAction)droitsAction:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    NSInteger tag = [sender tag];
    switch (tag) {
            //ToutDiscuter Chat
        case 1:
        {
            m_droits_chat_add = DROITS_TOUT;
        }
            break;
            //AdminDiscuter Chat
        case 2:
        {
            m_droits_chat_add = DROITS_ADMIN;

        }
            break;
            //ToutConsulter Chat
        case 3:
        {
            m_droits_chat_show = DROITS_TOUT;
        }
            break;
            //AdminConsulter Chat
        case 4:
        {
            m_droits_chat_show = DROITS_ADMIN;
        }
            break;
            //ToutDiscuter Pub
        case 5:
        {
            m_droits_pub_add = DROITS_TOUT;
        }
            break;
            //AdminDiscuter Pub
        case 6:
        {
            m_droits_pub_add = DROITS_ADMIN;

        }
            break;
            //ToutConsulter Pub
        case 7:
        {
            m_droits_pub_show = DROITS_TOUT;

        }
            break;
            //AdminConsulter Pub
        case 8:
        {
            m_droits_pub_show = DROITS_ADMIN;

        }
            break;
        default:
            break;
    }
    [self.tableControl reloadData];
}
//MARK: - MAPVIEW DELEGATE

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = mapView.camera.target.latitude;
    float lng = mapView.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
    [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(fnGetAddress) withObject:nil afterDelay:1];
}
-(void)fnGetAddress
{
    if(strLatitude.length == 0 && strLongitude.length == 0)
    {
        return;
    }
    /*
    __weak typeof(self) wself = self;
    [serviceAddress cancelTask];
    [serviceAddress getInfoLocation:strLatitude withLng:strLongitude];
    
    serviceAddress.onComplete = ^(NSDictionary*response, int errCode){
        NSLog(@"address : %@",response);
        if ([response[@"results"] isKindOfClass: [NSArray class]] && [response[@"status"] isEqualToString:@"OK"])
        {
            
            NSArray *retArr = response[@"results"];
            
            NSString * strLocation = @"";
            
            BOOL alreadyGotAddress = NO;
            
            for (NSDictionary*mDic in retArr)
            {
                NSArray*tmpArr = mDic[@"types"];
                
                if ([tmpArr containsObject :@"administrative_area_level_1"])
                {
                    alreadyGotAddress = YES;
                    
                    strLocation =  mDic[@"formatted_address"];
                    
                    break;
                }
            }
            
            if (alreadyGotAddress == NO) {
                if (retArr.count > 0) {
                    NSDictionary*mDic = retArr[0];
                    strLocation =  mDic[@"formatted_address"];
                }
            }
            wself.strAddress = strLocation;
            wself.cellMap.tfInput.text = wself.strAddress;
        }
        
    };
    */
    __weak typeof(self) wself = self;
    if ([COMMON isReachable])
    {
        [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
            
            //i got the address
            [wself fnSetAddress:mAddress];
            
        } withData:@{@"latitude" : strLatitude,
                     @"longitude" : strLongitude}];
    }
}
-(void)fnSetAddress:(NSString*)mAddress
{
    //i got the address
    self.strAddress = mAddress;
    self.cellMap.tfInput.text = self.strAddress;
//    [self.tableControl reloadData];
}
//MARK: - CREATE AGENDA
- (IBAction)annulerAgendaAction:(id)sender {
    [self gotoback];
}
- (IBAction)createAgendaAction:(id)sender {
    
    strLatitude = [ChassesCreateOBJ sharedInstance].latitude;
    strLongitude = [ChassesCreateOBJ sharedInstance].longitude;

    if (strLatitude == nil || strLongitude == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        strLatitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.latitude];

        strLongitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.longitude];
    }
    
    NSString *strName = [_tfNom.text emo_emojiString];
    
    [ChassesCreateOBJ sharedInstance].strName =strName;
    [ChassesCreateOBJ sharedInstance].strComment =@"";

    if (imageData) {
        [ChassesCreateOBJ sharedInstance].imgData =imageData;
    }
    
    if(strName.length == 0)
    {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
        return;
    }
    
    NSDateFormatter *dateFormatter = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    NSString *strEndDate= strTmpFin;
    NSDate *finDate = [dateFormatter dateFromString: strEndDate];
    //
    NSString *strStartDate= strTmpDebut;
    NSDate *DebutDate = [dateFormatter dateFromString: strStartDate];
    
    /**/
    if([DebutDate compare: finDate] == NSOrderedDescending) // if start is later in time than end
    {
        strTmpFin = @"";
        [self.tableControl reloadData];
        // do something
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strAlertTimeIssue) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [salonAlert show];
        return;
    }
    [ChassesCreateOBJ sharedInstance].strFin = [dateFormatter stringFromDate:finDate];
    [ChassesCreateOBJ sharedInstance].strDebut = [dateFormatter stringFromDate:DebutDate];

    if(strLatitude.length == 0 || strLongitude.length == 0)
    {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
        return;
    }
    
    [ChassesCreateOBJ sharedInstance].allow_add = (m_droits_pub_add == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_show = (m_droits_pub_show == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_chat_add = (m_droits_chat_add == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_chat_show = (m_droits_chat_show == DROITS_TOUT)? YES : NO;
    
    [ChassesCreateOBJ sharedInstance].accessKind =m_accessKind;

//    [COMMON addLoading:self];
//
//    WebServiceAPI *serviceObj = [WebServiceAPI new];
//    [serviceObj getInfoLocation:strLatitude withLng:strLongitude];
//
//    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//        [COMMON removeProgressLoading];
//
//        if ([response[@"results"] isKindOfClass: [NSArray class]])
//        {
//
//            NSArray *retArr = response[@"results"];
//
//            NSString * strLocation = @"";
//
//            BOOL alreadyGotAddress = NO;
//
//            for (NSDictionary*mDic in retArr)
//            {
//                NSArray*tmpArr = mDic[@"types"];
//
//                if ([tmpArr containsObject :@"administrative_area_level_1"])
//                {
//                    alreadyGotAddress = YES;
//
//                    strLocation =  mDic[@"formatted_address"];
//
//                    break;
//                }
//            }
//
//            if (alreadyGotAddress == NO) {
//                if (retArr.count > 0) {
//                    NSDictionary*mDic = retArr[0];
//                    strLocation =  mDic[@"formatted_address"];
//                }
//            }
    
            ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
            
            NSString *strAccess =[NSString stringWithFormat:@"%u",obj.accessKind];

            UIImage *image = [UIImage imageWithData:obj.imgData];
            NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
            
            NSDictionary *attachment;
            if(image)
            {
                attachment = @{@"kFileName": @"image.png",
                               @"kFileData": fileData};
            }
            NSDictionary *dicGeo = nil;
            //mld: address
            NSString * strLocation = self.strAddress;

            if (strLocation)
            {
                dicGeo =@{@"address":strLocation,
                          @"latitude":strLatitude,
                          @"longitude":strLongitude};
            }else{
                dicGeo =@{@"address":@"",
                          @"latitude":strLatitude,
                          @"longitude":strLongitude};
            }
            //Member
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < arrPersonne.count; i++ )
    {
        NSDictionary*dic = arrPersonne[i];
        [mutArr addObject:dic[@"id"]];
    }
    NSString *strPersonne = [mutArr componentsJoinedByString:@","];
    NSMutableDictionary *dicLounge = [@{@"name":obj.strName,
                                  @"description":obj.strComment,
                                  @"geolocation":@"0", //1 option for admin to allow user select GEO or not... Now is disabled
                                  @"access":strAccess,
                                  @"meetingAddress":dicGeo,
                                  @"meetingDate":obj.strDebut,
                                  @"endDate":obj.strFin,
                                  //ggtt
                                  @"allow_add":  [NSNumber numberWithBool:obj.allow_add]  ,
                                  @"allow_show": [NSNumber numberWithBool:obj.allow_show],
                                  @"allow_add_chat":  [NSNumber numberWithBool:obj.allow_chat_add]  ,
                                  @"allow_show_chat": [NSNumber numberWithBool:obj.allow_chat_show]
                                  
                                   } mutableCopy];
    if (strPersonne.length > 0) {
        [dicLounge setObject:strPersonne forKey:@"invitePersons"];

    }
            NSDictionary* postDict = @{@"lounge":   dicLounge};

            [ChassesCreateOBJ sharedInstance].attachment = [attachment copy];
            [ChassesCreateOBJ sharedInstance].address = strLocation;
            [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
            [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
            
            //update
            
            if (obj.isModifi) {
                //put as json...without image
                [[ChassesCreateOBJ sharedInstance]  modifiChassesWithVC:self];
            }else{
                [self getTree];
                //post multi-form
                
               dicPhoto =[NSMutableArray new];
                
                //SET DATA FORM
                [self processParsedObjectPhoto: postDict];

                //create new
                
                WebServiceAPI *serviceAPI =[WebServiceAPI new];
                [serviceAPI postLoungePhotoAction:dicPhoto withAttachmentMediaDic:attachment];
                serviceAPI.onComplete =^(id response, int errCode)
                {
                    [COMMON removeProgressLoading];
                    if ( [response[@"hunt"] isKindOfClass:[NSDictionary class]]) {
                        
                        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                        //update DB.
                        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                        [app updateAllowShowAdd:response];
                        
                        [ChassesCreateOBJ sharedInstance].strID  = response[@"hunt"][@"id"];
//                        if(arrPersonne.count > 0)
//                        {
//                            [self inviteMemberWitdChasses:[ChassesCreateOBJ sharedInstance].strID];
//                        }
//                        else
//                            {
                                [self gotoback];
//                            }
                    }
                    
                    
                };
                
            }
//
//        }else{
//
//            [COMMON removeProgressLoading];
//
//        }
//
//    };
    
}
     -(void)processParsedObjectPhoto:(id)object{
         [self processParsedObjectPhoto:object depth:0 parent:nil path:nil];
     }
     
     -(void)processParsedObjectPhoto:(id)object depth:(int)depth parent:(id)parent path:(NSString*)strPath{
         
         if([object isKindOfClass:[NSDictionary class]]){
             
             for(NSString * key in [object allKeys]){
                 id child = [object objectForKey:key];
                 if (!strPath) {
                     [self processParsedObjectPhoto:child depth:depth+1 parent:object path: key];
                 }else{
                     [self processParsedObjectPhoto:child depth:depth+1 parent:object path: [NSString stringWithFormat:@"%@[%@]",strPath,key ]];
                 }
             }
         }else if([object isKindOfClass:[NSArray class]]){
             
             for(id child in object){
                 [self processParsedObjectPhoto:child depth:depth+1 parent:object path:strPath];
             }
             
         }
         else{
             
             [dicPhoto addObject:@{@"path": strPath, @"value": [NSString stringWithFormat:@"%@",[object description]] }];
             
         }
     }
     -(void)getTree
    {
        
        if (strLatitude.length>0 && strLongitude.length>0 && [COMMON isReachable])
        {
            //But bad network?
            if (self.expectTarget != ISLOUNGE) {
                [COMMON addLoading:self];
            }
            
            WebServiceAPI *serviceObj = [WebServiceAPI new];
            [serviceObj getCategoriesGeolocated:strLatitude strLng:strLongitude];
            
            serviceObj.onComplete = ^(NSDictionary*response1, int errCode){
                //Has Data
                if (![response1[@"model"] isKindOfClass: [NSNull class]])
                {
                    
                    if ([response1[@"model"] isKindOfClass: [NSString class]])
                    {
                        if ([response1[@"model"] isEqualToString:@"default"])
                        {
                            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                            
                            [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                            // get tree/card from tree cache default
                        }
                        else
                        {
                            // Receiver_1....                                     "model": "receiver_7"
                            NSString *strReceiver = response1[@"model"];
                            
                            //check in the cache
                            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                            
                            if (treeDic[strReceiver] != [NSNull class] && treeDic[strReceiver])
                            {
                                [PublicationOBJ sharedInstance].treeCategory = treeDic[strReceiver];

                            }
                            else //doesn't exist...
                            {
                                strReceiver = [strReceiver stringByReplacingOccurrencesOfString:@"_" withString:@"="];
                                
                                //receiver_7
                                
                                WebServiceAPI *serviceGetTree =[WebServiceAPI new];
                                serviceGetTree.onComplete =^(id response2, int errCode){
                                    if ([response2 isKindOfClass: [NSDictionary class]]) {
                                        
                                        //if not contained -> request get new tree... then merge result to the cache
                                        [[PublicationOBJ sharedInstance] doMergeTreeWithNew:response2];
                                        //Get needed tree
                                        
                                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                                        
                                        [PublicationOBJ sharedInstance].treeCategory = treeDic[response1[@"model"]];

                                    }
                                    else
                                    {
                                        [COMMON removeProgressLoading];
                                    }
                                    
                                };
                                
                                [serviceGetTree fnGET_CATEGORIES_BY_RECEIVER:strReceiver];
                                
                            }
                        }
                    }
                    else if ([response1[@"model"] isKindOfClass: [NSDictionary  class]])
                    {
                        //a real tree. => merge..
                        [COMMON removeProgressLoading];
                        
                    }
                    //A TREE
                    else if ([response1[@"tree"] isKindOfClass: [NSArray  class]])
                    {
                        [PublicationOBJ sharedInstance].treeCategory = response1[@"tree"];
                        
                        return;
                    }else{
                        //Offline
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                        
                    }
                }else{
                    //Offline
                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                    
                    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                    
                }
            };
            
        }
        else
        {
            [AZNotification showNotificationWithTitle:str(strNETWORK) controller:self notificationType:AZNotificationTypeError];
        }
    }
-(void)inviteMemberWitdChasses:(NSString*)strID
{

    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < arrPersonne.count; i++ )
    {
        NSDictionary*dic = arrPersonne[i];
        [mutArr addObject:dic[@"id"]];
    }
    
    //Request
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_USER_LOUNGE:@{@"lounge": @{@"subscribers": mutArr}} loungeID:strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self gotoback];
    };
}
- (void)keyboardWillHide:(NSNotification*)notification {
    [self removeSuggestView];
}
@end
