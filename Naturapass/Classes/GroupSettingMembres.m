//
//  GroupSettingVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupSettingMembres.h"
#import "GroupAdminListVC.h"
#import "GroupSettingOBJ.h"
#import "GroupSettingMembresCell2.h"
#import "GroupMembersHeader.h"
#import "AddFriendCell.h"
#import "TypeCell51.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GroupEnterOBJ.h"
#import "CommonHelper.h"
#import "MDCheckDel.h"
#import "GroupCreate_Step3.h"
static NSString *cellIdentifier2 = @"Cell2";
static NSString *GroupesCellAdmin2 = @"GroupSettingMembresCell2";
//
static NSString *cellIdentifier = @"AddFriendCell";
static NSString *identifierSection1 = @"MyTableViewCell1";
//
static NSString *cellIdentifier3 = @"TypeCell51";
static NSString *identifierSection3 = @"TypeCell51ID";
@interface GroupSettingMembres ()
{


    NSArray        *HeaderTitle;

    NSMutableArray *arrOwner;
    NSMutableArray *arrAdmin;
    NSMutableArray *arrMembers;
    NSMutableArray *arrInvite;
    NSMutableArray *arrWatting;
    NSMutableArray *groupMemberArray;

    int owner_id;
    int sender_id;
    IBOutlet UILabel *lbTitle;

//    NSString *sender_id;
    IBOutlet UIButton *btnInvite;
}
@property (nonatomic, strong) IBOutlet UIButton *btnCreateGroup;
@end

@implementation GroupSettingMembres

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMEMBRES_DU_GROUPE);
    [self.tableControl registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:GroupesCellAdmin2 bundle:nil] forCellReuseIdentifier:cellIdentifier2];
    [self.tableControl registerNib:[UINib nibWithNibName:cellIdentifier3 bundle:nil] forCellReuseIdentifier:identifierSection3];


    HeaderTitle =@[str(strProprietaire),str(strAdministrateurs),str(strMMembres),str(strIInvites),str(strEn_attente_de_validation)];

    [[CommonHelper sharedInstance] setRoundedView:self.btnCreateGroup toDiameter:self.btnCreateGroup.frame.size.height /2];
    
    arrOwner =[NSMutableArray new];
    arrAdmin =[NSMutableArray new];
    arrMembers =[NSMutableArray new];
    arrInvite =[NSMutableArray new];
    arrWatting =[NSMutableArray new];
    groupMemberArray =[NSMutableArray new];
    
    owner_id =(int)[[GroupEnterOBJ sharedInstance].dictionaryGroup[@"owner"][@"id"] intValue];
    sender_id= [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];


}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.isNotifi) {
        self.isNotifi =!self.isNotifi;
        [self gotoNotifi_leaves];
    }
    else
    {
        [self getSubcribeMember: [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
    }
    
    if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
        if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
        {
            //admin
            btnInvite.hidden=NO;
            
        }else{
            btnInvite.hidden =YES;
            
        }
        
    }else{
        btnInvite.hidden =YES;
    }
}

#pragma mark - API
-(void)getSubcribeMember:(NSString*)mykind_id
{


    
    [groupMemberArray removeAllObjects];
    [arrOwner removeAllObjects];
    [arrAdmin removeAllObjects];
    [arrMembers removeAllObjects];
    [arrInvite removeAllObjects];
    [arrWatting removeAllObjects];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    
    if(self.expectTarget ==ISLOUNGE)
    {
        //mld
        [serviceObj getLoungeSubscribeAction:mykind_id];
        
    }
    else
    {
        [serviceObj getGroupToutesSubscribeAction:mykind_id];
        
    }
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        NSMutableArray *friendArray = [[NSMutableArray alloc] init];
        
        friendArray=[response objectForKey:@"subscribers"];
        
        if(friendArray.count>0)
        {
            [groupMemberArray addObjectsFromArray:[friendArray copy]];
            for (NSDictionary *dic in groupMemberArray) {
                //new chinh la owner
                if ([dic[@"user"][@"id"] integerValue] ==owner_id ) {
                    [arrOwner addObject:dic];
                }
                else
                {
                    switch ([dic[@"access"] intValue]) {
                        case USER_ADMIN:
                        {
                            [arrAdmin addObject:dic];
                        }
                            break;
                        case USER_NORMAL:
                        {
                            [arrMembers addObject:dic];
                        }
                            break;
                        case USER_INVITED:
                        {
                            [arrInvite addObject:dic];
                        }
                            break;
                        case USER_WAITING_APPROVE:
                        {
                            [arrWatting addObject:dic];
                        }
                            break;
                            
                        default:
                            break;
                    }
                }
            }
        }
        [self.tableControl reloadData];
    };
}
-(IBAction)gotoInvition:(id)sender
{
    GroupCreate_Step3 *viewController1 = [[GroupCreate_Step3 alloc] initWithNibName:@"GroupCreate_Step3" bundle:nil];
    viewController1.arrSubcriber =groupMemberArray;
    viewController1.fromSetting=YES;
    [GroupCreateOBJ sharedInstance].group_id =[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    
    [self pushVC:viewController1 animate:YES];
}
#pragma mark - validerAction
-(IBAction)validerAction:(UIButton *)sender{
    
    NSMutableArray *arr =[ NSMutableArray new];
    arr= arrWatting;
    int tag = (int)[sender tag];
    int index =tag-1000;
    NSString *groupid=     [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    NSString *UserId= arr[index][@"user"][@"id"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE mykind_id:groupid strUserID:UserId ];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        [self getSubcribeMember:groupid];
    };
}
#pragma mark - doAction
-(IBAction)deleteJoinAction:(UIButton *)sender {
    NSMutableArray *arr =[ NSMutableArray new];
    int tag = (int)[sender tag];
    int index =tag-1100;
    
    arr= arrWatting;
    NSString *groupid=     [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    NSString *UserId= arr[index][@"user"][@"id"];
    
    [COMMON addLoading:self];
    WebServiceAPI *deleteLoungAction = [[WebServiceAPI alloc]init];
    [deleteLoungAction deleteJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE UserID:UserId mykind_id:groupid];
    deleteLoungAction.onComplete =^(NSDictionary *response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        [self getSubcribeMember:groupid];
    };
    
    
}
#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    NSMutableArray *arr =[ NSMutableArray new];
    int tag = (int)[sender tag];
    int index =0;
    if (tag<2000) {
        arr= arrOwner;
        index =tag-1000;
    }
    else if (tag<3000 && tag>=2000)
    {
        arr= arrAdmin;
        index =tag-2000;
    }
    else if (tag<4000 && tag>=3000)
    {
        arr= arrMembers;
        index =tag-3000;
        
    }
    else if (tag<5000 && tag>=4000)
    {
        arr= arrInvite;
        index =tag-4000;
        
    }
    [COMMON addLoading:self];
    NSDictionary *dic = arr[index];
    
    NSString *groupid=     [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"user"][@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOui), nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            [self getSubcribeMember:groupid];

//            //co qh
//            
//            NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
//            NSMutableDictionary *userDic = [mulDic[@"user"] mutableCopy];
//            
//            [userDic removeObjectForKey:@"relation"];
//            [userDic setObject:response[@"relation"] forKey:@"relation"];
//            [mulDic setObject:userDic forKey:@"user"];
//
//            for (int i=0; i<groupMemberArray.count; i++) {
//                NSDictionary *temp =groupMemberArray[i];
//                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
//                    [groupMemberArray replaceObjectAtIndex:i withObject:mulDic];
//                    break;
//                }
//            }
//            //reload item
//            NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
//            [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }

    };
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //is owner
    if (section==0) {
        return arrOwner.count;
    }
    //is admin
    else if (section==1)
    {
        return arrAdmin.count;
    }
    //is normal
    else if (section ==2)
    {
        return arrMembers.count;
    }
    //is invite
    else if (section ==3)
    {
        return arrInvite.count;
    }
    //is wait
    else if (section ==4)
    {
        //is aks
        return arrWatting.count;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==4) {
        return 80;
    }
    return 60+6;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==0 && arrOwner.count>0) {
        return 40;
    }
    else if (section==1 && arrAdmin.count>0)
    {
        return 40;
    }
    else if (section ==2 && arrMembers.count>0)
    {
        return 40;
    }
    else if (section ==3 && arrInvite.count>0)
    {
        return 40;
    }
    else if (section ==4 && arrWatting.count>0)
    {
        return 40;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *strTitle =HeaderTitle[section];
    TypeCell51 *cell = (TypeCell51 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3];
    cell.label1.text=strTitle;
    cell.backgroundColor =UIColorFromRGB(MAIN_COLOR);
    cell.view1.backgroundColor =[UIColor clearColor];
    return  cell;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSMutableArray *arr =[ NSMutableArray new];
    int tag=0;
    if (indexPath.section== 4) {
        
        GroupSettingMembresCell2 *cell1 = (GroupSettingMembresCell2 *)[self.tableControl dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        //title
        cell1.lbTitle.text = arrWatting[indexPath.row][@"user"][@"fullname"];
        //image
        NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,arrWatting[indexPath.row][@"user"][@"photo"]];
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        [cell1.imgViewAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"UserList"] ];
        //button
        cell1.btnValider.tag= indexPath.row+1000;
        [cell1.btnValider addTarget:self action:@selector(validerAction:) forControlEvents:UIControlEventTouchUpInside];
        //button
        cell1.btnRefuser.tag= indexPath.row+1100;
        [cell1.btnRefuser addTarget:self action:@selector(deleteJoinAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];

        return cell1;
    }else if (indexPath.section==0) {
        arr= [arrOwner copy];
        tag=1000;
    }
    else if (indexPath.section==1)
    {
        arr= arrAdmin;
        tag=2000;

    }
    else if (indexPath.section ==2)
    {
        arr= arrMembers;
        tag=3000;

        
    }
    else if (indexPath.section ==3)
    {
        arr= arrInvite;
        tag=4000;

        
    }
    else
    {
        return nil;
    }
    AddFriendCell *cell = nil;
    
    cell = (AddFriendCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    if(indexPath.section== 0)
    {
        //crown
        cell.imageCrown.hidden=NO;
    }
    else
    {
        cell.imageCrown.hidden=YES;
    }
    cell.label1.text = arr[indexPath.row][@"user"][@"fullname"];
    cell.label2.text = arr[indexPath.row][@"user"][@"fullname"];

    //image
    NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,arr[indexPath.row][@"user"][@"photo"]];
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];

    
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];
    ///
    int indexArr =(int)indexPath.row;
    NSDictionary *dic = arr[indexArr];
    BOOL isAddFriendHide =YES;
    if ([dic[@"user"][@"id"] integerValue] ==sender_id) {
        isAddFriendHide= YES;
    }
    else
    {
        NSDictionary *dicRelation =dic[@"user"][@"relation"];
        if ([dicRelation isKindOfClass:[NSDictionary class]]) {
            
            if ([dicRelation[@"friendship"] isKindOfClass:[NSDictionary class]])
            {
                isAddFriendHide =YES;
            }else{
                isAddFriendHide=NO;
            }
        }else{
            isAddFriendHide=NO;
        }
    }
    
    [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    
    if (isAddFriendHide) {
        cell.btnAddFriend.hidden =YES;
        cell.imgAddFriend.hidden=YES;
        
    }else{
        cell.btnAddFriend.hidden=NO;
        cell.imgAddFriend.hidden=NO;
        
        [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.btnAddFriend.tag= indexPath.row+tag;

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
#pragma mark -notifi
-(void)gotoNotifi_leaves
{
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getGroupAction:self.IdNotifi];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        if (response) {
                //
                NSDictionary *dic = response[@"group"];
                [[GroupEnterOBJ sharedInstance] resetParams];
                [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                [self getSubcribeMember: [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];

        }
        
    };
}

@end
