//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCell10.h"
#import "MeoteoView.h"
@implementation TypeCell10
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }

}

-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
    _colorNavigation = color;
}
-(void)fnSetWeatherData:(NSArray*)arrWearther
{
    
    float delta = 88;
    [self.scrollContent setTranslatesAutoresizingMaskIntoConstraints:YES];
    //del constraint
    // With some valid UIView *view:
    for(UIView *subview in [self.scrollContent subviews]) {
        [subview removeFromSuperview];
    }
    for (int i =0; i<arrWearther.count; i++) {
        NSArray *nibArray = nil;
        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MeoteoView" owner:self options:nil];
        MeoteoView *meoteo = (MeoteoView*)[nibArray objectAtIndex:0];
        meoteo.tag = 100+i;
        meoteo.lbTime.textColor = _colorNavigation;
        if (![self.scrollContent.subviews containsObject:[self.scrollContent viewWithTag:100+i]] ) {
            
            
            
            [self.scrollContent addSubview:meoteo];
            [self.scrollContent addConstraint: [NSLayoutConstraint
                                                constraintWithItem:meoteo attribute:NSLayoutAttributeTop
                                                relatedBy:NSLayoutRelationEqual
                                                toItem:self.scrollContent
                                                attribute:NSLayoutAttributeTop
                                                multiplier:1.0 constant:0] ];
     
                [self.scrollContent addConstraint: [NSLayoutConstraint
                                                    constraintWithItem:meoteo attribute:NSLayoutAttributeLeading
                                                    relatedBy:NSLayoutRelationEqual
                                                    toItem:self.scrollContent
                                                    attribute:NSLayoutAttributeLeading
                                                    multiplier:1.0 constant:i*delta] ];
        }
        [meoteo fnDataView:arrWearther[i]];
    }
}
/*
 {
 clouds =     {
 all = 88;
 };
 dt = 1449187200;
 "dt_txt" = "2015-12-04 00:00:00";
 main =     {
 "grnd_level" = "1023.42";
 humidity = 79;
 pressure = "1023.42";
 "sea_level" = "1031.97";
 temp = "288.38";
 "temp_kf" = 0;
 "temp_max" = "288.38";
 "temp_min" = "288.375";
 };
 rain =     {
 };
 sys =     {
 pod = n;
 };
 weather =     (
 {
 description = "overcast clouds";
 icon = 04n;
 id = 804;
 main = Clouds;
 }
 );
 wind =     {
 deg = "226.501";
 speed = "5.37";
 };
 }
 */
@end
