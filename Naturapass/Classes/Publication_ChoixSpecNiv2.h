//
//  Publication_ChoixSpecNiv2.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"


//RECURSIVE
@interface Publication_ChoixSpecNiv2 : PublicationBaseVC

@property (nonatomic, strong) NSDictionary *myDic;
@property(nonatomic,strong) NSString * myName;
@property (strong, nonatomic) IBOutlet UILabel *precisezLabel;

@end
