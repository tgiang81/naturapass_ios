//
//  MesSalonCustomCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 3/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "ChasseInfoCell.h"
#import "CommonHelper.h"

@implementation ChasseInfoCell
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    [self.Img1.layer setMasksToBounds:YES];
    self.Img1.layer.cornerRadius= 30;
    self.Img1.layer.borderWidth =0;

    [self.view1.layer setMasksToBounds:YES];
    self.view1.layer.cornerRadius= 5.0;
    self.view1.layer.borderWidth =0.5;
    self.view1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    UIView *tmpV = [self viewWithTag:POP_MENU_VIEW_TAG2];
    tmpV.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    [[CommonHelper sharedInstance] setRoundedView:self.Img1 toDiameter:self.Img1.frame.size.height /2];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];

            [self.Img7 setHidden:NO];
            [self.view3 setHidden:NO];
            self.Img7.image =[UIImage imageNamed:@"ic_group_admin"];
            
            self.constraintHeight.constant =23;
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            [self.Img7 setHidden:YES];
            [self.view3 setHidden:YES];
            self.constraintHeight.constant =0;

        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];

            [self.Img7 setHidden:NO];
            [self.view3 setHidden:NO];
            self.Img7.image =[UIImage imageNamed:@"ic_chasse_admin"];
            
            self.constraintHeight.constant =23;
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];

            [self.Img7 setHidden:YES];
            [self.view3 setHidden:YES];
            
            self.constraintHeight.constant =0;
        }
            break;

        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
    [self.label3 setTextColor: color];
}
@end
