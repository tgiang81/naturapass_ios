//
//  FriendInfoProfileCell.h
//  Naturapass
//
//  Created by JoJo on 12/17/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "Config.h"
NS_ASSUME_NONNULL_BEGIN

@interface FriendInfoProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@property (weak, nonatomic) IBOutlet UILabel *lbOuilellChasse;
@property (weak, nonatomic) IBOutlet UILabel *lbValueOuilellChasse;

@property (weak, nonatomic) IBOutlet UILabel *lbSesPapiers;
@property (weak, nonatomic) IBOutlet UILabel *lbValueSesPapiers;

@property (weak, nonatomic) IBOutlet UILabel *lbSesArmes;
@property (weak, nonatomic) IBOutlet UILabel *lbValueSesArmes;

@property (weak, nonatomic) IBOutlet UILabel *lbSesChiens;
@property (weak, nonatomic) IBOutlet UILabel *lbValueSesChiens;

@property (weak, nonatomic) IBOutlet UILabel *lbSesTypesChasse;
@property (weak, nonatomic) IBOutlet UILabel *lbValueSesTypesChasse;

@property (weak, nonatomic) IBOutlet UILabel *lbSesPaysChasse;
@property (weak, nonatomic) IBOutlet UILabel *lbValueSesPaysChasse;

@property (weak, nonatomic) IBOutlet UILabel *lbGrade;
@property (weak, nonatomic) IBOutlet UILabel *lbVaueGrade;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property(nonatomic,strong)  UIColor *colorCommon;
@property(nonatomic,strong)  UIColor *colorAnnuler;
-(void)fnSetColorWithExpectTarget:(ISSCREEN)expTarget;
-(void)fnSetFriendInfo:(NSDictionary*)dic;
/*
Où il/elle chasse ? : Bourg-en-Bresse

Ses papiers : Demander I'information

Ses armes : Carabine, fusil

Ses chiens : Setter anglais, Epagneul Breton

Ses types de chasse : Arc

Ses pays de chasse : France

Grade : Observateur
*/

@end

NS_ASSUME_NONNULL_END
