//
//  UploadProgress.m
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "UploadProgress.h"
#import "CellKind12.h"
#import "ServiceHelper.h"
#import "AppCommon.h"
#import "Define.h"

@interface UploadProgress ()
{
    IBOutlet UITableView *tableControl;
    UIColor *color;
}
@property (nonatomic, strong) NSMutableArray *activeConnections;
@property (nonatomic, strong) IBOutlet UIView *viewHeader;
@end

static NSString *identifier = @"CellKind12ID";

@implementation UploadProgress

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view.layer setMasksToBounds:YES];
    self.view.layer.cornerRadius= 4.0;
    self.view.layer.borderWidth =0.5;
    self.view.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    
    switch (self.expectTarget) {
        case ISMUR://MUR
        {
            color = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        case ISCARTE://CARTE
        {
            color = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);

        }
            break;
        case ISDISCUSS://DISCUSSION
        {
            color = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);

        }
            break;
        case ISGROUP://GROUP
        {
            color = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);

        }
            break;
        case ISLOUNGE://CHASSES
        {
            color = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);

        }
            break;
        case ISAMIS://AMIS
        {
            color = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);

        }
            break;
        case ISPARAMTRES://PARAMETERS
        {
            color = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);

        }
            break;
        default:
            break;
    }
    _viewHeader.backgroundColor =color;
    [tableControl registerNib:[UINib nibWithNibName:@"CellKind12" bundle:nil] forCellReuseIdentifier:identifier];
    
    
    self.activeConnections = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view from its nib.
    [tableControl setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self loadData];
    
}

-(void) loadData
{
    [self.activeConnections removeAllObjects];
    
    id value =[[NSUserDefaults standardUserDefaults] objectForKey:concatstring([COMMON getUserId],strPostPublication)];
    if (value) {
        NSDictionary *postingList = (NSDictionary *)value;
        if ([postingList allKeys].count > 0)
        {
            for (NSString *key in postingList) {
                NSDictionary*dataDic = postingList[key];
                [self.activeConnections addObject:@{@"id":dataDic[@"Data"][@"publication"][@"guid"],
                                                    @"type":dataDic[strPostTypeKey],
                                                    @"text":dataDic[@"Data"][@"publication"][@"content"],
                                                    
                                                    @"obj":dataDic,
                                                    @"kind":dataDic[strPostTypeKey],
                                                    
                                                    @"percentage": [NSNumber numberWithFloat:0.0f] } ];
            }
        }
    }
    [tableControl reloadData];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doUpdateProgress:) name: UPDATE_UPLOADING_PROGRESS object:nil];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]  removeObserver:self name:UPDATE_UPLOADING_PROGRESS object:nil];
    [super viewWillDisappear:animated];
    
}
/*
 @{@"id":dataDic[@"myid"],
 @"type":postType,
 @"text":  dataDic[@"Data"][@"publication"][@"content"]}
 */

-(void) doUpdateProgress:(NSNotification*)notif
{
    
    NSDictionary*dic = [notif object];
    int index = -1;
    for (int i=0; i < self.activeConnections.count; i++) {
        NSDictionary*theDic = self.activeConnections[i];
        if ([dic[@"id"] isEqualToString:theDic[@"id"]]) {
            index = i;
            
            //replace
            [self.activeConnections replaceObjectAtIndex:index withObject:dic];
            break;
        }
    }
    
    if (index >= 0) {
        //reload row
        NSIndexPath *indexPath = [ NSIndexPath indexPathForRow:index inSection:0];
        
        if ([dic [@"percentage"] intValue] >= 100) {
            [self.activeConnections removeObjectAtIndex:index];
            [tableControl reloadData];
        }else{
            [tableControl reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                                withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.activeConnections count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind12 *cell = nil;
    
    cell = (CellKind12 *)[tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    // look for active connecton for this cell
    NSDictionary *dict = self.activeConnections[indexPath.row];
    
    [cell.title setText:   [NSString stringWithFormat:@"%@-%@", dict[@"type"],dict[@"text"] ] ];
    cell.progress.progress = [dict [@"percentage"] floatValue]/100;
    cell.btnDelete.tag = 1000 + indexPath.section;
    [cell.btnDelete addTarget:self action:@selector(deleteTask:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.percentage.text = [NSString stringWithFormat:@"%d%%",[dict [@"percentage"] intValue]];
    cell.progress.tintColor =color;
    return cell;
}

-(void)deleteTask:(UIButton*)btn
{
    NSDictionary*dic = self.activeConnections[btn.tag-1000];
    [[ServiceHelper sharedInstance] removePostingTask:dic[@"obj"]];
    
    [self loadData];
}

-(IBAction)fnDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
