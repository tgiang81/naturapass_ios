//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step8.h"
#import "ChassesCreate_Step9.h"
#import "ChassesCreate_Step11.h"

#import "TypeCell31.h"

static NSString *typecell31 =@"TypeCell31";
static NSString *typecell31ID =@"TypeCell31ID";
@interface ChassesCreate_Step8 ()
{
    NSMutableArray *arrData;
}

@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@end

@implementation ChassesCreate_Step8

#pragma mark -setColor
-(void)fnColorTheme
{
    _label1.text =str(strFiltrezLesPointsQueVousImprotez);
    _label2.text =str(strChoisissezLesPointsdeGeo);
    
    //list the tree here...for selecting categories. in last selected page... then suivant.
//    
//    NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_OBSERVATION_SAVE ];
//    
//    NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
//    
//    [PublicationOBJ sharedInstance].dicCategories = dic;
    
//    NSMutableArray *listTree = [PublicationOBJ sharedInstance].dicCategories [@"tree"];


}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fnColorTheme];
    [self initialization];
    self.needChangeMessageAlert = YES;

}

-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell31 bundle:nil] forCellReuseIdentifier:typecell31ID];
}

-(IBAction)choosesInvite:(id)sender
{
    NSInteger index =[sender tag];
    switch (index) {
        case 0:
        {
            //Tous
            ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            
        }
            break;
        case 1:
        {
            //Animaux
            ChassesCreate_Step9 *viewController1 = [[ChassesCreate_Step9 alloc] initWithNibName:@"ChassesCreate_Step9" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        }
            break;
        case 2:
        {
            //Equipements
            ChassesCreate_Step9 *viewController1 = [[ChassesCreate_Step9 alloc] initWithNibName:@"ChassesCreate_Step9" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        }
        case 3:
        {
            //Degats
            ChassesCreate_Step9 *viewController1 = [[ChassesCreate_Step9 alloc] initWithNibName:@"ChassesCreate_Step9" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            
        }
            break;
        default:
            break;
    }

}
@end
