//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

typedef void (^callBackGroup) (NSInteger);
@interface CellKind7 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UISwitch *swControl;
@property (weak, nonatomic) IBOutlet UIImageView *imageLine;

@property (nonatomic, copy) callBackGroup CallBackGroup;

@end
