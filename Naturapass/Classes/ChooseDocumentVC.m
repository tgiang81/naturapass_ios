//
//  ChooseDocumentVC.m
//  MDReadPDF
//
//  Created by Manh on 3/8/16.
//  Copyright © 2016 Manh. All rights reserved.
//

#import "ChooseDocumentVC.h"
#import "PdfCollectionCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CommonHelper.h"
#import "SVPullToRefresh.h"
#import "NSString+Extensions.h"

@implementation ALAsset (Export)

static const NSUInteger BufferSize = 1024*1024;

- (BOOL) exportDataToURL: (NSURL*) fileURL error: (NSError**) error
{
    [[NSFileManager defaultManager] createFileAtPath:[fileURL path] contents:nil attributes:nil];
    NSFileHandle *handle = [NSFileHandle fileHandleForWritingToURL:fileURL error:error];
    if (!handle) {
        return NO;
    }
    
    ALAssetRepresentation *rep = [self defaultRepresentation];
    uint8_t *buffer = calloc(BufferSize, sizeof(*buffer));
    NSUInteger offset = 0, bytesRead = 0;
    
    do {
        @try {
            bytesRead = [rep getBytes:buffer fromOffset:offset length:BufferSize error:error];
            [handle writeData:[NSData dataWithBytesNoCopy:buffer length:bytesRead freeWhenDone:NO]];
            offset += bytesRead;
        } @catch (NSException *exception) {
            free(buffer);
            return NO;
        }
    } while (bytesRead > 0);
    
    free(buffer);
    return YES;
}

@end

@interface ChooseDocumentVC ()
{
    int iCountAssetSelected;
    
}
@property (nonatomic, strong) DirectoryWatcher *docWatcher;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;
@property (nonatomic, strong) NSMutableArray *arrSelected;

@end

@implementation ChooseDocumentVC

-(NSString*) videoAssetURLToTempFile:(NSURL*)url dest:(NSString*) tmpfile
{
    
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        
        ALAssetRepresentation * rep = [myasset defaultRepresentation];
        
        NSUInteger size = [rep size];
        const int bufferSize = 8192;
        
        NSLog(@"Writing to %@",tmpfile);
        FILE* f = fopen([tmpfile cStringUsingEncoding:1], "wb+");
        if (f == NULL) {
            NSLog(@"Can not create tmp file.");
            return;
        }
        
        Byte * buffer = (Byte*)malloc(bufferSize);
        int read = 0, offset = 0, written = 0;
        NSError* err;
        if (size != 0) {
            do {
                read = [rep getBytes:buffer
                          fromOffset:offset
                              length:bufferSize
                               error:&err];
                written = fwrite(buffer, sizeof(char), read, f);
                offset += read;
            } while (read != 0);
            
            
        }
        fclose(f);
        
        
    };
    
    
    ALAssetsLibraryAccessFailureBlock failureblock  = ^(NSError *myerror)
    {
        NSLog(@"Can not get asset - %@",[myerror localizedDescription]);
        
    };
    
    if(url)
    {
        ALAssetsLibrary* assetslibrary = [ALAssetsLibrary new];
        [assetslibrary assetForURL:url
                       resultBlock:resultblock
                      failureBlock:failureblock];
    }
    
    return tmpfile;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [myCollection registerNib:[UINib nibWithNibName:@"PdfCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"PdfCollectionCellID"];
    
    // start monitoring the document directory…
    self.docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:self];
    
    self.documentURLs = [NSMutableArray new];
    self.arrSelected = [NSMutableArray new];
    
    // scan for existing documents
    [self directoryDidChange:self.docWatcher];
    
    //Refresh/loadmore for collection view
    
    __weak ChooseDocumentVC *weakSelf = self;
    
    [myCollection addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    [myCollection addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    
    //fake time to refresh
    [COMMON addLoading:self];
    
    [self performSelector:@selector(setTimer) withObject:myCollection afterDelay:0.75];
    
    ALAssetsLibrary*library = [[ALAssetsLibrary alloc] init];
    
    
    void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != nil) {
            if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]  ||
               [[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]
               ) {
                //                    [assetURLDictionaries addObject:[result valueForProperty:ALAssetPropertyURLs]];
                
                NSURL *url = (NSURL*) [[result defaultRepresentation]url];
                
                [library assetForURL:url resultBlock:^(ALAsset *asset) {
                    if (asset) {
                        UIImage* returnThumbnail =  [UIImage imageWithCGImage: asset.thumbnail];
                        NSURL *urlTmp = [[[asset defaultRepresentation] url] copy];
                        NSString* originalFileName = [[asset defaultRepresentation] filename];
                        
                        [self.documentURLs addObject:@{@"type":@0,@"thumbnail":returnThumbnail,@"filePath":urlTmp,@"fileName":originalFileName, @"keycheck": [url absoluteString]  }];
                    }
                    
                }
                        failureBlock:^(NSError *error){ ASLog(@"operation was not successfull!"); } ];
            }
            
        }
    };
    NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
    
    void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
        if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
            [assetGroups addObject:group];
            
            [myCollection reloadData];

        }
    };
    assetGroups = [[NSMutableArray alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupAll
                           usingBlock:assetGroupEnumerator
                         failureBlock:^(NSError *error) { ASLog(@"There is an error"); }];
    
}

#pragma mark - REFRESH/LOAD MORE
-(void) setTimer
{
    [myCollection reloadData];
    
    [COMMON removeProgressLoading];
    
}
//overwrite
- (void)insertRowAtTop {
    [self stopRefreshControl];
}

- (void)insertRowAtBottom {
    
    [myCollection reloadData];
    [self stopRefreshControl];
    
}

-(void)stopRefreshControl
{
    [myCollection.pullToRefreshView stopAnimating];
    [myCollection.infiniteScrollingView stopAnimating];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - action
#pragma callback
-(void)setCallback:(documentCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(documentCbk ) cb
{
    self.callback = cb;
    
}

-(IBAction)terminerAction:(id)sender
{

    NSMutableArray *arrReturn =  [NSMutableArray new];

    NSMutableArray *arrListChoose = [NSMutableArray new];
    for (NSDictionary *dic in self.documentURLs) {
        if ([dic[@"count"] intValue] > 0) {
            
            if ([dic[@"type"] intValue]== 2) {

                //add later
            }
            else
            {
                //image
                [arrListChoose addObject:dic];
            }
            
        }
    }
    
    //has selected image items
    if (arrListChoose.count > 0) {
        //to get selected data
        ALAssetsLibrary*library = [[ALAssetsLibrary alloc] init];
        
        iCountAssetSelected = 0;
        
        [COMMON addLoading:self];
        
        void (^assetEnumerator)( ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
            if(result != nil) {
                if([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]||
                   [[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]
                   ) {
                    
                    NSURL *url = (NSURL*) [[result defaultRepresentation]url];
                    
                    [library assetForURL:url resultBlock:^(ALAsset *asset) {
                        NSURL *url = (NSURL*) [[result defaultRepresentation]url];
                        
                        for (NSDictionary *dic in arrListChoose) {
                            
                            if ([dic[@"keycheck"] isEqualToString: [url absoluteString]  ]) {
                                
                                
                                //this selected file.
                                UIImage* returnThumbnail =  [UIImage imageWithCGImage: asset.thumbnail];
                                
                                NSURL *urlTmp = [[[asset defaultRepresentation] url] copy];
                                NSString* originalFileName = [[asset defaultRepresentation] filename];
                                
                                //write selected file to temp folder...only store file path
                                NSString *fileName = [NSString GetUUID];
                                
                                //TEMP Profile Image/video selected.
                                
                                NSString *strPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent: fileName ];
                                
                                if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                                    // asset is a video
                                    [arrReturn addObject:@{@"type":@3,@"thumbnail":returnThumbnail,@"filePath":urlTmp,@"fileName":originalFileName,@"keyPathFile":fileName }];
                                    //3 Video
                                }else{
                                    //0 photo
                                    [arrReturn addObject:@{@"type":@0,@"thumbnail":returnThumbnail,@"filePath":urlTmp,@"fileName":originalFileName,@"keyPathFile":fileName }];

                                }
                                
                                
                                //write to file
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                                    if ([[asset valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
                                        
                                        [self videoAssetURLToTempFile:urlTmp dest:strPath];
                                        ASLog(@"%@", strPath);
                                    }else{
                                        [asset exportDataToURL: [NSURL URLWithString:strPath] error:nil];
                                        
                                    }
                                    

                                    
                                    //END... BACK
                                    dispatch_async( dispatch_get_main_queue(), ^{
                                        iCountAssetSelected += 1;
                                        
                                        if (iCountAssetSelected == arrListChoose.count) {
                                            //finish

                                            [self checkAddPDF:arrReturn];
                                        }
                                    });
                                    
                                });
                                
                            }
                        }
                    }
                            failureBlock:^(NSError *error){ ASLog(@"operation was not successfull!"); } ];
                }
            }
        };
        
        NSMutableArray *assetGroups = [[NSMutableArray alloc] init];
        
        void (^ assetGroupEnumerator) ( ALAssetsGroup *, BOOL *)= ^(ALAssetsGroup *group, BOOL *stop) {
            if(group != nil) {
                [group enumerateAssetsUsingBlock:assetEnumerator];
                [assetGroups addObject:group];
            }
        };
        
        assetGroups = [[NSMutableArray alloc] init];
        
        [library enumerateGroupsWithTypes:ALAssetsGroupAll
                               usingBlock:assetGroupEnumerator
                             failureBlock:^(NSError *error) { ASLog(@"There is an error"); }];
        
    }else{
        //No select
        [self checkAddPDF:nil];

    }
    
}

-(void) checkAddPDF:(NSArray*)arrIn
{
    NSMutableArray*retunArr = [NSMutableArray new];
    
    if (arrIn) {
        [retunArr addObjectsFromArray:arrIn];
    }
    
    for (NSDictionary *dic in self.documentURLs) {
        if ([dic[@"count"] intValue] > 0) {
            
            //add pdf
            if ([dic[@"type"] intValue]== 2) {
                [retunArr addObject:dic];
            }
            
        }
    }

    if (_callback) {
        
        [COMMON removeProgressLoading];
        
        _callback(retunArr);
    }
    [self gotoback];
}

#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
    [self.documentURLs removeAllObjects];    // clear out the old docs and start over
    
    NSString *documentsDirectoryPath = [FileHelper PDFLocal];
    
    NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath
                                                                                              error:NULL];
    
    for (NSString* curFileName in [documentsDirectoryContents objectEnumerator])
    {
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
        BOOL isDirectory;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"]))
        {
            //2 pdf...only PDF...
            
            if ([[curFileName pathExtension] isEqualToString:@"pdf"]) {
                [self.documentURLs addObject:@{@"type":@2,@"filePath":filePath,@"fileName":curFileName}];
            }else{
                //3 photo...photo saved here?
            }
        }
    }
    
    [myCollection reloadData];
}
- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    //checks if docInteractionController has been initialized with the URL
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}
#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.documentURLs.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    BOOL isExit =NO;
    NSMutableArray *tmpArr = [NSMutableArray new];
    
    for (id indexSelect in self.arrSelected) {
        if ([indexSelect intValue]==(int)indexPath.row) {
            //remove
            [tmpArr addObject:indexSelect];
            NSMutableDictionary *dic = [self.documentURLs[[indexSelect intValue]] mutableCopy];
            [dic removeObjectsForKeys:@[@"count"]];
            [self.documentURLs replaceObjectAtIndex:[indexSelect intValue] withObject:dic];
            isExit = YES;
            break;
        }
    }
    
    [self.arrSelected removeObjectsInArray:tmpArr];
    
    
    if (isExit==NO) {
        
        [self.arrSelected addObject:@(indexPath.row)];
    }
    for (int i = 0; i< self.arrSelected.count; i++) {
        id indexSelect =self.arrSelected[i];
        NSMutableDictionary *dic = [self.documentURLs[[indexSelect intValue]] mutableCopy];
        [dic setValue:@(i+1) forKey:@"count"];
        [self.documentURLs replaceObjectAtIndex:[indexSelect intValue] withObject:dic];
        
    }
    [myCollection reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"PdfCollectionCellID";
    
    PdfCollectionCell *cell = (PdfCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.progressView removeFromSuperview];
    
    NSDictionary *dic =self.documentURLs[indexPath.row];
    if ([dic[@"count"] intValue]>0) {
        cell.vColor.layer.borderWidth = 2.0;
        cell.vColor.layer.borderColor=UIColorFromRGB(DOCUMENT_CELL_CHECK).CGColor;
        cell.lbCount.backgroundColor = UIColorFromRGB(DOCUMENT_CELL_CHECK);
        cell.lbCount.text =[NSString stringWithFormat:@"%@",dic[@"count"]];
        
    }else{
        cell.vColor.layer.borderWidth = 0.0;
        cell.vColor.layer.borderColor=[UIColor clearColor].CGColor;
        cell.lbCount.backgroundColor =[UIColor clearColor];
        cell.lbCount.text= @"";
    }
    
    if ([dic[@"type"] intValue]==0) {
        
        //        ALAsset *asset = dic[@"filePath"];
        //        [cell.imgThumb setImage:[UIImage imageWithCGImage:[asset thumbnail]]];
        
        [cell.imgThumb setImage:dic[@"thumbnail"]];
    } else if ([dic[@"type"] intValue] ==2 ) {
        NSString *fileURL = dic[@"filePath"];
        NSURL* pdfFileUrl = [NSURL fileURLWithPath:fileURL];
        CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
        
        cell.imgThumb.image =[[CommonHelper sharedInstance] buildThumbnailImage:pdf];
        
    } else if ([dic[@"type"] intValue] ==3 ) {
        NSString *fileURL = dic[@"filePath"];
//        cell.imgThumb.image = [UIImage imageWithContentsOfFile:fileURL];
  
                cell.imgThumb.image = [UIImage imageNamed:@"icon"];
    }
    return cell;
}

//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3.05 , collectionView.frame.size.width/3.05);
}

/*

Local File
documentPDFINBOX: PDF+ image to show: Private folder PDF
DID change: documentPDFINBOX

email...INBOX: copy to PDF_LOCAL
 
??? Downloaded file? PDF_DOWNLOAD.
 
(rename file in PDF_LOCAL)
 => check file view in Local first...if not exist -> download and look PDF download.

 
 
 Each item profile create => generate new File with name MD5...
 
 
 If delete item successful => Delete that local file.s
 
 If exported media not used (cancel create => Delete it).
 
 
 */


@end
