//
//  CellHomeLiveHunt.h
//  Naturapass
//
//  Created by GiangTT on 11/28/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface CellHomeLiveHunt : UITableViewCell
@property (weak, nonatomic) IBOutlet MarqueeLabel *lbName;
@property (weak, nonatomic) IBOutlet MarqueeLabel *lbDate;

@end
