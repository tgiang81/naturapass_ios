//
//  PhotoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CarteTypeContentVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "TypeCell8.h"
#import "TypeCell9.h"
#import "TreeViewNode.h"
#import "TheProjectCell.h"
#import "PublicationOBJ.h"

static NSString *cell8 =@"TypeCell8";
static NSString *cellID8 =@"cellID8";

static NSString *cell9 =@"TypeCell9";
static NSString *cellID9 =@"cellID9";
static NSString *CellIdentifier = @"treeNodeCell";

@interface CarteTypeContentVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSArray * arrData;
    NSUInteger indentation;
    NSMutableArray *nodes;
    NSMutableArray *arrID;
    __weak IBOutlet NSLayoutConstraint *constraintWidthContent;
    __weak IBOutlet UILabel *lblDesctiption;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIButton *btnValider;

}
@property (nonatomic,strong) IBOutlet UITableView *tableControl;
@property (nonatomic, retain) NSMutableArray *displayArray;
@property (nonatomic,strong) IBOutlet UIScrollView *svScollView;
@property (nonatomic,strong) IBOutlet UISwitch *swFilter;
@property (nonatomic, strong) TheProjectCell *prototypeCell;

- (void)expandCollapseNode:(NSNotification *)notification;

- (void)fillDisplayArray;
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray;

@end

@implementation CarteTypeContentVC
#pragma mark - init
-(id)initFromParent
{
    self = [super initWithNibName];
    if (self) {
        // Custom initialization
    }
    return self;

}
-(void) addSubContent:(NSString*) nameView
{
    NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:nameView owner:self options:nil];
    self.vSubContent = (UIView*)[nibArray objectAtIndex:0];
    [self.vContent addSubview:self.vSubContent];
    [self addContraintView:self.vSubContent];
    self.constraintHeight.constant =self.vSubContent.frame.size.height;

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [btnClose setTitle:str(strAnuler)forState:UIControlStateNormal];
    [btnValider setTitle:str(strValider) forState:UIControlStateNormal];
    lblDesctiption.text = str(strSelectionLeTypeContenu);
    [self addSubContent:@"CarteTypeContentVC"];
    [self.lbTitle setText:str(strFilterParTypeDeContenu)];
    
    UINib *nib = [UINib nibWithNibName:@"ProjectCell" bundle:nil];
    [self.tableControl registerNib:nib forCellReuseIdentifier:CellIdentifier];
    self.tableControl.estimatedRowHeight = 44;
    self.tableControl.rowHeight = UITableViewAutomaticDimension;

    //set default status
    
    [self.swFilter setOn:self.isCategoryON];

    if (self.isCategoryON) {
        self.tableControl.hidden = NO;
    }else{
        self.tableControl.hidden = YES;
    }
    

    //
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(expandCollapseNode:) name:@"ProjectTreeNodeButtonClicked" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(selectedNode:) name:@"ProjectTreeNodeSelected" object:nil];
    [self.svScollView setContentSize:CGSizeMake(700, self.svScollView.frame.size.height)];
//    constraintWidthContent.constant = 700;
    
    arrID = [NSMutableArray new];
    nodes =[NSMutableArray new];
    [self.swFilter setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
    self.swFilter.transform = CGAffineTransformMakeScale(0.75, 0.75);
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.arrNodes.count>0) {
        nodes = [NSMutableArray arrayWithArray:self.arrNodes];
    }
    else
    {
        NSArray *listTree;
        listTree =[[PublicationOBJ sharedInstance] getCache_Tree][@"default"];
        
        TreeViewNode *nodeData = [TreeViewNode new];
        nodeData.nodeChildren = [NSMutableArray new];
        [self fillData:listTree nodeLevel:0 nodeParent:nodeData];
        nodes = nodeData.nodeChildren;
    }
    [self fillDisplayArray];
    [self.tableControl reloadData];

}
- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fillData:(NSArray *)childrenArray nodeLevel:(NSUInteger)nodeLevel nodeParent:(TreeViewNode*)nodeParent
{
    for (NSDictionary *node in childrenArray) {
        
        TreeViewNode *treeViewNode = [[TreeViewNode alloc]init];
        treeViewNode.nodeLevel = nodeLevel;
        treeViewNode.nodeID = [node[@"id"] intValue];
        treeViewNode.nodeObject = node[@"name"];
        treeViewNode.isExpanded = NO;
        treeViewNode.nodeChildren =[NSMutableArray new];
        [nodeParent.nodeChildren addObject:treeViewNode];
        treeViewNode.parent = nodeParent;
        
        if (node[@"children"]) {
            [self fillData:node[@"children"] nodeLevel:treeViewNode.nodeLevel+1 nodeParent:treeViewNode];
        }

    }
}

#pragma mark - Messages to fill the tree nodes and the display array

//This function is used to expand and collapse the node as a response to the ProjectTreeNodeButtonClicked notification
- (void)expandCollapseNode:(NSNotification *)notification
{
    [self fillDisplayArray];
    [self.tableControl reloadData];
}
- (void)selectedNode:(NSNotification *)notification
{
    TreeViewNode *nodeSelected = (TreeViewNode *) [notification object];
    int statusNode = nodeSelected.statusNode;
    switch (nodeSelected.statusNode) {
        case NON_CHECK:
        {
            statusNode = IS_CHECK;
        }
            break;
        case UN_CHECK:
        {
            statusNode = IS_CHECK;

        }
            break;
        case IS_CHECK:
        {
            statusNode = NON_CHECK;

        }
            break;
        default:
            break;
    }
    [self checkFromParent:nodeSelected withStatusNode:statusNode];
    
    nodeSelected.statusNode = statusNode;
    [self checkFromChild:nodeSelected];
    
    [self.tableControl reloadData];
}
- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    parent.statusNode = statusNode;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        node.statusNode = statusNode;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        if (treenode.statusNode == IS_CHECK) {
            countCheck++;
        }
        if (treenode.statusNode == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        parent.statusNode = IS_CHECK;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        parent.statusNode = UN_CHECK;
    }
    else
    {
        parent.statusNode = NON_CHECK;

    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }

}
-(void)getFilterList:(NSArray*)arrNode
{
    for (TreeViewNode *node in arrNode) {
        if (node.statusNode == IS_CHECK) {
            [self addListIDWithNode:node];
//            continue;
        }
        if (node.nodeChildren) {
            [self getFilterList:node.nodeChildren];
        }
    }
}

-(void)addListIDWithNode:(TreeViewNode*)node
{
    //add list id
    NSString *strNodeID = [NSString stringWithFormat:@"%lu",(unsigned long)node.nodeID];
    if (node.statusNode == IS_CHECK) {
        if ([arrID indexOfObject:strNodeID]== NSNotFound) {
            [arrID addObject:strNodeID];
        }
    }
    else
    {
        if ([arrID indexOfObject:strNodeID]!= NSNotFound) {
            [arrID removeObject:strNodeID];
        }
    }
}
//This function is used to fill the array that is actually displayed on the table view
- (void)fillDisplayArray
{
    constraintWidthContent.constant =270;
    self.displayArray = [[NSMutableArray alloc]init];
    for (TreeViewNode *node in nodes) {
        [self.displayArray addObject:node];
//        [self caculatorWithContent:node];
        if (node.isExpanded) {
            node.lineBottom =NO;
            [self fillNodeWithChildrenArray:node.nodeChildren];
            
            int index  =self.displayArray.count;
            if(index>0)
            {
                TreeViewNode *nodeLeave = self.displayArray[index-1];
                nodeLeave.lineBottom =YES;
                [self.displayArray replaceObjectAtIndex:index-1 withObject:nodeLeave];
            }
        }
        else
        {
            node.lineBottom =YES;
        }
    }
}

//This function is used to add the children of the expanded node to the display array
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        node.lineBottom =NO;
        [self.displayArray addObject:node];
//        [self caculatorWithContent:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
        }
    }
}

-(void)caculatorWithContent:(TreeViewNode*)node
{
    CGRect r = [node.nodeObject boundingRectWithSize:CGSizeMake(2000, 20)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}
                                             context:nil];
    int with =(int)node.nodeLevel * 25+ r.size.width;
    if (with > constraintWidthContent.constant) {
        constraintWidthContent.constant= with;
    }
}
#pragma mark - Table view data source
- (TheProjectCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    return _prototypeCell;
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[TheProjectCell class]])
    {
        TheProjectCell *cell = (TheProjectCell *)cellTmp;
        TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
        cell.treeNode = node;
        
        cell.cellLabel.text = node.nodeObject;
        if (node.nodeChildren.count==0) {
            [cell setTheButtonBackgroundImage:nil];
            
        }
        else
        {
            if (node.isExpanded) {
                [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"arrow_down"]];
            }
            else {
                [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"arrow_up"]];
            }
        }
        switch (node.statusNode) {
            case NON_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                
            }
                break;
            case UN_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_inactive"] forState:UIControlStateNormal];
                
            }
                break;
            case IS_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"ic_check_active_pink"] forState:UIControlStateNormal];
                
            }
                break;
            default:
                break;
        }
        if(node.lineBottom)
        {
            cell.lineBottom.hidden = NO;
        }
        else
        {
            cell.lineBottom.hidden = YES;
        }
        //check_inactive // ic_check_active_pink// check
        [cell setNeedsDisplay];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    }
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
//    
//    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    return size.height+1;
//}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.displayArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //It's cruical here that this identifier is treeNodeCell and that the cell identifier in the story board is anything else but not treeNodeCell
    TheProjectCell *cell = (TheProjectCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

-(IBAction)fnFromUI:(UIButton*)sender
{
    switch (sender.tag) {
        case 10:
        {
            //Cancel
            [self closeAction:nil];
        }
            break;
        case 11:
        {
            [self getFilterList:nodes];
            NSDictionary *dic = nil;
            
            if (self.isCategoryON == NO) {
                dic = @{@"status":[NSNumber numberWithBool:NO]};
            }else{
                if (arrID.count) {
                    dic = @{@"listID":arrID ,@"status":[NSNumber numberWithBool:YES]};
                }else{
                    dic = @{@"status":[NSNumber numberWithBool:YES]};

                }
            }
            NSDictionary *dicMul =@{@"nodes":nodes,@"filter":dic};
            self.myCallback(dicMul);

            [self closeAction:nil];
        }
            break;

        default:
            break;
    }
}

- (IBAction)doSwitch:(id)sender {
    
    UISwitch *ui = (UISwitch*)sender;
    
    self.isCategoryON = ui.isOn;
    
    if (self.isCategoryON) {
        self.tableControl.hidden = NO;
    }else{
        self.tableControl.hidden = YES;
    }

}

@end
