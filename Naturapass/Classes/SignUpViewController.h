//
//  SignUpViewController.h
//  Naturapass
//
//  Created by ocsdev7 on 29/11/13.
//  Copyright (c) 2013 OCSMobi - 11. All rights reserved.
//

@protocol REGISTERDELEGATE;


#import <UIKit/UIKit.h>

@interface SignUpViewController : UIViewController
{
    //   toolBar
    
    UIToolbar                   *keyboardToolbar;
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
    UIBarButtonItem             *previousBarItem;
    UIBarButtonItem             *nextBarItem;
    
    //parsing
    NSURLConnection             *urlConnection;
    NSMutableData               *responseData;
    NSData                      *imageData;
    UIImage                     *userImage;
    NSMutableURLRequest         *request;
    NSMutableArray              *jsonList;
    
    //string
    NSNumber                    *pickerValue;
    NSString                    *strPassword;
    NSString                    *strUserImage;
    
    IBOutlet UIView             *civiliteView;
    CGFloat                     statusBarHeight;

    NSString                    *civiliteString;
    
    NSString                    *strAvatarUserImage;
    NSString                    *strAvatarCount;
    //picker
    UIPickerView                *formPicker;
    UIActionSheet               *formActionSheet;
    IBOutlet UIButton           *dismissButton;

    IBOutlet UISwitch *switchAcceptCGU;
}
@property (nonatomic,retain)id<REGISTERDELEGATE>registerDelegate;

@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *viewPickerBackground;

@end

@protocol REGISTERDELEGATE <NSObject>

-(void)loadUserValue:(NSString *)userName pass:(NSString *)passwords;

@end
