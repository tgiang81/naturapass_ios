//
//  NetworkCheckSignal.m
//  Naturapass
//
//  Created by Giang on 10/20/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "NetworkCheckSignal.h"

#import "Define.h"
#import "Config.h"

#import "CommonHelper.h"
#import "FileHelper.h"
#import "AppCommon.h"
#import "AFNetworking.h"

static NetworkCheckSignal *sharedInstance = nil;

@implementation NetworkCheckSignal

+ (NetworkCheckSignal *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) getAddressFromCoordinate :(callBackSignal) cb withData:(NSDictionary*) data
{
    self.myCallBack = cb;
    if(!ceo)
    {
      ceo = [[CLGeocoder alloc]init];
    }
    [ceo cancelGeocode];
    loc = [[CLLocation alloc]initWithLatitude: [data[@"latitude"] floatValue] longitude: [data[@"longitude"] floatValue]];
    [ceo reverseGeocodeLocation: loc completionHandler:
     ^(NSArray *placemarks, NSError *error) {
         
         
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         NSLog(@"placemark %@",placemark);
         //String to hold address
         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
         if (self.myCallBack) {
             self.myCallBack(locatedAt);
         }
     }];
}

@end
