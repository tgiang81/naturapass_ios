//
//  PinView.h
//  Naturapass
//
//  Created by GS-Soft on 6/9/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PinView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *imgV;

@end
