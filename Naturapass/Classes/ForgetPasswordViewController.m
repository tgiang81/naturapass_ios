//
//  ForgetPasswordViewController.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 5/28/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "PasswordAcceptViewController.h"
#import "KeychainItemWrapper.h"
#import "AppDelegate.h"
#import "WebServiceAPI.h"
#import "AppCommon.h"
#import "Define.h"
#import "SignUpNavigation.h"
#import "KSToastView.h"
#define  FIELDS_COUNT   1

@interface ForgetPasswordViewController ()<WebServiceAPIDelegate, MPGTextFieldDelegate, UITextFieldDelegate> {
    NSMutableArray *suggestList;
    SignUpNavigation *navigation;

    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;

}

@property (nonatomic, retain) NSMutableArray *suggestList;

@end

@implementation ForgetPasswordViewController
@synthesize suggestList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lbDescription1.text = str(strRecuperationMotDePasse);
    lbDescription2.text = str(strRenseignerVotreAdresseEmail);
    forgetEmailTextField.placeholder = str(strVotreAdresseEmail);
    self.navigationItem.hidesBackButton=YES;
    [self loadNavigation];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];

    [self InitializeKeyboardToolBar];
    [self generateData];
    [forgetEmailTextField setDelegate:self];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}
#pragma mark - Custom Navigation
- (void)loadNavigation{
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    navigation =[[SignUpNavigation alloc] initWithNibName:@"SignUpNavigation" bundle:nil];
    [self.navigationController.navigationBar addSubview:navigation.view];
    
    //update frame
    CGRect recTmp =navigation.view.frame;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    recTmp.size.width = screenWidth;
    navigation.view.frame = recTmp;
    [navigation.btnBack addTarget:self action:@selector(gotoBack) forControlEvents:UIControlEventTouchUpInside];
}
- (void)gotoBack{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
-(void)putUserResetPasswordAction{
    NSDictionary * postDict = [NSDictionary dictionaryWithObjectsAndKeys:forgetEmailTextField.text,@"email", nil];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putUserResetPasswordAction:postDict];
    serviceAPI.onComplete =^(id response,int errCode)
    {
        [COMMON removeProgressLoading];
        
        if ([response isKindOfClass:[NSDictionary class]]) {
            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                [KSToastView ks_showToast: response[@"message"] duration:2.0f completion: ^{
                }];
                return;
            }
        }
        
        if(response[@"success"])
        {
            PasswordAcceptViewController *passwordAcceptVC =[[PasswordAcceptViewController alloc]initWithNibName:@"PasswordAcceptViewController" bundle:nil];
            [self.navigationController pushViewController:passwordAcceptVC animated:YES];
        }
    };
}

-(void)goback {
//    [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    
}

-(IBAction)validerButtonAction:(id)sender{
    if([forgetEmailTextField.text length]){
    [self putUserResetPasswordAction];
    }
    [forgetEmailTextField resignFirstResponder];

}

- (void)generateData
{
    forgetEmailTextField.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.8];
    forgetEmailTextField.seperatorColor = [UIColor clearColor];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        NSArray *contents = [[NSUserDefaults standardUserDefaults] objectForKey:SUGGEST_LIST];

        if (suggestList == nil) {
            suggestList = [[NSMutableArray alloc] init];
        } else {
            [suggestList removeAllObjects];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
            [suggestList addObjectsFromArray:contents];
        });
    });
}

#pragma mark - KeyBoard

-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (keyboardToolbar == nil)
    {
        keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:NSLocalizedString(@"OK", @"")
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard:)];
        
        [keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                    spaceBarItem,
                                   doneBarItem, nil]];
        
        
    }
     [forgetEmailTextField setInputAccessoryView:keyboardToolbar];
    
}

- (void)animateView:(NSUInteger)tag
{
    CGRect rect = self.view.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    if (tag == 1)
        rect.origin.y = -20.0f ;
    [UIView commitAnimations];
}
- (void)resignKeyboard:(id)sender
{
     [forgetEmailTextField resignFirstResponder];
}
 - (void)checkBarButton:(NSUInteger)tag
{
    UIBarButtonItem *previousBarItem1 = (UIBarButtonItem *)[[keyboardToolbar items] objectAtIndex:0];
    UIBarButtonItem *nextBarItem1     = (UIBarButtonItem *)[[keyboardToolbar items] objectAtIndex:1];
    
    [previousBarItem1 setEnabled:tag == 1 ? NO : YES];
    [nextBarItem1     setEnabled:tag == FIELDS_COUNT ? NO : YES];
}

- (id)getFirstResponder
{
    NSUInteger index = 0;
    while (index <= FIELDS_COUNT) {
        UITextField *textField = (UITextField *)[self.view viewWithTag:index];
        if ([textField isFirstResponder]) {
            return textField;
        }
        index++;
    }
    return 0;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [forgetEmailTextField resignFirstResponder];
    return YES;
}

#pragma mark - MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    if ([textField isEqual:forgetEmailTextField]) {
        return suggestList;
    } else {
        return nil;
    }
}

- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}

#pragma mark -

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
