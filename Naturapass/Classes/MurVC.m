//
//  MurVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurVC.h"
#import "MediaCell.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "PublicationVC.h"
#import "GroupMesVC.h"
#import "CommentDetailVC.h"
#import "FriendInfoVC.h"
#import "MapLocationVC.h"
#import "MurEditPublicationVC.h"
#import "ImageViewAction.h"
#import "AlertViewMembersLike.h"
#import "MurPeopleLikes.h"
#import "AZNotification.h"
#import "MurView.h"
#import "AmisAddScreen1.h"
#import "GroupCreateOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateV2.h"
#import "Publication_FavoriteAddress.h"
#import "ChatListe.h"
#import "Publication_Carte.h"
#import "NSString+HTML.h"

#import "SignalerTerminez.h"

@interface MurVC ()
{
    
    NSMutableArray                  *publicationArray;
    BOOL                            isFilterOff;
//    NSOperationQueue                *operationQueue;
    MurView                         *murView;
    BOOL bFilterON;
    BOOL isHaveChooseQUI;
}

@property (nonatomic,strong)   IBOutlet UILabel     *lbMessage;
@property (nonatomic, strong) NSMutableDictionary   *heightCache;
@property (nonatomic,strong)   IBOutlet UIView      *viewContent;

@end

@implementation MurVC

static NSString *MediaCellIDMur = @"MediaCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.vContainer.backgroundColor = UIColorFromRGB(NEW_MUR_BACKGROUND_COLOR);

    [self.vViewFilter.layer setMasksToBounds:YES];
    self.vViewFilter.layer.cornerRadius= 4.0;
    self.vViewFilter.layer.borderWidth =0.5;
    self.vViewFilter.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    // drop shadow
    [self.vViewFilter.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.vViewFilter.layer setShadowOpacity:0.8];
    [self.vViewFilter.layer setShadowRadius:3.0];
    [self.vViewFilter.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    jtSwitch.transform = CGAffineTransformMakeScale(0.65, 0.65);

    [self addSubViewFilter];
    [self addShortCut];
    //refresh
    __weak typeof(self) wself = self;
    self.isGoin = YES;
    self.warning.text = @"";
    self.warning.hidden = TRUE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRefresh) name: NOTIFY_REFRESH_MES object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPublicationNew:) name: NOTIFY_REFRESH_MES_NEW object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPublicationEdit:) name: NOTIFY_REFRESH_MES_EDIT object:nil];

//    operationQueue = [NSOperationQueue new];
    _lbMessage.text=EMPTY_MUR;
    publicationArray =[NSMutableArray new];

    //add view mur
    /*
     [murView fnSetDataPublication:publicGroupArray];
     [murView stopRefreshControl];
     [murView startRefreshControl];
     [murView fnSetDataPublication:publicGroupArray];
     
     */
    murView = [[MurView alloc] initWithEVC:self expectTarget:self.expectTarget];
    [murView setCallback:^(VIEW_ACTION_TYPE type, NSArray *arrData)
     {
         switch (type) {
             case VIEW_ACTION_REFRESH_TOP:
             {
                 [wself insertRowAtTop];
                 
             }
                 break;
             case VIEW_ACTION_REFRESH_BOTTOM:
             {
                 [wself insertRowAtBottom];
             }
                 break;
             case VIEW_ACTION_UPDATE_DATA:
             {
                 [wself updateDataPublication:arrData];
             }
                 break;
             default:
                 break;
         }
     }];
    [murView addContraintSupview:self.viewContent];
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    if ([filterDic[@"bFilterON"] boolValue] ) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }

    [self fnSetSwitchFilter];

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self fnSetSwitchFilter];
    if (self.isGoin) {
        self.isGoin = NO;
        [self loadCache];
        [self doRefresh];
    }
    else
    {
        //
        [murView fnSetDataPublication:publicationArray];
    }
    [self getFilter];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fnSetSwitchFilter
{
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    NSString *strFilter2 =@"";
    
    if (bFilterON) {
        int count = 0;
        if ([filterDic[@"filterQUI"] boolValue]) {
            count +=1;
            strFilter2 = @" QUI ?";
        }
        if ([filterDic[@"filterQUOI"] boolValue]) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" QUOI ?"];
        }
        if ([filterDic[@"filterQUAND"] boolValue]) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" QUAND ?"];
        }
        if (self.myC_Search_Text.length > 0) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" MOTS-CLES"];
        }
        if (count > 1) {
            lbFilter1.text = @"Filtres actifs :";
        }
        else if (count == 1) {
            lbFilter1.text = @"Filtre actif :";
        }
        else
        {
            lbFilter1.text = @"Filtre actif";
            
        }
    }
    else
    {
        lbFilter1.text = @"Filtre Désactivé";
        
    }
    
    lbFilter2.text = strFilter2;
    jtSwitch.on = bFilterON;
    jtSwitch.transform = CGAffineTransformMakeScale(0.65, 0.65);
    if (self.expectTarget == ISMUR) {
        [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH)];
    }
    else
    {
        [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
    }
    
    [jtSwitch setBackgroundColor:UIColorFromRGB(OFF_SWITCH)];
    jtSwitch.layer.cornerRadius = 16;
    
    UIImageView *imgFilter = [self.subview viewWithTag:92];
    if (bFilterON) {
        imgFilter.image = [[UIImage imageNamed:@"ic_mur_filter"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imgFilter.tintColor = UIColorFromRGB(ON_SWITCH);

    }
    else
    {
        imgFilter.image = [UIImage imageNamed:@"ic_mur_filter"];

    }
}
-(IBAction)filterSwitchAction:(id)sender
{
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    NSMutableDictionary *dic = [NSMutableDictionary new];
    [dic addEntriesFromDictionary:filterDic];
    UISwitch *sv = (UISwitch*)sender;
    [dic setObject:[NSNumber numberWithBool:sv.on] forKey:@"bFilterON"];
    
    [[NSUserDefaults standardUserDefaults]setObject:dic forKey:
     [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
    [[NSUserDefaults standardUserDefaults]synchronize];

    bFilterON = sv.on;
    [self doRefreshFilter];
    [self fnSetSwitchFilter];

}
#pragma mark - GET DATA

-(void) getFilter
{
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    self.strFilterMoi = [filterDic[@"iSharingMoi"] stringValue];
    self.strFilterAmis = [filterDic[@"iSharingAmis"] stringValue];
    self.myC_Search_Text = filterDic[@"c_search"];
    self.arrCategoriesFilter = filterDic[@"listID"];

    if (self.strFilterMoi.boolValue && self.strFilterAmis.boolValue) {
        self.strFilterType = [NSString stringWithFormat:@"%d",F_Moi_Amis];
    }
    else if (self.strFilterMoi.boolValue && !self.strFilterAmis.boolValue) {
        self.strFilterType = [NSString stringWithFormat:@"%d",F_Moi];

    }
    else if (!self.strFilterMoi.boolValue && self.strFilterAmis.boolValue) {
        self.strFilterType = [NSString stringWithFormat:@"%d",F_Amis];

    }
    else
    {
        self.strFilterType = nil;
    }
    self.strFilterDebut = filterDic[@"debut"];
    self.strFilterFin = filterDic[@"fin"];
    
    if ([filterDic[@"bFilterON"] boolValue] ) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }
    if( filterDic !=nil)
    {
        if (bFilterON)
        {
            //Personne
            NSString *strPath_Personne = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_PERSONNE_SAVE)  ];
            NSArray *arrTmp_Personne = [NSArray arrayWithContentsOfFile:strPath_Personne];
            
            NSMutableArray *childPerson = [NSMutableArray new];
            for (NSDictionary *dicPersonne in arrTmp_Personne) {
                if ([dicPersonne[@"status"] boolValue]) {
                    [childPerson addObject:dicPersonne[@"id"]];
                }
            }
            self.strFilterPersonne = [childPerson componentsJoinedByString:@","];

            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_MES_GROUP_SAVE)   ];
            NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
            
            NSMutableArray *arrMut = [NSMutableArray new];
            
            if (arrTmp.count > 0) {
                
                for (NSDictionary*dic in arrTmp) {
                    if ([dic[@"status"] boolValue] == YES) {
                        [arrMut addObject:dic[@"id"]];
                        
                    }
                }
            }
            
            self.strMesGroupFilter = [arrMut componentsJoinedByString:@","];
            
            //HUNT
            
            strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_MES_HUNT_SAVE)  ];
            arrTmp = [NSArray arrayWithContentsOfFile:strPath];
            arrMut = [NSMutableArray new];
            if (arrTmp.count > 0) {
                
                for (NSDictionary*dic in arrTmp) {
                    if ([dic[@"status"] boolValue] == YES) {
                        [arrMut addObject:dic[@"id"]];
                    }
                }
            }
            
            self.strMesHuntFilter = [arrMut componentsJoinedByString:@","];
            isFilterOff = NO;
            
            //ALARM
            NSString *strFilter = [self fnStringFilter];
            if ((strFilter == nil || [strFilter isEqualToString:@""]) && self.strFilterType == nil) {
                self.warning.hidden = NO;
                self.warning.text = str(strMessage22);
            }else{
                self.warning.hidden = TRUE;
                self.warning.text = @"";

            }
            
        }else{
            //API for get all publications: /api/v2/publication/filteroff?limit=200&offset=0
            self.warning.hidden = TRUE;
            self.warning.text = @"";
            
            isFilterOff = YES;
            
        }
        
    }else{
        isFilterOff = YES;
    }
    
}
-(BOOL)checkAddpublication:(NSDictionary*)publicationNew
{
    BOOL isAdd = NO;
    
    NSDictionary*dic = publicationNew;
    if (isFilterOff) {
        isAdd = YES;
    }
    else
    {
        if (self.strFilterMoi.boolValue) {
            isAdd = YES;
            return isAdd;
        }
        if (self.strMesGroupFilter.length > 0 ) {
            NSArray *arr = [self.strMesGroupFilter componentsSeparatedByString:@","];
            NSArray *arrTmp = dic[@"groups"];
            for (NSDictionary *dic in arrTmp) {
                for (NSString *strID in arr) {
                    if ([dic[@"id"] intValue] == [strID intValue]) {
                        isAdd = YES;
                        return isAdd;
                    }
                }
            }
        }
        if (self.strMesHuntFilter.length > 0 ) {
            NSArray *arr = [self.strMesHuntFilter componentsSeparatedByString:@","];
            NSArray *arrTmp = dic[@"hunts"];
            for (NSDictionary *dic in arrTmp) {
                for (NSString *strID in arr) {
                    if ([dic[@"id"] intValue] == [strID intValue]) {
                        isAdd = YES;
                        return isAdd;
                    }
                }
            }
        }
    }
    return isAdd;
}
-(void)refreshPublicationNew:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    if (dic && [self checkAddpublication:dic]) {

        if (publicationArray.count>0) {
            [publicationArray insertObject:dic atIndex:0];
        }
        else
        {
            [publicationArray addObject:dic];
        }
        [murView fnPublicationNew:publicationArray];
    }
}
-(void)refreshPublicationEdit:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    if (dic[@"publication"]) {
        for (int i = 0; i < publicationArray.count; i++) {
            NSDictionary *dicItem = publicationArray[i];
            if ([dicItem[@"id"] intValue] == [dic[@"publication"][@"id"] intValue]) {
                [publicationArray replaceObjectAtIndex:i withObject:dic[@"publication"]];
                break;
            }
        }
        [murView fnSetDataPublication:publicationArray];
    }
}
-(void) doRefresh
{
    [self getFilter];
    if ([COMMON isReachable]) {
        [murView startRefreshControl];
    }
}
-(void) doRefreshFilter
{
    [publicationArray removeAllObjects];
    [murView fnSetDataPublication:publicationArray];
    [self getFilter];
    if ([COMMON isReachable]) {
        [murView startRefreshControl];
    }
}
-(void)loadCache
{
    NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        [publicationArray removeAllObjects];
        [publicationArray  addObjectsFromArray:arrTmp];
        [murView fnSetDataPublication:publicationArray];
    }
    if (![COMMON isReachable]) {
        [self checkEmpty:publicationArray];
    }
}
-(void)checkEmpty:(NSMutableArray*)arr
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
    }
    else
    {
        self.lbMessage.hidden=NO;
    }
}
//MARK: - String Filter
-(NSString*)fnStringFilter
{
    NSMutableString *strGroup = [NSMutableString new];

    //Special case... if ONLY filter time...is selected => must using api filter off to get data.
    isHaveChooseQUI = NO;
    
    if (self.strMesGroupFilter.length>0) {
        NSArray *arr = [self.strMesGroupFilter componentsSeparatedByString:@","];
        
        for (NSString *gr in arr) {
            [strGroup appendString: [ NSString stringWithFormat:@"&groups[]=%@", gr]];
        }
        isHaveChooseQUI = YES;
    }
    
    if (self.strMesHuntFilter.length>0) {
        NSArray *arr = [self.strMesHuntFilter componentsSeparatedByString:@","];

        for (NSString *gr in arr) {
            [strGroup appendString: [ NSString stringWithFormat:@"&hunts[]=%@", gr]];
        }
        isHaveChooseQUI = YES;
    }
    
    if (self.strFilterPersonne.length>0) {
        NSArray *arr = [self.strFilterPersonne componentsSeparatedByString:@","];

        for (NSString *gr in arr) {
            [strGroup appendString: [ NSString stringWithFormat:@"&users[]=%@", gr]];
        }
        isHaveChooseQUI = YES;
    }
    if (self.arrCategoriesFilter.count>0) {

        for (NSString *gr in self.arrCategoriesFilter) {
            [strGroup appendString: [ NSString stringWithFormat:@"&categories[]=%@", gr]];
        }
    }
    if (self.strFilterDebut && self.strFilterFin) {
        [strGroup appendString: [ NSString stringWithFormat:@"&startTime=%@&endTime=%@", self.strFilterDebut,self.strFilterFin]];
    }
    //c_search
    if (self.myC_Search_Text && ![self.myC_Search_Text isEqualToString:@""]) {
        [strGroup appendString: [ NSString stringWithFormat:@"&filter=%@", [self.myC_Search_Text urlencode]]];
        
    }
    return strGroup;
}
#pragma mark - REFRESH/LOAD MORE

-(void) fnOperationDoTop
{
    NSString *strFilter = [self fnStringFilter];
    if (self.strFilterType.length > 0) {
        strFilter =[NSString stringWithFormat:@"%@&sharing=%@",strFilter,self.strFilterType];
    }
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if (self.isMoiMur) {
//        - (void) getPublicationsPrivite:(NSString *)strLimit withOffset:(NSString *)strOffset;
//        - (void) getPublicationsPublic:(NSString *)strLimit withOffset:(NSString *)strOffset withFilter:(NSString*) strFilter;
    [serviceObj getPublicationsPrivite:mur_limit withOffset:@"0" withFilter:strFilter];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [murView stopRefreshControl];
        
        [COMMON removeProgressLoading];
        if ([response isKindOfClass:[NSDictionary class]]) {
            
            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                if (errCode == -1001) {
                    //Bad  network
                    [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                }
                else
                {
                    if([wself fnCheckResponse:response]) return;
                    
                }
                [self checkEmpty:publicationArray];
                return ;
            }
        }
        for (int i =0; i<publicationArray.count; i++) {
            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:publicationArray[i][@"kFileData"]];
            [FileHelper removeFileAtPath:photoFile];
            
        }
        
        if (response[@"publications"]) {
            NSArray*arrGroups = nil;
            if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
                arrGroups = response[@"publications"];
            }
            if ((int)arrGroups.count >= 0) {
                [publicationArray removeAllObjects];
                [publicationArray addObjectsFromArray:arrGroups];
                [murView fnSetDataPublication:publicationArray];
                
                // Path to save array data
                NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                //                NSLog(@">>> %@", strPath);
                // Write array
                [publicationArray writeToFile:strPath atomically:YES];
            }
        }
        [self checkEmpty:publicationArray];
    };
        return;
    }
    /*
    if (isFilterOff)
    {
        [serviceObj getPublicationFilterOffWithLimit:mur_limit offset:@"0" withGroups:@""];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [murView stopRefreshControl];
                
                [COMMON removeProgressLoading];

                if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
                    NSArray*arrGroups = response[@"publications"];
                    if ((int)arrGroups.count >= 0) {
                        [publicationArray removeAllObjects];
                        [publicationArray addObjectsFromArray:response[@"publications"] ];
                        [murView fnSetDataPublication:publicationArray];
                        
                        // Path to save array data
                        NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                        //                    NSLog(@">>> %@", strPath);
                        // Write array
                        [publicationArray writeToFile:strPath atomically:YES];
                        
                    }
                }
                else
                {

                    if (errCode == -1001) {
                        [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                        //                        [self loadCache];
                    }
                    else
                    {
                        if([wself fnCheckResponse:response]) return;
                    }
                }
                [self checkEmpty:publicationArray];
            });
            
        };
        
        return;
    }
    
    //ON filter
    NSString *strFilter = [self fnStringFilter];
    //
    if ((strFilter == nil ||[strFilter isEqualToString:@""]) && self.strFilterType == nil) {
        [murView stopRefreshControl];
        [publicationArray removeAllObjects];
        [murView fnSetDataPublication:publicationArray];
         return;
    }
    if (!(isHaveChooseQUI || self.strFilterType)) {
        [serviceObj getPublicationFilterOffWithLimit:mur_limit offset:@"0" withGroups:strFilter];
    }
    else
    {
        [serviceObj getPublicationsAction:mur_limit forSharing:self.strFilterType withOffset:@"0" withGroups:strFilter];
    }
    */

    [serviceObj getPublicationsPublic:mur_limit withOffset:@"0" withFilter:strFilter];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [murView stopRefreshControl];
        
        [COMMON removeProgressLoading];
        if ([response isKindOfClass:[NSDictionary class]]) {
            
            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                if (errCode == -1001) {
                    //Bad  network
                    [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                }
                else
                {
                    if([wself fnCheckResponse:response]) return;
                    
                }
                [self checkEmpty:publicationArray];
                return ;
            }
        }
        for (int i =0; i<publicationArray.count; i++) {
            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:publicationArray[i][@"kFileData"]];
            [FileHelper removeFileAtPath:photoFile];
            
        }
        
        if (response[@"publications"]) {
            NSArray*arrGroups = nil;
            if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
                arrGroups = response[@"publications"];
            }
            if ((int)arrGroups.count >= 0) {
                [publicationArray removeAllObjects];
                [publicationArray addObjectsFromArray:arrGroups];
                [murView fnSetDataPublication:publicationArray];
                
                // Path to save array data
                NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                
                //                NSLog(@">>> %@", strPath);
                // Write array
                [publicationArray writeToFile:strPath atomically:YES];
            }
        }
        [self checkEmpty:publicationArray];
    };
}

//overwrite
- (void)insertRowAtTop {
    [self fnOperationDoTop];
}
-(void) fnOperationDoBottom
{
    NSString *strFilter = [self fnStringFilter];
    if (self.strFilterType.length > 0) {
        strFilter =[NSString stringWithFormat:@"%@&sharing=%@",strFilter,self.strFilterType];
    }
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    if (self.isMoiMur) {
    [serviceObj getPublicationsPrivite:mur_limit withOffset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count ] withFilter:strFilter];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        [murView stopRefreshControl];
        if ([response isKindOfClass:[NSDictionary class]]) {
            
            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                if (errCode == -1001) {
                    //Bad  network
                    [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                }
                else
                {
                    if([wself fnCheckResponse:response]) return;
                    
                }
                [self checkEmpty:publicationArray];
                return ;
            }
        }
        if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
            NSArray*arrGroups = response[@"publications"];
            
            if (arrGroups.count > 0)
            {
                [publicationArray addObjectsFromArray:response[@"publications"] ];
                [murView fnAddMorePublication:arrGroups];

                NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                //                NSLog(@">>> %@", strPath);
                // Write array
                [publicationArray writeToFile:strPath atomically:YES];
            }
            
        }
        
    };
        return;
    }
    /*
    if (isFilterOff) {
        [serviceObj getPublicationFilterOffWithLimit:mur_limit offset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count ] withGroups:@""];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            
            [murView stopRefreshControl];
            if ([response isKindOfClass:[NSDictionary class]]) {
                if ([response[@"message"] isKindOfClass:[NSString class]]) {
                    if (errCode == -1001) {
                        //Bad  network
                        [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                    }
                    else
                    {
                        if([wself fnCheckResponse:response]) return;
                        
                    }
                    [self checkEmpty:publicationArray];
                    return ;
                }
            }
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
                NSArray*arrGroups = response[@"publications"];
                
                if (arrGroups.count > 0)
                {
                    [publicationArray addObjectsFromArray:response[@"publications"] ];
                    [murView fnSetDataPublication:publicationArray];
                    
                NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                    //                    NSLog(@">>> %@", strPath);
                    // Write array
                    [publicationArray writeToFile:strPath atomically:YES];
                    
                }
                
            }
            
        };
        
        return;
    }
    
    //FILTER ON
    //ON filter
    NSString *strFilter = [self fnStringFilter];
    //
    if ((strFilter == nil ||[strFilter isEqualToString:@""]) && self.strFilterType == nil) {
        [murView stopRefreshControl];
        return;
    }
    if (!(isHaveChooseQUI || self.strFilterType)) {
        [serviceObj getPublicationFilterOffWithLimit:mur_limit offset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count ] withGroups:strFilter];
    }
    else
    {
        [serviceObj getPublicationsAction:mur_limit forSharing:self.strFilterType withOffset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count ] withGroups:strFilter];
    }
    */
    [serviceObj getPublicationsPublic:mur_limit withOffset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count ] withFilter:strFilter];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        [murView stopRefreshControl];
        if ([response isKindOfClass:[NSDictionary class]]) {
            
            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                if (errCode == -1001) {
                    //Bad  network
                    [AZNotification showNotificationWithTitle:@"La demande a expiré." controller:self notificationType:AZNotificationTypeError];
                }
                else
                {
                    if([wself fnCheckResponse:response]) return;
                    
                }
                [self checkEmpty:publicationArray];
                return ;
            }
        }
        if ([response[@"publications"] isKindOfClass: [NSArray class] ]) {
            NSArray*arrGroups = response[@"publications"];
            
            if (arrGroups.count > 0)
            {
                [publicationArray addObjectsFromArray:response[@"publications"] ];
                [murView fnAddMorePublication:arrGroups];

                NSString *strPath = [FileHelper pathForApplicationDataFile:[NSString stringWithFormat:@"%@_%@",self.isMoiMur?@"Moi":@"All",concatstring([COMMON getUserId],FILE_MUR_SAVE)]];
                //                NSLog(@">>> %@", strPath);
                // Write array
                [publicationArray writeToFile:strPath atomically:YES];
            }
            
        }
        
    };

}
- (void)insertRowAtBottom {
    [self fnOperationDoBottom];
}
-(void)updateDataPublication:(NSArray*)arrData
{
    publicationArray = [arrData mutableCopy];
    [self checkEmpty:publicationArray];

}

- (IBAction)fnAddPublication:(id)sender {
        SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
    [[PublicationOBJ sharedInstance]  resetParams];
}

//MARK: view filter
-(void) addSubViewFilter
{
    __weak typeof(self) wself = self;
    
    self.viewFilter = [[CarterFilterView alloc] initWithEVC:self expectTarget:ISMUR isLiveHunt:self.isLiveHunt];
    [self.viewFilter addContraintSupview:self.view];
    [self.viewFilter setCallback:^(NSString *strSearch)
     {
         [wself doRefreshFilter];
         [wself fnSetSwitchFilter];
         
     }];
    [self.viewFilter setup];
}
//MARK: short cut
-(void) addShortCut
{
    __weak typeof(self) wself = self;
    self.btnShortCut.hidden = NO;
    self.viewShortCut = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:ISMUR];
    [self.viewShortCut setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         wself.btnShortCut.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_ADD_FIREND:
             {
                 AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_AGENDA:
             {
                 [[ChassesCreateOBJ sharedInstance] resetParams];
                 ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_GROUP:
             {
                 [[GroupCreateOBJ sharedInstance] resetParams];
                 GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:YES];

             }
                 break;
             case SHORTCUT_ACTION_DISCUSTION:
             {
                 ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISDISCUSS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
                 viewController1.isFromSetting = YES;
                 [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_PUBLICATION:
             {
                 [wself fnAddPublication: nil];
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCut.constraintBottomCloseButton.constant = 30;
    self.viewShortCut.constraintTraillingCloseButton.constant = 17;
    [self.viewShortCut addContraintSupview:self.view];
    self.viewShortCut.hidden = YES;
    [self.viewShortCut fnAllowAdd:YES];
}
-(IBAction)fnShortCut:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.btnShortCut.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        self.btnShortCut.hidden = YES;
        [self.viewShortCut hide:NO];
        [self returnRotation];
    }];
}

-(void) returnRotation{
    [UIView animateWithDuration:0.1 animations:^{
        self.btnShortCut.transform = CGAffineTransformIdentity;
    }];
}

-(IBAction) fnShowFilterView
{
    [self.viewFilter hide:NO];
    [self.viewFilter fnGetDataWithStringSearch:_myC_Search_Text];
}

@end
