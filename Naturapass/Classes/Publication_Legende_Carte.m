//
//  Publication_Legende_Carte.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Legende_Carte.h"
#import "Publication_Specification.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "Publication_Color.h"

@interface Publication_Legende_Carte ()
{
    IBOutlet UITextField *legendTxt;
    __weak IBOutlet TPKeyboardAvoidingScrollView *scrollKeyboard;
    __weak IBOutlet UILabel *numberRemain;
    int maxNumChar;
    IBOutlet UIButton*  btnSuivatnt;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UIImageView *imgLogoScreen;

}
@property (strong, nonatomic) IBOutlet UIView *viewBottom;

@end

@implementation Publication_Legende_Carte

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isFromNewSignal)
    {
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myDesc setText:@"Utiliser un favori..."];
        lbDescription1.text = str(strEntrez_une_legende_qui_apparaitra_sur_la_carte);
        
        lbDescription2.text = str(strFacultatif);
        
        lbDescription1.textColor = [UIColor whiteColor];
        lbDescription2.textColor = [UIColor whiteColor];
        numberRemain.textColor = [UIColor whiteColor];
        
        [btnSuivatnt setTitle:str(strSUIVANT) forState:UIControlStateNormal ];
        self.viewBottom.backgroundColor = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);
        [btnSuivatnt.layer setMasksToBounds:YES];
        btnSuivatnt.layer.cornerRadius= 25.0;
        
        legendTxt.placeholder = str(strLegende_carte);
        imgLogoScreen.image = [[UIImage imageNamed:@"ic_map_message"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        imgLogoScreen.tintColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
    }else{
        
        lbDescription1.text = str(strEntrez_une_legende_qui_apparaitra_sur_la_carte);
        
        lbDescription2.text = str(strFacultatif);
        [btnSuivatnt setBackgroundColor:UIColorFromRGB(MUR_TINY_BAR_COLOR)];

        [btnSuivatnt setTitle:str(strSUIVANT) forState:UIControlStateNormal ];
        legendTxt.placeholder = str(strLegende_carte);
    }
    
    [PublicationOBJ sharedInstance].isEditFavo= NO;
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(textFieldDidChange:)
                                                  name:UITextFieldTextDidChangeNotification
                                                object:legendTxt];
    maxNumChar = 13;
    if ([PublicationOBJ sharedInstance].isEditer) {
            legendTxt.text = [PublicationOBJ sharedInstance].legend;
            [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    //reset
    if (!self.isEditFavo) {
        [PublicationOBJ sharedInstance].dicFavoris =nil;
    }
    else
    {
        NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
        legendTxt.text = dicFavo[@"legend"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 nicolasmendez [2:47 PM]
 yes no worry :wink:
 NEW MESSAGES
 
 nicolasmendez [2:49 PM]
 I have other precision !
 we need to make specification observation while our publication is not geolocated
 */

- (IBAction)onNext:(id)sender {

    
    NSString *strTmp = legendTxt.text;
    
    NSString *trimmedString = [strTmp stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    if ([PublicationOBJ sharedInstance].isEditer) {
            [PublicationOBJ sharedInstance].legend = trimmedString;
        [[PublicationOBJ sharedInstance]  modifiPublication:self  withType:EDIT_LEGEND];
    }
    else
    {
        [PublicationOBJ sharedInstance].legend = trimmedString;
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        [dicFavo setValue:trimmedString forKey:@"legend"];
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
        if (self.isEditFavo) {
            [self gotoback];
        }
        else
        {
            Publication_Color *viewController1 = [[Publication_Color alloc] initWithNibName:@"Publication_Color" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }


    }
}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
//    if (textField == legendTxt)
//        [scrollKeyboard setContentOffset:CGPointMake(0, 270) animated:YES];
//    else if (textField == textfieldDebut)
//        [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 270) animated:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [scrollKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    //Validate next step
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)a shouldChangeCharactersInRange:(NSRange)b replacementString:(NSString *)c {
    
    BOOL bCheck = (   (a.text.length+c.length<=maxNumChar)  +   (c.length<1) + (b.length>=c.length) >0    );

    return bCheck;
}

- (void)textFieldDidChange:(NSNotification *)info {
    // Update the character count
    
    int remain = (int) (maxNumChar - [[legendTxt text] length]);
    [numberRemain setText:[NSString stringWithFormat:@"(%d)", remain]];
    
    // Check if the count is over the limit
    if(remain < 0) {
        // Change the color
        [numberRemain setTextColor:[UIColor orangeColor]];
    }
    else if(remain < 3) {
        // Change the color to yellow
        [numberRemain setTextColor:[UIColor redColor]];
    }
    else {
        // Set normal color
        [numberRemain setTextColor:[UIColor whiteColor]];
    }

}

@end
