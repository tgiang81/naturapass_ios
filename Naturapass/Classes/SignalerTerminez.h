//
//  SignalerTerminez.h
//  Naturapass
//
//  Created by JoJo on 9/11/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SignalerBaseVC.h"

@interface SignalerTerminez : SignalerBaseVC
@property (nonatomic, strong) NSMutableArray *arrData;
@property (nonatomic,strong) IBOutlet UIView *viewBottom;
@property (strong, nonatomic) IBOutlet UIView *viewBottom1;
@property (nonatomic,strong) IBOutlet UIButton *btnTERMINER;
@property(nonatomic,assign) BOOL isEditFavo;
@property(nonatomic,assign) BOOL isFromFavo;
@property(assign) MEDIATYPE autoShow;
@property(nonatomic,strong) NSDictionary *dicShareExtention;

@end
