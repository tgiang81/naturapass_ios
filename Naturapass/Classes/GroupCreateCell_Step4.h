//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDCheckBox.h"
#import "AppCommon.h"

@interface GroupCreateCell_Step4: UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar;
@property (weak, nonatomic) IBOutlet MDCheckBox *btnInvite;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteOver;
@end
