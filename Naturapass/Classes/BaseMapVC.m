//
//  BaseMapVC.m
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//add

#import "BaseMapVC.h"
#import "AZNotification.h"
#import "FMDatabaseQueue.h"
#import "DatabaseManager.h"
#import "AlertVC.h"
#import "AlertSearchMapVC.h"
#import "ChassesCreate_Step1.h"
#import "MDMarkersText.h"
#import "UIImage+ProportionalFill.h"
#import "MDTitleLayersDownloader.h"
#import "TestTileLayer.h"
#import "AgendaFromMap.h"
#import "GroupEnterOBJ.h"
#import "NSDate+Extensions.h"
#import "MapGlobalVC.h"
#import "PublicationFromMap.h"
#import "AlertShareDrawShapeVC.h"
#import "MapGlobalVC.h"

@interface BaseMapVC ()
{
    float headingValue;
    float viewAngleValue;

    double metTer;
    GMSCircle * whiteCircle;
    GMSMarker *textCircleTop;
    GMSMarker *textCircleBottom;
    CLLocationDirection bearing;
    BOOL isOverlayMap, isTapOnMap;
}
@end

@implementation BaseMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dicMarkerDistribution = [NSMutableDictionary new];
    
    self.myDIC_MarkerPublication = [NSMutableDictionary new];
    
    self.myDIC_MarkerAgenda = [NSMutableDictionary new];
    
    self.dicOverlays = [NSMutableDictionary new];
    self.dicDrawShape = [NSMutableDictionary new];
    self.dicPolygon = [NSMutableDictionary new];
    isFirsTime = YES;
    
//    BaseMapUI_short
    NSArray *nibArrayTab;
    
    if ([self isKindOfClass: MapGlobalVC.class] ) {
        nibArrayTab = [[NSBundle mainBundle]loadNibNamed:@"BaseMapUI_short" owner:self options:nil];
    }else{
        nibArrayTab = [[NSBundle mainBundle]loadNibNamed:@"BaseMapUI" owner:self options:nil];
    }
    
    vTmpMap = (BaseMapUI*)[nibArrayTab objectAtIndex:0];
    [vTmpMap attachToParent:self.baseMapView];
    vTmpMap.mapView_.delegate = self;
    vTmpMap.delegate = self;
    vTmpMap.datasource = self;
    
    isOverlayMap = NO;
    
    [vTmpMap updateUI];
    [vTmpMap.mapView_ setIndoorEnabled:NO];
    
    layerIGN = [[TestTileLayerIGN alloc] init];
    
    layerOverlay  = [[TestTileLayer alloc] init];
    
    layerParcel  = [[TestTileLayerParcel alloc] init];
    
    self.toussearchBar.placeholder = str(strPlaceHolderFilter_Csearch);
    
    if (self.expectTarget == ISLOUNGE) {
        
        if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
            if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
            {
                //admin
                self.dicOption =@{@"admin":@1};
                
            }else{
                self.dicOption =@{@"admin":@0};
                
            }
            
        }else{
            self.dicOption =@{@"admin":@0};
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(exitMapView) name:@"exitmapview" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActiveNotif:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActiveNotif:) name:UIApplicationWillResignActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRightToChat:) name: UPDATE_RIGHT_CHAT object:nil];
    
    self.tableControl.hidden=YES;
    self.tableControl.alpha = 0.75;
    
    // NOTE:
    // you MUST NOT add the mapScaleView yourself to the hosting map view,
    // the LXMapScaleView does that for you
    
    //init zoom level
    iZoomDefaultLevel = ZOOM_DEFAULT;

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    
    [[self navigationItem] setHidesBackButton: YES];
    
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    
    arrDistributorAnnotations = [[NSMutableArray alloc] init];
    arrPublication =[NSMutableArray new];
    self.arrTitleShapeAnnotations =[NSMutableArray new];
    
    locationManager =[[CLLocationManager alloc] init];
    locationManager.delegate=self;
    
    vTmpMap.mapView_.settings.scrollGestures = YES;
    vTmpMap.mapView_.settings.zoomGestures = YES;
    vTmpMap.mapView_.myLocationEnabled = YES;
    
    //ggtt
    //    [vTmpMap.mapView_ setMinZoom:2 maxZoom:17];
    
    //IGN test
    vTmpMap.mapView_.settings.compassButton = YES;
    //    mapView_.camera = camera;
    
    vTmpMap.mapView_.settings.allowScrollGesturesDuringRotateOrZoom = NO;
    
    vTmpMap.mapView_.delegate = self;
    //Load latest point & data cached
    NSDictionary * map_type = [[NSUserDefaults standardUserDefaults] objectForKey:concatstring([COMMON getUserId],@"CACHE_MAP_TYPE")];
    
    if (map_type) {
        vTmpMap.mapView_.mapType = [map_type[@"type"] intValue];
    }
    else
    {
        vTmpMap.mapView_.mapType = kGMSTypeNormal;
        [[NSUserDefaults standardUserDefaults] setObject:@{@"type":[NSNumber numberWithInt:vTmpMap.mapView_.mapType]} forKey:concatstring([COMMON getUserId],@"CACHE_MAP_TYPE")];
        [[NSUserDefaults standardUserDefaults]  synchronize];
    }
    
    //
    /*
     If the geolocalisation is not activated, put a popup when you arrive on EVERY maps (publication or "ma carte") and center the map on the user's commune (Address declared when registering account)
     */
    //    serAddress = [[WebServiceAPI alloc]init];
    
    [locationManager startUpdatingHeading];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate setCallback:^(CLLocation *newLocation) {
        //Log new location..checking if Heading mode -> move the center point
        
        NSLog(@">>>>>   %f", newLocation.coordinate.latitude);
        //heading mode...Move location to the new pos...
        if (self.indexTracking ==1 ) {
            
            /*
            CLLocationCoordinate2D target =
            CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
            vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:target zoom:iZoomDefaultLevel];
             */
            
            
            
            GMSCameraPosition *fancy = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                   longitude:newLocation.coordinate.longitude
                                                                        zoom:iZoomDefaultLevel
                                                                     bearing:headingValue
                                                                viewingAngle:viewAngleValue];
            [vTmpMap.mapView_ setCamera:fancy];
            

        }
        
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.btnFollowHeading.hidden = NO;
    self.btnRecenter.hidden = YES;
    self.imgRecenter.hidden = YES;
    
    [self.btnFollowHeading setImage: [UIImage imageNamed:@"ic_live_gps_normal"] forState:UIControlStateNormal];
}

-(void) updateRightToChat:(NSNotification*)notif
{}


- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void) exitMapView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    vTmpMap.mapView_.delegate = nil;
    [vTmpMap.mapView_ removeFromSuperview];
    vTmpMap.mapView_ = nil;
    
}

-(void) viewDidUnload{
    [super viewDidUnload];
}

-(void)appDidBecomeActiveNotif:(NSNotification*)notif
{
    if ( self.indexTracking == 1) {
        [self doFnTracking:TRUE];
    }
    
}

-(void)appWillResignActiveNotif:(NSNotification*)notif
{
//    [locationManager stopUpdatingHeading];
    _isHeading = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GMSMapViewDelegate

- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    NSLog(@"You tapped at %f,%f", coordinate.latitude, coordinate.longitude);
    
    //Check Tap
    isTapOnMap = TRUE;
    
    //#DRAWING_SHAPE
    //neu dang ve shape thi add them 1 marker
    
    if (self.drawingShape) {
        NSDictionary *dicDrawShape =     @{
                                           @"c_lat": @(coordinate.latitude),
                                           @"c_lon": @(coordinate.longitude),
                                           };
        [self addMarkerDrawShape:dicDrawShape];
    }
}




- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    mapView.selectedMarker = marker;
    switch ([marker.userData[@"c_marker"] intValue]) {
        case MARKER_LEGEND:
        {
            NSDictionary *mDic =marker.userData;
            
            if ([self.dicOverlays[mDic[@"c_id"]][@"shape"] isKindOfClass: [GMSPolygon class]]) {
                GMSOverlay *overlay = self.dicOverlays[mDic[@"c_id"]][@"shape"];
                
                [self showAlertEditDrawShape:overlay];
                
            }
            else
            {
                AlertEditDrawShapeVC *vcSearch = [[AlertEditDrawShapeVC alloc] init];
                
                [vcSearch doBlock:^(NSString *type) {
                }];
                [vcSearch showAlertWithDic:mDic withEdit:NO];
                /*
                 NSString *strTitle = mDic[@"c_title"];
                 AlertVC *vc = [[AlertVC alloc] initWithTitle:strTitle.uppercaseString message:mDic[@"c_description"] cancelButtonTitle:nil otherButtonTitles:str(strFERMER)];
                 
                 [vc doBlock:^(NSInteger index) {
                 if (index==0) {
                 // NON
                 }
                 else if(index==1)
                 {
                 //OUI
                 }
                 }];
                 [vc showAlert];
                 */
                
            }
            
            
            
        }
            break;
            
            
        case  MARKER_AGENDA:
        case MARKER_PUBLICATION:
        {
            [self showAllPointTogetherPositionWithDidTapMarker:marker];
            
        }
            break;
            /*
             case  MARKER_AGENDA:
             {
             [self gotoAgendaDidTapMarker:marker];
             
             }
             break;
             case MARKER_PUBLICATION:
             {
             [self gotoPublicationDidTapMarker:marker];
             //            [self gotoGroupAndAgenda:marker];
             }
             break;
             */
        case MARKER_DISTRIBUTRION:
        {
            
        }
            break;
        default:
            break;
    }
    
    return YES;
}
//-(void)gotoGroupAndAgenda:(GMSMarker *)marker
//{
//    __weak typeof(self) wself = self;
//    if ([COMMON isReachable]) {
//        
//        [COMMON addLoading:self];
//        
//        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
//        
//        [serviceObj getPublicationsActionItem:marker.userData[@"c_id"]];
//        
//        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [COMMON removeProgressLoading];
//            });
//            if([wself fnCheckResponse:response]) return;
//            
//            if (!response) {
//                return ;
//            }
//            if([response isKindOfClass: [NSArray class] ]) {
//                return ;
//            }
//            if (response[@"publication"]) {
//                //success...go detail.
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    //reload header
//                    PublicationFromMap *viewcontroller1=[[PublicationFromMap alloc]initWithNibName:@"PublicationFromMap" bundle:nil];
//                    [viewcontroller1 setDataWithPublication:@[response[@"publication"]]];
//                    [self pushVC:viewcontroller1 animate:YES expectTarget:ISLOUNGE];
//                });
//            }
//        };
//    }
//}
-(void)gotoAgendaDidTapMarker:(GMSMarker *)marker
{
    NSDictionary *mDic =marker.userData;
    
    AgendaFromMap *viewcontroller1=[[AgendaFromMap alloc]initWithNibName:@"AgendaFromMap" bundle:nil];
    viewcontroller1.dicChassesItem =  mDic;
    [self pushVC:viewcontroller1 animate:YES expectTarget:ISLOUNGE];
    
}
-(void)gotoPublicationDidTapMarker:(GMSMarker *)marker
{
    //if online -> get item's data
    __weak typeof(self) wself = self;
    if ([COMMON isReachable]) {
        
        [COMMON addLoading:self];
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj getPublicationsActionItem:marker.userData[@"c_id"]];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            dispatch_async(dispatch_get_main_queue(), ^{
                [COMMON removeProgressLoading];
                
            });
            if([wself fnCheckResponse:response]) return;
            
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            if (response[@"publication"]) {
                
                //success...go detail.
                dispatch_async(dispatch_get_main_queue(), ^{
                    /*
                    NSArray *arrGroup = response[@"publication"][@"groups"];
                    NSArray *arrHunt = response[@"publication"][@"hunts"];
                    
                    if (arrGroup.count > 0 || arrHunt.count > 0) {
                        PublicationFromMap *viewcontroller1=[[PublicationFromMap alloc]initWithNibName:@"PublicationFromMap" bundle:nil];
                        [viewcontroller1 setDataWithPublication:@[response[@"publication"]]];
                        // push
                        [viewcontroller1 setCallbackWithDelItem:^(NSDictionary *dicPublication)
                         {
                             //refresh list
                             [self refreshChatlive];
                             
                             NSArray*keys=[self.myDIC_MarkerPublication allKeys];
                             for (id c_id in keys) {
                                 GMSMarker *markerTmp = self.myDIC_MarkerPublication[c_id];
                                 if ([markerTmp.userData[@"c_marker"] intValue]== MARKER_PUBLICATION) {
                                     if ([markerTmp.userData[@"c_id"] intValue] == [marker.userData[@"c_id"]intValue]) {
                                         //remove marker
                                         markerTmp.map =nil;
                                         [self.myDIC_MarkerPublication removeObjectForKey:c_id];
                                         break;
                                     }
                                 }
                                 
                             }
                             
                         }];
                        [self pushVC:viewcontroller1 animate:YES expectTarget:ISLOUNGE];
                        return ;
                    }
                    */
                    //reload header
                    CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
                    commentVC.commentDic = response[@"publication"];
                    if (self.isLiveHunt) {
                        commentVC.isLiveHuntDetail = YES;
                    }
                    // push
                    [commentVC setCallbackWithDelItem:^(NSDictionary *dicPublication)
                     {
                         //refresh list
                         [self refreshChatlive];
                         
                         NSArray*keys=[self.myDIC_MarkerPublication allKeys];
                         for (id c_id in keys) {
                             GMSMarker *markerTmp = self.myDIC_MarkerPublication[c_id];
                             if ([markerTmp.userData[@"c_marker"] intValue]== MARKER_PUBLICATION) {
                                 if ([markerTmp.userData[@"c_id"] intValue] == [marker.userData[@"c_id"]intValue]) {
                                     //remove marker
                                     markerTmp.map =nil;
                                     [self.myDIC_MarkerPublication removeObjectForKey:c_id];
                                     break;
                                 }
                             }
                             
                         }
                         
                     }];
                    
                    [self pushVC:commentVC animate:YES expectTarget:ISCARTE];
                });
                
            }
        };
        
    }
    else
    {
        //Show comment offline mode
        
        //querry from db to get text + geo + name owner.
        
        //marker.userData[@"c_id"]
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSMutableString *mutStr = [NSMutableString new];
        
        [mutStr setString:@"SELECT DISTINCT * FROM tb_carte "];
        [mutStr appendString: [NSString stringWithFormat:@" WHERE c_id = %@ ",marker.userData[@"c_id"]] ];
        [mutStr appendString:[ NSString stringWithFormat:@" AND ( c_user_id=%@ OR c_user_id= %d)  ", sender_id, SPECIAL_USER_ID]];
        
        
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db)
         {
             FMResultSet *set_querry = [db  executeQuery:mutStr];
             
             NSString *mOwnerName = @"Test";
             
             NSString *mContent = @"Test";
             NSString *mLat =@"";
             NSString *mLon =@"";
             int iShare = 0;
             
             while ([set_querry next])
             {
                 mOwnerName = CHECKSTRING([set_querry stringForColumn:@"c_owner_name"]);
                 
                 mContent = CHECKSTRING([set_querry stringForColumn:@"c_text"]);
                 
                 mLat = CHECKSTRING([set_querry stringForColumn:@"c_lat"]);
                 mLon = CHECKSTRING([set_querry stringForColumn:@"c_lon"]);
                 iShare = [set_querry intForColumn:@"c_sharing"];
                 
             }
             // Nil param???
             //if any error => don't go
             
             dispatch_async( dispatch_get_main_queue(), ^{
                 CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
                 commentVC.commentDic = @{
                                          @"owner": @{@"firstname": mOwnerName},
                                          @"content" : mContent,
                                          @"geolocation":@{@"latitude":mLat,
                                                           @"longitude" :mLon,
                                                           },
                                          @"sharing": @{@"share": [NSNumber numberWithInt: iShare ]}
                                          };
                 if (self.isLiveHunt) {
                     commentVC.isLiveHuntDetail = YES;
                 }
                 [self pushVC:commentVC animate:YES expectTarget:ISCARTE];
                 
                 [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
                 }];
                 
             });
             
             
         }];
    }
}
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker {
    switch ([marker.userData[@"c_marker"] intValue]) {
        case MARKER_DISTRIBUTRION:
        {
            MDDistributionView *view = [[MDDistributionView alloc] initForData:marker.userData];
            
            CGPoint point = [vTmpMap.mapView_.projection pointForCoordinate:marker.position];
            point.x = point.x + marker.icon.size.width/2;
            point.y = point.y - view.frame.size.height/2;
            
            GMSCameraUpdate *camera = [GMSCameraUpdate setTarget:[vTmpMap.mapView_.projection coordinateForPoint:point]];
            [vTmpMap.mapView_ animateWithCameraUpdate:camera];
            
            return view;
        }
            break;
            /*
             case MARKER_DRAWSHAPE:
             {
             UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
             view.backgroundColor = [UIColor redColor];
             
             return view;
             }
             break;
             */
        default:
            break;
    }
    
    return nil;
}

- (void)mapView:(GMSMapView *)mapView didCloseInfoWindowOfMarker:(GMSMarker *)marker
{
    /*
     switch ([marker.userData[@"c_marker"] intValue]) {
     case MARKER_DRAWSHAPE:
     {
     marker.map = nil;
     }
     break;
     default:
     break;
     }
     */
    
}
#pragma mark - Apple Location delegate

//MARK: --- CIRCLE
-(void) refreshCircle
{
    /*
     if (!(self.expectTarget == ISCARTE || (self.expectTarget == ISLOUNGE && self.isLiveHunt))) {
     return;
     }
     */
    
    [self.mapScaleView hideShowLabel:NO];
    
    double meter = 0;
    meter = self.currentDist/3; //metter
    
    [self.mapScaleView update:meter];
    
    [self.mapScaleView reloadBar: vTmpMap.mapView_.frame.size.width/3];
    
    if (whiteCircle) {
        whiteCircle.map = nil;
        whiteCircle = nil;
    }
    
    if (meter != 0) {
        whiteCircle = [GMSCircle circleWithPosition: vTmpMap.mapView_.myLocation.coordinate radius:meter];
        whiteCircle.strokeWidth = 0.7;
        whiteCircle.strokeColor = [UIColor whiteColor];
        whiteCircle.map = vTmpMap.mapView_;
        //add text
        
        [self addTextCircle:meter];
    }
    
    
}
-(void)removeCircle
{
    if (whiteCircle) {
        whiteCircle.map = nil;
    }
    
}
-(void)addTextCircle:(double)meter
{
    
    NSUInteger maxValue;
    NSString* unit;
    
    if ( meter >= 1000.0f )
    {
        maxValue = meter / 1000.0f;
        // use kilometer scale
        unit = @"km";
    }
    else
    {
        // use meter scale
        unit = @"m";
        maxValue = meter;
    }
    
    //top
    CLLocationCoordinate2D position = [self coordinateFromCoord:vTmpMap.mapView_.myLocation.coordinate atDistanceKm:self.currentDist/2.8 atBearingDegrees:0];
    if (textCircleTop == nil) {
        textCircleTop = [GMSMarker markerWithPosition:position];
        textCircleTop.infoWindowAnchor = CGPointMake(0.5, 0.5);
        textCircleTop.groundAnchor = CGPointMake(0.5, 0.5);
        textCircleTop.map = vTmpMap.mapView_;
        
        
    }
    
    //update position
    textCircleTop.position = position;
    
    //bottom
    CLLocationCoordinate2D position_bottom = [self coordinateFromCoord:vTmpMap.mapView_.myLocation.coordinate atDistanceKm:self.currentDist/2.8 atBearingDegrees:180];
    if (textCircleBottom == nil) {
        textCircleBottom = [GMSMarker markerWithPosition:position_bottom];
        textCircleBottom.infoWindowAnchor = CGPointMake(0.5, 0.5);
        textCircleBottom.groundAnchor = CGPointMake(0.5, 0.5);
        textCircleBottom.map = vTmpMap.mapView_;
        
    }
    
    //update position
    textCircleBottom.position = position_bottom;
    
    MDMarkersText *tmpImageBottom= [[MDMarkersText alloc] initWithText:[NSString stringWithFormat:@"%lu %@",(unsigned long)maxValue,unit]];
    [tmpImageBottom setCallBackMarkers:^(UIImage *image)
     {
         textCircleTop.icon = image;
         textCircleTop.map = vTmpMap.mapView_;
         
         textCircleBottom.icon = image;
         textCircleBottom.map = vTmpMap.mapView_;
         
     }];
    [tmpImageBottom doSetCalloutView];
    
}

- (double)radiansFromDegrees:(double)degrees
{
    return degrees * (M_PI/180.0);
}

- (double)degreesFromRadians:(double)radians
{
    return radians * (180.0/M_PI);
}

- (CLLocationCoordinate2D)coordinateFromCoord:
(CLLocationCoordinate2D)fromCoord
                                 atDistanceKm:(double)distanceKm
                             atBearingDegrees:(double)bearingDegrees
{
    double distanceRadians = distanceKm / (1000*6371.0);
    //6,371 = Earth's radius in km
    double bearingRadians = [self radiansFromDegrees:bearingDegrees];
    double fromLatRadians = [self radiansFromDegrees:fromCoord.latitude];
    double fromLonRadians = [self radiansFromDegrees:fromCoord.longitude];
    
    double toLatRadians = asin( sin(fromLatRadians) * cos(distanceRadians)
                               + cos(fromLatRadians) * sin(distanceRadians) * cos(bearingRadians) );
    
    double toLonRadians = fromLonRadians + atan2(sin(bearingRadians)
                                                 * sin(distanceRadians) * cos(fromLatRadians), cos(distanceRadians)
                                                 - sin(fromLatRadians) * sin(toLatRadians));
    
    // adjust toLonRadians to be in the range -180 to +180...
    toLonRadians = fmod((toLonRadians + 3*M_PI), (2*M_PI)) - M_PI;
    
    CLLocationCoordinate2D result;
    result.latitude = [self degreesFromRadians:toLatRadians];
    result.longitude = [self degreesFromRadians:toLonRadians];
    return result;
}


double getDistanceMetresBetweenLocationCoordinates(
                                                   CLLocationCoordinate2D coord1,
                                                   CLLocationCoordinate2D coord2)
{
    CLLocation* location1 =     [[CLLocation alloc]      initWithLatitude: coord1.latitude      longitude: coord1.longitude];
    CLLocation* location2 =     [[CLLocation alloc]      initWithLatitude: coord2.latitude      longitude: coord2.longitude];
    
    return [location1 distanceFromLocation: location2];
}

#pragma mark - CLICK MEDIA ITEM

-(void) refreshChatlive{}

//MARK:- search point via c_search

-(void) doPublication{}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    self.myC_Search_Text = searchText;
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self showBlurBackGround];
    [searchBar setShowsCancelButton:YES animated:YES];
    UITextField *searchBarTextField = [searchBar valueForKey:@"_searchField"];
    
    searchBarTextField.enablesReturnKeyAutomatically = NO;
    //show opaque view...here...// hide it when finish search...
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    //
    
    //clear points...reload normal data...
    self.myC_Search_Text = @"";
    [self doClearAll];
}

-(void) cancelCurrentTasks{}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSString *strsearchValues=self.toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.toussearchBar.text=strsearchValues;
    
    self.myC_Search_Text = strsearchValues;
    
    [self cancelCurrentTasks];
    
    [self doClearAll];
    
    [theSearchBar resignFirstResponder];
    
}

-(void) doClearAll
{
    [self hideBlurBackGround];
    [self removeAllPublication];
    [self removeAllAgenda];
    [self.dicOverlays removeAllObjects];
    [vTmpMap.mapView_ clear];
    
    [self doMapType:iMapType];
    
    [self doReloadRefresh];
}

//MARK: UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""]) {
        
        [self clearSearchResults];
        
    } else {
        
        [[HNKGooglePlacesAutocompleteQuery sharedQuery] resetAPIKEY:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
        
        [self.searchQuery fetchPlacesForSearchQuery:searchString
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                                 }];
                                                 
                                             } else {
                                                 self.searchResults = places;
                                                 [self.tableControl reloadData];
                                             }
                                         }];
    }
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kHNKDemoSearchResultsCellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //enable request
    iCanRequest = YES;
    isDragging = NO;

    //move to other place ... other idle function delegate called -> auto display button Center...
    
    HNKGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    [CLPlacemark hnk_placemarkFromGooglePlace:place
                                       apiKey:self.searchQuery.apiKey
                                   completion:^(CLLocation *m_Placemark, NSString *addressString, NSError *error) {
                                       if (error) {
                                           [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                           }];
                                           
                                       } else if (m_Placemark) {
                                           if (self.searchResults.count > indexPath.row) {
                                               [self addPlacemarkAnnotationToMap:m_Placemark addressString:addressString];
                                               [self recenterMapToPlacemark:m_Placemark];
                                               [self.tableControl
                                                deselectRowAtIndexPath:indexPath
                                                animated:NO];
                                               self.tableControl.hidden=YES;
                                               self.searchAddress.text=  [self placeAtIndexPath:indexPath].name;
                                               [self.searchAddress setShowsCancelButton:NO animated:YES];
                                               [self.searchAddress resignFirstResponder];
                                               
                                               
                                               [self doReloadRefresh];
                                           }
                                       }
                                   }];
    
}

-(void)showAlertSearch
{
    AlertSearchMapVC *vcSearch = [[AlertSearchMapVC alloc] init];
    
    [vcSearch doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
        //enable request
        iCanRequest = YES;
        isDragging = NO;
        [self removeAllPublication];
        [self removeAllAgenda];
        [self.dicOverlays removeAllObjects];
        
        [vTmpMap.mapView_ clear];
        [self doMapType:iMapType];
        
        [self addPlacemarkAnnotationToMap:m_Placemark addressString:addressString];
        [self recenterMapToPlacemark:m_Placemark];
        [self doReloadRefresh];
    }];
    [vcSearch showAlert];
}
//need overwrite
-(void)doReloadRefresh
{}

#pragma mark - Helpers

- (HNKGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.searchResults.count <indexPath.row)
        return nil;
    
    return self.searchResults[indexPath.row];
}

#pragma mark Map Helpers

- (void)addPlacemarkAnnotationToMap:(CLLocation *)mLocation addressString:(NSString *)address
{
    //remove
    self.selectedPlaceAnnotation = nil;
    
    //add new
    self.selectedPlaceAnnotation = [GMSMarker markerWithPosition:mLocation.coordinate];
    
    self.selectedPlaceAnnotation.map = vTmpMap.mapView_;
}

- (void)recenterMapToPlacemark:(CLLocation *)m_Placemark
{
    vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:m_Placemark.coordinate zoom: vTmpMap.mapView_.camera.zoom];
}

#pragma mark Search Helpers

- (void)clearSearchResults
{
    self.searchResults = @[];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}


//Can xem xet khi nao goi...dung lam gi

#pragma mark - locationManagerDelegates


//- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
-(BOOL)locationCheckStatusDenied
{
    if(![CLLocationManager locationServicesEnabled] ||
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strTitle_app) message:@"La localisation est inactive. Souhaitez-vous l'activer ?"  cancelButtonTitle:str(strAnuler) otherButtonTitles:str(strOK)];
        [vc doBlock:^(NSInteger index, NSString *str) {
            if (index==0) {
                // NON
            }
            else if(index==1)
            {
                //OUI
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }
        }];
        [vc showAlert];
        return YES;
    }
    return NO;
}

-(IBAction)fnComeBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
//IGN map...min zoom level: 17

-(IBAction)fnIncrease
{
    iZoomDefaultLevel = vTmpMap.mapView_.camera.zoom;
    iZoomDefaultLevel += 1;
    
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomIn];
    [vTmpMap.mapView_ animateWithCameraUpdate:zoomCamera];
}

-(IBAction)fnDecrease
{
    iZoomDefaultLevel = vTmpMap.mapView_.camera.zoom;
    iZoomDefaultLevel -= 1;
    
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomOut];
    [vTmpMap.mapView_ animateWithCameraUpdate:zoomCamera];
    
    [self doReloadRefresh];
}


//distribution
//agenda
-(void)addDistribution:(GMSMarker*)marker
{
    [self.dicMarkerDistribution setObject:marker forKey:marker.userData[@"c_id"]];
}

-(void)removeAllDistribution
{
    //    [self.arrMarkerPublication removeAllObjects];
    
    [self.dicMarkerDistribution removeAllObjects];
    
}


//agenda
-(void)addAgenda:(GMSMarker*)marker
{
    //    [self.arrMarkerPublication addObject:marker];
    [self.myDIC_MarkerAgenda setObject:marker forKey:marker.userData[@"c_id"]];
}

-(void)removeAllAgenda
{
    //    [self.arrMarkerPublication removeAllObjects];
    
    [self.myDIC_MarkerAgenda removeAllObjects];
    
}

-(void)removeAgendaWithKey:(NSString*) key
{
    [self.myDIC_MarkerAgenda removeObjectForKey:key];
}




-(void)addPublication:(GMSMarker*)marker
{
    //    [self.arrMarkerPublication addObject:marker];
    [self.myDIC_MarkerPublication setObject:marker forKey:marker.userData[@"c_id"]];
}

-(void)removeAllPublication
{
    //    [self.arrMarkerPublication removeAllObjects];
    
    [self.myDIC_MarkerPublication removeAllObjects];
    
}

-(void)removePublicationWithKey:(NSString*) key
{
    [self.myDIC_MarkerPublication removeObjectForKey:key];
}


//
#pragma mark - GMS marker
-(void)GMSaddMarkerLegendMap:(NSDictionary*)dic
{
    NSDictionary *dicLegend =     @{
                                    @"c_id":dic[@"c_id"],
                                    @"c_lat": dic[@"c_lat_center"],
                                    @"c_lon": dic[@"c_lng_center"],
                                    @"c_name": @"name",
                                    @"c_address":@"c_address",
                                    @"c_title":dic[@"c_title"],
                                    @"c_description":dic[@"c_description"],
                                    @"c_marker":@(MARKER_LEGEND),
                                    };
    
    CLLocationCoordinate2D position;
    position.latitude = [dicLegend[@"c_lat"] doubleValue];
    position.longitude= [dicLegend[@"c_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    marker.userData = dicLegend;
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    MDMarkersLegend *tmpImage= [[MDMarkersLegend alloc] initWithDictionary:dicLegend withResize:NO];
    [tmpImage setCallBackMarkers:^(UIImage *image)
     {
         marker.icon = image;
         marker.map = vTmpMap.mapView_;
         if (dic[@"c_id"]) {
             NSMutableDictionary *dicTmp = [NSMutableDictionary dictionaryWithDictionary:self.dicOverlays[dic[@"c_id"]]];
             [dicTmp setObject:marker forKey:@"shapeLegend"];
             [self.dicOverlays setObject:dicTmp forKey:dic[@"c_id"]];
         }
     }];
    [tmpImage doSetCalloutView];
}
-(UIImage*)resizeImage:(UIImage*)image withLevelZoom:(NSInteger)leveZoom withResize:(BOOL)isResize withC_Marker:(int)c_marker
{
    NSInteger _heightMarker = 0;
    if (isResize) {
        _heightMarker = 12;
    }
    else
    {
        if (leveZoom >= 20) {
            _heightMarker = 32;
        }
        else if (leveZoom >= 18 && leveZoom < 20)
        {
            _heightMarker = 28;
            
        }
        else if (leveZoom >= 16 && leveZoom < 18)
        {
            _heightMarker = 24;
            
        }
        else if (leveZoom >= 14 && leveZoom < 16)
        {
            _heightMarker = 20;
            
        }
        else if (leveZoom >= 12 && leveZoom < 14)
        {
            _heightMarker = 16;
            
        }
        else
        {
            _heightMarker = 12;
            
        }
    }
    if (c_marker == MARKER_AGENDA) {
        _heightMarker += 10;
    }
    CGSize sizeImage = image.size;
    sizeImage.width = _heightMarker*sizeImage.width/sizeImage.height;
    sizeImage.height = _heightMarker;
    UIImage *imagTemp = [image imageScaledToFitSize:sizeImage];
    return imagTemp;
}
-(UIImage*)resizeImageDistribution:(UIImage*)image withLevelZoom:(NSInteger)leveZoom
{
    NSInteger _heightMarker = 62;
    if (leveZoom >= 20) {
        _heightMarker -=4*1 ;
    }
    else if (leveZoom >= 18 && leveZoom < 20)
    {
        _heightMarker -=4*2 ;
        
    }
    else if (leveZoom >= 16 && leveZoom < 18)
    {
        _heightMarker -=4*3 ;
        
    }
    else if (leveZoom >= 14 && leveZoom < 16)
    {
        _heightMarker -=4*4 ;
        
    }
    else if (leveZoom >= 12 && leveZoom < 14)
    {
        _heightMarker -=4*5 ;
        
    }
    else
    {
        _heightMarker -=4*6 ;
        
    }
    CGSize sizeImage = image.size;
    sizeImage.width = _heightMarker*sizeImage.width/sizeImage.height;
    sizeImage.height = _heightMarker;
    UIImage *imagTemp = [image imageScaledToFitSize:sizeImage];
    return imagTemp;
    
}

#pragma mark - shapes
-(void)loadOptionShapes:(NSArray*)mapShapes
{
    //if drawing then return
    if (self.drawingShape) {
        return;
    }
    for (NSDictionary *dicShapes in mapShapes) {
        NSString *strType =dicShapes[@"c_type"];
        if ([strType isEqualToString:SHAPE_TYPE_POLYGON]) {
            [self addPolygon:dicShapes];
        }
        else if ([strType isEqualToString:SHAPE_TYPE_POLYLINE])
        {
            [self addPolyline:dicShapes];
            
        }
        else if ([strType isEqualToString:SHAPE_TYPE_CIRCLE])
        {
            [self addCircle:dicShapes];
            
        }
        else if ([strType isEqualToString:SHAPE_TYPE_RECTANGLE])
        {
            [self addRectangle:dicShapes];
        }
    }
}

-(void)addPolygon:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"paths"];
    
    // Create a rectangular path
    GMSMutablePath *rect = [GMSMutablePath path];
    
    for(int i = 0; i < listPoint.count; i++) {
        [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[i][@"lat"] doubleValue],[listPoint[i][@"lng"] doubleValue])];
    }
    
    // Create the polygon, and assign it to the map.
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 0.7;
    polygon.tappable = TRUE;
    polygon.map = vTmpMap.mapView_;
    polygon.userData = dic;
    [self.dicOverlays setObject:@{@"c_id":dic[@"c_id"],
                                  @"c_updated":dic[@"c_updated"],
                                  @"shape":polygon,
                                  } forKey:dic[@"c_id"]];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self GMSaddMarkerLegendMap:dic];
    }
    
}

-(void)addPolyline:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"paths"];
    
    
    GMSMutablePath *path = [GMSMutablePath path];
    
    for(int i = 0; i < listPoint.count; i++) {
        [path addCoordinate:CLLocationCoordinate2DMake([listPoint[i][@"lat"] doubleValue],[listPoint[i][@"lng"] doubleValue])];
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    
    polyline.strokeColor = [self convertColorWithString:dicShapes[@"options"][@"color"]];
    
    polyline.strokeWidth = 1;
    polyline.geodesic = YES;
    polyline.map = vTmpMap.mapView_;
    [self.dicOverlays setObject:@{@"c_id":dic[@"c_id"],
                                  @"c_updated":dic[@"c_updated"],
                                  @"shape":polyline,
                                  } forKey:dic[@"c_id"]];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self GMSaddMarkerLegendMap:dic];
    }
    
}

-(void)addCircle:(NSDictionary*)dic
{
    NSDictionary *dicShapes =dic[@"c_data"];
    CLLocationCoordinate2D coord_Shapes;
    coord_Shapes.latitude = [dicShapes[@"center"][@"lat"] doubleValue];
    coord_Shapes.longitude= [dicShapes[@"center"][@"lng"] doubleValue];
    double radius_Shapes = [dicShapes[@"radius"] doubleValue];
    
    GMSCircle *circ = [GMSCircle circleWithPosition:coord_Shapes
                                             radius:radius_Shapes];
    circ.strokeWidth = 0.7;
    circ.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    circ.map = vTmpMap.mapView_;
    [self.dicOverlays setObject:@{@"c_id":dic[@"c_id"],
                                  @"c_updated":dic[@"c_updated"],
                                  @"shape":circ,
                                  } forKey:dic[@"c_id"]];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self GMSaddMarkerLegendMap:dic];
    }
}

-(void)addRectangle:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"bounds"];
    
    
    // Create a rectangular path
    GMSMutablePath *rect = [GMSMutablePath path];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[0][@"lat"] doubleValue],[listPoint[0][@"lng"] doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[1][@"lat"] doubleValue],[listPoint[1][@"lng"]  doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[2][@"lat"] doubleValue],[listPoint[2][@"lng"] doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[3][@"lat"] doubleValue],[listPoint[3][@"lng"] doubleValue])];
    
    // Create the polygon, and assign it to the map.
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 0.7;
    //    polygon.tappable = TRUE;
    polygon.map = vTmpMap.mapView_;
    [self.dicOverlays setObject:@{@"c_id":dic[@"c_id"],
                                  @"c_updated":dic[@"c_updated"],
                                  @"shape":polygon,
                                  } forKey:dic[@"c_id"]];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self GMSaddMarkerLegendMap:dic];
    }
}

-(UIColor*)convertColorWithStringWithAlpha:(NSString*)strColor
{
    strColor = [strColor stringByReplacingOccurrencesOfString:@"#"
                                                   withString:@""];
    
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:strColor];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&result];
    return UIColorFromRGBAlpha(result , 0.3f);
}

-(UIColor*)convertColorWithString:(NSString*)strColor
{
    strColor = [strColor stringByReplacingOccurrencesOfString:@"#"
                                                   withString:@""];
    
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:strColor];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&result];
    return UIColorFromRGB(result);
}

-(void) doMapType:(int) mType
{
//    [[NSUserDefaults standardUserDefaults] setInteger: mType forKey:@"defaultMapType"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    switch (mType) {
        case 10:
        {
            layerIGN.map = nil;
            layerParcel.map = nil;
            vTmpMap.mapView_.mapType = kGMSTypeNormal;
        }
            
            break;
        case 11:
        {
            layerIGN.map = nil;
            layerParcel.map = nil;
            
            vTmpMap.mapView_.mapType = kGMSTypeSatellite;
        }
            
            break;
        case 12:
        {
            layerIGN.map = nil;
            layerParcel.map = nil;
            
            vTmpMap.mapView_.mapType = kGMSTypeHybrid;//
        }
            
            break;
        case 13:
        {
            layerIGN.map = nil;
            layerParcel.map = nil;
            vTmpMap.mapView_.mapType = kGMSTypeTerrain;//
        }
            break;
        case 14:
        {
            layerParcel.map = nil;
            //switch IGN map type
            vTmpMap.mapView_.mapType = kGMSTypeNone;//
            
            layerIGN.map = vTmpMap.mapView_;
            
        }
            break;
            
        case 15:
        {
            layerIGN.map = nil;
            vTmpMap.mapView_.mapType = kGMSTypeNone;//
            
            layerParcel.map = vTmpMap.mapView_;
            
        }
            break;
            
        default:
            break;
    }
    
    //save maptype
    [[NSUserDefaults standardUserDefaults] setInteger: mType forKey:@"defaultMapType"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    //decide overlay
    //neu iZoomDefaultLevel >= 15, tat che do cadastre
    // nen mType = 15 thi luon luon hien thi cadastre
    if (([CommonHelper sharedInstance].isActiveOverlay && iZoomDefaultLevel >= 15)
        || mType == 15)
    {
        sleep(0.75);
        
        layerOverlay.map = nil;
        
        layerOverlay.map = vTmpMap.mapView_;
        
    }else{
        layerOverlay.map = nil;
    }
    
}

-(IBAction)CarteTypeAction:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initMapType: iMapType withColor:_colorNavigation];
    
    [vc doBlock:^(NSInteger index, NSString *str) {
        //is map type selected, not close action
        if (index > 1) {
            iMapType = (int)index;
            [self doMapType:iMapType];
            
        }
    }];
    [vc showAlert];
    
}


-(void)settingBlurBackGround
{
}
-(void)hideBlurBackGround
{
    
}
-(void)showBlurBackGround
{
}
-(void)downloadTitleLayer
{
    GMSVisibleRegion visibleRegion;
    visibleRegion = vTmpMap.mapView_.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithRegion: visibleRegion];
    CLLocationCoordinate2D northEast = bounds.northEast;
    CLLocationCoordinate2D southWest = bounds.southWest;
    [[MDTitleLayersDownloader sharedInstance] cacheMapForNorthEast:northEast withSouthWest:southWest withZoom:iZoomDefaultLevel];
}
//MARK - #DRAWING_SHAPE
-(IBAction)fnCloseDrawShape:(id)sender
{
    //
    [self rollbackShape];
    //
    [self disableDrawShape];
    [self removeDrawshape];
}
-(IBAction)fnValiderDrawShape:(id)sender
{
    [self showAlertColorDrawShape];
}
-(void)removeDrawshape
{
    
    for (GMSMarker *marker in self.dicDrawShape.allValues) {
        NSDictionary *dicData = marker.userData;
        marker.map = nil;
        if (dicData[@"c_id"]) {
            [self.dicDrawShape removeObjectForKey: dicData[@"c_id"]];
        }
    }
    [self updateDataDrawShape];
    
    [self.dicPolygon removeAllObjects];
    [self.dicDrawShape removeAllObjects];
    
}
-(IBAction) enableDrawShape
{
    //clear data befor new draw
    [self fnCloseDrawShape:nil];
    //
    self.drawingShape = TRUE;
    vTmpMap.btnShortCut.hidden = TRUE;
    vTmpMap.viewDrawShape.hidden = NO;
}
-(void)disableDrawShape
{
    self.drawingShape = FALSE;
    vTmpMap.btnShortCut.hidden = FALSE;
    vTmpMap.viewDrawShape.hidden = TRUE;
}
-(void)showAlertColorDrawShape
{
    NSDictionary *dicOvelays = self.dicPolygon[@"c_old_data"];
    NSDictionary *userData = nil;
    if (dicOvelays) {
        GMSOverlay *overlay = (GMSOverlay*)dicOvelays[@"shape"];
        userData = [overlay.userData mutableCopy];
    }
    
    AlertColorDrawShapeVC *vcSearch = [[AlertColorDrawShapeVC alloc] init];
    
    [vcSearch doBlock:^(NSDictionary *dic) {
        if (dic) {
            //default is black color
            //call api
            NSMutableDictionary *dicTmp = [NSMutableDictionary dictionaryWithDictionary:self.dicPolygon[@"c_data"]];
            //set color
            if (userData) {
                [dicTmp setObject:userData[@"c_data"][@"options"] forKey:@"options"];
            }
            else
            {
                [dicTmp setObject:@{@"color" : @"000000"} forKey:@"options"];
            }
            
            if (dic[@"title"]) {
                [dicTmp setObject:dic[@"title"] forKey:@"c_title"];
            }
            if (dic[@"description"]) {
                [dicTmp setObject:dic[@"description"] forKey:@"c_description"];
            }
            if (dic[@"color"]) {
                [dicTmp setObject:@{@"color" : dic[@"color"]} forKey:@"options"];
            }
            if (dic[@"sharing"]) {
                [dicTmp setObject:dic[@"sharing"] forKey:@"sharing"];
            }
            if (dic[@"groups"]) {
                [dicTmp setObject:dic[@"groups"] forKey:@"groups"];
            }
            if (dic[@"hunts"]) {
                [dicTmp setObject:dic[@"hunts"] forKey:@"hunts"];
            }
            [self.dicPolygon setObject:dicTmp forKey:@"c_data"];
            
            [self postShape];
        }
        
    }];
    [vcSearch showAlertWithDic:userData];
}
//{"shape":{"data":{"paths":[[46.54448752760141,5.762972831726074],[46.5469963971072,5.7558488845825195],[46.54407429091397,5.755462646484375]],"options":{"color":"#ff0000"}},"type":"polygon","title":"f","description":"d","sharing":0,"groups":[],"hunts":[]}}
-(void)postShape
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if (self.dicPolygon[@"c_data"]) {
        //
        NSMutableDictionary *dicData = [NSMutableDictionary new];
        [dicData setObject:self.dicPolygon[@"c_data"][@"options"] forKey:@"options"];
        //
        NSMutableArray *arrPath = [NSMutableArray new];
        for (NSDictionary *dicGeo in self.dicPolygon[@"c_data"][@"paths"]) {
            [arrPath addObject:@[dicGeo[@"lat"],dicGeo[@"lng"]]];
        }
        [dicData setObject:arrPath forKey:@"paths"];
        //
        [dic setObject:dicData forKey:@"data"];
        //
        if (self.dicPolygon[@"c_data"][@"c_type"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"c_type"] forKey:@"type"];
        }
        //
        if (self.dicPolygon[@"c_data"][@"c_title"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"c_title"] forKey:@"title"];
            
        }
        if (self.dicPolygon[@"c_data"][@"c_description"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"c_description"] forKey:@"description"];
            
        }
        //
        if (self.dicPolygon[@"c_data"][@"sharing"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"sharing"] forKey:@"sharing"];
            
        }
        if (self.dicPolygon[@"c_data"][@"groups"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"groups"] forKey:@"groups"];
            
        }
        if (self.dicPolygon[@"c_data"][@"hunts"]) {
            [dic setObject:self.dicPolygon[@"c_data"][@"hunts"] forKey:@"hunts"];
            
        }
    }
    NSString *shape_id = nil;
    if (self.dicPolygon[@"c_old_data"]) {
        NSDictionary *dicOvelays = self.dicPolygon[@"c_old_data"];
        GMSOverlay *overlay = (GMSOverlay*)dicOvelays[@"shape"];
        NSMutableDictionary *userData = [overlay.userData mutableCopy];
        shape_id = userData[@"c_id"];
    }
    [COMMON addLoadingForView:self.view];
    //Request
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if (shape_id) {
        [serviceObj fnPUT_SHAPES:@{@"shape":dic} withShapeID:shape_id];
    }
    else
    {
        [serviceObj fnPOST_SHAPES:@{@"shape":dic}];
        
    }
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        if (response[@"shapeId"] || response[@"success"]) {
            [self showIndicator];
            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_shape]];
            [MapDataDownloader sharedInstance].CallBackEditShape = ^(){
                    [self disableDrawShape];
                    [self removeDrawshape];
                [MapDataDownloader sharedInstance].CallBackEditShape = nil;
            };

        }
        else if (response[@"message"]){
            //message timeout...
            
            [KSToastView ks_showToast:response[@"message"]  duration:3.0f completion: ^{
            }];
        }
        else{
            //message timeout...
            
            [KSToastView ks_showToast:str(strRequestTimeout)  duration:3.0f completion: ^{
            }];
        }
    };
    
}


-(void)addMarkerDrawLines:(NSDictionary*)dic
{
    NSInteger count = [self.dicDrawShape allKeys].count +  1;
    
    NSDictionary *dicTmp =     @{
                                 @"c_id": @(count),
                                 @"c_lat": dic[@"c_lat"],
                                 @"c_lon": dic[@"c_lon"],
                                 @"c_marker":@(MARKER_DRAWSHAPE),
                                 };
    
    CLLocationCoordinate2D position;
    position.latitude = [dicTmp[@"c_lat"] doubleValue];
    position.longitude= [dicTmp[@"c_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    marker.userData = dicTmp;
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    UIImage *image =  [UIImage imageNamed:@"ic_dot_white"];
    CGSize sizeImage = CGSizeMake(20, 20);
    UIImage *imagTemp = [image imageScaledToFitSize:sizeImage];
    marker.icon = imagTemp;
    
    marker.map = vTmpMap.mapView_;
    marker.draggable=YES;
    
    [self.dicDrawShape setObject:marker forKey:dicTmp[@"c_id"]];
    [self updateDataDrawShapeLine];
}

-(void)addMarkerDrawShape:(NSDictionary*)dic
{
    NSInteger count = [self.dicDrawShape allKeys].count +  1;
    
    NSDictionary *dicTmp =     @{
                                 @"c_id": @(count),
                                 @"c_lat": dic[@"c_lat"],
                                 @"c_lon": dic[@"c_lon"],
                                 @"c_marker":@(MARKER_DRAWSHAPE),
                                 };
    
    CLLocationCoordinate2D position;
    position.latitude = [dicTmp[@"c_lat"] doubleValue];
    position.longitude= [dicTmp[@"c_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    marker.userData = dicTmp;
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    UIImage *image =  [UIImage imageNamed:@"ic_dot_white"];
    CGSize sizeImage = CGSizeMake(20, 20);
    UIImage *imagTemp = [image imageScaledToFitSize:sizeImage];
    marker.icon = imagTemp;
    
    marker.map = vTmpMap.mapView_;
    marker.draggable=YES;
    
    [self.dicDrawShape setObject:marker forKey:dicTmp[@"c_id"]];
    [self updateDataDrawShape];
}
-(void)updateMarkerDrawShape:(GMSMarker *)marker
{
    NSDictionary *dicData = marker.userData;
    if (dicData[@"c_id"]) {
        NSMutableDictionary *dicTmp = [NSMutableDictionary dictionaryWithDictionary:dicData];
        CLLocationCoordinate2D position = marker.position;
        
        [dicTmp setObject:@(position.latitude) forKey:@"c_lat"];
        [dicTmp setObject:@(position.longitude) forKey:@"c_lon"];
        [self.dicDrawShape setObject:marker forKey:dicData[@"c_id"]];
        
    }
    [self updateDataDrawShape];
}
-(void)deleteMarkerDrawShape:(GMSMarker *)marker
{
    NSDictionary *dicData = marker.userData;
    marker.map = nil;
    if (dicData[@"c_id"]) {
        [self.dicDrawShape removeObjectForKey: dicData[@"c_id"]];
    }
    
    [self updateDataDrawShape];
}
-(void)updateDataDrawShape
{
    NSMutableDictionary *dicTmp = [NSMutableDictionary dictionaryWithDictionary:self.dicPolygon[@"c_data"]];
    if (dicTmp) {
        //remove shape
        if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSPolyline class]]) {
            
            GMSPolyline *polyline = (GMSPolyline*)self.dicPolygon[@"shape"];
            polyline.map = nil;
        }
        else if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSPolygon class]]) {
            GMSPolygon *polygon = (GMSPolygon*)self.dicPolygon[@"shape"];
            polygon.map = nil;
        }
        else if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSCircle class]]) {
            GMSCircle *pvcircle = (GMSCircle*)self.dicPolygon[@"shape"];
            pvcircle.map = nil;
        }
        //remove legend
        if ([self.dicPolygon[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
            GMSMarker *shapeLegendDel = (GMSMarker*)self.dicPolygon[@"shapeLegend"];
            shapeLegendDel.map = nil;
        }
    }
    NSDictionary *dicOvelays = self.dicPolygon[@"c_old_data"];
    if (dicOvelays) {
        GMSOverlay *overlay = (GMSOverlay*)dicOvelays[@"shape"];
        NSMutableDictionary *userData = [overlay.userData mutableCopy];
        [dicTmp setObject:userData[@"c_data"][@"options"] forKey:@"options"];
    }
    else
    {
        [dicTmp setObject:@{@"color" : @"000000"} forKey:@"options"];
    }
    [dicTmp setObject:SHAPE_TYPE_POLYGON forKey:@"c_type"];
    [dicTmp setObject:@"" forKey:@"c_title"];
    
    NSMutableArray *arrPath = [NSMutableArray new];
    NSArray *allKey = self.dicDrawShape.allKeys;
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    allKey = [allKey sortedArrayUsingDescriptors:@[sd]];
    
    for (id key in allKey) {
        GMSMarker *marker = self.dicDrawShape[key];
        CLLocationCoordinate2D position = marker.position;
        [arrPath addObject:@{@"lat":@(position.latitude),
                             @"lng":@(position.longitude)}];
    }
    [dicTmp setObject:arrPath forKey:@"paths"];
    
    [self.dicPolygon setObject:dicTmp forKey:@"c_data"];
    
    //add shape
    if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_POLYGON]) {
        [self addPolygonDrawShape:self.dicPolygon];
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_POLYLINE])
    {
        [self addPolylineDrawShape:self.dicPolygon];
        
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_CIRCLE])
    {
        [self addCircleDrawShape:self.dicPolygon];
        
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_RECTANGLE])
    {
        [self addRectangleDrawShape:self.dicPolygon];
    }
    
    
}

-(void)updateDataDrawShapeLine
{
    NSMutableDictionary *dicTmp = [NSMutableDictionary dictionaryWithDictionary:self.dicPolygon[@"c_data"]];
    if (dicTmp) {
        //remove shape
        if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSPolyline class]]) {
            
            GMSPolyline *polyline = (GMSPolyline*)self.dicPolygon[@"shape"];
            polyline.map = nil;
        }
        else if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSPolygon class]]) {
            GMSPolygon *polygon = (GMSPolygon*)self.dicPolygon[@"shape"];
            polygon.map = nil;
        }
        else if ([self.dicPolygon[@"shape"] isKindOfClass: [GMSCircle class]]) {
            GMSCircle *pvcircle = (GMSCircle*)self.dicPolygon[@"shape"];
            pvcircle.map = nil;
        }
        //remove legend
        if ([self.dicPolygon[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
            GMSMarker *shapeLegendDel = (GMSMarker*)self.dicPolygon[@"shapeLegend"];
            shapeLegendDel.map = nil;
        }
    }
    NSDictionary *dicOvelays = self.dicPolygon[@"c_old_data"];
    if (dicOvelays) {
        GMSOverlay *overlay = (GMSOverlay*)dicOvelays[@"shape"];
        NSMutableDictionary *userData = [overlay.userData mutableCopy];
        [dicTmp setObject:userData[@"c_data"][@"options"] forKey:@"options"];
    }
    else
    {
        [dicTmp setObject:@{@"color" : @"000000"} forKey:@"options"];
    }
    [dicTmp setObject:SHAPE_TYPE_POLYLINE forKey:@"c_type"];
    [dicTmp setObject:@"" forKey:@"c_title"];
    
    NSMutableArray *arrPath = [NSMutableArray new];
    NSArray *allKey = self.dicDrawShape.allKeys;
    NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    allKey = [allKey sortedArrayUsingDescriptors:@[sd]];
    
    for (id key in allKey) {
        GMSMarker *marker = self.dicDrawShape[key];
        CLLocationCoordinate2D position = marker.position;
        [arrPath addObject:@{@"lat":@(position.latitude),
                             @"lng":@(position.longitude)}];
    }
    [dicTmp setObject:arrPath forKey:@"paths"];
    
    [self.dicPolygon setObject:dicTmp forKey:@"c_data"];
    
    //add shape
    if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_POLYGON]) {
        [self addPolygonDrawShape:self.dicPolygon];
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_POLYLINE])
    {
        [self addPolylineDrawShape:self.dicPolygon];
        
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_CIRCLE])
    {
        [self addCircleDrawShape:self.dicPolygon];
        
    }
    else if ([dicTmp[@"c_type"] isEqualToString:SHAPE_TYPE_RECTANGLE])
    {
        [self addRectangleDrawShape:self.dicPolygon];
    }
    
    
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker
{
    
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    if ([marker.userData[@"c_marker"] intValue] == MARKER_DRAWSHAPE) {
        [self updateMarkerDrawShape:marker];
    }
}
- (void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker
{
    if ([marker.userData[@"c_marker"] intValue] == MARKER_DRAWSHAPE) {
        [self updateMarkerDrawShape:marker];
    }
}
-(void)addMarkerLegendDrawShape:(NSDictionary*)dic
{
    NSDictionary *dicLegend =     @{
                                    @"c_title":dic[@"c_title"]?dic[@"c_title"]:@"",
                                    @"c_description":dic[@"c_description"]?dic[@"c_description"]:@"",
                                    @"c_marker":@(MARKER_DRAWSHAPE_LEGEND),
                                    };
    
    CLLocationCoordinate2D position;
    position.latitude = [dicLegend[@"c_lat"] doubleValue];
    position.longitude= [dicLegend[@"c_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    marker.userData = dicLegend;
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    
    MDMarkersLegend *tmpImage= [[MDMarkersLegend alloc] initWithDictionary:dicLegend withResize:NO];
    [tmpImage setCallBackMarkers:^(UIImage *image)
     {
         marker.icon = image;
         marker.map = vTmpMap.mapView_;
         [self.dicPolygon setObject:marker forKey:@"shapeLegend"];
     }];
    [tmpImage doSetCalloutView];
}
-(void)addPolygonDrawShape:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"paths"];
    
    // Create a rectangular path
    GMSMutablePath *rect = [GMSMutablePath path];
    
    for(int i = 0; i < listPoint.count; i++) {
        [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[i][@"lat"] doubleValue],[listPoint[i][@"lng"] doubleValue])];
    }
    
    // Create the polygon, and assign it to the map.
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 0.7;
    polygon.map = vTmpMap.mapView_;
    //    polygon.tappable = TRUE;
    [self.dicPolygon setObject:polygon forKey:@"shape"];
    
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self addMarkerLegendDrawShape:dic];
    }
    
}
/*
 {
 "c_data" =     {
 options =         {
 color = ff0000;
 };
 paths =         (
 {
 lat = "21.028310265384";
 lng = "105.81786632538";
 },
 {
 lat = "21.029411837651";
 lng = "105.8197760582";
 },
 {
 lat = "21.029031295425";
 lng = "105.82082748413";
 },
 {
 lat = "21.027869634198";
 lng = "105.82119226456";
 },
 {
 lat = "21.027028425588";
 lng = "105.82026958466";
 },
 {
 lat = "21.026968339077";
 lng = "105.81902503967";
 }
 );
 };
 "c_description" = "";
 "c_friend" = 1;
 "c_groups" = "";
 "c_hunts" = "";
 "c_id" = 4120;
 "c_lat_center" = "21.028103303235";
 "c_lng_center" = "105.81982612546";
 "c_member" = 1;
 "c_owner_id" = 3608;
 "c_sharing" = 0;
 "c_title" = "";
 "c_type" = polygon;
 "c_updated" = 1521016743;
 }
 */
/*
 {
 "c_data" =     {
 options =         {
 color = EE82EE;
 };
 paths =         (
 {
 lat = "21.025085616132";
 lng = "105.82003355026";
 },
 {
 lat = "21.023883865648";
 lng = "105.82213640213";
 },
 {
 lat = "21.023142781354";
 lng = "105.81956148148";
 }
 );
 };
 "c_description" = "";
 "c_friend" = 1;
 "c_groups" = "";
 "c_hunts" = "";
 "c_id" = 4137;
 "c_lat_center" = "21.024037424708";
 "c_lng_center" = "105.8205771444";
 "c_member" = 1;
 "c_owner_id" = 3608;
 "c_sharing" = 0;
 "c_title" = "";
 "c_type" = polyline;
 "c_updated" = 1521088259;
 }*/
-(void)addPolylineDrawShape:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"paths"];
    
    
    GMSMutablePath *path = [GMSMutablePath path];
    
    for(int i = 0; i < listPoint.count; i++) {
        [path addCoordinate:CLLocationCoordinate2DMake([listPoint[i][@"lat"] doubleValue],[listPoint[i][@"lng"] doubleValue])];
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    
    polyline.strokeColor = [self convertColorWithString:dicShapes[@"options"][@"color"]];
    
    polyline.strokeWidth = 1;
    polyline.geodesic = YES;
    polyline.map = vTmpMap.mapView_;
    [self.dicPolygon setObject:polyline forKey:@"shape"];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self addMarkerLegendDrawShape:dic];
    }
    
}

-(void)addCircleDrawShape:(NSDictionary*)dic
{
    NSDictionary *dicShapes =dic[@"c_data"];
    CLLocationCoordinate2D coord_Shapes;
    coord_Shapes.latitude = [dicShapes[@"center"][@"lat"] doubleValue];
    coord_Shapes.longitude= [dicShapes[@"center"][@"lng"] doubleValue];
    double radius_Shapes = [dicShapes[@"radius"] doubleValue];
    
    GMSCircle *circ = [GMSCircle circleWithPosition:coord_Shapes
                                             radius:radius_Shapes];
    circ.strokeWidth = 0.7;
    circ.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    circ.map = vTmpMap.mapView_;
    [self.dicPolygon setObject:circ forKey:@"shape"];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self addMarkerLegendDrawShape:dic];
    }
}

-(void)addRectangleDrawShape:(NSDictionary*)dic
{
    NSDictionary *dicShapes = dic[@"c_data"];
    NSArray *listPoint = dicShapes[@"bounds"];
    
    
    // Create a rectangular path
    GMSMutablePath *rect = [GMSMutablePath path];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[0][@"lat"] doubleValue],[listPoint[0][@"lng"] doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[1][@"lat"] doubleValue],[listPoint[1][@"lng"]  doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[2][@"lat"] doubleValue],[listPoint[2][@"lng"] doubleValue])];
    [rect addCoordinate:CLLocationCoordinate2DMake([listPoint[3][@"lat"] doubleValue],[listPoint[3][@"lng"] doubleValue])];
    
    // Create the polygon, and assign it to the map.
    GMSPolygon *polygon = [GMSPolygon polygonWithPath:rect];
    polygon.fillColor = [self convertColorWithStringWithAlpha:dicShapes[@"options"][@"color"]];
    polygon.strokeColor = [UIColor blackColor];
    polygon.strokeWidth = 0.7;
    polygon.map = vTmpMap.mapView_;
    //    polygon.tappable = TRUE;
    [self.dicPolygon setObject:polygon forKey:@"shape"];
    //add center legend point...
    NSString *str = dic[@"c_title"];
    if (str.length > 0) {
        [self addMarkerLegendDrawShape:dic];
    }
}

//- EVENT SHAPE
- (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay
{
    [self showAlertEditDrawShape:overlay];
    /*
     //remove shape
     if ([overlay isKindOfClass: [GMSPolyline class]]) {
     
     GMSPolyline *polyline = (GMSPolyline*)self.dicPolygon[@"shape"];
     }
     else if ([overlay isKindOfClass: [GMSPolygon class]]) {
     GMSPolygon *polygon = (GMSPolygon*)self.dicPolygon[@"shape"];
     }
     else if ([overlay isKindOfClass: [GMSCircle class]]) {
     GMSCircle *pvcircle = (GMSCircle*)self.dicPolygon[@"shape"];
     }
     */
    /*
     NSMutableDictionary *dicLegend = [overlay.userData mutableCopy];
     [dicLegend setObject:@(MARKER_DRAWSHAPE) forKey:@"c_marker"];
     
     CLLocationCoordinate2D position;
     position.latitude = [dicLegend[@"c_lat_center"] doubleValue];
     position.longitude= [dicLegend[@"c_lng_center"] doubleValue];
     GMSMarker *marker = [GMSMarker markerWithPosition:position];
     
     marker.userData = dicLegend;
     marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
     marker.groundAnchor = CGPointMake(0.5, 0.5);
     InfoWindowEditShape *view = [[InfoWindowEditShape alloc] init];
     [view setCallback:^(NSString *type)
     {
     mapView.selectedMarker = nil;
     }];
     
     marker.iconView = view;
     marker.map = vTmpMap.mapView_;
     [mapView setSelectedMarker:marker];
     */
}
-(void)showAlertEditDrawShape:(GMSOverlay *)overlay
{
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    NSMutableDictionary *userData = [overlay.userData mutableCopy];
    NSString *strTitle = userData[@"c_title"];
    
    if ([overlay isKindOfClass: [GMSPolygon class]] && [userData[@"c_owner_id"] intValue] == [sender_id intValue]) {
        AlertEditDrawShapeVC *vcSearch = [[AlertEditDrawShapeVC alloc] init];
        
        [vcSearch doBlock:^(NSString *type) {
            if ([type isEqualToString:@"delete"]) {
                [self deleteShape:overlay];
            }
            else if ([type isEqualToString:@"modify"]) {
                [self fillDataEdit:overlay];
            }
        }];
        [vcSearch showAlertWithDic:userData withEdit:YES];
        
    }
    else
    {
        if (strTitle.length > 0) {
            AlertEditDrawShapeVC *vcSearch = [[AlertEditDrawShapeVC alloc] init];
            
            [vcSearch doBlock:^(NSString *type) {
            }];
            [vcSearch showAlertWithDic:userData withEdit:NO];
            
        }
    }
}
-(void)deleteShape:(GMSOverlay *)overlay
{
    NSMutableDictionary *userData = [overlay.userData mutableCopy];
    
    [COMMON addLoadingForView:self.view];
    //Request
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnDELETE_SHAPES:userData[@"c_id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        if (response[@"success"]) {
            [self showIndicator];
            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_shape]];
            [MapDataDownloader sharedInstance].CallBackEditShape = ^(){
                [self removeShape:overlay];
                [MapDataDownloader sharedInstance].CallBackEditShape = nil;
            };
        }
        else if (response[@"message"]){
            //message timeout...
            
            [KSToastView ks_showToast:response[@"message"]  duration:3.0f completion: ^{
            }];
        }
        else{
            //message timeout...
            
            [KSToastView ks_showToast:str(strRequestTimeout)  duration:3.0f completion: ^{
            }];
        }
    };
}
/*
 {
 "c_data" =     {
 options =         {
 color = FF00FF;
 };
 paths =         (
 {
 lat = "21.028330294044";
 lng = "105.81784486771";
 },
 {
 lat = "21.02937178062";
 lng = "105.81962585449";
 },
 {
 lat = "21.028911123994";
 lng = "105.82099914551";
 },
 {
 lat = "21.027829576753";
 lng = "105.82117080688";
 },
 {
 lat = "21.027108540898";
 lng = "105.82063436508";
 },
 {
 lat = "21.027128569719";
 lng = "105.81960439682";
 },
 {
 lat = "21.027108540898";
 lng = "105.81883192062";
 },
 {
 lat = "21.027609260612";
 lng = "105.81827402115";
 }
 );
 };
 "c_description" = "";
 "c_friend" = 1;
 "c_groups" = "[552],[650]";
 "c_hunts" = "";
 "c_id" = 4187;
 "c_lat_center" = "21.027924714935";
 "c_lng_center" = "105.81962317165";
 "c_member" = 1;
 "c_owner_id" = 3608;
 "c_sharing" = 1;
 "c_title" = "";
 "c_type" = polygon;
 "c_updated" = 1522292579;
 }
 
 */
-(void)fillDataEdit:(GMSOverlay *)overlay
{
    NSMutableDictionary *userData = [overlay.userData mutableCopy];
    if (userData) {
        [self enableDrawShape];
        
        [self.dicPolygon setObject:self.dicOverlays[userData[@"c_id"]] forKey:@"c_old_data"];
        [self removeShape:overlay];
        
        NSArray *listPoint = userData[@"c_data"][@"paths"];
        
        for(int i = 0; i < listPoint.count; i++) {
            NSDictionary *dicDrawShape =     @{
                                               @"c_lat": @([listPoint[i][@"lat"] doubleValue]),
                                               @"c_lon": @([listPoint[i][@"lng"] doubleValue]),
                                               };
            [self addMarkerDrawShape:dicDrawShape];
            
        }
        
    }
}
-(void)removeShape:(GMSOverlay *)overlay
{
    NSMutableDictionary *userData = [overlay.userData mutableCopy];
    //remove shape
    overlay.map = nil;
    //remove legend
    if ([self.dicOverlays[userData[@"c_id"]][@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
        GMSMarker *shapeLegendDel = (GMSMarker*)self.dicOverlays[userData[@"c_id"]][@"shapeLegend"];
        shapeLegendDel.map = nil;
    }
    //remove data
    [self.dicOverlays removeObjectForKey:userData[@"c_id"]];
    
}
-(void)rollbackShape
{
    NSDictionary *dicOvelays = self.dicPolygon[@"c_old_data"];
    if (dicOvelays) {
        GMSOverlay *overlay = (GMSOverlay*)dicOvelays[@"shape"];
        NSMutableDictionary *userData = [overlay.userData mutableCopy];
        //rollback shape
        overlay.map = vTmpMap.mapView_;
        //rollback legend
        if ([dicOvelays[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
            GMSMarker *shapeLegendDel = (GMSMarker*)dicOvelays[@"shapeLegend"];
            shapeLegendDel.map = vTmpMap.mapView_;
        }
        //rollback data
        [self.dicOverlays setObject:dicOvelays forKey:userData[@"c_id"]];
        
    }
}


- (void) mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    viewAngleValue = position.viewingAngle;
    
    
    //Firt time open the app...no processing tap on the map.
    //    if (isFirsTime == NO) {
    //        isTapOnMap = TRUE;
    //    }
}

-(void) fnResetButtonCenter
{
    //ggtt
    //reset status to my local position ...
    self.indexTracking = 0;
    self.isShowMylocation = NO;

    //
//    [locationManager stopUpdatingHeading];
    _isHeading = NO;
    
    self.btnRecenter.hidden = NO;
    self.imgRecenter.hidden = NO;
    [self.btnFollowHeading setImage: nil forState:UIControlStateNormal];
    self.btnFollowHeading.hidden = YES;
}

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    
    //idle + heading mode ? reset current location
//    if(self.isLiveHunt && self.indexTracking == 1)
    if(self.indexTracking == 1)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        

        CLLocation *location = [[CLLocation alloc] initWithLatitude:cameraPosition.target.latitude longitude:cameraPosition.target.longitude];
        
        // calculate distance between them
        CLLocationDistance distance = [location distanceFromLocation:appDelegate.locationManager.location];
        NSLog(@">>>>>>>>>>   %f",distance);
        if (distance > 30.0f)
        {//heading moving
            [self fnResetButtonCenter];
        }
    }else{
        //
        if (self.isShowMylocation == TRUE) {
            //my location + move end.
            self.isShowMylocation = FALSE;
        }
        else
        {
            
            //Not the first time anymore
            
            
            //
            //            if (isTapOnMap == TRUE) {
            //                isTapOnMap = FALSE;
            //
            //                vTmpMap.btnRecenter.hidden = NO;
            //                vTmpMap.imgRecenter.hidden = NO;
            //                //reset status.
            //                self.indexTracking = 0;
            //            }
        }
        
        //not my location => show button center.
        
        if (isFirsTime) {
            isFirsTime = NO;
            //fake
            //                self.isShowMylocation = YES;
        }else{
            
            if (self.isShowMylocation == NO) {
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                
                CLLocation *location = [[CLLocation alloc] initWithLatitude:cameraPosition.target.latitude longitude:cameraPosition.target.longitude];
                
                // calculate distance between them
                CLLocationDistance distance = [location distanceFromLocation:appDelegate.locationManager.location];
                NSLog(@">>>>>>>>>>   %f",distance);
                if (distance > 5.0f)
                {//heading moving
                    [self fnResetButtonCenter];
                }
                
            }else{
                self.btnRecenter.hidden = TRUE;
                self.imgRecenter.hidden = TRUE;
            }
        }
    }

    
    ASLog(@"Map move");
    iZoomDefaultLevel = vTmpMap.mapView_.camera.zoom;
    [self doMapType:iMapType];
    
    CGPoint middleLeftPoint = CGPointMake( 0, mapView.frame.size.height / 2);
    CGPoint middleRightPoint = CGPointMake( mapView.frame.size.width, mapView.frame.size.height / 2);
    
    CLLocationCoordinate2D middleLeftCoord =     [mapView.projection coordinateForPoint: middleLeftPoint];
    CLLocationCoordinate2D middleRightCoord =     [mapView.projection coordinateForPoint: middleRightPoint];
    
    self.currentDist = getDistanceMetresBetweenLocationCoordinates( middleLeftCoord, middleRightCoord);
    
}

//new logic is:
//2 states switching: grey - heading...
//if move map -> center

-(IBAction)fnTracking_CurrentPos:(id)sender
{
    if([self locationCheckStatusDenied])
    {
        [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
        return;
    }
    
    //check current index
    switch (self.indexTracking) {
            
        case 0://=> Current location.
        {
            self.indexTracking = 1; //=> switch to heading mode.
            [self doFnTracking:YES];
        }
            break;

        case 1: ////=> enable heading mode
        {
            self.indexTracking = 0;
            [self doFnTracking:NO];
        }
        default:
            break;
    }
}

-(IBAction)fnCenter:(id)sender
{
    [self doFnTracking:NO];
}

-(void) doFnTracking: (BOOL)isHeading
{
    self.isShowMylocation = TRUE;

    if (isHeading)
    {
        //Heading is from My location
        [locationManager startUpdatingHeading];
        
        _isHeading = YES;
        
        switch (self.expectTarget) {
            case ISCARTE:
            {
                [self.btnFollowHeading setImage: [UIImage imageNamed:@"ic_map_gps_compass"] forState:UIControlStateNormal];
                self.btnFollowHeading.hidden = NO;

            }
                break;
            case ISLOUNGE:
            {
                if(self.isLiveHunt)
                {
                    [self.btnFollowHeading setImage: [UIImage imageNamed:@"ic_live_gps_compass"] forState:UIControlStateNormal];
                    self.btnFollowHeading.hidden = NO;
                    //                        vTmpMap.btnRecenter.hidden = YES;
                    //                        vTmpMap.imgRecenter.hidden = YES;
                }
                else
                {
                    [self.btnFollowHeading setImage: [UIImage imageNamed:@"chasse_ic_direction"] forState:UIControlStateNormal];
                    
                }
            }
                break;
            case ISGROUP:
            {
                [self.btnFollowHeading setImage: [UIImage imageNamed:@"group_ic_direction"] forState:UIControlStateNormal];
            }
                break;
            default:
                break;
        }

    }
    else //my location
    {
//        [locationManager stopUpdatingHeading];
        _isHeading = NO;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:vTmpMap.mapView_.camera.zoom]];
        
        
        switch (self.expectTarget) {
            case ISCARTE:
            case ISLOUNGE:
            case ISGROUP:
            {
                
                self.btnFollowHeading.hidden = NO;
                self.btnRecenter.hidden = YES;
                self.imgRecenter.hidden = YES;
                
                [self.btnFollowHeading setImage: [UIImage imageNamed:@"ic_live_gps_normal"] forState:UIControlStateNormal];
            }
                break;
            default:
                break;
        }
    }

}

- (void)locationManager:(CLLocationManager *)manager  didUpdateHeading:(CLHeading *)newHeading
{
//   headingValue newHeading.trueHeading
    
    headingValue = newHeading.trueHeading; //in degrees

    if (_isHeading) {
        [vTmpMap.mapView_ animateToBearing:headingValue];
    }
    
    CGAffineTransform t = CGAffineTransformMakeRotation(degreesToRadians(headingValue));

    [UIView animateWithDuration:0.5 animations:^{
        vTmpMap.icCompass.transform = t;
        self.icCompass.transform = t;
    }];    
    

//    CLLocationDirection trueNorth = [newHeading trueHeading];
//    bearing = trueNorth;
//
//
//    vTmpMap.mapView_.camera.
}

//mapView.camera.heading = newHeading.magneticHeading
//mapView.setCamera(mapView.camera, animated: true)

@end
