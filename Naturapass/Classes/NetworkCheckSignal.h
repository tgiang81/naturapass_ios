//
//  NetworkCheckSignal.h
//  Naturapass
//
//  Created by Giang on 10/20/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^callBackSignal)(NSString*);

@interface NetworkCheckSignal : NSObject <NSURLConnectionDataDelegate>
{
    CLGeocoder *ceo;
    CLLocation *loc;

}
@property (nonatomic, strong) NSURLConnection *connection; // we'll use presence or existence of this connection to determine if download is done
@property (nonatomic) NSUInteger length;                   // the numbers of bytes downloaded from the server thus far
@property (nonatomic, strong) NSDate *startTime;           // when did the download start
@property (nonatomic,copy) callBackSignal myCallBack;


+ (NetworkCheckSignal *) sharedInstance;

-(void) getAddressFromCoordinate :(callBackSignal) cb withData:(NSDictionary*) data;

@end
