//
//  ChassesCreateHeaderCell.m
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateSwitchCell.h"

@implementation ChassesCreateSwitchCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.swFilter.transform = CGAffineTransformMakeScale(0.75, 0.75);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
