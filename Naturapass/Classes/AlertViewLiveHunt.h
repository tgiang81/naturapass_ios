//
//  UploadProgress.h
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
#import "Define.h"
#import "AlertVC.h"
typedef void (^AlertViewLiveHuntCallBack) (NSInteger index, NSDictionary *dic);

@interface AlertViewLiveHunt : AlertVC
@property(nonatomic,strong) NSString       *strID;
@property (nonatomic, copy) AlertViewLiveHuntCallBack callbackLive;
- (instancetype)initListLiveHunt;
@end
