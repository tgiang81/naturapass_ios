//
//  MurAgendaCell.m
//  Naturapass
//
//  Created by manh on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MurAgendaCell.h"
@implementation MurAgendaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.vContraint.layer setMasksToBounds:YES];
    self.vContraint.layer.cornerRadius= 4.0;
    self.vContraint.layer.borderWidth =0.5;
    self.vContraint.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _lbTextDebut.text = [NSString stringWithFormat:@"%@ :",@"Début le"];
    _lbTextFin.text = [NSString stringWithFormat:@"%@ :",@"Fin le"];
    _lbTextLocation.text = [NSString stringWithFormat:@"%@ :",@"Rendez-vous à"];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)fnSetColorWithExpectTarget:(ISSCREEN)target withIsLiveHunt:(BOOL)liveHunt
{
    _expectTarget = target;
    
    if (_expectTarget == ISLOUNGE) {
        _constraintHeightLocation.priority = UILayoutPriorityDefaultLow;
        _vLocation.hidden = NO;
        _vTitle.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        _btnAccess.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        if (liveHunt) {
            _vAvatar.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            _imgAvatar.image = [UIImage imageNamed:@"icon_mur_live"];
        }
        else
        {
            _vAvatar.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            _imgAvatar.image = [UIImage imageNamed:@"icon_mur_agenda"];
        }
    }
    else
    {
        _vAvatar.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        _imgAvatar.image = [UIImage imageNamed:@"icon_mur_group"];
        _vTitle.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        _btnAccess.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        _constraintHeightLocation.constant = 0;
        _constraintHeightLocation.priority = UILayoutPriorityRequired;
        _vLocation.hidden = YES;
    }

}
@end
