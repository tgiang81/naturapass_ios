//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

typedef void (^AllerMURGPSCallback)(NSInteger index);

@interface AllerMURGPS : UIView
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
    NSDictionary *myDic;
}
@property (nonatomic,copy) AllerMURGPSCallback callback;

-(void)doBlock:(AllerMURGPSCallback ) cb;
-(IBAction)closeAction:(id)sender;
-(void)showAlert;
-(instancetype)initWithData:(NSDictionary*) data;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDescriptions;
@property (nonatomic,strong) IBOutlet UIButton *cancelButton;
@property (nonatomic,strong) IBOutlet UIButton *otherButton;

@end
