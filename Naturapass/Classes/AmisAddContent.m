//
//  AmisAddContent.m
//  Naturapass
//
//  Created by Giang on 9/10/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisAddContent.h"
#import "AmisFelicitations.h"

#import "SAMTextView.h"
#import "WebServiceAPI.h"
#import "Amis_Demand_Invitation.h"
#import "AmisAddScreen1.h"
#import "AmisSearchVC.h"

@interface AmisAddContent ()
{
    IBOutlet SAMTextView *txtView;
    UITapGestureRecognizer *tapGesture;
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescriptionScreen;
    __weak IBOutlet UIButton *btnSuivant;
}
@end

@implementation AmisAddContent

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strINVITEZ_DES_AMIS);
    lblDescriptionScreen.text = str(strRedigez_un_message_personnalise);
    [btnSuivant setTitle:str(strSUIVANT) forState:UIControlStateNormal];

    txtView.placeholder = str(strTapez_votre_texte_personnalise_ici);
    
    [self InitializeKeyboardToolBar];
    [txtView setInputAccessoryView:self.keyboardToolbar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)fnSend:(id)sender
{
    NSString *trimmedString = [txtView.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    
    if (trimmedString && ![trimmedString isEqualToString:@""]) {
        NSDictionary *postDict =[NSDictionary new];
        
        
        NSString * strEmails = [self.listEmails componentsJoinedByString:@";"];
        postDict = @{@"emails" : strEmails,
                     
                     @"body": txtView.text};
        
        [COMMON addLoading:self];
        if ([COMMON isReachableCheck]) {
            
            WebServiceAPI *serviceAPI =[WebServiceAPI new];
            [serviceAPI postInvitionEmailWithDictionary:postDict];
            serviceAPI.onComplete =^(NSDictionary *response, int errCode)
            {
                [COMMON removeProgressLoading];
                if (!response) {
                    return;
                }
                //Success
                AmisFelicitations *viewController1 = [[AmisFelicitations alloc] initWithNibName:@"AmisFelicitations" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:NO];
                
                
            };
        } else {
            [COMMON removeProgressLoading];
        }
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
    [txtView addGestureRecognizer:tapGesture];
}
- (void)textViewDidEndEditing:(UITextView *)textView

{
    [self.view removeGestureRecognizer:tapGesture];
    [txtView removeGestureRecognizer:tapGesture];

}
- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture {
    [txtView resignFirstResponder];
}
- (void)resignKeyboard:(id)sender
{
    [txtView resignFirstResponder];
    
}
@end
