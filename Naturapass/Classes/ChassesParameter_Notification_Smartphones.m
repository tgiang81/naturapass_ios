//
//  MurParameter_Notification_Smartphones.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesParameter_Notification_Smartphones.h"
#import "CMSwitchView.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChassesParameter_Notification_Smartphones () <CMSwitchViewDelegate>
{
    NSArray * arrData;
    
}

@end

@implementation ChassesParameter_Notification_Smartphones

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableDictionary *dic1 = [@{@"name": str(strConfirmation_d_amitie),
                                   @"image":@"ic_publication_notification_setting"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strDemande_d_amitie),
                                   @"image":@"ic_publication_notification_setting"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strReponse_commentaire_sur_une_publication),
                                   @"image":@"ic_publication_notification_setting"} copy];
    
    NSMutableDictionary *dic4 = [@{@"name":str(strNouveau_aime_sur_une_publication),
                                   @"image":@"ic_publication_notification_setting"} copy];
    
    arrData =  [@[dic1,dic2,dic3,dic4] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    //
    cell.constraint_image_width.constant = 20;
    cell.constraint_image_height.constant = 20;
    //FONT
    cell.label1.text = dic[@"name"];
    cell.label1.numberOfLines=2;
    cell.constraint_control_width.constant = 60;
    cell.constraintRight.constant = 5;
    cell.constraint_control_height.constant = 30;
    CMSwitchView *btnFilterControl = [[CMSwitchView alloc] initWithFrame:CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant)] ;
    btnFilterControl.delegate = self;
    
    
    //set default status
    btnFilterControl.animDuration = 0.1f;
    btnFilterControl.rounded = NO;
    btnFilterControl.borderColor = [UIColor redColor];
    btnFilterControl.dotColor = [UIColor redColor];
    btnFilterControl.color = [UIColor whiteColor];
    
    [cell.viewControl addSubview:btnFilterControl];
    
    //Status
    [btnFilterControl fnThemeWithColorON:UIColorFromRGB(ON_SWITCH_CHASSES) ColorOFF:UIColorFromRGB(OFF_SWITCH_CHASSES)];
    [btnFilterControl onSwitch];
    [btnFilterControl yesSelect];
    //
    
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)switchValueChanged:(id)sender andNewValue:(BOOL)value
{
    
}


@end
