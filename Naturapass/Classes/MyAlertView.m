//
//  MyAlertView.m
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MyAlertView.h"

#import "Define.h"
#import "PublicationControlCheckboxCell.h"
#import "AppCommon.h"
static NSString *identifier = @"CheckboxCellID";
static NSString *tableCell = @"PublicationControlCheckboxCell";

@interface MyAlertView ()
{
    NSMutableArray *arrMultiple;
    NSDictionary *dicImage;
}
@end

@implementation MyAlertView

-(void)initParams:(NSString*)stitle arrContent:(NSArray*)arrContent arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC parent:(UIViewController*)pr
{
    strTitle =stitle;
    arrData = arrContent;
    listCheck = arrCheck;
    typeControl =typeC;
    
    _lbTitle.text =strTitle;
    //mld
    switch (typeControl) {
        case CONTROL_CHECKBOX:
        {
            dicImage= @{@"non_check":@"check",@"check":@"check_active"};
        }
            break;
        case CONTROL_RADIO:
        {
            dicImage= @{@"non_check":@"radio_inactive",@"check":@"radio_active"};
        }
            break;
        default:
            break;
    }
    arrMultiple= [NSMutableArray arrayWithArray:arrData];
    for (int i =0; i <listCheck.count; i++) {
        for (int j=0; j< arrMultiple.count; j++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[j]];
            if ([listCheck[i] intValue]== [dic[@"id"] intValue]) {
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:j withObject:dic];
                break;
            }
            
        }
    }
    int iCount =0;
    int min = 2;
    int max = 5;
    if (arrMultiple.count<min) {
        iCount= min;
    }
    else if (arrMultiple.count>max)
    {
        iCount =max;
    }
    else
    {
        iCount = (int)arrMultiple.count;
    }
    self.constraintHeightViewBorder.constant = iCount*40+50;
}

-(void)awakeFromNib{
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];
    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    [popTable registerNib:[UINib nibWithNibName:tableCell bundle:nil] forCellReuseIdentifier:identifier];
    
}

-(void) doShow :(UIViewController*)parent
{
    [parent.view addSubview:self];
    
    CGRect r = parent.view.frame;
    
    self.frame = CGRectMake(0, 0, r.size.width, r.size.height);

    [popTable reloadData];
}

#pragma callback
-(void)setCallback:(MyAlertViewCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(MyAlertViewCbk ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showInVC:(UIViewController*)vc
{
    
//    self.view.frame = vc.view.frame;
//    
//    [vc.view addSubview:self.view];
//    [vc.view bringSubviewToFront:self.view];
//    [self.view setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    
    //    [vc presentViewController:self animated:NO completion:^{
    //
    //    }];
}


-(IBAction)ANNULERAction:(id)sender
{
    [self removeFromSuperview];

}

-(IBAction)VALIDAction:(id)sender
{
    NSMutableArray *arrValue = [NSMutableArray new];
    NSMutableArray *arrText = [NSMutableArray new];
    
    for (NSDictionary *dic in arrMultiple) {
        if ([dic[@"isSelect"] boolValue]== YES) {
            [arrValue addObject:dic[@"id"]];
            [arrText addObject:dic[@"name"]];
        }
    }
    //    [self dismissViewControllerAnimated:YES completion:^{}];
    [self removeFromSuperview];
    
    if (_callback) {
        _callback(@{@"value":arrValue,@"text":arrText});
    }
}


#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrMultiple count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicationControlCheckboxCell *cell = nil;
    
    cell = (PublicationControlCheckboxCell *)[popTable dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = arrMultiple[indexPath.row];
    cell.lbTittle.text = dic[@"name"];
    if ([dic[@"isSelect"] boolValue]== YES) {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:dicImage[@"check"]] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:dicImage[@"non_check"]] forState:UIControlStateNormal];
    }
    cell.btnCheckBox.tag = indexPath.row +200;
    [cell.btnCheckBox addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self fnSelectItem:indexPath.row];
}

- (IBAction)selectAction:(id)sender
{
    int index = (int)([sender tag] -200);
    [self fnSelectItem:index];
}

-(void)fnSelectItem:(int)index
{
    
    switch (typeControl) {
        case CONTROL_CHECKBOX:
        {
            if (index<arrMultiple.count) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [popTable reloadData];
            }
        }
            break;
        case CONTROL_RADIO:
        {
            if (index<arrMultiple.count) {
                arrMultiple= [NSMutableArray arrayWithArray:arrData];
                
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [popTable reloadData];
            }
        }
            break;
        default:
            break;
    }
}
@end
