//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertSearchMapVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"

static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";
static int  kHeightCell = 40;
static int  kMaxNumberCell = 5;

@implementation AlertSearchMapVC
{
    CLLocation *my_Placemark;
    NSString   *my_addressString;
}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSearchMapVC" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        self.tableControl.hidden=YES;
        self.tableControl.alpha = 0.75;
        
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        [COMMON listSubviewsOfView:self];
        
        self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
        my_Placemark= [CLLocation new];
        [_txtSearch becomeFirstResponder];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertSearchMapCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSearchMapCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
}
-(IBAction)validerAction:(id)sender
{
    [self removeFromSuperview];
    if (my_addressString.length>0) {
        if (_callback) {
            _callback(my_Placemark,my_addressString);
        }
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
}

//MARK:- searBar

-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    [self processLoungesSearchingURL:searchText.text];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *strsearchValues=self.txtSearch.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.txtSearch.text=strsearchValues;
    [self handleSearchForSearchString:self.txtSearch.text];
    [self.txtSearch resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self clearSearchResults];
    [self checkHeightTable];
    self.tableControl.hidden=NO;
    [self.tableControl reloadData];
    
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=self.txtSearch.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.txtSearch.text=strsearchValues;
    
    [self handleSearchForSearchString:self.txtSearch.text];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self processLoungesSearchingURL:searchText];
}
//MARK: UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""]) {
        [self clearSearchResults];
        [self checkHeightTable];
        
    } else {
        [[HNKGooglePlacesAutocompleteQuery sharedQuery] resetAPIKEY:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
        
        [self.searchQuery fetchPlacesForSearchQuery:searchString
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                                 }];
                                                 
                                             } else {
                                                 self.searchResults = places;
                                                 [self checkHeightTable];
                                                 [self.tableControl reloadData];
                                             }
                                         }];
    }
}
#pragma mark Search Helpers

- (void)clearSearchResults
{
    self.searchResults = @[];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}
-(void)checkHeightTable
{
    if (self.searchResults.count< kMaxNumberCell) {
        self.constraintHeight.constant = self.searchResults.count*kHeightCell;
    }
    else
    {
        self.constraintHeight.constant = kMaxNumberCell*kHeightCell;
    }
}
#pragma mark - Helpers

- (HNKGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.searchResults.count <indexPath.row)
        return nil;
    
    return self.searchResults[indexPath.row];
}
#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kHeightCell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kHNKDemoSearchResultsCellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.textLabel.text = [self placeAtIndexPath:indexPath].name;
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    my_addressString = nil;
    HNKGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    [CLPlacemark hnk_placemarkFromGooglePlace:place
                                       apiKey:self.searchQuery.apiKey
                                   completion:^(CLLocation *m_Placemark, NSString *addressString, NSError *error) {
                                       if (error) {
                                           [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                           }];
                                           
                                       } else if (m_Placemark) {
                                           if (self.searchResults.count > indexPath.row) {
                                               my_Placemark = m_Placemark;
                                               _txtSearch.text = addressString;
                                               my_addressString= addressString;
                                               [self.txtSearch resignFirstResponder];
                                               
                                               [self.tableControl deselectRowAtIndexPath:indexPath animated:NO];
                                               self.tableControl.hidden=YES;
                                           }
                                       }
                                   }];
}



@end
