//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLKMenuPopover.h"
typedef void (^callBackGroup) (NSInteger);
@interface AddFriendCell : UITableViewCell<MLKMenuPopoverDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@property (weak, nonatomic) IBOutlet UIView *viewControl;
@property (weak, nonatomic) IBOutlet UIImageView *imageCrown;
@property (weak, nonatomic) IBOutlet UIImageView *imageLine;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidth;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_height;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_control_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_control_height;
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property (nonatomic, copy) callBackGroup CallBackGroup;
@property (nonatomic,retain) IBOutlet UIButton       *btnAddFriend;
@property (nonatomic,retain) IBOutlet UIImageView       *imgAddFriend;
-(void) createMenuList:(NSArray*)arr;

-(void) show:(UIView*)view;
-(void) show:(UIView*)view offset:(CGRect)offset;
@end
