//
//  MyAlertView.m
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "VoisChiens_Document_WebView_Alert.h"

#import "Config.h"
#import "AppCommon.h"
@interface VoisChiens_Document_WebView_Alert ()
{
    NSString *strUrl;
}
@end

@implementation VoisChiens_Document_WebView_Alert

-(instancetype)initWithURL:(NSString*)url
{
    self =[super initWithNibName:@"VoisChiens_Document_WebView_Alert" bundle:nil];
    if (self) {
        strUrl =url;
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];

    self.view.backgroundColor = [UIColor clearColor];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    wvWeb.delegate = self;
    wvWeb.scalesPageToFit = YES;
    
    NSURL *url = [NSURL fileURLWithPath:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [wvWeb loadRequest:request];

}
-(IBAction)nonAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [COMMON addLoadingForView:wvWeb];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [COMMON removeProgressLoading];

}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [COMMON removeProgressLoading];

}

@end
