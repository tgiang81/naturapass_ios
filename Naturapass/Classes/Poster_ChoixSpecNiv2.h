//
//  Poster_ChoixSpecNiv2.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PosterBaseVC.h"


//RECURSIVE
@interface Poster_ChoixSpecNiv2 : PosterBaseVC

@property (nonatomic, strong) NSDictionary *myDic;
@property(nonatomic,strong) NSString * myName;

@end
