//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step2.h"
#import "ChassesCreate_Step3.h"
#import "GroupCreateCell_Step2.h"


#import "Define.h"

static NSString *GroupCreateCellStep2 =@"GroupCreateCell_Step2";
static NSString *GroupesCreateCellID =@"GroupesCreateCellID";

@interface ChassesCreate_Step2 ()<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *_postDict;
    NSDictionary *_attachment;
    NSMutableArray *_arrAccess;
    __weak IBOutlet UILabel *lblTitleScreen;
}
@property (nonatomic, strong) GroupCreateCell_Step2 *prototypeCell;

@end

@implementation ChassesCreate_Step2

- (GroupCreateCell_Step2 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:GroupesCreateCellID];
    }
    
    return _prototypeCell;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strTYPEDACCES);
    [self initialization];
    // Do any additional setup after loading the view from its nib.
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:GroupCreateCellStep2 bundle:nil] forCellReuseIdentifier:GroupesCreateCellID];
    
    _arrAccess = [NSMutableArray arrayWithObjects:str(strMessage11),
                  str(strMessage12),
                  str(strMessage13),
                  nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDataPostDict:(NSDictionary*)postdict Attachment:(NSDictionary*)attachment
{
    _postDict = postdict;
    _attachment =attachment;
    
}
#pragma mark - TableView datasource & delegate
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupCreateCell_Step2 *cell = (GroupCreateCell_Step2 *)cellTmp;
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch (indexPath.row) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"ic_private_chasse"];
            cell.lblTitle.text =str(strPRIVATE);
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"ic_private_chasse"];
            cell.lblTitle.text = str(strSEMIPRIVATE);
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"ic_public_chasse"];
            cell.lblTitle.text = str(strPUBLIC);

        }
            
            break;
        default:
            break;
    }
    cell.lblDescription.text =_arrAccess[indexPath.row];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAccess.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupCreateCell_Step2 *cell = (GroupCreateCell_Step2 *)[tableView dequeueReusableCellWithIdentifier:GroupesCreateCellID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    [self configureCell:self.prototypeCell cellForRowAtIndexPath:indexPath];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableControl deselectRowAtIndexPath:indexPath animated:NO];
    
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch (indexPath.row) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
        }
            break;
        default:
            break;
    }
    
    [ChassesCreateOBJ sharedInstance].accessKind =m_accessKind;
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [[ChassesCreateOBJ sharedInstance] modifiChassesWithVC:self];
    }
    else
    {
        ChassesCreate_Step3 *Step3 = [[ChassesCreate_Step3 alloc]initWithNibName:@"ChassesCreate_Step3" bundle:nil];
        [self pushVC:Step3 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
}

@end
