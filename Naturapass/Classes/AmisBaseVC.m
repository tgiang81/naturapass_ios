//
//  AmisBaseVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisBaseVC.h"
#import "NotificationVC.h"
#import "AmisListVC.h"
#import "Amis_Demand_Invitation.h"
#import "AmisAddScreen1.h"
#import "AmisSearchVC.h"
#import "CommonObj.h"
@interface AmisBaseVC ()

@end

@implementation AmisBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    if ([self isKindOfClass:[AmisListVC class]] ||
        [self isKindOfClass:[Amis_Demand_Invitation class]] ||
        [self isKindOfClass:[AmisSearchVC class]] ||
        [self isKindOfClass:[AmisAddScreen1 class]]
        ) {
        self.showTabBottom = YES;
    }
    
    //SUB
    [self addSubNav:@"SubNavigationAMIS"];
    
    
    //Confort
    UIButton *btnCheck = (UIButton*)[self.subview viewWithTag: 2*( START_SUB_NAV_TAG + 3)];
    [self.subview badgingBtn:btnCheck count:[CommonObj sharedInstance].nbUserWaiting];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getUserWaitingAction];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        NSArray *arr = response[@"users"];
        [CommonObj sharedInstance].nbUserWaiting = (int)arr.count;
        [self.subview badgingBtn:btnCheck count:[CommonObj sharedInstance].nbUserWaiting];
    };

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strAMIS)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
    

    //Change status bar color
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(AMIS_TINY_BAR_COLOR) ];
    [self setThemeNavSub:NO withDicOption:nil];

    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];

}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];

    [super onSubNavClick:btn];

    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[ self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                        [self.navigationController popToViewController:controller animated:NO];
                        return;
                    }
            }
            [self gotoback];
        }
            break;
            
        case 1://List Amis
        {
            if ([self isKindOfClass: [AmisListVC class]])
                return;
            
            [self doPushVC:[AmisListVC class] iAmParent:NO];
        }
            break;
            
            
        case 2://Add new Amis
        {
            if ([self isKindOfClass: [AmisAddScreen1 class]])
                return;
            
            [self doPushVC:[AmisAddScreen1 class] iAmParent:NO];
        }
            break;
            
        case 3://Demand invitation amis
        {
            if ([self isKindOfClass: [Amis_Demand_Invitation class]])
                return;
            
            [self doPushVC:[Amis_Demand_Invitation class] iAmParent:NO];
        }
            
            break;
        case 4://Search Amis
        {
            if ([self isKindOfClass: [AmisSearchVC class]])
                return;
            
            [self doPushVC:[AmisSearchVC class] iAmParent:NO];
        }
            break;
        default:
            break;
    }
}



@end
