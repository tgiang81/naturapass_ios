//
//  Signaler_ChoixSpecNiv1.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "SignalerBaseVC.h"

@interface Signaler_ChoixSpecNiv1 : SignalerBaseVC
@property (nonatomic,strong) IBOutlet UIView *viewBottom;
@property (nonatomic,strong) IBOutlet UIImageView *imgFavorisBg;

@end
