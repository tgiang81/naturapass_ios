//
//  ChassesCreateV2.h
//  Naturapass
//

#import "NewPublicationBaseVC.h"
#import "Define.h"

@interface PublicationNew_Signalement : NewPublicationBaseVC

@property (nonatomic, strong) NSString *myName;

@property (nonatomic, assign) BOOL isHideFavorite;
@property (nonatomic, assign) BOOL isHideSentinelle;

@property (nonatomic, assign) NEW_PUBSCREEN_TYPE iType;
@property (nonatomic, strong) NSMutableArray *arrData;

@property (nonatomic, strong) IBOutlet UIView *vBottom;
@property (nonatomic, strong) IBOutlet UIView *vBottom1;
@property (nonatomic, strong) IBOutlet UIImageView *imgFavorisBG;
@property (nonatomic, strong) IBOutlet UIImageView *imgSentinelleBG;

-(void) fnHideFavorite;

@end
