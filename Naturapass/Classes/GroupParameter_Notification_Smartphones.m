//
//  MurParameter_Notification_Smartphones.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupParameter_Notification_Smartphones.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface GroupParameter_Notification_Smartphones ()
{
    NSMutableArray * arrData;
    IBOutlet UILabel *lbTitle;
}

@end

@implementation GroupParameter_Notification_Smartphones

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strNOTIFICATIONS_SMARTPHONE);
    arrData = [NSMutableArray new];
    NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnGET_GROUPS_PARAMETERS_MOBILE:strGroup_id mykind:(self.expectTarget == ISGROUP)?MYGROUP:MYCHASSE];
    
    [COMMON addLoading:self];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        
        if([response[@"group_parameters"] isKindOfClass: [NSArray class]])
        {
            
            NSArray *arr = response[@"group_parameters"];
            
            for (NSDictionary*dic in arr) {
                [arrData addObject:@{@"name": dic[@"label"],
                                     @"image":@"mur_st_ic_message",
                                     @"status": dic[@"wanted"] ,
                                     @"type": dic[@"id"],
                                     }];
            }
            
            [self.tableControl reloadData];
            
        }
        //        "emails": [
        //                   {
        //                       "id": 1,
        //                       "label": "Commentaire sur votre publication",
        //                       "wanted": 1
        //                   },
    };
    
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind7" bundle:nil] forCellReuseIdentifier:identifierSection1];
    self.tableControl.estimatedRowHeight = 66;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellKind7 *cell = nil;
    
    cell = (CellKind7 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];
    
    
    
    
    cell.swControl.tag = indexPath.row + 100;
    //Status
    cell.swControl.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [cell.swControl setOnTintColor:UIColorFromRGB(ON_SWITCH_PARAM)];
    [cell.swControl setOn:[dic[@"status"] boolValue]];
    [cell.swControl addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
- (void)switchValueChanged:(id)sender
{
    NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    UISwitch *sv = (UISwitch*)sender;
    int index = (int)sv.tag - 100;
    NSDictionary *dic = [arrData[index] mutableCopy];
    //1 10 3 4 11 6 7 5 8 9
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (response) {
            if ([response[@"success"] boolValue]) {
                //                ASLog(@"%@",response);
                //change data
                [dic setValue: @(!([dic[@"status"] boolValue])) forKey: @"status" ];
                [arrData replaceObjectAtIndex:index withObject:dic];
                
            }
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:index inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.tableControl reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    };
    
    [serverAPI putGroupSmartphoneNotificationSetting:@{@"option": dic[@"type"],
                                                    @"value": [NSNumber numberWithBool:sv.isOn],
                                                    @"group_id": strGroup_id
                                                    }];
}
@end
