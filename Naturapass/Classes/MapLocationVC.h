//
//  MapLocationVC.h
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "BaseMapVC.h"

@interface MapLocationVC : BaseMapVC
{
}

@property (nonatomic,assign) BOOL isSpecial;

@property (nonatomic,retain) NSDictionary *simpleDic;

@end
