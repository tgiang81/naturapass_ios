//
//  MDTitleLayersDownloader.h
//  Naturapass
//
//  Created by Manh on 9/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MDTitleLayersDownloader : NSObject
@property (nonatomic, readonly) dispatch_queue_t myQueue;
+ (MDTitleLayersDownloader *) sharedInstance;
-(void)cacheMapForNorthEast:(CLLocationCoordinate2D)northEast withSouthWest:(CLLocationCoordinate2D)southWest withZoom:(int)zoom;
@end
