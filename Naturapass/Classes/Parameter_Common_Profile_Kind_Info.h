//
//  Parameter_VoisChiens_Info.h
//  Naturapass
//
//  Created by Manh on 3/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ParameterBaseVC.h"
@interface Parameter_Common_Profile_Kind_Info : ParameterBaseVC
@property (nonatomic, strong) NSDictionary *dicInfo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthImage;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBirthDay;

@end
