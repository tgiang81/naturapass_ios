//
//  MediaCell.h
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"
#import "CellBase.h"
#import "Define.h"
#import "SOLabel.h"
#import "ImageViewAction.h"
#import "MDHTMLLabel.h"
typedef void(^onClickText)(UIButton*theBtn);
typedef void(^onClickObs)(UIButton*theBtn);

@interface MediaCell : CellBase <OHAttributedLabelDelegate,MDHTMLLabelDelegate>
@property (weak, nonatomic) IBOutlet UIButton *btnOpenFile;
@property (weak, nonatomic) IBOutlet UIView *viewFileOpen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewFileOpen;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintGeoHeight;


@property (weak, nonatomic) IBOutlet UIImageView *ongNhom;
@property (nonatomic, copy) onClickText cbClickText;
@property (nonatomic, copy) onClickObs cbClickObs;

@property (weak, nonatomic) IBOutlet UIView *tempView;

@property (weak, nonatomic) IBOutlet UIImageView *imgLocation;

@property (weak, nonatomic) IBOutlet UIImageView *imgSetting;

@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;

@property (weak, nonatomic) IBOutlet UIButton *btnProfile;

@property (weak, nonatomic) IBOutlet SOLabel *shareType;
@property (weak, nonatomic) IBOutlet SOLabel *observation;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightDoubleImages;

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *btnTitle;

@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIView *viewLocation;
@property (weak, nonatomic) IBOutlet UIView *viewRedirect;

@property (weak, nonatomic) IBOutlet UIButton *location;
@property (weak, nonatomic) IBOutlet UILabel *locationTxt;

@property (nonatomic,retain) IBOutlet MDHTMLLabel        *myContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightObservationIcon;

@property (weak, nonatomic) IBOutlet UILabel *obs_label_static;

@property (weak, nonatomic) IBOutlet UILabel *obs_tree_content;

@property (weak, nonatomic) IBOutlet UIView *imageLeftIndice;


@property (weak, nonatomic) IBOutlet UIButton *btnContent;
//View Single image
@property (weak, nonatomic) IBOutlet ImageViewAction *imageContent;

//View couple
@property (weak, nonatomic) IBOutlet ImageViewAction *image1;
@property (weak, nonatomic) IBOutlet ImageViewAction *image2;

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgIconShare;

//abc
@property (weak, nonatomic) IBOutlet UIView *viewArrowComment;
//cba

@property (weak, nonatomic) IBOutlet UILabel *numLike;
@property (weak, nonatomic) IBOutlet UILabel *numComment;
@property (weak, nonatomic) IBOutlet UIImageView *imageLike;
@property (weak, nonatomic) IBOutlet UIButton *btnLike;
@property (weak, nonatomic) IBOutlet UIButton *btnComment;
@property (weak, nonatomic) IBOutlet UIButton *btnTextComment;
@property (weak, nonatomic) IBOutlet UILabel *lbComment;
@property (weak, nonatomic) IBOutlet UIButton *btnLikesList;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightImageContent;


@property (weak, nonatomic) IBOutlet UIButton *btnIconLike;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightImage;
@property (nonatomic,strong) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnObs;
@property (weak, nonatomic) IBOutlet UILabel *lbUserLikePub;
@property (weak, nonatomic) IBOutlet UIButton *btnShareDetail;
@property (weak, nonatomic) IBOutlet UIButton *btnAller;

@property int iWhoIamI;
@property BOOL isMurView;

-(void)assignValues:(NSDictionary *)publicationDic;
-(void)setThemeWithFromScreen:(ISSCREEN)isScreen;
@end
