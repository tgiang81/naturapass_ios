//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"

@interface TypeCell51: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;
//label
@property (nonatomic,retain) IBOutlet UILabel           *label1;

-(void)fnSettingCell:(int)type;
@end
