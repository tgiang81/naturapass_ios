//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
typedef void (^AlertShareDrawShapeVCCallback)(NSString *type,NSDictionary *dic);

@interface AlertShareDrawShapeVC : UIView<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *subAlertView;
}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;

@property (nonatomic,copy) AlertShareDrawShapeVCCallback callback;
@property (nonatomic,strong) IBOutlet UIButton *validerButton;

-(void)doBlock:(AlertShareDrawShapeVCCallback ) cb;
-(IBAction)validerAction:(id)sender;
-(void)showAlertWithDic:(NSDictionary*)dicInput;
-(instancetype)init;
@end
