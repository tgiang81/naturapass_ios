//
//  ChassesCreateMapCell.m
//  Naturapass
//
//  Created by JoJo on 8/13/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateMapCell.h"
#import "AlertVC.h"
#import "Define.h"
@implementation ChassesCreateMapCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.mapView_.settings.scrollGestures = YES;
    self.mapView_.settings.zoomGestures = YES;
    self.mapView_.myLocationEnabled = YES;
    [self.mapView_ setIndoorEnabled:NO];

    [self.mapView_.layer setMasksToBounds:YES];
    self.mapView_.layer.cornerRadius= 4;
    self.mapView_.layer.borderWidth =0;

    [vContour.layer setMasksToBounds:YES];
    vContour.layer.cornerRadius= 4;
    vContour.layer.borderWidth =0;
    
    [self.btnCloseMap.layer setMasksToBounds:YES];
    self.btnCloseMap.layer.cornerRadius= 15;
    
    [self.btnCurrentLocation.layer setMasksToBounds:YES];
    self.btnCurrentLocation.layer.cornerRadius= 15;
    //ggtt
    //    [vTmpMap.mapView_ setMinZoom:2 maxZoom:17];
    
    //IGN test
    self.mapView_.settings.compassButton = YES;
    //    mapView_.camera = camera;
    
    self.mapView_.settings.allowScrollGesturesDuringRotateOrZoom = NO;
    
//    self.mapView_.delegate = self;
        self.mapView_.mapType = kGMSTypeNormal;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
