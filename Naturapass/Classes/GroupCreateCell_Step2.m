//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GroupCreateCell_Step2.h"
#import "CommonHelper.h"

@implementation GroupCreateCell_Step2
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [[CommonHelper sharedInstance] setRoundedView:self.imgViewAvatar toDiameter:self.imgViewAvatar.frame.size.height /2];

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.lblDescription.preferredMaxLayoutWidth = CGRectGetWidth(self.lblDescription.frame);
}
@end
