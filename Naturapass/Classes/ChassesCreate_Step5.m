//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step5.h"
#import "ChassesCreate_Step6.h"
#import "TypeCell31.h"
//
#import "ChassesCreate_Step51.h"
#import "ChassesCreate_Step52.h"
#import "ChassesCreate_Step53.h"
#import "ChassesCreate_Step54.h"

#import "GroupCreate_Step5.h"
#import "GroupCreate_Step6.h"
#import "GroupCreate_Step8.h"

#import "ChassesCreate_Step11.h"

#import "Etape9_Agenda_Groupe.h"
#import "Chasse_Invite_bis_supVC.h"


//
static NSString *typecell31 =@"TypeCell31";
static NSString *typecell31ID =@"TypeCell31ID";

@interface ChassesCreate_Step5 ()
{
    NSMutableArray *arr;
    NSArray        *arrImg;
    NSMutableArray *arrAmis;
    
}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;

@end

@implementation ChassesCreate_Step5

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.subview removeFromSuperview];
    [self addSubNav:nil];

    [self initialization];
    // Do any additional setup after loading the view from its nib.
    
}
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell31 bundle:nil] forCellReuseIdentifier:typecell31ID];
    
    arr = [NSMutableArray arrayWithObjects: str(strMes_amis),
           str(strMesGroupes),
           str(strAutres_Natiz),
           str(strNon_Natiz),
           nil];
    arrImg =@[strIC_chasse_amis,strIC_chasse_group,strIC_chasse_invite_member,strIC_chasse_non_member];
    _label1.text =str(strINVITERDESPARTICPANTS);
    _label2.text =str(strVousPouvezInviter);
    arrAmis =[NSMutableArray new];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI getTheFriends:YES withID:nil];
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (response!=nil) {
            /*
             friends =     (
             {
             courtesy = 1;
             firstname = Burdeyron;
             fullname = "Burdeyron Patrick";
             id = 16;
             lastname = Patrick;
             parameters =             {
             friend = 0;
             };
             photo = "/uploads/users/images/thumb/33cb1ae096b8edc787ae6a141eb39d39ebaed189.jpeg";
             relation =             {
             friendship =                 {
             state = 2;
             way = 2;
             };
             mutualFriends = 6;
             };
             usertag = "burdeyron-patrick";
             }*/
            if ([response isKindOfClass: [NSDictionary class]]) {
                arrAmis = [NSMutableArray arrayWithArray:response[@"friends"]];
                [self checkInvited];
                NSString*str = [NSString stringWithFormat: @"Mes amis (%d)", (int) arrAmis.count];
                [arr replaceObjectAtIndex:0 withObject:str];
                [self.tableControl reloadData];
                
            }
        }
        
    };
    
    //Load mes group
    WebServiceAPI *serviceObjNumGroup = [[WebServiceAPI alloc]init];
    [serviceObjNumGroup getUserGroupsAction];
    //get groups that user joined
    serviceObjNumGroup.onComplete = ^(NSDictionary*response, int errCode){
        
        if (response !=nil) {
            
            NSArray *arrGroupes = response[@"groups"];
            if (arrGroupes.count > 0) {
                
            }
            
            NSString*str = [NSString stringWithFormat: @"%@ (%d)",str(strMesGroupes), (int) arrGroupes.count];
            
            [arr replaceObjectAtIndex:1 withObject:str];
            [self.tableControl reloadData];
        }
    };
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}
-(void)checkInvited
{
    for (int i=0; i<arrAmis.count; i++) {
        NSDictionary *dic=arrAmis[i];
        for (int j =0; j<self.arrSubcriber.count; j++) {
            NSDictionary *dic1 =self.arrSubcriber[j];
            if ((dic[@"id"] ==dic1[@"user"][@"id"]) && [dic1[@"access"] integerValue]>=USER_NORMAL) {
                [arrAmis removeObjectAtIndex:i];
            }
        }
    }
}
#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeCell31 *cell = (TypeCell31 *)[tableView dequeueReusableCellWithIdentifier:typecell31ID];
    cell.img1.image=  [UIImage imageNamed:arrImg[indexPath.row]];
    cell.label1.text =arr[indexPath.row];
    cell.button1.tag =indexPath.row;
    [cell.button1 addTarget:self action:@selector(choosesInvite:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(IBAction)choosesInvite:(id)sender
{
    NSInteger index =[sender tag];
    switch (index) {
        case 0:
        {
            //Mes amis
            ChassesCreate_Step51 *viewController1 = [[ChassesCreate_Step51 alloc] initWithNibName:@"ChassesCreate_Step51" bundle:nil];
            viewController1.arrSubcriber =self.arrSubcriber;
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

            
        }
            break;
        case 1:
        {
            //Mes groupes
            ChassesCreate_Step52 *viewController1 = [[ChassesCreate_Step52 alloc] initWithNibName:@"ChassesCreate_Step52" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

        }
            break;
        case 2:
        {
            //Autres Natiz
            ChassesCreate_Step53 *viewController1 = [[ChassesCreate_Step53 alloc] initWithNibName:@"ChassesCreate_Step53" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

        }
            break;
        case 3:
        {
            //Non-Natiz
            Chasse_Invite_bis_supVC *viewController1 = [[Chasse_Invite_bis_supVC alloc] initWithNibName:@"Chasse_Invite_bis_supVC" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

        }
            break;
        default:
            break;
    }
}

- (IBAction)onNext:(id)sender {
    
    //Etape9-Import-Groupe
    //If join group from HUNT available API -> use this code, delete the above:
    if (self.fromSetting) {
        [self gotoback];
    }
    else
    {
        if ([ChassesCreateOBJ sharedInstance].isModifi) {
            [self backEditListChasse];
        }
        else
        {
        Etape9_Agenda_Groupe *viewController1 = [[Etape9_Agenda_Groupe alloc] initWithNibName:@"Etape9_Agenda_Groupe" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        }
    }
    
}

@end
