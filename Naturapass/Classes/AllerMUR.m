//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AllerMUR.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "ASSharedTimeFormatter.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation AllerMUR
{
    __weak IBOutlet UIImageView *imgProfile;
    __weak IBOutlet UILabel *fullName;
    __weak IBOutlet UILabel *dateCreate;
    __weak IBOutlet UILabel *addressName;
    __weak IBOutlet UILabel *latitude;
    __weak IBOutlet UILabel *longitude;
    __weak IBOutlet UILabel *altitude;
    
}

-(instancetype)initWithData:(NSDictionary*) data
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AllerMUR" owner:self options:nil] objectAtIndex:0] ;

    if (self) {

        myDic = data;
        
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;

        [COMMON listSubviewsOfView:self];
        
        [self updateData];
        
    }
    return self;
}

-(void) updateData{
    //full name
    NSString *strUserName=[NSString stringWithFormat:@"%@ %@", CHECKSTRING(myDic[@"owner"][@"firstname"]), CHECKSTRING(myDic[@"owner"][@"lastname"]) ];
    [fullName setText:strUserName];
    
    //image profile
    
    NSString *strImage=@"";
    if (myDic[@"owner"][@"profilepicture"] != nil) {
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,myDic[@"owner"][@"profilepicture"]];
    }else{
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,myDic[@"owner"][@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    [imgProfile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];

    
    
    //date create
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] timeFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy H:mm" forFormatter: outputFormatterBis];
    
    NSString *relativeTime = [myDic valueForKey:@"created"];
    NSDate * inputDate = [ inputFormatter dateFromString:relativeTime ];
    NSString * outputString = [ outputFormatterBis stringFromDate:inputDate ];
    
    if (outputString) {
        NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];
        [dateCreate setText: strDateLocation];
    }else{
        [dateCreate setText: @""];
    }
    
    //address + lat + long
    if ([myDic[@"geolocation"] isKindOfClass: [NSDictionary class] ]) {
        [latitude setText: [ NSString stringWithFormat:@"Lat. %.5f" , [myDic[@"geolocation"] [@"latitude"] floatValue] ] ];
        
        [longitude setText: [ NSString stringWithFormat:@"Lng. %.5f" , [myDic[@"geolocation"] [@"longitude"] floatValue]] ];
        
        if ( ![myDic[@"geolocation"] [@"altitude"] isKindOfClass: [NSNull class]]) {
            if ([myDic[@"geolocation"] [@"altitude"] doubleValue] > 0) {
                [altitude setText: [ NSString stringWithFormat:@"Alt. %.5f" , [myDic[@"geolocation"] [@"altitude"] floatValue ]] ];
            }else{
                [altitude setText:@""];
            }
            
        }else{
            [altitude setText:@""];
            
        }
        
        
        if (myDic[@"geolocation"][@"address"] != nil) {
            [addressName setText:myDic[@"geolocation"][@"address"] ];
            
        }else{
            [addressName setText:@""];
        }
    }
}

-(void)awakeFromNib{
    [super awakeFromNib];

}

#pragma callback
-(void)setCallback:(AllerMURCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AllerMURCallback ) cb
{
    self.callback = cb;
    
}

#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
}

-(IBAction)closeAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0);
    }
}



-(IBAction)fnChooseNaturapass:(id)sender
{
    [self removeFromSuperview];
    
    if (_callback) {
        _callback(1);
    }
}

-(IBAction)fnchooseGPSRoute:(id)sender
{
    [self removeFromSuperview];
    
    if (_callback) {
        _callback(2);
    }
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(void)fnColorWithExpectTarget:(ISSCREEN)expectTarget
{
    _expectTarget = expectTarget;
    UIColor *color ;
    if (_expectTarget ==ISLOUNGE) {
        color =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    }
    else if (_expectTarget ==ISGROUP) {
        color =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    }
    else if (_expectTarget ==ISLIVEMAP) {
        color =UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
    }
    else
    {
        color =UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    }
    fullName.textColor = color;
    addressName.textColor = color;
    self.voirNatura.backgroundColor = color;
//    self.voirGPS.backgroundColor = color;

}
@end
