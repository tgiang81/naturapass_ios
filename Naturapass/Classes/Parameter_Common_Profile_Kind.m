//
//  ParameterVC.m
//  Naturapass
//
//  Created by Giang on 11/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Parameter_Common_Profile_Kind.h"

#import "Parameter_Common_Profile_Kind_Change.h"
#import "CellKind23.h"
#import "VoisChiens_Document_Modifi_Alert.h"
#import "VoisChiens_Document_Valider_Alert.h"
#import "Parameter_Common_Profile_Kind_Info.h"
#import "Parameter_Profile_Papers_Change.h"
#import "Parameter_Parpers_Profile_Info.h"
#import "CommonHelper.h"

static NSString *identifierSection1 = @"TableDog";

@interface Parameter_Common_Profile_Kind ()
{
    NSMutableArray *arrCommom;
    IBOutlet UILabel *lbNameScreen;
    BOOL check_busy;
}
@end

@implementation Parameter_Common_Profile_Kind

- (void)viewDidLoad {
    [super viewDidLoad];
    arrCommom = [NSMutableArray new];

    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    //Refresh/loadmore
    [self initRefreshControl];
    [self fnSetValueUI];
    
    //cache to view offline.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startRefreshControl];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fnSetValueUI
{
    
    NSString *strKind = @"";
    NSString *strProfileFile = @"";

    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            strKind =str(strVOS_CHIENS);
            strProfileFile = FILE_PROFILE_MULTI_DOGS;

        }
            break;
        case TYPE_WEAPONS:
        {
            strKind =str(strVOS_ARMES);
            strProfileFile = FILE_PROFILE_MULTI_WEAPONS;

        }
            break;
        case TYPE_PAPER:
        {
            strKind =str(strVOS_PAPIERS);
            strProfileFile = FILE_PROFILE_MULTI_PAPERS;

        }
            break;
        default:
            break;
    }
    
    //offline...view target
    if (![COMMON isReachable]) {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],strProfileFile)   ];
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        if (arrTmp.count > 0) {
            
            [arrCommom removeAllObjects];
            
            [arrCommom addObjectsFromArray:arrTmp];
            [self.tableControl reloadData];
        }
    }
    
    lbNameScreen.text = strKind;
}
- (void)insertRowAtTop {
    [self getListProfileKindWithIsMore:NO];
}
- (void)insertRowAtBottom {
    [self getListProfileKindWithIsMore:YES];
}
#pragma mark - API
-(void)getListProfileKindWithIsMore:(BOOL)isMore
{
    //neu dang request thi khong get nua
    if (check_busy) {
        [self stopRefreshControl];
        return;
    }
    check_busy = YES;
    NSString *strKind = @"";
    
    NSString *strProfileFile = @"";
    
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            strKind =PROFILE_MULTI_DOGS;
            strProfileFile = FILE_PROFILE_MULTI_DOGS;
            
        }
            break;
        case TYPE_WEAPONS:
        {
            strKind =PROFILE_MULTI_WEAPONS;
            strProfileFile = FILE_PROFILE_MULTI_WEAPONS;

        }
            break;
        case TYPE_PAPER:
        {
            strKind =PROFILE_MULTI_PAPERS;
            strProfileFile = FILE_PROFILE_MULTI_PAPERS;

        }
            break;
        default:
            break;
    }
    
    NSString *offset =@"0";
    if (isMore) {
        offset = [NSString stringWithFormat:@"%ld",arrCommom.count];
    }
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getProfileAllWithKind:strKind withLimit:@"20" withOffset:offset];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        check_busy = NO;
        
        [self performSelector:@selector(stopRefreshControl) withObject:nil afterDelay:0.75];

        if ([response isKindOfClass: [NSDictionary class]]) {
            if (!isMore) {
                [arrCommom removeAllObjects];
            }
            if (response[strKind]) {
                [arrCommom addObjectsFromArray:response[strKind]];
                //Cache
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],strProfileFile)   ];
                [arrCommom writeToFile:strPath atomically:YES];
                
                
                [self.tableControl reloadData];
            }
            
        }
    };
  
}

-(void)fnDeleteVoisChiesWithIndex:(int)indexData
{
    NSDictionary *dic = arrCommom[indexData];

    NSString *strKind = @"";
    NSString *strTitleAlert = @"";
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            strKind =PROFILE_ONE_DOG;
            strTitleAlert= [NSString stringWithFormat:@"%@ \"%@\"?",str(strVouloir_supprimer_ce_chien),dic[@"name"]];
        }
            break;
        case TYPE_WEAPONS:
        {
            strKind =PROFILE_ONE_WEAPON;
            strTitleAlert= [NSString stringWithFormat:@"%@ \"%@\"?",str(strVouloir_supprimer_cette_arme),dic[@"name"]];
        }
            break;
        case TYPE_PAPER:
        {
            strKind =PROFILE_ONE_PAPER;
            strTitleAlert= [NSString stringWithFormat:@"%@ \"%@\"?",str(strVouloir_supprimer_les_informations_liees_papier),dic[@"name"]];
        }
            break;
        default:
            break;
    }
    
    VoisChiens_Document_Valider_Alert *ttest = [[[NSBundle mainBundle] loadNibNamed:@"VoisChiens_Document_Valider_Alert" owner:self options:nil] objectAtIndex:0] ;
    
    [ttest setTitle:strTitleAlert];
    [ttest doBlock:^(NSInteger index) {
        switch (index) {
                //@"oui"
            case 0:
            {
                /*
                medias =     (
                              {
                                  id = 111;
                                  path = "/uploads/dogs/images/resize/43c797925667f9437b7a0a0878bfb565f8d31fd0.jpeg";
                                  type = 100;
                              },
                              {
                                  id = 112;
                                  path = "/uploads/dogs/images/resize/75288a611a069da5ba7afb425bf847b9f7ce52fa.jpeg";
                                  type = 100;
                              }
                              );
                
                */
                [COMMON addLoading:self];
                WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                [serviceObj deleteProfileWithKind:strKind kindID:dic[@"id"]];
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    [COMMON removeProgressLoading];
                    if (response[@"success"]) {
                        
                        
                        //remove file.
                        
                        if ([dic[@"medias"] isKindOfClass: [NSArray class]]){
                        
                            for (NSDictionary *mDic in dic[@"medias"]) {
                        
                                NSString *strImage = mDic[@"path"];
                                
                                if ([mDic[@"type"] intValue] == 100) {
                                //photo
                                    strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
                                    
                                    NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
                                    //upload successful...rename local file.
                                    NSString *destPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
                                    [FileHelper removeFileAtPath:destPath];

                                }else if ([mDic[@"type"] intValue] == 102) {
                                    //PDF
                                    strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
                                    
                                    strImage=[strImage stringByReplacingOccurrencesOfString:@"jpeg" withString:@"pdf"];
                                    
                                    NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
                                    
                                    //SelectedFiles
                                    //write kFileData to file. PDF
                                    
                                    
                                    NSString *destPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
                                    [FileHelper removeFileAtPath:destPath];

                                }

                            }
                        }
                            
                        [arrCommom removeObjectAtIndex:indexData];
                        [self.tableControl reloadData];
                    }
                };
            }
                break;
                //@"non"
            case 1:
            {
                
            }
                break;
            default:
                break;
        }
    }];
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];

}
#pragma mark - action

-(IBAction)fnAddVoisChiens:(id)sender
{
    switch (self.myTypeVview) {
        case TYPE_DOG:
        case TYPE_WEAPONS:
        {
            Parameter_Common_Profile_Kind_Change *viewController1 = [[Parameter_Common_Profile_Kind_Change alloc] initWithNibName:@"Parameter_Common_Profile_Kind_Change" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            [self pushVC:viewController1 animate:YES];
            
        }
            break;
        case TYPE_PAPER:
        {
            Parameter_Profile_Papers_Change *viewController1 = [[Parameter_Profile_Papers_Change alloc] initWithNibName:@"Parameter_Profile_Papers_Change" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            [self pushVC:viewController1 animate:YES];
        }
            break;
        default:
            break;
    }
    
}
-(void)editAction:(int)indexData
{
    switch (self.myTypeVview) {
        case TYPE_DOG:
        case TYPE_WEAPONS:
        {
            NSDictionary *dic = arrCommom[indexData];
            
            Parameter_Common_Profile_Kind_Change *viewController1 = [[Parameter_Common_Profile_Kind_Change alloc] initWithNibName:@"Parameter_Common_Profile_Kind_Change" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            viewController1.isModifi =YES;
            viewController1.dicModifi = dic;
            [self pushVC:viewController1 animate:YES];
            
        }
            break;
        case TYPE_PAPER:
        {
            NSDictionary *dic = arrCommom[indexData];
            
            Parameter_Profile_Papers_Change *viewController1 = [[Parameter_Profile_Papers_Change alloc] initWithNibName:@"Parameter_Profile_Papers_Change" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            viewController1.isModifi =YES;
            viewController1.dicModifi = dic;
            [self pushVC:viewController1 animate:YES];
        }
            break;
        default:
            break;
    }

}
-(void)fnGetInfo:(int)indexData
{
    switch (self.myTypeVview) {
        case TYPE_DOG:
        case TYPE_WEAPONS:
        {
            
            NSDictionary *dic = arrCommom[indexData];
            
            Parameter_Common_Profile_Kind_Info *viewController1 = [[Parameter_Common_Profile_Kind_Info alloc] initWithNibName:@"Parameter_Common_Profile_Kind_Info" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            viewController1.dicInfo = dic;
            [self pushVC:viewController1 animate:YES];
            
        }
            break;
        case TYPE_PAPER:
        {
            
            NSDictionary *dic = arrCommom[indexData];
            
            Parameter_Parpers_Profile_Info *viewController1 = [[Parameter_Parpers_Profile_Info alloc] initWithNibName:@"Parameter_Parpers_Profile_Info" bundle:nil];
            viewController1.myTypeVview = self.myTypeVview;
            viewController1.dicInfo = dic;
            [self pushVC:viewController1 animate:YES];
            
        }
            break;
        default:
            break;
    }
    
}
-(void)modifiAction:(int)indexData
{
    VoisChiens_Document_Modifi_Alert *ttest = [[[NSBundle mainBundle] loadNibNamed:@"VoisChiens_Document_Modifi_Alert" owner:self options:nil] objectAtIndex:0] ;
    if (self.myTypeVview == TYPE_PAPER) {
        NSDictionary *dic = arrCommom[indexData];
        NSArray *arrTitle;
        if ([dic[@"deletable"] boolValue] == YES) {
            arrTitle =@[@{@"icon":@"param_icon_eye",@"name":str(strVoir)},
                                 @{@"icon":@"param_ic_modifi",@"name":str(strModifier)},
                                 @{@"icon":@"param_icon_delete",@"name":str(strSupprimer)}];
        }
        else
        {
           arrTitle =@[@{@"icon":@"param_icon_eye",@"name":str(strVoir)},
                                 @{@"icon":@"param_ic_modifi",@"name":str(strModifier)}];
        }
        [ttest fnSetTitleWithArray:arrTitle];

    }
    [ttest doBlock:^(NSInteger index) {
        switch (index) {
                //@"Voir"
            case 0:
            {
                [self fnGetInfo:indexData];
            }
                break;
                //@"Modifier"
            case 1:
            {
                [self editAction:indexData];
            }
                break;
                //@"Supprimer"
            case 2:
            {
                [self fnDeleteVoisChiesWithIndex:indexData];
            }
                break;
            default:
                break;
        }
    }];
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrCommom.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind23 *cell = nil;
    
    cell = (CellKind23 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrCommom[indexPath.row];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.rightIcon.image =[ UIImage imageNamed:@"param_ic_3dot"];
    cell.constraintRightIconWidth.constant = 3;
    cell.constraintRightIconHeight.constant = 11;
    [cell.btnDel removeFromSuperview];
    
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self modifiAction:(int)indexPath.row];
}
@end
