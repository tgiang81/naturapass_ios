//
//  CellKind14.h
//  Naturapass
//
//  Created by Giang on 10/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDCheckBox.h"
#import "AppCommon.h"

@interface CellKind14 : UITableViewCell

@property (nonatomic,retain)IBOutlet MDCheckBox   *checkbox;

@property (nonatomic,retain)IBOutlet UILabel    *slideTitleLabel;

@property (nonatomic,retain)IBOutlet UIImageView *imgArrow;

@property (nonatomic,retain)IBOutlet UIButton * btnOver;

@end
