
//
//  Publication_Favoris_System.m
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Publication_Favoris_System.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "TreeViewNode.h"
#import "PublicationFavorisSystemCell.h"
#import "PublicationOBJ.h"
#import "OHAttributedLabel.h"
#import "FavorisSystemBaseCell.h"
#import "PublicationFavorisSystemColorCell.h"
#import "Publication_Ajout_Favoris.h"

static NSString *favorisCell = @"FavorisCell";
static NSString *favorisColorCell = @"FavorisColorCell";

@interface Publication_Favoris_System ()
{
    NSMutableArray *nodes;
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnSuivant;

}

@property (nonatomic, strong) PublicationFavorisSystemCell *prototypeCell;

@end

@implementation Publication_Favoris_System

- (PublicationFavorisSystemCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:favorisCell];
    }
    return _prototypeCell;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strSelectionnez_les_informations);
    [btnSuivant setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    [self.tableControl registerNib:[UINib nibWithNibName:@"PublicationFavorisSystemCell" bundle:nil] forCellReuseIdentifier:favorisCell];
    [self.tableControl registerNib:[UINib nibWithNibName:@"PublicationFavorisSystemColorCell" bundle:nil] forCellReuseIdentifier:favorisColorCell];
    nodes =[NSMutableArray new];
    NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
    //legend
    if (dicFavo[@"legend"]) {
        TreeViewNode *treeLegend = [[TreeViewNode alloc]init];
        treeLegend.nodeLevel = 0;
        treeLegend.nodeObject = dicFavo[@"legend"];
        treeLegend.nodeTitle = str(strLegende);
        treeLegend.isExpanded = NO;
        treeLegend.typeCell = FAV_LEGENDE;
        treeLegend.isEditItem =YES;
        treeLegend.statusNode =IS_CHECK;
        [nodes addObject:treeLegend];
    }
    if (dicFavo[@"color"]) {
        //publication color
        TreeViewNode *treeColor = [[TreeViewNode alloc]init];
        treeColor.nodeLevel = 0;
        treeColor.nodeObject = dicFavo[@"color"][@"color"];
        treeColor.nodeTitle = str(strCouleur);
        treeColor.isExpanded = NO;
        treeColor.typeCell = FAV_COULEUR;
        treeColor.isEditItem =YES;
        treeColor.statusNode =IS_CHECK;
        [nodes addObject:treeColor];
    }
    if (dicFavo[@"category_tree"]) {
        //publication precistions
        TreeViewNode *treePrecistions = [[TreeViewNode alloc]init];
        treePrecistions.nodeLevel = 0;
        treePrecistions.nodeObject = dicFavo[@"category_tree"];
        treePrecistions.nodeTitle = str(strPrecisions);
        treePrecistions.isExpanded = NO;
        treePrecistions.typeCell = FAV_PRECISIONS;
        treePrecistions.isEditItem =NO;
        treePrecistions.statusNode =IS_CHECK;
        [nodes addObject:treePrecistions];
    }
    
    if (dicFavo[@"attachments"]) {
        //observation
        TreeViewNode *treeOb = [[TreeViewNode alloc]init];
        treeOb.nodeLevel = 0;
        treeOb.nodeObject = nil;
        treeOb.nodeTitle = str(strFiche);
        treeOb.isExpanded = YES;
        treeOb.typeCell = FAV_FICHE;
        treeOb.nodeChildren =[NSMutableArray new];
        treeOb.isEditItem =NO;
        treeOb.statusNode =IS_CHECK;
        
        //observation child
        NSArray *arrAttactment =dicFavo[@"attachments"];
        for (int i =0 ; i<arrAttactment.count; i++) {
            NSDictionary *dicAtt = arrAttactment[i];
            TreeViewNode *treeAtt = [[TreeViewNode alloc]init];
            treeAtt.nodeLevel = 1;
            treeAtt.nodeObject = dicAtt[@"text"];
            treeAtt.nodeTitle = dicAtt[@"name"];
            treeAtt.nodeItemAttachment = dicAtt;
            treeAtt.isExpanded = NO;
            treeAtt.typeCell = FAV_FICHE_CHILD;
            treeAtt.isEditItem =YES;
            treeAtt.parent = treeOb;
            treeAtt.statusNode =IS_CHECK;
            [treeOb.nodeChildren addObject:treeAtt];
            
        }
        [nodes addObject:treeOb];
    }
    NSString *strpartage =@"";
    switch ([dicFavo[@"iSharing"] intValue]) {
        case 0:
        {
            strpartage =str(strMoi);
        }
            break;
        case 1:
        {
            strpartage =str(strAAmis);
        }
            break;
        case 3:
        {
            strpartage =str(strTous_les_natiz);
        }
            break;
        default:
            break;
    }

//    NSArray *arrGroup =dicFavo[@"sharingGroupsArray"];//"1,2,3"
//    if (arrGroup.count>0) {
//        NSMutableArray *arrGroupName =  [NSMutableArray new];
//        NSString*strGroupName = nil;
//        for (NSDictionary*dicGroup in arrGroup) {
//            [arrGroupName addObject:dicGroup[@"categoryName"]];
//        }
//        strGroupName = [arrGroupName componentsJoinedByString:@", "];
//        
//        strpartage = [NSString stringWithFormat:@"%@\nGroup: %@",strpartage,strGroupName];
//    }
//    
//    NSArray *arrHunt =dicFavo[@"sharingHuntsArray"];
//    if (arrHunt.count>0) {
//        NSMutableArray *arrChassesName =  [NSMutableArray new];
//        NSString*strChassesName = nil;
//        for (NSDictionary*dicHunt in arrHunt) {
//            [arrChassesName addObject:dicHunt[@"categoryName"]];
//        }
//        strChassesName = [arrChassesName componentsJoinedByString:@", "];
//        
//        strpartage = [NSString stringWithFormat:@"%@\nChasse: %@",strpartage,strChassesName];
//    }
    if (strpartage.length>0) {
        //publication precistions
        TreeViewNode *treePartage = [[TreeViewNode alloc]init];
        treePartage.nodeLevel = 0;
        treePartage.nodeObject = strpartage;
        treePartage.nodeTitle = str(strPartage);
        treePartage.isExpanded = NO;
        treePartage.typeCell = FAV_PARTAGE;
        treePartage.isEditItem =YES;
        treePartage.statusNode =IS_CHECK;
        [nodes addObject:treePartage];
    }
    // group
    NSArray *arrGroup =dicFavo[@"sharingGroupsArray"];//"1,2,3"
    if (arrGroup.count>0) {
        //groups
        TreeViewNode *treeGroups = [[TreeViewNode alloc]init];
        treeGroups.nodeLevel = 0;
        treeGroups.nodeObject = nil;
        treeGroups.nodeTitle = str(strGroupes);
        treeGroups.isExpanded = YES;
        treeGroups.typeCell = FAV_GROUPS;
        treeGroups.nodeChildren =[NSMutableArray new];
        treeGroups.isEditItem =NO;
        treeGroups.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrGroup.count; i++) {
            NSDictionary *dicGroup = arrGroup[i];
            TreeViewNode *treeGroupChild = [[TreeViewNode alloc]init];
            treeGroupChild.nodeLevel = 1;
            treeGroupChild.nodeObject = dicGroup[@"categoryName"];
            treeGroupChild.nodeItemAttachment = dicGroup;
            treeGroupChild.isExpanded = NO;
            treeGroupChild.typeCell = FAV_GROUPS_CHILD;
            treeGroupChild.isEditItem =YES;
            treeGroupChild.parent = treeGroups;
            treeGroupChild.statusNode =IS_CHECK;
            
            [treeGroups.nodeChildren addObject:treeGroupChild];
            
        }
        [nodes addObject:treeGroups];
    }
    // hunts
    NSArray *arrHunt =dicFavo[@"sharingHuntsArray"];
    if (arrHunt.count>0) {
        //groups
        TreeViewNode *treeHunts = [[TreeViewNode alloc]init];
        treeHunts.nodeLevel = 0;
        treeHunts.nodeObject = nil;
        treeHunts.nodeTitle = str(strAAgenda);
        treeHunts.isExpanded = YES;
        treeHunts.typeCell = FAV_HUNTS;
        treeHunts.nodeChildren =[NSMutableArray new];
        treeHunts.isEditItem =NO;
        treeHunts.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrHunt.count; i++) {
            NSDictionary *dicHunt = arrHunt[i];
            TreeViewNode *treeHuntChild = [[TreeViewNode alloc]init];
            treeHuntChild.nodeLevel = 1;
            treeHuntChild.nodeObject = dicHunt[@"categoryName"];
            treeHuntChild.nodeItemAttachment = dicHunt;
            treeHuntChild.isExpanded = NO;
            treeHuntChild.typeCell = FAV_HUNTS_CHILD;
            treeHuntChild.isEditItem =YES;
            treeHuntChild.parent = treeHunts;
            treeHuntChild.statusNode =IS_CHECK;
            
            [treeHunts.nodeChildren addObject:treeHuntChild];
            
        }
        [nodes addObject:treeHunts];
    }
    // hunts
    NSArray *arrUsers =dicFavo[@"sharingUsers"];
    if (arrUsers.count>0) {
        //groups
        TreeViewNode *treeUser = [[TreeViewNode alloc]init];
        treeUser.nodeLevel = 0;
        treeUser.nodeObject = nil;
        treeUser.nodeTitle = str(@"Personnes");
        treeUser.isExpanded = YES;
        treeUser.typeCell = FAV_USERS;
        treeUser.nodeChildren =[NSMutableArray new];
        treeUser.isEditItem =NO;
        treeUser.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrUsers.count; i++) {
            NSDictionary *dicUsers = arrUsers[i];
            TreeViewNode *treeUsersChild = [[TreeViewNode alloc]init];
            treeUsersChild.nodeLevel = 1;
            treeUsersChild.nodeObject = dicUsers[@"name"];
            treeUsersChild.nodeItemAttachment = dicUsers;
            treeUsersChild.isExpanded = NO;
            treeUsersChild.typeCell = FAV_USERS_CHILD;
            treeUsersChild.isEditItem =YES;
            treeUsersChild.parent = treeUser;
            treeUsersChild.statusNode =IS_CHECK;
            
            [treeUser.nodeChildren addObject:treeUsersChild];
            
        }
        [nodes addObject:treeUser];
    }
    [self fillDisplayArray];
    [self.tableControl reloadData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}
#pragma mark - Messages to fill the tree nodes and the display array

- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    parent.statusNode = statusNode;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        node.statusNode = statusNode;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    if (!parent) {
        return;
    }
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        if (treenode.statusNode == IS_CHECK) {
            countCheck++;
        }
        if (treenode.statusNode == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        parent.statusNode = IS_CHECK;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        parent.statusNode = UN_CHECK;
    }
    else
    {
        parent.statusNode = NON_CHECK;
        
    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }
    
}
//This function is used to fill the array that is actually displayed on the table view
- (void)fillDisplayArray
{
    self.displayArray = [[NSMutableArray alloc]init];
    for (TreeViewNode *node in nodes) {
        [self.displayArray addObject:node];
        //        [self caculatorWithContent:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
            
            int index  =(int)self.displayArray.count;
            if(index>0)
            {
                TreeViewNode *nodeLeave = self.displayArray[index-1];
                nodeLeave.lineBottom =YES;
                [self.displayArray replaceObjectAtIndex:index-1 withObject:nodeLeave];
            }
        }
    }
}
//This function is used to add the children of the expanded node to the display array
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [self.displayArray addObject:node];
        //        [self caculatorWithContent:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
        }
    }
}

#pragma mark - Table view data source

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    PublicationFavorisSystemCell *cell = (PublicationFavorisSystemCell*)cellTmp;
    
    if (node.isExpanded) {
        [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"favo_arrow_down"]];
    }
    else {
        [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"favo_arrow_right"]];
    }
    
    switch (node.statusNode) {
        case NON_CHECK:
        {
            [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
            
        }
            break;
        case UN_CHECK:
        {
            [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_inactive"] forState:UIControlStateNormal];
            
        }
            break;
        case IS_CHECK:
        {
            [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_active"] forState:UIControlStateNormal];
            
        }
            break;
        default:
            break;
    }
    cell.treeNode = node;
    //
    
    NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
    UIColor *color = [UIColor blackColor]; // select needed color
    if (node.nodeTitle) {
        NSString *title = (NSString*)node.nodeTitle;
        
        NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
        
        NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
        [mLabel appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@": "]];
        [mLabel addAttribute:NSFontAttributeName
                       value:FONT_HELVETICANEUE_MEDIUM(15)
                       range:NSMakeRange(0, title.length+1)];
        //Add label
        [mAttach appendAttributedString:mLabel];
    }
    if (node.nodeObject) {
        NSString *strValue =@"";
        if (![node.nodeObject isKindOfClass:[NSArray class]]) {
            strValue =node.nodeObject;
            
        }
        else
        {
            strValue= [node.nodeObject componentsJoinedByString:@", "];
            
        }
        //value
        if(node.typeCell == FAV_FICHE)
        {
            color =  UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
        else
        {
            color = [UIColor blackColor];
        }
        
        NSDictionary *attrsVal = @{ NSForegroundColorAttributeName : color };
        
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:strValue attributes:attrsVal];
        
        [mAttach appendAttributedString:mValue];
    }
    //
    if (node.nodeChildren.count>0) {
        cell.imgArrow.hidden = NO;
        cell.selectCellButton.hidden = NO;
        [cell.selectCellButton addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        cell.imgArrow.hidden = YES;
        cell.selectCellButton.hidden = YES;
        [cell.selectCellButton removeTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.selectCellButton.tag = indexPath.row +100;
    
    cell.btnSelect.tag = indexPath.row +200;
    [cell.btnSelect addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //
    if (node.typeCell == FAV_PRECISIONS) {
        cell.btnCheckBox.hidden=YES;
    }
    else
    {
        cell.btnCheckBox.hidden=NO;
    }
    [cell.cellLabel setAttributedText:mAttach];
    
    cell.lineBottom.hidden = NO;
    [cell setNeedsDisplay];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell layoutIfNeeded];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.displayArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.typeCell==FAV_COULEUR) {
        return UITableViewAutomaticDimension;
        
    }else{
        [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
        
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.typeCell==FAV_COULEUR) {
        PublicationFavorisSystemColorCell *cell = (PublicationFavorisSystemColorCell*)[tableView dequeueReusableCellWithIdentifier:favorisColorCell];
        NSString *strColor = node.nodeObject;
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:strColor];
        
        [scanner setScanLocation:0]; // bypass '#' character
        [scanner scanHexInt:&result];
        cell.imgColor.backgroundColor = UIColorFromRGB(result);
        switch (node.statusNode) {
            case NON_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
                
            }
                break;
            case UN_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_inactive"] forState:UIControlStateNormal];
                
            }
                break;
            case IS_CHECK:
            {
                [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_active"] forState:UIControlStateNormal];
                
            }
                break;
            default:
                break;
        }
        cell.treeNode = node;
        //
        
        NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
        UIColor *color = [UIColor blackColor]; // select needed color
        if (node.nodeTitle) {
            NSString *title = (NSString*)node.nodeTitle;
            
            NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
            
            NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
            [mLabel appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@": "]];
            [mLabel addAttribute:NSFontAttributeName
                           value:FONT_HELVETICANEUE_MEDIUM(15)
                           range:NSMakeRange(0, title.length+1)];
            //Add label
            [mAttach appendAttributedString:mLabel];
        }
        cell.btnSelect.tag = indexPath.row +200;
        if (node.isEditItem == YES) {
            cell.btnSelect.hidden = NO;
            [cell.btnSelect addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.btnSelect.hidden = YES;
            [cell.btnSelect removeTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        [cell.cellLabel setAttributedText:mAttach];
        
        cell.imgArrow.hidden = YES;
        cell.selectCellButton.hidden = YES;
        [cell.selectCellButton removeTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lineBottom.hidden = NO;
        [cell setNeedsDisplay];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else
    {
        PublicationFavorisSystemCell *cell = [tableView dequeueReusableCellWithIdentifier:favorisCell forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    }
    
}
- (IBAction)expand:(id)sender
{
    int index = (int)([sender tag] -100);
    TreeViewNode *node = [self.displayArray objectAtIndex:index];
    node.isExpanded = !node.isExpanded;
    [self fillDisplayArray];
    [self.tableControl reloadData];
    
}
- (IBAction)selectAction:(id)sender
{
    int index = (int)([sender tag] -200);
    TreeViewNode *node = [self.displayArray objectAtIndex:index];
    node.isSelected = !node.isSelected;
    int statusNode = node.statusNode;
    switch (node.statusNode) {
        case NON_CHECK:
        {
            statusNode = IS_CHECK;
        }
            break;
        case UN_CHECK:
        {
            statusNode = IS_CHECK;
            
        }
            break;
        case IS_CHECK:
        {
            statusNode = NON_CHECK;
            
        }
            break;
        default:
            break;
    }
    [self checkFromParent:node withStatusNode:statusNode];
    node.statusNode = statusNode;
    [self checkFromChild:node];
    
    [self fillDisplayArray];
    [self.tableControl reloadData];
}
-(IBAction)onNext:(id)sender
{
    NSMutableDictionary *dicFavoTmp = [[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    for (int i=0; i<self.displayArray.count;i++) {
        TreeViewNode *node = [self.displayArray objectAtIndex:i];
        if (node.statusNode== NON_CHECK) {
            switch (node.typeCell) {
                case FAV_LEGENDE:
                {
                    [dicFavoTmp removeObjectForKey:@"legend"];
                }
                    break;
                case FAV_COULEUR:
                {
                    [dicFavoTmp removeObjectForKey:@"color"];
                }
                    break;
                case FAV_FICHE:
                {
                    [dicFavoTmp removeObjectForKey:@"attachments"];
                    if (dicFavoTmp[@"observation"]) {
                        NSMutableDictionary *dicAttach = [dicFavoTmp[@"observation"] mutableCopy];
                        [dicAttach removeObjectForKey:@"attachments"];
                        [dicFavoTmp setObject:dicAttach forKey:@"observation"];
                    }
                }
                    break;
                case FAV_FICHE_CHILD:
                {
                    if ([dicFavoTmp[@"attachments"] isKindOfClass:[NSArray class]]) {
                        NSMutableArray *arrAtt = [dicFavoTmp[@"attachments"] mutableCopy];
                        NSMutableDictionary *dicObs = [dicFavoTmp[@"observation"] mutableCopy];
                        
                        NSMutableArray *arrAtt1 = [dicObs[@"attachments"] mutableCopy];
                        
                        for (int i=0; i<arrAtt.count; i++) {
                            if ([arrAtt[i][@"label"] intValue] == [node.nodeItemAttachment[@"label"] intValue]) {
                                [arrAtt removeObjectAtIndex:i];
                                [arrAtt1 removeObjectAtIndex:i];
                                [dicFavoTmp setObject:arrAtt forKey:@"attachments"];
                                
                                [dicObs setObject:arrAtt forKey:@"attachments"];
                                [dicFavoTmp setObject:dicObs forKey:@"observation"];
                                break;
                            }
                        }
                    }
                }
                    break;
                case FAV_PARTAGE:
                {
                    [dicFavoTmp removeObjectForKey:@"sharing"];
                    [dicFavoTmp removeObjectForKey:@"iSharing"];
//                    [dicFavoTmp removeObjectForKey:@"receivers"];
                }
                    break;
                case FAV_GROUPS:
                {
                    [dicFavoTmp removeObjectForKey:@"sharingGroupsArray"];

                }
                    break;
                case FAV_GROUPS_CHILD:
                {
                    /*
                    <__NSArrayM 0x7fb00982d1e0>(
                    {
                        categoryName = "test 06/02/2015";
                        groupID = 9;
                        isSelected = 1;
                    },
                    {
                        categoryName = Fd2;
                        groupID = 409;
                        isSelected = 1;
                    },
                    {
                        categoryName = "New He";
                        groupID = 412;
                        isSelected = 1;
                    },
                    {
                        categoryName = 3;
                        groupID = 413;
                        isSelected = 1;
                    }
                                                )
                    Printing description of arrHunt:
                    (NSArray *) arrHunt = 0x000000010df86000
                    Printing description of arrHunt:
                    <__NSArrayM 0x7fb00982d210>(
                    {
                        categoryName = "my chasse";
                        huntID = 868;
                        isSelected = 1;
                    },
                    {
                        categoryName = "my chass 2";
                        huntID = 869;
                        isSelected = 1;
                    }
                                                )
                    */
                    NSMutableArray *arrGroup =[dicFavoTmp[@"sharingGroupsArray"] mutableCopy];
                    for (int i =0 ; i<arrGroup.count; i++) {
                        NSDictionary *dicGroup = arrGroup[i];
                        if ([dicGroup[@"groupID"] intValue]== [node.nodeItemAttachment[@"groupID"] intValue]) {
                            [arrGroup removeObjectAtIndex:i];
                            [dicFavoTmp setObject:arrGroup forKey:@"sharingGroupsArray"];
                        }
                    }
                }
                    break;
                case FAV_USERS:
                {
                    [dicFavoTmp removeObjectForKey:@"sharingUsers"];

                }
                    break;
                case FAV_USERS_CHILD:
                {
                    /*
                     sharingUsers =     (
                     {
                     id = 2932;
                     name = "Evance Riviere";
                     status = 1;
                     type = 6;
                     },
                     */
                    NSMutableArray *arrHunt =[dicFavoTmp[@"sharingUsers"] mutableCopy];
                    for (int i =0 ; i<arrHunt.count; i++) {
                        NSDictionary *dicHunt = arrHunt[i];
                        if ([dicHunt[@"id"] intValue]== [node.nodeItemAttachment[@"id"] intValue]) {
                            [arrHunt removeObjectAtIndex:i];
                            [dicFavoTmp setObject:arrHunt forKey:@"sharingUsers"];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    Publication_Ajout_Favoris *viewController1 = [[Publication_Ajout_Favoris alloc] initWithNibName:@"Publication_Ajout_Favoris" bundle:nil];
    viewController1.dicFavo = dicFavoTmp;
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}
@end
