//
//  CarterFilterView.m
//  Naturapass
//
//  Created by Manh on 9/23/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "CarterFilterView.h"
#import "Filter_Cell_Type1.h"
#import "Filter_Cell_Type2.h"
#import "Filter_Cell_Type3.h"
#import "Filter_Cell_DatePersonnaliser.h"
#import "Filter_Cell_Type5.h"
#import "Filter_Cell_SubNode.h"
#import "Filter_Cell_AddMore.h"
#import "Filter_Cell_Description.h"
#import "TreeViewNode.h"
#import "DatabaseManager.h"
#import "NSDate+Extensions.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";
static NSString *identifierSection3 = @"MyTableViewCell3";
static NSString *identifierSection4 = @"MyTableViewCell4";
static NSString *identifierSection5 = @"MyTableViewCell5";
static NSString *identifierSection6 = @"MyTableViewCell6";
static NSString *identifierSection7 = @"MyTableViewCell7";
static NSString *identifierSection8 = @"MyTableViewCell8";
static NSString *identifierSection9 = @"MyTableViewCell9";

@interface CarterFilterView ()<UITableViewDelegate>
{
    NSMutableArray * arrData;
    NSMutableArray * arrDataTmp;
    NSMutableArray              *filterGroupsArray;
    NSMutableArray              *filterHuntsArray;
    NSMutableArray *arrOwnerOfPublications ;
    NSMutableArray *arrID;
    NSArray *arrChooseQUOI;
    BOOL isCheckChildQUOI;
}
@property (nonatomic, retain) TreeViewNode *nodes_QUI;
@property (nonatomic, retain) TreeViewNode *nodes_QUOI;
@property (nonatomic, retain) TreeViewNode *nodes_QUAND;
@property (nonatomic, strong) NSMutableArray *nodes;


@end

@implementation CarterFilterView
#pragma mark - INIT

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0] ;
    if (self) {
        _expectTarget = expectTarget;
        _parentVC = vc;
        _isLiveHunt = isLiveHunt;
        [self instance];
    }
    return self;
}

-(void)instance
{
    filterGroupsArray = [[NSMutableArray alloc]init];
    
    filterHuntsArray = [[NSMutableArray alloc]init];
    arrOwnerOfPublications = [[NSMutableArray alloc]init];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(expandCollapseNode:) name:@"ProjectTreeNodeButtonClicked" object:nil];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(selectedNode:) name:@"ProjectTreeNodeSelected" object:nil];
    //
    arrDataTmp = [NSMutableArray new];
    [self.btnCancel setTitle:str(strAnuler) forState:UIControlStateNormal];
    [self.btnValider setTitle:str(strValider) forState:UIControlStateNormal];
    switch (_expectTarget) {
        case ISMUR:
        {
            self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            [self.vContent setBackgroundColor:UIColorFromRGB(FILTER_VIEW_COLOR)];
            self.lbTitleScreen.textColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            self.lbTitleScreen.text = str(strFilterPublication);
            self.vViewSearch.hidden= NO;
            self.contraintHeaderViewSearch.constant = 60;
        }
            break;
        case ISGROUP:
        {
            self.colorNavigation = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            [self.vContent setBackgroundColor:UIColorFromRGB(FILTER_VIEW_COLOR)];
            self.lbTitleScreen.textColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            self.lbTitleScreen.text = str(strFilterPublication);
            self.vViewSearch.hidden= NO;
            self.contraintHeaderViewSearch.constant = 60;
        }
            break;
        case ISLOUNGE:
        {
            if (_isLiveHunt) {
                self.colorNavigation = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                [self.vContent setBackgroundColor:UIColorFromRGB(FILTER_VIEW_COLOR)];
                self.lbTitleScreen.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                self.lbTitleScreen.text = str(strFilterPublication);
                self.vViewSearch.hidden= NO;
                self.contraintHeaderViewSearch.constant = 60;
            }
            else
            {
                self.colorNavigation = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                [self.vContent setBackgroundColor:UIColorFromRGB(FILTER_VIEW_COLOR)];
                self.lbTitleScreen.textColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                self.lbTitleScreen.text = str(strFilterPublication);
                self.vViewSearch.hidden= NO;
                self.contraintHeaderViewSearch.constant = 60;
                
            }
        }
            break;
        default:
        {
            self.colorNavigation = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            [self.vContent setBackgroundColor:UIColorFromRGB(FILTER_VIEW_COLOR)];
            self.lbTitleScreen.textColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            self.lbTitleScreen.text = str(strFilterPublicationOfCarte);
            self.vViewSearch.hidden= NO;
            self.contraintHeaderViewSearch.constant = 60;
            
        }
            break;
    }
    [self.tfMotCle.layer setMasksToBounds:YES];
    self.tfMotCle.layer.cornerRadius= 4.0;
    self.tfMotCle.layer.borderWidth =0.5;
    self.tfMotCle.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    //
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type1" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection2];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type3" bundle:nil] forCellReuseIdentifier:identifierSection3];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_DatePersonnaliser" bundle:nil] forCellReuseIdentifier:identifierSection4];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type5" bundle:nil] forCellReuseIdentifier:identifierSection5];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_SubNode" bundle:nil] forCellReuseIdentifier:identifierSection6];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_AddMore" bundle:nil] forCellReuseIdentifier:identifierSection7];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Description" bundle:nil] forCellReuseIdentifier:identifierSection8];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection9];
    
    self.tableControl.estimatedRowHeight = 66;
}
-(void)addContraintSupview:(UIView*)viewSuper
{
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    view.frame = viewSuper.frame;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
}
-(void)setCallback:(FilterViewCallback)callback
{
    _callback = callback;
}
#pragma mark - CONFIG

-(IBAction)dismissView:(id)sender
{
    [self hide:YES];
}
-(void)hide:(BOOL)hidden
{
    [self endEditing:YES];
    if (hidden) {
        _contraintLeading.constant = 375;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"HIDE");
             self.hidden = hidden;
         }];
    }
    else
    {
        self.hidden = hidden;
        _contraintLeading.constant = 70;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"SHOW");
             [self addSubViewValiderAdd];
         }];
    }
    
}
-(void)setup
{
    self.hidden = YES;
    _contraintLeading.constant = 375;
}
//MARK: - GENERAL DATA
-(void)fnGetDataWithStringSearch:(NSString*)strSearch
{
    _myC_Search_Text = strSearch;
    [filterGroupsArray removeAllObjects];
    [filterHuntsArray removeAllObjects];
    [self.viewValiderAdd removeFromSuperview];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //list shared mes groups
        
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE (c_admin=1 OR c_allow_show=1) AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmp) {
                if ([kDic[@"id"] intValue ]== strID) {
                    [filterGroupsArray addObject:kDic];
                    break;
                }
            }
            
        }
        
        //list shared mes chasses
        
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],FILTER_MES_HUNT_SAVE) ];
        
        
        NSDate *date = [NSDate date];
        NSTimeInterval time = [date timeIntervalSince1970];
        //list shared mes chasses
        int current_time = (int)time;

        
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_hunt WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ AND c_end_date >= %d ",sender_id, current_time];

        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            
            int strID = [set_querry_hunt intForColumn:@"c_id"];
            //compare to get defaul
            for (NSDictionary*kDic in arrTmpHunt) {
                if ([kDic[@"id"] intValue ]== strID) {
                    [filterHuntsArray addObject:kDic];
                    break;
                }
            }
            
            
        }
        
        //        NSMutableString *strQuerry_Personne = [NSMutableString new];
        //
        //        [strQuerry_Personne setString: [ NSString stringWithFormat:@"SELECT DISTINCT c_owner_id , c_owner_name FROM tb_carte where  c_user_id=%@ ", sender_id]];
        //
        //
        //        FMResultSet *set_querry_Personne = [db  executeQuery:strQuerry_Personne];
        //
        //        while ([set_querry_Personne next])
        //        {
        //            [arrOwnerOfPublications addObject:@{@"id":[set_querry_Personne stringForColumn:@"c_owner_id"],
        //                                                @"name":[set_querry_Personne stringForColumn:@"c_owner_name"]?[set_querry_Personne stringForColumn:@"c_owner_name"]:@""}];
        //        }
        [self fillData];
    }];
    
}
-(void)fillData
{
    NSDictionary   *filterDic;
    switch (self.expectTarget) {
        case ISGROUP:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_GROUP"];
        }
            break;
        case ISLOUNGE:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_LOUNGE"];
            
        }
            break;
        default:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
        }
            break;
    }
    BOOL filterQUI = [filterDic[@"filterQUI"] boolValue];
    BOOL filterQUOI = [filterDic[@"filterQUOI"] boolValue];
    BOOL filterQUAND = [filterDic[@"filterQUAND"] boolValue];
    BOOL iSharingMoi = [filterDic[@"iSharingMoi"] boolValue];
    BOOL iSharingAmis = [filterDic[@"iSharingAmis"] boolValue];
    int  filter_date = [filterDic[@"filter_date"] intValue];
    _myC_Search_Text = filterDic[@"c_search"];
    _tfMotCle.text = _myC_Search_Text;
    NSString *strPath_Personne = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_PERSONNE_SAVE)];
    NSArray *arrPersonne = [NSArray arrayWithContentsOfFile:strPath_Personne];
    
    arrChooseQUOI = filterDic[@"listID"];
    
    _nodes = [ NSMutableArray new];
    //MARK: - object QUI
    NSMutableDictionary *dic1 = [@{@"name": str(strFiltrerSurQUI),
                                   @"type": @(1),
                                   @"status":@(filterQUI),
                                   @"filter_type":@(FILTER_QUI) } copy];
    TreeViewNode *nodeQUI = [TreeViewNode new];
    nodeQUI.nodeObject = dic1;
    /////////////
    NSMutableDictionary *dic2 = [@{@"name":@"Moi",
                                   @"iFilter": @(0),
                                   @"filter_type":@(FILTER_MOI),
                                   @"status":@(iSharingMoi),
                                   @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI2 = [TreeViewNode new];
    nodeChildQUI2.nodeObject = dic2;
    nodeChildQUI2.parent = nodeQUI;
    ////////////
    NSMutableDictionary *dic3 = [@{@"name":@"Mes amis",
                                   @"iFilter": @(1),
                                   @"filter_type":@(FILTER_AMIS),
                                   @"status":@(iSharingAmis),
                                   @"type": @(2)} copy];
    
    TreeViewNode *nodeChildQUI3 = [TreeViewNode new];
    nodeChildQUI3.nodeObject = dic3;
    nodeChildQUI3.parent = nodeQUI;
    /////////////
    TreeViewNode *nodeChildQUI4 = [TreeViewNode new];
    nodeChildQUI4.parent = nodeQUI;
    nodeChildQUI4.nodeChildren = [NSMutableArray new];
    for (NSDictionary *dicPresonne in arrPersonne) {
        // object QUOI
        if ([dicPresonne[@"status"] boolValue]) {
            NSMutableDictionary *dicPP = [@{@"id":dicPresonne[@"id"],
                                            @"name":dicPresonne[@"name"],
                                            @"type": @(6),
                                            @"status":dicPresonne[@"status"]} copy];
            TreeViewNode *nodePP = [TreeViewNode new];
            nodePP.nodeObject = dicPP;
            nodePP.parent = nodeChildQUI4;
            [nodeChildQUI4.nodeChildren addObject:nodePP];
        }
    }
    NSMutableDictionary *dic4 = [@{@"name":@"Personne",
                                   @"type": @(2),
                                   @"filter_type":@(FILTER_PERSION),
                                   @"input_type":@(INPUT_TYPE_FIND),
                                   @"status":@(nodeChildQUI4.nodeChildren.count>0),
                                   @"arrow":@(1)} copy];
    nodeChildQUI4.nodeObject = dic4;
    /////////////
    TreeViewNode *nodeChildQUI5 = [TreeViewNode new];
    nodeChildQUI5.parent = nodeQUI;
    nodeChildQUI5.nodeChildren = [NSMutableArray new];
    for (NSDictionary *dicHunt in filterHuntsArray) {
        if ([dicHunt[@"status"] boolValue]) {
            // object QUOI
            NSMutableDictionary *dicAA = [@{@"id":dicHunt[@"id"],
                                            @"name":dicHunt[@"name"],
                                            @"type": @(6),
                                            @"status":dicHunt[@"status"]} copy];
            TreeViewNode *nodeAA = [TreeViewNode new];
            nodeAA.nodeObject = dicAA;
            nodeAA.parent = nodeChildQUI5;
            [nodeChildQUI5.nodeChildren addObject:nodeAA];
            
        }
    }
    NSMutableDictionary *dic5 = [@{@"name":@"Agenda",
                                   @"type": @(2),
                                   @"filter_type":@(FILTER_AGENDA),
                                   @"input_type":@(INPUT_TYPE_FIND),
                                   @"status":@(nodeChildQUI5.nodeChildren.count>0),
                                   @"arrow":@(1)} copy];
    nodeChildQUI5.nodeObject = dic5;
    /////////////
    TreeViewNode *nodeChildQUI6 = [TreeViewNode new];
    nodeChildQUI6.parent = nodeQUI;
    nodeChildQUI6.nodeChildren = [NSMutableArray new];
    for (NSDictionary *dicGroup in filterGroupsArray) {
        // object QUOI
        if ([dicGroup[@"status"] boolValue]) {
            
            NSMutableDictionary *dicGG = [@{@"id":dicGroup[@"id"],
                                            @"name":dicGroup[@"name"],
                                            @"type": @(6),
                                            @"status":dicGroup[@"status"]} copy];
            TreeViewNode *nodeGG = [TreeViewNode new];
            nodeGG.nodeObject = dicGG;
            nodeGG.parent = nodeChildQUI6;
            [nodeChildQUI6.nodeChildren addObject:nodeGG];
        }
    }
    NSMutableDictionary *dic6 = [@{@"name":@"Groupe",
                                   @"type": @(2),
                                   @"filter_type":@(FILTER_GROUP),
                                   @"input_type":@(INPUT_TYPE_FIND),
                                   @"status":@(nodeChildQUI6.nodeChildren.count>0),
                                   @"arrow":@(1)} copy];
    nodeChildQUI6.nodeObject = dic6;
    /////////////
    nodeQUI.nodeChildren = [NSMutableArray arrayWithArray: @[nodeChildQUI2,nodeChildQUI3,nodeChildQUI4,nodeChildQUI5,nodeChildQUI6]];
    //MARK: - object QUOI
    
    NSMutableDictionary *dic7 = [@{@"name":str(strFiltrerSurQUOI),
                                   @"type": @(1),
                                   @"status":@(filterQUOI),
                                   @"filter_type":@(FILTER_QUOI)} copy];
    TreeViewNode *nodeQUOI = [TreeViewNode new];
    nodeQUOI.nodeObject = dic7;
    //get tree
    //    NSArray *listTree;
    nodeQUOI.nodeChildren = [NSMutableArray new];
    
    //Only filter Observation that included mes groupe.
    NSMutableArray *listTree = [NSMutableArray new];
    
    for (NSDictionary*dic in  [[PublicationOBJ sharedInstance] getCache_Tree][@"default"]) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            
            [listTree addObject:dic];
        }
        
    }
    
    
    
    [self fillData:listTree nodeLevel:0 nodeParent:nodeQUOI];
    
    //MARK: - object QUAND
    
    NSMutableDictionary *dic8 = [@{@"name":str(strFiltrerSurQUAND),
                                   @"type": @(1),
                                   @"status":@(filterQUAND),
                                   @"listchoose":@[],
                                   @"filter_type":@(FILTER_QUAND)} mutableCopy];
    TreeViewNode *nodeQUAND = [TreeViewNode new];
    /////////////
    NSMutableDictionary *dic9 = [@{@"name":@"Aujourd'hui",
                                   @"type": @(2),
                                   @"filter_date": @(DATE_TODAY),
                                   @"status":@(0)} copy];
    TreeViewNode *nodeChildQUI9 = [TreeViewNode new];
    nodeChildQUI9.nodeObject = dic9;
    nodeChildQUI9.parent = nodeQUAND;
    /////////////
    NSMutableDictionary *dic10 = [@{@"name":@"Hier",
                                    @"filter_date": @(DATE_YESTERDAY),
                                    @"status":@(0),
                                    @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI10 = [TreeViewNode new];
    nodeChildQUI10.nodeObject = dic10;
    nodeChildQUI10.parent = nodeQUAND;
    /////////////
    NSMutableDictionary *dic11 = [@{@"name":@"Mois en cours",
                                    @"filter_date": @(DATE_THISMONTH),
                                    @"status":@(0),
                                    @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI11 = [TreeViewNode new];
    nodeChildQUI11.nodeObject = dic11;
    nodeChildQUI11.parent = nodeQUAND;
    /////////////
    NSMutableDictionary *dic12 = [@{@"name":@"Mois precedent",
                                    @"filter_date": @(DATE_LASTMONTH),
                                    @"status":@(0),
                                    @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI12 = [TreeViewNode new];
    nodeChildQUI12.nodeObject = dic12;
    nodeChildQUI12.parent = nodeQUAND;
    
    NSMutableDictionary *dic122 = [@{@"name":str(strDerniersJours),
                                     @"filter_date": @(DATE_LAST30),
                                     @"status":@(0),
                                     @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI122 = [TreeViewNode new];
    nodeChildQUI122.nodeObject = dic122;
    nodeChildQUI122.parent = nodeQUAND;
    
    
    /////////////
    NSMutableDictionary *dic13 = [@{@"name":@"Saison en cours",
                                    @"filter_date": @(DATE_THISYEAR),
                                    @"status":@(0),
                                    @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI13 = [TreeViewNode new];
    nodeChildQUI13.nodeObject = dic13;
    nodeChildQUI13.parent = nodeQUAND;
    /////////////
    NSMutableDictionary *dic14 = [@{@"name":@"Saison dernière",
                                    @"filter_date": @(DATE_LASTYEAR),
                                    @"status":@(0),
                                    @"type": @(2)} copy];
    TreeViewNode *nodeChildQUI14 = [TreeViewNode new];
    nodeChildQUI14.nodeObject = dic14;
    nodeChildQUI14.parent = nodeQUAND;
    
    /////////////
    NSMutableDictionary *dic15 = [@{@"name":@"Personnaliser",
                                    @"type": @(2),
                                    @"status":@(0),
                                    @"filter_date": @(DATE_PERSONNALISER)/*,
                                                                          @"input_type":@(INPUT_TYPE_DATE)*/} copy];
    TreeViewNode *nodeChildQUI15 = [TreeViewNode new];
    nodeChildQUI15.nodeObject = dic15;
    nodeChildQUI15.parent = nodeQUAND;
    nodeChildQUI15.nodeChildren = [NSMutableArray new];
    NSMutableDictionary *dicChild = [NSMutableDictionary new];
    TreeViewNode *nodeTmp = [TreeViewNode new];
    
    switch (filter_date) {
        case DATE_TODAY:
        {
            dicChild = [dic9 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI9.nodeChildren;
            
        }
            break;
        case DATE_YESTERDAY:
        {
            dicChild = [dic10 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI10.nodeChildren;
            
        }
            break;
            
        case DATE_LAST30:
        {
            dicChild = [dic122 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI122.nodeChildren;
            
        }
            break;
            
        case DATE_THISMONTH:
        {
            dicChild = [dic11 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI11.nodeChildren;
            
        }
            break;
        case DATE_LASTMONTH:
        {
            dicChild = [dic12 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI12.nodeChildren;
            
        }
            break;
        case DATE_THISYEAR:
        {
            dicChild = [dic13 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI13.nodeChildren;
            
        }
            break;
        case DATE_LASTYEAR:
        {
            dicChild = [dic14 mutableCopy];
            nodeTmp.nodeChildren = nodeChildQUI14.nodeChildren;
            
        }
            break;
        case DATE_PERSONNALISER:
        {
            dicChild = [dic15 mutableCopy];
            [dicChild setObject:@(4) forKey:@"type"];
            NSString  *Timer_debut = filterDic[@"date_debut"];
            NSString  *Timer_fin = filterDic[@"date_fin"];
            [dicChild setObject:Timer_debut forKey:@"debut"];
            [dicChild setObject:Timer_fin forKey:@"fin"];
            
            nodeTmp.nodeChildren = nodeChildQUI15.nodeChildren;
        }
            break;
        default:
            break;
    }
    if (dicChild.allKeys.count > 0) {
        [dicChild setObject:@(1) forKey:@"status"];
        nodeTmp.nodeObject = dicChild;
        nodeTmp.parent = nodeQUAND;
        
        [dic8 setObject:@[nodeTmp] forKey:@"listchoose"];
        
    }
    
    /////////////
    
    nodeQUAND.nodeObject = dic8;
    nodeQUAND.nodeChildren = [NSMutableArray arrayWithArray: @[nodeChildQUI9,nodeChildQUI10,nodeChildQUI11,nodeChildQUI122,nodeChildQUI12,nodeChildQUI13,nodeChildQUI14,nodeChildQUI15]];
    /////////////
    _nodes_QUI = [ TreeViewNode new];
    [_nodes_QUI setParent:nodeQUI.parent];
    [_nodes_QUI setNodeObject:nodeQUI.nodeObject];
    [_nodes_QUI setNodeChildren:nodeQUI.nodeChildren];
    
    _nodes_QUOI = [ TreeViewNode new];
    
    _nodes_QUAND = [ TreeViewNode new];
    [_nodes_QUAND setParent:nodeQUAND.parent];
    [_nodes_QUAND setNodeObject:nodeQUAND.nodeObject];
    [_nodes_QUAND setNodeChildren:nodeQUAND.nodeChildren];
    if (!((_expectTarget == ISGROUP) || (_expectTarget ==ISLOUNGE))) {
        [_nodes addObject:nodeQUI];
    }
    [_nodes addObject:nodeQUOI];
    [_nodes addObject:nodeQUAND];
    
    arrData = [NSMutableArray new];
    [self reloadData:_nodes];
    dispatch_async( dispatch_get_main_queue(), ^{
        [self.tableControl reloadData];
    });
}
-(void)reloadData:(NSArray*)arrNodes
{
    
    [arrData removeAllObjects];
    //fill QUI
    for (TreeViewNode *node in arrNodes) {
        [arrData addObject:node];
        switch ([node.nodeObject[@"filter_type"] intValue]) {
            case FILTER_QUI:
            {
                if ([node.nodeObject[@"status"] boolValue]) {
                    [self fillChildrenQUI:node.nodeChildren];
                    
                    NSMutableDictionary *dicAjouter = [@{@"name":@"Ajouter d'autres personnes",
                                                         @"type": @(7),
                                                         @"filter_type":@(FILTER_ADDMORE),
                                                         @"status":@(1)} copy];
                    TreeViewNode *nodeNew = [TreeViewNode new];
                    nodeNew.nodeObject = dicAjouter;
                    nodeNew.parent = node;
                    nodeNew.nodeChildren = [NSMutableArray new];
                    [arrData addObject:nodeNew];
                }
            }
                break;
            case FILTER_QUOI:
            {
                if ([node.nodeObject[@"status"] boolValue]) {
                    /*
                     NSMutableDictionary *dicDesc = [@{@"name":str(strDescriptionQUOI),
                     @"type": @(8),
                     } copy];
                     TreeViewNode *nodeNew = [TreeViewNode new];
                     nodeNew.nodeObject = dicDesc;
                     [arrData addObject:nodeNew];
                     */
                    [self fillChildrenQUOI:node.nodeChildren];
                    
                    NSMutableDictionary *dicAjouter = [@{@"name":@"Ajouter d'autres éléments",
                                                         @"type": @(7),
                                                         @"filter_type":@(FILTER_ADDMORE),
                                                         @"status":@(1)} copy];
                    TreeViewNode *nodeNew = [TreeViewNode new];
                    nodeNew.nodeObject = dicAjouter;
                    nodeNew.parent = node;
                    nodeNew.nodeChildren = [NSMutableArray new];
                    [arrData addObject:nodeNew];
                    
                }
            }
                break;
            case FILTER_QUAND:
            {
                NSArray *listChoose = node.nodeObject[@"listchoose"];
                if ([node.nodeObject[@"status"] boolValue]) {
                    /*
                     NSMutableDictionary *dicDesc = [@{@"name":str(strDescriptionQUAND),
                     @"type": @(8),
                     } copy];
                     TreeViewNode *nodeNew = [TreeViewNode new];
                     nodeNew.nodeObject = dicDesc;
                     [arrData addObject:nodeNew];
                     */
                    if (listChoose.count > 0) {
                        [self fillChildrenQUAND:listChoose];
                        
                    }
                }
            }
                break;
            default:
                break;
        }
    }
}
//MARK: - NODE QUOI
- (void)fillChildrenQUI:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        if ([node.nodeObject[@"status"] boolValue]) {
            [arrData addObject:node];
            [self fillChildrenQUI:node.nodeChildren];
        }
    }
}
- (void)fillChildrenQUOI:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        if ([node.nodeObject[@"check"] intValue] != NON_CHECK) {
            [arrData addObject:node];
        }
        [self fillChildrenQUOI:node.nodeChildren];
    }
}
- (void)fillChildrenQUAND:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [arrData addObject:node];
        //            [self fillChildrenQUAND:node.nodeChildren];
    }
}
- (void)fillData:(NSArray *)childrenArray nodeLevel:(NSUInteger)nodeLevel nodeParent:(TreeViewNode*)nodeParent
{
    for (NSDictionary *dic in childrenArray) {
        NSMutableDictionary *node = [dic mutableCopy];
        int isCheck = 0;
        if ([arrChooseQUOI containsObject:dic[@"id"]]) {
            isCheck = IS_CHECK;
        }
        [node setValue:@(0) forKey:@"status"];
        [node setValue:@(nodeLevel) forKey:@"level"];
        [node setValue:@(isCheck) forKey:@"check"];
        [node setValue:@(5) forKey:@"type"];
        [node setValue:@(0) forKey:@"lineBottom"];
        
        TreeViewNode *treeViewNode = [[TreeViewNode alloc]init];
        treeViewNode.nodeObject = node;
        treeViewNode.nodeChildren =[NSMutableArray new];
        [nodeParent.nodeChildren addObject:treeViewNode];
        treeViewNode.parent = nodeParent;
        
        if (node[@"children"]) {
            [self fillData:node[@"children"] nodeLevel:nodeLevel+1 nodeParent:treeViewNode];
        }
        
    }
}
-(void)getFilterList:(NSArray*)arrNode
{
    for (TreeViewNode *node in arrNode) {
        if ([node.nodeObject[@"check"] intValue] == IS_CHECK) {
            [self addListIDWithNode:node];
            //            continue;
        }
        if (node.nodeChildren) {
            [self getFilterList:node.nodeChildren];
        }
    }
}

-(void)addListIDWithNode:(TreeViewNode*)node
{
    //add list id
    if ([node.nodeObject[@"check"] intValue] == IS_CHECK) {
        if ([arrID indexOfObject:node.nodeObject[@"id"]]== NSNotFound) {
            [arrID addObject:node.nodeObject[@"id"]];
        }
    }
    else
    {
        if ([arrID indexOfObject:node.nodeObject[@"id"]]!= NSNotFound) {
            [arrID removeObject:node.nodeObject[@"id"]];
        }
    }
}
- (void)expandCollapseNode:(NSNotification *)notification
{
    [self reloadData: _nodes];
    [self.tableControl reloadData];
}
//MARK: - ACTION QUOI
- (IBAction)selectAction:(id)sender
{
    UIButton *sv = (UIButton*)sender;
    int index = (int)sv.tag - 300;
    TreeViewNode *nodeSelected = arrData[index];
    NSMutableDictionary *dic = [nodeSelected.nodeObject mutableCopy];
    int statusNode = [dic[@"check"] intValue];
    switch (statusNode) {
        case NON_CHECK:
        {
            statusNode = IS_CHECK;
        }
            break;
        case UN_CHECK:
        {
            statusNode = IS_CHECK;
            
        }
            break;
        case IS_CHECK:
        {
            statusNode = NON_CHECK;
            
        }
            break;
        default:
            break;
    }
    [dic setObject:@(statusNode) forKey:@"check"];
    nodeSelected.nodeObject = dic;
    
    [self checkFromParent:nodeSelected withStatusNode:statusNode];
    [self checkFromChild:nodeSelected];
    NSMutableArray *countCheck = [NSMutableArray new];
    TreeViewNode *noteQUOI = _nodes[1];
    [self countChildSelectQUOI:noteQUOI.nodeChildren withCount:countCheck];
    if (countCheck.count == 0) {
        NSMutableDictionary *dic = [noteQUOI.nodeObject mutableCopy];
        [dic setObject:@(0) forKey:@"status"];
        noteQUOI.nodeObject = dic;
    }
    [self reloadData:_nodes];
    [self.tableControl reloadData];
    
}
- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
    [dic setObject:@(statusNode) forKey:@"check"];
    parent.nodeObject = dic;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        NSMutableDictionary *dic = [node.nodeObject mutableCopy];
        [dic setObject:@(statusNode) forKey:@"check"];
        node.nodeObject = dic;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        NSMutableDictionary *dic = [treenode.nodeObject mutableCopy];
        if ([dic[@"check"] intValue] == IS_CHECK) {
            countCheck++;
        }
        if ([dic[@"check"] intValue] == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(IS_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(UN_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
        
    }
    else
    {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(NON_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
        
    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }
    
}
- (void)countChildSelectQUOI:(NSArray *)childrenArray withCount:(NSMutableArray*)arrCount
{
    for (TreeViewNode *node in childrenArray) {
        if ([node.nodeObject[@"check"] intValue] != NON_CHECK) {
            [arrCount addObject:node];
        }
        [self countChildSelectQUOI:node.nodeChildren withCount:arrCount];
    }
}
//MARK: ADD SUBVIEW
-(void) addSubViewValiderAdd
{
    [self.viewValiderAdd removeFromSuperview];
    __weak CarterFilterView *wself = self;
    self.viewValiderAdd = [[CarterFilterAddView alloc] initWithEVC:_parentVC expectTarget:self.expectTarget isLiveHunt:_isLiveHunt];
    [self.viewValiderAdd addContraintSupview:self];
    [self.viewValiderAdd setCallback:^(TreeViewNode *nodeParent,TreeViewNode *nodeSub,NSString *strSearch)
     {
         wself.tfMotCle.text = strSearch;
         //reload data
         if ([nodeParent.nodeObject[@"filter_type"] intValue] == FILTER_QUI) {
             [wself selectWithdicChoose:nodeSub withChildrenArray:wself.nodes];
             [wself reloadData:wself.nodes];
             [wself.tableControl reloadData];
         }
         else if ([nodeParent.nodeObject[@"filter_type"] intValue] == FILTER_QUOI) {
             for (TreeViewNode *nodeQUOI in wself.nodes) {
                 if ([nodeQUOI.nodeObject[@"filter_type"] intValue] == FILTER_QUOI) {
                     nodeQUOI.nodeChildren = nodeParent.nodeChildren;
                     [wself reloadData:wself.nodes];
                     [wself.tableControl reloadData];
                     break;
                 }
             }
         }
         else if ([nodeParent.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {
             for (TreeViewNode *nodeQUAND in wself.nodes) {
                 if ([nodeQUAND.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {
                     nodeQUAND.nodeChildren = nodeParent.nodeChildren;
                     [wself reloadData:wself.nodes];
                     [wself.tableControl reloadData];
                     break;
                 }
             }
         }
         
     }];
    [self.viewValiderAdd setup];
}

- (void)selectWithdicChoose:(TreeViewNode*)subNode withChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        if ([node.nodeObject[@"name"] isEqualToString:subNode.nodeObject[@"name"]]) {
            NSMutableDictionary *dicObject = [NSMutableDictionary dictionaryWithDictionary:subNode.nodeObject];
            [dicObject setObject:@(1) forKey:@"status"];
            
            if (([node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION
                 || [node.nodeObject[@"filter_type"] intValue] == FILTER_GROUP
                 || [node.nodeObject[@"filter_type"] intValue] == FILTER_AGENDA) && subNode.nodeChildren.count == 0) {
                [dicObject setObject:@(0) forKey:@"status"];
                
            }
            
            if (!node.nodeChildren) {
                node.nodeChildren = [NSMutableArray new];
            }
            node.nodeChildren = [subNode.nodeChildren mutableCopy];
            node.nodeObject = dicObject;
            
            [self openParent:node.parent];
            [self reloadData:_nodes];
            [self.tableControl reloadData];
            break;
        }
        else
        {
            [self selectWithdicChoose:subNode withChildrenArray:node.nodeChildren];
        }
    }
}
- (void)openParent:(TreeViewNode *)parent
{
    
    NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
    [dic setObject:@(1) forKey:@"status"];
    parent.nodeObject = dic;
    if (parent.parent) {
        [self openParent:parent.parent];
    }
}
//MARK: - ACTION
-(IBAction)switchValueChanged:(id)sender
{
    [_tfMotCle resignFirstResponder];
    UISwitch *sv = (UISwitch*)sender;
    int index = (int)sv.tag - 100;
    TreeViewNode *node = arrData[index];
    
    NSMutableDictionary *dic = [node.nodeObject mutableCopy];
    if (sv.on) {
        [dic setObject:@(1) forKey:@"status"];
        
        BOOL isSelected = NO;
        if ([node.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {
            NSArray *listChoose = node.nodeObject[@"listchoose"];
            if (listChoose.count > 0) {
                isSelected = YES;
            }
        }
        else
        {
            for (TreeViewNode *tree  in node.nodeChildren) {
                if ([tree.nodeObject[@"status"] boolValue]) {
                    isSelected = YES;
                    break;
                }
            }
        }
        if (!isSelected) {
            if ([node.nodeObject[@"filter_type"] intValue] == FILTER_QUI) {
                [self addMoreWithNode:node];            }
            else if ([node.nodeObject[@"filter_type"] intValue] == FILTER_QUOI)
            {
                [self addMoreWithNode:node];
            }
            else if ([node.nodeObject[@"filter_type"] intValue] == FILTER_QUAND)
            {
                [self addMoreWithNode:node];
            }
        }
    }
    else
    {
        [dic setObject:@(0) forKey:@"status"];

    }
    node.nodeObject = dic;
    [self reloadData:_nodes];
    [self.tableControl reloadData];
    
}
-(IBAction)closeCellAction:(id)sender
{
    UIButton *sv = (UIButton*)sender;
    int index = (int)sv.tag - 200;
    TreeViewNode *node = arrData[index];
    NSMutableDictionary *dic = [node.nodeObject mutableCopy];
    if ([dic[@"type"] intValue] == 4)
    {
        [dic setObject:@(2) forKey:@"type"];
        node.nodeObject = dic;
        
        TreeViewNode *nodeParent = node.parent;
        NSMutableDictionary *dicParent = [nodeParent.nodeObject mutableCopy];
        [dicParent removeObjectForKey:@"status"];
        nodeParent.nodeObject = dicParent;
        [self reloadData: _nodes];
        [self.tableControl reloadData];
        
    }
    
}
-(IBAction)removeItemChoose:(id)sender
{
    [_tfMotCle resignFirstResponder];
    UIButton *btn = (UIButton*)sender;
    int index = (int)btn.tag - 300;
    //un select current nod
    TreeViewNode *node = arrData[index];
    if ([node.parent.nodeObject[@"filter_type"] intValue] == FILTER_QUAND)
    {
        TreeViewNode *nodeParent = node.parent;
        NSMutableDictionary *dicParent = [nodeParent.nodeObject mutableCopy];
        NSArray *listChoose = dicParent[@"listchoose"];
        if (listChoose.count > 0) {
            TreeViewNode *node_Child = listChoose[0];
            NSMutableDictionary *dicChild = [node_Child.nodeObject mutableCopy];
            [dicChild setObject:@(0) forKey:@"status"];
            [dicChild setObject:@(2) forKey:@"type"];
            [dicChild setObject:@"" forKey:@"fin"];
            [dicChild setObject:@"" forKey:@"debut"];
            node_Child.nodeObject = dicChild;
            
            for (TreeViewNode *child in nodeParent.nodeChildren) {
                if ([dicChild[@"filter_date"] intValue] == [child.nodeObject[@"filter_date"] intValue]) {
                    child.nodeObject = node_Child.nodeObject;
                    break;
                }
            }

        }
        [dicParent setObject:@[] forKey:@"listchoose"];
        [dicParent setObject:@(0) forKey:@"status"];
        nodeParent.nodeObject = dicParent;
        
    }
    else if ([node.parent.nodeObject[@"filter_type"] intValue] == FILTER_QUI)
    {
        NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:node.nodeObject];
        [dicObj setObject:@(0) forKey:@"status"];
        node.nodeObject = dicObj;
        //set node child
        for (TreeViewNode *nodChild in node.nodeChildren) {
            NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:nodChild.nodeObject];
            [dicObj setObject:@(0) forKey:@"status"];
            nodChild.nodeObject = dicObj;
        }
        
        [self selectParent:node.parent WithSelected:NO];
        
    }
    else{
        for (int i = 0; i <node.parent.nodeChildren.count; i++) {
            TreeViewNode *nodeTmp = node.parent.nodeChildren[i];
            if ([nodeTmp.nodeObject[@"id"] intValue] == [node.nodeObject[@"id"] intValue]) {
                [node.parent.nodeChildren removeObjectAtIndex:i];
                break;
            }
        }
        [self selectParent:node.parent WithSelected:NO];
    }
    [self reloadData: _nodes];
    [self.tableControl reloadData];
    if ([node.parent.nodeObject[@"filter_type"] intValue] == FILTER_QUAND)
    {
        NSIndexPath* ipath = [NSIndexPath indexPathForRow: arrData.count-1 inSection: 0];
        [self.tableControl scrollToRowAtIndexPath: ipath atScrollPosition: UITableViewScrollPositionTop animated: YES];
    }
    
}
-(void)selectParent:(TreeViewNode*)parent WithSelected:(BOOL)isSelected
{
    //count children selected
    int countSelected = 0;
    for (TreeViewNode *nodChild in parent.nodeChildren) {
        if ([nodChild.nodeObject[@"status"] boolValue]) {
            countSelected ++;
        }
    }
    //if childrend count unselected = 0 => set super parent
    if (countSelected == 0) {
        NSMutableDictionary *dicObjParent = [NSMutableDictionary dictionaryWithDictionary:parent.nodeObject];
        [dicObjParent setObject:@(0) forKey:@"status"];
        parent.nodeObject = dicObjParent;
        
    }
    if (parent.parent) {
        if (countSelected == 0) {
            [self selectParent:parent.parent WithSelected:isSelected];
        }
    }
    else
    {
        //root
        if( countSelected < 1)
        {
            for (TreeViewNode *nodChild in parent.nodeChildren) {
                if ([nodChild.nodeObject[@"status"] boolValue]) {
                    NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:nodChild.nodeObject];
                    [dicObj setObject:@(0) forKey:@"status"];
                    nodChild.nodeObject = dicObj;
                    
                    NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
                    [dic setObject:@(0) forKey:@"status"];
                    parent.nodeObject = dic;
                    break;
                }
            }
        }
        
    }
    
}
//MARK: - CANCEL
-(IBAction)bacButtonAction:(id)sender
{
    [self hide:YES];
}
//MARK: - VALIDER
-(IBAction)addMoreAction:(id)sender
{
    [_tfMotCle resignFirstResponder];
    UIButton *btn = (UIButton*)sender;
    int index = (int)btn.tag - 700;
    TreeViewNode *node = arrData[index];
    [self addMoreWithNode:node.parent];
    
}
-(IBAction)validerAction:(id)sender
{
    //gen filter QUI
    [self saveFilterObjs];
    
}

-(void) saveFilterObjs
{
    BOOL isCheckValider = YES;
    NSMutableDictionary *dic = [NSMutableDictionary new];
    for (TreeViewNode *nodeQUI in _nodes) {
        if ([nodeQUI.nodeObject[@"filter_type"] intValue] == FILTER_QUI) {
            TreeViewNode *nodeMoi = nodeQUI.nodeChildren[0];
            int filterQUI = 0;
            if ([nodeQUI.nodeObject[@"status"] boolValue]) {
                filterQUI = 1;
                int iSharingMoi = [nodeMoi.nodeObject[@"status"] intValue];
                TreeViewNode *nodeAmis = nodeQUI.nodeChildren[1];
                int iSharingAmis = [nodeAmis.nodeObject[@"status"] intValue];
                //personne
                NSMutableArray *arrPersonne = [NSMutableArray new];
                TreeViewNode *nodeChild_Personne = nodeQUI.nodeChildren[2];
                for (TreeViewNode *dicGroup in nodeChild_Personne.nodeChildren) {
                    NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:dicGroup.nodeObject];
                    [arrPersonne addObject:dicObj];
                }
                //Save mesgroup filter
                NSString *strPath_Personne = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_PERSONNE_SAVE)];
                [arrPersonne writeToFile:strPath_Personne atomically:YES];
                
                //agenda
                NSMutableArray *arrAgendaSave = [NSMutableArray new];
                TreeViewNode *nodeChild_Agenda = nodeQUI.nodeChildren[3];
                
                for (TreeViewNode *dicAgenda in nodeChild_Agenda.nodeChildren) {
                    NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:dicAgenda.nodeObject];
                    [arrAgendaSave addObject:dicObj];
                }
                //group
                NSMutableArray *arrGroupSave = [NSMutableArray new];
                TreeViewNode *nodeChild_Group = nodeQUI.nodeChildren[4];
                
                for (TreeViewNode *dicGroup in nodeChild_Group.nodeChildren) {
                    NSMutableDictionary *dicObj = [NSMutableDictionary dictionaryWithDictionary:dicGroup.nodeObject];
                    [arrGroupSave addObject:dicObj];
                }
                
                
                //
                //Save mesgroup filter
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_MES_GROUP_SAVE)   ];
                [arrGroupSave writeToFile:strPath atomically:YES];
                
                strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_MES_HUNT_SAVE)  ];
                [arrAgendaSave writeToFile:strPath atomically:YES];
                //
                [dic setObject:[NSNumber numberWithInt:filterQUI] forKey:@"filterQUI"];
                [dic setObject:[NSNumber numberWithInt:iSharingMoi] forKey:@"iSharingMoi"];
                [dic setObject:[NSNumber numberWithInt:iSharingAmis] forKey:@"iSharingAmis"];
                
            }
            else
            {
                //personne
                NSMutableArray *arrPersonne = [NSMutableArray new];
                NSString *strPath_Personne = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_PERSONNE_SAVE)];
                [arrPersonne writeToFile:strPath_Personne atomically:YES];
                //group
                NSMutableArray *arrGroupSave = [NSMutableArray new];
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILTER_MES_GROUP_SAVE)   ];
                [arrGroupSave writeToFile:strPath atomically:YES];
                //agenda
                NSMutableArray *arrAgendaSave = [NSMutableArray new];
                strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_MES_HUNT_SAVE)  ];
                [arrAgendaSave writeToFile:strPath atomically:YES];
                
            }
            
        }
        //get filter QUOI
        if ([nodeQUI.nodeObject[@"filter_type"] intValue] == FILTER_QUOI) {
            TreeViewNode *nodeQUOI = nodeQUI;
            int filterQUOI = 0;
            if ([nodeQUOI.nodeObject[@"status"] boolValue]) {
                filterQUOI = 1;
                arrID = [NSMutableArray new];
                [self getFilterList:nodeQUOI.nodeChildren];
                [dic setObject:[NSNumber numberWithInt:filterQUOI] forKey:@"filterQUOI"];
                [dic setObject:arrID?arrID:@[] forKey:@"listID"];
            }
        }
        if ([nodeQUI.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {
            //get filter QUAND
            TreeViewNode *nodeQUAND = nodeQUI;
            //Save parameter...
            
            int filterQUAND = 0;
            if ([nodeQUAND.nodeObject[@"status"] boolValue]) {
                filterQUAND = 1;
                NSArray *listChoose = nodeQUAND.nodeObject[@"listchoose"];
                if (listChoose.count > 0) {
                    TreeViewNode *nodeChoose = listChoose[0];
                    NSTimeInterval timerBegin = 0;
                    NSTimeInterval timerEnd =0;
                    switch ([nodeChoose.nodeObject[@"filter_date"] intValue]) {
                        case DATE_TODAY:
                        {
                            timerBegin = [NSDate getBeginToday];
                            timerEnd   = [NSDate getEndToday];
                        }
                            break;
                        case DATE_YESTERDAY:
                        {
                            timerBegin = [NSDate getBeginYesterday];
                            timerEnd   = [NSDate getEndYesterday];
                            
                        }
                            break;
                            
                        case DATE_LAST30:
                        {
                            timerBegin = [NSDate getBeginLast30];
                            timerEnd   = [NSDate getEndLast30];
                            
                        }
                            break;
                            
                        case DATE_THISMONTH:
                        {
                            timerBegin = [NSDate getBeginThisMonth];
                            timerEnd   = [NSDate getEndThisMonth];
                        }
                            break;
                        case DATE_LASTMONTH:
                        {
                            timerBegin = [NSDate getBeginLastMonth];
                            timerEnd   = [NSDate getEndLastMonth];
                        }
                            break;
                        case DATE_THISYEAR:
                        {
                            //                    timerBegin = [NSDate getBeginThisYear];
                            //                    timerEnd   = [NSDate getEndThisYear];
                            timerBegin = [NSDate getBeginWithStringDate:@"2016-07-01 00:00:00"];
                            timerEnd = [NSDate getEndWithStringDate:@"2017-06-30 23:59:59"];
                        }
                            break;
                        case DATE_LASTYEAR:
                        {
                            //                    timerBegin = [NSDate getBeginLastYear];
                            //                    timerEnd   = [NSDate getEndLastYear];
                            timerBegin = [NSDate getBeginWithStringDate:@"2015-07-01 00:00:00"];
                            timerEnd = [NSDate getEndWithStringDate:@"2016-06-30 23:59:59"];
                            
                        }
                            break;
                        case DATE_PERSONNALISER:
                        {
                            
                            NSString *strDebut = nodeChoose.nodeObject[@"debut"];
                            NSString *strFin = nodeChoose.nodeObject[@"fin"];
                            
                            if (strDebut.length> 0 && strFin.length >0) {
                                NSDateFormatter *dateFormatter = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
                                [ASSharedTimeFormatter checkFormatString: @"dd/MM/yyyy HH:mm" forFormatter: dateFormatter];
                                
                                NSDate *dateDebut = [dateFormatter dateFromString: strDebut];
                                timerBegin = [dateDebut timeIntervalSince1970];
                                
                                NSDate *dateFin = [dateFormatter dateFromString: strFin];
                                timerEnd = [dateFin timeIntervalSince1970];
                                [dic setObject:strDebut forKey:@"date_debut"];
                                [dic setObject:strFin forKey:@"date_fin"];
                                
                            }
                            else
                            {
                                isCheckValider= NO;
                            }
                        }
                            break;
                        default:
                            break;
                    }
                    if (timerBegin > 0 && timerEnd > 0) {
                        [dic setObject:[NSString stringWithFormat:@"%d",(int)timerBegin] forKey:@"debut"];
                        [dic setObject:[NSString stringWithFormat:@"%d",(int)timerEnd] forKey:@"fin"];
                        [dic setObject:nodeChoose.nodeObject[@"filter_date"] forKey:@"filter_date"];
                    }
                }
                [dic setObject:[NSNumber numberWithInt:filterQUAND] forKey:@"filterQUAND"];
            }
        }
    }
    
    _myC_Search_Text =_tfMotCle.text?_tfMotCle.text: @"";
    [dic setObject:_myC_Search_Text forKey:@"c_search"];
    
    BOOL bFilterON = NO;
    
    if ([dic[@"filterQUI"] boolValue] || [dic[@"filterQUOI"] boolValue] || [dic[@"filterQUAND"]boolValue] || _myC_Search_Text.length > 0) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }
    [dic setObject:[NSNumber numberWithBool:bFilterON] forKey:@"bFilterON"];
    switch (_expectTarget) {
        case ISGROUP:
        {
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"OBJECT_FILTER_GROUP"];
        }
            break;
        case ISLOUNGE:
        {
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"OBJECT_FILTER_LOUNGE"];
        }
            break;
        default:
        {
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:
             [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
            
            [[NSUserDefaults standardUserDefaults]synchronize];

        }
            break;
    }
    //
    if (!isCheckValider) {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
        return;
        
    }
    else
    {
        if (_callback) {
            _callback(_myC_Search_Text);
        }
        [self hide:YES];
    }
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TreeViewNode *node = arrData[indexPath.row];
    NSMutableDictionary  *dic = [node.nodeObject mutableCopy];
    
    if ([dic[@"type"] intValue] == 1) {
        Filter_Cell_Type1 *cell = (Filter_Cell_Type1 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        cell.lbTitle.text = dic[@"name"];
        //Status
        cell.swStatus.transform = CGAffineTransformMakeScale(0.75, 0.75);
        switch (self.expectTarget) {
            case ISMUR:
            {
                [cell.swStatus setOnTintColor:UIColorFromRGB(ON_SWITCH)];
            }
                break;
            case ISGROUP:
            {
                [cell.swStatus setOnTintColor:UIColorFromRGB(ON_SWITCH_GROUP)];
            }
                break;
            case ISLOUNGE:
            {
                if (_isLiveHunt) {
                    [cell.swStatus setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
                }
                else
                {
                    [cell.swStatus setOnTintColor:UIColorFromRGB(ON_SWITCH_CHASSES)];
                }
            }
                break;
            default:
            {
                [cell.swStatus setOnTintColor:self.colorNavigation];
            }
                break;
        }
        
        
        [cell.swStatus setOn:[dic[@"status"] boolValue]];
        [cell.swStatus addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
        [cell.swStatus setBackgroundColor:UIColorFromRGB(OFF_SWITCH)];
        cell.swStatus.layer.cornerRadius = 16;
        cell.swStatus.tag = indexPath.row + 100;
        //
        if ([dic[@"status"] boolValue]) {
            cell.imgArrow.image = [UIImage imageNamed:@"favo_arrow_down"];
        }
        else
        {
            cell.imgArrow.image = [UIImage imageNamed:@"favo_arrow_right"];
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else if ([dic[@"type"] intValue] == 2)
    {
        Filter_Cell_Type2 *cell = (Filter_Cell_Type2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
        [cell.btnClose removeTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnClose removeTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lbTitle.text = dic[@"name"];
        //        if ([dic[@"filter_date"] intValue]==DATE_LAST30) {
        //            cell.lbDescription.text = str(strDerniersJours);
        //
        //        }
        //        else
        //        {
        //            cell.lbDescription.text = nil;
        //        }
        if ([dic[@"status"] boolValue]) {
            cell.imgArrow.image = [UIImage imageNamed:@"close"];
            [cell.btnClose addTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            if ([node.parent.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {
                cell.imgArrow.image = nil;
            }
            else
            {
                cell.imgArrow.image = [UIImage imageNamed:@"ic_arrow"];
            }
            [cell.btnClose removeTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.btnClose.tag = indexPath.row + 300;
        
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    //    else if ([dic[@"type"] intValue] == 3)
    //    {
    //        Filter_Cell_Type3 *cell = (Filter_Cell_Type3 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3 forIndexPath:indexPath];
    //        cell.lbTitle.text = dic[@"name"];
    //        [cell.btnClose addTarget:self action:@selector(closeCellAction:) forControlEvents:UIControlEventTouchUpInside];
    //        cell.btnClose.tag =indexPath.row+200;
    //
    //        //Status
    //        cell.backgroundColor=[UIColor clearColor];
    //        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //        return cell;
    //
    //    }
    else if ([dic[@"type"] intValue] == 4)
    {
        Filter_Cell_DatePersonnaliser *cell = (Filter_Cell_DatePersonnaliser *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection4 forIndexPath:indexPath];
        
        cell.lbTitle.text = dic[@"name"];
        [cell.btnClose addTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnClose.tag =indexPath.row+300;
        cell.lbTitleFin.text = str(strDateDeFin);
        cell.lbTitleDebut.text = str(strDateDeDebut);
        cell.lbDebut.text = dic[@"debut"];
        cell.lbFin.text = dic[@"fin"];
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    
    else if ([dic[@"type"] intValue] == 5)
    {
        Filter_Cell_Type2 *cell = (Filter_Cell_Type2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection9 forIndexPath:indexPath];
        [cell.btnClose removeTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnClose removeTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lbTitle.text = dic[@"name"];
        cell.imgArrow.image = [UIImage imageNamed:@"close"];
        [cell.btnClose addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnClose.tag = indexPath.row + 300;
        int level = [dic[@"level"] intValue];
        int indentation = level * 15 + 20;
        cell.contraintPaddingLeft.constant=indentation;
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else if ([dic[@"type"] intValue] == 6)
    {
        Filter_Cell_SubNode *cell = (Filter_Cell_SubNode *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection6 forIndexPath:indexPath];
        cell.lbTitle.text = dic[@"name"];
        [cell.btnClose addTarget:self action:@selector(removeItemChoose:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnClose.tag = indexPath.row + 300;
        cell.imgArrow.image = [UIImage imageNamed:@"xx-icon"];
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else if ([dic[@"type"] intValue] == 7)
    {
        Filter_Cell_AddMore *cell = (Filter_Cell_AddMore *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection7 forIndexPath:indexPath];
        [cell.btnAddMore addTarget:self action:@selector(addMoreAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnAddMore.tag = indexPath.row + 700;
        cell.lbTitle.text = dic[@"name"];
        //Status
        cell.backgroundColor=[UIColor blackColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    /*
     else if ([dic[@"type"] intValue] == 8)
     {
     Filter_Cell_Description *cell = (Filter_Cell_Description *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection8 forIndexPath:indexPath];
     cell.lbDescription.text = dic[@"name"];
     //Status
     cell.backgroundColor=[UIColor clearColor];
     [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
     return cell;
     
     }
     */
    else
    {
        NSLog(@"node nil: %@",dic);
        return nil;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TreeViewNode *node = arrData[indexPath.row];
    if ([node.nodeObject[@"filter_type"] intValue] == FILTER_QUI) {
        //        [self addMoreWithNode:node];
    }
    else if ([node.parent.nodeObject[@"filter_type"] intValue] == FILTER_QUAND)
    {
        
        NSMutableDictionary *dicChild = [node.nodeObject mutableCopy];
        [dicChild setObject:@(1) forKey:@"status"];
        if([dicChild[@"filter_date"] intValue] == DATE_PERSONNALISER)
        {
            [dicChild setObject:@(4) forKey:@"type"];
        }
        
        TreeViewNode *nodeTmp = [TreeViewNode new];
        nodeTmp.nodeChildren = node.nodeChildren;
        nodeTmp.nodeObject = dicChild;
        nodeTmp.parent = node.parent;
        
        TreeViewNode *nodeParent = node.parent;
        NSMutableDictionary *dicParent = [nodeParent.nodeObject mutableCopy];
        [dicParent setObject:@[nodeTmp] forKey:@"listchoose"];
        nodeParent.nodeObject = dicParent;
        [self reloadData: _nodes];
        [self.tableControl reloadData];
    }
}

-(void)addMoreWithNode:(TreeViewNode *)node
{
    //set header
    NSMutableDictionary *dic = [node.nodeObject mutableCopy];
    if ([dic[@"filter_type"] intValue] == FILTER_QUI) {
        NSMutableDictionary *dicParent = [_nodes_QUI.nodeObject mutableCopy];
        [dicParent setObject:@(1) forKey:@"status"];
        _nodes_QUI.nodeObject = dicParent ;
        self.viewValiderAdd.tfMotCle.text = _tfMotCle.text;
        [self.viewValiderAdd fnTreeNode:_nodes_QUI];
        [self.viewValiderAdd hide:NO];
    }
    else if ([dic[@"filter_type"] intValue] == FILTER_QUOI ) {
        self.viewValiderAdd.tfMotCle.text = _tfMotCle.text;
        [self.viewValiderAdd fnTreeNode:node];
        [self.viewValiderAdd hide:NO];
    }
    else if ([dic[@"filter_type"] intValue] == FILTER_QUAND ) {
        self.viewValiderAdd.tfMotCle.text = _tfMotCle.text;
        [self.viewValiderAdd fnTreeNode:node];
        [self.viewValiderAdd hide:NO];
    }
}
//MARK: - TEXTFIELD DELEGATe
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    return YES;
}

@end
