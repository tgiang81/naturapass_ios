//
//  LegendAnnotationView.m
//  Naturapass
//
//  Created by Giang on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "LegendAnnotationView.h"
#import "Define.h"

@import QuartzCore;

NSString * const kJPSThumbnailAnnotationViewReuseID = @"JPSThumbnailAnnotationView";

static CGFloat const kJPSThumbnailAnnotationViewStandardWidth     = 75.0f;
static CGFloat const kJPSThumbnailAnnotationViewStandardHeight    = 50.0f;
static CGFloat const kJPSThumbnailAnnotationViewExpandOffset      = 70.0f;
static CGFloat const kJPSThumbnailAnnotationViewVerticalOffset    = 10.0f;
static CGFloat const kJPSThumbnailAnnotationViewAnimationDuration = 0.25f;

@interface LegendAnnotationView ()
{
    float iWidth ;
}
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;

@property (nonatomic, strong) CAShapeLayer *bgLayer;
@property (nonatomic, strong) UIButton *disclosureButton;

@end

@implementation LegendAnnotationView

- (id)initWithAnnotation:(id<MKAnnotation>)annotation {
    self = [super initWithAnnotation:annotation reuseIdentifier:kJPSThumbnailAnnotationViewReuseID];
    
    if (self) {
        
        self.mapViewAnnotation = annotation;
        
        self.canShowCallout = NO;
        
        UILabel*tmp = [UILabel new];
        tmp.font = [UIFont boldSystemFontOfSize:13];
        
        tmp.text = [self.mapViewAnnotation.myDictionary[@"c_title"] uppercaseString];
        
        CGSize textSize = [[tmp text] sizeWithAttributes:@{NSFontAttributeName:[tmp font]}];
        
        CGFloat strikeWidth = textSize.width + 5+20;

        iWidth = strikeWidth;

        self.frame = CGRectMake(0, 0, strikeWidth, kJPSThumbnailAnnotationViewVerticalOffset + 15);
        self.backgroundColor = [UIColor clearColor];
        self.centerOffset = CGPointMake(0, -kJPSThumbnailAnnotationViewVerticalOffset);
        
        [self setupView];
    }
    
    return self;
}

- (void)setLayerProperties {
    _bgLayer = [CAShapeLayer layer];
    CGPathRef path = [self newBubbleWithRect:self.bounds];
    _bgLayer.path = path;
    CFRelease(path);
    
    ASLog(@"%@",self.mapViewAnnotation.myDictionary);
//    color = 008080;

    NSString *strColor = self.mapViewAnnotation.myDictionary[@"c_color"];
    
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:strColor];
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&result];

    
    _bgLayer.fillColor = UIColorFromRGB(result).CGColor;//[UIColor grayColor].CGColor;
    
    _bgLayer.opacity = 0.4;
    
    _bgLayer.masksToBounds = NO;
    
    [self.layer insertSublayer:_bgLayer atIndex:0];
}

- (void)setupView {
    
    [self animateBubbleWithDirection];
    
    UIButtonType buttonType = UIButtonTypeCustom;
    _disclosureButton = [UIButton buttonWithType:buttonType];
    _disclosureButton.backgroundColor = [UIColor clearColor];
    _disclosureButton.frame = CGRectMake(self.frame.origin.x,
                                         2,
                                         iWidth,
                                         20);
    [_disclosureButton.titleLabel setFont:[UIFont systemFontOfSize:13]];
    
    [_disclosureButton setTitle:[self.mapViewAnnotation.myDictionary[@"c_title"] uppercaseString] forState:UIControlStateNormal];
    
    [_disclosureButton addTarget:self action:@selector(didTapDisclosureButton) forControlEvents:UIControlEventTouchDown];
    //
    UIEdgeInsets titleInsets = UIEdgeInsetsMake(0.0, -15.0, 0.0, 0.0);
    _disclosureButton.titleEdgeInsets = titleInsets;

    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(iWidth-15, 5, 10, 10)];
    image.image = [UIImage imageNamed:@"icon_arrow_carter"];
    [_disclosureButton addSubview:image];
    [self addSubview:_disclosureButton];
    
    
    
    [self setLayerProperties];
    
}
-(void) didTapDisclosureButton
{
    ASLog(@"abc");
    
    [[NSNotificationCenter defaultCenter] postNotificationName: SHOW_TITLE_SHAPE_NOTIFICATION object: nil userInfo: self.mapViewAnnotation.myDictionary];

}

+ (UIImage *)disclosureButtonImage {
    CGSize size = CGSizeMake(21.0f, 36.0f);
    UIGraphicsBeginImageContextWithOptions(size, NO, [[UIScreen mainScreen] scale]);
    
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(2.0f, 2.0f)];
    [bezierPath addLineToPoint:CGPointMake(10.0f, 10.0f)];
    [bezierPath addLineToPoint:CGPointMake(2.0f, 18.0f)];
    [[UIColor lightGrayColor] setStroke];
    bezierPath.lineWidth = 3.0f;
    [bezierPath stroke];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)animateBubbleWithDirection {
    // Bubble
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = 1;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.duration = kJPSThumbnailAnnotationViewAnimationDuration;
    
    
    CGPathRef fromPath = [self newBubbleWithRect:self.bounds ];
    animation.fromValue = (__bridge id)fromPath;
    CGPathRelease(fromPath);
    
    [self.bgLayer addAnimation:animation forKey:animation.keyPath];
}


#pragma mark - Geometry

- (CGPathRef)newBubbleWithRect:(CGRect)rect {
    CGFloat stroke = 1.0f;
    CGFloat radius = 1.5f;
    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat parentX = rect.origin.x + rect.size.width/2.0f;

    rect.origin.x += 0;//stroke / 2.0f + 7.0f;
    rect.origin.y += 0;//stroke / 2.0f + 7.0f;

    CGPathMoveToPoint(path, NULL, rect.origin.x, rect.origin.y + radius);
    CGPathAddLineToPoint(path, NULL, rect.origin.x, rect.origin.y + rect.size.height - radius);
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + rect.size.height - radius, radius, M_PI, M_PI_2, 1);
    CGPathAddLineToPoint(path, NULL, parentX - 5.0f, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, parentX, rect.origin.y + rect.size.height + 5.0f);
    CGPathAddLineToPoint(path, NULL, parentX + 5.0f, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height);
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height - radius, radius, M_PI_2, 0.0f, 1.0f);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width, rect.origin.y + radius);
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + radius, radius, 0.0f, -M_PI_2, 1.0f);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + radius, rect.origin.y);
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + radius, radius, -M_PI_2, M_PI, 1.0f);
    CGPathCloseSubpath(path);
    return path;
}

@end
