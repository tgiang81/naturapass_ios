//
//  ErrorViewController.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 6/13/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "ErrorViewController.h"
#import "AppCommon.h"
#import "Define.h"
@interface ErrorViewController ()

@end

@implementation ErrorViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [imgLogo setImage:[UIImage imageNamed:strLogo]];
    //self.navigationItem.hidesBackButton=YES;
    self.navigationController.navigationBarHidden=NO;
    [self loadFrame];
}

-(void)loadFrame {

    [errorTitleLabel setText:str(strERRORTITLE)];
    [errorDescLabel setText:str(strERRORDESCRIPTION)];
    [inconvienenceLabel setText:str(strINCONVIENINENCE)];
    
    [inconvienenceLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-LtCn" size:15]];
    [errorDescLabel setFont:[UIFont fontWithName:@"HelveticaNeueLTStd-LtCn" size:15]];
}


-(IBAction)goBack {

}
-(IBAction)gotoHomeAction:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate hideSplashView];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
