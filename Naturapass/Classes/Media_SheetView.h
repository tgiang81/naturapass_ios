//
//  Media_SheetView.h
//  Naturapass
//
//  Created by GiangTT on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^Media_SheetViewCallback)(NSDictionary* dataRet);

@interface Media_SheetView : UIView
{
    IBOutlet UIView *subAlertView;
    NSString *strDescriptions;
    UIViewController *myParent;
    MEDIATYPE myType;
    
}
@property (nonatomic,assign) ISSCREEN expectTarget;

@property (nonatomic,strong) IBOutlet UIButton *btnPhoto;
@property (nonatomic,strong) IBOutlet UIButton *btnLibrary;
@property (nonatomic,strong) IBOutlet UIButton *btnClose;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UIView *viewSelected;


@property (nonatomic,copy) Media_SheetViewCallback callback;
-(void)doBlock:(Media_SheetViewCallback ) cb;

-(IBAction)closeAction:(id)sender;
-(void)showAlert;
-(void)autoShowCamera;
-(instancetype)initWithType:(MEDIATYPE)type withParent:(UIViewController*) parent target:(ISSCREEN)target;


@end
