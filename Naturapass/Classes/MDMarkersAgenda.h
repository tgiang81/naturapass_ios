//
//  MDMarkersAgenda.h
//  Naturapass
//
//  Created by Giang on 10/31/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^callBackMarkersCustom) (UIImage *image,UIImage *image_nonLegend, float percent_arrow);
@interface MDMarkersAgenda : UIView
{
    int sizeLegend;
    UILabel *lbMarker;
    UIImageView *imageViewMarker;
    float percent_arrow;
}
//@property (nonatomic,assign) BOOL isResize;
//@property (nonatomic,assign) NSInteger levelZoom;
//@property (nonatomic,assign) NSInteger heightMarker;

@property (nonatomic,strong) NSDictionary *dicMarker;
@property (nonatomic, copy) callBackMarkersCustom CallBackMarkers;
-(instancetype)initWithDictionary:(NSDictionary*)dicMarker;
- (void)doSetCalloutView;
@end
