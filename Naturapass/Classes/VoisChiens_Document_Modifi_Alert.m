//
//  MyAlertView.m
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "VoisChiens_Document_Modifi_Alert.h"

#import "Config.h"
#import "CellKind18.h"
#import "AppCommon.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface VoisChiens_Document_Modifi_Alert ()
{
    NSArray *arrTitle;
}
@end

@implementation VoisChiens_Document_Modifi_Alert

-(void)awakeFromNib{
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];

    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [popTable registerNib:[UINib nibWithNibName:@"CellKind18" bundle:nil] forCellReuseIdentifier:identifierSection1];
    arrTitle =@[@{@"icon":@"param_icon_eye",@"name":str(strVoir)},
                @{@"icon":@"param_ic_modifi",@"name":str(strModifier)},
                @{@"icon":@"param_icon_delete",@"name":str(strSupprimer)}];
    self.constraintHeightViewBorder.constant = 4*40;
}
-(void)fnSetTitleWithArray:(NSArray*)arr
{
    arrTitle =arr;
    self.constraintHeightViewBorder.constant = (arrTitle.count+1)*40;
    [popTable reloadData];
}
-(IBAction)annulerAction:(id)sender
{
    [self removeFromSuperview];
}
#pragma callback
-(void)setCallback:(MyAlertViewCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(MyAlertViewCbk ) cb
{
    self.callback = cb;
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrTitle count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrTitle[indexPath.row];

    CellKind18 *cell = nil;
    cell = (CellKind18 *)[popTable dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    [cell.imageIcon setImage:[UIImage imageNamed:dic[@"icon"]]];
    cell.constraintLeftIconWidth.constant=13;
    cell.constraintLeftIconHeight.constant=11;
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.rightIcon.hidden=YES;
    cell.imageLine.hidden =YES;
    [cell layoutIfNeeded];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self removeFromSuperview];
    if (_callback) {
        _callback(indexPath.row);
    }
}
@end
