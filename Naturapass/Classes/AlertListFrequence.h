//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
#import "Define.h"
typedef void (^AlertListFrequenceCallback)(NSDictionary *dicUserData);
@interface AlertListFrequence : UIView<UITextFieldDelegate>
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
}
@property (strong, nonatomic)  NSMutableArray *arrFrequence;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,copy) AlertListFrequenceCallback callback;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (strong, nonatomic) IBOutlet UIView *viewBorder;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;

-(void)doBlock:(AlertListFrequenceCallback ) cb;
-(void)showAlert;
-(instancetype)init;
@end
