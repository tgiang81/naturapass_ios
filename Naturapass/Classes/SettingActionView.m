//
//  SettingActionView.m
//  Naturapass
//
//  Created by JoJo on 11/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SettingActionView.h"
#import "NSString+EMOEmoji.h"

@implementation SettingActionView

-(instancetype)initSettingActionView
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"SettingActionView" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [self.subView.layer setMasksToBounds:YES];
        self.subView.layer.cornerRadius= 5;
        self.subView.layer.borderWidth = 0.5;
        self.subView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
        [self.viewMore.layer setMasksToBounds:YES];
        self.viewMore.layer.cornerRadius= 15;
        [self.bgImageShare.layer setMasksToBounds:YES];
        self.bgImageShare.layer.cornerRadius= 13;
        _btnDismiss = [UIButton new];
    }
    return self;
}
#pragma callback
-(void)setCallback:(SettingActionViewViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(SettingActionViewViewCallback ) cb
{
    self.callback = cb;
    
}
-(void)fnInputData:(NSDictionary*)dic
{
    publicationDic = [dic copy];
    _lbGroupTitle.text = @"";
    _lbGroupDesc.text = @"";
    _lbAgendaTitle.text = @"";
    _lbAgendaDesc.text = @"";
    _lbPersonneTitle.text = @"";
    _lbPersonneDesc.text = @"";
    _lbAmisTitle.text = @"";

    
    //Amis
    BOOL isShareAmis = NO;
    
    if ([dic[@"sharing"] isKindOfClass: [NSDictionary class]]) {
        if ( [dic[@"sharing"][@"share"] intValue] == 1 ) {
            //share AMIS
            isShareAmis = YES;
        }
    }
    if (isShareAmis) {
        _lbAmisTitle.text = @"Amis";
    }
    
    //personne
    NSMutableArray *arrPersonne =  [NSMutableArray new];
    NSString*strPersonneName = nil;
    
    for (NSDictionary*gDic in publicationDic[@"shareusers"]) {
        [arrPersonne addObject:[gDic[@"fullname"] emo_emojiString]];
    }
    
    
    if (arrPersonne.count > 0) {
        strPersonneName = [arrPersonne componentsJoinedByString:@", "];
        _lbPersonneDesc.text = strPersonneName;
        if (arrPersonne.count > 1) {
            _lbPersonneTitle.text = @"Personnes:";
        }
        else
        {
            _lbPersonneTitle.text = @"Personne:";

        }
    }
    
    //Involve group/chasse
    NSMutableArray *arrGroupsName =  [NSMutableArray new];
    NSString*strGroupsName = nil;
    
    for (NSDictionary*gDic in publicationDic[@"groups"]) {
        BOOL isAdd = YES;
        if ([gDic[@"access"] intValue] == 0) {
            if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                int connected_access = [gDic[@"connected"][@"access"] intValue];
                if (connected_access < 2) {
                    isAdd = NO;
                }
            }
            else
            {
                isAdd = NO;
            }
        }
        if (isAdd) {
            [arrGroupsName addObject:[gDic[@"name"] emo_emojiString]];
        }
    }
    if (arrGroupsName.count > 0) {
        strGroupsName = [arrGroupsName componentsJoinedByString:@", "];
        _lbGroupDesc.text = strGroupsName;
        if (arrPersonne.count > 1) {
            _lbGroupTitle.text = @"Groupes:";
        }
        else
        {
            _lbGroupTitle.text = @"Groupe:";
            
        }
    }
    
    
    //Chasse
    NSMutableArray *arrChassesName =  [NSMutableArray new];
    NSString*strChassesName = nil;
    
    for (NSDictionary*gDic in publicationDic[@"hunts"]) {
        BOOL isAdd = YES;
        if ([gDic[@"access"] intValue] == 0) {
            if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                int connected_access = [gDic[@"connected"][@"access"] intValue];
                if (connected_access < 2) {
                    isAdd = NO;
                }
            }
            else
            {
                isAdd = NO;
            }
        }
        if (isAdd) {
            [arrChassesName addObject:[gDic[@"name"] emo_emojiString]];
        }
    }
    
    if (arrChassesName.count > 0) {
        strChassesName = [arrChassesName componentsJoinedByString:@", "];
        _lbAgendaDesc.text = strChassesName;
        if (arrPersonne.count > 1) {
            _lbAgendaTitle.text = @"Agendas:";
        }
        else
        {
            _lbAgendaTitle.text = @"Agenda:";
            
        }
    }
    _paddingLine1.hidden = TRUE;
    _paddingLine2.hidden = TRUE;
    _paddingLine3.hidden = TRUE;
    _contraintHeight1.constant = 0;
    _contraintHeight2.constant = 0;
    _contraintHeight3.constant = 0;

    if (arrPersonne.count > 0 && (arrGroupsName.count > 0 || arrChassesName.count > 0 || isShareAmis)) {
        _paddingLine3.hidden = FALSE;
        _contraintHeight3.constant = 5;

    }
    if (arrChassesName.count > 0 && (arrGroupsName.count > 0 || isShareAmis)) {
        _paddingLine2.hidden = FALSE;
        _contraintHeight2.constant = 5;

    }
    if (arrGroupsName.count > 0 && isShareAmis) {
        _paddingLine1.hidden = FALSE;
        _contraintHeight1.constant = 5;
    }


}
#pragma mark -action
-(void)showAlertWithSuperView:(UIView*)viewSuper withPointView:(UIView*)pointView
{

    UIView *view = self;
    [viewSuper addSubview:view];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    /* Leading space to superview */
    NSLayoutConstraint *leadingConstraint = [NSLayoutConstraint
                                                 constraintWithItem:view attribute:NSLayoutAttributeLeading
                                                 relatedBy:NSLayoutRelationEqual toItem:pointView attribute:
                                                 NSLayoutAttributeLeading multiplier:1.0 constant:-5];
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint
                                             constraintWithItem:view attribute:NSLayoutAttributeBottom
                                             relatedBy:NSLayoutRelationEqual toItem:pointView attribute:
                                             NSLayoutAttributeTop multiplier:1.0 constant:22];
    [viewSuper addConstraint:leadingConstraint];
    [viewSuper addConstraint:bottomConstraint];
    
    UIView *parentView = [viewSuper superview];
    //Add button dissmiss
    _btnDismiss = [[UIButton alloc] initWithFrame:parentView.frame];
    [parentView addSubview:_btnDismiss];
    [_btnDismiss addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    _btnDismiss.tag = 11111;
    UIButton *viewButton = _btnDismiss;
    viewButton.translatesAutoresizingMaskIntoConstraints = NO;
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[viewButton]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(viewButton)]];
    
    [parentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[viewButton]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(viewButton)]];
}
-(IBAction)closeAction:(UIButton*)sender
{
    [self removeFromSuperview];
    [_btnDismiss removeFromSuperview];
}
-(void)hideAlert
{
    [self removeFromSuperview];
    [_btnDismiss removeFromSuperview];
}
-(IBAction)selectedAction:(id)sender
{
    NSInteger index = [sender tag];
    [self removeFromSuperview];
    if (_callback) {
        _callback(@{@"index":@(index)});
    }
}
@end
