//
//  AvatarViewController.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 5/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "AvatarViewController.h"
#import "AppCommon.h"
#import "AvatarColllectCell.h"

@interface AvatarViewController ()
{
    int iSelect;
}
@end

@implementation AvatarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    iSelect = -1;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    
    
    [myCollection registerNib:[UINib nibWithNibName:@"AvatarColllectCell" bundle:nil] forCellWithReuseIdentifier:@"AvatarColllectCell"];
    
    //    self.navigationItem.hidesBackButton=YES;
    
    avatarArray = @[@"avatar_1.png", @"avatar_2.png",@"avatar_3.png",@"avatar_4.png",@"avatar_5.png",@"avatar_6.png",@"avatar_7.png",@"avatar_8.png",@"avatar_9.png",@"avatar_10.png",@"avatar_11.png",@"avatar_12.png",@"avatar_13.png",@"avatar_14.png",@"avatar_15.png",@"avatar_16.png",@"avatar_17.png",@"avatar_18.png",@"avatar_19.png",@"avatar_20.png",@"avatar_21.png",@"avatar_22.png",@"avatar_23.png",@"avatar_24.png",@"avatar_25.png",@"avatar_26.png",@"avatar_27.png",@"avatar_28.png",@"avatar_29.png",@"avatar_30.png"];
    
    [myCollection reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear: animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
}

-(void)gotoBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return avatarArray.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    iSelect = (int)indexPath.row;
    
    NSString *strAvatarImage=[NSString stringWithFormat:@"%@",avatarArray[indexPath.row]];
    [self.avatarDelegate loadAvatarImage:strAvatarImage];
    [myCollection reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"AvatarColllectCell";
    
    AvatarColllectCell *cell = (AvatarColllectCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSString *strName = avatarArray[indexPath.row];
    
    [cell.imgV setImage: [UIImage imageNamed: strName ]];
    
    if (indexPath.row == iSelect) {
        cell.imgV.layer.borderWidth = 2.0;
        cell.imgV.layer.borderColor=[[UIColor redColor]CGColor];
        
    }else{
        cell.imgV.layer.borderWidth = 0.0;
        
    }
    
    return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(80, 80);
}


-(IBAction)suivantButtonClicked:(id)sender{
    //Save image
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self.avatarDelegate gotoBack];

}
#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
