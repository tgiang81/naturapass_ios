//
//  Filter_Cell_Type3.h
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_Type3 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UITextField *txtInput;

@end
