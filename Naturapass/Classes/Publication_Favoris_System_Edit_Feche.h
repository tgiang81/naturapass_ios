//
//  Publication_ChoixSpecNiv5.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"
typedef void (^EditFecheCallback)(int index,NSDictionary *dicResult);
@interface Publication_Favoris_System_Edit_Feche : PublicationBaseVC <UITableViewDelegate, UITableViewDataSource>

@property(nonatomic,strong) NSString * myName;

@property (nonatomic, strong) NSDictionary *myDic;
@property (nonatomic, strong) NSDictionary *myCard;

@property (nonatomic, assign) int iSpecific;// From Search
@property (nonatomic, strong) NSString *animalNameSpecific;
@property (nonatomic, strong) NSString *id_animal;
@property(nonatomic,assign) BOOL isEditFavo;
@property (nonatomic, strong) NSDictionary *nodeItemAttachment;
@property (nonatomic, assign) int  indexItem;

@property (nonatomic,copy) EditFecheCallback callback;
-(void)doBlock:(EditFecheCallback ) cb;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIView *viewPickerBackground;

@end
