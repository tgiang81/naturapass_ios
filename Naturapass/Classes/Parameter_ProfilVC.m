//
//  ParameterVC.m
//  Naturapass
//
//  Created by Giang on 11/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Parameter_ProfilVC.h"


#import "CellKind9.h"
#import "Parameter_MonProfile.h"
#import "Parameter_PassProfile.h"
#import "Parameter_PhotoProfile.h"

#import "Parameter_Common_Profile_Kind.h"

#import "Parameter_CommonSTEP0.h"
#import "Parameter_CommonSTEP1.h"
#import "MapDataDownloader.h"

static NSString *identifierSection1 = @"MyTableViewCell1";


#define kProfile_name1 @"Informations personnelles"
#define kProfile_name2 @"Photo de profil"
#define kProfile_name3 @"Mot de passe"
#define kProfile_name4 @"Où chassez-vous ?"
#define kProfile_name5 @"Vos papiers"
#define kProfile_name6 @"Vos armes"
#define kProfile_name7 @"Vos chiens"
#define kProfile_name8 @"Type de chasse"
#define kProfile_name9 @"Dans quels pays chassez-vous ou avez-vous chassé?"

@interface Parameter_ProfilVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation Parameter_ProfilVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strPROFIL);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name": kProfile_name1,
                                   @"image":@"ic_info_profile"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":kProfile_name2,
                                   @"image":@"ic_picture_prof"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":kProfile_name3,
                                   @"image":@"ic_change_password"} copy];
    
    
    NSMutableDictionary *dic4 = [@{@"name":kProfile_name4,
                                   @"image":@"ic_profile_city"} copy];
    
    NSMutableDictionary *dic9 = [@{@"name":kProfile_name5,
                                   @"image":@"param_ic_paper"} copy];
    
    NSMutableDictionary *dic8 = [@{@"name":kProfile_name6,
                                   @"image":@"param_ic_gun"} copy];
    
    NSMutableDictionary *dic5 = [@{@"name":kProfile_name7,
                                   @"image":@"param_ic_dog"} copy];
    
    NSMutableDictionary *dic6 = [@{@"name":kProfile_name8,
                                   @"image":@"ic_type_de_chasse"} copy];
    
    NSMutableDictionary *dic7 = [@{@"name":kProfile_name9,
                                   @"image":@"ic_profile_country"} copy];

    
    arrData =  [@[dic1,dic2,dic3,dic4,dic9,dic8,dic5,dic6,dic7] copy];

    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind9" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    [[MapDataDownloader sharedInstance] doUpdateFavorites];
    [[MapDataDownloader sharedInstance] doUpdateDogsWeapon];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind9 *cell = nil;
    
    cell = (CellKind9 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.arrow setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
            NSDictionary*dic = arrData[indexPath.row];
            
            if ([dic[@"name"] isEqualToString:kProfile_name1]) {
                Parameter_MonProfile *viewController1 = [[Parameter_MonProfile alloc] initWithNibName:@"Parameter_MonProfile" bundle:nil];
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name2]) {
                Parameter_PhotoProfile *viewController1 = [[Parameter_PhotoProfile alloc] initWithNibName:@"Parameter_PhotoProfile" bundle:nil];
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name3]) {
                Parameter_PassProfile *viewController1 = [[Parameter_PassProfile alloc] initWithNibName:@"Parameter_PassProfile" bundle:nil];
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name4]) {
                //ou chassez vouz
                Parameter_CommonSTEP1 *viewController1 = [[Parameter_CommonSTEP1 alloc] initWithNibName:@"Parameter_CommonSTEP1" bundle:nil];
                viewController1.myTypeVview = CITY;
                
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name7]) {
                Parameter_Common_Profile_Kind *viewController1 = [[Parameter_Common_Profile_Kind alloc] initWithNibName:@"Parameter_Common_Profile_Kind" bundle:nil];
                viewController1.myTypeVview = TYPE_DOG;
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name8]) {
                Parameter_CommonSTEP0 *viewController1 = [[Parameter_CommonSTEP0 alloc] initWithNibName:@"Parameter_CommonSTEP0" bundle:nil];
                viewController1.myTypeVview = TYPE_CHASSE;
                
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name9]) {
                Parameter_CommonSTEP1 *viewController1 = [[Parameter_CommonSTEP1 alloc] initWithNibName:@"Parameter_CommonSTEP1" bundle:nil];
                viewController1.myTypeVview = COUNTRY;
                
                [self pushVC:viewController1 animate:YES];
                
            }else if ([dic[@"name"] isEqualToString:kProfile_name6]) {
                Parameter_Common_Profile_Kind *viewController1 = [[Parameter_Common_Profile_Kind alloc] initWithNibName:@"Parameter_Common_Profile_Kind" bundle:nil];
                viewController1.myTypeVview = TYPE_WEAPONS;
                [self pushVC:viewController1 animate:YES];
            }
            else if ([dic[@"name"] isEqualToString:kProfile_name5]) {
                Parameter_Common_Profile_Kind *viewController1 = [[Parameter_Common_Profile_Kind alloc] initWithNibName:@"Parameter_Common_Profile_Kind" bundle:nil];
                viewController1.myTypeVview = TYPE_PAPER;
                [self pushVC:viewController1 animate:YES];
            }
}

@end
