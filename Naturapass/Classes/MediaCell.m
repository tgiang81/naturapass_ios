//
//  MediaCell.m
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MediaCell.h"
#import "AppCommon.h"
#import "OHASBasicHTMLParser.h"
#import "ASSharedTimeFormatter.h"
#import "NSString+Extensions.h"
#import "Define.h"
#import "MediaCell+Color.h"
#import "NSString+HTML.h"
#import "NSString+EMOEmoji.h"
#import "UILabel+Copyable.h"
@implementation MediaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.viewFileOpen.layer setMasksToBounds:YES];
    self.viewFileOpen.layer.cornerRadius= 4.0;
    self.viewFileOpen.layer.borderWidth =0.5;
    self.viewFileOpen.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    

    // Initialization code
    [self.mainView.layer setMasksToBounds:YES];
    self.mainView.layer.cornerRadius= 4.0;
    self.mainView.layer.borderWidth =0.5;
    self.mainView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.imageProfile.layer setMasksToBounds:YES];
    self.imageProfile.layer.cornerRadius=  30;

    self.imageProfile.layer.borderWidth =0;
    

    //Reverse
    self.viewArrowComment.hidden = YES;
    self.btnInfo.hidden =YES;
    //
    self.myContent.delegate = self;
    
    [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnLike setTitleColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR) forState:UIControlStateSelected];
    //
//    [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_like"] forState:UIControlStateSelected];
//    [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_unlike"] forState:UIControlStateNormal];
    //
    [self.imgBackGroundSetting setImage:[UIImage imageNamed:@"bg_setting_post"]];
    
    [self.imgSettingSelected setImage:[UIImage imageNamed:@"ic_admin_setting"]];
    [self.imgSettingNormal setImage:[UIImage imageNamed:@"mur_ic_action_setting_post"]];
    
    self.myContent.copyingEnabled = YES;
//    NSDictionary *attrs = @{ NSForegroundColorAttributeName : UIColorFromRGB(MUR_MAIN_BAR_COLOR),NSFontAttributeName: self.myContent.font};
//    self.myContent.truncationTokenStringAttributes = attrs;
//    self.myContent.truncationTokenString = @"...Voir plus";
    self.imgBgMore.layer.masksToBounds = YES;
    self.imgBgMore.layer.cornerRadius = 15;
    self.imgBgMore.backgroundColor = UIColorFromRGB(0x8394A4);
    self.viewMenu.backgroundColor = UIColorFromRGB(0x8394A4);
    self.viewLocation.backgroundColor = UIColorFromRGB(0xE1E4E8);
    self.viewRedirect.layer.masksToBounds = YES;
    self.viewRedirect.layer.cornerRadius = 4;
}
//publier_option_bg
- (void)layoutSubviews
{
    [super layoutSubviews];
}

-(void) youJustClickOnText
{
    if (self.cbClickText) {
        self.cbClickText(self.btnTextComment);
    }
    
}

- (BOOL)stringContainsEmoji:(NSString *)string {
    __block BOOL returnValue = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         
         const unichar hs = [substring characterAtIndex:0];
         // surrogate pair
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     returnValue = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 returnValue = YES;
             }
             
         } else {
             // non surrogate
             if (0x2100 <= hs && hs <= 0x27ff) {
                 returnValue = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 returnValue = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 returnValue = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 returnValue = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                 returnValue = YES;
             }
         }
     }];
    
    return returnValue;
}

-(void)assignValues:(NSDictionary *)publicationDic{
    
    ASLog(@"%@", publicationDic);
    
    NSString *strUserName=[NSString stringWithFormat:@"%@ %@", CHECKSTRING(publicationDic[@"owner"][@"firstname"]), CHECKSTRING(publicationDic[@"owner"][@"lastname"]) ];
    NSInteger commentsCount = 0;
    
    if (publicationDic[@"totalComments"] != nil) {
        commentsCount = [publicationDic[@"totalComments"] integerValue ];
        
    }else{
        commentsCount = [publicationDic[@"comments"][@"total"] integerValue ];
    }
    
    NSString *strComment=[NSString stringWithFormat:@"%ld",(long)commentsCount];

    //TO EMOJI
    NSString *messageContent;
    if ([publicationDic[@"content"] isKindOfClass:[NSString class]]) {
        messageContent = [publicationDic[@"content"] emo_emojiString];
    }
    else{
        messageContent = @"";
    }
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] timeFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy H:mm" forFormatter: outputFormatterBis];
    
    NSString *relativeTime = [publicationDic valueForKey:@"created"];
    NSDate * inputDate = [ inputFormatter dateFromString:relativeTime ];
    NSString * outputString = [ outputFormatterBis stringFromDate:inputDate ];
    
    [self.numComment setText: strComment];
    if (commentsCount>1) {
        self.lbComment.text =@"commentaires";
    }
    else
    {
        self.lbComment.text =@"commentaire";
        
    }
    
    /*
     geolocation =     {
     created = "2015-07-24T05:34:38+0200";
     id = 45378;
     latitude = "0.000000";
     longitude = "0.000000";
     };
     */
    
    if ([publicationDic[@"sharing"] isKindOfClass: [NSDictionary class]])
    {
        int iShare = [publicationDic[@"sharing"][@"share"] intValue];
        
        NSString *strShare = nil;
        
        switch (iShare) {
            case 0:
                strShare = str(strMoi);
                
                break;
            case 1:
                strShare = str(strAAmis);
                
                break;
            case 3:
                strShare = str(strTous_les_natiz);
                
                break;
                
            default:
                break;
        }
        
        //Amis:
//        sharing =     {
//            share = 1;
//            withouts =         (
//            );
//        };
        
        BOOL isShareAmis = NO;
        
        if ([publicationDic[@"sharing"] isKindOfClass: [NSDictionary class]]) {
            if ( [publicationDic[@"sharing"][@"share"] intValue] == 1 ) {
                //share AMIS
                isShareAmis = YES;
            }
        }
        
        //personne
        NSMutableAttributedString *attShare = [NSMutableAttributedString new];
        NSMutableArray *arrPersonne =  [NSMutableArray new];
        NSString*strPersonneName = nil;
        
        for (NSDictionary*gDic in publicationDic[@"shareusers"]) {
            [arrPersonne addObject:[gDic[@"fullname"] emo_emojiString]];
        }

        if (arrPersonne.count > 0) {
            [attShare appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
            
            strPersonneName = [arrPersonne componentsJoinedByString:@", "];
            
            NSMutableAttributedString *mGroup = [[NSMutableAttributedString alloc] initWithString:@"Personne(s): "];
            
            [mGroup addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:13.0f]
                           range:arrPersonne.count >1?NSMakeRange(0, 12):NSMakeRange(0, 9)];
            [mGroup appendAttributedString: [ NSMutableAttributedString attributedStringWithString:strPersonneName]];
            
            [attShare appendAttributedString:mGroup];
        }
        
        //Involve group/chasse
        NSMutableArray *arrGroupsName =  [NSMutableArray new];
        NSString*strGroupsName = nil;
        
        for (NSDictionary*gDic in publicationDic[@"groups"]) {
            BOOL isAdd = YES;
            if ([gDic[@"access"] intValue] == 0) {
                if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                   int connected_access = [gDic[@"connected"][@"access"] intValue];
                    if (connected_access < 2) {
                        isAdd = NO;
                    }
                }
                else
                {
                    isAdd = NO;
                }
            }
            if (isAdd) {
                [arrGroupsName addObject:[gDic[@"name"] emo_emojiString]];
            }
        }
        if (arrGroupsName.count > 0) {
            [attShare appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
            
            strGroupsName = [arrGroupsName componentsJoinedByString:@", "];
            
            NSMutableAttributedString *mGroup = [[NSMutableAttributedString alloc] initWithString:@"Groupe(s): "];
            
            [mGroup addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:13.0f]
                           range:NSMakeRange(0, 10)];
            [mGroup appendAttributedString: [ NSMutableAttributedString attributedStringWithString:strGroupsName]];
            
            [attShare appendAttributedString:mGroup];
        }
        
        
        //Chasse
        NSMutableArray *arrChassesName =  [NSMutableArray new];
        NSString*strChassesName = nil;
        
        for (NSDictionary*gDic in publicationDic[@"hunts"]) {
            BOOL isAdd = YES;
            if ([gDic[@"access"] intValue] == 0) {
                if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                    int connected_access = [gDic[@"connected"][@"access"] intValue];
                    if (connected_access < 2) {
                        isAdd = NO;
                    }
                }
                else
                {
                    isAdd = NO;
                }
            }
            if (isAdd) {
                [arrChassesName addObject:[gDic[@"name"] emo_emojiString]];
            }
        }
        
        if (arrChassesName.count > 0) {
            [attShare appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
            
            strChassesName = [arrChassesName componentsJoinedByString:@", "];
            
            NSMutableAttributedString *mHunt = [[NSMutableAttributedString alloc] initWithString:@"Agenda(s): "];
            
            [mHunt addAttribute:NSFontAttributeName
                          value:[UIFont boldSystemFontOfSize:13.0f]
                          range:NSMakeRange(0, 10)];
            [mHunt appendAttributedString: [ NSMutableAttributedString attributedStringWithString:strChassesName]];
            
            [attShare appendAttributedString:mHunt];
        }
        self.btnShareDetail.hidden = TRUE;
        //Receiver
        //observation
        NSArray *arrObservation = publicationDic[@"observations"];
        NSMutableArray *arrReceiverName =  [NSMutableArray new];

        if (arrObservation.count > 0)
        {
            NSDictionary*dicObservation = arrObservation[0];
            
            NSArray*arrSharingReciever = dicObservation[@"sharing_receiver"];
            //Receiver
            NSString*strReceiverName = nil;
            
            for (NSDictionary*gDic in arrSharingReciever) {
                [arrReceiverName addObject:[gDic[@"name"] emo_emojiString]];
            }
            
            if (arrReceiverName.count > 0) {
                [attShare appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
                
                strReceiverName = [arrReceiverName componentsJoinedByString:@", "];
                
                NSMutableAttributedString *mReceiver = [[NSMutableAttributedString alloc] initWithString:@"Sentinelle: "];
                
                [mReceiver addAttribute:NSFontAttributeName
                                  value:[UIFont boldSystemFontOfSize:13.0f]
                                  range:NSMakeRange(0, 11)];
                [mReceiver appendAttributedString: [ NSMutableAttributedString attributedStringWithString:strReceiverName]];
                
                [attShare appendAttributedString:mReceiver];
            }
        }
        NSMutableAttributedString *attString = [NSMutableAttributedString new];
        _imgIconShare.image = [UIImage imageNamed:@"ic_mur_share"];
        if (arrPersonne.count > 0 || arrGroupsName.count > 0 || arrChassesName.count > 0 || isShareAmis) {
            //if the sharing is complicated (groups / hunts / other people) we'll put "Autres..."
            [self.shareType setText:@"Autres"];
            self.btnShareDetail.hidden = FALSE;
        }
        else
        {
            if (iShare == 0) {
                _imgIconShare.image = [UIImage imageNamed:@"ic_non_share_type"];
                NSMutableAttributedString *mNonShare = [[NSMutableAttributedString alloc] initWithString:@"Non partagée"];
                [mNonShare addAttribute:NSFontAttributeName
                                  value:[UIFont systemFontOfSize:13.0f]
                                  range:NSMakeRange(0, 11)];
                [attString appendAttributedString:mNonShare];
                [self.shareType setAttributedText:attString];
                
            }
            else
            {
                [attString appendAttributedString: [ NSMutableAttributedString attributedStringWithString:strShare]];
                [self.shareType setAttributedText:attString];
            }
        }

    }
    
    //observation tree
    self.btnObs.hidden = YES;
    [self.btnObs addTarget:self action:@selector(readmoreObs:) forControlEvents:UIControlEventTouchUpInside];
    //observation
    NSArray *arrObservation = publicationDic[@"observations"];
    
    if (arrObservation.count > 0)
    {
        
        UIColor *color;
        
        //value
        switch (self.iWhoIamI) {
                
            case ISLOUNGE:
            {
                color =  UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            }
                break;
                
            case ISGROUP:
            {
                color =  UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
                
            }
                break;
            case ISLIVEMAP:
            {
                color = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                
            }
                break;
                
            default:
            {
                color =  UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            }
                break;
        }
        
        [self.observation setTextColor:color];
        
        NSDictionary*dicObservation = arrObservation[0];
        
        NSArray*arrTree = dicObservation[@"tree"];
        
        NSMutableString *mutString = [NSMutableString new];
        
        for (int i = 0;  i< arrTree.count; i++) {
            [mutString appendString:[arrTree[i] emo_emojiString]];
            
            if (i != arrTree.count - 1) {
                [mutString appendString:@" / "];
            }
        }
        
        self.constraintHeightObservationIcon.constant = 15;
        [self.observation setText:mutString];
        self.obs_label_static.hidden = NO;
        
        NSArray*arrAttachment = dicObservation[@"attachments"];
//        if ([publicationDic[@"readobs"] boolValue]) {
        if (!_isMurView) {
            if (arrAttachment.count > 0)
            {
                NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
                
                for (int i = 0; i < arrAttachment.count; i++) {
                    
                    NSDictionary*attchDic =  arrAttachment[i];
                    
                    UIColor *color = [UIColor blackColor]; // select needed color
                    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
                    
                    NSString *strLabel = [NSString stringWithFormat:@"%@: ",attchDic[@"label"] ];
                    NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:strLabel attributes:attrs];
                    
                    //Add label
                    [mAttach appendAttributedString:mLabel];
                    
                    //value
                    switch (self.iWhoIamI) {
                            
                        case ISLOUNGE:
                        {
                            color =  UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                        }
                            break;
                            
                        case ISGROUP:
                        {
                            color =  UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
                            
                        }
                            break;
                        case ISLIVEMAP:
                        {
                            color = [UIColor redColor];
                            
                        }
                            break;
                            
                        default:
                        {
                            color =  UIColorFromRGB(MUR_MAIN_BAR_COLOR);
                        }
                            break;
                    }
                    
                    /*
                     {
                     label = "type 30";
                     values =     (
                     "select type 2"
                     );
                     }
                     */
                    NSDictionary *attrsVal = @{ NSForegroundColorAttributeName : color };
                    
                    //I send "values" when it's an array and value when it's string
                    
                    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:@""];
                    
                    
                    if (attchDic[@"value"])
                    {
                        [mValue appendAttributedString: [NSAttributedString attributedStringWithString:  [attchDic[@"value"] emo_emojiString]] ];
                    }
                    else if (attchDic[@"values"])
                    {
                        NSArray *mArr = attchDic[@"values"];
                        
                        for (int i = 0; i < mArr.count; i++) {
                            [mValue appendAttributedString: [NSAttributedString attributedStringWithString: [attchDic[@"values"][i] emo_emojiString] ] ];
                            if (mArr.count > 1 && i < mArr.count-1) {
                                [mValue appendAttributedString: [NSAttributedString attributedStringWithString: @", " ] ];
                            }
                        }
                    }
                    
                    NSMutableAttributedString *strValue = [[NSMutableAttributedString alloc] initWithString:mValue.string attributes:attrsVal];
                    
                    [mAttach appendAttributedString:strValue];
                    
                    if (i != arrAttachment.count - 1) {
                        [mAttach appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
                    }
                }
                
                [self.obs_tree_content setAttributedText:mAttach];
                self.imageLeftIndice.hidden = NO;
            }
            else
            {
                [self.obs_tree_content setText:@""];
                self.imageLeftIndice.hidden = YES;
            }
            
        }
        else
        {
            [self.obs_tree_content setText:@""];
            self.imageLeftIndice.hidden = YES;
        }

        if (arrTree.count == 0 && arrAttachment.count == 0)
        {
            self.ongNhom.hidden = YES;
            self.obs_label_static.hidden = YES;
            self.btnObs.hidden = YES;

        }
        else
        {
            self.ongNhom.hidden = NO;
            self.obs_label_static.hidden = NO;
            self.btnObs.hidden = NO;
        }
    }else{
        self.constraintHeightObservationIcon.constant = 0;
        self.obs_label_static.hidden = YES;
        self.imageLeftIndice.hidden = YES;

        self.obs_label_static.hidden = YES;
        self.ongNhom.hidden = YES;

        
        [self.observation setText:@""];
        [self.obs_tree_content setText:@""];
        self.btnObs.hidden = YES;
    }
    NSString *strLat = @"";
    NSString *strLon = @"";
    NSString *strAlt = @"";
    NSString *strAdress = @"";
    if ([[publicationDic valueForKey:@"geolocation"] isKindOfClass: [NSDictionary class] ]) {
        strLat = [ NSString stringWithFormat:@"Lat. %.5f" , [publicationDic[@"geolocation"] [@"latitude"] floatValue] ];
        strLon = [ NSString stringWithFormat:@"Long. %.5f" , [ publicationDic[@"geolocation"] [@"longitude"] floatValue]] ;
        
        if ( ![publicationDic[@"geolocation"] [@"altitude"] isKindOfClass: [NSNull class]]) {
            if ([publicationDic[@"geolocation"] [@"altitude"] doubleValue] > 0) {
                strAlt = [ NSString stringWithFormat:@"Alt. %.5f" , [publicationDic[@"geolocation"] [@"altitude"] floatValue ]];
            }

        }
        
        
        if (publicationDic[@"geolocation"][@"address"] != nil &&
            ![publicationDic[@"geolocation"][@"address"]  isEqual:@"<null>"] ) {
            strAdress = publicationDic[@"geolocation"][@"address"];
            
        }
    }
    if (strAdress.length > 0 || strLat.length > 0) {
        NSMutableString *strTmpAddress = [NSMutableString new];
        [strTmpAddress appendString:strAdress];
        if (strLat.length > 0) {
            if (strAdress.length > 0) {
                [strTmpAddress appendString:@"\n"];
            }
            [strTmpAddress appendString:strLat];
        }
        if (strLon.length > 0) {
            [strTmpAddress appendString:@"  "];
            [strTmpAddress appendString:strLon];
        }
        if (strAlt.length > 0) {
            [strTmpAddress appendString:@"\n"];
            [strTmpAddress appendString:strAlt];
        }
        self.locationTxt.text = strTmpAddress;
        //hidden..
        self.viewLocation.hidden = NO;
        self.contraintGeoHeight.active = TRUE;
    }
    else
    {
        self.locationTxt.text = @"";
        //hidden..
        self.viewLocation.hidden = TRUE;
        self.contraintGeoHeight.active = FALSE;
    }
    if (outputString) {
        NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];
        [self.time setText: strDateLocation];
    }else{
        [self.time setText: @""];
    }
    
    [self.title setText:strUserName];
    
    self.myContent.htmlText = messageContent;
    if ([publicationDic[@"isUserLike"] integerValue] == 1) {
                self.imageLike.image = [UIImage imageNamed:@"ic_mur_love"];
        //        [self.btnLike setTitleColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR) forState:UIControlStateNormal];
        self.btnLike.selected=YES;
        self.btnIconLike.selected=YES;
        
    }else{
        self.btnLike.selected=NO;
        self.btnIconLike.selected=NO;
        
        
                self.imageLike.image = [UIImage imageNamed:@"ic_mur_love_inactive"];
        //        [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
    }
    
    [self.btnLike setSelected:[publicationDic[@"isUserLike"] integerValue]==1];
    [self menuListwithPublication:publicationDic];
    [self showUserLikePublication:publicationDic];
}
-(IBAction)readmoreObs:(id)sender
{
    if (self.cbClickObs) {
        self.cbClickObs(self.btnObs);
    }
}
-(void)showUserLikePublication:(NSDictionary*)dic
{
        self.btnLikesList.hidden = NO;
        NSInteger numberLike = 0;
        if ([dic[@"likes"] isKindOfClass:[NSDictionary class]]) {
            numberLike = [dic[@"likes"][@"total"] integerValue];

        }
        else
        {
           numberLike = [dic[@"likes"] integerValue];

        }
        NSString *nameUserLike = @"";
        NSString *moreNumberUser = @"";
        /*
         if nobody = "Soyez le premier à aimer cette publication"
         if 1 = "Phat aime cette publication"
         if >1 <=3 = "Phat, Giang aiment cette publication"
         if 4 = "Phat, Giang, Mathis et une autre personne aime cette publication"
         if >4 = "Phat, Giang, Mathis et n autres personnes aiment cette publication"
         */
        NSArray *arrUserLike = dic[@"likedusers"];
        NSMutableArray *arrListName = [NSMutableArray new];
        for (NSDictionary *dicUserLike in arrUserLike) {
//            NSString *strUserName=[NSString stringWithFormat:@"%@ %@", CHECKSTRING(dicUserLike[@"first_name"]), CHECKSTRING(dicUserLike[@"last_name"]) ];
            NSString *strUserName=[NSString stringWithFormat:@"%@",CHECKSTRING(dicUserLike[@"last_name"]) ];

            [arrListName addObject:strUserName];
        }
        nameUserLike = [arrListName componentsJoinedByString:@", "];
        
        if (numberLike > 4) {
            moreNumberUser = [NSString stringWithFormat:@" et %ld autres personnes aiment cette publication",numberLike - 3];
        }
        else if(numberLike == 4)
        {
            moreNumberUser = @" et une autre personne aime cette publication";
        }
        else if(numberLike > 1 && numberLike <= 3)
        {
            moreNumberUser = @" aiment cette publication";
        }
        else if(numberLike == 1)
        {
            moreNumberUser = @" aime cette publication";
        }
        else
        {
            moreNumberUser = @"Soyez le premier à aimer cette publication";
            self.btnLikesList.hidden = YES;
        }
        NSDictionary *attrs = @{ NSForegroundColorAttributeName : UIColorFromRGB(0x8896A5),NSFontAttributeName: FONT_HELVETICANEUE_BOLD(13)};
        NSMutableAttributedString *mNameUserLike = [[NSMutableAttributedString alloc] initWithString:nameUserLike attributes:attrs];
        
        NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : UIColorFromRGB(0xA5AFBC),NSFontAttributeName: FONT_HELVETICANEUE(13)};
        NSMutableAttributedString *mMoreNumberUser = [[NSMutableAttributedString alloc] initWithString:moreNumberUser attributes:attrs1];
        
        NSMutableAttributedString *mFull = [NSMutableAttributedString new];
        [mFull appendAttributedString:mNameUserLike];
        [mFull appendAttributedString:mMoreNumberUser];
        [mFull addAttribute:NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:1]
                      range:(NSRange){0,[mFull length]}];
        self.lbUserLikePub.attributedText = mFull;
    
    NSString *strLikes = [NSString stringWithFormat:@"%ld",numberLike];
    [self.numLike setText:strLikes];
}
-(void)setThemeWithFromScreen:(ISSCREEN)isScreen
{
    self.iWhoIamI = isScreen;
    
    if (isScreen ==ISLOUNGE) {
        [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnLike setTitleColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR) forState:UIControlStateSelected];
        //
//        [self.btnIconLike setImage:[UIImage imageNamed:@"chasse_ic_like"] forState:UIControlStateSelected];
//        [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_unlike"] forState:UIControlStateNormal];
        
        [self fnSetTheme:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
        [self.imgSetting setImage: [UIImage imageNamed:@"ic_chasse_setting_active"]];
        [self.btnInfo setBackgroundImage:[UIImage imageNamed:strIC_chasse_info] forState:UIControlStateNormal];
        
    }
    else if (isScreen ==ISGROUP) {
        [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnLike setTitleColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR) forState:UIControlStateSelected];
        //
//        [self.btnIconLike setImage:[UIImage imageNamed:@"group_ic_like"] forState:UIControlStateSelected];
//        [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_unlike"] forState:UIControlStateNormal];
        
        [self fnSetTheme:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
        [self.btnInfo setBackgroundImage:[UIImage imageNamed:strIC_group_setting_info] forState:UIControlStateNormal];
        [self.imgSetting setImage: [UIImage imageNamed:@"ic_group_setting_active"]];
    }
   else if (isScreen ==ISLIVEMAP) {
        [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnLike setTitleColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) forState:UIControlStateSelected];
        //
//        [self.btnIconLike setImage:[UIImage imageNamed:@"live_ic_like"] forState:UIControlStateSelected];
//        [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_unlike"] forState:UIControlStateNormal];
       
        [self fnSetTheme:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
        [self.imgSetting setImage: [UIImage imageNamed:@"live_ic_setting"]];
        [self.btnInfo setBackgroundImage:[UIImage imageNamed:strIC_chasse_info] forState:UIControlStateNormal];

    }
    
    else
    {
        [self.btnLike setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.btnLike setTitleColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR) forState:UIControlStateSelected];
        //
//        [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_like"] forState:UIControlStateSelected];
//        [self.btnIconLike setImage:[UIImage imageNamed:@"mur_ic_post_unlike"] forState:UIControlStateNormal];
        [self.btnInfo setBackgroundImage:[UIImage imageNamed:@"mur_ic_info"] forState:UIControlStateNormal];

    }
}
@end
