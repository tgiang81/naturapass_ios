//
//  MediaCell.h
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"
#import "Define.h"
#import "SOLabel.h"
#import "ImageViewAction.h"
#import "AppCommon.h"
#import "MDHTMLLabel.h"

typedef void(^onClickText)(UIButton*theBtn);

@interface ChatLiveHuntBaseCell : UITableViewCell <OHAttributedLabelDelegate>

@property (nonatomic, copy) onClickText cbClickText;


@property (weak, nonatomic) IBOutlet UIImageView *imageProfile;
@property (weak, nonatomic) IBOutlet UIImageView *img_chat_pub;


@property (weak, nonatomic) IBOutlet SOLabel *observation;


@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *lbHeader;

@property (weak, nonatomic) IBOutlet UILabel *timeCreate;


@property (nonatomic,retain) IBOutlet MDHTMLLabel        *myContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightObservationIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightHeader;

@property (weak, nonatomic) IBOutlet UILabel *obs_label_static;

@property (weak, nonatomic) IBOutlet UILabel *obs_tree_content;

@property (weak, nonatomic) IBOutlet UIButton *btnDetail;
@property (weak, nonatomic) IBOutlet UIView *lbLeftIndice;
@property (weak, nonatomic) IBOutlet ImageViewAction *imageContent;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightImage;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contraintPaddingDesctionTime;

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgCicle;

-(void)assignValues:(NSDictionary *)liveHuntDic;
-(void)setThemeWithFromScreen:(ISSCREEN)isScreen;
@end
