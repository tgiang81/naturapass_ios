//
//  Filter_Cell_Type1.h
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_Type1 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UISwitch *swStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;

@end
