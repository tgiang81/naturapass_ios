//
//  GroupCreate_Step1.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ParameterBaseVC.h"
#import "DPTextField.h"

@interface Parameter_Common_Profile_Kind_Change : ParameterBaseVC
{        
        //textfield
    IBOutlet UITextField    *nameTextField;
    IBOutlet UITextField    *raceTextField;
    IBOutlet UITextField    *sexTextField;
    IBOutlet UITextField    *typeTextField;
    IBOutlet UITextField    *documtentTextField;
    IBOutlet UIView         *viewChoose;
    IBOutlet UIView         *viewBirthDay;

}
@property(nonatomic,assign) BOOL isModifi;
@property(nonatomic,strong) NSDictionary *dicModifi;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBirthDay;

@end
