//
//  PhotoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PhotoSheetSignUpVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "AvatarViewController.h"
#import "AppDelegate.h"
@interface PhotoSheetSignUpVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate,AVATARVIEWDELEGATE>
{
    IBOutlet UIButton *btnAvatar;
    IBOutlet UIButton *btnCamera;
    IBOutlet UIButton *btnLibrary;
    IBOutlet UIButton *btnCancel;


}

@end

@implementation PhotoSheetSignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [btnAvatar   setTitle:str(strChoose_Avatar) forState:UIControlStateNormal];
    [btnCamera   setTitle:str(strTake_Photo) forState:UIControlStateNormal];
    [btnLibrary  setTitle:str(strChoose_Library) forState:UIControlStateNormal];
    [btnCancel   setTitle:str(strCancel) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)avatarAction:(id)sender {
    AvatarViewController *avatarVC=[[AvatarViewController alloc]initWithNibName:@"AvatarViewController" bundle:nil];
    avatarVC.avatarDelegate=self;
    [self presentViewController:avatarVC animated:NO completion:^{}];

}
-(void)loadAvatarImage:(NSString *)_strImage{
    if (self.myCallback) {
        self.myCallback(@{@"image":[UIImage imageNamed:_strImage],@"name":@"AVATARIMAGE",@"nameAvatar":_strImage});
    }
}
-(void)gotoBack
{
    [self onCancel:nil];
}
- (IBAction)onTakePhoto:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorry_No_Camera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
        [altView show];
        return;
    }
    [self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];

}

- (IBAction)onTakeLibrary:(id)sender {
    [self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}


- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType
{
    //iPhone
    UIImagePickerController *imagePicker;
    
    NSArray *mediaTypes = [UIImagePickerController
                           availableMediaTypesForSourceType:sourceType];
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.mediaTypes = mediaTypes;
    imagePicker.delegate = (id)self;
    //    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = sourceType;
    
    [self presentViewController:imagePicker animated:NO completion:Nil];

}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}


- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data

    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    CLLocation * loc = app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

#pragma mark  -  PICKER DELEGATE

// FIXME: Rewrite me!

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // FIXME: USE NSSTRING CONSTANT NOT STRING VALUE: UIImagePickerControllerOriginalImage
    
    if([info objectForKey:@"UIImagePickerControllerOriginalImage"])
    {
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            UIImage *tmpimage = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                
//                [self saveImage:tmpimage withInfo:info];
            }
            void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *) = ^(ALAsset *asset)
            {
                NSDictionary *metadata = asset.defaultRepresentation.metadata;
                ASLog(@"Image Meta Data: %@",metadata);
                NSNumber *latNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Latitude"];
                NSNumber *lonNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Longitude"];
                
                if (latNum && lonNum) {
                    NSDictionary *retDic = @{@"image":tmpimage,
                                             @"Latitude":latNum,
                                             @"Longitude":lonNum,
                                             @"name":@"USERIMAGES",
                                             @"nameAvatar":@""};
                    self.myCallback(retDic);
                }else{
                    self.myCallback(@{@"image":tmpimage,@"name":@"USERIMAGES",@"nameAvatar":@""});
                }

                [self onCancel:nil];
            };
            
            NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
            //            NSURL *imageDataURL=[NSURL URLWithString:@"assets-library" ];
            //            [[UIApplication sharedApplication] openURL:imageDataURL];
            [library assetForURL:assetURL
                     resultBlock:ALAssetsLibraryAssetForURLResultBlock
                    failureBlock:^(NSError *error) {
                    }];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [self onCancel:nil];

    [self dismissViewControllerAnimated: YES completion: NULL];
}

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
