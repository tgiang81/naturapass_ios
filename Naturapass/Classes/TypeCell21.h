//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"

@interface TypeCell21: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;

//image
@property (nonatomic,retain) IBOutlet UIImageView       *img1;

//label
@property (nonatomic,retain) IBOutlet UILabel           *label1;

//button
@property (nonatomic,retain) IBOutlet UIButton          *button1;

//funtion
-(void)fnSettingCell:(int)type;
@end
