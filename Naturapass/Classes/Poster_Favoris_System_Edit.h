//
//  Publication_Favoris_System.h
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PosterBaseVC.h"

@interface Poster_Favoris_System_Edit : PosterBaseVC
@property (nonatomic, retain) NSMutableArray *displayArray;
-(void)refreshCard;
@end
