//
//  MurParameter_PublicationFav.m
//  Naturapass
//
//  Created by Giang on 3/8/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MurParameter_PublicationFav.h"
#import "CellKind8.h"
#import "DatabaseManager.h"
#import "MapDataDownloader.h"
#import "NSString+HTML.h"


static NSString *identifierSection1 = @"MyTableViewCell1";

@interface MurParameter_PublicationFav ()
{
    NSMutableArray * arrData;
    __weak IBOutlet UILabel *warning;
    
}

@end

@implementation MurParameter_PublicationFav

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:UPDATE_OBJ_NOTIFICATION object:nil];
    
    arrData = [NSMutableArray new];
    warning.hidden = YES;
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind8" bundle:nil] forCellReuseIdentifier:identifierSection1];
    // get offset favorite publication
    
    //Refresh/loadmore
    [self initRefreshControl];
    
    [self startRefreshControl];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
}

-(void) refreshData
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        [COMMON removeProgressLoading];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrData removeAllObjects];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrData addObject:objDic];
        }
        
        [self.tableControl reloadData];
        
        
        if ([COMMON isReachable]) {
            
            if (arrData.count == 0) {
                warning.hidden = NO;
                
                warning.text = str(strMessage18);
                
            }else{
                
                warning.hidden = YES;
            }
            
        }
        
    }];
    
}

#pragma mark - REFRESH/LOAD MORE
//overwrite
- (void)insertRowAtTop {
    
    [self refreshData];
    
    [self stopRefreshControl];
    [self checkEmpty];
}

- (void)insertRowAtBottom {
    [self refreshData];
    
    [self stopRefreshControl];
    [self checkEmpty];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind8 *cell = nil;
    
    cell = (CellKind8 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: @"mur_ic_publication_fav_detail"];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.label1.numberOfLines=2;
    
    [cell.btnDel addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDel.tag =indexPath.row;
    //Arrow
    [cell.rightIcon setImage: [UIImage imageNamed:@"mur_st_ic_delete" ]];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


-(IBAction)deleteAction:(id)sender
{
    NSInteger index = [sender tag];
    [UIAlertView showWithTitle:str(strSuppression_favori)
                       message:str(strSupprimer_ce_favori_de_vos_publications_favorites)
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[str(strNon)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSDictionary *dic = arrData[index];
                              
                              [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
                              
                              [COMMON addLoading:self];
                              
                              WebServiceAPI *serverAPI =[WebServiceAPI new];
                              serverAPI.onComplete = ^(id response, int errCode)
                              {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      [COMMON removeProgressLoading];
                                      
                                      [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                                      
                                      if ([response[@"success"] isKindOfClass: [NSNumber class]] && [response[@"success"] intValue] == 1 )
                                      {
                                          [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                                          
                                          [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                              NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                                              NSString *strInsertTblVersion = [ NSString stringWithFormat: @"DELETE FROM `tb_favorite` WHERE `c_id` IN (%@) AND `c_user_id` = '%@';", dic[@"id"], sender_id ] ;
                                              //add version number to tble version.
                                              
                                              [db  executeUpdate:strInsertTblVersion];
                                              
                                              NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
                                              
                                              FMResultSet *set_querry = [db  executeQuery:strQuerry];
                                              
                                              [arrData removeAllObjects];
                                              
                                              while ([set_querry next])
                                              {
                                                  //title/id
                                                  NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                                                            @"id":[set_querry stringForColumn:@"c_id"]
                                                                            };
                                                  [arrData addObject:objDic];
                                              }
                                              
                                              [self.tableControl reloadData];
                                              
                                              
                                              if ([COMMON isReachable]) {
                                                  
                                                  if (arrData.count == 0) {
                                                      warning.hidden = NO;
                                                      
                                                      warning.text = str(strMessage18);
                                                      
                                                  }else{
                                                      
                                                      warning.hidden = YES;
                                                  }
                                                  
                                              }
                                              
                                          }];
                                      }
                                  });
                                  
                              };
                              
                              [serverAPI fnDELETE_FAVORITE_PUBLICATION: dic[@"id"] ];
                              
                          }
                          else
                          {
                              
                          }
                      }];
}

-(void) checkEmpty
{
    if (arrData.count == 0) {
        warning.hidden = NO;
        
        warning.text = str(NO_FAV_PUBLICATION);
        [warning setNeedsDisplay];
    }else{
        
        warning.hidden = YES;
    }
}

@end
