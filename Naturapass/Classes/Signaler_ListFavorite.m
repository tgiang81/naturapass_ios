//
//  Signaler_ListFavorite.m
//  Naturapass
//
//  Created by Giang on 1/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Signaler_ListFavorite.h"
#import "Signaler_Favoris_System_Edit.h"
#import "CellKind2.h"
#import "MapDataDownloader.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"

@interface Signaler_ListFavorite ()
{
    int indexCatFav, iSelected;
    BOOL bFound;
    NSMutableDictionary *favDic;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;

}
@end

static NSString *typecell31 =@"CellKind2";
static NSString *identifierSection1 =@"TypeCell31ID";

@implementation Signaler_ListFavorite

- (void)viewDidLoad {
    [super viewDidLoad];
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myDesc setText:@"Utiliser un favori..."];
    self.vContainer.backgroundColor = self.colorAttachBackGround;
    lbDescription2.textColor = [UIColor whiteColor];
    lbDescription2.text = @"Retrouvez ici vos spécifications favorites";

    favDic = [NSMutableDictionary new];
    
    [self.tableControl registerNib:[UINib nibWithNibName:typecell31 bundle:nil] forCellReuseIdentifier:identifierSection1];
    self.tableControl.backgroundColor = [UIColor clearColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [PublicationOBJ sharedInstance].arrCacheFavorites.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = [PublicationOBJ sharedInstance].arrCacheFavorites[indexPath.row];
   CellKind2 *cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    cell.constraint_image_width.constant = cell.constraint_image_height.constant = 40;

    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor whiteColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"icon_sign_arrow_right" ]];
    
    
    btn.contentMode = UIViewContentModeScaleAspectFit;
    cell.constraint_image_width.constant = 0;
    cell.constraint_control_width.constant = 10;
    cell.constraint_control_height.constant = 20;
    
    btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
    
    
    cell.constraintRight.constant = 15;
    
    [cell.viewControl addSubview:btn];
    
    cell.backgroundColor=self.colorAttachCellBackGround;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Remove here your data
        NSInteger index = indexPath.row;
        [UIAlertView showWithTitle:str(strSuppression_favori)
                           message:str(strSupprimer_ce_favori_de_vos_publications_favorites)
                 cancelButtonTitle:str(strOui)
                 otherButtonTitles:@[str(strNon)]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex == [alertView cancelButtonIndex]) {
                                  NSDictionary *dic = [PublicationOBJ sharedInstance].arrCacheFavorites[index];
                                  
                                  [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
                                  
                                  WebServiceAPI *serverAPI =[WebServiceAPI new];
                                  serverAPI.onComplete = ^(id response, int errCode)
                                  {
                                      [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                                      
                                      if ([response[@"success"] isKindOfClass: [NSNumber class]] && [response[@"success"] intValue] == 1 )
                                      {
                                          
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                                              
                                                  [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                                      NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                                                      NSString *strInsertTblVersion = [ NSString stringWithFormat: @"DELETE FROM `tb_favorite` WHERE `c_id` IN (%@) AND `c_user_id` = '%@';", dic[@"id"], sender_id ] ;
                                                      //add version number to tble version.
                                                      
                                                      [db  executeUpdate:strInsertTblVersion];
                                                  }];

                                          });
                                          [[PublicationOBJ sharedInstance].arrCacheFavorites removeObjectAtIndex:index];
                                          
                                          // This line manages to delete the cell in a nice way
                                          [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                                      }
                                  };
                                  
                                  [serverAPI fnDELETE_FAVORITE_PUBLICATION: dic[@"id"] ];
                                  
                              }
                              else
                              {
                                  
                              }
                          }];
        
        
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self choosesItemFavo:indexPath.row];
}


/*
 "card_id" = 10;
 "category_id" = 137;
 */

-(void) getDataTree:(NSDictionary*) trDic
{
    if (bFound) {
        return;
    }
    
    if ([trDic[@"id"] intValue] == indexCatFav ) {
        //break
        bFound = YES;
        
//        ASLog(@"%@", trDic);
        [favDic setValue:trDic[@"receivers"] forKey:@"receivers"];
        [PublicationOBJ sharedInstance].dicFavoris=favDic;
        
        Signaler_Favoris_System_Edit *viewController1 = [[Signaler_Favoris_System_Edit alloc] initWithNibName:@"Signaler_Favoris_System_Edit" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
    }else{
        
        for (NSDictionary*nDic in trDic[@"children"]) {
            [self getDataTree:nDic];
        }
    }
}
/*
 
 sharing =             {
 share = 1;
 withouts =                 (
 );
 };
 specific = 0;
 },
 
 */

-(NSArray*) strToObject:(NSString*)strin
{
    NSData *data = [strin dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSError *e;
    NSArray *arrAttachements = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&e];
    return arrAttachements;
}

-(NSArray*) strToArr:(NSString*)strin
{
    if (strin.length > 0) {
        NSArray * array = [strin componentsSeparatedByString:@","];
        return array;
        
    }
    return nil;
}
-(NSArray*) strToArrShareUser:(NSString*)strin
{
    if (strin.length > 0) {
        NSArray * array = [strin componentsSeparatedByString:@","];
        NSMutableArray *arrTmp = [NSMutableArray new];
        for (int i = 0; i < array.count; i++) {
            NSArray * arr = [array[i] componentsSeparatedByString:@"_"];
            if (arr.count > 1) {
                [arrTmp addObject:@{@"id":arr[0],@"name": arr[1]}];
            }

        }
        return arrTmp;
        
    }
    return nil;
}
-(void) getDataDetailFav :(NSDictionary*)dicIN database:(FMDatabase *)db
{
    
        NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];

        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE c_user_id=%@ AND c_id=%@  ORDER BY c_id", sender_id, dicIN[@"id"]];
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
        while ([set_querry next])
        {
            
            NSString *strColorID = [set_querry stringForColumn:@"c_color"];
            NSDictionary *dColor;
            if (strColorID) {
                
                for (NSDictionary*dDic in dic[@"colors"]) {
                    if ([dDic[@"id"] intValue] ==  [strColorID intValue]) {
                        dColor = dDic;
                        break;
                    }
                }
                
            }
            //groups, hunts
            NSMutableArray *sharingGroupsArray =[NSMutableArray new];
            NSMutableArray *sharingHuntsArray =[NSMutableArray new];
            
            
            NSArray *arrFavoGroups = [self strToArr:[set_querry stringForColumn:@"c_groups"]];
            
            
            strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE) ];
            
            
            NSArray *arrTmpGroup = [NSArray arrayWithContentsOfFile:strPath];
            
            for (NSString *strFavoGroup in arrFavoGroups) {
                for (NSDictionary *dicMesGroup in arrTmpGroup) {
                    if ([dicMesGroup[@"groupID"] intValue] == [strFavoGroup intValue]) {
                        [sharingGroupsArray addObject:@{@"name":  dicMesGroup[@"categoryName"],
                                                        @"id":  dicMesGroup[@"groupID"]
                                                        }];
                        
                        break;
                    }
                }
            }
            
            
            //list shared mes chasses
            
            strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
            
            
            NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
            
            NSArray *arrFavoHunts = [self strToArr:[set_querry stringForColumn:@"c_hunts"]];
            
            for (NSString *strFavoHunt in arrFavoHunts) {
                for (NSDictionary *dicMesHunt in arrTmpHunt) {
                    if ([dicMesHunt[@"huntID"] intValue] == [strFavoHunt intValue]) {
                        [sharingHuntsArray addObject:dicMesHunt[@"hunt"]];
                        break;
                    }
                }
            }
            //shareusers
            NSMutableArray *arrFavoUser =[NSMutableArray new];
            [arrFavoUser  addObjectsFromArray:[self strToArrShareUser:[set_querry stringForColumn:@"c_shareusers"]]];
            //title/id
            NSDictionary *dicRet = @{ @"attachments":[self strToObject:[set_querry stringForColumn:@"c_attchments"]],
                                      @"legend":[set_querry stringForColumn:@"c_legend"],
                                      @"card_id":[set_querry stringForColumn:@"c_card"],
                                      @"category_tree":[set_querry stringForColumn:@"c_tree"],
                                      
                                      @"category_id":[set_querry stringForColumn:@"c_category"],
                                      
                                      @"color":dColor ? dColor :@{},
                                      //special format  "1,2,3"
                                      @"groups":sharingGroupsArray,
                                      @"hunts":sharingHuntsArray,
                                      @"sharingUsers":arrFavoUser,
                                      @"id":[set_querry stringForColumn:@"c_id"],
                                      @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"specific":[set_querry stringForColumn:@"c_specific"],
                                      @"sharing": @{@"share": [[set_querry stringForColumn:@"c_sharing"] isEqualToString: @""]? @"0": [set_querry stringForColumn:@"c_sharing"]  }
                                      
                                      };
            
            [favDic addEntriesFromDictionary:dicRet];
            if ([dicRet[@"category_id" ] isEqualToString:@""]) {
                [PublicationOBJ sharedInstance].dicFavoris=favDic;
                Signaler_Favoris_System_Edit *viewController1 = [[Signaler_Favoris_System_Edit alloc] initWithNibName:@"Signaler_Favoris_System_Edit" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
            }
            else
            {
                indexCatFav = [dicRet[@"category_id"] intValue];
                
                bFound = NO;
                
                //from category_id get card detail...
                for (NSDictionary*nDic in [PublicationOBJ sharedInstance].treeCategory) {
                    [self getDataTree:nDic];
                }
                
            }
        }
}

-(IBAction)choosesItemFavo:(int)index
{
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
            iSelected = index;
            
            NSDictionary *dic = [PublicationOBJ sharedInstance].arrCacheFavorites[index];
            //
            
            NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
            
            NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE c_user_id=%@ AND c_id=%@  ORDER BY c_id", sender_id, dic[@"id"]];
            
            FMResultSet *set_querry = [db  executeQuery:strQuerry];
            
            BOOL bExistItem = NO;
            
            while ([set_querry next])
            {
                bExistItem = YES;
                
            }
            
            if (bExistItem) {
                [self getDataDetailFav:dic database:db];
            }else{
                //update db...
                [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_favorite]];
                
            }
            
        }];
 
}

@end
