//
//  Publication_DocumentPDF.m
//  Naturapass
//
//  Created by GiangTT on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Publication_DocumentPDF.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "CommonHelper.h"
#import "SVPullToRefresh.h"
#import "NSString+Extensions.h"
#import "SubNavigation_PRE_General.h"
#import "DocumentViewCell.h"

@interface Publication_DocumentPDF ()
{
    int iCountAssetSelected;
}

@property (nonatomic, strong) DirectoryWatcher *docWatcher;
@property (nonatomic, strong) NSMutableArray *documentURLs;
@property (nonatomic, strong) UIDocumentInteractionController *docInteractionController;
@property (nonatomic, strong) NSMutableArray *arrSelected;

@end

@implementation Publication_DocumentPDF

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addSubNav:@"SubNavigation_PRE_General"];
    
    SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
    //Change background color
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"DocumentViewCell" bundle:nil] forCellReuseIdentifier:@"PDFCellDocument"];
    
    self.tableControl.estimatedRowHeight = 50;
    self.tableControl.rowHeight = UITableViewAutomaticDimension;

    
    
    // start monitoring the document directory…
    self.docWatcher = [DirectoryWatcher watchFolderWithPath:[self applicationDocumentsDirectory] delegate:self];
    
    self.documentURLs = [NSMutableArray new];
    self.arrSelected = [NSMutableArray new];
    
    // scan for existing documents
    [self directoryDidChange:self.docWatcher];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - action
#pragma callback
-(void)setCallback:(documentCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(documentCbk ) cb
{
    self.callback = cb;
    
}

#pragma mark - File system support

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

- (void)directoryDidChange:(DirectoryWatcher *)folderWatcher
{
    [self.documentURLs removeAllObjects];    // clear out the old docs and start over
    
    NSString *documentsDirectoryPath = [FileHelper PDFLocal];
    
    NSArray *documentsDirectoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectoryPath
                                                                                              error:NULL];
    
    for (NSString* curFileName in [documentsDirectoryContents objectEnumerator])
    {
        NSString *filePath = [documentsDirectoryPath stringByAppendingPathComponent:curFileName];
        BOOL isDirectory;
        [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
        
        // proceed to add the document URL to our list (ignore the "Inbox" folder)
        if (!(isDirectory && [curFileName isEqualToString:@"Inbox"]))
        {
            //2 pdf...only PDF...
            //Get file extention => file Type.
            
            
            if ([[curFileName pathExtension] containsString:@"pdf"]) {
                [self.documentURLs addObject:@{@"type":@2,@"filePath":filePath,@"fileName":curFileName}];
            }
            else if ([[curFileName pathExtension] containsString:@"doc"])
            {
                [self.documentURLs addObject:@{@"type":@3,@"filePath":filePath,@"fileName":curFileName}];

            }
            else if ([[curFileName pathExtension] containsString:@"psd"])
            {
                [self.documentURLs addObject:@{@"type":@4,@"filePath":filePath,@"fileName":curFileName}];

            }
            else if ([[curFileName pathExtension] containsString:@"xls"])
            {
                [self.documentURLs addObject:@{@"type":@5,@"filePath":filePath,@"fileName":curFileName}];

            }else{}

        }
    }
    
    [self.tableControl reloadData];
}

- (void)setupDocumentControllerWithURL:(NSURL *)url
{
    //checks if docInteractionController has been initialized with the URL
    if (self.docInteractionController == nil)
    {
        self.docInteractionController = [UIDocumentInteractionController interactionControllerWithURL:url];
        self.docInteractionController.delegate = self;
    }
    else
    {
        self.docInteractionController.URL = url;
    }
}

#pragma mark - TABLEVIEW

//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.documentURLs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DocumentViewCell *cell = (DocumentViewCell *)[self.tableControl dequeueReusableCellWithIdentifier:@"PDFCellDocument" forIndexPath:indexPath];
    
    NSDictionary *dic =self.documentURLs[indexPath.row];
    cell.nameFile.text = dic[@"fileName"];

    if ([dic[@"type"] intValue] ==2 ) {
        cell.iconThumb.image = [UIImage imageNamed:@"icon_pdf"];
    }
    else if ([dic[@"type"] intValue] ==3 ) {
        cell.iconThumb.image = [UIImage imageNamed:@"icon_doc"];
    }
    else if ([dic[@"type"] intValue] ==4 ) {
        cell.iconThumb.image = [UIImage imageNamed:@"icon_psd"];
        
    }
    else if ([dic[@"type"] intValue] ==5 ) {
        cell.iconThumb.image = [UIImage imageNamed:@"icon_xls"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = self.documentURLs[indexPath.row];
    
    //goback
    
    if (_callback) {
        _callback(dic);
    }
    
    [self gotoback];
}

@end
