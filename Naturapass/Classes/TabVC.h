//
//  TabVC.h
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+MGBadgeView.h"
typedef void (^callBack) (UIButton*);

@interface TabVC : UIView
@property (nonatomic, copy) callBack myCallBack;
-(void) badgingBtn:(UIButton*)btn count:(int) iCount;
@end
