//
//  PhotoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CarteStatusVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
@interface CarteStatusVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    __weak IBOutlet UILabel *lblDesctiption;
    __weak IBOutlet UIButton *btnClose;

}

@end

@implementation CarteStatusVC

-(id)initFromParent
{
    self = [super initWithNibName];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSubContent:@"CarteStatusVC"];
    [btnClose setTitle:str(strAnuler) forState:UIControlStateNormal];

    lblDesctiption.text = str(strSelectionVotreStatut);

    [self.lbTitle setText:str(strMonStatut)];
}
-(void) addSubContent:(NSString*) nameView
{
    NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:nameView owner:self options:nil];
    self.vSubContent = (UIView*)[nibArray objectAtIndex:0];
    [self.vContent addSubview:self.vSubContent];
    [self addContraintView:self.vSubContent];
    self.constraintHeight.constant =self.vSubContent.frame.size.height;
    
}
@end
