//
//  GroupCreate_Step2.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateBaseVC.h"

@interface GroupCreate_Step2 : GroupCreateBaseVC
-(void)setDataPostDict:(NSDictionary*)postdict Attachment:(NSDictionary*)attachment;
@end
