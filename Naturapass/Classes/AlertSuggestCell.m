//
//  AlertSuggestCell.m
//  Naturapass
//
//  Created by JoJo on 4/12/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "AlertSuggestCell.h"

@implementation AlertSuggestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contraintWidthAvatar.constant = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
