//
//  MDMarkersLegend.m
//  Naturapass
//
//  Created by Manh on 3/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDMarkersLegend.h"
#import "Define.h"

@import QuartzCore;

NSString * const kJPSThumbnailAnnotationViewReuseID = @"JPSThumbnailAnnotationView";

static CGFloat const kJPSThumbnailAnnotationViewVerticalOffset    = 10.0f;
static CGFloat const kJPSThumbnailAnnotationViewAnimationDuration = 0.25f;

@interface MDMarkersLegend ()
{
    float iWidth ;
}
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;

@property (nonatomic, strong) CAShapeLayer *bgLayer;
@property (nonatomic, strong) UIButton *disclosureButton;

@end

@implementation MDMarkersLegend
-(instancetype)initWithDictionary:(NSDictionary*)dicMarker withResize:(BOOL)resize
{
    self = [super init];
    if (self) {
        //set legend
        self.isResize = resize;
        self.dicMarker = dicMarker;
        
        UILabel*tmp = [UILabel new];
        tmp.font = [UIFont boldSystemFontOfSize:13];
        
        tmp.text = [self.dicMarker[@"c_title"] uppercaseString];
        
        CGSize textSize = [[tmp text] sizeWithAttributes:@{NSFontAttributeName:[tmp font]}];
        
        CGFloat strikeWidth = textSize.width + 5+20;
        
        iWidth = strikeWidth;
        
        self.frame = CGRectMake(0, 0, strikeWidth, kJPSThumbnailAnnotationViewVerticalOffset + 15);
        self.backgroundColor = [UIColor clearColor];
        
        [self setupView];
    }
    return self;
}
- (void)doSetCalloutView {
    
    [self setupView];
}
- (void)setLayerProperties {
    _bgLayer = [CAShapeLayer layer];
    CGPathRef path = [self newBubbleWithRect:self.bounds];
    _bgLayer.path = path;
    CFRelease(path);
    
//    ASLog(@"%@",self.dicMarker);
    //    color = 008080;
    if (self.dicMarker[@"c_color"]) {
        NSString *strColor = self.dicMarker[@"c_color"];
        
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:strColor];
        [scanner setScanLocation:0]; // bypass '#' character
        [scanner scanHexInt:&result];
        
        
        _bgLayer.fillColor = UIColorFromRGB(result).CGColor;//[UIColor grayColor].CGColor;
    }
    
    _bgLayer.opacity = 0.4;
    
    _bgLayer.masksToBounds = NO;
    
    [self.layer insertSublayer:_bgLayer atIndex:0];
    
    UIImage *image = [self imageFromWithView:self];
    if (_CallBackMarkers) {
        _CallBackMarkers(image);
    }

}

- (void)setupView {
    
    [self animateBubbleWithDirection];
    
    UIButtonType buttonType = UIButtonTypeCustom;
    _disclosureButton = [UIButton buttonWithType:buttonType];
    _disclosureButton.backgroundColor = [UIColor clearColor];
    _disclosureButton.frame = CGRectMake(self.frame.origin.x,
                                         2,
                                         iWidth,
                                         20);
    [_disclosureButton.titleLabel setFont:[UIFont systemFontOfSize:13]];
    
    [_disclosureButton setTitle:[self.dicMarker[@"c_title"] uppercaseString] forState:UIControlStateNormal];
    
    UIEdgeInsets titleInsets = UIEdgeInsetsMake(0.0, -15.0, 0.0, 0.0);
    _disclosureButton.titleEdgeInsets = titleInsets;
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(iWidth-15, 5, 10, 10)];
    image.image = [UIImage imageNamed:@"icon_arrow_carter"];
    [_disclosureButton addSubview:image];
    [self addSubview:_disclosureButton];
    
    
    
    [self setLayerProperties];
    
}
- (UIImage *)imageFromWithView:(UIView*)vView
{
    UIView *view = vView;
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)animateBubbleWithDirection {
    // Bubble
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"path"];
    
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.repeatCount = 1;
    animation.removedOnCompletion = NO;
    animation.fillMode = kCAFillModeForwards;
    animation.duration = kJPSThumbnailAnnotationViewAnimationDuration;
    
    
    CGPathRef fromPath = [self newBubbleWithRect:self.bounds ];
    animation.fromValue = (__bridge id)fromPath;
    CGPathRelease(fromPath);
    
    [self.bgLayer addAnimation:animation forKey:animation.keyPath];
}


#pragma mark - Geometry

- (CGPathRef)newBubbleWithRect:(CGRect)rect {
    CGFloat radius = 1.5f;
    CGMutablePathRef path = CGPathCreateMutable();
    CGFloat parentX = rect.origin.x + rect.size.width/2.0f;
    
    rect.origin.x += 0;//stroke / 2.0f + 7.0f;
    rect.origin.y += 0;//stroke / 2.0f + 7.0f;
    
    CGPathMoveToPoint(path, NULL, rect.origin.x, rect.origin.y + radius);
    CGPathAddLineToPoint(path, NULL, rect.origin.x, rect.origin.y + rect.size.height - radius);
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + rect.size.height - radius, radius, M_PI, M_PI_2, 1);
    CGPathAddLineToPoint(path, NULL, parentX - 5.0f, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, parentX, rect.origin.y + rect.size.height + 5.0f);
    CGPathAddLineToPoint(path, NULL, parentX + 5.0f, rect.origin.y + rect.size.height);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height);
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + rect.size.height - radius, radius, M_PI_2, 0.0f, 1.0f);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + rect.size.width, rect.origin.y + radius);
    CGPathAddArc(path, NULL, rect.origin.x + rect.size.width - radius, rect.origin.y + radius, radius, 0.0f, -M_PI_2, 1.0f);
    CGPathAddLineToPoint(path, NULL, rect.origin.x + radius, rect.origin.y);
    CGPathAddArc(path, NULL, rect.origin.x + radius, rect.origin.y + radius, radius, -M_PI_2, M_PI, 1.0f);
    CGPathCloseSubpath(path);
    return path;
}

@end