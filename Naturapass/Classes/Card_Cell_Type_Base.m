//
//  Card_Cell_Type10.m
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Card_Cell_Type_Base.h"
#import "MDPopover.h"

@implementation Card_Cell_Type_Base

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.txtInputNumber.layer setMasksToBounds:YES];
    self.txtInputNumber.layer.cornerRadius= 5.0;
    self.txtInputNumber.layer.borderWidth =0.5;
    self.txtInputNumber.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.lbTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.lbTitle.frame);
    self.lbDescription.preferredMaxLayoutWidth = CGRectGetWidth(self.lbDescription.frame);

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state

}
-(IBAction)changeValue:(id)sender
{
    UITextField *tf = (UITextField*)sender;
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value":tf.text});
    }
}
- (void)textViewDidEndEditing:(UITextView *)m_textView {
    if (_CallBackEditing) {
        _CallBackEditing(self.index);
    }
}
- (void)textViewDidChange:(UITextView *)textView
{
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value":textView.text});
    }
}
-(IBAction)down:(id)sender
{
    switch (self.typenumber) {
        case NUMBER_FLOAT:
        {
            [self updateValueFloatWithDown:YES];

        }
            break;
            
        default:
        {
            [self updateValueWithDown:YES];

        }
            break;
    }
}
-(IBAction)up:(id)sender
{
    switch (self.typenumber) {
        case NUMBER_FLOAT:
        {
            [self updateValueFloatWithDown:NO];
            
        }
            break;
            
        default:
        {
            [self updateValueWithDown:NO];
            
        }
            break;
    }}
-(void) updateValueWithDown:(BOOL)isDown
{
    
    int index_tf= [_txtInputNumber.text  intValue];
    
    if (isDown) {
//        if (index_tf > 0) {
            index_tf -= 1;
        if(index_tf <= 0) {index_tf = 0;}
//        }
    }else{
//        if (index_tf < 99) {
            index_tf += 1;
//        }
    }
    _txtInputNumber.text=[NSString stringWithFormat:@"%d", index_tf ];
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value":_txtInputNumber.text});

    }

}
-(void) updateValueFloatWithDown:(BOOL)isDown
{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterDecimalStyle];
//    NSNumber *myNumber = [nf numberFromString:_txtInputNumber.text];
    double index_tf= [_txtInputNumber.text  doubleValue];
    
    if (isDown) {
        //        if (index_tf > 0) {
        index_tf -= 0.1;
        if(index_tf <= 0) {index_tf = 0;}
        //        }
    }else{
        //        if (index_tf < 99) {
        index_tf += 0.1;
        //        }
    }
    if (index_tf>-0.1 && index_tf<0.1) {
        index_tf =0;
    }
    
    [nf setMaximumFractionDigits:5];
    nf.usesGroupingSeparator = YES;
    nf.groupingSeparator = @",";
    nf.decimalSeparator = @".";
    
    _txtInputNumber.text=[nf stringFromNumber:[NSNumber numberWithDouble:index_tf]];
    if (_CallBackValue) {
        _CallBackValue(self.index,@{@"value":_txtInputNumber.text});
        
    }
    
}
@end
