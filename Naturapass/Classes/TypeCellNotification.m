//
//  CellKind1.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "TypeCellNotification.h"

@implementation TypeCellNotification

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label1.preferredMaxLayoutWidth = CGRectGetWidth(self.label1.frame);
//    self.label2.preferredMaxLayoutWidth = CGRectGetWidth(self.label2.frame);
//    [super layoutSubviews];

}
@end
