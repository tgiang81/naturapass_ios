//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step6.h"
#import "ChassesCreate_Step7.h"
#import "ChassesCreate_Step11.h"
#import "ChassesCreateOBJ.h"
@interface ChassesCreate_Step6 ()
{}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@property(nonatomic,strong) IBOutlet UIImageView *img1;
@property(nonatomic,strong) IBOutlet UIButton *button1;
@property(nonatomic,strong) IBOutlet UIButton *button2;

@end

@implementation ChassesCreate_Step6
#pragma mark -setColor
-(void)fnColorTheme
{
    _label1.text =str(strSouhaitezVousImporter);
    _label2.text =str(strVousPermetDePartager);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self fnColorTheme];
    self.needChangeMessageAlert = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNext:(id)sender {
    ChassesCreate_Step7 *viewController1 = [[ChassesCreate_Step7 alloc] initWithNibName:@"ChassesCreate_Step7" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    
}
-(IBAction)NonAction:(id)sender
{
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self backEditListChasse];
    }
    else
    {
    ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
};
-(IBAction)OUIAction:(id)sender
{
    ChassesCreate_Step7 *viewController1 = [[ChassesCreate_Step7 alloc] initWithNibName:@"ChassesCreate_Step7" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
};
@end
