//
//  AgendaFromMapCell.h
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaFromMapCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UILabel *lbTitle;
@property(nonatomic,strong) IBOutlet UILabel *lbAccess;
@property(nonatomic,strong) IBOutlet UILabel *lbDebut;
@property(nonatomic,strong) IBOutlet UILabel *lbFin;
@property(nonatomic,strong) IBOutlet UILabel *lbRendez;
@property(nonatomic,strong) IBOutlet UILabel *lbDescription;
@property(nonatomic,strong) IBOutlet UIImageView *imgIcon;
@property(nonatomic,strong) IBOutlet UIButton *btnLocation;
@property(nonatomic,strong) IBOutlet UIButton *btnEnter;

@end
