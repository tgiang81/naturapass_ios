//
//  Chasse_Invite_bis_supVC.m
//  Naturapass
//
//  Created by Giang on 10/16/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Chasse_Invite_bis_supVC.h"
#import "ChassesCreate_Step54.h"
#import "ChasseCreatePersonneFictif.h"
#import "CellKind19.h"

static NSString *CellKind19ID =@"CellKind19ID";

@interface Chasse_Invite_bis_supVC ()
{
    NSArray *arrData;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblDescription1;
    __weak IBOutlet UILabel *lblDescription2;
    __weak IBOutlet UILabel *lblDescription3;

}
@end

@implementation Chasse_Invite_bis_supVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strINVITERDESNONNATIZ);
    lblDescription1.text = str(strNonNatizDesc1);
    lblDescription2.text = str(strNonNatizDesc2);
    lblDescription3.text = str(strNonNatizDesc3);
    self.needChangeMessageAlert = YES;

    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind19" bundle:nil] forCellReuseIdentifier:CellKind19ID];
    
    arrData = @[@{@"image":@"ic_chasse_invite_email", @"name":str(strEnvoi_demails)},
                @{@"image":@"ic_chasse_register_menual", @"name":str(strInscription_manuelle)}];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic =arrData[indexPath.row];

    CellKind19 *cell = (CellKind19 *)[tableView dequeueReusableCellWithIdentifier:CellKind19ID];
    cell.img1.image=  [UIImage imageNamed:dic[@"image"]];
    cell.label1.text =dic[@"name"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [self.tableControl deselectRowAtIndexPath:indexPath animated:NO];

    switch (indexPath.row) {
        case 0:
        {
            ChassesCreate_Step54 *viewController1 = [[ChassesCreate_Step54 alloc] initWithNibName:@"ChassesCreate_Step54" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        }
            break;
        case 1:
        {
            
            //MANH
//            Chasse_Invite_bis_supVC *viewController1 = [[Chasse_Invite_bis_supVC alloc] initWithNibName:@"Chasse_Invite_bis_supVC" bundle:nil];
//            [self pushVC:viewController1 animate:YES];
            ChasseCreatePersonneFictif  *viewController1=[[ChasseCreatePersonneFictif alloc]initWithNibName:@"ChasseCreatePersonneFictif" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        }
            break;
        default:
            break;
    }
    
    
}

@end
