//
//  Signaler_Favoris_System.h
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignalerBaseVC.h"

@interface Signaler_Favoris_System : SignalerBaseVC
@property (nonatomic, retain) NSMutableArray *displayArray;
@property (nonatomic,strong) IBOutlet UIView *viewBottom;
@property (nonatomic,strong) IBOutlet UIButton *btnTERMINER;
@property (nonatomic,strong) IBOutlet UIView *viewTF;
@property (nonatomic,strong) IBOutlet UITextField *tfInput;
@property (nonatomic,strong) IBOutlet UIView *viewHeader;

@end
