//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertPublicationMultipleSelect.h"
#import "Define.h"
#import "PublicationControlCheckboxCell.h"
#import "AppCommon.h"
static NSString *identifier = @"CheckboxCellID";
static NSString *tableCell = @"PublicationControlCheckboxCell";

@implementation AlertPublicationMultipleSelect
{
    NSMutableArray *arrMultiple;
    NSDictionary *dicImage;
}
-(instancetype)initWithTitle:(NSString*)stitle arrContent:(NSArray*)arrContent arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC
{
    self =[super initWithNibName:@"AlertPublicationMultipleSelect" bundle:nil];
    if (self) {
        strTitle =stitle;
        arrData = arrContent;
        listCheck = arrCheck;
        typeControl =typeC;
    }
    return self;
}
#pragma mark - viewdid
-(void)viewDidLoad
{
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];

    _lbTitle.text =strTitle;
    //mld
    switch (typeControl) {
        case CONTROL_CHECKBOX:
        {
            dicImage= @{@"non_check":@"check",@"check":@"check_active"};
        }
            break;
        case CONTROL_RADIO:
        {
            dicImage= @{@"non_check":@"radio_inactive",@"check":@"radio_active"};
        }
            break;
        default:
            break;
    }
    arrMultiple= [NSMutableArray arrayWithArray:arrData];
    for (int i =0; i <listCheck.count; i++) {
        for (int j=0; j< arrMultiple.count; j++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[j]];
            if ([listCheck[i] intValue]== [dic[@"id"] intValue]) {
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:j withObject:dic];
                break;
            }

        }
    }
    
    [tableControl registerNib:[UINib nibWithNibName:tableCell bundle:nil] forCellReuseIdentifier:identifier];
}

#pragma callback
-(void)setCallback:(AlertSeclectCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSeclectCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showInVC:(UIViewController*)vc
{
    
    [vc presentViewController:self animated:NO completion:^{
        
    }];
}
-(IBAction)ANNULERAction:(id)sender
{
    NSMutableArray *arrValue = [NSMutableArray new];
    NSMutableArray *arrText = [NSMutableArray new];

    for (NSDictionary *dic in arrMultiple) {
        if ([dic[@"isSelect"] boolValue]== YES) {
            [arrValue addObject:dic[@"id"]];
            [arrText addObject:dic[@"name"]];
        }
    }
    [self dismissViewControllerAnimated:YES completion:^{}];
    if (_callback) {
        _callback(@{@"value":arrValue,@"text":arrText});
    }
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrMultiple count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicationControlCheckboxCell *cell = nil;
    
    cell = (PublicationControlCheckboxCell *)[tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = arrMultiple[indexPath.row];
    cell.lbTittle.text = dic[@"name"];
    if ([dic[@"isSelect"] boolValue]== YES) {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:dicImage[@"check"]] forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:dicImage[@"non_check"]] forState:UIControlStateNormal];
    }
    cell.btnCheckBox.tag = indexPath.row +200;
    [cell.btnCheckBox addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self fnSelectItem:indexPath.row];
}
- (IBAction)selectAction:(id)sender
{
    int index = (int)([sender tag] -200);
    [self fnSelectItem:index];
}
-(void)fnSelectItem:(int)index
{
    
    switch (typeControl) {
        case CONTROL_CHECKBOX:
        {
            if (index<arrMultiple.count) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [tableControl reloadData];
            }
        }
            break;
        case CONTROL_RADIO:
        {
            if (index<arrMultiple.count) {
                arrMultiple= [NSMutableArray arrayWithArray:arrData];
                
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [tableControl reloadData];
            }
        }
            break;
        default:
            break;
    }
}
@end
