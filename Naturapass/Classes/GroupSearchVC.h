//
//  GroupSearchVC.h
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "GroupBaseVC.h"

@interface GroupSearchVC : GroupBaseVC
@property (nonatomic,retain) NSString *strDate;
@property (nonatomic,assign) BOOL isSearching;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintY_Search;

@end
