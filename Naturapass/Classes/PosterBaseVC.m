//
//  PosterBaseVC.m
//  Naturapass
//
//  Created by JoJo on 9/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "PosterBaseVC.h"
#import "MapGlobalVC.h"

@interface PosterBaseVC ()

@end

@implementation PosterBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self fnSetColor];
    [self addMainNav:@"MainNavSignaler"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:@"POSTER QUOI ?"];
    //Change background color
    subview.backgroundColor = _colorNavigation;
    subview.imgBackground.image = [UIImage imageNamed:@"bg_nav_mur_v31"];
    subview.imgClose.image = [[UIImage imageNamed:@"icon_chasses_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    subview.imgClose.tintColor = [UIColor whiteColor];

    
    [self addSubNav:nil];
    
    
    self.vContainer.backgroundColor = _colorBackGround;

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: _colorNavigation ];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)gobackParent
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        //Code here.. e.g. print their titles to see the array setup;
        
        if ([controller isKindOfClass:[self.mParent class]] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }
            
            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }
}
-(void)fnSetColor
{
    switch (self.expectTarget) {
        case ISMUR:
        {
            _colorBackGround = UIColorFromRGB(POSTER_BACKGROUND_COLOR);
            _colorNavigation = UIColorFromRGB(POSTER_NAV_COLOR);
            _colorCellBackGround = UIColorFromRGB(POSTER_CELL_BACKGROUND_COLOR);
            _colorAnnuler = UIColorFromRGB(POSTER_ANNULER_COLOR);
        }
            break;
            
        default:
        {
            _colorBackGround = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);
            _colorNavigation = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
            _colorCellBackGround = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
            _colorAnnuler = UIColorFromRGB(CHASSES_ANNULER_COLOR);

        }
            break;
    }
}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://HOME
        {
            [self gotoback];
            
        }
            break;
            
        case 1://
        {
            //go to Live hunt screen
                NSArray * controllerArray = [[self navigationController] viewControllers];
                
                for (UIViewController *controller in controllerArray)
                {
                    //Code here.. e.g. print their titles to see the array setup;
                    
                    if ([controller isKindOfClass:[MapGlobalVC class]] ) {
                        [self.navigationController popToViewController:controller animated:TRUE];
                        return;
                    }
                }
            
            //else
            [self.navigationController popToRootViewControllerAnimated:YES];
                
        }
            break;

        default:
            break;
    }
    
}

@end
