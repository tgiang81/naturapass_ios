//
//  TheProjectCell.m
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "TheProjectCell.h"

@interface TheProjectCell()
{
    __weak IBOutlet NSLayoutConstraint *contraintPaddingLeft;
}
@property (nonatomic) BOOL isExpanded;

@end

@implementation TheProjectCell

#pragma mark - Draw controls messages
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.cellLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.cellLabel.frame);
}
- (void)drawRect:(CGRect)rect
{
    int indentation = (int)self.treeNode.nodeLevel * 15;
    contraintPaddingLeft.constant=indentation;
}

- (void)setTheButtonBackgroundImage:(UIImage *)backgroundImage
{
    [self.cellButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
}

- (IBAction)expand:(id)sender
{
    self.treeNode.isExpanded = !self.treeNode.isExpanded;
    [self setSelected:NO];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ProjectTreeNodeButtonClicked" object:self];
}
- (IBAction)selectAction:(id)sender
{
    self.treeNode.isSelected = !self.treeNode.isSelected;
    [self setSelected:NO];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ProjectTreeNodeSelected" object:self.treeNode];
}
@end
