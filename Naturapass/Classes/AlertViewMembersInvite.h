//
//  UploadProgress.h
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//
#import "Define.h"
typedef void (^callBackAlert) (NSInteger index, NSDictionary *dic);

@interface AlertViewMembersInvite : UIViewController
@property(nonatomic,strong) NSMutableArray *arrMembers;
@property(nonatomic,strong) NSString       *strID;
@property (nonatomic,assign) ISSCREEN      expectTarget;
@property (nonatomic, copy) callBackAlert CallBack;
- (id)initWithArrayMembers:(NSMutableArray*)arrMembers  wihtID:(NSString*)strID;

@end
