//
//  GroupCreate_AddHunts.m
//  Naturapass
//
//  Created by Giang on 11/25/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_AddHunts.h"

#import "TypeCell41.h"
#import "ChassesCreate_Step6.h"

static NSString *typecell41 =@"TypeCell41";
static NSString *typecell41ID =@"TypeCell41ID";
@interface GroupCreate_AddHunts ()
{
    NSMutableArray *arrData;
    IBOutlet UIButton *suivant;
}

@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@property(nonatomic,strong) IBOutlet UILabel *label3;

@property(nonatomic,strong) IBOutlet UILabel *labelWarning;

@end

@implementation GroupCreate_AddHunts


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];
    suivant.backgroundColor = UIColorFromRGB(GROUP_BACK);
    self.needChangeMessageAlert = YES;

}

//membres ~ number subscriber.
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell41 bundle:nil] forCellReuseIdentifier:typecell41ID];
    
    if ([GroupCreateOBJ sharedInstance].isModifi) {
        
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
        [COMMON addLoading:self];
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj getAllHuntsAdmin];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            //or get from local save...
            if (response) {
                [GroupCreateOBJ sharedInstance].listHuntAdmin = response[@"hunts"];
                
                arrData = [NSMutableArray new];
                
                for (NSDictionary*dic in [GroupCreateOBJ sharedInstance].listHuntAdmin) {
                    
                    [arrData addObject:@{@"id":dic[@"id"],
                                         @"name":dic[@"name"],
                                         @"photo":dic[@"photo"],
                                         @"nbSubscribers": dic[@"nbSubscribers"],
                                         @"status":[NSNumber numberWithBool:NO]
                                         }];
                }
                
                if (arrData.count ==0) {
                    self.labelWarning.hidden = NO;
                }else{
                    self.labelWarning.hidden = YES;
                }
                
                [self.tableControl reloadData];
            }
        };
    }
    else
    {
        arrData = [NSMutableArray new];
        
        for (NSDictionary*dic in [GroupCreateOBJ sharedInstance].listHuntAdmin) {
            
            [arrData addObject:@{@"id":dic[@"id"],
                                 @"name":dic[@"name"],
                                 @"photo":dic[@"photo"],
                                 @"nbSubscribers": dic[@"nbSubscribers"],
                                 @"status":[NSNumber numberWithBool:NO]
                                 }];
        }
        
        if (arrData.count ==0) {
            self.labelWarning.hidden = NO;
        }else{
            self.labelWarning.hidden = YES;
        }
        
        [self.tableControl reloadData];
    }
    
}

#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeCell41 *cell = (TypeCell41 *)[tableView dequeueReusableCellWithIdentifier:typecell41ID];
    
    //photo
    NSDictionary *dic = arrData[indexPath.row];
    
    NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    
    [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
    
    NSString*strMembres = nil;
    
    cell.label1.text = dic[@"name"];
    if ([dic[@"nbSubscribers"] intValue] > 1) {
        strMembres = [NSString stringWithFormat:@"%d %@s",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
        
    }else{
        strMembres = [NSString stringWithFormat:@"%d %@",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
    }
    
    cell.constraint_control_width.constant=20;
    [cell.checkbox setTypeCheckBox:UI_CHASSES_MUR_NORMAL];
    NSNumber *num = dic[@"status"];
    
    if ( [num boolValue] ) {
        [cell.checkbox setSelected:TRUE];
        
    }else{
        [cell.checkbox setSelected:FALSE];
        
    }
    cell.checkbox.tag = indexPath.row;
    [cell.checkbox addTarget:self action:@selector(checkbox:) forControlEvents:UIControlEventTouchUpInside];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    cell.label2.text = strMembres;
    [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self checkboxStatusWithIndex:indexPath.row];
}
-(IBAction)checkbox:(id)sender
{
    NSInteger index =[sender tag];
    [self checkboxStatusWithIndex:index];
}
-(void)checkboxStatusWithIndex:(NSInteger)index
{
    NSDictionary *dic=[[arrData objectAtIndex:index] mutableCopy];
    
    if ([dic[@"status"] boolValue] == YES)
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"status"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
    
    [arrData replaceObjectAtIndex:index withObject:dic];
    
    
    NSRange range = NSMakeRange(0,1);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
}

//Suivant
//http://doc-naturapass.e-conception.fr/source-class-Api.ApiBundle.Controller.v2.Hunts.HuntsController.html#37-89

-(IBAction)onNext:(id)sender
{
    
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    for (NSDictionary*dic in arrData) {
        if ([dic[@"status"] boolValue] == YES) {
            [arrTmp addObject:dic[@"id"]];
        }
    }
    
    if (arrTmp.count == 0) {
        return;
    }
    
    [COMMON addLoading:self];
    //Request
    
    // 44:      *      hunts[] = 1, 2, 3

    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postGroupAgendaWithGroupId:[GroupCreateOBJ sharedInstance].group_id
                         withParametersDic:@{@"hunts":arrTmp}];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        NSLog(@"%@", response);
        if (response[@"success"]) {
            if ([GroupCreateOBJ sharedInstance].isModifi) {
                [self backEditListChasse];
            }
            else
            {
            }
            
        }
    };
    
}

@end
