//
//  GroupCreateOBJ.m
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupEnterOBJ.h"

@implementation GroupEnterOBJ


static GroupEnterOBJ *sharedInstance = nil;

+ (GroupEnterOBJ *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) resetParams
{
    _dictionaryGroup =nil;
}

@end
