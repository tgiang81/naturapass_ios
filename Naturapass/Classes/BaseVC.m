//
//  BaseVC.m
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import "BaseVC.h"

#import "MainNavigationBaseView.h"
#import "SubNavBaseView.h"
#import "ServiceHelper.h"
#import "ASSharedTimeFormatter.h"
#import "ResearchVC.h"
#import "NotificationVC.h"
#import "MurFilterVC.h"
#import "MurVC.h"
#import "MurSettingVC.h"

#import "MurFilterVC.h"



#import "GroupMesVC.h"
#import "GroupSearchVC.h"
#import "GroupSettingVC.h"

#import "ChassesMurVC.h"
#import "ChassesSearchVC.h"
#import "ChassesSettingVC.h"
#import "ChassesMurPassVC.h"
#import "ChassesParticipeVC.h"

#import "GroupEnterMurVC.h"
#import "MapGlobalVC.h"
#import "GroupEnterAgenda.h"
#import "GroupEnterSettingVC.h"

#import "SubNavigationMUR.h"
#import "UIButton+ButtonGroup_ENTER.h"

#import "ChatVC.h"
#import "AmisListVC.h"
#import "AmisAddScreen1.h"
#import "Amis_Demand_Invitation.h"
#import "AmisSearchVC.h"
#import "ChatVC.h"
#import "ChatListe.h"
#import "Parameter_Favorites.h"
#import "Parameter_Notification_GlobalVC.h"
#import "Parameter_Notification_Detail.h"
#import "Parameter_Publication.h"
#import "ChasseSettingMembres.h"
#import "MapLocationVC.h"
#import "UploadProgress.h"
#import "MainNavigationController.h"
#import <CCMPopup/CCMPopupTransitioning.h>
#import "ChassesInvitationAttend.h"
#import "GroupInvitationAttend.h"
#import "GroupAdminListVC.h"
#import "GroupSettingMembres.h"
#import "ChassesEnterInfoVC.h"
#import "CommonObj.h"
#import "Parameter_ProfilVC.h"
#import "Parameter_MesFavoris.h"
#import "SubNavigationGROUPENTER.h"
#import "SubNavigationCHASSES.h"
#import "Parameter_PassProfile.h"
#import "Parameter_MonProfile.h"
#import "SVPullToRefresh.h"
#import "Parameter_PhotoProfile.h"
#import "chassesEditListVC.h"
#import "groupEditListVC.h"
#import "MurEditPublicationVC.h"
#import "CommentDetailVC.h"
#import "UINavigationController+Autorotate.h"

#import "ChaseEnterNonAdminInfoVC.h"
#import "MurParameter_Publication.h"
#import "MurParameter_Notification_Email.h"
#import "MurParameter_Notification_Smartphones.h"
#import "MurParameter_PublicationFav.h"
#import "MemberBlackListVC.h"
#import "FriendInfoVC.h"
#import "Parameter_MesPublicationFav.h"
#import "Publication_Favoris_System_Edit.h"
#import "Parameter_BlackListVC.h"

#import "Parameter_CommonSTEP0.h"
#import "Parameter_CommonSTEP1.h"
#import "Parameter_CommonSTEP2.h"
#import "OpinionzAlertView.h"
#import "Parameter_Common_Profile_Kind.h"
#import "Parameter_Common_Profile_Kind_Change.h"
#import "Parameter_Profile_Papers_Change.h"
#import "SettingNotificationEmailMobileVC.h"
#import "GroupParameter_Notification_Smartphones.h"
#import "Naturapass-Swift.h"//Common filter
#import "SignalerTerminez.h"
#import "ChassesCreateV2.h"
#import "AlertVC.h"
#import "LiveHuntOBJ.h"
#import "AlertViewLiveHunt.h"
#import "Signaler_Favoris_System_Edit.h"
#import "Signaler_Favoris_System.h"
BOOL bFilterON;

@interface BaseVC ()
{
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
    OpinionzAlertView           *alertPublicWithLocation;
    //Fix bug titleview navigation + refactor code.
    MyCustomTitleView *navigation;
    UIImageView *imgV;
}

@end

@implementation BaseVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
    
}

-(MainNavigationBaseView*) getSubMainView{
    MainNavigationBaseView *subview =  (MainNavigationBaseView*) [navigation.viewNavigation1 viewWithTag:TAG_MAIN_NAV_VIEW];
    
    return subview;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    //register KVO
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fnDoRefreshNotification) name: NOTIFICATION_REQUEST_UPDATING_BADGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkUploadCount) name: NOTIFICATION_UPDATING_UPLOAD_COUNT object:nil];

    
    self.app = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self loadNavigation];
    
    //clean
    [self.view removeConstraints:self.view.constraints];
    
    self.constraintBottom.constant = 0;
    
    self.operationQueue = [NSOperationQueue new];
    
    //Change status bar color
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
    
    //Load option
    if ([filterDic[@"bFilterON"] boolValue] ) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }
    
    //using custom navigation
    self.navigationItem.hidesBackButton=YES;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    
    self.vContainer.backgroundColor = UIColorFromRGB(MAIN_COLOR);
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(checkNotification:) name:NOTIFICATIONTIMER object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(killTextFocus) name: KILL_TEXT_FOCUS object:nil];
    [COMMON listSubviewsOfView:self.view];

}

-(void) willMoveToParentViewController:(UIViewController *)parent
{
    [super willMoveToParentViewController:parent];
    if (!parent) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REQUEST_UPDATING_BADGE object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_UPDATING_UPLOAD_COUNT object:nil];
        
        //remove all notification subsribe
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}


-(void) fnDoRefreshNotification
{
    UIImageView *btnAmis =(UIImageView*)[subviewCount viewWithTag:24];
    UIImageView *btnDiscussion =(UIImageView*)[subviewCount viewWithTag:26];
    UIImageView *btnNotification =(UIImageView*)[subviewCount viewWithTag:28];
    
    [subviewCount badgingBtn:btnAmis count:[CommonObj sharedInstance].nbUserWaiting];
    [subviewCount badgingBtn:btnDiscussion count:[CommonObj sharedInstance].nbUnreadMessage];
    [subviewCount badgingBtn:btnNotification count:[CommonObj sharedInstance].nbUnreadNotification];

}

-(void)killTextFocus {
    [self.view endEditing:YES];
}

//Stupid
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    UIImageView *btnAmis =(UIImageView*)[subviewCount viewWithTag:24];
    UIImageView *btnDiscussion =(UIImageView*)[subviewCount viewWithTag:26];
    UIImageView *btnNotification =(UIImageView*)[subviewCount viewWithTag:28];
    [subviewCount badgingBtn:btnAmis count:[CommonObj sharedInstance].nbUserWaiting];
    [subviewCount badgingBtn:btnDiscussion count:[CommonObj sharedInstance].nbUnreadMessage];
    [subviewCount badgingBtn:btnNotification count:[CommonObj sharedInstance].nbUnreadNotification];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
    //    [self loadNavigation];
    [self checkUploadCount];
}
-(void)checkUploadCount
{
    
    MainNavigationBaseView *subview =  [self getSubMainView];
    
    if ([self checkCachePublicationHasData] > 0) {
        
        subview.uploadConstraintWidth.constant = 43;
        subview.imgUpload.hidden  = NO;
        [subview badgingBtn:subview.imgUpload count:(int)[self checkCachePublicationHasData]];
    }else{
        subview.uploadConstraintWidth.constant = 0;
        subview.imgUpload.hidden  = TRUE;
        [subview badgingBtn:subview.imgUpload count:0];
    }
    
}
-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (self.keyboardToolbar == nil)
    {
        self.keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        self.keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:self
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strOK)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard:)];
        
        [self.keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                        spaceBarItem,
                                        doneBarItem, nil]];
        
        
    }
    //    [self.emailContent setInputAccessoryView:keyboardToolbar];
    
}

-(void) updateStatusNavControls
{
    //    Update image Subnav
    
    UIButton*btnChange = (UIButton*) [self.subview viewWithTag: 2*(START_SUB_NAV_TAG+2)];
    
    if (bFilterON) {
        [btnChange setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter_active"] forState:UIControlStateSelected];
        [btnChange setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter"] forState:UIControlStateNormal];
        
    }else{
        
        [btnChange setBackgroundImage: [UIImage imageNamed:@"mur_ic_filter_off_active"] forState:UIControlStateSelected];
        [btnChange setBackgroundImage: [UIImage imageNamed:@"mur_ic_filter_off"] forState:UIControlStateNormal];
    }
    
}

- (void)resignKeyboard:(id)sender
{
}
- (void)animateView:(NSUInteger)tag
{
    [self checkBarButton:tag];
    CGRect rect = self.view.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    if (tag == 1) {
        rect.origin.y = -20.0f ;
    } else if (tag == 2){
        rect.origin.y =-55.0f ;
    }
    else if (tag ==3)
    {
        rect.origin.y =-130.0f ;
        
    }
    else if (tag ==4)
    {
        rect.origin.y=-280.0f;
    }
    [UIView commitAnimations];
}
- (void)previousField:(id)sender
{
    id firstResponder = [self getFirstResponder];
    NSUInteger tag = [firstResponder tag];
    
    NSUInteger previousTag = tag == 1 ? 1 : tag - 1;
    [self checkBarButton:previousTag];
    [self animateView:previousTag];
    UITextField *previousField = (UITextField *)[self.view viewWithTag:previousTag];
    [previousField becomeFirstResponder];
    [self checkSpecialFields:previousTag];
}

- (void)nextField:(id)sender
{
    id firstResponder = [self getFirstResponder];
    NSUInteger tag = [firstResponder tag];
    NSUInteger nextTag = tag == 5 ? 5 : tag + 1;
    [self checkBarButton:nextTag];
    [self animateView:nextTag];
    UITextField *nextField = (UITextField *)[self.view viewWithTag:nextTag];
    [nextField becomeFirstResponder];
    [self checkSpecialFields:nextTag];
}
- (void)checkSpecialFields:(NSUInteger)tag
{
    
}
- (void)checkBarButton:(NSUInteger)tag
{
    UIBarButtonItem *previousBarItem1 = (UIBarButtonItem *)[[self.keyboardToolbar items] objectAtIndex:0];
    UIBarButtonItem *nextBarItem1     = (UIBarButtonItem *)[[self.keyboardToolbar items] objectAtIndex:1];
    
    [previousBarItem1 setEnabled:tag == 1 ? NO : YES];
    [nextBarItem1     setEnabled:tag == 5 ? NO : YES];
}

- (id)getFirstResponder
{
    NSUInteger index = 0;
    while (index <= 5) {
        UITextField *textField = (UITextField *)[self.view viewWithTag:index];
        if ([textField isFirstResponder]) {
            return textField;
        }
        index++;
    }
    return 0;
}

-(NSInteger) checkCachePublicationHasData
{
    NSInteger hasData = 0;
    
    

    id value =[[NSUserDefaults standardUserDefaults] objectForKey:concatstring([COMMON getUserId],strPostPublication)];
    if (value) {
        hasData = [value allKeys].count;
    }

    return hasData;
}

-(void) addMainNav:(NSString*) nameView
{
    NSArray *nibArray = nil;
    id md_view=nil;
    if ([nameView isEqualToString:@"MainNavLiveMap"]) {
        
        [self.navigationController setNavigationBarHidden:NO animated:NO];

        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavLiveMap" owner:self options:nil];
//        if (self.isLiveHuntDetail) {
//            md_view =[nibArray objectAtIndex:1];
//        }
//        else
//        {
            md_view =[nibArray objectAtIndex:0];
//
//        }
    }
   else if ([nameView isEqualToString:@"MainNavDetailMap"]) {
        
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavDetailMap" owner:self options:nil];
        md_view =[nibArray objectAtIndex:0];
    }
    else if ([nameView isEqualToString:@"CARTER"]) {
        [self.navigationController setNavigationBarHidden:YES animated:NO];

//        [navigation.viewNavigation1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-|"
//                                                                                           options:0
//                                                                                           metrics:nil
//                                                                                             views:nil]];
//        [navigation.viewNavigation1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-|"
//                                                                                           options:0
//                                                                                           metrics:nil
//                                                                                             views:nil]];

        
        return;
        
    }
    
    else if ([nameView isEqualToString:@"MainNavChasses"])
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        
        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavChasses" owner:self options:nil];
        md_view =[nibArray objectAtIndex:0];
    }
    else if ([nameView isEqualToString:@"MainNavSignaler"])
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        
        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavSignaler" owner:self options:nil];
        md_view =[nibArray objectAtIndex:0];
    }
    else if ([nameView isEqualToString:@"MainNavMUR_V3"])
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        
        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavMUR_V3" owner:self options:nil];
        md_view =[nibArray objectAtIndex:0];
    }
    else
    {
        [self.navigationController setNavigationBarHidden:NO animated:NO];

        nibArray = [[NSBundle mainBundle]loadNibNamed:@"MainNavMUR_UPLOAD" owner:self options:nil];
        md_view =[nibArray objectAtIndex:0];
    }
    
    MainNavigationBaseView *subview = (MainNavigationBaseView*)md_view;
    
    //update constraint for last item uploading.
    
    [subview setMyMainNavCallback:^(UIButton*btn)
     {

             [self onMainNavClick:btn];
     }];
    
    subview.translatesAutoresizingMaskIntoConstraints = NO;
    
    [navigation.viewNavigation1 addSubview:subview];
    subview.tag = TAG_MAIN_NAV_VIEW;
    NSDictionary *views = NSDictionaryOfVariableBindings(subview);
    [navigation.viewNavigation1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[subview]-0-|"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:views]];
    [navigation.viewNavigation1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subview]|"
                                                                                       options:0
                                                                                       metrics:nil
                                                                                         views:views]];
    subviewCount =subview;
    if ([nameView isEqualToString:@"MainNavLiveMap"]) {
        _navLiveMap = (MainNavLiveMap*)subview;
    }
}

//0 1 2 3 4 5

//Demo 4
-(void) removeItem:(int) index
{
    UIButton *btn4 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG + index];
    UIButton *btn42 = (UIButton*)[self.subview viewWithTag:2*(START_SUB_NAV_TAG + index)];
    UILabel *lb4 = (UILabel*)[self.subview viewWithTag:TEXT_START_SUB_NAV_TAG + index];
    UILabel *lb42 = (UILabel*)[self.subview viewWithTag:2*(TEXT_START_SUB_NAV_TAG + index)];
    UIView *view = (UIView*)[self.subview viewWithTag:3*(START_SUB_NAV_TAG + index)];

    [lb4 removeFromSuperview];
    [lb42 removeFromSuperview];
    [btn4 removeFromSuperview];
    [btn42 removeFromSuperview];
    [view removeFromSuperview];

    int pre = index -1;
    int next  = index+1;
    UIButton *btnPre;
    UIButton *btnNext;
    
    while (pre>=0) {
        btnPre = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG + pre];
        if (btnPre != nil) {
            break;
        }
        pre-=1;
    }
    while (next<=5) {
        btnNext = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG + next];
        if (btnNext != nil) {
            break;
        }
        next+=1;
    }
    if (btnPre!= nil && btnNext!=nil) {
        [self.subview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnPre]-0-[btnNext]"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:NSDictionaryOfVariableBindings(btnPre,btnNext)]];
    }
    else if (btnPre!= nil && btnNext==nil)
    {
        [self.subview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnPre]-0-|"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:NSDictionaryOfVariableBindings(btnPre)]];
        
    }
    else if (btnPre== nil && btnNext!=nil)
    {
        [self.subview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[btnNext]"
                                                                             options:0
                                                                             metrics:nil
                                                                               views:NSDictionaryOfVariableBindings(btnNext)]];
    }
    else
    {
    }

}

-(void) addSubNav:(NSString*) nameView
{
    for (NSLayoutConstraint *constraint in self.view.constraints) {
        if (constraint.firstItem == self.vContainer || constraint.secondItem == self.vContainer) {
            [self.view removeConstraint:constraint];
        }
    }
    if ([nameView isEqualToString:@"livechat"])
    {
        
        //
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0 constant:0] ];
        
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0 constant:0] ];

//                                   multiplier:1.0 constant:-44] ];
        
        //Add bottom
        //
        /*
        NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:@"TabVC" owner:self options:nil];
        
        UIView *vv = self.vContainer;
        
        self.myTabVC = (TabVC*)[nibArray objectAtIndex:0];
        
        __weak BaseVC *wself = self;
        [self.myTabVC setMyCallBack:^(UIButton*btn)
         {
             [wself tabVCAction:btn];
         }];
        
        
        self.myTabVC.translatesAutoresizingMaskIntoConstraints = NO;
        
        UIView *sview = self.myTabVC;
        
        [self.view addSubview:self.myTabVC];
        
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"V:|-0-[vv]-0-[sview]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(sview,vv)]];
        
        
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"H:|-0-[sview]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(sview)]];
        
        */
        
    }else if ([nameView isEqualToString:@"CARTER"])
    {
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:20] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0 constant:0] ];
        
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0 constant:0] ];

    }
    else if (nameView == nil)
    {
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTop
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0 constant:0] ];
        
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0 constant:0] ];
        
//        UIView *vCon = self.vContainer;
//        
//        [self.view addConstraints:[NSLayoutConstraint
//                                   constraintsWithVisualFormat:@"V:|-0-[vCon]-0-|"
//                                   options:NSLayoutFormatDirectionLeadingToTrailing
//                                   metrics:nil
//                                   views:NSDictionaryOfVariableBindings(vCon)]];
//        
//        [self.view addConstraints:[NSLayoutConstraint
//                                   constraintsWithVisualFormat:@"H:|-0-[vCon]-0-|"
//                                   options:NSLayoutFormatDirectionLeadingToTrailing
//                                   metrics:nil
//                                   views:NSDictionaryOfVariableBindings(vCon)]];
//        
        
        
        
    }else{
        //add subview
        NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:nameView owner:self options:nil];
        
        
        self.subview = (SubNavBaseView*)[nibArray objectAtIndex:0];
        CGRect rect = self.subview.frame;
        rect.size.height = 44;
        
        self.subview.frame = rect;
        
        UIView *sview = self.subview;
        
        __weak BaseVC * v = self;
        [self.subview setMyCallBack:^(UIButton*btn)
         {
             
             [v onSubNavClick:btn];
             
         }];
        
        self.subview.translatesAutoresizingMaskIntoConstraints = NO;  //This part hung me up

        
        [self.view addSubview:self.subview];
        
        //add tabBottom
//        if (self.showTabBottom) {
//            UIView *vv = self.vContainer;
//            NSString *nameTabBottom = @"TabBottomVC";
//            if(self.murV3)
//            {
//                nameTabBottom = @"TabBottomVC_V3";
//            }
//            NSArray *nibArrayTab = [[NSBundle mainBundle]loadNibNamed:nameTabBottom owner:self options:nil];
//
//
//            self.bottomVC = (TabBottomVC*)[nibArrayTab objectAtIndex:0];
//
//            __weak BaseVC *wself = self;
//            [self.bottomVC setMyCallBack:^(UIButton*btn)
//             {
//                 [wself tabBottomVCAction:btn];
//             }];
//
//
//            self.bottomVC.translatesAutoresizingMaskIntoConstraints = NO;
//
//            UIView *tview = self.bottomVC;
//
//            CGRect rectTab = self.bottomVC.frame;
//            rectTab.size.height = 44;
//            self.bottomVC.frame = rectTab;
//
//            [self.view addSubview:self.bottomVC];
//
//
//            [self.view addConstraints:[NSLayoutConstraint
//                                       constraintsWithVisualFormat:@"V:|-0-[sview(44)]-0-[vv]-0-[tview]-0-|"
//                                       options:NSLayoutFormatDirectionLeadingToTrailing
//                                       metrics:nil
//                                       views:NSDictionaryOfVariableBindings(sview,vv,tview)]];
//
//
//
//            [self.view addConstraints:[NSLayoutConstraint
//                                       constraintsWithVisualFormat:@"H:|-0-[tview]-0-|"
//                                       options:NSLayoutFormatDirectionLeadingToTrailing
//                                       metrics:nil
//                                       views:NSDictionaryOfVariableBindings(tview)]];
//        }
//        else
        {
            
            [self.view addConstraints:[NSLayoutConstraint
                                       constraintsWithVisualFormat:@"V:|-0-[sview(44)]-0-|"
                                       options:NSLayoutFormatDirectionLeadingToTrailing
                                       metrics:nil
                                       views:NSDictionaryOfVariableBindings(sview)]];
            
            [self.view addConstraint: [NSLayoutConstraint
                                       constraintWithItem:self.vContainer attribute:NSLayoutAttributeBottom
                                       relatedBy:NSLayoutRelationEqual
                                       toItem:self.view
                                       attribute:NSLayoutAttributeBottom
                                       multiplier:1.0 constant:0] ];
            
        }

        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"H:|-0-[sview]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(sview)]];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.subview attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.vContainer
                                   attribute:NSLayoutAttributeTop
                                   multiplier:1.0 constant:0] ];
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeLeading
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeLeading
                                   multiplier:1.0 constant:0] ];
        
        
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.vContainer attribute:NSLayoutAttributeTrailing
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeTrailing
                                   multiplier:1.0 constant:0] ];
        

    }
    //add tabBottom
    if (self.showTabBottom) {
        UIView *superview = self.vContainer.superview;
        NSLayoutConstraint *bottomConstraint;
        for (NSLayoutConstraint *c in superview.constraints) {
            if ((c.firstItem == self.vContainer && c.firstAttribute == NSLayoutAttributeBottom) || (c.secondItem == self.vContainer && c.secondAttribute == NSLayoutAttributeBottom)) {
                bottomConstraint = c;
                break;
            }
        }
        if (bottomConstraint) {
            bottomConstraint.constant = -44;
        }
        
        NSString *nameTabBottom = @"TabBottomVC";
        if(self.murV3)
        {
            nameTabBottom = @"TabBottomVC_V3";
        }
        NSArray *nibArrayTab = [[NSBundle mainBundle]loadNibNamed:nameTabBottom owner:self options:nil];
        
        
        self.bottomVC = (TabBottomVC*)[nibArrayTab objectAtIndex:0];
        
        __weak BaseVC *wself = self;
        [self.bottomVC setMyCallBack:^(UIButton*btn)
         {
             [wself tabBottomVCAction:btn];
         }];
        
        
        self.bottomVC.translatesAutoresizingMaskIntoConstraints = NO;
        
        UIView *tview = self.bottomVC;
        
        CGRect rectTab = self.bottomVC.frame;
        rectTab.size.height = 44;
        self.bottomVC.frame = rectTab;
        
        [self.view addSubview:self.bottomVC];
        [self.view addConstraint: [NSLayoutConstraint
                                   constraintWithItem:self.bottomVC attribute:NSLayoutAttributeBottom
                                   relatedBy:NSLayoutRelationEqual
                                   toItem:self.view
                                   attribute:NSLayoutAttributeBottom
                                   multiplier:1.0 constant:0] ];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.bottomVC
                                                              attribute:NSLayoutAttributeHeight
                                                              relatedBy:NSLayoutRelationEqual
                                                                 toItem:nil
                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                             multiplier:1.0
                                                               constant:44]];
        [self.view addConstraints:[NSLayoutConstraint
                                   constraintsWithVisualFormat:@"H:|-0-[tview]-0-|"
                                   options:NSLayoutFormatDirectionLeadingToTrailing
                                   metrics:nil
                                   views:NSDictionaryOfVariableBindings(tview)]];
    }
}


- (NSLayoutConstraint *)pin:(id)item attribute:(NSLayoutAttribute)attribute andView:(UIView*) v
{
    return [NSLayoutConstraint constraintWithItem:v
                                        attribute:attribute
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:item
                                        attribute:attribute
                                       multiplier:1.0
                                         constant:0.0];
}

#pragma mark - NAV

-(void) doRemoveObservation
{
    //remove all observation...
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [BaseVC class]]) {
            BaseVC *vc = (BaseVC*) controllerArray[i];
            [vc fnRemoveObserver];
        }
    }

}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://HOME
        {
            //[self doRemoveObservation];

            //[self.navigationController popToRootViewControllerAnimated:YES];
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate gotoApplication];
        }
            break;
            
        case 1://Search
        {
            if ([self isKindOfClass: [ResearchVC class]])
                return;
            
            [self doPushVC:[ResearchVC  class] iAmParent:TRUE];
        }
            break;
            
            
        case 2://AMIS
        {
            if ([self isKindOfClass: [Amis_Demand_Invitation class]])
                return;
            
            [self doPushVC:[Amis_Demand_Invitation  class] iAmParent:TRUE expectTarget:ISAMIS];
        }
            break;
            
        case 3://DISCUSS
        {
            if ([self isKindOfClass: [ChatListe class]])
                return;
            
            [self doPushVC:[ChatListe  class] iAmParent:TRUE expectTarget:ISDISCUSS];
        }
            break;
        case 4://NOTIFICATION
        {
            if ([self isKindOfClass: [NotificationVC class]])
                return;
            
            [self doPushVC:[NotificationVC  class] iAmParent:TRUE expectTarget:ISMUR];
            
        }
            break;
            
        case 5://PROGRESS
        {
            if ([self isKindOfClass: [UploadProgress class]])
                return;
            
            UploadProgress *viewController1 = [[UploadProgress alloc] initWithNibName:@"UploadProgress" bundle:nil];
            viewController1.expectTarget =self.expectTarget;
            CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
            
            //            if (self.view.bounds.size.height < 420) {
            //                popup.destinationBounds = CGRectMake(0, 0, ([UIScreen mainScreen].bounds.size.height-20) * .75, [UIScreen mainScreen].bounds.size.height-20);
            //            } else {
            popup.destinationBounds = CGRectMake(0, 0, 300, 460);
            //            }
            popup.presentedController = viewController1;
            popup.presentingController = self;
            [self presentViewController:viewController1 animated:YES completion:nil];
        }
            break;
            
            
        default:
            break;
    }
    
}

-(void) onSubNavClick:(UIButton*)btn
{
    [self viewWillDisappear:YES];
    
    [btn setSelected:NO];
    UIButton * subBtnTmp = (UIButton*)[self.subview viewWithTag:2* (btn.tag)];
    [subBtnTmp setSelected:YES];
}

-(void) onTabItemClick:(UIButton*)btn
{
//    [self viewWillDisappear:YES];
//    
//    [btn setSelected:NO];
//    UIButton * subBtnTmp = (UIButton*)[self.myTabVC viewWithTag:2* (btn.tag)];
//    [subBtnTmp setSelected:YES];
//    
//    switch (btn.tag -START_SUB_NAV_TAG)
//    {
//        case 0://MUR
//        {
//            
//            if ([self isKindOfClass: [GroupEnterMurVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[GroupEnterMurVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//        }
//            break;
//            
//        case 1://
//        {
//            //Carte
//            if ([self isKindOfClass: [MapGlobalVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[MapGlobalVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//        }
//            break;
//            
//        case 2://TOUT
//        {
//            //Chat
//            if ([self isKindOfClass: [ChatVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[ChatVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//            
//        }
//            break;
//            
//        default:
//            break;
//    }
}

-(void) clickSubNav_Mur:(int) index
{
    switch (index) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
            
        case 1://MUR
        {
            if ([self isKindOfClass: [MurVC class]])
                return;
            [self doPushVC:[MurVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://Filter
        {
            if ([self isKindOfClass: [MurFilterVC class]])
                return;
            [self doPushVC:[MurFilterVC  class] iAmParent:NO];
        }
            break;
            
        case 3://SETTING
        {
            if ([self isKindOfClass: [MurSettingVC class]])
                return;
            [self doPushVC:[MurSettingVC  class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
}

-(void) clickSubNav_Group:(int) index
{
    
    switch (index) {
        case 0://BACK
        {
            
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }

            [self gotoback];
        }
            break;
            
        case 1://MES GROUP
        {
            if ([self isKindOfClass: [GroupEnterMurVC class]])
                return;
            [self doPushVC:[GroupEnterMurVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://CARTE
        {
            if ([self isKindOfClass: [MapGlobalVC class]])
                return;
            [self doPushVC:[MapGlobalVC  class] iAmParent:NO];
        }
            break;
            
        case 3://DISCUSTION
        {
            if ([self isKindOfClass: [ChatVC class]])
                return;
            [self doPushVC:[ChatVC  class] iAmParent:NO];
        }
            
            break;
        case 4://Agenda
        {
            if ([self isKindOfClass: [GroupEnterAgenda class]])
                return;
            
            [self doPushVC:[GroupEnterAgenda  class] iAmParent:NO];
            
        }
            
            break;
        case 5://SET
        {
            if ([self isKindOfClass: [GroupEnterSettingVC class]])
                return;
            [self doPushVC:[GroupEnterSettingVC  class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
}

-(void) clickSubNav_Chass:(int) index
{
    
    switch (index) {
        case 0://BACK
        {
            
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://MES GROUP
        {
            if ([self isKindOfClass: [GroupEnterMurVC class]])
                return;
            
            [self doPushVC:[GroupEnterMurVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://CARTE
        {
            if ([self isKindOfClass: [MapGlobalVC class]])
                return;
            
            [self doPushVC:[MapGlobalVC  class] iAmParent:NO];
            
        }
            break;
            
        case 3://DISCUSTION
        {
            if ([self isKindOfClass: [ChatVC class]])
                return;
            
            [self doPushVC:[ChatVC  class] iAmParent:NO];
        }
            
            break;
        case 4://PRO
        {
            
            //If non Admin
            
            
            if (self.dicOption)
            {
                if ([self.dicOption[@"admin"] boolValue])
                {
                    if ([self isKindOfClass: [ChasseSettingMembres class]])
                        return;
                    
                    [self doPushVC:[ChasseSettingMembres  class] iAmParent:NO];
                }else{
                    if ([self isKindOfClass: [ChaseEnterNonAdminInfoVC class]])
                        return;
                    
                    [self doPushVC:[ChaseEnterNonAdminInfoVC  class] iAmParent:NO];
                    
                    
                }
                
            }
        }
            
            
            break;
        case 5://SET
        {
            
            if ([self isKindOfClass: [GroupEnterSettingVC class]])
                return;
            
            [self doPushVC:[GroupEnterSettingVC  class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
}

-(void) clickSubNav_Discuss:(int) index
{
    switch (index) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://CHAT
        {
            if ([self isKindOfClass: [ChatVC class]])
                return;
            
            [self doPushVC:[ChatVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://SETTING
        {
            
        }
            
            break;
            
        default:
            break;
    }
}


-(void) clickSubNav_Special:(int) index
{
    switch (index) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            [self.navigationController popToViewController:controllerArray[controllerArray.count-3] animated:YES];
        }
            break;
            
        case 1://CHAT
        {
            if ([self isKindOfClass: [ChatVC class]])
                return;
            
            [self doPushVC:[ChatVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://SETTING
        {
            
        }
            
            break;
            
        default:
            break;
    }
}



//From Invite chasee -> Map -> <Special case>

-(void) onSubNavClick_Invite_Chasse:(int) index{
    
    switch (index) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://MUR
        {
            if ([self isKindOfClass: [ChassesMurVC class]])
                return;
            
            [self doPushVC:[ChassesMurVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://TOUT
        {
            if ([self isKindOfClass: [ChassesSearchVC class]])
                return;
            
            [self doPushVC:[ChassesSearchVC  class] iAmParent:NO];
            
        }
            break;
            
        case 3://old
        {
            if ([self isKindOfClass: [ChassesMurPassVC class]])
                return;
            
            [self doPushVC:[ChassesMurPassVC  class] iAmParent:NO];
        }
            
            break;
        case 4://Invitation
        {
            if ([self isKindOfClass: [ChassesInvitationAttend class]])
                return;
            
            [self doPushVC:[ChassesInvitationAttend  class] iAmParent:NO];
        }
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

#pragma mark - Custom Navigation
- (void)loadNavigation{
    
    navigation = [[[NSBundle mainBundle] loadNibNamed:@"MyCustomTitleView" owner:self options:nil] objectAtIndex:0];
    
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationItem.titleView = navigation;
}

- (IBAction)notificationButtonAction:(id)sender{
    //    NotificationViewController *notificationVC=[[NotificationViewController alloc]initWithNibName:@"NotificationViewController" bundle:nil];
    //    [self.navigationController pushViewController:notificationVC animated:NO];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) cleanAnyTmpData{}

-(IBAction)gotoback
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        //Code here.. e.g. print their titles to see the array setup;
        
        if ([controller isKindOfClass:[self.mParent class]] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }
        }
    }

    [self.navigationController popViewControllerAnimated:YES];
}

-(void) doPushVC:(Class)class iAmParent:(BOOL)isParent expectTarget:(ISSCREEN)expectTarget
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        if ([controller isKindOfClass:class] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }

            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }
    
    BaseVC *viewController1 = [[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
    viewController1.expectTarget = expectTarget;
    if (isParent) {
        viewController1.mParent = self;
    }else{
        viewController1.mParent = self.mParent;
    }
    
    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
}


-(void) doPushVCLiveHunt:(Class)class iAmParent:(BOOL)isParent expectTarget:(ISSCREEN)expectTarget
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        if ([controller isKindOfClass:class] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }

            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }
    
    BaseVC *viewController1 = [[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
    viewController1.expectTarget = expectTarget;
    if (isParent) {
        viewController1.mParent = self;
    }else{
        viewController1.mParent = self.mParent;
    }
    
    viewController1.isLiveHunt = YES;
    
    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
}
-(void) doPushVC:(Class)class iAmParent:(BOOL)isParent
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        if ([controller isKindOfClass:class] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }

            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }
    
    BaseVC *viewController1 = [[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
    viewController1.expectTarget = self.expectTarget;
    if (isParent) {
        viewController1.mParent = self;
    }else{
        viewController1.mParent = self.mParent;
    }
    
    //
    viewController1.needRemoveSubItem = self.needRemoveSubItem;
    
    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
}
-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim
{
    vc.expectTarget = self.expectTarget;
    
    vc.mParent = self;
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:anim completion:^ {
        NSLog(@"COMPLETED");
    }];
    
}
-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim expectTarget:(ISSCREEN)expectTarget
{
    vc.expectTarget = expectTarget;
    
    vc.mParent = self;
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:anim completion:^ {
        NSLog(@"COMPLETED");
    }];
    
}
-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim expectTarget:(ISSCREEN)expectTarget iAmParent:(BOOL)isParent
{
    vc.expectTarget = expectTarget;
    
    if (isParent) {
        vc.mParent = self;
    }else{
        vc.mParent = self.mParent;
    }
    
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:anim completion:^ {
        NSLog(@"COMPLETED");
    }];
    
}

#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] timeFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy H:mm" forFormatter: outputFormatterBis];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}
#pragma mark - REFERSH CONTROL

-(void) initRefreshControl
{
    __weak BaseVC *weakSelf = self;
    
    [self.tableControl addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    [self.tableControl addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    
}
-(void)startRefreshControl
{
    [self.tableControl triggerPullToRefresh];
    
}
-(void)moreRefreshControl
{
    [self.tableControl triggerInfiniteScrolling];
    
}
-(void)stopRefreshControl
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableControl.pullToRefreshView stopAnimating];
        [self.tableControl.infiniteScrollingView stopAnimating];
    });
}
-(void)isRemoveScrollingViewHeight:(BOOL)enable
{
    self.tableControl.infiniteScrollingView.isRemoveScrollingViewHeight = enable;

}
//overwrite
- (void)insertRowAtTop {
}

//overwrite
- (void)insertRowAtBottom {
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
-(void)setThemeNavSub :(BOOL) forceParent withDicOption:(NSDictionary*)dicOption
{
    if (self.isLiveHunt) {
//        [self setThemeTabVC:self];
        return;
    }
    
    UIButton  *btnTmp1 = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + 1)];
    UIButton  *btnTmp2 = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + 2)];
    UIButton  *btnTmp3 = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + 3)];
    UIButton  *btnTmp4 = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + 4)];
    UIButton  *btnTmp5 = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + 5)];
    
    UILabel  *txtTag1 = (UILabel*)[self.subview viewWithTag:2* (TEXT_START_SUB_NAV_TAG + 1)];
    UILabel  *txtTag2 = (UILabel*)[self.subview viewWithTag:2* (TEXT_START_SUB_NAV_TAG + 2)];
    UILabel  *txtTag3 = (UILabel*)[self.subview viewWithTag:2* (TEXT_START_SUB_NAV_TAG + 3)];
    UILabel  *txtTag4 = (UILabel*)[self.subview viewWithTag:2* (TEXT_START_SUB_NAV_TAG + 4)];
    UILabel  *txtTag5 = (UILabel*)[self.subview viewWithTag:2* (TEXT_START_SUB_NAV_TAG + 5)];
    
    NSArray *arr =[self fnSUBNAVTAG];
    BOOL isChild= NO;
    if (arr) {
        isChild = [arr[0]  boolValue];
    }
    

    switch (self.expectTarget) {
        case ISLOUNGE:
        {
            if (forceParent || isChild == NO) {
                [btnTmp1 setImageWithImgStateNomal:@"ic_mes_chasse_inactive" ImgStateSelected:@"ic_mes_chasse_active"];
                [btnTmp2 setImageWithImgStateNomal:@"ic_search_chasse_inactive" ImgStateSelected:@"ic_search_chasse_active"];
                [btnTmp3 setImageWithImgStateNomal:@"ic_chasse_history_inactive" ImgStateSelected:@"ic_chasse_history_active"];
                [btnTmp4 setImageWithImgStateNomal:@"ic_invite_chasse_inactive" ImgStateSelected:@"ic_invite_chasse_active"];
                //text
                txtTag1.text =str(strMonAgenda);
                txtTag2.text =str(strRecherche);
                txtTag3.text =str(strHistorique);
                txtTag4.text =str(strInvitations);
                txtTag5.text =str(strEMPTY);
                
            }else{
                //text
                txtTag1.text =str(strMur);
                txtTag2.text =str(strCarte);
                txtTag3.text =str(strDiscussion);
                txtTag5.text =str(strParametres);
                
                [btnTmp1 setImageWithImgStateNomal:@"ic_chasse_mur_inactive" ImgStateSelected:@"ic_chasse_mur_active"];
                [btnTmp2 setImageWithImgStateNomal:@"ic_chasse_carte_inactive" ImgStateSelected:@"ic_chasse_carte_active"];
                [btnTmp3 setImageWithImgStateNomal:@"ic_chasse_chat_inactive" ImgStateSelected:@"ic_chasse_chat_active"];
                [btnTmp4 setImageWithImgStateNomal:@"ic_info_chasse_unselect" ImgStateSelected:@"ic_info_chasse_select"];
                if (dicOption) {
                    if ([dicOption[@"admin"] boolValue]) {
                        [btnTmp4 setImageWithImgStateNomal:@"ic_chasse_member_inactive" ImgStateSelected:@"ic_chasse_member_active"];
                        txtTag4.text =str(strInvites);
                        
                    }else{
                        txtTag4.text =str(strInfos);
                    }
                }else{
                    txtTag4.text =str(strInfos);
                }
                [btnTmp5 setImageWithImgStateNomal:@"ic_chasse_setting_inactive" ImgStateSelected:@"ic_chasse_setting_active"];
            }
        }
            break;
        case ISGROUP:
        {
            if (isChild )
            {
                //text
                txtTag1.text =str(strMur);
                txtTag2.text =str(strCarte);
                txtTag3.text =str(strDiscussion);
                txtTag4.text =str(strAgenda);
                txtTag5.text =str(strParametres);
                
                [btnTmp1 setImageWithImgStateNomal:@"ic_group_mur_inactive" ImgStateSelected:@"ic_group_mur_active"];
                [btnTmp2 setImageWithImgStateNomal:@"ic_group_map_inactive" ImgStateSelected:@"ic_group_map_active"];
                [btnTmp3 setImageWithImgStateNomal:@"ic_chat_inactive" ImgStateSelected:@"ic_group_chat_active"];
                [btnTmp4 setImageWithImgStateNomal:@"ic_group_event_inactive" ImgStateSelected:@"ic_group_event_active"];
                [btnTmp5 setImageWithImgStateNomal:@"ic_group_setting" ImgStateSelected:@"ic_group_setting_active"];
                
            }
            else
            {
                //text
                txtTag1.text =str(strMesgroupes);
                txtTag2.text =str(strRecherche);
                txtTag3.text =str(strInvitations);
                txtTag4.text =str(strInfos);
                txtTag5.text =str(strParametres);
                
                [btnTmp1 setImageWithImgStateNomal:@"ic_my_group_inactive" ImgStateSelected:@"ic_my_group"];
                [btnTmp2 setImageWithImgStateNomal:@"ic_search_group" ImgStateSelected:@"ic_search_group_active"];
                [btnTmp3 setImageWithImgStateNomal:@"group_ic_invite_inactive" ImgStateSelected:@"group_ic_invite"];
                [btnTmp4 setImageWithImgStateNomal:@"ic_group_event_inactive" ImgStateSelected:@"ic_group_event_active"];
                [btnTmp5 setImageWithImgStateNomal:@"group_ic_invite_inactive" ImgStateSelected:@"group_ic_invite"];
            }
            
            
        }
            break;
        case ISAMIS:
        {
            //text
            txtTag1.text = str(strAmis);
            txtTag2.text = str(strInviter);
            txtTag3.text = str(strInvitations);
            txtTag4.text = str(strRecherche);
            txtTag5.text = str(strEMPTY);
            
            [btnTmp1 setImageWithImgStateNomal:@"amis_ic_friend" ImgStateSelected:@"amis_ic_friend_active"];
            [btnTmp2 setImageWithImgStateNomal:@"amis_ic_friend_add" ImgStateSelected:@"amis_ic_friend_add_active"];
            
            [btnTmp3 setImageWithImgStateNomal:@"amis_ic_friend_added" ImgStateSelected:@"amis_ic_friend_added_active"];
            [btnTmp4 setImageWithImgStateNomal:@"amis_ic_search_friend" ImgStateSelected:@"amis_ic_search_friend_active"];
            
        }
            break;
        case ISMUR:
        {
            if (bFilterON) {
                [btnTmp2 setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter_active"] forState:UIControlStateSelected];
                [btnTmp2 setBackgroundImage: [UIImage imageNamed:@"mur_ic_action_filter"] forState:UIControlStateNormal];
                
            }else{
                
                [btnTmp2 setBackgroundImage: [UIImage imageNamed:@"mur_ic_filter_off_active"] forState:UIControlStateSelected];
                [btnTmp2 setBackgroundImage: [UIImage imageNamed:@"mur_ic_filter_off"] forState:UIControlStateNormal];
            }
            
            [btnTmp1 setImageWithImgStateNomal:@"mur_ic_action_mur" ImgStateSelected:@"mur_ic_action_mur_active"];
            [btnTmp3 setImageWithImgStateNomal:@"mur_ic_action_setting" ImgStateSelected:@"mur_ic_action_setting_post"];
            [btnTmp4 setImageWithImgStateNomal:@"ic_chasse_member_inactive" ImgStateSelected:@"ic_chasse_member_active"];
            [btnTmp5 setImageWithImgStateNomal:@"ic_chasse_setting_inactive" ImgStateSelected:@"ic_chasse_setting_active"];
            
            //text
            txtTag1.text = str(strMur);
            txtTag2.text = str(strFiltres);
            txtTag3.text =str(strParametres);
            
            
        }
            break;
        case ISPARAMTRES:
        {
            //text
            txtTag1.text = str(strProfil);
            txtTag2.text = str(strGeneral);
            txtTag3.text = str(strEmail);
            txtTag4.text = str(strMobile);
            txtTag5.text = str(strFavoris);
            
            [btnTmp1 setImageWithImgStateNomal:@"param_ic_pub_unactive" ImgStateSelected:@"param_ic_pub_active"];
            [btnTmp2 setImageWithImgStateNomal:@"param_ic_general_inactive" ImgStateSelected:@"param_ic_general_active"];
            [btnTmp3 setImageWithImgStateNomal:@"param_ic_email_unactive" ImgStateSelected:@"param_ic_email"];
            [btnTmp4 setImageWithImgStateNomal:@"param_ic_smartphones_unactive" ImgStateSelected:@"param_ic_smartphones_active"];
            [btnTmp5 setImageWithImgStateNomal:@"param_ic_address_unactive" ImgStateSelected:@"param_ic_address_active"];
            
        }
            break;
        default:
        {
        }
            break;
    }
    
    btnTmp1.selected=NO;
    btnTmp2.selected=NO;
    btnTmp3.selected=NO;
    btnTmp4.selected=NO;
    btnTmp5.selected=NO;
    //color text
    txtTag1.textColor = [UIColor blackColor];
    txtTag2.textColor = [UIColor blackColor];
    txtTag3.textColor = [UIColor blackColor];
    txtTag4.textColor = [UIColor blackColor];
    txtTag5.textColor = [UIColor blackColor];
    
}
-(void)setColorTextNavSubWithIndex:(int)index
{
    UILabel  *txtTag1 = (UILabel*)[self.subview viewWithTag:index];
    switch (self.expectTarget) {
        case ISLOUNGE:
        {
            txtTag1.textColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP:
        {
            txtTag1.textColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            
        }
            break;
        case ISAMIS:
        {
            txtTag1.textColor = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
            
        }
            break;
        case ISMUR:
        {
            txtTag1.textColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            
        }
            break;
        case ISPARAMTRES:
        {
            txtTag1.textColor = [UIColor blackColor];
            
        }
            break;
        default:
        {
        }
            break;
    }
}
-(UIButton* )returnButton
{
    UIButton *btnTmp = nil;
    NSArray *arr =[self fnSUBNAVTAG];
    if (arr) {
        int  index = [arr[1] intValue];
        btnTmp = (UIButton*)[self.subview viewWithTag:2* (START_SUB_NAV_TAG + index)];
        
    }
    return btnTmp;
}
-(NSArray*)fnSUBNAVTAG
{
    NSArray *arr =nil;
    //GROUP ENTER
    
    //Focus | index order.
    
    if ([self isKindOfClass: [GroupEnterMurVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    else if ([self isKindOfClass: [FriendInfoVC class]] && self.expectTarget != ISMUR)
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    else if ([self isKindOfClass: [MapLocationVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"2",nil];
        
    }
    else if ([self isKindOfClass: [MapGlobalVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"2",nil];
        
    }
    else if ([self isKindOfClass: [ChatVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"3",nil];
        
    }
    else if ([self isKindOfClass: [GroupEnterAgenda class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"4",nil];
        
    }
    else if ([self isKindOfClass: [GroupEnterSettingVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [ChassesEnterInfoVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [GroupAdminListVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [GroupSettingMembres class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [GroupParameter_Notification_Smartphones class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [SettingNotificationEmailMobileVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"5",nil];
        
    }
    else if ([self isKindOfClass: [ChaseEnterNonAdminInfoVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"4",nil];
        
    }
    
    //GROUP MES
    else if ([self isKindOfClass: [GroupMesVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [GroupSearchVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    else if ([self isKindOfClass: [GroupSettingVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [GroupInvitationAttend class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    
    //MUR
    else if ([self isKindOfClass: [MurVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [FriendInfoVC class]] && self.expectTarget == ISMUR)
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [MurFilterVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    else if ([self isKindOfClass: [MurSettingVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [MurParameter_Publication class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [MurParameter_Notification_Smartphones class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [MurParameter_Notification_Email class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [MurParameter_PublicationFav class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    
    else if ([self isKindOfClass: [MemberBlackListVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    //CHASSE MUR
    else if ([self isKindOfClass: [ChassesMurVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [ChassesParticipeVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [ChassesSearchVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    else if ([self isKindOfClass: [ChassesMurPassVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    
    else if ([self isKindOfClass: [ChassesSettingVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"4",nil];
        
    }
    else if ([self isKindOfClass: [ChassesInvitationAttend class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"4",nil];
        
    }
    //CHASSE ENTER
    else if ([self isKindOfClass: [ChasseSettingMembres class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"4",nil];
        
    }
    
    //AMIS
    else if ([self isKindOfClass: [AmisListVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [AmisAddScreen1 class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    else if ([self isKindOfClass: [Amis_Demand_Invitation class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [AmisSearchVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"4",nil];
        
    }
    
    //PARAM
    
    else if ([self isKindOfClass: [Parameter_CommonSTEP0 class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_CommonSTEP1 class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    
    else if ([self isKindOfClass: [Parameter_CommonSTEP2 class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Common_Profile_Kind class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Profile_Papers_Change class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }

    else if ([self isKindOfClass: [Parameter_ProfilVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Common_Profile_Kind_Change class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    
    else if ([self isKindOfClass: [Parameter_PassProfile class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_PhotoProfile class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"1",nil];
        
    }
    
    else if ([self isKindOfClass: [Parameter_Publication class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Notification_GlobalVC class]] && self.nofiticationType == ISEMAIL)
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Notification_GlobalVC class]] && self.nofiticationType == ISSMARTPHONE)
    {
        arr =[NSArray arrayWithObjects:@"0",@"4",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Notification_Detail class]] && self.nofiticationType == ISEMAIL)
    {
        arr =[NSArray arrayWithObjects:@"0",@"3",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Notification_Detail class]] && self.nofiticationType == ISSMARTPHONE)
    {
        arr =[NSArray arrayWithObjects:@"0",@"4",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_MesFavoris class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"5",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_MesPublicationFav class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"5",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_Favorites class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"5",nil];
        
    }
    else if ([self isKindOfClass: [Parameter_BlackListVC class]])
    {
        arr =[NSArray arrayWithObjects:@"0",@"2",nil];
        
    }
    
    //COMMENT DETAIL
    else if ([self isKindOfClass: [CommentDetailVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    //Edit Publication
    else if ([self isKindOfClass: [MurEditPublicationVC class]])
    {
        arr =[NSArray arrayWithObjects:@"1",@"1",nil];
        
    }
    return arr;
}

-(void)checkNotification:(NSNotification*)notif
{
    NSDictionary *response =(NSDictionary*)[notif object];
    
    UIImageView *btnAmis =(UIImageView*)[subviewCount viewWithTag:24];
    UIImageView *btnDiscussion =(UIImageView*)[subviewCount viewWithTag:26];
    UIImageView *btnNotification =(UIImageView*)[subviewCount viewWithTag:28];
    
    [CommonObj sharedInstance].nbUnreadMessage = [response[@"nbUnreadMessage"] intValue] ;
    [CommonObj sharedInstance].nbUnreadNotification = [response[@"nbUnreadNotification"] intValue] ;
    [CommonObj sharedInstance].nbUserWaiting = [response[@"nbUserWaiting"] intValue];
    
    [subviewCount badgingBtn:btnAmis count:[CommonObj sharedInstance].nbUserWaiting];
    [subviewCount badgingBtn:btnDiscussion count:[CommonObj sharedInstance].nbUnreadMessage];
    [subviewCount badgingBtn:btnNotification count:[CommonObj sharedInstance].nbUnreadNotification];
}

-(void)gotoNotifi_leaves
{
}

-(void) fnGetAllNotifications:(NSInteger)index creen:(ISSCREEN)screen
{
    UIButton *btnCheck = (UIButton*)[self.subview viewWithTag: 2*( START_SUB_NAV_TAG + index)];
    
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj fnGET_ALL_NOTIFICATION];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([response isKindOfClass: [NSDictionary class]]) {
                [CommonObj sharedInstance].nbChasseInvitation = [response[@"nbChasseInvitation"] intValue];
                [CommonObj sharedInstance].nbGroupInvitation = [response[@"nbGroupInvitation"] intValue] ;
                [CommonObj sharedInstance].nbUnreadMessage = [response[@"nbUnreadMessage"] intValue] ;
                [CommonObj sharedInstance].nbUnreadNotification = [response[@"nbUnreadNotification"] intValue] ;
                [CommonObj sharedInstance].nbUserWaiting = [response[@"nbUserWaiting"] intValue];
                
                if (screen == ISLOUNGE) {
                    [self.subview badgingBtn:btnCheck count: (int)[CommonObj sharedInstance].nbChasseInvitation];
                }
                else if (screen == ISGROUP)
                {
                    [self.subview badgingBtn:btnCheck count: (int)[CommonObj sharedInstance].nbGroupInvitation];
                }
                
            }
            
        });

    };
}
-(void) doRefresh
{
}
-(void)backEditListChasse
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [chassesEditListVC class]]) {
            
            [self.navigationController popToViewController:controllerArray[i] animated:YES];
            
            return;
        }
    }
    
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)backEditListGroups
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [groupEditListVC class]]) {
            
            [self.navigationController popToViewController:controllerArray[i] animated:YES];
            
            return;
        }
    }
    
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)backEditListMur
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [MurEditPublicationVC class]]) {
            
            [self.navigationController popToViewController:controllerArray[i] animated:YES];
            
            return;
        }
    }
    
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)backEditFavo
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [Publication_Favoris_System_Edit class]]) {
            Publication_Favoris_System_Edit *vc =(Publication_Favoris_System_Edit*)controllerArray[i];
            [vc refreshCard];
            [self.navigationController popToViewController:vc animated:YES];
            
            return;
        }
        if ([controllerArray[i] isKindOfClass: [Signaler_Favoris_System_Edit class]]) {
            Signaler_Favoris_System_Edit *vc =(Signaler_Favoris_System_Edit*)controllerArray[i];
            [vc refreshCard];
            [self.navigationController popToViewController:vc animated:YES];
            
            return;
        }
        if ([controllerArray[i] isKindOfClass: [Signaler_Favoris_System class]]) {
            Signaler_Favoris_System *vc =(Signaler_Favoris_System*)controllerArray[i];
            [self.navigationController popToViewController:vc animated:YES];
            
            return;
        }

    }
    
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - API noti
- (void)putNotification:(NSString *)strNotificationID{
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj putUserReadNotificationAction:strNotificationID];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //reset list
    };
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}
-(CGSize) fnResize:(CGSize)size width:(float)wWidth
{
    if (0 == size.height) {
        return CGSizeMake(0,0);
    }

    float actualHeight = size.height;
    float actualWidth = size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    return CGSizeMake(actualWidth, actualHeight);
}

#pragma mark- tabVC
-(IBAction)tabVCAction:(UIButton*)btn
{
//    [self viewWillDisappear:YES];
//    
//    [btn setSelected:NO];
//    UIButton * subBtnTmp = (UIButton*)[self.myTabVC viewWithTag:2* (btn.tag)];
//    [subBtnTmp setSelected:YES];
//    
//    switch (btn.tag -START_SUB_NAV_TAG)
//    {
//        case 0://MUR
//        {
//            
//            if ([self isKindOfClass: [GroupEnterMurVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[GroupEnterMurVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//        }
//            break;
//            
//        case 1://
//        {
//            //Carte
//            if ([self isKindOfClass: [MapGlobalVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[MapGlobalVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//        }
//            break;
//            
//        case 2://TOUT
//        {
//            //Chat
//            if ([self isKindOfClass: [ChatVC class]])
//                return;
//            
//            [self doPushVCLiveHunt:[ChatVC  class] iAmParent:NO expectTarget:ISLOUNGE];
//            
//        }
//            break;
//            
//        default:
//            break;
//    }
    
}


-(void) doShowAlert
{
    alertPublicWithLocation = [[OpinionzAlertView alloc] initWithTitle:str(strAjoutDunRepere)
                                                               message:str(strVousAvezFaitUnePublicationGeo)
                                                     cancelButtonTitle:str(strOK)
                                                     otherButtonTitles:nil];
    alertPublicWithLocation.iconType = OpinionzAlertIconSuccess;
    alertPublicWithLocation.color = [UIColor colorWithRed:0.15 green:0.68 blue:0.38 alpha:1];
    
    [alertPublicWithLocation show];
    
}




-(IBAction)tabBottomVCAction:(UIButton*)btn
{
        [self viewWillDisappear:YES];
        [[PublicationOBJ sharedInstance]  resetParams];

        [btn setSelected:NO];
        UIButton * subBtnTmp = (UIButton*)[self.bottomVC viewWithTag:2* (btn.tag)];
        [subBtnTmp setSelected:YES];
    
    if (self.murV3) {
        switch (btn.tag -START_SUB_NAV_TAG)
        {
            case 0://PHOTO
            {
                if ([self isKindOfClass: [MurVC class]])
                    return;
                
//                [self doPushVC:[MurVC  class] iAmParent:NO];

                MurVC *viewController1 = [[MurVC alloc] initWithNibName:@"MurVC" bundle:nil];
                viewController1.mParent = self;
                [self pushVC:viewController1 animate:NO expectTarget:ISMUR];
            }
                break;
                
            case 1://
            {
                [self gotoAlertConfirmLiveHunt];
            }
                break;
                
            case 2://CARTE
            {
                if ([self isKindOfClass: [MapGlobalVC class]])
                    return;
                MapGlobalVC *viewController1 = [[MapGlobalVC alloc] initWithNibName:@"MapGlobalVC" bundle:nil];
                viewController1.mParent = self;
                [self pushVC:viewController1 animate:NO expectTarget:ISCARTE];
            }
                break;
                
            default:
                break;
        }

    }
    else
    {
        
        switch (btn.tag -START_SUB_NAV_TAG)
        {
            case 0://PHOTO
            {
    
                if ([self isKindOfClass: [SignalerTerminez class]])
                    return;
                SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                viewController1.autoShow = TYPE_PHOTO;
                [self pushVC:viewController1 animate:NO expectTarget:ISMUR];
            }
                break;
    
            case 1://
            {
                //VIDEO
                if ([self isKindOfClass: [SignalerTerminez class]])
                    return;
                SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                viewController1.autoShow = TYPE_VIDEO;
                [self pushVC:viewController1 animate:NO expectTarget:ISMUR];
            }
                break;
    
            case 2://LOCATION
            {
                double lat = self.app.locationManager.location.coordinate.latitude;
                double lng = self.app.locationManager.location.coordinate.longitude;
                
                NSString *strLatitude=[NSString stringWithFormat:@"%.5f",lat];
                NSString * strLongitude=[NSString stringWithFormat:@"%.5f",lng];
                NSString * strAltitude = [NSString stringWithFormat:@"%f",self.app.locationManager.location.altitude];

                [COMMON addLoading:self];
                WebServiceAPI *serviceObj = [WebServiceAPI new];
                [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",lat] withLng:[NSString stringWithFormat:@"%f",lng]];
                
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    [COMMON removeProgressLoading];
                    BOOL isExit = NO;
                    if ([response isKindOfClass:[NSDictionary class]]) {
                        if ([response[@"results"] isKindOfClass: [NSArray class]]) {
                            
                            NSArray *retArr = response[@"results"];
                            
                            for (NSDictionary*mDic in retArr)
                            {
                                NSArray*tmpArr = mDic[@"types"];
                                
                                if ([tmpArr containsObject :@"administrative_area_level_1"])
                                {
                                    
                                    NSString * strLocation =  mDic[@"formatted_address"];
                                    
                                    //send...new address
                                    [PublicationOBJ sharedInstance].enableGeolocation = YES;
                                    [PublicationOBJ sharedInstance].latitude =strLatitude;
                                    [PublicationOBJ sharedInstance].longtitude = strLongitude;
                                    [PublicationOBJ sharedInstance].address =strLocation;
                                    [PublicationOBJ sharedInstance].altitude = strAltitude;
                                    [[PublicationOBJ sharedInstance] uploadData:@{@"iSharing":[NSNumber numberWithInt:0]}];
                                    isExit =YES;
                                    [self doShowAlert];
                                    
                                    break;
                                }
                            }
                            
                        }
                    }
                    if (isExit == NO) {
                        [PublicationOBJ sharedInstance].enableGeolocation = YES;
                        [PublicationOBJ sharedInstance].latitude =strLatitude;
                        [PublicationOBJ sharedInstance].longtitude = strLongitude;
                        [PublicationOBJ sharedInstance].altitude = strAltitude;
                        [[PublicationOBJ sharedInstance] uploadData:@{@"iSharing":[NSNumber numberWithInt:0]}];
                        [self doShowAlert];
                        
                    }
                };
                

            }
                break;
                
            default:
                break;
        }
    }

}
-(void)setThemeTabVC:(UIViewController*)vc
{
    UIButton  *btnTmp1 = (UIButton*)[self.view viewWithTag: (START_SUB_NAV_TAG*2)];
    UIButton  *btnTmp2 = (UIButton*)[self.view viewWithTag: ((START_SUB_NAV_TAG+1)*2)];
    UIButton  *btnTmp3 = (UIButton*)[self.view viewWithTag: ((START_SUB_NAV_TAG+2)*2)];
    UIButton  *btnTmp4 = (UIButton*)[self.view viewWithTag: ((START_SUB_NAV_TAG+3)*2)];
    
    UILabel  *txtTag1 = (UILabel*)[self.view viewWithTag:(START_SUB_NAV_TAG+0)*3];
    UILabel  *txtTag2 = (UILabel*)[self.view viewWithTag:(START_SUB_NAV_TAG+1)*3];
    UILabel  *txtTag3 = (UILabel*)[self.view viewWithTag:(START_SUB_NAV_TAG+2)*3];
    UILabel  *txtTag4 = (UILabel*)[self.view viewWithTag:(START_SUB_NAV_TAG+3)*3];
    //text
    txtTag1.text = str(strMur);
    txtTag2.text = str(strCarte);
    txtTag3.text = str(strDiscussion);
    txtTag4.text = str(strParametres);
    
    [btnTmp1 setImageWithImgStateNomal:@"live_ic_mur" ImgStateSelected:@"live_ic_mur_active"];
    [btnTmp2 setImageWithImgStateNomal:@"live_ic_map" ImgStateSelected:@"live_ic_map_active"];
    [btnTmp3 setImageWithImgStateNomal:@"live_ic_discussion" ImgStateSelected:@"live_ic_discussion_active"];
    [btnTmp4 setImageWithImgStateNomal:@"live_sos_button" ImgStateSelected:@"live_sos_button"];
    
    btnTmp1.selected=NO;
    btnTmp2.selected=NO;
    btnTmp3.selected=NO;
    btnTmp4.selected=NO;
    //color text
    txtTag1.textColor = [UIColor blackColor];
    txtTag2.textColor = [UIColor blackColor];
    txtTag3.textColor = [UIColor blackColor];
    txtTag4.textColor = [UIColor blackColor];
    
    if ([self isKindOfClass: [GroupEnterMurVC class]])
    {
        btnTmp1.selected=YES;
        txtTag1.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        [CommonObj sharedInstance].nbLiveMur=0;
    }
    else if ([self isKindOfClass: [MapGlobalVC class]])
    {
        btnTmp2.selected=YES;
        txtTag2.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);

    }
    else  if ([self isKindOfClass: [ChatVC class]])
    {
        btnTmp3.selected=YES;
        txtTag3.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        [CommonObj sharedInstance].nbLiveChat=0;

    }
    
//    [self.myTabVC badgingBtn:btnTmp1 count:[CommonObj sharedInstance].nbLiveMur];
//    
//    [self.myTabVC badgingBtn:btnTmp3 count:[CommonObj sharedInstance].nbLiveChat];
}
-(BOOL)fnCheckResponse:(NSDictionary*)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        if ([response[@"message"] isKindOfClass:[NSString class]]) {
            return YES;
        }
    }
    
    return NO;
}

-(void)checkAllow
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        BOOL theValue1 = NO;
        BOOL theValue2 = NO;
        BOOL theValue3 = NO;
        BOOL theValue4 = NO;
        BOOL isAdmin   = NO;

        NSString *strID = [NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
        NSString *table = @"";
        if (self.expectTarget == ISLOUNGE) {
            table = @"tb_hunt";
        }
        else
        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
            table = @"tb_group";

        }
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM %@ WHERE (c_user_id=%@ AND c_id=%@ ) ",table,sender_id, strID];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        while ([set_querry1 next])
        {
            
            theValue1 = [set_querry1 boolForColumn:@"c_allow_chat_add"];
            theValue2 = [set_querry1 boolForColumn:@"c_allow_chat_show"];
            theValue3 = [set_querry1 boolForColumn:@"c_allow_add"];
            theValue4 = [set_querry1 boolForColumn:@"c_allow_show"];
            isAdmin   = [set_querry1 boolForColumn:@"c_admin"];

        }
        if (isAdmin) {
            allow_add_chat = YES;
            allow_show_chat = YES;
            allow_add = YES;
            allow_show = YES;
        }
        else{
            allow_add_chat = theValue1;
            allow_show_chat = theValue2;
            allow_add = theValue3;
            allow_show = theValue4;
        }

       
    }];
}

-(void) fnRemoveObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)gotoAlertConfirmLiveHunt
{
    // 3 cases:
    //- No live event, display this popup
    //- 1 live event - > open the natura live
    //- many live event, display this popup
    NSInteger countLiveEvent = [LiveHuntOBJ sharedInstance].arrLiveHunt.count;
    if (countLiveEvent > 1) {
        
        AlertViewLiveHunt *vc = [[AlertViewLiveHunt alloc] initListLiveHunt];
        [vc setCallbackLive:^(NSInteger index,NSDictionary *dic)
         {
             if (dic) {
                 [self gotoLiveHunt:dic];
             }

         }];
        [vc showAlert];
    }
    else if (countLiveEvent == 1)
    {
        NSDictionary *dicLiveEvent = [LiveHuntOBJ sharedInstance].arrLiveHunt[0];
        [self gotoLiveHunt:dicLiveEvent];
    }
    else
    {
        AlertVC *vc = [[AlertVC alloc] initConfirmLiveHuntAlert];
        [vc doBlock:^(NSInteger index, NSString *strContent) {
            //cancel
            if (index == 0) {
                
            }
            //create livehunt
            else if (index == 1) {
                if ([self isKindOfClass: [ChassesCreateV2 class]])
                    return;
                [[ChassesCreateOBJ sharedInstance] resetParams];
                ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                viewController1.mParent = self;
                [self pushVC:viewController1 animate:NO expectTarget:ISMUR];
            }
            //rejoin livehunt
            else if (index == 2) {
                
                ChassesSearchVC *viewController1 = [[ChassesSearchVC alloc] initWithNibName:@"ChassesSearchVC" bundle:nil];
                viewController1.mParent = self;
                [self pushVC:viewController1 animate:NO expectTarget:ISLOUNGE];
            }
            
        }];
        [vc showAlert];
    }
    
}
-(void)gotoLiveHunt:(NSDictionary*)dicLiveEvent
{
    if (dicLiveEvent != nil) {
        
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
        MapGlobalVC *viewController1 = [sb instantiateViewControllerWithIdentifier:@"MapGlobalVC"];
        
        [LiveHuntOBJ sharedInstance].dicLiveHunt = dicLiveEvent;
        [GroupEnterOBJ sharedInstance].dictionaryGroup = dicLiveEvent;
        
        viewController1.isSubVC = YES;
        
        viewController1.isLiveHunt = YES;
        
        viewController1.expectTarget = ISLOUNGE;
        viewController1.mParent = self;
        [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:NO completion:^ {
            NSLog(@"COMPLETED");
        }];
    }
    
    NSString* strKey = [NSString stringWithFormat:@"Intersticeshowed_%@", dicLiveEvent[@"id"] ];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:strKey] == FALSE )
    {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey: strKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //show interstice
        [self fnShowInterstice];
    }

}
-(void) fnShowInterstice
{
   imgV = [UIImageView new];
    imgV.image = [UIImage imageNamed:@"livehunt_interstice.jpg"];
    
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIImageView *view = imgV;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    view.frame = app.window.frame;
    
    [[app window] addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [self performSelector:@selector(fnKillInterstice) withObject:nil afterDelay: 3];
    
}
-(void) fnKillInterstice
{
    [imgV removeFromSuperview];
}
@end
