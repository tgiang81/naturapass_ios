//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCellHeaderNotification.h"

@implementation TypeCellHeaderNotification
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [self.btnTick.layer setMasksToBounds:YES];
    self.btnTick.layer.cornerRadius= 16.0;
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
}
@end
