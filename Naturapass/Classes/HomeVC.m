//
//  HomeVC.m
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
//Home -> get cound number unread...chat
//Go in 1 discussion -> server response had read message ->
//Total - read -> remain.


#import "HomeVC.h"
#import "Define.h"
#import "UIView+MGBadgeView.h"
#import "GroupMesVC.h"
#import "GroupSearchVC.h"

#import "Amis_Demand_Invitation.h"
#import "ChassesMurVC.h"

#import "MurVC.h"
#import "ChatListe.h"
#import "SignalerTerminez.h"

#import "AppDelegate.h"
#import "Home_Amis_suggestion.h"
#import "Home_News.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASSharedTimeFormatter.h"
#import "NSDate+Extensions.h"
#import "CommonObj.h"

#import "AnimalsEntity.h"
#import "GroupEnterOBJ.h"
#import "CommentDetailVC.h"
#import "GroupEnterMurVC.h"
#import "GroupSettingMembres.h"
#import "ChasseSettingMembres.h"
#import "ChatVC.h"
#import "MapGlobalVC.h"
#import "AmisListVC.h"
#import "AmisAddScreen1.h"
#import "FriendInfoVC.h"
#import "Parameter_Publication.h"
#import <AFNetworking/AFNetworking.h>
#import "Parameter_ProfilVC.h"
#import "ChassesInvitationAttend.h"
#import "GroupInvitationAttend.h"
#import <MessageUI/MessageUI.h>
#import "FileHelper.h"
#import "ParameterVC.h"
#import "GroupCreateOBJ.h"
#import "ChassesCreateOBJ.h"
#import "FriendInfoVC.h"
#import "LiveHuntOBJ.h"
#import "UIAlertView+Blocks.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "ServiceHelper.h"
#import "ChassesParticipeVC.h"
#import "ChassesParticipeVC.h"
#import "TabBottomVC.h"
#import "OpinionzAlertView.h"
#import "CellHomeLiveHunt.h"
#import "ChassesCreateV2.h"

#import <CoreMotion/CoreMotion.h>
#import "SignalerTerminez.h"

#import "ChassesCreate_Step1.h"


static NSString *identifierCellHome = @"CellHome";

//GoogleMap

@interface HomeVC () <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
    OpinionzAlertView           *alertPublicWithLocation;
    
    UIImageView *imgV;
    __weak IBOutlet UILabel *lbLiveHuntHeader;
    __weak IBOutlet UILabel *lbLiveSignaler;

    __weak IBOutlet UIButton *btnDiscuss;
    __weak IBOutlet UIButton *btnGroup;
    __weak IBOutlet UIButton *btnChasses;
    __weak IBOutlet UIButton *btnAmis;
    
    __weak IBOutlet UIScrollView *scroll_Partner;
    __weak IBOutlet UIScrollView *scroll_Boutique;
    
    __weak IBOutlet UIScrollView *scroll_Suggestion_Amis;
    
    __weak IBOutlet UIScrollView *scroll_News;
    
    __weak IBOutlet UIPageControl *pagePartners;
    
    __weak IBOutlet UIPageControl *pageBoutiques;
    __weak IBOutlet UIPageControl *pageNews;
    
    __weak IBOutlet UIView *viewBottom;
    
    __weak IBOutlet NSLayoutConstraint *constraintHeightAmis;
    __weak IBOutlet NSLayoutConstraint *constraintHeightNews;
    
    int iNumberPartners, iNumberBoutique,iNumberNews;
    NSArray *arrFriendSuggestion;
    NSMutableArray *myArrNews;
    
    BOOL isGoing;
    
    __weak IBOutlet UIView *vHuntView;
    __weak IBOutlet NSLayoutConstraint *constraintLiveHunt;
    __weak IBOutlet NSLayoutConstraint *constraintTopLiveHunt;
    dispatch_queue_t backgroundQueue;
    AppDelegate *appDelegate;
    NSDictionary *tmpDicEventSpecial;
    __weak IBOutlet UILabel *lbTitleMur;
    __weak IBOutlet UILabel *lbTitleCarte;
    __weak IBOutlet UILabel *lbTitleDiscustions;
    __weak IBOutlet UILabel *lbTitleGroupes;
    __weak IBOutlet UILabel *lbTitleAgenda;
    __weak IBOutlet UILabel *lbTitleAmis;
    __weak IBOutlet UILabel *lbTitleParametres;
    __weak IBOutlet UILabel *lbTitleAIDE;
    __weak IBOutlet UIButton *btnTitleDeconnexion;
    BOOL isRequesting;
    __weak IBOutlet NSLayoutConstraint *constraintHeightTableLiveHunt;
    
    IBOutlet UITableView *tableControl;
    IBOutlet UIView *vSuggestAmis;
}

@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (weak, nonatomic)  NSTimer *timerPartner;
@property (weak, nonatomic)  NSTimer *timerBoutique;
@property (weak, nonatomic)  NSTimer *timerNews;
@property (strong, nonatomic)  NSArray *dicLiveHunt;
@property (weak, nonatomic)  NSTimer *timerLiveHuntCheck;
@end

@implementation HomeVC

- (void)scrollViewDidScroll:(UIScrollView *)scrollView  {
    // Calculate current page
    
    if (scroll_News == scrollView) {
        
        CGRect visibleBounds = scroll_News.bounds;
        NSInteger index = (NSInteger) (floorf(CGRectGetMidX(visibleBounds) / CGRectGetWidth(visibleBounds)));
        if (index < 0) index = 0;
        
        if (index > myArrNews.count - 1) index = myArrNews.count - 1;
        pageNews.currentPage = (int) index;
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    if (scroll_Partner == scrollView) {
        CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
        // Change the indicator
        pagePartners.currentPage = (int) currentPage;
    }
    
    if (scroll_Boutique == scrollView) {
        CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
        // Change the indicator
        pageBoutiques.currentPage = (int) currentPage;
    }
    if (scroll_News == scrollView) {
        CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
        // Change the indicator
        pageNews.currentPage = (int) currentPage;
    }
    
}

-(void) badgingBtn:(UIButton*)btn :(int) iNumber
{
    [btn.badgeView setBadgeValue:iNumber];
    [btn.badgeView setOutlineWidth:1.0];
    [btn.badgeView setPosition:MGBadgePositionTopRight];
    [btn.badgeView setOutlineColor:[UIColor whiteColor]];
    [btn.badgeView setBadgeColor:[UIColor redColor]];
    [btn.badgeView setTextColor:[UIColor whiteColor]];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitleMur.text= str(strMUR);
    lbTitleCarte.text= str(strCARTE);
    lbTitleDiscustions.text = str(strDISCUSSIONS);
    lbTitleGroupes.text = str(strGROUPES);
    lbTitleAgenda.text = str(strCHANTIERS);
    lbTitleAmis.text = str(strAMIS);
    lbTitleParametres.text = str(strPARAMETRES);
    lbTitleAIDE.text = str(strAIDE);
    [btnTitleDeconnexion setTitle:str(strDeconnexion) forState:UIControlStateNormal];
    
    //ggtt customize
    vSuggestAmis.hidden = YES;
    constraintHeightAmis.constant = 0;
    constraintHeightNews.constant = 0;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    arrFriendSuggestion = nil;
    scroll_Partner.delegate = self;
    scroll_Boutique.delegate = self;
    scroll_News.delegate = self;
    
    pageNews.currentPage = 0;
    pageNews.hidden =YES;
    self.myScrollView.showsVerticalScrollIndicator = NO;
    myArrNews = [NSMutableArray new];
    iNumberBoutique = 5;
    
    isGoing = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fnUpdateAllNotifications) name: NOTIFICATION_REQUEST_UPDATING object:nil];
    
    //load shape full
    backgroundQueue = dispatch_queue_create("com.naturapass.myqueue.Home", 0);
    
    //this one is using operation background
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
    
    
    [tableControl registerNib:[UINib nibWithNibName:@"CellHomeLiveHunt" bundle:nil] forCellReuseIdentifier:identifierCellHome];
    
    //    tableControl.estimatedRowHeight = 40;
    //    tableControl.rowHeight = UITableViewAutomaticDimension;
    
    //add shotcut
    
    NSArray *nibArrayTab = [[NSBundle mainBundle]loadNibNamed:@"TabBottomVC" owner:self options:nil];
    
    
    TabBottomVC* bottomVC = (TabBottomVC*)[nibArrayTab objectAtIndex:0];
    
    __weak HomeVC *wself = self;
    [bottomVC setMyCallBack:^(UIButton*btn)
     {
         [wself tabBottomVCAction:btn];
     }];
    
    
    bottomVC.translatesAutoresizingMaskIntoConstraints = NO;
    
    [viewBottom addSubview:bottomVC];
    
    [viewBottom addConstraints:[NSLayoutConstraint
                                constraintsWithVisualFormat:@"V:|[bottomVC]|"
                                options:NSLayoutFormatDirectionLeadingToTrailing
                                metrics:nil
                                views:NSDictionaryOfVariableBindings(bottomVC)]];
    
    
    
    [viewBottom addConstraints:[NSLayoutConstraint
                                constraintsWithVisualFormat:@"H:|[bottomVC]|"
                                options:NSLayoutFormatDirectionLeadingToTrailing
                                metrics:nil
                                views:NSDictionaryOfVariableBindings(bottomVC)]];
    
    [viewBottom setNeedsDisplay];
    [self attributeTextSignaler];
//    _activityManager = [CMMotionActivityManager new];
//
//    if ([CMMotionActivityManager isActivityAvailable])
//    {
//
//        [self.activityManager startActivityUpdatesToQueue: [NSOperationQueue mainQueue] withHandler:^(CMMotionActivity * data) {
//
//            dispatch_async(dispatch_get_main_queue(), ^{
//
//                if(data.stationary == true){
//                    lbCounting.text = @"stationary";
//                } else if (data.walking == true){
//                    lbCounting.text = @"walking";
//                } else if (data.running == true){
//                    lbCounting.text = @"running";
//                } else if (data.automotive == true){
//                    lbCounting.text = @"automotive";
//                }
//            });
//
//        }];
//    }
//    
//    if ([CMPedometer isStepCountingAvailable]) {
//        self.pedometer = [CMPedometer new];
//    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
//    if ([self.myScrollView observationInfo]) {
//        [self.myScrollView removeObserver:self forKeyPath:@"contentSize" context:NULL];
//    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isRequesting = NO;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
}


//Layout loaded.
-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //load cache live hunt:
    
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],LIVE_HUNT_HOME_SAVE)   ];
    
    NSArray *fDic = [NSArray arrayWithContentsOfFile:strPath];
    
    if (fDic!= nil && fDic.count > 0) {
        [self doShowhideLiveHuntWithData:fDic];
    }else{
        [self showLiveHuntView:NO];
    }
    
    
    //mld_ test url
    /* NSUserDefaults *userData = [[NSUserDefaults alloc] initWithSuiteName:@"group.fr.e-conception.naturapass"];
     
     NSDictionary *dicExtentions = [userData objectForKey:@"share_extension"];
     NSArray *alllKey =  dicExtentions.allKeys;
     NSDictionary *dic = dicExtentions[alllKey[0]];
     if (alllKey.count > 0) {
     
     if (dic[@"type"] != nil ) {
     //open image
     if ([self isKindOfClass: [SignalerTerminez class]])
     return;
     SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
     viewController1.expectTarget = ISMUR;
     viewController1.dicShareExtention = dic;
     [self pushVC:viewController1];
     
     }
     
     }
     */
    /*
     //Test Notification
     NSString *jsonString = @"{\"aps\":{\"alert\":\"publication in group or lounge\",\"badge\":1,\"sound\":\"default\"},\"element\":\"message\",\"notification_id\":\"<null>\",\"object_id\" : 600,\"publication_id\" : 31711,\"projectID\" : 541284815128,\"type\":\"group.publication.new\"}";
     NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
     id userInfo = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
     NSString *strAlert = [userInfo[@"aps"][@"alert"] emo_emojiString];
     [AZNotification showNotificationWithTitle:strAlert controller:self notificationType:AZNotificationTypeMessage];
     [appDelegate cacheLocalNotify:userInfo];
     */
    [[NSNotificationCenter defaultCenter] postNotificationName:@"exitmapview" object:nil];
    
    [self.myScrollView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    //1 time
    if (isGoing) {
        
        isGoing = NO;
        [self fnGetPublicationColor];
        [[MapDataDownloader sharedInstance] fnGetObservationTree];
    }
    
    //Do update data...
    if (!self.timerPartner) {
        [self fnPartners];
    }
    
    if (!self.timerBoutique) {
        
        [self fnBoutique];
        
    }
    
    if (!self.timerNews) {
        [self loadNewsCache];
    }
    
    [self fnGetNews];
    
    [self gotoHomeItem];
    
    //Show livehunt from cache...update later if have API.
    
    [self fnGetLiveHunt];
    
    [self fnAmisSuggestion];
    
    
}
-(void)attributeTextSignaler
{
    NSMutableAttributedString *strN = [[NSMutableAttributedString alloc] initWithString:@"\n"];

    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:@"CRÉER UNE CHASSE... "];
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"SUIVEZ-VOTRE CHASSE EN "];
    NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"TEMPS RÉEL "];
    NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:@"ET ÉCHANGEZ AVEC "];
    NSMutableAttributedString *str5 = [[NSMutableAttributedString alloc] initWithString:@"VOS PARTENAIRES!"];

    [str1 addAttribute:NSFontAttributeName
                 value:FONT_HELVETICANEUE_BOLD(18)
                 range:NSMakeRange(0, str1.length-1)];
    [str2 addAttribute:NSFontAttributeName
                 value:FONT_HELVETICANEUE(10)
                 range:NSMakeRange(0, str2.length-1)];
    [str3 addAttribute:NSFontAttributeName
                 value:FONT_HELVETICANEUE_BOLD(10)
                 range:NSMakeRange(0, str3.length-1)];
    [str4 addAttribute:NSFontAttributeName
                 value:FONT_HELVETICANEUE(10)
                 range:NSMakeRange(0, str4.length-1)];
    [str5 addAttribute:NSFontAttributeName
                 value:FONT_HELVETICANEUE_BOLD(10)
                 range:NSMakeRange(0, str4.length-1)];
    [str1 appendAttributedString:strN];
    [str1 appendAttributedString:str2];
    [str1 appendAttributedString:str3];
    [str1 appendAttributedString:strN];
    [str1 appendAttributedString:str4];
    [str1 appendAttributedString:str5];

//    CRÉER UNE CHASSE...
//    SUIVEZ-VOTRE CHASSE EN TEMPS RÉEL
//    ET ÉCHANGEZ AVEC VOS PARTENAIRES!

    lbLiveSignaler.attributedText =str1;
}
-(IBAction)fnStartCounting:(id)sender
{
    /*
    if ([CMPedometer isStepCountingAvailable]) {
        [self.pedometer startPedometerUpdatesFromDate:[NSDate date]
                                          withHandler:^(CMPedometerData *pedometerData, NSError *error) {
                                              
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  ASLog(@"number of steps   => :%d, error:%@", [pedometerData.numberOfSteps intValue], error);
                                                  
                                                  lbSteps.text = [pedometerData.numberOfSteps stringValue];
                                              });
                                          }];
        
        //start counter
        NSDate *to   = [NSDate date];
        NSDate *from = [to dateByAddingTimeInterval:-(24. * 3600.)];
        
        [self.pedometer queryPedometerDataFromDate:from
                                            toDate:to
                                       withHandler:
         ^(CMPedometerData *pedometerData, NSError *error) {
             
             ASLog(@"number of steps   => :%d, error:%@", [pedometerData.numberOfSteps intValue], error);
             dispatch_async(dispatch_get_main_queue(), ^{
                 lbSteps.text = [pedometerData.numberOfSteps stringValue];

             });
         }];
    }
    */
}

-(void) doShowAlert
{
    alertPublicWithLocation = [[OpinionzAlertView alloc] initWithTitle:str(strAjoutDunRepere)
                                                               message:str(strVousAvezFaitUnePublicationGeo)
                                                     cancelButtonTitle:str(strOK)
                                                     otherButtonTitles:nil];
    alertPublicWithLocation.iconType = OpinionzAlertIconSuccess;
    alertPublicWithLocation.color = [UIColor colorWithRed:0.15 green:0.68 blue:0.38 alpha:1];
    
    [alertPublicWithLocation show];
    
}

-(IBAction)tabBottomVCAction:(UIButton*)btn
{
    [self viewWillDisappear:YES];
    
    [[PublicationOBJ sharedInstance]  resetParams];
    
    switch (btn.tag -START_SUB_NAV_TAG)
    {
        case 0://PHOTO
        {
            
            if ([self isKindOfClass: [SignalerTerminez class]])
                return;
            SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
            viewController1.autoShow = TYPE_PHOTO;
            viewController1.expectTarget = ISMUR;
            
            [self pushVC:viewController1];
            
        }
            break;
            
        case 1://
        {
            //VIDEO
            if ([self isKindOfClass: [SignalerTerminez class]])
                return;
            SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
            viewController1.autoShow = TYPE_VIDEO;
            viewController1.expectTarget = ISMUR;
            
            [self pushVC:viewController1];
        }
            break;
            
        case 2://LOCATION
        {
            double lat = appDelegate.locationManager.location.coordinate.latitude;
            double lng = appDelegate.locationManager.location.coordinate.longitude;
            
            NSString *strLatitude=[NSString stringWithFormat:@"%.5f",lat];
            NSString * strLongitude=[NSString stringWithFormat:@"%.5f",lng];
            NSString * strAltitude = [NSString stringWithFormat:@"%f",appDelegate.locationManager.location.altitude];
            
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [WebServiceAPI new];
            [serviceObj getInfoLocation:[NSString stringWithFormat:@"%f",lat] withLng:[NSString stringWithFormat:@"%f",lng]];
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [COMMON removeProgressLoading];
                BOOL isExit = NO;
                if ([response isKindOfClass:[NSDictionary class]]) {
                    if ([response[@"results"] isKindOfClass: [NSArray class]]) {
                        
                        NSArray *retArr = response[@"results"];
                        
                        for (NSDictionary*mDic in retArr)
                        {
                            NSArray*tmpArr = mDic[@"types"];
                            
                            if ([tmpArr containsObject :@"administrative_area_level_1"])
                            {
                                
                                NSString * strLocation =  mDic[@"formatted_address"];
                                
                                //send...new address
                                [PublicationOBJ sharedInstance].enableGeolocation = YES;
                                [PublicationOBJ sharedInstance].latitude =strLatitude;
                                [PublicationOBJ sharedInstance].longtitude = strLongitude;
                                [PublicationOBJ sharedInstance].address =strLocation;
                                [PublicationOBJ sharedInstance].altitude = strAltitude;
                                [[PublicationOBJ sharedInstance] uploadData:@{@"iSharing":[NSNumber numberWithInt:0]}];
                                isExit =YES;
                                [self doShowAlert];
                                
                                break;
                            }
                        }
                        
                    }
                }
                if (isExit == NO) {
                    [PublicationOBJ sharedInstance].enableGeolocation = YES;
                    [PublicationOBJ sharedInstance].latitude =strLatitude;
                    [PublicationOBJ sharedInstance].longtitude = strLongitude;
                    [PublicationOBJ sharedInstance].altitude = strAltitude;
                    [[PublicationOBJ sharedInstance] uploadData:@{@"iSharing":[NSNumber numberWithInt:0]}];
                    [self doShowAlert];
                    
                }
            };
            
        }
            break;
            
        default:
            break;
    }
    
}


-(void) showLiveHuntView:(BOOL) bShow
{
    if (bShow) {
        
        vHuntView.hidden = NO;
        
        constraintLiveHunt.constant = 64;
        constraintTopLiveHunt.constant =14;
    }else{
        vHuntView.hidden = YES;
        //hiden table view
        constraintHeightTableLiveHunt.constant = 0;
        constraintLiveHunt.constant = 0;
        constraintTopLiveHunt.constant =14;
    }
    [vHuntView setNeedsDisplay];

}

#pragma mark - LIVEHUNT TABLE
//? reset Livehunt resetParams


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.dicLiveHunt.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellHomeLiveHunt *cell = (CellHomeLiveHunt *)[tableControl dequeueReusableCellWithIdentifier:identifierCellHome];
    NSDictionary *dic = self.dicLiveHunt[indexPath.row];
    NSString *strName = [dic[@"name"] emo_emojiString];
    
    cell.lbName.text = strName;
    [cell.lbDate  setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: dic[@"meeting"][@"date_begin"] withDateEnd:dic[@"meeting"][@"date_end"]]];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void) fnKillInterstice
{
    [imgV removeFromSuperview];
}

-(void) fnShowInterstice
{
    imgV = [UIImageView new];
    imgV.image = [UIImage imageNamed:@"livehunt_interstice.jpg"];
    
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIImageView *view = imgV;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    view.frame = app.window.frame;
    
    [[app window] addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [self performSelector:@selector(fnKillInterstice) withObject:nil afterDelay: 3];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    constraintHeightTableLiveHunt.constant = 0;
    [self fnTextLabelLiveHunt];
    NSDictionary *dic = self.dicLiveHunt[indexPath.row];
    
    if (dic != nil) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
        MapGlobalVC *viewController1 = [sb instantiateViewControllerWithIdentifier:@"MapGlobalVC"];
        
        [LiveHuntOBJ sharedInstance].dicLiveHunt = dic;
        [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
        
        viewController1.isSubVC = YES;
        
        viewController1.isLiveHunt = YES;
        
        viewController1.expectTarget = ISLOUNGE;
        viewController1.mParent = self;
        
        [self pushVC: viewController1];
    }

    NSString* strKey = [NSString stringWithFormat:@"Intersticeshowed_%@", dic[@"id"] ];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:strKey] == FALSE )
    {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey: strKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //show interstice
        [self fnShowInterstice];
    }
}

#pragma mark - GET DATA

-(void) fnGetLiveHunt
{
    if (![COMMON isReachable]) {
        return;
    }
    
    //    dispatch_async(backgroundQueue, ^{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnGET_DASHBOARD_LIVE_HUNT];;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //update data
            if ([response[@"live"] isKindOfClass: [NSArray class]]) {
                
                NSArray *result = response[@"live"];
                
                //save for play offline
                // Path to save array data
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],LIVE_HUNT_HOME_SAVE)   ];
                // Write array
                [result writeToFile:strPath atomically:YES];

                [self doShowhideLiveHuntWithData:result];

                //check and Reschedule...
                //
                [appDelegate fnScheduleWithListLivehunt: result];
                
            }else if (response == nil) {
                
//                [self doShowhideLiveHuntWithData:@[] ];
            }
            
        });
    };
}

-(void) doShowhideLiveHuntWithData:(NSArray*) arrIn{
    if(arrIn.count > 0)
    {
        ASLog(@"%@", arrIn);
        
        //Sorting array via date...recent date at first.
        NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"meeting.date_begin"  ascending:YES];
        self.dicLiveHunt = [arrIn sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
        
        vHuntView.hidden = NO;

        if (arrIn.count >1)
        {
            [tableControl reloadData];
        }
        
        [self fnTextLabelLiveHunt];
        constraintLiveHunt.constant = 64;
        constraintTopLiveHunt.constant =14;
        

    }else{
        //No live hunt or expired.
        //Hide it.
        self.dicLiveHunt = arrIn;
        constraintLiveHunt.constant = 0;
        [self showLiveHuntView:NO];
    }
    [LiveHuntOBJ sharedInstance].arrLiveHunt = [self.dicLiveHunt copy];

    [vHuntView setNeedsDisplay];

}

-(void)fnTextLabelLiveHunt
{
    if (self.dicLiveHunt.count == 1) {
        NSDictionary *dic = self.dicLiveHunt[0];
        NSString *strName = [dic[@"name"] emo_emojiString];
        
        
        UIColor *color1 = [UIColor whiteColor];
        NSDictionary *attrsVal1 = @{ NSForegroundColorAttributeName : color1 };
        
        NSMutableAttributedString *text0 = [[NSMutableAttributedString alloc]   initWithString:[NSString stringWithFormat:@"%@ \n",strName] attributes:attrsVal1];
        
        [text0 appendAttributedString:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: dic[@"meeting"][@"date_begin"] withDateEnd:dic[@"meeting"][@"date_end"]]];
        
        [lbLiveHuntHeader setAttributedText:text0];
        constraintHeightTableLiveHunt.constant = 0;
    }
    else
    {
        UIColor *color1 = [UIColor whiteColor];
        NSDictionary *attrsVal1 = @{ NSForegroundColorAttributeName : color1 };
        NSString *strCopy = (constraintHeightTableLiveHunt.constant == 0)?@"Afficher ":@"Masquer ";
        NSMutableAttributedString *text0 = [[NSMutableAttributedString alloc] initWithString:strCopy];
        NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%lu Lives",(unsigned long)self.dicLiveHunt.count] attributes:attrsVal1];
        [text1 addAttribute:NSFontAttributeName
                      value:[UIFont boldSystemFontOfSize:19.0f]
                      range:NSMakeRange(0, text1.length)];
        
        
        NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc] initWithString:@"\nen cours" attributes:attrsVal1];
        
        [text0 appendAttributedString:text1];
        [text0 appendAttributedString:text2];
        [lbLiveHuntHeader setAttributedText:text0];
        //if >0 then caculator height.
        if (constraintHeightTableLiveHunt.constant > 0) {
            constraintHeightTableLiveHunt.constant = 40 * self.dicLiveHunt.count;
        }else{
            constraintHeightTableLiveHunt.constant = 0;
        }
        
    }
    
}
-(void) fnGetPublicationColor
{
    
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj getPublicationColor];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        if (!response) {
            return ;
        }
        if ([response[@"colors"] isKindOfClass: [NSArray class]] ) {
            [PublicationOBJ sharedInstance].dicColor = response;
            
            //save to file as TREE
            NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
            [response writeToFile:strPath atomically:YES];
        }
    };
    
}
/*
 "animals": [
 {
 "id": 2577,
 "name": "Accenteur alpin"
 },
 */

//1 time attached for each release
-(void) createAnimationList:(NSDictionary*) dic
{
    AnimalsEntity *obj = [AnimalsEntity MR_createEntity];
    obj.name = dic[@"name"];
    obj.myid =  [NSNumber numberWithInt:  [dic[@"id"] intValue] ];
    
    //    NSArray *mama = [AnimalsEntity MR_findAll];
}


/*
 {
 nbChasseInvitation = 1;
 nbGroupInvitation = 1;
 nbUnreadMessage = 5;
 nbUnreadNotification = 134;
 nbUserWaiting = 0;
 }
 
 */

//Check socket noti... => request get num...

-(void) fnUpdateAllNotifications
{
    [self badgingBtn:btnDiscuss :[CommonObj sharedInstance].nbUnreadMessage ];
    [self badgingBtn:btnGroup :[CommonObj sharedInstance].nbGroupInvitation ];
    [self badgingBtn:btnChasses :[CommonObj sharedInstance].nbChasseInvitation  ];
    [self badgingBtn:btnAmis :[CommonObj sharedInstance].nbUserWaiting ];
}


-(void)ShowNewsRandom
{
    //remove subview scroll news
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (UIView *view in scroll_News.subviews) {
            [view removeFromSuperview];
        }
        //show news
        
        iNumberNews = (int) myArrNews.count;
        
        scroll_News.contentSize = CGSizeMake(scroll_News.frame.size.width * iNumberNews, scroll_News.frame.size.height);
        scroll_News.pagingEnabled = TRUE;
        
        
        UIView * prev;
        
        for (int index = 0;  index < iNumberNews; index++) {
            
            NSDictionary*dic = myArrNews[index];
            
            Home_News * subview = [[[NSBundle mainBundle] loadNibNamed:@"Home_News" owner:self options:nil] objectAtIndex:0];
            
            subview.title.text = dic[@"title"];
            [subview.myContent setText:  dic[@"content"]];
            
            //LInk
            [subview.btnClick addTarget:self
                                 action:@selector(onClickNews:)
                       forControlEvents:UIControlEventTouchUpInside];
            subview.btnClick.tag = index + 100;
            
            //date
            NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            
            //"date": "2015-09-21T00:00:00+0200"
            NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"date"]];
            NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
            
            subview.date.text= [inputDates timeAgo];
            
            NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"][@"path"]];
            
            [subview.image sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
            
            //Adding view
            subview.translatesAutoresizingMaskIntoConstraints = NO;
            [scroll_News addSubview:subview];
            
            if (index == 0){
                [scroll_News addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[subview(==scroll_News)]"
                                                                                    options:0
                                                                                    metrics:nil
                                                                                      views:@{ @"subview":subview,
                                                                                               @"scroll_News":scroll_News
                                                                                               } ]];
                prev = subview;
                
            }
            else{
                [scroll_News addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev][subview(==prev)]"
                                                                                    options:0
                                                                                    metrics:nil
                                                                                      views:@{ @"subview":subview,
                                                                                               @"prev":prev
                                                                                               } ]];
                prev = subview;
            }
            
            [scroll_News addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[subview(==scroll_News)]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"subview":subview,
                                                                                           @"scroll_News":scroll_News
                                                                                           } ]];
            if (index == iNumberNews-1){
                [scroll_News addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev]|"
                                                                                    options:0
                                                                                    metrics:nil
                                                                                      views:@{ @"prev":prev } ]];
                
            }
            
        }
        
    });
}

-(void)onClickNews:(UIButton*) sender
{
    NSDictionary*dic = myArrNews[sender.tag - 100];
    //go to link dic[@"link"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:dic[@"link"]]];
    
}

-(void)loadNewsCache
{
    self.timerNews = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(scrollingNews) userInfo:nil repeats:YES];
    [myArrNews removeAllObjects];
    
    NSString *strPath = [FileHelper pathForApplicationDataFile:FILE_HOME_NEWS_SAVE];
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        for (NSDictionary*dic in arrTmp) {
            [myArrNews addObject:dic];
            
        }
        pageNews.numberOfPages = myArrNews.count;
        if (myArrNews.count>0) {
            pageNews.hidden =NO;
            
        }
        [self ShowNewsRandom];
    }
}

-(void) fnGetNews
{
    dispatch_async(backgroundQueue, ^{
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj fnGET_GET_NEWS];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            NSArray *arrNews = response[@"news"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (arrNews.count > 0) {
                    constraintHeightNews.constant = 249;
                    pageNews.hidden =NO;
                    
                    // Path to save array data
                    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_HOME_NEWS_SAVE)   ];
                    // Write array
                    [arrNews writeToFile:strPath atomically:YES];
                    
                    [myArrNews removeAllObjects];
                    
                    for (int i = 0; i < arrNews.count; i++) {
                        [myArrNews addObject:arrNews[i] ];
                    }
                    
                    pageNews.numberOfPages = myArrNews.count;
                    [self ShowNewsRandom];
                    
                }else{
                    
                    [scroll_News setTranslatesAutoresizingMaskIntoConstraints:NO];
                    
                    constraintHeightNews.constant = 0;
                    pageNews.hidden =YES;
                    
                }
                
            });
        };
        
    });
}

#pragma mark - Amis suggestion


-(void) fnAmisSuggestion
{
    dispatch_async(backgroundQueue, ^{
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj getFriendsRecommendations];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            /*
             {
             firstname = Phathv3;
             fullname = "Phathv3 Ha";
             id = 177;
             lastname = Ha;
             mutualFriends = 1;
             pertinence = "19.77";
             profilepicture = "/uploads/users/images/thumb/4f2b1db6af06206b4a096723dc89b81d12b0ead6.jpeg";
             usertag = "phathv3-ha";
             },
             
             fullname
             mutualFriends ami en commun
             
             
             https://naturapass.e-conception.fr/api/v2/users/174/friendships
             {"relation":{"mutualFriends":0,"friendship":{"state":1,"way":2}}}
             */
            
            //remove all subview...reloading
            
            //
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSArray *viewsToRemove = [scroll_Suggestion_Amis subviews];
                for (UIView *v in viewsToRemove) [v removeFromSuperview];
                
                
                arrFriendSuggestion =response [@"recommendations"];
                
                if (arrFriendSuggestion && arrFriendSuggestion.count > 0) {
                    vSuggestAmis.hidden = NO;
                    constraintHeightAmis.constant = 234;
                    [self.view updateConstraints];
                    
                    //reset
                    [[scroll_Suggestion_Amis subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
                    
                    float delta = CGRectGetWidth(scroll_Suggestion_Amis.frame)/3;
                    
                    for (int i=0; i < arrFriendSuggestion.count; i++) {
                        
                        NSDictionary*dic = arrFriendSuggestion[i];
                        
                        UIView *v =[UIView new];
                        v.frame = CGRectMake( i*delta, 0 , delta , constraintHeightAmis.constant);
                        
                        [scroll_Suggestion_Amis addSubview:v];
                        
                        Home_Amis_suggestion * Cell = [[[NSBundle mainBundle] loadNibNamed:@"Home_Amis_suggestion" owner:self options:nil] objectAtIndex:0];
                        
                        Cell.layer.cornerRadius = 3.0f;
                        Cell.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.9].CGColor;
                        Cell.layer.borderWidth = 1.0f;
                        
                        Cell.frame = CGRectMake( 0, 0 ,0, 0);
                        
                        [Cell setTranslatesAutoresizingMaskIntoConstraints:NO];
                        
                        [v addSubview:Cell];
                        //
                        //
                        [v addConstraint: [NSLayoutConstraint
                                           constraintWithItem:Cell attribute:NSLayoutAttributeTop
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:v
                                           attribute:NSLayoutAttributeTop
                                           multiplier:1.0 constant:0] ];
                        
                        [v addConstraint: [NSLayoutConstraint
                                           constraintWithItem:Cell attribute:NSLayoutAttributeLeading
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:v
                                           attribute:NSLayoutAttributeLeading
                                           multiplier:1.0 constant:0] ];
                        
                        
                        [v addConstraint: [NSLayoutConstraint
                                           constraintWithItem:Cell attribute:NSLayoutAttributeTrailing
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:v
                                           attribute:NSLayoutAttributeTrailing
                                           multiplier:1.0 constant:0] ];
                        
                        [v addConstraint: [NSLayoutConstraint
                                           constraintWithItem:Cell attribute:NSLayoutAttributeBottom
                                           relatedBy:NSLayoutRelationEqual
                                           toItem:v
                                           attribute:NSLayoutAttributeBottom
                                           multiplier:1.0 constant:0] ];
                        
                        
                        [Cell.btnAjouter addTarget:self
                                            action:@selector(onClickAjouter:)
                                  forControlEvents:UIControlEventTouchUpInside];
                        
                        Cell.btnAjouter.tag = 100+ i;
                        
                        [Cell.btnFriendInfo addTarget:self
                                               action:@selector(onClickFriendInfo:)
                                     forControlEvents:UIControlEventTouchUpInside];
                        
                        Cell.btnFriendInfo.tag = 900+ i;
                        
                        
                        NSString *fullName = dic[@"firstname"];
                        NSString *strFriends;
                        
                        ASLog(@"%@",fullName);
                        if ([dic[@"mutualFriends"] intValue] >1) {
                            strFriends = [NSString stringWithFormat:@"%d %@", [dic[@"mutualFriends"] intValue],str(stramis_en_commun) ];
                        }
                        else
                        {
                            strFriends = [NSString stringWithFormat:@"%d %@", [dic[@"mutualFriends"] intValue],str(stramis_en_commun) ];
                        }
                        
                        UIColor *color1 = [UIColor blackColor];
                        
                        NSMutableAttributedString *text0 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ \n",fullName]];
                        
                        NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ \n",strFriends]];
                        
                        [text0 addAttribute:NSFontAttributeName
                                      value:[UIFont boldSystemFontOfSize:13.0f]
                                      range:NSMakeRange(0, text0.length-1)];
                        
                        
                        [text0 appendAttributedString:text1];
                        
                        [Cell.amis_num setAttributedText:text0];
                        
                        NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
                        
                        [Cell.image sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
                    }
                    
                    [scroll_Suggestion_Amis setContentSize:CGSizeMake(arrFriendSuggestion.count*delta, CGRectGetHeight(scroll_Suggestion_Amis.frame))];
                    [scroll_Suggestion_Amis setPagingEnabled:YES];
                    
                }else{
                    
                    [scroll_Suggestion_Amis setTranslatesAutoresizingMaskIntoConstraints:NO];
                    vSuggestAmis.hidden = YES;
                    constraintHeightAmis.constant = 0;
                }
            });
            
            
            
            
        };
    });
}

#pragma mark - PARTNER& BOUTIQUE
-(void) fnBoutique
{
    //Demo
    NSArray *arrTest = @[@"produit1.jpg",@"produit2.jpg",@"produit3.jpg",@"produit4.jpg",@"produit5.jpg"];
    
    pageBoutiques.numberOfPages = arrTest.count;
    pageBoutiques.currentPage = 0;
    
    float delta = CGRectGetWidth(scroll_Boutique.frame);
    UIButton * prev;
    for (int i=0; i < iNumberBoutique; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(onClickBoutique:)
         forControlEvents:UIControlEventTouchUpInside];
        
        button.frame = CGRectMake( i*delta, 0 , CGRectGetWidth(scroll_Boutique.frame), CGRectGetHeight(scroll_Boutique.frame));
        
        [button setBackgroundImage: [UIImage imageNamed:arrTest[i] ] forState:UIControlStateNormal];
        
        
//        [scroll_Boutique addSubview:button];
        //Adding view
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [scroll_Boutique addSubview:button];
        
        if (i == 0){
            [scroll_Boutique addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button(==scroll_Boutique)]"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{ @"button":button,
                                                                                              @"scroll_Boutique":scroll_Boutique
                                                                                              } ]];
            prev = button;
            
        }
        else{
            [scroll_Boutique addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev][button(==prev)]"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{ @"button":button,
                                                                                              @"prev":prev
                                                                                              } ]];
            prev = button;
        }
        
        [scroll_Boutique addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[button(==scroll_Boutique)]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:@{ @"button":button,
                                                                                          @"scroll_Boutique":scroll_Boutique
                                                                                          } ]];
        if (i == iNumberBoutique-1){
            [scroll_Boutique addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev]|"
                                                                                   options:0
                                                                                   metrics:nil
                                                                                     views:@{ @"prev":prev } ]];
            
        }    }
    
    [scroll_Boutique setContentSize:CGSizeMake(iNumberBoutique * delta, CGRectGetHeight(scroll_Boutique.frame))];
    
    // enable timer after each 2 seconds for scrolling.
    self.timerBoutique = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingBoutique) userInfo:nil repeats:YES];
}

-(void) fnPartners
{
    //Demo
    NSArray *arrTest = @[@"Browning.jpg",@"remington.jpg",@"opinel.jpg",@"leica.jpg",@"sauer.jpg",@"aimpoint.jpg",@"beretta.jpg", @"tunet.jpg", @"winchester.jpg"];
    iNumberPartners = (int) arrTest.count;
    
    pagePartners.numberOfPages = arrTest.count;
    pagePartners.currentPage = 0;
    
    float delta = CGRectGetWidth(scroll_Partner.frame);
    UIButton * prev;
    for (int i=0; i < iNumberPartners; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self
                   action:@selector(onClickPartner:)
         forControlEvents:UIControlEventTouchUpInside];
        
        button.tag = 10+i;
        
        button.frame = CGRectMake( i*delta, 0 , CGRectGetWidth(scroll_Partner.frame), CGRectGetHeight(scroll_Partner.frame));
        //no Scale... aspect fill.
        button.contentMode = UIViewContentModeScaleAspectFit;
        
        [button setImage: [UIImage imageNamed:arrTest[i] ] forState:UIControlStateNormal];
        
//        [scroll_Partner addSubview:button];
        //Adding view
        button.translatesAutoresizingMaskIntoConstraints = NO;
        [scroll_Partner addSubview:button];
        
        if (i == 0){
            [scroll_Partner addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button(==scroll_Partner)]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"button":button,
                                                                                           @"scroll_Partner":scroll_Partner
                                                                                           } ]];
            prev = button;
            
        }
        else{
            [scroll_Partner addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev][button(==prev)]"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"button":button,
                                                                                           @"prev":prev
                                                                                           } ]];
            prev = button;
        }
        
        [scroll_Partner addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[button(==scroll_Partner)]|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:@{ @"button":button,
                                                                                       @"scroll_Partner":scroll_Partner
                                                                                       } ]];
        if (i == iNumberPartners-1){
            [scroll_Partner addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[prev]|"
                                                                                options:0
                                                                                metrics:nil
                                                                                  views:@{ @"prev":prev } ]];
            
        }
    }
    
    [scroll_Partner setContentSize:CGSizeMake(iNumberPartners * delta, CGRectGetHeight(scroll_Partner.frame))];
    
    
    // enable timer after each 2 seconds for scrolling.
    self.timerPartner = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingPartner) userInfo:nil repeats:YES];
    
}

- (void)scrollingBoutique {
    // access the scroll view with the tag
    UIScrollView *scrMain = scroll_Boutique;
    
    CGFloat contentOffset = scrMain.contentOffset.x;
    
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    
    if( nextPage!=iNumberBoutique )  {
        
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
    } else {
        [scroll_Boutique scrollRectToVisible:CGRectMake(0,0,scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
    }
}


- (void)scrollingPartner {
    // access the scroll view with the tag
    UIScrollView *scrMain = scroll_Partner;
    
    CGFloat contentOffset = scrMain.contentOffset.x;
    
    int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
    
    if( nextPage!=iNumberPartners )  {
        
        [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
    } else {
        [scroll_Partner scrollRectToVisible:CGRectMake(0,0,scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
    }
}

- (void)scrollingNews {
    // access the scroll view with the tag
    
    CGFloat contentOffset = scroll_News.contentOffset.x;
    
    int nextPage = (int)(contentOffset/scroll_News.frame.size.width) + 1 ;
    
    if( nextPage!=iNumberNews )  {
        
        [scroll_News scrollRectToVisible:CGRectMake(nextPage*scroll_News.frame.size.width, 0, scroll_News.frame.size.width, scroll_News.frame.size.height) animated:YES];
    } else {
        [self ShowNewsRandom];
        [scroll_News scrollRectToVisible:CGRectMake(0,0,scroll_News.frame.size.width, scroll_News.frame.size.height) animated:YES];
    }
}

-(void)onClickBoutique:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://boutique.naturapass.com/"]];
    
}

//NSArray *arrTest = @[@"Browning.jpg",@"remington.jpg",@"opinel.jpg",@"leica.jpg",@"sauer.jpg"];

-(void)onClickPartner:(UIButton*) sender
{
    NSString *strLink =nil;
    
    switch (sender.tag) {
        case 10:
            strLink = @"http://browning.eu/";
            break;
        case 11:
            strLink = @"https://www.remington.com/";
            break;
        case 12:
            strLink = @"https://www.opinel.com/";
            break;
        case 13:
            strLink = @"https://fr.leica-camera.com/";
            break;
        case 14:
            strLink = @"http://www.sauer.de/fr/";
            break;
        case 15:
            strLink = @"http://www.aimpoint.com/";
            break;
        case 16:
            strLink = @"http://www.beretta.com/";
            break;
        case 17:
            strLink = @"https://www.tunet.fr/";
            break;
        case 18:
            strLink = @"https://en.winchesterint.com/";
            break;
        default:
            break;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strLink]];
}

-(void)onClickFriendInfo:(UIButton*) sender
{
    NSDictionary*dic = arrFriendSuggestion[sender.tag-900];
    
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic: dic];
    friend.expectTarget = ISMUR;
    
    [self pushVC:friend];
    
}
/*
 {"relation":{"mutualFriends":0,"friendship":{"state":1,"way":2}}}
 */

-(void)onClickAjouter:(UIButton*) sender
{
    
    //add friend
    //https://naturapass.e-conception.fr/api/v2/users/42/friendships
    
    NSDictionary*dic = arrFriendSuggestion[sender.tag-100];
    
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    dispatch_async(backgroundQueue, ^{
        [serviceObj postAjouterFriendShips:dic[@"id"]];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            dispatch_async(dispatch_get_main_queue(), ^{
                [COMMON removeProgressLoading];
                
                //                ASLog(@"%@",response);
                if ([response[@"relation"] isKindOfClass: [NSDictionary class]]) {
                    [self fnAmisSuggestion];
                }
            });
            
        };
        
    });
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    
    if (![@"contentSize" isEqualToString:keyPath])
        return;
    
    [self.myScrollView removeObserver:self forKeyPath:@"contentSize" context:NULL];
    
    CGSize contentSize = [[change valueForKey:NSKeyValueChangeNewKey] CGSizeValue];
    self.myScrollView.contentSize = contentSize;
    
    
}


- (IBAction)onClickHomeItem:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://MUR
        {
            
            MurVC *viewController1 = [[MurVC alloc] initWithNibName:@"MurVC" bundle:nil];
            viewController1.expectTarget = ISMUR;
            viewController1.isMoiMur = NO;
            viewController1.mParent = self;
            [self pushVC:viewController1];
            
//            ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
//            viewController1.expectTarget = ISMUR;
//            viewController1.mParent = self;
//            [self pushVC:viewController1];
        }
            break;
        case 1://CARTE
        {
            
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
            
            MapGlobalVC *viewController1 = [sb instantiateViewControllerWithIdentifier: @"MapGlobalVC_Carte"];
            
            viewController1.isSubVC = YES;
            viewController1.expectTarget = ISCARTE;
            viewController1.mParent = self;
            [self pushVC:viewController1];
        }
            break;
        case 2://DISCUSSION
        {
            
            ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
            viewController1.expectTarget = ISHOME;
            viewController1.mParent = self;
            [self pushVC:viewController1];
            
        }
            break;
        case 3://GROUP
        {
            GroupMesVC *viewController1 = [[GroupMesVC alloc] initWithNibName:@"GroupMesVC" bundle:nil];
            viewController1.expectTarget = ISGROUP;
            viewController1.mParent = self;
            
            [self pushVC:viewController1];
            /*
            SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
            viewController1.expectTarget = ISMUR;
            viewController1.mParent = self;
            [self pushVC:viewController1];
             */
        }
            break;
        case 4://CHASSES
        {
            
            ChassesMurVC *viewController1 = [[ChassesMurVC alloc] initWithNibName:@"ChassesMurVC" bundle:nil];
            viewController1.expectTarget = ISLOUNGE;
            viewController1.mParent = self;
            [self pushVC:viewController1];
        }
            break;
        case 5://AMIS
        {
            
            //            AmisListVC *viewController1 = [[AmisListVC alloc] initWithNibName:@"AmisListVC" bundle:nil];
            //            viewController1.mParent = self;
            AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
            viewController1.mParent = self;
            
            
            [self pushVC:viewController1];
            viewController1.expectTarget = ISAMIS;
        }
            break;
        case 6://PARAMETERS
        {
            
            Parameter_ProfilVC *viewController1 = [[Parameter_ProfilVC alloc] initWithNibName:@"Parameter_ProfilVC" bundle:nil];
            viewController1.expectTarget = ISPARAMTRES;
            viewController1.mParent = self;
            [self pushVC:viewController1];
        }
            break;
            
            /*
             
             user =     {
             courtesy = 1;
             email = "aide@naturapass.com";
             firstname = Aide;
             lastname = Naturapass;
             photo = "https://www.naturapass.com/uploads/users/images/resize/a4c276b8ba5b41a9580e7b4f2c8e98cff12b9860.jpeg";
             };
             
             
             
             
             firstname = "Ad\U00e9la\U00efde";
             friendship = 0;
             fullname = "Ad\U00e9la\U00efde Rufenacht";
             id = 1748;
             lastname = Rufenacht;
             mutualFriends = 0;
             profilepicture = "/uploads/users/images/thumb/ccc1d54e7c0fe15fd5f1e2f424ddc7f57d1cb723.jpeg";
             state = 0;
             usertag = "adelaide-rufenacht";
             
             
             */
            
        case 7://NATURAPASS
        {
            if (isRequesting) {
                return;
            }
            isRequesting = YES;
            WebServiceAPI* serviceAPI = [WebServiceAPI new];
            serviceAPI.onComplete =^(id response, int errCode)
            {
                isRequesting = NO;
                if ([response[@"user"] isKindOfClass: [NSDictionary class]]) {
                    
                    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
                    [friend setFriendDic: response[@"user"]];
                    friend.expectTarget = ISMUR;
                    [self pushVC:friend];
                    
                }
            };
            
            //ggtt
            [serviceAPI getFriendAction:@"4070"];
            
            //            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.naturapass.com/users/profile/aide-naturapass"]];
        }
            break;
        case 8://SUGGESTION
        {
            
        }
            break;
        case 9://DISCONNECTION
        {
            
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
        [appDelegate loadLoginView];
            
            
            //
        }
            break;
        case 10://LIVE HUNT
        {
            if (self.dicLiveHunt.count > 1)
            {
                //Show hide table list hunts.
                if (constraintHeightTableLiveHunt.constant == 0) {
                    constraintHeightTableLiveHunt.constant = 40 * self.dicLiveHunt.count;
                }else{
                    constraintHeightTableLiveHunt.constant = 0;
                }
                [self fnTextLabelLiveHunt];
            }
            else
            {
                NSDictionary *dic = self.dicLiveHunt[0];
                if (dic != nil) {
                    
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
                    MapGlobalVC *viewController1 = [sb instantiateViewControllerWithIdentifier:@"MapGlobalVC"];
                    
                    [LiveHuntOBJ sharedInstance].dicLiveHunt = dic;
                    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                    
                    viewController1.isSubVC = YES;
                    
                    viewController1.isLiveHunt = YES;
                    
                    viewController1.expectTarget = ISLOUNGE;
                    viewController1.mParent = self;
                    [self pushVC: viewController1];
                }
                
                NSString* strKey = [NSString stringWithFormat:@"Intersticeshowed_%@", dic[@"id"] ];
                
                if ([[NSUserDefaults standardUserDefaults] boolForKey:strKey] == FALSE )
                {
                    [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey: strKey];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //show interstice
                    [self fnShowInterstice];
                }

            }
        }
            break;
        case 11://GameFair
        {
            //            [self doCheckSpecialEventSuggestion];
        }
            break;
        case 12://Signaler
        {
            //
            [[ChassesCreateOBJ sharedInstance] resetParams];
            ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];

            [self pushVC: viewController1];

//                        [[ChassesCreateOBJ sharedInstance] resetParams];
//                        ChassesCreate_Step1 *viewController1 = [[ChassesCreate_Step1 alloc] initWithNibName:@"ChassesCreate_Step1" bundle:nil];
//            
//                        [self pushVC: viewController1];

        }
            break;
        default:
            break;
    }
    
}

-(void) doCheckSpecialEventSuggestion
{
    [COMMON addLoading:self];
    //join public event
    dispatch_async(backgroundQueue, ^(void) {
        //if user infor is nul or wating status => POST / PUT to join... then go to detail. to get mur detail.
        
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        
        [LoungeJoinAction fnGET_HUNT_INFORMATION: ID_EVENT_SPECIAL];
        
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            
            //            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            //
            //            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
            
            //fail....
            if (respone[@"lounge"]) {
                
                //cache tmp data
                tmpDicEventSpecial = respone[@"lounge"];
                
                if ([respone[@"lounge"][@"connected"] isKindOfClass: [NSDictionary class]]) {
                    NSInteger access= [respone[@"lounge"][@"connected"][@"access"] integerValue];
                    if (access == USER_WAITING_APPROVE) { //wait
                        //put access...
                        [self doJoinSpecialEventPut];
                        return;
                    }
                    else if (access > USER_WAITING_APPROVE)
                    {
                        //da la wall
                        [COMMON removeProgressLoading];
                        [self enterChassesMur];
                        return;
                    }
                }
            }
            //post
            [self doJoinSpecialEventPost];
        };
        
    });
}

-(void) doJoinSpecialEventPut
{
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:ID_EVENT_SPECIAL];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        
        [COMMON removeProgressLoading];
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        
        NSInteger access= [respone[@"access"] integerValue];
        if (access == 2) {
            //jump
            [self enterChasesWall];
            
        }
    };
    
}

-(void) doJoinSpecialEventPost
{
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:ID_EVENT_SPECIAL];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [COMMON removeProgressLoading];
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        
        NSInteger access= [respone[@"access"] integerValue];
        if (access == 2) {
            //jump
            [self enterChasesWall];
            
        }
    };
    
}
-(void)enterChasesWall
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app updateAllowShowAdd:tmpDicEventSpecial];
    
    ChassesParticipeVC  *vc=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
    vc.dicChassesItem = tmpDicEventSpecial;
    vc.isInvitionAttend =YES;
    vc.expectTarget = ISLOUNGE;
    [vc doCallback:^(NSString *strID) {
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
        [vc pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
    }];
    [self pushVC:vc];
    
}
-(void)enterChassesMur
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [app updateAllowShowAdd:tmpDicEventSpecial];
    
    GroupEnterMurVC *vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    vc.expectTarget = ISLOUNGE;
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
    [self pushVC:vc];
    
}
-(void) pushVC:(UIViewController*)vc
{
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - push notification
-(void)gotoScreenWithTypeNotification:(NSString*)type IdNoti:(NSString*)IdNoti dicNotif:(NSDictionary*)dic;
{
    _typeNotifi =type;
    _IdNotifi =IdNoti;
    _isNotifi=YES;
    _dicNotifi =dic;
}

//@tuyen @giangheolys @manhheolys khi tao agenda, minh nen convert local time sang France time roi truyen len
/*
"data":

{ "type": "live.date.changed", "start_time": <the new start date time>, "end_time": <the new end date time>, "object_id": <id of the live>, }
*/

-(void)gotoHomeItem
{
    BaseVC *vc =nil;
    BOOL getAPI =  NO;
    //reset
    [[ChassesCreateOBJ sharedInstance] resetParams];
    [[GroupCreateOBJ sharedInstance] resetParams];
    if(_isNotifi){
        
        NSString *pushType =_typeNotifi;
        if (  [pushType isEqualToString:publication_comment_liked]  ||  [pushType isEqualToString:publication_commented]||[pushType isEqualToString:publication_same_commented] || [pushType isEqualToString:publication_liked]|| [pushType isEqualToString:publication_processed_success] || [pushType isEqualToString:publication_shared]) {
            vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
            vc.expectTarget = ISMUR;
            
            
        }
        else if ([pushType isEqualToString:group_publication_new])
        {
            vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
            vc.expectTarget = ISGROUP;
            _IdNotifi = _dicNotifi[@"publication_id"];
        }
        else if ([pushType isEqualToString:lounge_publication_new])
        {
            
            /*
             When we received notification for a new publication in a livehunt, by clicking on the notification, user should be redirect to naturalive.
             If the live is not started (>48h) you can keep the current action (redirect to the publication)

             ONLY return live-id along push type lounge.publication.new if the hutn is a live hunt. If the hunt is not a live, please don't return hunt id along this push type.

             
             */
            //check if Dic of pushnotification is full...if not -> get dic with input id.

            //ggtt
//            UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:
//                                     [NSString stringWithFormat:@"%@",_dicNotifi]   delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//            [salonAlert show];
//
            
            if (_dicNotifi[@"live"]) {
                
                if (_dicNotifi[@"lounge"]) {
                    //catch string to JSON
                    NSString*strTmp = [_dicNotifi valueForKey:@"lounge"];
                    NSData *webData = [strTmp dataUsingEncoding:NSUTF8StringEncoding];
                    NSError *error;
                    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:webData options:0 error:&error];
                    ASLog(@"JSON DIct: %@", jsonDict);

                    [[GroupEnterOBJ sharedInstance] resetParams];
                    
                    [GroupEnterOBJ sharedInstance].dictionaryGroup=jsonDict;
                    [LiveHuntOBJ sharedInstance].dicLiveHunt = jsonDict;
                    [GroupEnterOBJ sharedInstance].dictionaryGroup = jsonDict;
                    
                    [appDelegate showLiveHunt];
                }

                /*
                vc.isLiveHunt = YES;
                //
                [COMMON addLoading:self];
                
                WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                [serviceObj getItemWithKind:MYCHASSE myid:_dicNotifi[@"live"]];
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    
                    [COMMON removeProgressLoading];
                    if (!response) {
                        return;
                    }
                    if([response isKindOfClass: [NSArray class] ]) {
                        return ;
                    }
                    
                    if (response[@"lounge"]) {
                        NSDictionary *dic = [response valueForKey:@"lounge"];
                        [[GroupEnterOBJ sharedInstance] resetParams];
                        
                        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
                        [LiveHuntOBJ sharedInstance].dicLiveHunt = dic;
                        [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                        
                        [appDelegate showLiveHunt];
                    }
                    
                };
                */
                
                return;
            }else{
                vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
                vc.expectTarget = ISLOUNGE;
                _IdNotifi = _dicNotifi[@"publication_id"];
            }

            
        }
        else if ([pushType isEqualToString:group_join_invited ])
        {
            vc = [[GroupInvitationAttend alloc] initWithNibName:@"GroupInvitationAttend" bundle:nil];
            vc.expectTarget = ISGROUP;
            
        }
        else if ([pushType isEqualToString:group_join_accepted])
        {
            vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
            vc.expectTarget = ISGROUP;
            [[GroupEnterOBJ sharedInstance] resetParams];
            getAPI = YES;
            
        }
        else if ([pushType isEqualToString:group_join_asked]||
                 [pushType isEqualToString:group_join_valid_asked])
        {
            vc = [[GroupSettingMembres alloc] initWithNibName:@"GroupSettingMembres" bundle:nil];
            vc.expectTarget = ISGROUP;
            //
            [[GroupEnterOBJ sharedInstance] resetParams];
            getAPI = YES;
            
        }
        else if ([pushType isEqualToString:lounge_join_invited ])
        {
            vc = [[ChassesInvitationAttend alloc] initWithNibName:@"ChassesInvitationAttend" bundle:nil];
            vc.expectTarget = ISLOUNGE;
        }
        else if ([pushType isEqualToString:lounge_join_accepted])
        {
            
            vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
            vc.expectTarget = ISLOUNGE;
            [[GroupEnterOBJ sharedInstance] resetParams];
            getAPI = YES;
            
        }
        else if ([pushType isEqualToString:lounge_join_asked]||
                 [pushType isEqualToString:lounge_join_valid_asked]||
                 [pushType isEqualToString:lounge_status_changed])
        {
            //validation
            vc = [[ChasseSettingMembres alloc] initWithNibName:@"ChasseSettingMembres" bundle:nil];
            vc.expectTarget = ISLOUNGE;
            [[GroupEnterOBJ sharedInstance] resetParams];
            getAPI = YES;
            
        }
        else if ([pushType isEqualToString:group_chat_new_message]||
                 [pushType isEqualToString:lounge_chat_new_message])
        {
            //se nhay den man hinh chat group
            vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
            if ([pushType isEqualToString:lounge_chat_new_message]) {
                vc.expectTarget = ISLOUNGE;
                if (_dicNotifi[@"live"]) {
                    vc.isLiveHunt = YES;
                    
                }
            }
            else
            {
                vc.expectTarget = ISGROUP;
                
            }
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=@{@"id": _IdNotifi};
            getAPI = YES;
        }
        
        else if ([pushType isEqualToString:discussion_new_message])
        {
            //se nhay den man hinh chat
            vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
            vc.expectTarget = ISDISCUSS;
        }
        else if([pushType isEqualToString:user_friendship_asked])
        {
            vc = [[Amis_Demand_Invitation alloc] initWithNibName:@"Amis_Demand_Invitation" bundle:nil];
            vc.expectTarget =ISAMIS;
        }
        // sau khi gui loi moi ket ban, neu nguoi do chap nhan, se nhan duoc thong bao confirmed va khi click vao se toi friendinfo
        else if([pushType isEqualToString:user_friendship_confirmed])
        {
            FriendInfoVC *vc1 = [[FriendInfoVC alloc] initWithNibName:@"FriendInfoVC" bundle:nil];
            vc1.expectTarget = ISMUR;
            [vc1 setFriendDic:_dicNotifi[@"sender"]];
            
            vc1.mParent = self;
            vc1.isNotifi=YES;
            vc1.IdNotifi =_IdNotifi;
            [self pushVC:vc1 ];
            return;
        }
        else if([pushType isEqualToString:lounge_discussion_new_message])
        {
            //se nhay den man hinh chat group
            vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
            vc.expectTarget = ISLOUNGE;
            
            if (_dicNotifi[@"live"])
            {
                vc.isLiveHunt = YES;
            }
            
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=@{@"id": _IdNotifi};
            getAPI = YES;
        }
        
        
        // push
        vc.isNotifi=_isNotifi;
        vc.IdNotifi =_IdNotifi;
        
        if (getAPI == YES) {
            [self gotoNotifi_withVC:vc];
        }
        else
        {
            [self pushVC:vc];
            self.isNotifi=!self.isNotifi;
            
        }
    }
}
-(void)gotoNotifi_withVC:(BaseVC *)vc
{
    
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if (vc.expectTarget== ISGROUP) {
        [serviceObj getItemWithKind:MYGROUP myid:vc.IdNotifi];
        
    }
    else
    {
        [serviceObj getItemWithKind:MYCHASSE myid:vc.IdNotifi];
        
    }
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        BOOL isExist = NO;
        
        if (vc.expectTarget== ISGROUP && response[@"group"]) {
            isExist = YES;
            NSDictionary *dic = [response valueForKey:@"group"];
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
            [self pushVC:vc];
            self.isNotifi=!self.isNotifi;
        }
        if (vc.expectTarget== ISLOUNGE && response[@"lounge"]) {
            isExist = YES;
            NSDictionary *dic = [response valueForKey:@"lounge"];
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
            [self pushVC:vc];
            self.isNotifi=!self.isNotifi;
        }
        
        if (!isExist && response[@"message"]) {
            //warning
            [AZNotification showNotificationWithTitle:response[@"message"] controller:self notificationType:AZNotificationTypeError];
        }
    };
}
//email
-(IBAction)sendMail:(id)sender
{
}


#pragma mark - mail compose delegate
-(void)mailComposeController:(MFMailComposeViewController *)controller
         didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
