//

//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AlertBaseVC.h"

@interface CarteTypeContentVC : AlertBaseVC
@property (nonatomic, assign) BOOL isCategoryON;
@property (nonatomic, strong) NSMutableArray *arrNodes;

-(id)initFromParent;


@end
