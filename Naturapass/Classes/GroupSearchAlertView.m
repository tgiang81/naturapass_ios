//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupSearchAlertView.h"
#import "Define.h"
#import "AppCommon.h"
@implementation GroupSearchAlertView
{
    NSString *_desc;
}
- (id)initWithTitle:(NSString*)title Desc:(NSString*)desc
{
    self = [super initWithNibName:@"GroupSearchAlertView" bundle:nil];
    if (self) {
        // Custom initialization
        _desc=desc;
    }
    return self;
}
#pragma mark - viewdid
-(void)viewDidLoad
{
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];
    subAlertView.layer.cornerRadius=3.0f;
    subAlertView.layer.masksToBounds=YES;
    
    [btnClose setTitle:str(strClose) forState:UIControlStateNormal];
    self.titleLabel.text=_desc;

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.exp ==ISGROUP) {
        btnClose.backgroundColor =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    }
    else if (self.exp == ISLOUNGE)
    {
        btnClose.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);

    }
}
#pragma callback
-(void)setCallback:(GroupSearchAlertViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(GroupSearchAlertViewCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark-  action
-(IBAction)fnClose:(id)sender
{
    if (_callback) {
        [self dismissViewControllerAnimated:YES completion:nil];
        _callback(0);
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)showInVC:(UIViewController*)vc
{
    [vc presentViewController:self animated:NO completion:^{
        
    }];
}
@end
