//
//  ChassesCreate_Step1.h
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChassesCreateBaseVC.h"
@interface ChassesCreate_Step1 :ChassesCreateBaseVC
{
    UIToolbar                   *pickerToolbar;
    
    //image
    UIImage                     *userImage;
    
    //photo
    NSData                      *imageData;
}
@end