//
//  ChassesCreateOBJ.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreateOBJ.h"
#import "MapDataDownloader.h"
#import "DatabaseManager.h"
#import "LiveHuntOBJ.h"
#import "AlertVC.h"
#import "NSDate+Extensions.h"

@implementation ChassesCreateOBJ

+ (ChassesCreateOBJ *) sharedInstance
{
    static ChassesCreateOBJ *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [(ChassesCreateOBJ *)[self alloc] init];
    }
    return sharedInstance;
}

-(void) resetParams
{
    self.accessKind = TYPE_MESSAGE;
    self.photo = @"";

    self.strName = @"";
    self.strComment = @"";
    self.strFin = @"";
    self.strDebut = @"";
    self.imgData = nil;
    self.isModifi =NO;
    self.latitude = nil;
    self.longitude = nil;
    self.address = nil;
    self.isMurPass=NO;
    self.listGroupAdmin = nil;
    self.attachment =nil;
    
    self.allow_add = NO;
    self.allow_show = NO;

    self.allow_chat_add = NO;
    self.allow_chat_show = NO;
    self.strID = nil;
}

-(void)fnSetData:(NSDictionary*)dic
{
    [[ChassesCreateOBJ sharedInstance] resetParams];
    NSString *strImage;
    if (dic[@"profilepicture"] != nil) {
        strImage=dic[@"profilepicture"];
    }else{
        strImage=dic[@"photo"];
    }
    if(![strImage containsString:IMAGE_ROOT_API])
    {
        strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,strImage];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    
    //load image async
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            [ChassesCreateOBJ sharedInstance].imgData =data;
        }
    }];
    [task resume];
    
    
    
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch ([dic[@"access"] integerValue]) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
        }
            break;
        default:
            break;
    }
    NSString *strEndDate= [self convertDateTime:[NSString stringWithFormat:@"%@",dic[@"endDate"]]];
    NSString *strMeetingDate= [self convertDateTime:[NSString stringWithFormat:@"%@",dic[@"meetingDate"]]];
    
    
    [ChassesCreateOBJ sharedInstance].strID =dic[@"id"];
    [ChassesCreateOBJ sharedInstance].strName =dic[@"name"];
    [ChassesCreateOBJ sharedInstance].strComment =   CHECKSTRING( dic[@"description"]);
    [ChassesCreateOBJ sharedInstance].accessKind =m_accessKind;
    [ChassesCreateOBJ sharedInstance].isModifi =YES;
    [ChassesCreateOBJ sharedInstance].strFin =strEndDate;
    [ChassesCreateOBJ sharedInstance].strDebut =strMeetingDate;
    
    [ChassesCreateOBJ sharedInstance].photo =dic[@"photo"];

    [ChassesCreateOBJ sharedInstance].latitude = dic[@"meetingAddress"][@"latitude"];
    [ChassesCreateOBJ sharedInstance].longitude = dic[@"meetingAddress"][@"longitude"];
    [ChassesCreateOBJ sharedInstance].address = dic[@"meetingAddress"][@"address"];
    if (dic[@"limitation"]) {
        [ChassesCreateOBJ sharedInstance].allow_add = [dic[@"limitation"][@"allow_add"] boolValue];
        [ChassesCreateOBJ sharedInstance].allow_show = [dic[@"limitation"][@"allow_show"] boolValue];
        
        [ChassesCreateOBJ sharedInstance].allow_chat_add = [dic[@"limitation"][@"allow_add_chat"] boolValue];
        [ChassesCreateOBJ sharedInstance].allow_chat_show = [dic[@"limitation"][@"allow_show_chat"] boolValue];
    }

}
#pragma mark - CONVERTDATE
-(NSString*)convertDateTime:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *dateFormatterTer = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd/MM/yyyy HH:mm" forFormatter: dateFormatterTer];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ dateFormatterTer stringFromDate:inputDates ];
    return outputStrings;
}
-(void)modifiChassesWithVC:(UIViewController*)viewcontroller
{
    BaseVC *vc =(BaseVC*)viewcontroller;
//    [UIAlertView showWithTitle:@"Modification la chasse"
//                        message:NSLocalizedString(@"Etes-vous sur de vouloir modifier votre chasse ?", @"")
//              cancelButtonTitle:@"Oui"
//              otherButtonTitles:@[@"Non"]
//                       tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                           if (buttonIndex == [alertView cancelButtonIndex]) {
                               ChassesCreateOBJ *obj = self;
                               
                               NSString *strAccess =[NSString stringWithFormat:@"%u",obj.accessKind];
                               NSDictionary *dicGeo = nil;
                               
                               if (obj.address)
                               {
                                   dicGeo =@{@"address":obj.address,
                                             @"latitude":obj.latitude,
                                             @"longitude":obj.longitude};
                               }else{
                                   dicGeo =@{@"address":@"",
                                             @"latitude":obj.latitude,
                                             @"longitude":obj.latitude};
                               }
                               
                               NSDictionary* postDict = @{@"lounge":   @{@"name":obj.strName,
                                                                         @"description":obj.strComment,
                                                                         @"geolocation":@"0", //1 option for admin to allow user select GEO or not... Now is disabled
                                                                         @"access":strAccess,
                                                                         @"meetingAddress":dicGeo,
                                                                         @"meetingDate":obj.strDebut,
                                                                         @"endDate":obj.strFin,
                                                                         //ggtt
                                                                         @"allow_add":  [NSString stringWithFormat:@"%u",obj.allow_add],
                                                                         @"allow_show": [NSString stringWithFormat:@"%u",obj.allow_show],
                                                                         @"allow_add_chat":  [NSString stringWithFormat:@"%u",obj.allow_chat_add],
                                                                         @"allow_show_chat": [NSString stringWithFormat:@"%u",obj.allow_chat_show]

                                                                         }};
                               
                               NSDictionary *attachment;
                               if (obj.imgData) {
                                   UIImage *image = [UIImage imageWithData:obj.imgData];
                                  NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //

                                   if (fileData) {
                                       attachment = @{@"kFileName": @"image.png",
                                                      @"kFileData": fileData};

                                   }
                               }
                               
                               if (obj.isModifi) {
                                   //put as json...without image
                                   [COMMON addLoading:vc];
                                   WebServiceAPI *serviceObj =[WebServiceAPI new];
                                   [serviceObj putLoungeAction:obj.strID withParametersDic:postDict imageattachment:attachment?attachment:nil];
                                   serviceObj.onComplete =^(NSDictionary *response, int errCode)
                                   {
                                       AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                       [app updateAllowShowAdd:response];

                                       if(attachment !=nil)
                                       {
                                           [self postLoungePhotos:obj.strID attachment:attachment withVC:vc];
                                       }
                                           if (response[@"sqlite"] ) {
                                               
                                               [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                                   NSArray *arrSqls = response[@"sqlite"] ;
                                                   
                                                   for (NSString*strQuerry in arrSqls) {
                                                       //correct API
                                                       NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                                                       
                                                       ASLog(@"%@", tmpStr);
                                                       
                                                       [db  executeUpdate:tmpStr];
                                                       
                                                   }
                                               }];
                                           }
                                           
                                           if (response[@"sqlite_agenda"] ) {
                                               
                                               [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                                   NSArray *arrSqls = response[@"sqlite"] ;
                                                   
                                                   for (NSString*strQuerry in arrSqls) {
                                                       //correct API
                                                       NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                                                       
                                                       ASLog(@"%@", tmpStr);
                                                       
                                                       [db  executeUpdate:tmpStr];
                                                       
                                                   }
                                               }];
                                           }
                                           
                                           
                                           if (response[@"sqlite_carte"] ) {
                                               
                                               [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                                   NSArray *arrSqls = response[@"sqlite"] ;
                                                   
                                                   for (NSString*strQuerry in arrSqls) {
                                                       //correct API
                                                       NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                                                       
                                                       ASLog(@"%@", tmpStr);
                                                       
                                                       [db  executeUpdate:tmpStr];
                                                       
                                                   }
                                               }];
                                           }
                                           
                                           //haizzz lounge////hunt???
                                           if ( [response[@"lounge"] isKindOfClass:[NSDictionary class]]) {
                                               
                                               
                                               WebServiceAPI *serviceObjXXX = [[WebServiceAPI alloc]init];
                                               
                                               [serviceObjXXX fnGET_DASHBOARD_LIVE_HUNT];;
                                               
                                               serviceObjXXX.onComplete = ^(NSDictionary*responseXXX, int errCode){
                                                   
                                                   [COMMON removeProgressLoading];
                                                   
                                                   //update data
                                                   if ([responseXXX[@"live"] isKindOfClass: [NSArray class]]) {
                                                       
                                                       NSArray *result = responseXXX[@"live"];
                                                       //save to cache file...will be loaded in Dashboard.
                                                       NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],LIVE_HUNT_HOME_SAVE)   ];
                                                       // Write array
                                                       [result writeToFile:strPath atomically:YES];
                                                       

                                                       //continue ...
                                                       [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                                                       //                        //update DB.
                                                       AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                                       [app fnScheduleWithListLivehunt: result];

                                                       //                        [app updateAllowShowAdd:response];
                                                       //
                                                       [ChassesCreateOBJ sharedInstance].strID  = response[@"lounge"][@"id"];
                                                       
                                                       [LiveHuntOBJ sharedInstance].dicLiveHunt = response[@"lounge"];
                                                       [GroupEnterOBJ sharedInstance].dictionaryGroup = response[@"lounge"];
                                                       //
                                                       
                                                       double numberOfSecondsIn48h = 172800.0;
                                                       NSDateFormatter *dateFormatter = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
                                                       
                                                       //
                                                       NSString *strStartDate= obj.strDebut;
                                                       NSDate *DebutDate = [dateFormatter dateFromString: strStartDate];
                                                       
                                                       // get current date
                                                       NSString *strCurrentDate = [NSDate currentStandardUTCTime];
                                                       NSDate *currentDate = [dateFormatter dateFromString:strCurrentDate];
                                                       
                                                       NSTimeInterval intervalBetweenNowAndStartDate = (NSTimeInterval)[DebutDate timeIntervalSinceDate:currentDate];

                                                       
                                                       
                                                       //check if The Hunt's startdate is more than 48h -> show Alert.
                                                       if(intervalBetweenNowAndStartDate > numberOfSecondsIn48h )
                                                       {
                                                           
                                                           AlertVC *vc = [[AlertVC alloc] initAlert48h];
                                                           
                                                           [vc doBlock:^(NSInteger index, NSString *strContent) {
                                                               
                                                               switch ((int)index) {
                                                                       //ACCUEIL -> Homepage
                                                                   case 0:
                                                                   {
                                                                       [viewcontroller.navigationController popToRootViewControllerAnimated:YES];
                                                                   }
                                                                       break;
                                                                       
                                                                   case 1:
                                                                   {
                                                                       //show my agenda
                                                                       AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                                                       [appDelegate showAgenda];
                                                                       
                                                                   }
                                                                       break;
                                                                       
                                                                   default:
                                                                       break;
                                                               }
                                                           }];
                                                           [vc showAlert];
                                                           
                                                           return;
                                                       } else {
                                                           //Get live hunt and go...to save time.
                                                           
                                                           
                                                           [app showLiveHunt];
                                                       }
                                                       
                                                   }else if (responseXXX == nil) {
                                                   }
                                                   
                                                   
                                                   
                                               };
                                               
                                           }

                                           [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES object: nil userInfo: nil];
                                           [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES_PASS object: nil userInfo: nil];
                                           
                                           [COMMON removeProgressLoading];
                                           
                                   };
                               }
//                           }}];

}

-(void)postLoungePhotos:(NSString*)strID attachment:(NSDictionary *)attachment withVC:(BaseVC*)vc
{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postLoungePhoto:strID withData:attachment];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [vc gotoback];
        
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_agenda]];// to get all agenda for Carter search.

        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES object: nil userInfo: nil];
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES_PASS object: nil userInfo: nil];

    };
    
}
@end
