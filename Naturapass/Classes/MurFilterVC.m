//
//  MurFilterVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurFilterVC.h"
#import "SlideCustomCell.h"
#import "DatabaseManager.h"
/*

 From real publication...get this default value to set default seleciton.
 If not -> 0
 */


@interface MurFilterVC ()
{
    NSMutableArray              *mesArr;
    NSMutableArray              *sharingGroupsArray;
    
    NSMutableArray              *sharingHuntsArray;

    int iSharing;

    __weak IBOutlet UISwitch *swFilterControl;
    IBOutlet UILabel *lbTitle;
    
}
@end

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";
static NSString *identifierSection3 = @"MyTableViewCell3";

extern BOOL bFilterON;

@implementation MurFilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strFILTRE);
    //Section1
    [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection1];

    //Section2
    [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection2];
    
    //Section3
    [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection3];

    sharingGroupsArray=[[NSMutableArray alloc]init];
    
    sharingHuntsArray=[[NSMutableArray alloc]init];
    
    //Filter
    NSMutableDictionary *dic1 = [@{@"categoryName":str(strMOI),
                                   @"categoryImage":@"mon_icon"} copy];
    
    NSMutableDictionary *dic2 = [@{@"categoryName":str(strMYFRIEND),
                                   @"categoryImage":@"mes2_icon"} copy];
    
    NSMutableDictionary *dic3 = [@{@"categoryName":str(strMEMBER),
                                   @"categoryImage":strIcon_All_Member} copy];
    
    mesArr =  [@[dic1,dic2,dic3] copy];
    
    //Offline -> disable setting...
    if (![COMMON isReachable]) {
        swFilterControl.userInteractionEnabled = NO;
        return;
        
        
    }else{
        swFilterControl.userInteractionEnabled = YES;
    }
    
    [self updateUI];
    
    //if online -> get data from server
    
    //update numbers:
    // 777:      * GET /users/{user}/friends?name=Nom
    
    NSDictionary *dicA = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];

    if( dicA !=nil)
    {
        iSharing = [dicA[@"iFilter"] intValue];
    }else{
        iSharing = 3;
    }
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //list shared mes groups
        
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE (c_admin=1 OR c_allow_show=1) AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmp) {
                if ([kDic[@"groupID"] intValue ]== strID) {
                    [sharingGroupsArray addObject:kDic];
                    break;
                }
            }
            
        }
        
        //list shared mes chasses
        
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
        
        
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_hunt WHERE (c_admin=1 OR c_allow_show=1) AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            
            int strID = [set_querry_hunt intForColumn:@"c_id"];
            //compare to get defaul
            for (NSDictionary*kDic in arrTmpHunt) {
                if ([kDic[@"huntID"] intValue ]== strID) {
                    [sharingHuntsArray addObject:kDic];
                    break;
                }
            }

            
        }
        [self.tableControl reloadData];

    }];

        
}


-(NSString*) convertArray2String:(NSArray*)arr forGroup:(BOOL) isGroup
{
    NSMutableString * strRet = [NSMutableString new];
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            [array addObject:dic[@"id"]];
        }
    }
    
    if (array.count > 0) {
        NSString *tmp = [array componentsJoinedByString:@","];
        
        [strRet appendString:tmp];
    }
    return strRet;
}


-(void) saveFilterObjs
{
    //SAVE FILTERING OBJECTS.
    //Save parameter...
    NSDictionary *dic = @{ @"isFilter": [NSNumber numberWithBool:bFilterON],
                           @"iFilter" : [NSNumber numberWithInt:iSharing],
                           };
    
    [[NSUserDefaults standardUserDefaults]setObject:dic forKey:
     [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    //Save mesgroup filter
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)   ];
    [sharingGroupsArray writeToFile:strPath atomically:YES];
    
    strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE)  ];
    [sharingHuntsArray writeToFile:strPath atomically:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    if ([COMMON isReachable]) {
        [self saveFilterObjs];
        
        //refresh MUR
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES object: nil userInfo: nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//overwrite
- (void)insertRowAtTop {
}

//overwrite
- (void)insertRowAtBottom {
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return mesArr.count;
            break;
            
        case 1:
            return sharingGroupsArray.count;
            break;
        case 2:
            return sharingHuntsArray.count;
            break;
            
        default:
            return 0;
            break;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 15;
    }else{
        return 50;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = nil;
    
    if (section==0) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 15)];
    }else if (section==1){
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 200, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont boldSystemFontOfSize:17]];

        headerLabel.text= str(strMesGroupes);
        [sectionView addSubview:headerLabel];
    }else{
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 200, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont boldSystemFontOfSize:17]];
        headerLabel.text= str(strMesChantier);
        [sectionView addSubview:headerLabel];
    }
    
    [sectionView setBackgroundColor: [UIColor whiteColor] ];

    //Divider on top
    UIImageView *dividerImage =[[UIImageView alloc]initWithFrame:CGRectMake(18, 10, 255, 1)];
    
    [dividerImage setImage:[UIImage imageNamed:@"divider4.png"]];
    
    [dividerImage setAlpha:1];
    [sectionView addSubview:dividerImage];
    
    return sectionView;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    SlideCustomCell *cell = nil;
    
    if (indexPath.section == 0) {
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];

    }else if (indexPath.section == 1){
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
    }else{
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3 forIndexPath:indexPath];

    }
    
    cell.backgroundColor=[UIColor whiteColor];
    
    if (indexPath.section == 0)
    {
            [cell.tickButton setHidden:NO];
            [cell.arrowImage setHidden:YES];
            
            [cell setListLabel:[[mesArr objectAtIndex:indexPath.row] valueForKey:@"categoryName"]];
            [cell setListImages:[[mesArr objectAtIndex:indexPath.row] valueForKey:@"categoryImage"]];
            
            //selection
            [cell.tickButton setTag: indexPath.row+10 ];
            
            [cell.tickButton setSelected:NO];
            
            switch (iSharing) {
                    case -1:
                {
                    [cell.tickButton setSelected:NO];
                }
                    break;
                case 0:
                {
                    if (indexPath.row == 0) {
                        //ok me
                        [cell.tickButton setSelected:TRUE];
                    }else{
                        [cell.tickButton setSelected:NO];
                    }

                }
                    break;
                case 1:
                {
                    if (indexPath.row == 1) {
                        //ok
                        [cell.tickButton setSelected:TRUE];
                    }else{
                        [cell.tickButton setSelected:NO];
                    }
                }
                    break;
                case 3:
                {
                    if (indexPath.row == 2) {
                        //ok
                        [cell.tickButton setSelected:TRUE];
                    }else{
                        [cell.tickButton setSelected:NO];
                    }
                }
                    break;
                
                default:
                    break;
            }
        
        [cell.tickButton addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (indexPath.section == 1)
    {
            NSDictionary*dic = [sharingGroupsArray objectAtIndex:indexPath.row];
            //Mes group
     
        /*
         categoryImage = sharechamp;
         categoryName = as;
         groupID = 296;
         isSelected = 0;
         */
            [cell setListLabel:dic[@"categoryName"]];
            [cell setListImages:@"sharechamp"];
            [cell.tickButton setTag: indexPath.row + 100];
            NSNumber *num = dic[@"isSelected"];
            
            if ( [num boolValue]) {
                [cell.tickButton setSelected:TRUE];
            }else{
                [cell.tickButton setSelected:FALSE];
            }
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesGroup:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (indexPath.section == 2)
    {
        NSDictionary*dic = [sharingHuntsArray objectAtIndex:indexPath.row];
        //Mes group
        [cell setListLabel:dic[@"categoryName"]];
        [cell setListImages:@"sharechamp"];
        [cell.tickButton setTag: indexPath.row + 100];
        NSNumber *num = dic[@"isSelected"];
        
        if ( [num boolValue]) {
            [cell.tickButton setSelected:TRUE];
        }else{
            [cell.tickButton setSelected:FALSE];
        }
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesHunt:) forControlEvents:UIControlEventTouchUpInside];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


-(void)selectTickAction:(id)sender{

    if (![COMMON isReachable]) {
        return;
    }

    int iTag = (int)[sender tag] - 10;
    UIButton *btn =(UIButton*)sender;
    if (btn.selected) {
        iSharing=-1;
    }
    else
    {
        switch (iTag) {
            case 0:
                iSharing = 0;
                break;
            case 1:
                iSharing = 1;
                break;
            case 2:
                iSharing = 3;
                break;
                
            default:
                break;
        }
    }
    
    
    //save bFilter only
    NSDictionary *dicA = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    
    NSMutableDictionary *mutDic = [dicA mutableCopy];
    [mutDic setValue:[NSNumber numberWithInt:iSharing] forKey:@"iFilter"];
    
    [[NSUserDefaults standardUserDefaults]setObject:mutDic forKey:[NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    [self.tableControl reloadData];
}

-(IBAction)selectTickActionMesGroup:(UIButton*)sender
{
    if (![COMMON isReachable]) {
        return;
    }

    NSDictionary *dic=[[sharingGroupsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    
    if ([sender isSelected])
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingGroupsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}
-(IBAction)selectTickActionMesHunt:(UIButton*)sender
{
    if (![COMMON isReachable]) {
        return;
    }
    
    NSDictionary *dic=[[sharingHuntsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    
    if ([sender isSelected])
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingHuntsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}

-(void) updateUI
{
    //global
    [swFilterControl setOnTintColor:UIColorFromRGB(ON_SWITCH)];
    swFilterControl.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [swFilterControl setOn:bFilterON];
    [self switchValueChanged:swFilterControl];
}

- (IBAction)switchValueChanged:(id)sender
{
    UISwitch *sw =(UISwitch*)sender;
    
    if ([COMMON isReachable]) {
        bFilterON = sw.isOn;
        
        if (bFilterON == NO) {
            [[NSUserDefaults standardUserDefaults] setBool:bFilterON forKey:@"isCategoryON"];
        }

        //save bFilter only
        NSDictionary *dicA = [[NSUserDefaults standardUserDefaults]objectForKey:[NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
        
        NSMutableDictionary *mutDic = [dicA mutableCopy];
        [mutDic setValue:[NSNumber numberWithBool:bFilterON] forKey:@"isFilter"];
       
        [[NSUserDefaults standardUserDefaults]setObject:mutDic forKey:[NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
        [[NSUserDefaults standardUserDefaults]synchronize];

        if (bFilterON) {
            self.tableControl.hidden = NO;
        }else{
            self.tableControl.hidden = YES;
        }
        
        //sub nav menu
        [self updateStatusNavControls];

    }
}

@end
