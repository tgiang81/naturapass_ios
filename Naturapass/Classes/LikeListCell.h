//
//  LikeListCell.h
//  Naturapass
//
//  Created by JoJo on 11/28/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LikeListCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UIImageView *imgAvatar;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDesc;
@property (nonatomic,strong) IBOutlet UIButton *btnAddFriend;
@property (nonatomic,strong) IBOutlet UIImageView *imgAddFriend;

@end

NS_ASSUME_NONNULL_END
