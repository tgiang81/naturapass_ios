//
//  ChassesInvitationAttend.h
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ChassesBaseVC.h"

@interface ChassesInvitationAttend : ChassesBaseVC

@property(nonatomic,strong) IBOutlet UILabel *lbInvitation;
@end
