
//
//  Signaler_Favoris_System.m
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Signaler_Favoris_System.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "TreeViewNode.h"
#import "SignalerFavorisSystemCell.h"
#import "PublicationOBJ.h"
#import "OHAttributedLabel.h"
#import "FavorisSystemBaseCell.h"
#import "SignalerFavorisSystemColorCell.h"
#import "Publication_Ajout_Favoris.h"
#import "DatabaseManager.h"

static NSString *favorisCell = @"FavorisCell";
static NSString *favorisColorCell = @"FavorisColorCell";

@interface Signaler_Favoris_System ()
{
    NSMutableArray *nodes;
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnSuivant;

}

@property (nonatomic, strong) SignalerFavorisSystemCell *prototypeCell;

@end

@implementation Signaler_Favoris_System

- (SignalerFavorisSystemCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:favorisCell];
    }
    return _prototypeCell;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myDesc setText:@"Céer un favori..."];
    self.vContainer.backgroundColor = self.colorAttachBackGround;
    [self InitializeKeyboardToolBar];
    [self.tfInput setInputAccessoryView:self.keyboardToolbar];
    [self.viewTF.layer setMasksToBounds:YES];
    self.viewTF.layer.cornerRadius= 5.0;
    [self.btnTERMINER.layer setMasksToBounds:YES];
    self.btnTERMINER.layer.cornerRadius= 25.0;
    self.btnTERMINER.backgroundColor = self.colorNavigation;
    self.viewHeader.backgroundColor = self.colorCellBackGround;

    lbTitle.text = str(strSelectionnez_les_informations);
    [self.btnTERMINER setTitle:[str(strValider) uppercaseString] forState:UIControlStateNormal];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerFavorisSystemCell" bundle:nil] forCellReuseIdentifier:favorisCell];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerFavorisSystemColorCell" bundle:nil] forCellReuseIdentifier:favorisColorCell];
    nodes =[NSMutableArray new];
    NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
    //legend
    if (dicFavo[@"legend"]) {
        TreeViewNode *treeLegend = [[TreeViewNode alloc]init];
        treeLegend.nodeLevel = 0;
        treeLegend.nodeObject = dicFavo[@"legend"];
        treeLegend.nodeTitle = str(strLegende);
        treeLegend.isExpanded = NO;
        treeLegend.typeCell = FAV_LEGENDE;
        treeLegend.isEditItem =YES;
        treeLegend.statusNode =IS_CHECK;
        [nodes addObject:treeLegend];
    }
    if (dicFavo[@"color"]) {
        //publication color
        TreeViewNode *treeColor = [[TreeViewNode alloc]init];
        treeColor.nodeLevel = 0;
        treeColor.nodeObject = dicFavo[@"color"][@"color"];
        treeColor.nodeTitle = str(strCouleur);
        treeColor.isExpanded = NO;
        treeColor.typeCell = FAV_COULEUR;
        treeColor.isEditItem =YES;
        treeColor.statusNode =IS_CHECK;
        [nodes addObject:treeColor];
    }
    if (dicFavo[@"category_tree"]) {
        //publication precistions
        TreeViewNode *treePrecistions = [[TreeViewNode alloc]init];
        treePrecistions.nodeLevel = 0;
        treePrecistions.nodeObject = dicFavo[@"category_tree"];
        treePrecistions.nodeTitle = str(strPrecisions);
        treePrecistions.isExpanded = NO;
        treePrecistions.typeCell = FAV_PRECISIONS;
        treePrecistions.isEditItem =NO;
        treePrecistions.statusNode =IS_CHECK;
        [nodes addObject:treePrecistions];
    }
    
    if (dicFavo[@"attachments"]) {
        //observation
        TreeViewNode *treeOb = [[TreeViewNode alloc]init];
        treeOb.nodeLevel = 0;
        treeOb.nodeObject = nil;
        treeOb.nodeTitle = str(strFiche);
        treeOb.isExpanded = YES;
        treeOb.typeCell = FAV_FICHE;
        treeOb.nodeChildren =[NSMutableArray new];
        treeOb.isEditItem =NO;
        treeOb.statusNode =IS_CHECK;
        
        //observation child
        NSArray *arrAttactment =dicFavo[@"attachments"];
        for (int i =0 ; i<arrAttactment.count; i++) {
            NSDictionary *dicAtt = arrAttactment[i];
            TreeViewNode *treeAtt = [[TreeViewNode alloc]init];
            treeAtt.nodeLevel = 1;
            treeAtt.nodeObject = dicAtt[@"text"];
            treeAtt.nodeTitle = dicAtt[@"name"];
            treeAtt.nodeItemAttachment = dicAtt;
            treeAtt.isExpanded = NO;
            treeAtt.typeCell = FAV_FICHE_CHILD;
            treeAtt.isEditItem =YES;
            treeAtt.parent = treeOb;
            treeAtt.statusNode =IS_CHECK;
            [treeOb.nodeChildren addObject:treeAtt];
            
        }
        [nodes addObject:treeOb];
    }
    NSString *strpartage =@"";
    switch ([dicFavo[@"iSharing"] intValue]) {
        case 0:
        {
            strpartage =str(strMoi);
        }
            break;
        case 1:
        {
            strpartage =str(strAAmis);
        }
            break;
        case 3:
        {
            strpartage =str(strTous_les_natiz);
        }
            break;
        default:
            break;
    }

//    NSArray *arrGroup =dicFavo[@"sharingGroupsArray"];//"1,2,3"
//    if (arrGroup.count>0) {
//        NSMutableArray *arrGroupName =  [NSMutableArray new];
//        NSString*strGroupName = nil;
//        for (NSDictionary*dicGroup in arrGroup) {
//            [arrGroupName addObject:dicGroup[@"categoryName"]];
//        }
//        strGroupName = [arrGroupName componentsJoinedByString:@", "];
//        
//        strpartage = [NSString stringWithFormat:@"%@\nGroup: %@",strpartage,strGroupName];
//    }
//    
//    NSArray *arrHunt =dicFavo[@"sharingHuntsArray"];
//    if (arrHunt.count>0) {
//        NSMutableArray *arrChassesName =  [NSMutableArray new];
//        NSString*strChassesName = nil;
//        for (NSDictionary*dicHunt in arrHunt) {
//            [arrChassesName addObject:dicHunt[@"categoryName"]];
//        }
//        strChassesName = [arrChassesName componentsJoinedByString:@", "];
//        
//        strpartage = [NSString stringWithFormat:@"%@\nChasse: %@",strpartage,strChassesName];
//    }
    if (strpartage.length>0) {
        //publication precistions
        TreeViewNode *treePartage = [[TreeViewNode alloc]init];
        treePartage.nodeLevel = 0;
        treePartage.nodeObject = strpartage;
        treePartage.nodeTitle = str(strPartage);
        treePartage.isExpanded = NO;
        treePartage.typeCell = FAV_PARTAGE;
        treePartage.isEditItem =YES;
        treePartage.statusNode =IS_CHECK;
        [nodes addObject:treePartage];
    }
    // group
    NSArray *arrGroup =dicFavo[@"sharingGroupsArray"];//"1,2,3"
    if (arrGroup.count>0) {
        //groups
        TreeViewNode *treeGroups = [[TreeViewNode alloc]init];
        treeGroups.nodeLevel = 0;
        treeGroups.nodeObject = nil;
        treeGroups.nodeTitle = str(strGroupes);
        treeGroups.isExpanded = YES;
        treeGroups.typeCell = FAV_GROUPS;
        treeGroups.nodeChildren =[NSMutableArray new];
        treeGroups.isEditItem =NO;
        treeGroups.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrGroup.count; i++) {
            NSDictionary *dicGroup = arrGroup[i];
            TreeViewNode *treeGroupChild = [[TreeViewNode alloc]init];
            treeGroupChild.nodeLevel = 1;
            treeGroupChild.nodeObject = dicGroup[@"categoryName"];
            treeGroupChild.nodeItemAttachment = dicGroup;
            treeGroupChild.isExpanded = NO;
            treeGroupChild.typeCell = FAV_GROUPS_CHILD;
            treeGroupChild.isEditItem =YES;
            treeGroupChild.parent = treeGroups;
            treeGroupChild.statusNode =IS_CHECK;
            
            [treeGroups.nodeChildren addObject:treeGroupChild];
            
        }
        [nodes addObject:treeGroups];
    }
    // hunts
    NSArray *arrHunt =dicFavo[@"sharingHuntsArray"];
    if (arrHunt.count>0) {
        //groups
        TreeViewNode *treeHunts = [[TreeViewNode alloc]init];
        treeHunts.nodeLevel = 0;
        treeHunts.nodeObject = nil;
        treeHunts.nodeTitle = str(strAAgenda);
        treeHunts.isExpanded = YES;
        treeHunts.typeCell = FAV_HUNTS;
        treeHunts.nodeChildren =[NSMutableArray new];
        treeHunts.isEditItem =NO;
        treeHunts.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrHunt.count; i++) {
            NSDictionary *dicHunt = arrHunt[i];
            TreeViewNode *treeHuntChild = [[TreeViewNode alloc]init];
            treeHuntChild.nodeLevel = 1;
            treeHuntChild.nodeObject = dicHunt[@"categoryName"];
            treeHuntChild.nodeItemAttachment = dicHunt;
            treeHuntChild.isExpanded = NO;
            treeHuntChild.typeCell = FAV_HUNTS_CHILD;
            treeHuntChild.isEditItem =YES;
            treeHuntChild.parent = treeHunts;
            treeHuntChild.statusNode =IS_CHECK;
            
            [treeHunts.nodeChildren addObject:treeHuntChild];
            
        }
        [nodes addObject:treeHunts];
    }
    // hunts
    NSArray *arrUsers =dicFavo[@"sharingUsers"];
    if (arrUsers.count>0) {
        //groups
        TreeViewNode *treeUser = [[TreeViewNode alloc]init];
        treeUser.nodeLevel = 0;
        treeUser.nodeObject = nil;
        treeUser.nodeTitle = str(@"Personnes");
        treeUser.isExpanded = YES;
        treeUser.typeCell = FAV_USERS;
        treeUser.nodeChildren =[NSMutableArray new];
        treeUser.isEditItem =NO;
        treeUser.statusNode =IS_CHECK;
        
        //groups child
        for (int i =0 ; i<arrUsers.count; i++) {
            NSDictionary *dicUsers = arrUsers[i];
            TreeViewNode *treeUsersChild = [[TreeViewNode alloc]init];
            treeUsersChild.nodeLevel = 1;
            treeUsersChild.nodeObject = dicUsers[@"name"];
            treeUsersChild.nodeItemAttachment = dicUsers;
            treeUsersChild.isExpanded = NO;
            treeUsersChild.typeCell = FAV_USERS_CHILD;
            treeUsersChild.isEditItem =YES;
            treeUsersChild.parent = treeUser;
            treeUsersChild.statusNode =IS_CHECK;
            
            [treeUser.nodeChildren addObject:treeUsersChild];
            
        }
        [nodes addObject:treeUser];
    }
    [self fillDisplayArray];
    [self.tableControl reloadData];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}
#pragma mark - Messages to fill the tree nodes and the display array

- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    parent.statusNode = statusNode;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        node.statusNode = statusNode;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    if (!parent) {
        return;
    }
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        if (treenode.statusNode == IS_CHECK) {
            countCheck++;
        }
        if (treenode.statusNode == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        parent.statusNode = IS_CHECK;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        parent.statusNode = UN_CHECK;
    }
    else
    {
        parent.statusNode = NON_CHECK;
        
    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }
    
}
//This function is used to fill the array that is actually displayed on the table view
- (void)fillDisplayArray
{
    self.displayArray = [[NSMutableArray alloc]init];
    for (TreeViewNode *node in nodes) {
        [self.displayArray addObject:node];
        //        [self caculatorWithContent:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
            
            int index  =(int)self.displayArray.count;
            if(index>0)
            {
                TreeViewNode *nodeLeave = self.displayArray[index-1];
                nodeLeave.lineBottom =YES;
                [self.displayArray replaceObjectAtIndex:index-1 withObject:nodeLeave];
            }
        }
    }
}
//This function is used to add the children of the expanded node to the display array
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [self.displayArray addObject:node];
        //        [self caculatorWithContent:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
        }
    }
}

#pragma mark - Table view data source

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    SignalerFavorisSystemCell *cell = (SignalerFavorisSystemCell*)cellTmp;
    
    if (node.isExpanded) {
        [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"favo_arrow_down"]];
    }
    else {
        [cell setTheButtonBackgroundImage:[UIImage imageNamed:@"favo_arrow_right"]];
    }
    switch (node.statusNode) {
        case NON_CHECK:
        {
            [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor whiteColor] withColorBoderActive:[UIColor lightGrayColor]];
        }
            break;
        case UN_CHECK:
        {
            [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor lightGrayColor] withColorBoderActive:[UIColor lightGrayColor]];
        }
            break;
        case IS_CHECK:
        {
            [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:self.colorNavigation withColorBoderActive:[UIColor lightGrayColor]];
        }
            break;
        default:
            break;
    }
    cell.btnCheckBox.selected = TRUE;
    [cell.btnCheckBox setTypeButton:NO];
/*
     switch (node.statusNode) {
        case NON_CHECK:
        {
            [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
            
        }
            break;
        case UN_CHECK:
        {
            [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"check_inactive"] forState:UIControlStateNormal];
            
        }
            break;
        case IS_CHECK:
        {
            switch (self.expectTarget) {
                case ISLIVE:
                {
                    [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"sign_check_active"] forState:UIControlStateNormal];
                }
                    break;
                    
                default:
                {
                    [cell.btnCheckBox setBackgroundImage:[UIImage imageNamed:@"icon_pub_active_green"] forState:UIControlStateNormal];
                }
                    break;
            }

        }
            break;
        default:
            break;
    }*/
    cell.treeNode = node;
    //
    
    NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
    UIColor *color = [UIColor whiteColor]; // select needed color
    if (node.nodeTitle) {
        NSString *title = [NSString stringWithFormat:@"%@: ",(NSString*)node.nodeTitle];
        
        NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
        
        NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
        [mLabel addAttribute:NSFontAttributeName
                       value:FONT_HELVETICANEUE_MEDIUM(15)
                       range:NSMakeRange(0, title.length)];
        //Add label
        [mAttach appendAttributedString:mLabel];
    }
    if (node.nodeObject) {
        NSString *strValue =@"";
        if (![node.nodeObject isKindOfClass:[NSArray class]]) {
            strValue =node.nodeObject;
            
        }
        else
        {
            strValue= [node.nodeObject componentsJoinedByString:@", "];
            
        }
        //value
        if(node.typeCell == FAV_FICHE)
        {
            color =  self.colorNavigation;
        }
        else
        {
            color = [UIColor whiteColor];
        }
        
        NSDictionary *attrsVal = @{ NSForegroundColorAttributeName : color };
        
        NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:strValue attributes:attrsVal];
        
        [mAttach appendAttributedString:mValue];
    }
    //
    if (node.nodeChildren.count>0) {
        cell.imgArrow.hidden = NO;
        cell.selectCellButton.hidden = NO;
        [cell.selectCellButton addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else
    {
        cell.imgArrow.hidden = YES;
        cell.selectCellButton.hidden = YES;
        [cell.selectCellButton removeTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.selectCellButton.tag = indexPath.row +100;
    
    cell.btnSelect.tag = indexPath.row +200;
    [cell.btnSelect addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    
    //
    if (node.typeCell == FAV_PRECISIONS) {
        cell.btnCheckBox.hidden=YES;
    }
    else
    {
        cell.btnCheckBox.hidden=NO;
    }
    [cell.cellLabel setAttributedText:mAttach];
    
    cell.lineBottom.hidden = NO;
    [cell setNeedsDisplay];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell layoutIfNeeded];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.displayArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.typeCell==FAV_COULEUR) {
        return UITableViewAutomaticDimension;
        
    }else{
        [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
        
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
        
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.typeCell==FAV_COULEUR) {
        SignalerFavorisSystemColorCell *cell = (SignalerFavorisSystemColorCell*)[tableView dequeueReusableCellWithIdentifier:favorisColorCell];
        NSString *strColor = node.nodeObject;
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:strColor];
        
        [scanner setScanLocation:0]; // bypass '#' character
        [scanner scanHexInt:&result];
        cell.imgColor.backgroundColor = UIColorFromRGB(result);
        switch (node.statusNode) {
            case NON_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor whiteColor] withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            case UN_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor lightGrayColor] withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            case IS_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:self.colorNavigation withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            default:
                break;
        }
        cell.btnCheckBox.selected = TRUE;
        [cell.btnCheckBox setTypeButton:NO];

        cell.treeNode = node;
        //
        
        NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
        UIColor *color = [UIColor whiteColor]; // select needed color
        if (node.nodeTitle) {
            NSString *title = [NSString stringWithFormat:@"%@: ",(NSString*)node.nodeTitle];

            NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
            
            NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
            [mLabel addAttribute:NSFontAttributeName
                           value:FONT_HELVETICANEUE_MEDIUM(15)
                           range:NSMakeRange(0, title.length)];
            //Add label
            [mAttach appendAttributedString:mLabel];
        }
        cell.btnSelect.tag = indexPath.row +200;
        if (node.isEditItem == YES) {
            cell.btnSelect.hidden = NO;
            [cell.btnSelect addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            cell.btnSelect.hidden = YES;
            [cell.btnSelect removeTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        [cell.cellLabel setAttributedText:mAttach];
        
        cell.imgArrow.hidden = YES;
        cell.selectCellButton.hidden = YES;
        [cell.selectCellButton removeTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lineBottom.hidden = NO;
        [cell setNeedsDisplay];
        cell.backgroundColor = self.colorCellBackGround;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //mld hide arrow
        cell.imgArrow.hidden = YES;
        return cell;
        
    }
    else
    {
        SignalerFavorisSystemCell *cell = [tableView dequeueReusableCellWithIdentifier:favorisCell forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        cell.backgroundColor = self.colorCellBackGround;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //mld hide arrow
        cell.imgArrow.hidden = YES;
        return cell;
    }
    
}
- (IBAction)expand:(id)sender
{
    int index = (int)([sender tag] -100);
    TreeViewNode *node = [self.displayArray objectAtIndex:index];
    node.isExpanded = !node.isExpanded;
    [self fillDisplayArray];
    [self.tableControl reloadData];
    
}
- (IBAction)selectAction:(id)sender
{
    int index = (int)([sender tag] -200);
    TreeViewNode *node = [self.displayArray objectAtIndex:index];
    node.isSelected = !node.isSelected;
    int statusNode = node.statusNode;
    switch (node.statusNode) {
        case NON_CHECK:
        {
            statusNode = IS_CHECK;
        }
            break;
        case UN_CHECK:
        {
            statusNode = IS_CHECK;
            
        }
            break;
        case IS_CHECK:
        {
            statusNode = NON_CHECK;
            
        }
            break;
        default:
            break;
    }
    [self checkFromParent:node withStatusNode:statusNode];
    node.statusNode = statusNode;
    [self checkFromChild:node];
    
    [self fillDisplayArray];
    [self.tableControl reloadData];
}
-(IBAction)onNext:(id)sender
{
    NSMutableDictionary *dicFavoTmp = [[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    for (int i=0; i<self.displayArray.count;i++) {
        TreeViewNode *node = [self.displayArray objectAtIndex:i];
        if (node.statusNode== NON_CHECK) {
            switch (node.typeCell) {
                case FAV_LEGENDE:
                {
                    [dicFavoTmp removeObjectForKey:@"legend"];
                }
                    break;
                case FAV_COULEUR:
                {
                    [dicFavoTmp removeObjectForKey:@"color"];
                }
                    break;
                case FAV_FICHE:
                {
                    [dicFavoTmp removeObjectForKey:@"attachments"];
                    if (dicFavoTmp[@"observation"]) {
                        NSMutableDictionary *dicAttach = [dicFavoTmp[@"observation"] mutableCopy];
                        [dicAttach removeObjectForKey:@"attachments"];
                        [dicFavoTmp setObject:dicAttach forKey:@"observation"];
                    }
                }
                    break;
                case FAV_FICHE_CHILD:
                {
                    if ([dicFavoTmp[@"attachments"] isKindOfClass:[NSArray class]]) {
                        NSMutableArray *arrAtt = [dicFavoTmp[@"attachments"] mutableCopy];
                        NSMutableDictionary *dicObs = [dicFavoTmp[@"observation"] mutableCopy];
                        
                        NSMutableArray *arrAtt1 = [dicObs[@"attachments"] mutableCopy];
                        
                        for (int i=0; i<arrAtt.count; i++) {
                            if ([arrAtt[i][@"label"] intValue] == [node.nodeItemAttachment[@"label"] intValue]) {
                                [arrAtt removeObjectAtIndex:i];
                                [arrAtt1 removeObjectAtIndex:i];
                                [dicFavoTmp setObject:arrAtt forKey:@"attachments"];
                                
                                [dicObs setObject:arrAtt forKey:@"attachments"];
                                [dicFavoTmp setObject:dicObs forKey:@"observation"];
                                break;
                            }
                        }
                    }
                }
                    break;
                case FAV_PARTAGE:
                {
                    [dicFavoTmp removeObjectForKey:@"sharing"];
                    [dicFavoTmp removeObjectForKey:@"iSharing"];
//                    [dicFavoTmp removeObjectForKey:@"receivers"];
                }
                    break;
                case FAV_GROUPS:
                {
                    [dicFavoTmp removeObjectForKey:@"sharingGroupsArray"];

                }
                    break;
                case FAV_GROUPS_CHILD:
                {
                    /*
                    <__NSArrayM 0x7fb00982d1e0>(
                    {
                        categoryName = "test 06/02/2015";
                        groupID = 9;
                        isSelected = 1;
                    },
                    {
                        categoryName = Fd2;
                        groupID = 409;
                        isSelected = 1;
                    },
                    {
                        categoryName = "New He";
                        groupID = 412;
                        isSelected = 1;
                    },
                    {
                        categoryName = 3;
                        groupID = 413;
                        isSelected = 1;
                    }
                                                )
                    Printing description of arrHunt:
                    (NSArray *) arrHunt = 0x000000010df86000
                    Printing description of arrHunt:
                    <__NSArrayM 0x7fb00982d210>(
                    {
                        categoryName = "my chasse";
                        huntID = 868;
                        isSelected = 1;
                    },
                    {
                        categoryName = "my chass 2";
                        huntID = 869;
                        isSelected = 1;
                    }
                                                )
                    */
                    NSMutableArray *arrGroup =[dicFavoTmp[@"sharingGroupsArray"] mutableCopy];
                    for (int i =0 ; i<arrGroup.count; i++) {
                        NSDictionary *dicGroup = arrGroup[i];
                        if ([dicGroup[@"groupID"] intValue]== [node.nodeItemAttachment[@"groupID"] intValue]) {
                            [arrGroup removeObjectAtIndex:i];
                            [dicFavoTmp setObject:arrGroup forKey:@"sharingGroupsArray"];
                        }
                    }
                }
                    break;
                case FAV_USERS:
                {
                    [dicFavoTmp removeObjectForKey:@"sharingUsers"];

                }
                    break;
                case FAV_USERS_CHILD:
                {
                    /*
                     sharingUsers =     (
                     {
                     id = 2932;
                     name = "Evance Riviere";
                     status = 1;
                     type = 6;
                     },
                     */
                    NSMutableArray *arrHunt =[dicFavoTmp[@"sharingUsers"] mutableCopy];
                    for (int i =0 ; i<arrHunt.count; i++) {
                        NSDictionary *dicHunt = arrHunt[i];
                        if ([dicHunt[@"id"] intValue]== [node.nodeItemAttachment[@"id"] intValue]) {
                            [arrHunt removeObjectAtIndex:i];
                            [dicFavoTmp setObject:arrHunt forKey:@"sharingUsers"];
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    if ([self.tfInput.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return ;
    }
    [self addFavoriteWithName:self.tfInput.text withDic:dicFavoTmp];
//    Publication_Ajout_Favoris *viewController1 = [[Publication_Ajout_Favoris alloc] initWithNibName:@"Publication_Ajout_Favoris" bundle:nil];
//    viewController1.dicFavo = dicFavoTmp;
//    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}

-(void)addFavoriteWithName:(NSString*)strName withDic:(NSDictionary*)dicFavo
{
    ASLog(@"1");
    /*
     77:      * POST /v2/publications/favorites
     78:      *
     79:      * {
     80:      *     "favorite": {
     81:      *       "name": "premier fav",
     82:      *       "legend": "ma legend",
     83:      *       "sharing":{"share":1,"withouts":[]},
     84:      *       "groups": [206],
     85:      *       "hunts": [],
     86:      *       "publicationcolor": "4",
     87:      *       "category": 137,
     88:      *       "card": 14,
     89:      *       "specific": 0,
     90:      *       "animal": null,
     91:      *       "attachments":[{"label":32,"value":1},{"label":34,"value":1}]
     92:      *     }
     93:      * }
     */
    /*
     {
     attachments =     (
     {
     label = 58;
     name = "Ind\U00e9termin\U00e9";
     value = 0;
     },
     {
     label = 59;
     name = "M\U00e2le";
     value = 0;
     },
     {
     label = 60;
     name = Femelle;
     value = 0;
     },
     {
     label = 61;
     name = Jeune;
     value = 0;
     }
     );
     color =     {
     color = 000000;
     id = 8;
     name = noir;
     };
     iSharing = 0;
     legend = b;
     observation =     {
     attachments =         (
     {
     label = 58;
     value = 0;
     },
     {
     label = 59;
     value = 0;
     },
     {
     label = 60;
     value = 0;
     },
     {
     label = 61;
     value = 0;
     }
     );
     category = 149;
     specific = 0;
     };
     precision = "Animaux/Tu\U00e9 chasse/Oiseaux/Faisan v\U00e9n\U00e9r\U00e9, Faisans de Chasse";
     sharingGroupsArray =     (
     {
     categoryName = Dodd;
     groupID = 404;
     isSelected = 1;
     }
     );
     sharingHuntsArray =     (
     );
     }
     */
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    NSArray *arrHunt =nil;
    NSArray *arrGroup =nil;
    
    NSMutableDictionary *dicPost = [NSMutableDictionary new];
    if (strName) {
        [dicPost setValue:strName forKey:@"name"];
    }
    
    if (dicFavo[@"card_id"]) {
        [dicPost setValue:dicFavo[@"card_id"] forKey:@"card"];
    }
    
    if (dicFavo[@"legend"]) {
        [dicPost setValue:dicFavo[@"legend"] forKey:@"legend"];
    }
    
    if (dicFavo[@"iSharing"]) {
        [dicPost setValue:@{@"share":dicFavo[@"iSharing"]} forKey:@"sharing"];
    }
    
    if (dicFavo[@"sharingHuntsArray"]) {
        arrHunt =[self convertArray2ArrayID:dicFavo[@"sharingHuntsArray"] forGroup:NO];
        [dicPost setValue:arrHunt forKey:@"hunts"];
    }
    
    if (dicFavo[@"sharingGroupsArray"]) {
        arrGroup= [self convertArray2ArrayID:dicFavo[@"sharingGroupsArray"] forGroup:YES];
        [dicPost setValue:arrGroup forKey:@"groups"];
    }
    if (dicFavo[@"sharingUsers"]) {
        arrGroup= [self convertPersonneArray2ArrayID:dicFavo[@"sharingUsers"]];
        [dicPost setValue:arrGroup forKey:@"users"];
    }
    if (dicFavo[@"color"]) {
        [dicPost setValue:dicFavo[@"color"][@"id"] forKey:@"publicationcolor"];
    }
    
    //Default
    [dicPost setValue:@"0" forKey:@"specific"];
    
    if (dicFavo[@"observation"]) {
        if (dicFavo[@"observation"][@"attachments"]) {
            NSMutableDictionary *dicObservation = [dicFavo[@"observation"]  mutableCopy];
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{@"label": dic[@"label"],
                                       @"value": dic[@"value"]}];
            }
            [dicPost setValue:newAttach forKey:@"attachments"];
        }
        
        if (dicFavo[@"observation"][@"category"]) {
            [dicPost setValue:dicFavo[@"observation"][@"category"] forKey:@"category"];
        }
        if (dicFavo[@"observation"][@"card"]) {
            [dicPost setValue:dicFavo[@"observation"][@"card"] forKey:@"card"];
        }
        if (dicFavo[@"observation"][@"animal"]) {
            [dicPost setValue:dicFavo[@"observation"][@"animal"] forKey:@"animal"];
        }
        if (dicFavo[@"observation"][@"specific"]) {
            [dicPost setValue:dicFavo[@"observation"][@"specific"] forKey:@"specific"];
        }
    }
    
    [COMMON addLoading:self];
    
    [serviceObj fnPOST_PUBLICATION_FAVORITES:@{@"favorite":dicPost}];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        
        //reset
        [COMMON removeProgressLoading];
        
        if ([response[@"favorite"] isKindOfClass:[NSDictionary class]]) {
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];
                    
                }
            }];
            [PublicationOBJ sharedInstance].isPostFavo = YES;
//            [self gotoback];
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass: [self.mParent class]]) {
                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            //Back to Dashboard
            [self.navigationController popToRootViewControllerAnimated:YES];

        }else if (response[@"message"]){
            //message timeout...
            
            [KSToastView ks_showToast:response[@"message"]  duration:3.0f completion: ^{
            }];
        }
        
        
    };
    
}

- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}
-(NSArray*) convertArray2ArrayID:(NSArray*)arr forGroup:(BOOL) isGroup
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            if (isGroup) {
                [array addObject:dic[@"groupID"]];
                
            }else{
                [array addObject:dic[@"huntID"]];
            }
        }
    }
    return array;
}
-(NSArray*) convertPersonneArray2ArrayID:(NSArray*)arr
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"status"];
        if ( [num boolValue]) {
            [array addObject:dic[@"id"]];
        }
    }
    return array;
}
//MARK:- searBar
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];    
}
@end
