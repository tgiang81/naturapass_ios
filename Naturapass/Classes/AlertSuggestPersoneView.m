//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertSuggestPersoneView.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "AlertSuggestCell.h"
#import "CommonHelper.h"
#import "FileHelper.h"
static NSString *identifierSection0 = @"MyTableViewCell0";

@implementation AlertSuggestPersoneView
{
    WebServiceAPI *serviceAPI;
    NSMutableArray                  *searchedArray;
    NSMutableArray                  *searchedSelected;
    NSMutableArray *arrFriendList;
    NSMutableArray *searchedResponse;

}
-(instancetype)initSuggestView
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSuggestPersoneView" owner:self options:nil] objectAtIndex:0] ;

    if (self) {
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertSuggestCell" bundle:nil] forCellReuseIdentifier:identifierSection0];
        self.tableControl.estimatedRowHeight = 60;
        [self.subView.layer setMasksToBounds:YES];
        self.subView.layer.cornerRadius= 5;
        self.subView.layer.borderWidth = 0.5;
        self.subView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];

}
- (void)reloadFriendList {
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],MY_FRIEND_SAVE) ];
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        [arrFriendList removeAllObjects];
//        [arrFriendList  addObjectsFromArray:arrTmp];
        [self addFriendList:arrTmp];
        searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
        [self.tableControl reloadData];
    }
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getUserFriendsAction];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [arrFriendList removeAllObjects];
        if (response[@"friends"]) {
            NSArray *arrTmp = response[@"friends"] ;
            
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                         ascending:YES];
            //
            NSArray *results = [arrTmp sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
            //save
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],MY_FRIEND_SAVE)   ];
//            [arrFriendList addObjectsFromArray:results];
            [self addFriendList:results];
            searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
            [arrFriendList writeToFile:strPath atomically:YES];
            [self.tableControl reloadData];
        }
    };
    
}
-(void)addFriendList:(NSArray*)arrFriend
{
    NSMutableArray *arrResult = [NSMutableArray new];
    for (NSDictionary *dicFriend in arrFriend) {
        NSMutableDictionary *dicTmp = [dicFriend mutableCopy];
        [dicTmp setValue:@(TRUE) forKey:@"friend"];
        [arrResult addObject:dicTmp];
    }
    arrFriendList = [arrResult mutableCopy];
}
-(NSArray*)resultArray:(NSArray*)arrResponse withArrFriend:(NSArray*)arrFriend withArrSelected:(NSArray*)arrSelected
{
    NSMutableArray *arrMix = [NSMutableArray new];
    NSMutableArray *arrResult = [NSMutableArray new];
    NSMutableArray *arrUser = [NSMutableArray new];
    //Default hiển thị Friend List
    if(self.strSearch.length > 0)
    {
        // Lọc danh sach trong list friend
        // Kiểm tra từng phần tử trong response chưa co trong friend list thì add vào arrMix
        NSMutableArray *arrTmp = [NSMutableArray new];
        NSString *predicateString = [NSString stringWithFormat:@"fullname contains[c] '%@'", self.strSearch];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
        arrTmp = [[arrFriend filteredArrayUsingPredicate:predicate] mutableCopy];
        [arrMix addObjectsFromArray:arrTmp];
        for (NSDictionary *dicUser in arrResponse) {
            BOOL isSelected = FALSE;
            for (NSDictionary *dicSelected in arrTmp) {
                if([dicUser[@"id"] intValue] == [dicSelected[@"id"] intValue])
                {
                    isSelected = TRUE;
                }
                
            }
            if(isSelected == FALSE)
            {
                [arrMix addObject:dicUser];
            }
        }

    }
    else
    {
        [arrMix addObjectsFromArray:arrFriend];
    }
//    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
//                                                                 ascending:YES];
//    //
//    arrUser = [[arrMix sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]] mutableCopy];
    arrUser = [arrMix mutableCopy];
    //Kiểm tra danh sách mới đã được check hay chưa
    for (NSDictionary *dicUser in arrUser) {
        BOOL isSelected = FALSE;
        for (NSDictionary *dicSelected in arrSelected) {
            if([dicUser[@"id"] intValue] == [dicSelected[@"id"] intValue])
            {
                NSMutableDictionary *dicTmp = [dicUser mutableCopy];
                [dicTmp setValue:@(TRUE) forKey:@"selected"];
                [arrResult addObject:dicTmp];
                isSelected = TRUE;
            }
            
        }
        if(isSelected == FALSE)
        {
            [arrResult addObject:dicUser];
        }
    }
    return arrResult;
}
-(void)fnDataInput:(NSString*)strSearch withArrSelected:(NSArray*)arrSelected
{
    searchedArray = [NSMutableArray new];
    searchedSelected = [NSMutableArray new];
    arrFriendList = [NSMutableArray new];
    searchedResponse = [NSMutableArray new];
    [self.tableControl reloadData];
    self.strSearch = strSearch;
    [searchedSelected addObjectsFromArray:arrSelected];
    [self reloadFriendList];

}
#pragma callback
-(void)setCallback:(AlertSuggestPersoneViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSuggestPersoneViewCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect
{
    UIView *view = self;
    [viewSuper addSubview:view];
    view.frame = rect;
    
    serviceAPI = [WebServiceAPI new];
    
    __weak AlertSuggestPersoneView *weakVC = self;
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        [weakVC performSelectorOnMainThread:@selector(reloadNewData:) withObject:response waitUntilDone:YES];
        
    };
    [self processLoungesSearchingURL:self.strSearch];
}
    -(void)reloadNewData:(id) response
    {
        [searchedResponse removeAllObjects];
        NSArray *arrUsers = response[@"users"];
        [searchedResponse addObjectsFromArray:arrUsers];
        searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
        [self.tableControl reloadData];

    }
-(void)updateArraySelected:(NSArray*)arrSelected
{
    [searchedSelected removeAllObjects];
    [searchedSelected addObjectsFromArray:arrSelected];
    searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
    [self.tableControl reloadData];

}
    - (void) GetUsersSearchAction:(NSString*)searchString
    {
        if (searchString.length < VAR_MINIMUM_LETTRES) {
        }
        else
        {
            [COMMON removeProgressLoading];
            [COMMON addLoadingForView:self];
            [serviceAPI getUsersSearchAction:searchString];
        }
        
        
    }
-(IBAction)closeAction:(id)sender
{
    [_btnDissmiss removeFromSuperview];
    [self removeFromSuperview];
    if(_callback)
    {
        NSMutableArray *arrTmp = [NSMutableArray new];
        for (NSDictionary *dic in searchedSelected) {
            if([dic[@"selected"] boolValue])
            {
                [arrTmp addObject:dic];
            }
        }
        _callback(arrTmp);
    }
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
     NSString *strsearchValues= [searchText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.strSearch = searchText;
    
    [self handleSearchForSearchString:strsearchValues];
}
//MARK: UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""] || !searchString) {
        [self clearSearchResults];
        
    } else {
        [self GetUsersSearchAction:searchString];
    }
}
#pragma mark Search Helpers

- (void)clearSearchResults
{
    [searchedResponse removeAllObjects];
    searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
    [self.tableControl reloadData];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}
#pragma mark - Helpers

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchedArray.count;
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlertSuggestCell *cell = (AlertSuggestCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
    NSDictionary *dic = searchedArray[indexPath.row];
    cell.lbTitle.text = dic[@"fullname"];
    if([dic[@"selected"] boolValue])
    {
        cell.imgCheck.hidden = FALSE;
    }
    else
    {
        cell.imgCheck.hidden = YES;
    }
    cell.imgFriend.image = [[UIImage imageNamed:@"amis_ic_friend_active"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    cell.imgFriend.tintColor = UIColorFromRGB(0xD28F4E);
    if([dic[@"friend"] boolValue])
    {
        cell.imgFriend.hidden = FALSE;
        cell.contraintWidthFriend.constant = 15;
    }
    else
    {
        cell.imgFriend.hidden = YES;
        cell.contraintWidthFriend.constant = 0;
    }
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary *dic = [searchedArray[indexPath.row] mutableCopy];
//    BOOL isSelected = ![dic[@"selected"] boolValue];
//    [dic setObject:@(isSelected) forKey:@"selected"];
//    [searchedArray replaceObjectAtIndex:indexPath.row withObject:dic];
////     [self.tableControl reloadData];
//    [self addItemSelected:dic];
    
    [_btnDissmiss removeFromSuperview];
    [self removeFromSuperview];
    if(_callback)
    {
        NSMutableArray *arrTmp = [NSMutableArray new];
        [arrTmp addObject:dic];
        _callback(arrTmp);
    }

    
}
-(void)addItemSelected:(NSDictionary*)dic
{
    if(![dic[@"selected"] boolValue])
    {
    for (int i = 0; i < searchedSelected.count; i++) {
        NSDictionary *dicTmp = searchedSelected[i];
        if ([dicTmp[@"id"] intValue] == [dic[@"id"] intValue]) {

                [searchedSelected removeObjectAtIndex:i];
                break;
        }
    }
    }
    else
    {
        BOOL isExit = FALSE;
        for (int i = 0; i < searchedSelected.count; i++) {
            NSDictionary *dicTmp = searchedSelected[i];
            if ([dicTmp[@"id"] intValue] == [dic[@"id"] intValue]) {
                isExit = TRUE;
                break;
            }
        }
        if (!isExit) {
            [searchedSelected addObject:dic];
        }
        
    }

}
-(void)removeItemSelected:(NSDictionary*)dicRemove
{
    for (int i = 0; i < searchedSelected.count; i++) {
        NSDictionary *dicTmp = searchedSelected[i];
        if ([dicTmp[@"id"] intValue] == [dicRemove[@"id"] intValue]) {
            [searchedSelected removeObjectAtIndex:i];
            break;
        }
    }
    searchedArray = [[self resultArray:searchedResponse withArrFriend:arrFriendList withArrSelected:searchedSelected] mutableCopy];
    [self.tableControl reloadData];
}
@end
