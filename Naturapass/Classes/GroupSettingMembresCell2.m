//
//  ConfigurationMessageCustomCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 2/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "GroupSettingMembresCell2.h"

#import "CommonHelper.h"

@implementation GroupSettingMembresCell2
- (void)awakeFromNib {
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];

    
    [[CommonHelper sharedInstance] setRoundedView:self.imgViewAvatar toDiameter:self.imgViewAvatar.frame.size.height /2];

}
@end
