//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertMapVC.h"
#import "Define.h"
#import "AppCommon.h"
@implementation AlertMapVC
{
    UITapGestureRecognizer *tapGesture;
}
-(instancetype)initWithTitle:(NSString*)stitle message:(NSString*)smessage
{
    self =[super initWithNibName:@"AlertMapVC" bundle:nil];
    if (self) {
        strTitle =stitle;
        strDescriptions =smessage;
    }
    return self;
}
#pragma mark - viewdid
-(void)viewDidLoad
{
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];

    if (strTitle) {
        _lbTitle.text =strTitle;
        
    }
    if (strDescriptions) {
        _lbDescriptions.text=strDescriptions;
    }
}

#pragma callback
-(void)setCallback:(AlertMapVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertMapVCCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showInVC:(UIViewController*)vc
{
    tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:tapGesture];
    [vc presentViewController:self animated:NO completion:^{
        
    }];
}

-(IBAction)PlansAction:(id)sender
{
    [self hiddenAlert];
    if (_callback) {
        _callback(0);
    }
}
-(IBAction)MapsAction:(id)sender
{
    [self hiddenAlert];
    if (_callback) {
        _callback(1);
    }
}
-(IBAction)WazeAction:(id)sender
{
    [self hiddenAlert];
    if (_callback) {
        _callback(2);
    }
}
#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    CGPoint point =[sender locationInView:sender.view];
    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    if (viewTouched!= subAlertView) {
        [self hiddenAlert];
    }
}
-(void)hiddenAlert
{
    [self dismissViewControllerAnimated:YES completion:^{}];
    [bgImage removeGestureRecognizer:tapGesture];
}
@end
