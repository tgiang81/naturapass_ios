//
//  GroupEnterMurVC.h
//  Naturapass
//
//  Created by Manh on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupEnterBaseVC.h"
#import "ShortcutScreenVC.h"
@interface GroupEnterMurVC :GroupEnterBaseVC
@property (nonatomic,strong) ShortcutScreenVC* viewShortCut;
@property (weak, nonatomic) IBOutlet UIButton *btnShortCut;

@end
