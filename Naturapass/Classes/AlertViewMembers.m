//
//  UploadProgress.m
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AlertViewMembers.h"
#import "CellKind12.h"
#import "AppCommon.h"
#import "MembersCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "Define.h"
@interface AlertViewMembers ()
{
    IBOutlet UITableView *tableControl;
    IBOutlet UIView  *viewHearder;
    UIColor *color;
    UIImage *imgAdd;
    NSString *sender_id;
}
@end

static NSString *identifier = @"CellKind12ID";

@implementation AlertViewMembers
- (id)initWithArrayMembers:(NSMutableArray*)arrMembers
{
    self = [super initWithNibName:@"AlertViewMembers" bundle:nil];
    if (self) {
        // Custom initialization
        _arrMembers=arrMembers;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view.layer setMasksToBounds:YES];
    self.view.layer.cornerRadius= 4.0;
    self.view.layer.borderWidth =0.5;
    self.view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    NSString *strImg =@"";
    switch (self.expectTarget) {
        case ISMUR://MUR
        {
            color = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            strImg=@"mur_ic_amis_add_friend";
        }
            break;
        case ISCARTE://CARTE
        {
            color = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            
        }
            break;
        case ISDISCUSS://DISCUSSION
        {
            color = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP://GROUP
        {
            color = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            strImg=@"ic_add_member_group";

        }
            break;
        case ISLOUNGE://CHASSES
        {
            color = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            strImg=@"chasse_ic_add_friend";

        }
            break;
        case ISAMIS://AMIS
        {
            color = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
            
        }
            break;
        case ISPARAMTRES://PARAMETERS
        {
            color = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
            
        }
            break;
        default:
            break;
    }
    viewHearder.backgroundColor = color;
    imgAdd =[UIImage imageNamed:strImg];
    [tableControl registerNib:[UINib nibWithNibName:@"MembersCell" bundle:nil] forCellReuseIdentifier:identifier];
    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrMembers count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MembersCell *cell = nil;
    
    cell = (MembersCell *)[tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = _arrMembers[indexPath.row];
    cell.label1.text = dic[@"user"][@"fullname"];
    //image
    NSString *imgUrl;
    //profilepicture
    if (dic[@"user"][@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:imgUrl]];
    [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    
    
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];
    
    //Arrow
    // neu la normal thi an nut add friend con neu la admin thic check da co moi quan he thi an con khong thi show ra cho nguoi dung click
            BOOL isAddFriendHide =YES;
            if ([dic[@"user"][@"id"] integerValue] ==[sender_id integerValue]) {
                isAddFriendHide= YES;
            }
            else
            {
                    if ([dic[@"isFriend"] isKindOfClass:[NSDictionary class]])
                    {
                        isAddFriendHide =YES;
                    }else{
                        isAddFriendHide=NO;
                    }
            }
            
            if (isAddFriendHide) {
                cell.btnAddFriend.hidden =YES;
                cell.imgAddFriend.hidden=YES;
                
                [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
                cell.btnAddFriend.hidden=NO;
                cell.imgAddFriend.hidden=NO;
                
                [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
                
            }
    cell.btnAddFriend.tag= indexPath.row;
    cell.imgAddFriend.image =imgAdd;
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic =_arrMembers[indexPath.row][@"user"];
    if (_CallBack) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        _CallBack(indexPath.row, dic);
    }
}
/*
 {
 friendship =     {
 state = 1;
 way = 2;
 };
 }
*/
#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    int index =(int)[sender tag];
    [COMMON addLoading:self];
    NSDictionary *dic = self.arrMembers[index];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"user"][@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            //co qh
            
            NSMutableDictionary *userDic =[NSMutableDictionary dictionaryWithDictionary:dic];
            
            [userDic removeObjectForKey:@"isFriend"];
            [userDic setObject:@{@"state":@1,@"way":@2} forKey:@"isFriend"];
            
            for (int i=0; i<self.arrMembers.count; i++) {
                NSDictionary *temp =self.arrMembers[i];
                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
                    [self.arrMembers replaceObjectAtIndex:i withObject:userDic];
                    break;
                }
            }
            //reload item
            NSArray *indexArray =[tableControl indexPathsForVisibleRows];
            [tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }
        
    };
}
-(IBAction)fnDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

@end
