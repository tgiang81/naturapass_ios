//
//  TheProjectCell.h
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TreeViewNode.h"
#import "AppCommon.h"
#import "MDRadioButton.h"
@interface FavorisSystemBaseCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UILabel *cellLabel;
@property (retain, nonatomic) IBOutlet UIButton *btnSelect;

@property (retain, nonatomic) IBOutlet UIButton *selectCellButton;

@property (retain, nonatomic) IBOutlet MDRadioButton *btnCheckBox;
@property (retain, nonatomic) IBOutlet UIImageView *lineBottom;
@property (retain, nonatomic) IBOutlet UIImageView *imgColor;
@property (retain, nonatomic) IBOutlet UIImageView *imgArrow;

@property (retain, strong) TreeViewNode *treeNode;

- (void)setTheButtonBackgroundImage:(UIImage *)backgroundImage;
@property (nonatomic, assign) BOOL isEditType;

@end
