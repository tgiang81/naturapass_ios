//
//  MurSettingVC.h
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesBaseVC.h"
typedef void (^Callback)(NSString *strID);
@interface ChassesParticipeVC : ChassesBaseVC
@property(nonatomic,strong) NSDictionary *dicChassesItem;
-(void)doCallback:(Callback)callback;
@property (nonatomic,copy) Callback callback;
@property(nonatomic,assign) BOOL isInvitionAttend;
@end
