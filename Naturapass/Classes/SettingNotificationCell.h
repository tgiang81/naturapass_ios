//
//  SettingNotificationCell.h
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *vBorder;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@property (weak, nonatomic) IBOutlet UILabel *lbFrequence;
@property (weak, nonatomic) IBOutlet UILabel *lbValueFrequence;
@property (weak, nonatomic) IBOutlet UIButton *btnChoose;

@end
