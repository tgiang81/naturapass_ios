//
//  ChassesCreateOBJ.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesMemberOBJ.h"

@implementation ChassesMemberOBJ

static ChassesMemberOBJ *sharedInstance = nil;

+ (ChassesMemberOBJ *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) resetParams
{
    self.dictionaryMyKind = nil;
    self.strMyKindId = nil;
    self.fromScreen =0;
    self.strParticipe =nil;
    self.isNonMember =NO;
    self.strFirtName =nil;
    self.strLastName =nil;
    self.strCommentPublic=nil;
    self.strComentPrive=nil;
    self.isUserInvite =NO;
}
@end
