//
//  Parameter_Profile.m
//  Naturapass
//
//  Created by Giang on 10/12/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Parameter_MonProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PhotoSheetSignUpVC.h"
#import "AZNotification.h"
#import "CommonHelper.h"
@interface Parameter_MonProfile ()<UITextFieldDelegate,UIActionSheetDelegate,WebServiceAPIDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSDictionary         *dicProfile;
    NSDictionary         *dicImageInfo;
    NSArray              *civiliteArray;
    NSNumber             *pickerValue;
    UIPickerView         *formPicker;
    UIActionSheet        *formActionSheet;
    
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIImageView *imgIconProfile;
    
    IBOutlet UITextField *tfCivilite;
    IBOutlet UITextField *tfLastName;
    IBOutlet UITextField *tfFirtName;
    IBOutlet UITextField *tfEmail;
    IBOutlet UITextField *tfDate;
    IBOutlet UIButton    *btnSuivant;
    IBOutlet UIScrollView       *myPersonalScrollView;
    IBOutlet UILabel *lbTitle;
}

@end

@implementation Parameter_MonProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMON_PROFIL);
    tfCivilite.placeholder = str(strCivilite);
    tfLastName.placeholder = str(strNom);
    tfFirtName.placeholder = str(strPrenom);
    tfEmail.placeholder = str(strEEmail);
    tfDate.placeholder = str(strDate_de_naissance);
    [btnSuivant setTitle:  str(strTERMINER) forState:UIControlStateNormal];

    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/YYYY"];
    
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.pickerView.backgroundColor =[UIColor whiteColor];
    [self.datePicker addTarget:self action:@selector(dateUpdated:) forControlEvents:UIControlEventValueChanged];
    
    tfDate.delegate = self;
    tfCivilite.delegate = self;
    
    tfDate.inputView = self.viewPickerBackground;
    tfCivilite.inputView = self.viewPickerBackground;
    
    [self.viewPickerBackground removeFromSuperview];
    self.pickerView.delegate=self;

    
    //    Monsieur Madame
    
    dicProfile =[NSDictionary new];
    dicImageInfo =[NSDictionary new];
    civiliteArray = [[NSArray alloc] initWithObjects:str(strCivilite0),str(strCivilite1),str(strCivilite2), nil];
    
    [self InitializeKeyboardToolBar];

    [tfCivilite setInputAccessoryView:self.keyboardToolbar];

    
    [tfLastName setInputAccessoryView:self.keyboardToolbar];
    [tfFirtName setInputAccessoryView:self.keyboardToolbar];
    [tfEmail setInputAccessoryView:self.keyboardToolbar];
    [imgProfile.layer setMasksToBounds:YES];
    imgProfile.layer.cornerRadius= 30;
    imgProfile.layer.borderWidth =0;
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnGETUSERPROFILE_API];
    
    
    [COMMON addLoading:self];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        dicProfile =[NSDictionary dictionaryWithDictionary:response[@"user"]];
        [self loadDataOnView];
    };
    
    
}

#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == tfDate) {
        self.datePicker.hidden = false;
        self.pickerView.hidden = true;
    }else if (textField == tfCivilite){
        self.datePicker.hidden = true;
        self.pickerView.hidden = false;
    }
    
    return true;
}
- (void) dateUpdated:(UIDatePicker *)datePicker {
    
    tfDate.text = [formatter stringFromDate:self.datePicker.date];
}
- (IBAction)buttonDone:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - PickerView Delegate


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [civiliteArray objectAtIndex:row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [civiliteArray count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    tfCivilite.text=[civiliteArray objectAtIndex:row];
    
        pickerValue=[NSNumber numberWithInteger:row];
}





-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
}
/*
 {
 user =     {
 address =         {
 address = "Hanoi,Vietnam";
 created = "2015-10-08T10:42:57+02:00";
 favorite = 1;
 id = 116;
 latitude = "21.032221";
 longitude = "105.852955";
 title = hh;
 };
 birthday = "";
 courtesy = 0;
 firstname = Manh;
 fullname = "Manh ManhManh";
 id = 160;
 lastname = ManhManh;
 parameters =         {
 friend = 1;
 };
 photo = "/uploads/users/images/thumb/3699cad9e4240bf3508bad7958dca8f1d0fcec34.jpeg";
 relation = 0;
 usertag = "manh-manhmanh";
 };
 }
 */
-(void)loadDataOnView
{
    if(dicProfile)
    {
        //profilepicture
        NSString *imgUrl=@"";
        if (dicProfile[@"profilepicture"] != nil) {
            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicProfile[@"profilepicture"]];
        }else{
            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicProfile[@"photo"]];
        }
        [imgProfile sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
        imgIconProfile.hidden=YES;
        
        //lastname
        
        tfLastName.text =dicProfile[@"lastname"];
        tfFirtName.text =dicProfile[@"firstname"];
        
        tfEmail.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"my_email"];
        
        NSString *strdate =dicProfile[@"birthday"];
        tfDate.text =strdate;
        tfCivilite.text=[civiliteArray objectAtIndex:[dicProfile[@"courtesy"] intValue]];
    }
}
#pragma textfield delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    
//    if (textField == tfCivilite) {
//        [textField resignFirstResponder];
//        [self pickerViewInitialization];
//    }
//    else {
//        [self dismissActionSheet];
//    }
//    
//}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [myPersonalScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
#pragma mark - Profile Image Picker

- (IBAction)profileImageOptions:(id)sender{
    PhotoSheetSignUpVC *vc = [[PhotoSheetSignUpVC alloc] initWithNibName:@"PhotoSheetSignUpVC" bundle:nil];
    [vc setMyCallback:^(NSDictionary*data){
        dicImageInfo = data;
        imgProfile.image = dicImageInfo[@"image"];
        imgIconProfile.hidden=YES;
        
        [self uploadImage];
    }];
    
    [self presentViewController:vc animated:NO completion:^{}];
    [self.view endEditing:YES];
}

#pragma mark-PickerDelegate

-(void)selectedElement:(NSString *)_selectedElement index:(int)_index{
    [tfCivilite setText:_selectedElement];
}
- (BOOL)validateEmailWithString:(NSString*)emailText
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailText];
}
#pragma mark - validata
- (BOOL)isValidData{
    
    if ([tfLastName.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[NSLocalizedString(@"Name", @"") lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    if ([tfFirtName.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[NSLocalizedString(@"FirstName", @"") lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    
    if ([tfEmail.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[NSLocalizedString(@"Email", @"") lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    if(![self validateEmailWithString:tfEmail.text]){
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strINVALIDEMAIL),[NSLocalizedString(@"", @"") lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
        
    }
    
    if (pickerValue ==nil) {
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber * myNumber = [f numberFromString:@"0"];
        
        pickerValue=myNumber;
        [COMMON removeProgressLoading];
        
        return NO;
    }
    
    return YES;
}

- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}

/*
 user[courtesy] = 0 => 1 => Monsieur, 2 => Madame
 1714:      *      user[lastname] = "VALOT"
 1715:      *      user[firstname] = "Vincent"
 1716:      *      user[email] = "v.valot@e-conception.fr"
 1717:      *      user[password][old] = "Mot de passe encodé"
 1718:      *      user[password][new] = "Mot de passe encodé"
 1719:      *      user[password][confirmation] = "Mot de passe encodé"
 1720:      *      user[photo][file]
 */

#pragma mark
-(IBAction)suivantAction:(id)sender
{
    if([self isValidData])
    {
        
        NSMutableArray  *keys= [NSMutableArray new];
        NSMutableArray  *objects= [NSMutableArray new];
        NSDictionary  *postDict= [NSDictionary new];
        
            keys = [NSMutableArray arrayWithObjects:
                    @"user[courtesy]",
                    @"user[lastname]",
                    @"user[firstname]",
                    @"user[email]",
                    @"user[birthday]",
                    nil];
            objects = [NSMutableArray arrayWithObjects:
                       pickerValue,
                       tfLastName.text,
                       tfFirtName.text,
                       tfEmail.text,
                       tfDate.text,
                       nil
                       ];

        postDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        
        WebServiceAPI *serviceAPI =[WebServiceAPI new];

        serviceAPI.onComplete =^(id response, int errCode)
        {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

            [COMMON removeProgressLoading];
            
//            NSLog(@"%@", response);
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            if ([response valueForKey:@"success"]){
                    [UIAlertView showWithTitle:str(strTitle_app)
                                       message:str(strMessage21)
                             cancelButtonTitle:str(strOK)
                             otherButtonTitles:nil
                                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                          [self gotoback];
                                      }];
            }
            else{
                NSString *errorMsg=response[@"message"];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
                [alert show];
            }
        };
        
        [COMMON addLoading:self];
        
        [serviceAPI putUserAction:postDict withAttachmentImage:NULL];

    }
}

-(void) uploadImage
{

    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

//        NSLog(@"%@", response);
        
        if ([response[@"success"] intValue] == 1) {
            //Change success...->update image
        }
    };

    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    NSDictionary *attachment =[NSDictionary new];
    if(UIImageJPEGRepresentation(dicImageInfo[@"image"], 0.5)){        //neu su dun avarta thi su dung api, con su dung anh thi dung api khac
        
        attachment = @{@"kFileName": @"image.png",
                       @"kFileData": UIImageJPEGRepresentation(dicImageInfo[@"image"], 0.5) };
        [serviceAPI postUserProfilePictureActionwithAttachmentImage:attachment];
    }
}
@end
