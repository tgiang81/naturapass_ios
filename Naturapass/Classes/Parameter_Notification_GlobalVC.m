//
//  Parameter_Notification_GlobalVC.m
//  Naturapass
//
//  Created by manh on 2/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import "Parameter_Notification_GlobalVC.h"
#import "Parameter_Notification_Detail.h"
#import "CellKind18.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_Notification_GlobalVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;

}
@end

@implementation Parameter_Notification_GlobalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.nofiticationType == ISSMARTPHONE) {
        lbTitle.text = str(strNOTIFICATIONS_SMARTPHONE);
    }
    else
    {
        lbTitle.text = str(strNOTIFICATIONS_EMAIL);

    }
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind18" bundle:nil] forCellReuseIdentifier:identifierSection1];
//    self.tableControl.estimatedRowHeight = 66;
    arrData = [NSMutableArray new];

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *strPath;
    NSDictionary *fDic = nil;
    
    if (self.nofiticationType == ISSMARTPHONE) {
        strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],CACHE_PARAMESTER_NOTIFICATION)];
        
    }
    else
    {
        strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],CACHE_PARAMESTER_EMAIL)];
    }
    
    fDic = [NSDictionary dictionaryWithContentsOfFile:strPath];
    [self loadData:fDic];

    
    if ([COMMON isReachable]) {
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        NSString *strKey =@"";
        if (self.nofiticationType == ISSMARTPHONE) {
            strKey = @"notifications";
            [serviceObj fnGET_NOTI_PARAMETERS];
        }
        else
        {
            strKey = @"emails";
            [serviceObj fnGET_EMAIL_PARAMETERS];
            
        }
        if (fDic == nil) {
            [COMMON addLoading:self];
        }
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            [COMMON removeProgressLoading];
            
            if([response[strKey] isKindOfClass: [NSDictionary class]])
            {
                NSDictionary *dic = response[strKey];
                NSString *strPath;
                if (self.nofiticationType == ISSMARTPHONE) {
                    strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],CACHE_PARAMESTER_NOTIFICATION)];
                }
                else
                {
                    strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],CACHE_PARAMESTER_EMAIL)];
                }
                // Write array
                [dic writeToFile:strPath atomically:YES];
                
                [self loadData:dic];
            }
        };
    }
}

-(void)loadData:(NSDictionary*)dic
{
    NSMutableArray *arrTemp = [NSMutableArray new];
    if ([dic[@"publication"] isKindOfClass:[NSArray class]]) {
        NSDictionary *dicTmp = @{@"name":str(strPUBLICATION),
                                 @"image":@"icon-pub",
                                 @"listcontent":dic[@"publication"] };
        [arrTemp addObject:dicTmp];
    }
    if ([dic[@"amis"] isKindOfClass:[NSArray class]]) {
        NSDictionary *dicTmp = @{@"name":str(strAMIS),
                                 @"image":@"icon-amis",
                                 @"listcontent":dic[@"amis"] };
        [arrTemp addObject:dicTmp];
    }
    if ([dic[@"agenda"] isKindOfClass:[NSArray class]]) {
        NSDictionary *dicTmp = @{@"name":str(strCHANTIERS),
                                 @"image":@"icon-agenda",
                                 @"listcontent":dic[@"agenda"] };
        [arrTemp addObject:dicTmp];
    }
    if ([dic[@"group"] isKindOfClass:[NSArray class]]) {
        NSDictionary *dicTmp = @{@"name":@"GROUPE",
                                 @"image":@"icon-groupe",
                                 @"listcontent":dic[@"group"] };
        [arrTemp addObject:dicTmp];
    }
    if ([dic[@"chat"] isKindOfClass:[NSArray class]]) {
        NSDictionary *dicTmp = @{@"name":str(strDISCUSSION),
                                 @"image":@"icon-discussion",
                                 @"listcontent":dic[@"chat"] };
        [arrTemp addObject:dicTmp];
    }
    arrData = arrTemp;
    [self.tableControl reloadData];

}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 66;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind18 *cell = nil;
    
    cell = (CellKind18 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrData[indexPath.row];
    Parameter_Notification_Detail *viewController1 = [[Parameter_Notification_Detail alloc] initWithNibName:@"Parameter_Notification_Detail" bundle:nil];
    viewController1.arrDataFull = dic[@"listcontent"];
    viewController1.nofiticationType = self.nofiticationType;
    [self pushVC:viewController1 animate:YES];
}

@end
