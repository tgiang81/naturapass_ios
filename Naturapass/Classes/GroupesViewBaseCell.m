//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GroupesViewBaseCell.h"
#import "Define.h"
#import "CommonHelper.h"

@implementation GroupesViewBaseCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

    [self.mainView.layer setMasksToBounds:YES];
    self.mainView.layer.cornerRadius= 5.0;
    self.mainView.layer.borderWidth =0.5;
    self.mainView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.imgViewAvatar.layer.masksToBounds = YES;
    self.imgViewAvatar.layer.cornerRadius = 30;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_ADMIN_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_NORMAL_COLOR)];
            [self.btnUnsubscribe setBackgroundColor:UIColorFromRGB(GROUP_MUR_CANCEL_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.lblName setTextColor:color];
    [self.lblNbSubcribers setTextColor:color];
    [self.btnEnterGroup setBackgroundColor:color];
    [self.btnAdminstrator setBackgroundColor:color];
//    [self.btnSetting setBackgroundColor:color];

}
@end
