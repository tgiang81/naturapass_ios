//
//  SplashVC.m
//  Naturapass
//
//  Created by Giang on 4/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "SplashVC.h"
#import "FileHelper.h"

@interface SplashVC ()
{
    IBOutlet UIButton *btnClose;

}
@property (weak, nonatomic) IBOutlet UIImageView *imgSplash;

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imgSplash.image = [UIImage imageNamed: @"Partenaires_interstice.jpg"];
    
    [btnClose setAlpha:1.0f];
    [btnClose setTitleColor:[UIColor colorWithWhite:0.9 alpha:0.9] forState:UIControlStateNormal|UIControlStateHighlighted];
    [btnClose setTitle:NSLocalizedString(@"Close", @"") forState:UIControlStateNormal];
    [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:11.0f]];
    [btnClose setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
    btnClose.layer.cornerRadius = 3.0f;
    btnClose.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.9].CGColor;
    btnClose.layer.borderWidth = 1.0f;
    btnClose.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
}

- (IBAction)fnClose:(id)sender {
    if (self.CallBackClose) {
        self.CallBackClose();
    }
}

- (IBAction)fnGoToGameFair:(id)sender {
    if (self.callBackGoto) {
        self.callBackGoto();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
