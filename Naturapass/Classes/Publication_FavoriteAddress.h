//
//  Publication_FavoriteAddress.h
//  Naturapass
//
//  Created by Giang on 10/9/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "PublicationBaseVC.h"

typedef void (^onDataBack)(NSDictionary*);

@interface Publication_FavoriteAddress : PublicationBaseVC
@property (nonatomic, copy) onDataBack myCallback;
@property (assign) BOOL isFromSetting;

@end
