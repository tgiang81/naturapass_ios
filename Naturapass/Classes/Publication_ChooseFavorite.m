//
//  Publication_ChooseFavorite.m
//  Naturapass
//
//  Created by Giang on 1/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Publication_ChooseFavorite.h"
#import "Publication_Legende_Carte.h"
#import "Publication_ListFavorite.h"

@interface Publication_ChooseFavorite ()
{
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnOUI;
    IBOutlet UIButton *btnNON;

}
@end

@implementation Publication_ChooseFavorite

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strSouhaitezvous_utiliser_un_favori);
    [btnOUI setTitle:str(strOUI) forState:UIControlStateNormal];
    [btnNON setTitle:str(strNON) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    
    UIButton*btn = (UIButton*) sender;
    
    if (btn.tag == 10) {
        //cancel
        Publication_Legende_Carte *viewController1 = [[Publication_Legende_Carte alloc] initWithNibName:@"Publication_Legende_Carte" bundle:nil];
        
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

    }else{
    //next select favorite
        
        Publication_ListFavorite *viewController1 = [[Publication_ListFavorite alloc] initWithNibName:@"Publication_ListFavorite" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

    }
}

@end
