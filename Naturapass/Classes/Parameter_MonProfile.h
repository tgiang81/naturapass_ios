//
//  Parameter_Profile.h
//  Naturapass
//
//  Created by Giang on 10/12/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ParameterBaseVC.h"

@interface Parameter_MonProfile : ParameterBaseVC <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    NSDateFormatter *formatter;
    NSArray *arrayData;

}

@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *viewPickerBackground;

@end
