//
//  Filter_Cell_Type4.h
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDPickerTextField.h"
@interface Filter_Cell_Type4 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet MDPickerTextField *txtDebut;
@property (weak, nonatomic) IBOutlet MDPickerTextField *txtFin;

@end
