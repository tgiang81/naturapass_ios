//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"

typedef void (^AlertSuggestPersoneViewCallback)(NSArray *arrResult);

@interface AlertSuggestPersoneView : UIView<UITableViewDelegate,UITableViewDataSource>
{
}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic)  UIButton *btnDissmiss;

@property(nonatomic,strong)  NSString *strSearch;

@property (nonatomic,copy) AlertSuggestPersoneViewCallback callback;
-(void)doBlock:(AlertSuggestPersoneViewCallback ) cb;
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect;
-(instancetype)initSuggestView;
-(IBAction)closeAction:(id)sender;
- (void) processLoungesSearchingURL:(NSString *)searchText;
-(void)updateArraySelected:(NSArray*)arrSelected;
-(void)removeItemSelected:(NSDictionary*)dicRemove;
-(void)fnDataInput:(NSString*)strSearch withArrSelected:(NSArray*)arrSelected;
@end
