//
//  PublicationAddAddress.m
//  Naturapass
//
//  Created by Giang on 11/9/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "PublicationAddAddress.h"
#import "MapDataDownloader.h"
#import "DatabaseManager.h"

@interface PublicationAddAddress ()
{
    IBOutlet UILabel *lbDescription1;
    IBOutlet UIButton *btnSuivant;

}
@property (weak, nonatomic) IBOutlet UITextField *nameAddress;

@end

@implementation PublicationAddAddress

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self addSubNav:@"SubNavigation_PRE_General"];
    lbDescription1.text = str(strEntre_un_nom_pour_votre_adresse);
    [btnSuivant setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    self.nameAddress.placeholder = str(strEx_Cabane_de_agenda);
    UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
    
    btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(void) refreshData
{
        NSArray * controllerArray = [[self navigationController] viewControllers];
        
        for (int i=0; i < controllerArray.count; i++) {
            if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                
                [self.navigationController popToViewController:controllerArray[i] animated:YES];
                
                return;
            }
        }
        
        [self gotoback];
}

-(IBAction)fnAddAddress:(id)sender
{
    
    if ((int)self.nameAddress.text.length <= 0)
    {
        
        [UIAlertView showWithTitle:str(strTitle_app) message:str(strVeuillez_entrer_une_adresse) cancelButtonTitle:str(strOK) otherButtonTitles:nil tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
        }];
        return;
    }
    
    //send...new address
    //
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    [COMMON addLoading:self];
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    
    serverAPI.onComplete = ^(id response, int errCode)
    {
        //Update database with address...
        //        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_address]];
        //201
        
        /*
         address =     {
         address = "Hanoi, Vietnam";
         favorite = 0;
         id = 320;
         latitude = "21.02568";
         longitude = "105.82553";
         title = cc;
         };
         */
        [COMMON removeProgressLoading];

        if ([response[@"address"] isKindOfClass: [NSDictionary class]])
        {

                // Create and enqueue an operation using the previous method
                [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

                [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                    
                    /*
                     @"CREATE TABLE IF NOT EXISTS `tb_favorite_address` (\
                     `c_id`    INTEGER,\
                     `c_title`    TEXT,\
                     `c_favorite`    TEXT,\
                     `c_lat`    REAL,\
                     `c_lon`    REAL,\
                     `c_user_id`    INTEGER,\
                     PRIMARY KEY(c_id,c_user_id)\
                     );",
                     */
                    
                    NSDictionary *dic = response[@"address"];
                    
                    NSString *strSql = [NSString stringWithFormat: @"INSERT OR REPLACE INTO `tb_favorite_address` (`c_id`,`c_title`,`c_favorite`,`c_lat`,`c_lon`,`c_user_id`) VALUES ('%@','%@','%@','%@','%@','%@')", dic[@"id"], dic[@"title"], dic[@"favorite"], dic[@"latitude"], dic[@"longitude"], sender_id ] ;
                    //add version number to tble version.
                    
                    BOOL isOK =   [db  executeUpdate:strSql];

                }];
                
                [self refreshData];
            
            
        }
        
        
        
    };
    
    [serverAPI fnPOST_USER_ADDRESS:@{@"address":
                                         @{@"address":self.myDic[@"address"]
                                           ,@"latitude":self.myDic[@"latitude"],
                                           @"longitude":self.myDic[@"longitude"],
                                           @"title":self.nameAddress.text}}];
}
@end
