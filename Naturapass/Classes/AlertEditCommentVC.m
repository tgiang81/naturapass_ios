//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertEditCommentVC.h"
#import "Define.h"
#import "AppCommon.h"

@implementation AlertEditCommentVC
{
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
    UIBarButtonItem             *previousBarItem;
    UIBarButtonItem             *nextBarItem;
    __weak IBOutlet UILabel *numberRemain;

}
-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (self.keyboardToolbar == nil)
    {
        self.keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        self.keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:self
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strOK)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard:)];
        
        [self.keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                        spaceBarItem,
                                        spaceBarItem,
                                        spaceBarItem,
                                        doneBarItem, nil]];
        
        
    }
    [_tvEditText setInputAccessoryView:self.keyboardToolbar];
}
- (void)resignKeyboard:(id)sender
{
    [_tvEditText resignFirstResponder];
    
}
-(instancetype)initWithTitle:(NSString*)stitle  strcomment:(NSString*)strcomment;
{
    self =[super initWithNibName:@"AlertEditCommentVC" bundle:nil];
    if (self) {
        strTitle =stitle;
        strComment = strcomment;
    }
    return self;
}
#pragma mark - viewdid
-(void)viewDidLoad
{
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];
    _tvEditText.delegate = self;
    [numberRemain setText:[NSString stringWithFormat:@"%d/%d", 0,limitComment]];
    [numberRemain setTextColor:[UIColor darkGrayColor]];
    if (strTitle) {
        _lbTitle.text =strTitle;
        
    }
    if (strComment) {
        _tvEditText.text =strComment;
    }
    [self InitializeKeyboardToolBar];
}

#pragma callback
-(void)setCallback:(AlertEditCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertEditCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showInVC:(UIViewController*)vc
{
    
    [vc presentViewController:self animated:NO completion:^{
        
    }];
}
-(IBAction)closeAction:(id)sender
{
    NSDictionary *dic =@{@"index":@0};
    [self hiddenAlert];
    if (_callback) {
        _callback(dic);
    }
}
-(IBAction)OUIAction:(id)sender
{
    NSString *strText =self.tvEditText.text?self.tvEditText.text:strEMPTY;
    NSDictionary *dic =@{@"index":@1,@"comment":strText};
    [self hiddenAlert];
    if (_callback) {
        _callback(dic);
    }
}
-(void)hiddenAlert
{
    [self dismissViewControllerAnimated:YES completion:^{}];
    [self.view endEditing:YES];
}
#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
//MARK: - Text View Delegate
-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    int remain = (int) (limitComment - (a.text.length + c.length));
    if (remain < 0) {
        NSString *strFullText = [a.text stringByAppendingString:c];
        NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitComment)];
        a.text = subString;
        [numberRemain setTextColor:[UIColor redColor]];
        [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitComment:remain,limitComment]];
    }
    
    return remain >= 0;
}
- (void)textViewDidChange:(UITextView *)textView
{
    int remain = (int)textView.text.length;
    [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitComment:remain,limitComment]];
    
    // Check if the count is over the limit
    if(remain >= 4990) {
        // Change the color
        [numberRemain setTextColor:[UIColor redColor]];
    }
    else if(remain > 4950 && remain < 4990) {
        // Change the color to yellow
        [numberRemain setTextColor:[UIColor orangeColor]];
    }
    else {
        // Set normal color
        [numberRemain setTextColor:[UIColor darkGrayColor]];
    }
}
@end
