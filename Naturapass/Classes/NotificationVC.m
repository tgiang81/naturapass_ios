 
//
//  NotificationVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "NotificationVC.h"
#import "Define.h"

#import "MurVC.h"

#import "TypeCellNotification.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASSharedTimeFormatter.h"

//list target vc
#import "AnimalsEntity.h"
#import "GroupEnterOBJ.h"
#import "CommentDetailVC.h"
#import "GroupEnterMurVC.h"
#import "GroupSettingMembres.h"
#import "ChasseSettingMembres.h"
#import "ChatVC.h"
#import "MapGlobalVC.h"
#import "Amis_Demand_Invitation.h"
#import "FriendInfoVC.h"
#import "GroupInvitationAttend.h"
#import "ChassesInvitationAttend.h"
#import "ChatListe.h"
#import "ChassesCreateOBJ.h"
#import "GroupCreateOBJ.h"
#import "TypeCellHeaderNotification.h"
#import "TypeCellFooterNotification.h"
#import "CommonObj.h"
#import "NotifiEntity.h"
#import "NSDate+Extensions.h"
#import "LiveHuntOBJ.h"

static NSString *identifierSection1 = @"TypeCellNotification";
static NSString *identifierHeader = @"HeaderCellID";
static NSString *identifierFooter = @"FooterCellID";


@interface NotificationVC ()
{
    NSMutableArray *arrData;
    int myID;
    NSOperationQueue * operationQueue;
    IBOutlet UILabel *lbTitle;
}
@property (nonatomic, strong) TypeCellNotification *prototypeCell;

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(@"NOTIFICATIONS")];
    subview.imgComment.hidden = YES;
    self.vContainer.backgroundColor = UIColorFromRGB(NEW_MUR_BACKGROUND_COLOR);
    lbTitle.text = str(strNOTIFICATIONS);
    myID =[[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];
    // Do any additional setup after loading the view from its nib.
    
    arrData = [NSMutableArray new];
    operationQueue = [NSOperationQueue new];
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellNotification" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellHeaderNotification" bundle:nil] forCellReuseIdentifier:identifierHeader];
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellFooterNotification" bundle:nil] forCellReuseIdentifier:identifierFooter];
    
    self.tableControl.estimatedRowHeight = 80;
    self.tableControl.rowHeight = UITableViewAutomaticDimension;
    self.tableControl.backgroundColor = [UIColor clearColor];
    [self initRefreshControl];
    
    [self loadCache];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([COMMON isReachable]) {
        [self getNotificationsWithLoadMore:NO];
    }
}
- (void)insertRowAtTop {
    if ([COMMON isReachable]) {
        [self getNotificationsWithLoadMore:NO];
    }
}
- (void)insertRowAtBottom {
    [self stopRefreshControl];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - API
- (void)getNotificationsWithLoadMore:(BOOL)loadMore{
    /*
     "notifications": [
     {
     "id": 3506,
     "content": "Gilles Robillard vous a demandé en ami",
     "read": 1,
     "link": "https://naturapass.e-conception.fr/users/profile/gilles-robillard",
     "updated": "2015-12-09T08:03:48+01:00",
     "type": "user.friendship.asked",
     "object_id": "160",
     "sender": {
     "id": 259,
     "fullname": "Gilles Robillard",
     "firstname": "Gilles",
     "lastname": "Robillard",
     "usertag": "gilles-robillard",
     "courtesy": 1,
     "photo": "/uploads/users/images/thumb/21214c887270e5604b46c12de5404adc61214bad.jpeg",
     "parameters": {
     "friend": true
     }
     }
     }
     */
    
    [operationQueue cancelAllOperations];
    
    [COMMON addLoading:self];

    NSInvocationOperation *operationOne = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(doOperation:) object: [NSNumber numberWithBool:loadMore]];
    [operationQueue addOperation:operationOne];
}

- (void)doOperation:(NSNumber*)param
{
    BOOL loadMore = [param boolValue];
    
    NSString *strCount=@"0";
    if (loadMore) {
        strCount =[NSString stringWithFormat:@"%lu",(unsigned long)arrData.count];
        
    }
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj getUserNotificationStatus:strCount];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self stopRefreshControl];

        if (!loadMore) {
            [arrData removeAllObjects];
        }
        
        if (!response) {
            return ;
        }
        
        [arrData addObjectsFromArray:[response objectForKey:@"notifications"]];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [self saveCacheNotiWithData:arrData];
            [self.tableControl reloadData];
            
        });
    };
}

#pragma mark - TABLEVIEW
//- (TypeCellNotification *)prototypeCell
//{
//    if (!_prototypeCell)
//    {
//        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
//    }
//    return _prototypeCell;
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[TypeCellNotification class]])
    {
        TypeCellNotification *cell = (TypeCellNotification *)cellTmp;
        NSDictionary *dic = arrData[indexPath.row];
        
        NSString *strImage=@"";
        if (dic[@"owner"][@"profilepicture"] != nil) {
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"sender"][@"profilepicture"]];
        }else{
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"sender"][@"photo"]];
        }
        strImage = [[strImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSURL * url = [[NSURL alloc] initWithString:strImage];
        [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
        //Format
        NSString *strContent =[dic[@"content"] isKindOfClass:[NSString class]] ? [dic[@"content"] emo_emojiString]: @"";
        
        NSString *trimmedString = [strContent stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];

        cell.label1.text= trimmedString;

        NSString *relativeTime = [NSString stringWithFormat:@"%@",dic[@"updated"]];
        NSString * outputString = [NSDate convertDateGmt2ToSystem:relativeTime withInputFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ" withOupuFormat:@"dd-MM-yyyy HH:mm" ];
        
        NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];
        
        cell.label2.text = strDateLocation;
        cell.label2.textColor = UIColorFromRGB(POSTER_ANNULER_COLOR);
        //read
        if ([dic[@"read"]boolValue]==NO) {
            cell.backgroundColor = UIColorFromRGB(NOTIFI_CELL_ACTIVE_COLOR);
            [cell.btnDel addTarget:self action:@selector(tickReadAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.imgXIcon.hidden=NO;
            cell.imgDotRed.hidden = NO;
            cell.label1.textColor = [UIColor whiteColor];
        }
        else
        {
            cell.backgroundColor = UIColorFromRGB(NOTIFI_CELL_INACTIVE_COLOR);
            [cell.btnDel removeTarget:self action:@selector(tickReadAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.imgXIcon.hidden=YES;
            cell.imgDotRed.hidden = YES;
            cell.label1.textColor = UIColorFromRGB(POSTER_ANNULER_COLOR);
        }
        cell.btnDel.tag=indexPath.row+100;
        cell.contentView.backgroundColor=[UIColor clearColor];
        
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setNeedsLayout];
        [cell layoutIfNeeded];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 0;
    }
    BOOL countRead = false;
    for (NSDictionary *dic in arrData) {
        if ([dic[@"read"]boolValue]==NO) {
            countRead = true;
            break;
        }
    }
    if (countRead == false) {
        return 0;
    }
    return 50;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    TypeCellHeaderNotification *cell = nil;
    
    cell = (TypeCellHeaderNotification *)[self.tableControl dequeueReusableCellWithIdentifier:identifierHeader];
    cell.backgroundColor=[UIColor whiteColor];
    
    [cell.btnTick addTarget:self action:@selector(tickAllAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnTick setTitle:[@"Tout marquer comme lu" uppercaseString] forState:UIControlStateNormal];
    cell.btnTick.backgroundColor = UIColorFromRGB(POSTER_ANNULER_COLOR);
    cell.contentView.backgroundColor = UIColorFromRGB(NOTIFI_CELL_MARK_ALL_COLOR);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0) {
        return arrData.count;
    }
    else
    {
        if (arrData.count==0) {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (indexPath.row >= arrData.count) {
//        return 50;
//        
//    }
//    
//    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
//    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    return size.height+1;
//}
//
//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        TypeCellFooterNotification *cell = nil;
        
        cell = (TypeCellFooterNotification *)[self.tableControl dequeueReusableCellWithIdentifier:identifierFooter];
        cell.backgroundColor=[UIColor whiteColor];
        
        [cell.btnShowAll addTarget:self action:@selector(showAllAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.contentView.backgroundColor=[UIColor clearColor];
        cell.backgroundColor=[UIColor clearColor];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    
    TypeCellNotification *cell = (TypeCellNotification *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return;
    }
    // goto screen:
    //    AppDelegate  *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    NSDictionary*dic = [arrData objectAtIndex:indexPath.row];
    NSString *pushType =dic[@"type"] ;
    NSString *Idnotif =dic[@"object_id"];
    [self readNotification:(int)indexPath.row];
    
    BaseVC *vc =nil;
    BOOL getAPI =  NO;
    //refresh
    [[ChassesCreateOBJ sharedInstance] resetParams];
    [[GroupCreateOBJ sharedInstance] resetParams];
    if (  [pushType isEqualToString:publication_comment_liked] || [pushType isEqualToString:publication_commented]||[pushType isEqualToString:publication_same_commented] || [pushType isEqualToString:publication_liked]|| [pushType isEqualToString:publication_processed_success]|| [pushType isEqualToString:publication_shared]) {
        vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
        vc.expectTarget = ISMUR;
    }
    else if ([pushType isEqualToString:group_publication_new])
    {
        vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
        vc.expectTarget = ISGROUP;
        Idnotif = dic[@"publication_id"];
    }
    else if ([pushType isEqualToString:lounge_publication_new])
    {
        
        if (dic[@"live"]) {
            vc.isLiveHunt = YES;
            //
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
            [serviceObj getItemWithKind:MYCHASSE myid:dic[@"live"]];
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                
                [COMMON removeProgressLoading];
                if (!response) {
                    return;
                }
                if([response isKindOfClass: [NSArray class] ]) {
                    return ;
                }
                
                if (response[@"lounge"]) {
                    NSDictionary *dic = [response valueForKey:@"lounge"];
                    [[GroupEnterOBJ sharedInstance] resetParams];
                    
                    [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
                    [LiveHuntOBJ sharedInstance].dicLiveHunt = dic;
                    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                    AppDelegate  *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];

                    [appDelegate showLiveHunt];
                }
                return;
                
            };
            return;
        }else{
            vc=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
            vc.expectTarget = ISLOUNGE;
            Idnotif = dic[@"publication_id"];
        }
    }
    else if ([pushType isEqualToString:group_join_invited ])
    {
        vc = [[GroupInvitationAttend alloc] initWithNibName:@"GroupInvitationAttend" bundle:nil];
        vc.expectTarget = ISGROUP;
    }
    
    else if ([pushType isEqualToString:group_join_accepted] )
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group]];
        
        vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        vc.expectTarget = ISGROUP;
        [[GroupEnterOBJ sharedInstance] resetParams];
        getAPI = YES;
        
    }
    else if ([pushType isEqualToString:group_join_asked]||
             [pushType isEqualToString:group_join_valid_asked])
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group]];
        
        vc = [[GroupSettingMembres alloc] initWithNibName:@"GroupSettingMembres" bundle:nil];
        vc.expectTarget = ISGROUP;
        //
        [[GroupEnterOBJ sharedInstance] resetParams];
        getAPI = YES;

    }
    else if ([pushType isEqualToString:lounge_join_invited ])
    {
        vc = [[ChassesInvitationAttend alloc] initWithNibName:@"ChassesInvitationAttend" bundle:nil];
        vc.expectTarget = ISLOUNGE;
    }
    // nhan duoc thong bao khi chu group dong y hay moi vao group
    else if ([pushType isEqualToString:lounge_join_accepted])
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt,kSQL_agenda]];
        
        vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        vc.expectTarget = ISLOUNGE;
        [[GroupEnterOBJ sharedInstance] resetParams];
        getAPI = YES;

    }
    //nhan duoc thong bao khi co 1 nguoi muon duoic join vao group
    else if ([pushType isEqualToString:lounge_join_asked]||
             [pushType isEqualToString:lounge_join_valid_asked]||
             [pushType isEqualToString:lounge_status_changed])
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt,kSQL_agenda]];
        
        //validation
        vc = [[ChasseSettingMembres alloc] initWithNibName:@"ChasseSettingMembres" bundle:nil];
        vc.expectTarget = ISLOUNGE;
        [[GroupEnterOBJ sharedInstance] resetParams];
        getAPI = YES;

    }
    // nhan duoc thong bao khi co 1 nhom chat
    else if ([pushType isEqualToString:group_chat_new_message]||
             [pushType isEqualToString:lounge_chat_new_message])
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group, kSQL_hunt,kSQL_agenda]];
        
        //se nhay den man hinh chat group
        vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        if ([pushType isEqualToString:lounge_chat_new_message]) {
            vc.expectTarget = ISLOUNGE;
//            if (dic[@"live"])
//            {
//                vc.isLiveHunt = YES;
//            }
            
        }
        else
        {
            vc.expectTarget = ISGROUP;
            
        }
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=@{@"id": Idnotif};
        getAPI = YES;

    }
    else if ([pushType isEqualToString:discussion_new_message])
    {
        //se nhay den man hinh chat
        vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        vc.expectTarget = ISDISCUSS;
    }
    
    //nhan duoc thong bao khi co loi moi ket ban
    else if([pushType isEqualToString:user_friendship_asked])
    {
        vc = [[Amis_Demand_Invitation alloc] initWithNibName:@"Amis_Demand_Invitation" bundle:nil];
        vc.expectTarget = ISAMIS;
        
    }
    // sau khi gui loi moi ket ban, neu nguoi do chap nhan, se nhan duoc thong bao confirmed va khi click vao se toi friendinfo
    else if([pushType isEqualToString:user_friendship_confirmed])
    {
        FriendInfoVC *vc1 = [[FriendInfoVC alloc] initWithNibName:@"FriendInfoVC" bundle:nil];
        [self pushVC:vc1 animate:YES];
        vc1.expectTarget = ISMUR;
        [vc1 setFriendDic:dic[@"sender"]];
        
        vc1.isNotifi=YES;
        vc1.IdNotifi =Idnotif;
        return;
    }
    else if([pushType isEqualToString:lounge_discussion_new_message])
    {
        //se nhay den man hinh chat group
        vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        vc.expectTarget = ISLOUNGE;
        //default Agenda chat.
        
//        if (dic[@"live"])
//        {
//            vc.isLiveHunt = YES;
//        }
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=@{@"id": Idnotif};
        getAPI = YES;

    }
    // push
    vc.isNotifi=YES;
    vc.IdNotifi =Idnotif;
    vc.mParent = self;
    
    if (getAPI == YES) {
        [self gotoNotifi_withVC:vc];
    }
    else
    {

        [(MainNavigationController *)self.navigationController pushViewController:vc animated:YES completion:^ {
            NSLog(@"COMPLETED");
        }];
    }

}
-(void)gotoNotifi_withVC:(BaseVC *)vc
{
    
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if (vc.expectTarget== ISGROUP) {
        [serviceObj getItemWithKind:MYGROUP myid:vc.IdNotifi];
        
    } else{
        [serviceObj getItemWithKind:MYCHASSE myid:vc.IdNotifi];
        
    }
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        BOOL isExist = NO;
        
        if (vc.expectTarget== ISGROUP && response[@"group"]) {
            isExist = YES;
            NSDictionary *dic = [response valueForKey:@"group"];
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
            [(MainNavigationController *)self.navigationController pushViewController:vc animated:YES completion:^ {
                NSLog(@"COMPLETED");
            }];

        }
        if (vc.expectTarget== ISLOUNGE && response[@"lounge"]) {
            isExist = YES;

            NSDictionary *dic = [response valueForKey:@"lounge"];
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
            [(MainNavigationController *)self.navigationController pushViewController:vc animated:YES completion:^ {
                NSLog(@"COMPLETED");
            }];
        }
        
        if (!isExist && response[@"message"]) {
            //warning
            if (errCode == 404) {
                NSString *strMessage = response[@"message"];
                NSString *errMsg = OBJECT_DOES_NOT_EXIST;

                if ([strMessage rangeOfString:@"Group"].location == NSNotFound) {
                    //not found group => Agenda
                    errMsg = AGENDA_DOES_NOT_EXIST;

                }else{
                    //found GROUP
                    errMsg = GROUP_DOES_NOT_EXIST;

                }
                
                [KSToastView ks_showToast:errMsg duration:3.0f completion: ^{
                }];
            }
            else
            {
                [AZNotification showNotificationWithTitle:response[@"message"] controller:self notificationType:AZNotificationTypeError];
            }
        }
        
        /*
         "Cette publication n'existe plus" -> for a post
         "Cet agenda n'existe plus" -> for an event
         "Ce groupe n'existe plus" -> for a group
         "Cet objet n'existe plus" -> for all other objects*
         */
    };
}
#pragma mark --
-(IBAction)tickAllAction:(id)sender
{
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj putReadAllNotification];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (response[@"success"]) {
            for (int i=0; i<arrData.count; i++) {
                NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: [arrData objectAtIndex:i]];
                [dic setObject:@1 forKey:@"read"];
                [arrData replaceObjectAtIndex:i withObject:dic];
                [self fnReadCache:dic withRead:1];
            }
            
            [self.tableControl reloadData];
            [CommonObj sharedInstance].nbUnreadNotification =0;
            
        }
    };
}
-(IBAction)showAllAction:(id)sender
{
    [self getNotificationsWithLoadMore:YES];
}
-(IBAction)tickReadAction:(id)sender
{
    NSInteger index = (NSInteger)[sender tag]-100;
    [self readNotification:index];
}
-(void)readNotification:(NSInteger)index
{
    NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary: [arrData objectAtIndex:index]];
    NSString *strNotificationID =dic[@"id"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj putUserReadNotificationAction:strNotificationID];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //reset list
        [COMMON removeProgressLoading];
        if (response[@"success"]) {
            [dic setObject:@1 forKey:@"read"];
            if (index<arrData.count) {
                [arrData replaceObjectAtIndex:index withObject:dic];
                [self.tableControl reloadData];
                if ([CommonObj sharedInstance].nbUnreadNotification >0) {
                    [CommonObj sharedInstance].nbUnreadNotification -=1;
                }

                [self fnReadCache:dic withRead:1];
            }
        }
    };
}
#pragma mark - cache
-(void)saveCacheNotiWithData:(NSArray*)arr
{
    for (NSDictionary *dic in arr) {
        NSString *strID =[NSString stringWithFormat:@"%d", [dic[@"id"] intValue]];
        //cache database
        
        NSPredicate *ind=  [NSPredicate predicateWithFormat:@" (notiID == %d) AND (notiMyID == %d) ",[strID intValue],myID];
        [NotifiEntity MR_deleteAllMatchingPredicate:ind];
        NotifiEntity*nObj = nil;
        nObj = [NotifiEntity MR_createEntity];
        
        nObj.notiID = [NSString stringWithFormat:@"%@", dic[@"id"]];
        nObj.notiObjID = [NSString stringWithFormat:@"%@", dic[@"object_id"]];
        nObj.notiContent = [NSString stringWithFormat:@"%@", dic[@"content"]];
        nObj.notiUpdate = [NSString stringWithFormat:@"%@", dic[@"updated"]];
        nObj.notiType=[NSString stringWithFormat:@"%@", dic[@"type"]];
        nObj.notiMyID =[NSString stringWithFormat:@"%d", myID];
        nObj.notiRead =[NSNumber numberWithInt:[dic[@"read"] intValue]];
        
        nObj.notiSenderID =[NSString stringWithFormat:@"%@", dic[@"sender"][@"id"]];
        
    }
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    [localContext MR_saveToPersistentStoreAndWait];
    
}

-(void)loadCache
{
    NSArray *arrN=  [NotifiEntity MR_findAllSortedBy:@"notiUpdate" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"notiMyID =%d",myID]];
    NSMutableArray *arrCache = [NSMutableArray new];
    
    
    if (arrN.count>0) {
        for (NotifiEntity *obj in arrN) {
            
            NSDictionary *sender = [NSDictionary new];
            ASLog(@"obj.notiMyID %@",obj.notiMyID);
            ASLog(@"obj.notiID %@",obj.notiID);

            [arrCache addObject:@{
                                  @"id":[NSString stringWithFormat:@"%d",[obj.notiID intValue]],
                                  @"object_id":[NSString stringWithFormat:@"%d",[obj.notiObjID intValue]],
                                  @"content":obj.notiContent,
                                  @"updated":obj.notiUpdate,
                                  @"type":obj.notiType,
                                  @"read":[NSString stringWithFormat:@"%d",[obj.notiRead intValue]],
                                  @"sender":sender
                                  }
             ];
        }
        arrData = [arrCache mutableCopy];
    }
    
    [self.tableControl reloadData];
}

-(void)fnReadCache:(NSDictionary*)dic withRead:(int)read
{
    NSString *strID =[NSString stringWithFormat:@"%d", [dic[@"id"] intValue]];
    //cache database
    NSPredicate *ind=  [NSPredicate predicateWithFormat:@"notiID=%d && notiMyID =%d",[strID intValue],myID];
    
    [NotifiEntity MR_deleteAllMatchingPredicate:ind];
    NotifiEntity*nObj = nil;
    nObj = [NotifiEntity MR_createEntity];
    nObj.notiID = [NSString stringWithFormat:@"%@", dic[@"id"]];
    nObj.notiObjID = [NSString stringWithFormat:@"%@", dic[@"object_id"]];
    nObj.notiContent = [NSString stringWithFormat:@"%@", dic[@"content"]];
    nObj.notiUpdate = [NSString stringWithFormat:@"%@", dic[@"updated"]];
    nObj.notiType=[NSString stringWithFormat:@"%@", dic[@"type"]];
    nObj.notiMyID =[NSString stringWithFormat:@"%d", myID];
    nObj.notiRead =[NSNumber numberWithInt:read];
    
    nObj.notiSenderID =[NSString stringWithFormat:@"%@", dic[@"sender"][@"id"]];

}
@end
