//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCellFooterDiscussion.h"

@implementation TypeCellFooterDiscussion
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
    [self fnSetColor: UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR)];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
}
@end
