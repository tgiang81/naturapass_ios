//
//  SignalerTerminez.m
//  Naturapass
//
//  Created by JoJo on 9/11/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SignalerTerminez.h"
#import "ChassesCreateHeaderCell.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateMapCell.h"
#import "ChassesCreateTextFieldCell.h"
#import "SignalerColorTextFieldCell.h"
#import "SignalerMediaCell.h"
#import "SignalerThreeMediaCell.h"
#import "SignalerPartagerCell.h"
#import "Media_SheetView.h"
#import <AVFoundation/AVFoundation.h>
#import "AlertVC.h"
#import "NetworkCheckSignal.h"
#import "AlertSuggestView.h"
#import "AlertSuggestPersoneView.h"
#import "DatabaseManager.h"
#import "Signaler_Choix_Partage.h"
#import "LiveHuntOBJ.h"
#import "MapGlobalVC.h"
#import "Signaler_Favoris_System.h"
#import "ChassesCreateTextViewCell.h"
#import "ChassesCreateSwitchCell.h"
#import "Signaler_ChoixSpecNiv1.h"
#import "Signaler_ChoixSpecNiv5.h"
#import "SignalerPublierCell.h"
typedef enum
{
    FIELD_GEO_TITLE,
    FIELD_GEO_MAP,
    FIELD_GEO_SEARCH,
    FIELD_COMMENT_TITLE,
    FIELD_COMMENT,
    FIELD_COMMENT_TF,
    FIELD_MEDIA,
    FIELD_THREEMEDIA,
    FILED_PARTAGE,
    FIELD_LEGEND,
    FIELD_COLOR,
    FIELD_PRECISER,
    FIELD_FORMULAIRE,
    FIELD_PUBLIER,
    FIELD_FAVORIS
}SIGNALER_FIELD_TYPE;

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierTF = @"identifierTF";
static NSString *identifierColor = @"identifierColor";
static NSString *identifierMedia = @"identifierMedia";
static NSString *identifierThreeMedia = @"identifierThreeMedia";
static NSString *identifierPar = @"identifierPar";
static NSString *identifierPublier = @"identifierPublier";

static NSString *identifierMap = @"identifierMap";
static NSString *identifierTV = @"identifierTV";
static NSString *identifierSW = @"identifierSW";
@interface SignalerTerminez ()<UITextFieldDelegate, GMSMapViewDelegate , WWCalendarTimeSelectorProtocol>
{
    NSString *strLatitude, *strLongitude, *strAltitude;
    AlertSuggestView *suggestView;
    AlertSuggestPersoneView *suggestViewPersonne;
    NSInteger typeSelected;
    CLLocation *my_Placemark;
    NSMutableArray              *mesArr;
    
    NSMutableArray              *sharingGroupsArray;
    NSMutableArray              *sharingHuntsArray;
    NSMutableArray              *arrReceivers;
    NSMutableArray              *arrPersonne;
    int  MDshare;
    NSString *txtPartage;
    IBOutlet UIButton *btnSuivatnt;
    IBOutlet UIButton *btnAnnuler;
    NSDictionary *childCategory;
//    BOOL editHaveGeo;
    BOOL hideFieldComment;
}
@property(assign) MEDIATYPE mediaType;
@property (nonatomic, strong) ChassesCreateBaseCell *cellMap;
@property (strong, nonatomic)  NSString *strAddress;
@property (strong, nonatomic)  NSString *strTitle;
@property (strong, nonatomic)  NSString *strColor;


@end

@implementation SignalerTerminez

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MainNavigationBaseView *subview =  [self getSubMainView];

  //  [subview.myDesc setText:@"Terminez le signalement"];
    [subview.myDesc setText:@""];
    self.viewBottom.backgroundColor = self.colorBackGround;
    self.viewBottom1.backgroundColor = self.colorBackGround;

    [btnSuivatnt.layer setMasksToBounds:YES];
    btnSuivatnt.layer.cornerRadius= 25.0;
    btnSuivatnt.backgroundColor = self.colorNavigation;
    [btnSuivatnt setTitle:@"PUBLIER!" forState:UIControlStateNormal];
    [btnAnnuler.layer setMasksToBounds:YES];
    btnAnnuler.layer.cornerRadius= 20.0;
    btnAnnuler.backgroundColor = self.colorAnnuler;

    [self.btnTERMINER.layer setMasksToBounds:YES];
    self.btnTERMINER.layer.cornerRadius= 25.0;
    self.btnTERMINER.backgroundColor = self.colorNavigation;

    //reset Favorite saving status
    [PublicationOBJ sharedInstance].isPostFavo = NO;

   // self.vContainer.backgroundColor = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);

    
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateHeaderCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextFieldCell" bundle:nil] forCellReuseIdentifier:identifierTF];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerColorTextFieldCell" bundle:nil] forCellReuseIdentifier:identifierColor];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateMapCell" bundle:nil] forCellReuseIdentifier:identifierMap];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerMediaCell" bundle:nil] forCellReuseIdentifier:identifierMedia];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerThreeMediaCell" bundle:nil] forCellReuseIdentifier:identifierThreeMedia];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerPartagerCell" bundle:nil] forCellReuseIdentifier:identifierPar];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerPublierCell" bundle:nil] forCellReuseIdentifier:identifierPublier];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextViewCell" bundle:nil] forCellReuseIdentifier:identifierTV];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateSwitchCell" bundle:nil] forCellReuseIdentifier:identifierSW];

    self.tableControl.estimatedRowHeight = 60;
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self InitializeKeyboardToolBar];

    self.arrData = [NSMutableArray new];
    sharingGroupsArray= [NSMutableArray new];
    sharingHuntsArray= [NSMutableArray new];
    
    //Filter
    mesArr =        [NSMutableArray new];
    arrReceivers=  [NSMutableArray new];
    arrPersonne = [NSMutableArray new];
    
    NSDictionary *dicGeoTitle  = nil;
    NSDictionary *dicGeoMap  = @{@"type":@(FIELD_GEO_MAP)};
//    NSDictionary *dicGeoSearch  = @{
//                                    @"type":@(FIELD_GEO_SEARCH),
//                                    @"placehold":@"Utiliserma position...",
//                                    @"icontf":@"icon_chasses_address"};
    NSDictionary *dicCommentTitle  = @{@"name":@"CONTENU DE LA PUBLICATION",
                                   @"icon":@"icon_pub_pen_green",
                                   @"type":@(FIELD_COMMENT_TITLE)};
    NSDictionary *dicComment  = nil;
    NSDictionary *dicColorLegend  = nil;
    NSDictionary *dicPublier  = nil;
    NSDictionary *dicMedia  =   @{@"nameleft":@"PHOTO...",
                                   @"iconleft":@"icon_sign_photo",
                                  @"nameright":@"VIDÉO...",
                                  @"iconright":@"icon_sign_video",
                                   @"type":@(FIELD_MEDIA)};
    NSDictionary *dicLegend = nil;
    NSDictionary *dicPar  = @{@"name":@"PARTAGER...",
                              @"placehold":@"Chasse en cours",
                              @"type":@(FILED_PARTAGE)};
    NSDictionary *dicPreciser  = @{@"name":@"PRÉCISER",
                                   @"icon":@"icon_pub_preciser",
                                   @"type":@(FIELD_PRECISER)};
    NSDictionary *dicFavoris  = @{@"name":@"UTILISER EN TANT QUE FAVORIS...",
                                   @"icon":@"pub_ic_star",
                                   @"type":@(FIELD_FAVORIS)};
    /*
    [self.arrData addObject:dicCommentTitle];
    [self.arrData addObject:dicComment];
    if (![PublicationOBJ sharedInstance].isEditer) {
        [self.arrData addObject:dicMedia];
    }
    [self.arrData addObject:dicPar];
    [self.arrData addObject:dicGeoTitle];
    [self.arrData addObject:dicGeoMap];
    [self.arrData addObject:dicLegend];
    if ([PublicationOBJ sharedInstance].isEditer || self.expectTarget != ISLIVE) {
        [self.arrData addObject:dicPreciser];
    }
    if (![PublicationOBJ sharedInstance].isEditer && self.expectTarget != ISLIVE) {
        [self.arrData addObject:dicFavoris];
    }
    */
    suggestView = [[AlertSuggestView alloc] initSuggestView];
    suggestViewPersonne = [[AlertSuggestPersoneView alloc] initSuggestView];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    strLatitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.latitude];
    
    strLongitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.longitude];
    [PublicationOBJ sharedInstance].latitude =strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;

    sharingHuntsArray = [NSMutableArray new];
    if([LiveHuntOBJ sharedInstance].dicLiveHunt)
    {    [sharingHuntsArray addObject:@{@"categoryName":  [LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"] ,
                                        @"huntID":  [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"],
                                        @"isSelected": [NSNumber numberWithBool:YES]
                                        }];
        
        NSString * strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];

        [sharingHuntsArray writeToFile:strPath atomically:YES];

    }
    if(self.isFromFavo)
    {
        [self fnSetValueIsEditer];
    }
    hideFieldComment = FALSE;
    if([PublicationOBJ sharedInstance].isEditer)
    {
        [subview.myTitle setText:@"MODIFIER UNE PUBLICATION..."];
        self.viewBottom.hidden = YES;
        self.viewBottom1.hidden = NO;
        [btnSuivatnt setTitle:@"APPLIQUER !" forState:UIControlStateNormal];
        
        dicGeoTitle  = @{@"name":@"GÉOLOCALISATION",
                         @"icon":@"icon_sign_geo",
                         @"switch":@(TRUE),
                         @"type":@(FIELD_GEO_TITLE)};
        dicComment  = @{@"type":@(FIELD_COMMENT),
                        @"placehold":@"Ajouter un commentaire (facultatif)"};
        dicLegend  =   @{@"name":@"LÉGENDE COULEUR...",
                         @"icon":@"icon_sign_legend",
                         @"type":@(FIELD_LEGEND)};

        [self.arrData addObject:dicCommentTitle];
        [self.arrData addObject:dicComment];
        [self.arrData addObject:dicPar];
        [self.arrData addObject:dicGeoTitle];
        [self.arrData addObject:dicGeoMap];
        [self.arrData addObject:dicLegend];
        [self.arrData addObject:dicPreciser];

        [self fnFillDataEditPublication];

    }else{
        switch (self.expectTarget) {
            case ISLIVE:
            {
                [subview.myTitle setText:@"SIGNALER QUOI ?"];
                [subview.myDesc setText:@"Terminez le signalement"];
                self.viewBottom.hidden = NO;
                self.viewBottom1.hidden = YES;
                
                dicGeoTitle  = @{@"name":@"MODIFIER LA GÉOLOCALISATION",
                                 @"icon":@"icon_sign_geo",
                                 @"switch":@(FALSE),
                                 @"type":@(FIELD_GEO_TITLE)};
                dicComment  = @{@"type":@(FIELD_COMMENT_TF),
                                @"placehold":@"Ajouter un commentaire (facultatif)"};
                dicLegend  =   @{@"nameleft":@"CRÉER UN FAVORI...",
                                 @"iconleft":@"pub_ic_star",
                                 @"nameright":@"LÉGENDE COULEUR...",
                                 @"iconright":@"icon_sign_legend",
                                 @"type":@(FIELD_LEGEND)};

                [self.arrData addObject:dicGeoTitle];
                [self.arrData addObject:dicGeoMap];
                [self.arrData addObject:dicComment];
                [self.arrData addObject:dicMedia];
                [self.arrData addObject:dicPar];
                [self.arrData addObject:dicLegend];
                [PublicationOBJ sharedInstance].enableGeolocation = YES;
            }
                break;
            case ISCARTE:
            {
                [subview.myTitle setText:@"PUBLIER QUOI ?"];
                [subview.myDesc setText:@"Terminez la publication..."];
                self.viewBottom.hidden = YES;
                self.viewBottom1.hidden = NO;
                
                dicGeoTitle  = @{@"name":@"MODIFIER LA GÉOLOCALISATION",
                                 @"icon":@"icon_sign_geo",
                                 @"switch":@(FALSE),
                                 @"type":@(FIELD_GEO_TITLE)};
                dicPublier  =   @{@"nameleft":@"UTILISER EN TANT\nQUE FAVORIS...",
                                 @"iconleft":@"pub_ic_star",
                                 @"nameright":@"PUBLIER\nSUR LE MUR",
                                 @"iconright":@"icon_sign_legend",
                                 @"type":@(FIELD_PUBLIER)};
                dicMedia  =   @{@"nameleft":@"PHOTO",
                                @"iconleft":@"icon_sign_photo",
                                @"nameright":@"VIDÉO",
                                @"iconright":@"icon_sign_video",
                                @"namethree":@"COMMENTAIRE",
                                @"iconthree":@"ic_cat_commrent",
                                @"type":@(FIELD_THREEMEDIA)};
                dicColorLegend  = @{@"type":@(FIELD_COLOR),
                                @"placehold":@"Ajouter une légende (facultatif)"};
                dicComment  = @{@"type":@(FIELD_COMMENT_TF),
                                @"placehold":@"Ajouter un commentaire (facultatif)"};

                [self.arrData addObject:dicGeoTitle];
                [self.arrData addObject:dicGeoMap];
                [self.arrData addObject:dicColorLegend];
                [self.arrData addObject:dicMedia];
                [self.arrData addObject:dicComment];
                [self.arrData addObject:dicPar];
                [self.arrData addObject:dicPublier];
                [PublicationOBJ sharedInstance].enableGeolocation = YES;
                [PublicationOBJ sharedInstance].publicationcolor = @"12";
                [self tmpGetColor];
                hideFieldComment = TRUE;
            }
                break;
            default:
            {
                [subview.myTitle setText:@"POSTER UNE PUBLICATION..."];
                self.viewBottom.hidden = YES;
                self.viewBottom1.hidden = NO;
                
                dicGeoTitle  = @{@"name":@"GÉOLOCALISATION",
                                 @"icon":@"icon_sign_geo",
                                 @"switch":@(TRUE),
                                 @"type":@(FIELD_GEO_TITLE)};
                dicComment  = @{@"type":@(FIELD_COMMENT),
                                @"placehold":@"Ajouter un commentaire (facultatif)"};
                dicLegend  =   @{@"name":@"LÉGENDE COULEUR...",
                                 @"icon":@"icon_sign_legend",
                                 @"type":@(FIELD_LEGEND)};

                [self.arrData addObject:dicCommentTitle];
                [self.arrData addObject:dicComment];
                [self.arrData addObject:dicMedia];
                [self.arrData addObject:dicPar];
                [self.arrData addObject:dicGeoTitle];
                [self.arrData addObject:dicGeoMap];
                [self.arrData addObject:dicLegend];
                [self.arrData addObject:dicPreciser];
                [self.arrData addObject:dicFavoris];

                [PublicationOBJ sharedInstance].enableGeolocation = NO;
            }
                break;
        }
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //Auto...type...
    if (self.autoShow == TYPE_PHOTO) {
        [self onClickPhoto:nil];
        
    }else if (self.autoShow == TYPE_VIDEO) {
        [self onClickVideo:nil];
    }
    if (_dicShareExtention != nil) {
        [self imageShareExtentionWithInfo:_dicShareExtention];
    }
    
    NSDictionary *dicPatarge = [[PublicationOBJ sharedInstance].dicPatarge copy];
    if(dicPatarge)
    {
        MDshare =[dicPatarge[@"iSharing"] intValue];
        sharingGroupsArray = [dicPatarge[@"sharingGroupsArray"] mutableCopy];
        sharingHuntsArray = [dicPatarge[@"sharingHuntsArray"] mutableCopy];
        arrReceivers = [dicPatarge[@"receivers"] mutableCopy];
        arrPersonne = [dicPatarge[@"personne"] mutableCopy];

    }
    txtPartage = @"Autres...";
    //set check list hunt
//    [PublicationOBJ sharedInstance].dicPatarge
/*
 
        iSharing = 0;
        personne =     (
        );
        receivers =     (
        );
        sharingGroupsArray =     (

        sharingHuntsArray =     (

        sharingUser = "";
*/
    
    for (int i = 0; i<sharingHuntsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
        
        if ([[PublicationOBJ sharedInstance].huntID  intValue] == [dic[@"huntID"]  intValue] && [dic[@"isSelected"] boolValue])
        {
            txtPartage = @"Chasse en cours";
            [self.tableControl reloadData];
            break;
            
        }
    }
    //Change condition if any
    //•If he lets default sharing = agenda of the live where he comes from then on the button “PARTAGER AVEC” it’s still written “Chasse en cours” (means “share with : live agenda”)
    //•If he changes sthg on the sharing screen then the text inside button will change to : “Autres…”

    BOOL bOtherAction = NO;
    
    int iTestCount = 0;

    for (int i = 0; i<sharingHuntsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];

        if ([dic[@"isSelected"] boolValue] == TRUE ) {
            iTestCount += 1;
        }
    }
    
    //user interactive with Hunt
    if (iTestCount == 0 || iTestCount > 1) {
        bOtherAction = YES;
    }
    
    for (int i = 0; i<sharingGroupsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
        
        if ([dic[@"isSelected"] boolValue] == TRUE ) {
            iTestCount += 1;
        }
    }
    
    if (arrPersonne.count > 0) {
        iTestCount += 1;
    }

    if (MDshare > 0) {
        iTestCount += 1;
    }
    
    if (iTestCount > 1) {
        bOtherAction = YES;
    }
    
    if (bOtherAction) {
        txtPartage = @"Autres...";
    }
    
    [self.tableControl reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fnFillDataEditPublication
{
    /*
     Chú ý: + Nếu publication đã có geo thì không cho xóa, mà chỉ cho thay đổi vị trí (không cho switch off)
            + Nếu publication chưa có geo cho phép on/off switch
            + UI mới chưa có màn hình update giá trị cho observation chỉ cho phép chọn observation mới
     */
    NSDictionary *dicEditer =  [[PublicationOBJ sharedInstance].dicEditer copy];
    NSString *contentValue = [dicEditer[@"content"] emo_emojiString];
    if(contentValue)
    {
        self.strTitle = contentValue;
    }
    if ([dicEditer[@"geolocation"] isKindOfClass:[NSDictionary class]]) {
        [PublicationOBJ sharedInstance].latitude =dicEditer[@"geolocation"][@"latitude"];
        [PublicationOBJ sharedInstance].longtitude =dicEditer[@"geolocation"][@"longitude"];
        self.strAddress =dicEditer[@"geolocation"][@"address"];
        [PublicationOBJ sharedInstance].address = self.strAddress;
        [PublicationOBJ sharedInstance].location = self.strAddress;
        [PublicationOBJ sharedInstance].enableGeolocation = YES;
//        editHaveGeo = TRUE;
    }
    if (dicEditer[@"legend"]) {
        [PublicationOBJ sharedInstance].legend = dicEditer[@"legend"];
    }
    //color
    if ([dicEditer[@"color"] isKindOfClass:[NSDictionary class]]) {
        [PublicationOBJ sharedInstance].publicationcolor = dicEditer[@"color"][@"id"];

    }
    //save sharing option: Amis/Natiz/Other
    if ([dicEditer[@"sharing"] isKindOfClass:[NSDictionary class]]) {
        MDshare =[dicEditer[@"sharing"][@"share"] intValue];

    }
    NSArray *arrGroup = dicEditer[@"groups"];
    sharingGroupsArray = [NSMutableArray new];
    if (arrGroup.count > 0) {
        for (NSDictionary *kDic in arrGroup) {
            [sharingGroupsArray addObject: @{@"categoryName":  kDic[@"name"],
                                             @"groupID":  kDic[@"id"],
                                             @"isSelected": [NSNumber numberWithBool:YES]
                                             }];
        }
    }
    NSArray *arrHunts = dicEditer[@"hunts"];
    sharingHuntsArray = [NSMutableArray new];
    if (arrHunts) {
        for (NSDictionary *kDic in arrHunts) {
            [sharingHuntsArray addObject: @{@"categoryName":  kDic[@"name"],
                                             @"huntID":  kDic[@"id"],
                                             @"isSelected": [NSNumber numberWithBool:YES]
                                             }];
        }
    }
    NSArray *arrOb = dicEditer[@"observations"];
    if (arrOb.count>0) {
       NSArray *arrReceiversValue = [dicEditer[@"observations"][0][@"sharing_receiver"] copy];
        if (arrReceiversValue) {
            arrReceivers = [arrReceiversValue mutableCopy];
        }
        [self fillDataAttachment];
    }
    NSArray *arrUser = dicEditer[@"shareusers"];
    arrPersonne = [NSMutableArray new];
    if (arrUser.count > 0) {
        for (int i = 0; i < arrUser.count; i++) {
            NSMutableDictionary *dicUser = [arrUser[i] mutableCopy];
            [dicUser setObject:@(TRUE) forKey:@"status"];
            [arrPersonne addObject:dicUser];
        }
    }
}
-(void)fillDataAttachment
{
    NSDictionary *myCard = nil;
    NSArray *arrOb = [PublicationOBJ sharedInstance].dicEditer[@"observations"];
    NSMutableDictionary  *dicObservation = [NSMutableDictionary new];
    if (arrOb.count>0) {
        if (arrOb[0][@"category_id"]) {
            [dicObservation setObject:arrOb[0][@"category_id"] forKey:@"category"];
        }
        if (arrOb[0][@"specific"]) {
            [dicObservation setObject:arrOb[0][@"specific"] forKey:@"specific"];
        }
        if (arrOb[0][@"animal"]) {
            [dicObservation setObject:arrOb[0][@"animal"] forKey:@"animal"];
        }
        //get data with categrogy_id
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        for (NSDictionary*nDic in [PublicationOBJ sharedInstance].treeCategory) {
            [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
            if (childCategory != nil) {
                break;
            }
        }
        
        if (childCategory == nil) {
            // neu duyet trong tree default không có thì sẽ duyệt trong các tree còn lại
            BOOL isFinish = false;
            for (NSString *strKey in treeDic.allKeys) {
                if (![strKey isEqualToString:@"default"]) {
                    NSArray *arrTreeCategory = treeDic[strKey];
                    for (NSDictionary*nDic in arrTreeCategory) {
                        [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
                        if (childCategory != nil) {
                            isFinish =TRUE;
                            break;
                        }
                    }
                }
                if (isFinish == TRUE) {
                    break;
                }
            }
        }

        NSArray *listCards = [[PublicationOBJ sharedInstance] getCache_Cards];
        for (NSDictionary*dicCard in listCards) {
            if ([childCategory[@"card"][@"id"] intValue] == [dicCard[@"id"] intValue]) {
                //Edit...Card UP - DOWN
                myCard = dicCard;
                break;
            }
        }
        if (myCard) {
            NSMutableArray *arrTmp = [NSMutableArray new];

                NSMutableArray *arrLabels = [myCard[@"labels"] mutableCopy];
                NSArray *modifiAttachment = arrOb[0][@"attachments"];
                if (modifiAttachment > 0) {
                    for (NSDictionary *itemAttachment in modifiAttachment) {
                        for (NSDictionary *dicLabel in arrLabels) {
                            if ([itemAttachment[@"label"] isEqualToString: dicLabel[@"name"]] ) {
                                NSArray *arrValues = itemAttachment[@"values"];
                                if (arrValues.count > 0) {
                                    for (NSString *strTmp in arrValues) {
                                        [arrTmp addObject:@{
                                                            @"label":dicLabel[@"id"],
                                                            @"value":strTmp
                                                            }];
                                        }
                                    }
                                else
                                {
                                    [arrTmp addObject:@{
                                                        @"label":dicLabel[@"id"],
                                                        @"value":itemAttachment[@"value"]?itemAttachment[@"value"]:@""
                                                        }];
                                }
                            break;
                            }
                        }
                    }
                }
            if (arrTmp.count > 0) {
                [dicObservation setObject:arrTmp forKey:@"attachments"];
                NSDictionary *dicFormulaire  = @{@"name":@"MODIFIER LE FORMULAIRE",
                                                 @"icon":@"newpub_icon_edit",
                                                 @"type":@(FIELD_FORMULAIRE)};
                [self.arrData addObject:dicFormulaire];
            }
        }
        [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
    }
}
-(void) getDataTree:(NSDictionary*) trDic withCategoryID:(int)categoryID
{
    
    if ([trDic[@"id"] intValue] == categoryID ) {
        childCategory = trDic;
        return;
    }else{
        
        for (NSDictionary*nDic in trDic[@"children"]) {
            [self getDataTree:nDic withCategoryID:categoryID];
        }
    }
}
-(void)fnSetValueIsEditer
{
    NSString *strId =@"id";
    // set value ishare
    MDshare =[PublicationOBJ sharedInstance].iShare;

    //set check list group
    for (NSDictionary *dicGroup in [PublicationOBJ sharedInstance].arrGroup) {
        for (int i = 0; i<sharingGroupsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
            
            if ([dicGroup[strId]  intValue] == [dic[@"groupID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingGroupsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //    if (self.isEditFavo) {
    //        strId =@"huntID";
    //    }
    //set check list hunt
    for (NSDictionary *dicHunt in [PublicationOBJ sharedInstance].arrHunt) {
        for (int i = 0; i<sharingHuntsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
            
            if ([dicHunt[strId]  intValue] == [dic[@"huntID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingHuntsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //set check list hunt
    [arrPersonne removeAllObjects];
    for (NSDictionary *dicUser in [PublicationOBJ sharedInstance].arrsharingUsers) {
        NSMutableDictionary *dic=[dicUser mutableCopy];
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        [arrPersonne addObject:dic];
    }
    if ([PublicationOBJ sharedInstance].arrReceivers) {
        arrReceivers = [NSMutableArray new];
        [arrReceivers addObjectsFromArray:[PublicationOBJ sharedInstance].arrReceivers];
    }
    [self.tableControl reloadData];
    
    
}
#pragma mark - TABLEVIEW
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[ChassesCreateBaseCell class]])
    {
        NSDictionary *dicData = self.arrData[indexPath.row];
        ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)cellTmp;
        cell.btnAddress.hidden = TRUE;
        switch ([dicData[@"type"] intValue]) {
            case FIELD_COMMENT_TITLE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = YES;
                cell.btnSelected.hidden = YES;

            }
                break;
            case FIELD_GEO_TITLE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.swFilter.on = [PublicationOBJ sharedInstance].enableGeolocation;
                [cell.swFilter setOnTintColor:self.colorNavigation];
                cell.swFilter.backgroundColor = self.colorAnnuler;
                cell.swFilter.layer.masksToBounds = YES;
                cell.swFilter.layer.cornerRadius = 16.0;
                [cell.swFilter addTarget:self action:@selector(doSwitch:) forControlEvents:UIControlEventTouchUpInside];
                cell.swFilter.hidden = ![dicData[@"switch"] boolValue];
            }
                break;
            case FIELD_PRECISER:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = NO;
                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                cell.btnSelected.hidden = NO;
                
                //reset target

                [cell.btnSelected removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
                [cell.btnSelected addTarget:self action:@selector(preciserAction:) forControlEvents:UIControlEventTouchUpInside];

            }
                break;
            case FIELD_FORMULAIRE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = NO;
                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                cell.btnSelected.hidden = NO;
                
                //reset target
                
                [cell.btnSelected removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
                [cell.btnSelected addTarget:self action:@selector(formulaireAction:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
            case FIELD_FAVORIS:
            {
                cell.lbTitle.text = dicData[@"name"];
                if ([PublicationOBJ sharedInstance].isPostFavo) {
                    cell.imgIcon.image = [[UIImage imageNamed:@"pub_ic_star_created"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                }
                else
                {
                    cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                }
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = NO;
                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                cell.btnSelected.hidden = NO;
                //reset target
                
                [cell.btnSelected removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
                [cell.btnSelected addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];

            }
                break;
            case FIELD_GEO_MAP:
            {
                cell.mapView_.delegate = self;
                cell.ratioMap.active = NO;
                [cell fnSetLocation:[PublicationOBJ sharedInstance].latitude withLongitude:[PublicationOBJ sharedInstance].longtitude];

                [cell.btnCurrentLocation setImage:nil forState:UIControlStateNormal];
                cell.btnCurrentLocation.backgroundColor = self.colorNavigation;
                cell.imgCatCenter.image = [UIImage imageNamed:@"ic_cat_center"];

                [cell.btnCloseMap setImage:[[UIImage imageNamed:@"ic_cat_zoom"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                cell.btnCloseMap.tintColor = self.colorNavigation;
                cell.btnCloseMap.backgroundColor = [UIColor whiteColor];

                [cell.btnCloseMap addTarget:self action:@selector(extendMap:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnCurrentLocation.hidden= NO;
            }
                break;
            case FIELD_GEO_SEARCH:
            {
                    cell.imageLeftTF.image = [[UIImage imageNamed:dicData[@"icontf"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imageLeftTF.tintColor = self.colorNavigation;

                    cell.tfInput.placeholder = dicData[@"placehold"];
                    self.cellMap = cell;
                    cell.tfInput.delegate = self;
                    [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                    cell.tfInput.text = self.strAddress;
                    [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                    cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;

            }
                break;
            case FIELD_MEDIA:
            {
                cell.lbMediaLeft.text = dicData[@"nameleft"];
                cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"iconleft"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaLeft.tintColor = self.colorNavigation;
                
                cell.lbMediaRight.text = dicData[@"nameright"];
                cell.imgMediaRight.image = [[UIImage imageNamed:dicData[@"iconright"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaRight.tintColor = self.colorNavigation;
                [cell.btnMediaLeft removeTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnMediaRight removeTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnMediaLeft removeTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];

                [cell.btnMediaRight removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];

                [cell.btnMediaLeft addTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnMediaRight addTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case FIELD_THREEMEDIA:
            {
                cell.lbMediaLeft.text = dicData[@"nameleft"];
                cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"iconleft"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaLeft.tintColor = self.colorNavigation;
                
                cell.lbMediaRight.text = dicData[@"nameright"];
                cell.imgMediaRight.image = [[UIImage imageNamed:dicData[@"iconright"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaRight.tintColor = self.colorNavigation;
                [cell.btnMediaLeft removeTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnMediaRight removeTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnMediaLeft removeTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnMediaRight removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnMediaLeft addTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnMediaRight addTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.lbMediaThree.text = dicData[@"namethree"];
                cell.imgMediaThree.image = [[UIImage imageNamed:dicData[@"iconthree"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaThree.tintColor = self.colorNavigation;
                [cell.btnMediaThree addTarget:self action:@selector(onClickMediaThree:) forControlEvents:UIControlEventTouchUpInside];

            }
                break;
            case FIELD_PUBLIER:
            {
                if ([PublicationOBJ sharedInstance].isPostFavo) {
                    cell.imgMediaLeft.image = [[UIImage imageNamed:@"pub_ic_star_created"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                }else{
                    cell.imgMediaLeft.image = [[UIImage imageNamed:@"pub_ic_star_green"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                }
                cell.imgMediaLeft.tintColor = self.colorNavigation;

                cell.lbMediaLeft.text = dicData[@"nameleft"];

                [cell.btnMediaLeft addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                [cell.btnMediaRight addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];

                
                cell.lbMediaRight.text = dicData[@"nameright"];
                cell.swFilter.on = [PublicationOBJ sharedInstance].enableLandmark;
                [cell.swFilter setOnTintColor:self.colorNavigation];
                cell.swFilter.backgroundColor = self.colorAnnuler;
                cell.swFilter.layer.masksToBounds = YES;
                cell.swFilter.layer.cornerRadius = 16.0;
                [cell.swFilter addTarget:self action:@selector(landmarkSwitch:) forControlEvents:UIControlEventTouchUpInside];

            }
                break;
            case FIELD_LEGEND:
            {
                //has 2 sides
                if (dicData[@"nameleft"] != nil)
                {
                    cell.lbMediaLeft.text = dicData[@"nameleft"];
                    cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"iconleft"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgMediaLeft.tintColor = self.colorNavigation;
                    
                    cell.lbMediaRight.text = dicData[@"nameright"];
                    cell.imgMediaRight.image = [[UIImage imageNamed:dicData[@"iconright"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgMediaRight.tintColor = self.colorNavigation;
                    
                    [cell.btnMediaLeft removeTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnMediaRight removeTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnMediaLeft removeTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnMediaRight removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    //https://heolys.atlassian.net/browse/MA-402 hide add to favoris button
                    //But we can uncomment it to test
                    //                        cell.btnMediaLeft.hidden = TRUE;
                    //                        cell.lbMediaLeft.text = @"";
                    //                        cell.imgMediaLeft.image = nil;
                    //
                    if ([PublicationOBJ sharedInstance].isPostFavo) {
                        cell.imgMediaLeft.image = [[UIImage imageNamed:@"pub_ic_star_created"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    }else{
                        cell.imgMediaLeft.image = [[UIImage imageNamed:@"pub_ic_star_green"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    }
                    cell.imgMediaLeft.tintColor = self.colorNavigation;
                    cell.lbMediaLeft.text = @"UTILISER EN TANT QUE FAVORIS...";
                    
                    [cell.btnMediaLeft addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    [cell.btnMediaRight addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                else //just 1 field
                {
                    cell.lbTitle.text = dicData[@"name"];
                    cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgIcon.tintColor = self.colorNavigation;
                    cell.imgArrow.hidden = YES;
                    cell.btnSelected.hidden = NO;
                    
                    //reset target
                    [cell.btnSelected removeTarget:self action:@selector(preciserAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnSelected addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
                break;
                
            case FILED_PARTAGE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.lbPartager.text = txtPartage;//dicData[@"placehold"];
                [cell.btnPartager addTarget:self action:@selector(partageAction:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.imgDropdown.image = [[UIImage imageNamed:@"icon_sign_dropdown"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgDropdown.tintColor = self.colorNavigation;

            }
                break;
            case FIELD_COMMENT:
            {
                cell.textView.placeholder = dicData[@"placehold"];
                cell.textView.text = self.strTitle;
                [cell.textView setInputAccessoryView:self.keyboardToolbar];
                cell.textView.delegate = self;
                int remain = (int)cell.textView.text.length;
                [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
                
                // Check if the count is over the limit
                if(remain >= limitPublication - 10) {
                    // Change the color
                    [cell.numberRemain setTextColor:[UIColor redColor]];
                }
                else if(remain > limitPublication - 50 && remain < limitPublication - 10) {
                    // Change the color to yellow
                    [cell.numberRemain setTextColor:[UIColor orangeColor]];
                }
                else {
                    // Set normal color
                    [cell.numberRemain setTextColor:[UIColor darkGrayColor]];
                }
            }
                break;
            case FIELD_COLOR:
            {
                cell.imageLeftTF.image = [[UIImage imageNamed:dicData[@"icontf"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imageLeftTF.tintColor = self.colorNavigation;
                cell.tfInput.placeholder = dicData[@"placehold"];
                cell.tfInput.text = [PublicationOBJ sharedInstance].legend;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                [cell.btnChooseColor addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                cell.imgChooseColorBg.backgroundColor = [UIColor whiteColor];
                if (_strColor.length > 0) {
                    unsigned result = 0;
                    NSScanner *scanner = [NSScanner scannerWithString:_strColor];
                    
                    [scanner setScanLocation:0]; // bypass '#' character
                    [scanner scanHexInt:&result];
                    
                    [cell.imgChooseColor setBackgroundColor:UIColorFromRGB(result)];
                    if ([_strColor isEqualToString:@"FFFFFF"]) {
                        cell.imgChooseColorBg.backgroundColor = [UIColor darkGrayColor];
                    }
                }
                else
                {
                    [cell.imgChooseColor setBackgroundColor:nil];
                }
            }
                break;
            default:
            {
                cell.imageLeftTF.image = [[UIImage imageNamed:dicData[@"icontf"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imageLeftTF.tintColor = self.colorNavigation;
                cell.tfInput.placeholder = dicData[@"placehold"];
                cell.tfInput.text = self.strTitle;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
            }
                break;
        }
    }
    
}
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = self.arrData[indexPath.row];

    if ([PublicationOBJ sharedInstance].enableGeolocation == NO) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_LEGEND:
            case FIELD_GEO_MAP:
            case FIELD_GEO_SEARCH:
            {
                return 0;
            }
                break;
        }
    }
    if (hideFieldComment == TRUE) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_COMMENT:
            case FIELD_COMMENT_TF:
            {
                return 0;
            }
                break;
        }

    }
    return UITableViewAutomaticDimension;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChassesCreateBaseCell *cell = nil;
    NSDictionary *dicData = self.arrData[indexPath.row];
    switch ([dicData[@"type"] intValue]) {
        case FIELD_COMMENT_TITLE:
        case FIELD_PRECISER:
        case FIELD_FORMULAIRE:
        case FIELD_FAVORIS:
        {
            cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        }
            break;
        case FIELD_GEO_TITLE:
        {
            //if editor -> disable switch off geo.
            
//            if (![PublicationOBJ sharedInstance].isEditer) {
                cell = (ChassesCreateSwitchCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSW forIndexPath:indexPath];
//            }else{
//                cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
//            }

        }
            break;
        case FIELD_GEO_MAP:
        {
            cell = (ChassesCreateMapCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMap forIndexPath:indexPath];
            
        }
            break;
        case FIELD_GEO_SEARCH:
        {
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            
        }
            break;
        case FIELD_COLOR:
        {
            cell = (SignalerColorTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierColor forIndexPath:indexPath];
            
        }
            break;
        case FIELD_MEDIA:
        {
            cell = (SignalerMediaCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMedia forIndexPath:indexPath];
        }
            break;
        case FIELD_THREEMEDIA:
        {
            cell = (SignalerThreeMediaCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierThreeMedia forIndexPath:indexPath];
        }
            break;
        case FIELD_LEGEND:
        {
            //2 sides
            if (dicData[@"nameleft"] != nil) {
                cell = (SignalerMediaCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMedia forIndexPath:indexPath];
            }else{
            //1 side
                cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
            }
        }
            break;
        case FIELD_PUBLIER:
        {
            cell = (SignalerPublierCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPublier forIndexPath:indexPath];
            
        }
            break;
        case FILED_PARTAGE:
        {
            cell = (SignalerPartagerCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPar forIndexPath:indexPath];
            
        }
            break;
        case FIELD_COMMENT:
        {
            cell = (ChassesCreateTextViewCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTV forIndexPath:indexPath];
            
        }
            break;
        default:
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            break;
    }
    
    if (cell) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    cell.backgroundColor = self.colorCellBackGround;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.hidden = NO;
    if ([PublicationOBJ sharedInstance].enableGeolocation == NO) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_LEGEND:
            case FIELD_GEO_MAP:
            case FIELD_GEO_SEARCH:
            {
                cell.hidden = TRUE;
            }
                break;
        }
    }
    if (hideFieldComment == TRUE) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_COMMENT:
            case FIELD_COMMENT_TF:
            {
                cell.hidden = TRUE;
            }
                break;
        }
        
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}
-(IBAction)favoriteAction:(id)sender
{
    //favo
    /*
    NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
    NSMutableArray *arrGroup =[NSMutableArray new];
    NSMutableArray *arrHunt =[NSMutableArray new];
    
    if (sharingGroupsArray) {
        for (NSDictionary *dic in sharingGroupsArray) {
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                [arrGroup addObject :@{@"groupID":dic[@"groupID"],@"categoryName":dic[@"categoryName"],@"isSelected":dic[@"isSelected"]}];
            }
        }
        [dicFavo setValue:arrGroup forKey:@"sharingGroupsArray"];
    }
    
    if (sharingHuntsArray) {
        for (NSDictionary *dic in sharingHuntsArray) {
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                [arrHunt addObject :@{@"huntID":dic[@"huntID"],@"categoryName":dic[@"categoryName"],@"isSelected":dic[@"isSelected"]}];
            }
        }
        [dicFavo setValue:arrHunt forKey:@"sharingHuntsArray"];
    }
    if (arrPersonne.count) {
        [dicFavo setValue:arrPersonne forKey:@"sharingUsers"];
    }
    NSMutableArray *arrTmp = [NSMutableArray new];
    for (NSDictionary *dicPersonne in arrPersonne) {
        [arrTmp addObject:dicPersonne[@"id"]];
    }
    NSString *strUser = @"";
    if (arrTmp.count > 0) {
        strUser = [arrTmp componentsJoinedByString:@","];
    }
    [dicFavo setValue:[NSNumber numberWithInt:MDshare] forKey:@"iSharing"];
    [dicFavo setValue:@{@"share":[NSNumber numberWithInt:MDshare]} forKey:@"sharing"];
    [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
    
    AlertVC *vc = [[AlertVC alloc] initInputMessage ];
    [vc doBlock:^(NSInteger index, NSString* str) {
        if (str.length > 0) {
            [self addFavoriteWithName:str withDic:[PublicationOBJ sharedInstance].dicFavoris];

        }
    }];
    [vc showAlert];
    */
    AlertVC *vc = [[AlertVC alloc] initConfirmFavorisWithExpectTarget:self.expectTarget];
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        //valider
        if (index == 1) {
            
            //Plz note: we don't show Favorite here, just save to param to say that i will do favorite after that. We discussed in group
            /*
            thi co the luu lai thanh favorite
            
            tuyen [10:37 AM]
            đâu
            mathis mới bảo lại là
            khi click vào nút favorite thì hiển thị popup lên
            rồi xác nhận có tạo favorite hay ko
            chứ chưa di chuyển đến màn hình số 2
            khi nhấn tạo post
             thì nếu lúc trước chọn tạo favorite rồi
             thì mới show màn hình 2
             
             Tuyền đang hiểu đúng rồi đó
             khi chọn Valider thì nó chỉ confirm là nó sẽ tạo favorite cho cái pub này, chứ chưa chọn sẽ lưu cái gì.
             khi user click vào Terminer thì mới nhảy ra màn hình để chọn xem lưu cái gì cho favorite
             tại sao thế vì khi mình ấn Terminer thì mới chawcs chắn là mình đã input full infor cho cái pub đó, luc này mới tạo fav chuẩn đc chứ
             
             logic là thế
             */
            
            
            [PublicationOBJ sharedInstance].isPostFavo = YES;
            [self.tableControl reloadData];
            /*
            Signaler_Favoris_System *viewController1 = [[Signaler_Favoris_System alloc] initWithNibName:@"Signaler_Favoris_System" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
            */
        }else{
            [PublicationOBJ sharedInstance].isPostFavo = NO;
            [self.tableControl reloadData];
        }
    }];
    [vc showAlert];
}

-(void)addFavoriteWithName:(NSString*)strName withDic:(NSDictionary*)dicFavo
{
    if ([strName isEqualToString:@""] || strName == nil) {
        [KSToastView ks_showToast: [NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] duration:2.0f completion: ^{
        }];
        return;
    }
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    NSArray *arrHunt =nil;
    NSArray *arrGroup =nil;
    
    NSMutableDictionary *dicPost = [NSMutableDictionary new];
    if (strName) {
        [dicPost setValue:strName forKey:@"name"];
    }
    
    if (dicFavo[@"card_id"]) {
        [dicPost setValue:dicFavo[@"card_id"] forKey:@"card"];
    }
    
    if (dicFavo[@"legend"]) {
        [dicPost setValue:dicFavo[@"legend"] forKey:@"legend"];
    }
    
    if (dicFavo[@"iSharing"]) {
        [dicPost setValue:@{@"share":dicFavo[@"iSharing"]} forKey:@"sharing"];
    }
    
    if (dicFavo[@"sharingHuntsArray"]) {
        arrHunt =[self convertArray2ArrayID:dicFavo[@"sharingHuntsArray"] forGroup:NO];
        [dicPost setValue:arrHunt forKey:@"hunts"];
    }
    
    if (dicFavo[@"sharingGroupsArray"]) {
        arrGroup= [self convertArray2ArrayID:dicFavo[@"sharingGroupsArray"] forGroup:YES];
        [dicPost setValue:arrGroup forKey:@"groups"];
    }
    if (dicFavo[@"sharingUsers"]) {
        arrGroup= [self convertPersonneArray2ArrayID:dicFavo[@"sharingUsers"]];
        [dicPost setValue:arrGroup forKey:@"users"];
    }
    if (dicFavo[@"color"]) {
        [dicPost setValue:dicFavo[@"color"][@"id"] forKey:@"publicationcolor"];
    }
    
    //Default
    [dicPost setValue:@"0" forKey:@"specific"];
    
    if (dicFavo[@"observation"]) {
        if (dicFavo[@"observation"][@"attachments"]) {
            NSMutableDictionary *dicObservation = [dicFavo[@"observation"]  mutableCopy];
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{@"label": dic[@"label"],
                                       @"value": dic[@"value"]}];
            }
            [dicPost setValue:newAttach forKey:@"attachments"];
        }
        
        if (dicFavo[@"observation"][@"category"]) {
            [dicPost setValue:dicFavo[@"observation"][@"category"] forKey:@"category"];
        }
        if (dicFavo[@"observation"][@"card"]) {
            [dicPost setValue:dicFavo[@"observation"][@"card"] forKey:@"card"];
        }
        if (dicFavo[@"observation"][@"animal"]) {
            [dicPost setValue:dicFavo[@"observation"][@"animal"] forKey:@"animal"];
        }
        if (dicFavo[@"observation"][@"specific"]) {
            [dicPost setValue:dicFavo[@"observation"][@"specific"] forKey:@"specific"];
        }
    }
    
    [COMMON addLoading:self];
    
    [serviceObj fnPOST_PUBLICATION_FAVORITES:@{@"favorite":dicPost}];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        
        //reset
        [COMMON removeProgressLoading];
        
        if ([response[@"favorite"] isKindOfClass:[NSDictionary class]]) {
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];
                    
                }
            }];
            
            [KSToastView ks_showToast: @" Post favorite successful!!!" duration:2.0f completion: ^{
            }];
            
        }
        
        
    };
}
-(NSArray*) convertArray2ArrayID:(NSArray*)arr forGroup:(BOOL) isGroup
    {
        
        NSMutableArray *array = [NSMutableArray new];
        
        for (NSDictionary*dic in arr) {
            
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                if (isGroup) {
                    [array addObject:dic[@"groupID"]];
                    
                }else{
                    [array addObject:dic[@"huntID"]];
                }
            }
        }
        return array;
    }
-(NSArray*) convertPersonneArray2ArrayID:(NSArray*)arr
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
//        NSNumber *num = dic[@"status"];
//        if ( [num boolValue]) {
            [array addObject:dic[@"id"]];
//        }
    }
    return array;
}
-(IBAction)chooseColorAction:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initChooseColor ];
    [vc doBlockColor:^(NSInteger index, NSString* strTmp,NSString *strColor) {
        NSString *trimmedString = [strTmp stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        [PublicationOBJ sharedInstance].legend = trimmedString;
            [PublicationOBJ sharedInstance].publicationcolor = strColor;
        [self tmpGetColor];
        [self.tableControl reloadData];

    }];
    [vc fnSetexpectTarget:self.expectTarget withColor:self.colorNavigation];
    [vc showAlert];
}
-(void)tmpGetColor
{
   NSMutableArray *arrDataColor = [NSMutableArray new];
    
    NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
    
    NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
    
    if (dic) {
        [arrDataColor addObjectsFromArray: dic[@"colors"] ];
    }
    int indexSelect = -1;
    for (int i =0; i<arrDataColor.count; i++) {
        NSDictionary *dicData = arrDataColor[i];
        if ( [dicData[@"id"] intValue] == [[PublicationOBJ sharedInstance].publicationcolor intValue]) {
            indexSelect = i;
            _strColor = dicData[@"color"];
            break;
        }
    }

}
#pragma mark - SELECT MEDIA

- (IBAction)onClickPhoto:(id)sender {
    
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_PHOTO withParent:self target:self.expectTarget];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_PHOTO;
            
//            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
//            UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
            
            
            [PublicationOBJ sharedInstance].urlPhoto = data[@"image"];
//            if (data[@"Latitude"] != nil) {
//                [PublicationOBJ sharedInstance].latitude = [data[@"Latitude"] stringValue];
//                [PublicationOBJ sharedInstance].longtitude = [data[@"Longitude"] stringValue];
//            }else{
//                [PublicationOBJ sharedInstance].latitude = nil;
//                [PublicationOBJ sharedInstance].longtitude = nil;
//            }

        }
    }];
    
    if (self.autoShow == TYPE_PHOTO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    
    self.autoShow = TYPE_RESET;
    
}

- (IBAction)onClickVideo:(id)sender {
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_VIDEO withParent:self target:self.expectTarget];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_VIDEO;
            
            [PublicationOBJ sharedInstance].urlVideo = data[@"videourl"];
            
//            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: [NSURL fileURLWithPath: data[@"videourl"] ] options:nil];
//            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
//            generate1.appliesPreferredTrackTransform = YES;
//            NSError *err = NULL;
//            CMTime time = CMTimeMake(1, 2);
//            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
//            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
        }
    }];
    if (self.autoShow == TYPE_VIDEO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    self.autoShow = TYPE_RESET;
}
- (IBAction)onClickMediaThree:(id)sender {
    hideFieldComment = !hideFieldComment;
    [self.tableControl reloadData];
}
//MARK: - MAPVIEW DELEGATE

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = mapView.camera.target.latitude;
    float lng = mapView.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    [PublicationOBJ sharedInstance].latitude =strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;
    [PublicationOBJ sharedInstance].address = self.strAddress;
    [PublicationOBJ sharedInstance].location = self.strAddress;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(fnGetAddress) withObject:nil afterDelay:1];
}
-(void)fnGetAddress
{
    if(strLatitude.length == 0 && strLongitude.length == 0)
    {
        return;
    }
    __weak typeof(self) wself = self;
    if ([COMMON isReachable])
    {
        [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
            
            //i got the address
            [wself fnSetAddress:mAddress];
            
        } withData:@{@"latitude" : strLatitude,
                     @"longitude" : strLongitude}];
    }
}
-(void)fnSetAddress:(NSString*)mAddress
{
    //i got the address
    self.strAddress = mAddress;
    self.cellMap.tfInput.text = self.strAddress;
    [PublicationOBJ sharedInstance].address = self.strAddress;
    [PublicationOBJ sharedInstance].location = self.strAddress;
    //    [self.tableControl reloadData];
}
//MARK: - TextView Delegate
-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    UIView *parent = [a superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    int remain = (int) (limitPublication - (a.text.length + c.length));
    if (remain < 0) {
        NSString *strFullText = [a.text stringByAppendingString:c];
        NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitPublication)];
        a.text = subString;
        [cell.numberRemain setTextColor:[UIColor redColor]];
        [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
    }
    return remain >= 0;
}

- (void)textViewDidChange:(UITextView *)textView
{
    UIView *parent = [textView superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    int remain = (int)textView.text.length;
    [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
    
    // Check if the count is over the limit
    if(remain >= limitPublication - 10) {
        // Change the color
        [cell.numberRemain setTextColor:[UIColor redColor]];
    }
    else if(remain > limitPublication - 50 && remain < limitPublication - 10) {
        // Change the color to yellow
        [cell.numberRemain setTextColor:[UIColor orangeColor]];
    }
    else {
        // Set normal color
        [cell.numberRemain setTextColor:[UIColor darkGrayColor]];
    }
    self.strTitle = textView.text;
}
//MARK: - textfield delegate
- (BOOL)textField:(UITextField *)a shouldChangeCharactersInRange:(NSRange)b replacementString:(NSString *)c {
    UIView *parent = [a superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSDictionary *dicData = self.arrData[indexPath.row];
    int type = [dicData[@"type"] intValue];
    if(type == FIELD_COMMENT_TF)
    {
        int remain = (int) (limitPublication - (a.text.length + c.length));
        if (remain < 0) {
            NSString *strFullText = [a.text stringByAppendingString:c];
            NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitPublication)];
            a.text = subString;
            [cell.numberRemain setTextColor:[UIColor redColor]];
            [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
        }
        return remain >= 0;
    }
    else
    {
        return TRUE;
    }
    
}
-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    UIView *parent = [searchText superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;

    NSString *strsearchValues= searchText.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    searchText.text=strsearchValues;
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:
            
        {
            [self processLoungesSearchingURL:strsearchValues];
        }
            break;
        case FIELD_COMMENT_TF:
            
        {
            int remain = (int)searchText.text.length;
            [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
            
            // Check if the count is over the limit
            if(remain >= limitPublication - 10) {
                // Change the color
                [cell.numberRemain setTextColor:[UIColor redColor]];
            }
            else if(remain > limitPublication - 50 && remain < limitPublication - 10) {
                // Change the color to yellow
                [cell.numberRemain setTextColor:[UIColor orangeColor]];
            }
            else {
                // Set normal color
                [cell.numberRemain setTextColor:[UIColor darkGrayColor]];
            }
            self.strTitle = strsearchValues;
        }
            break;
        case FIELD_COLOR:
            
        {
            [PublicationOBJ sharedInstance].legend = strsearchValues;
        }
            break;
        default:
            {
                self.strTitle = strsearchValues;
            }
            break;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *strsearchValues= textField.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    textField.text=strsearchValues;
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:
            
        {
            [self processLoungesSearchingURL:strsearchValues];
        }
            break;
            
        default:
        {
            self.strTitle = strsearchValues;
        }
            break;
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSDictionary *dicData = self.arrData[indexPath.row];
    typeSelected = [dicData[@"type"] intValue];
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:

        {
            CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
            CGRect rectTf = cell.viewInput.frame;
            
            CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
            [self createSuggestViewWithSuperView:self.tableControl withRect:rectSubView withIndexPath:indexPath];
            [self.tableControl bringSubviewToFront:cell.viewInput];
            
        }
            break;

        default:
            break;
    }

    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [self removeSuggestView];
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        {
            self.strAddress = searchText;
            
            [suggestView processLoungesSearchingURL:searchText];
        }
            break;
        case FILED_PARTAGE:
        {
//            self.strPersonne = searchText;
            [suggestViewPersonne processLoungesSearchingURL:searchText];
        }
            break;
        default:
            break;
    }
}
-(void)createSuggestViewWithSuperView:(UIView*)view withRect:(CGRect)rect withIndexPath:(NSIndexPath*)indexPath
{
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        {
            [suggestView fnDataInput:self.strAddress];
            [suggestView doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
                [self resignKeyboard:nil];
                if (m_Placemark) {
                    my_Placemark = m_Placemark;
                    self.strAddress = addressString;
                    
                    float lat = m_Placemark.coordinate.latitude;
                    float lng = m_Placemark.coordinate.longitude;
                    
                    [PublicationOBJ sharedInstance].address = self.strAddress ;
                    [PublicationOBJ sharedInstance].latitude =[NSString stringWithFormat:@"%f",lat];
                    [PublicationOBJ sharedInstance].longtitude = [NSString stringWithFormat:@"%f",lng];
                    
                    [self.tableControl reloadData];
                }
                
            }];
            [suggestView showAlertWithSuperView:view withRect:rect];
        }
            break;
        case FILED_PARTAGE:
        {
//            [suggestViewPersonne fnDataInput:self.strPersonne withArrSelected:arrPersonne];
//            [suggestViewPersonne doBlock:^(NSArray *arrResult) {
//                [self resignKeyboard:nil];
//                [arrPersonne removeAllObjects];
//                if(arrResult)
//                {
//                    [arrPersonne addObjectsFromArray:arrResult];
//                }
//                [self.tableControl reloadData];
//            }];
//            [suggestViewPersonne showAlertWithSuperView:view withRect:rect];
            
        }
            break;
        default:
            break;
    }
    
}
-(void)updateSuggestView:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    
    [suggestView showAlertWithSuperView:self.tableControl withRect:rectSubView];
}
-(void)removeSuggestView
{
    if([suggestViewPersonne isDescendantOfView:self.tableControl]) {
        [suggestViewPersonne closeAction:nil];
    }
    if([suggestView isDescendantOfView:self.tableControl]) {
        [suggestView closeAction:nil];
    }
}
//MARK:- searBar
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    [self removeSuggestView];
    
}
- (void)keyboardWillHide:(NSNotification*)notification {
    [self removeSuggestView];
}
-(IBAction)annulerAction:(id)sender
{
    [self doRemoveObservation];
    //back to the Parent?
    //ggtt
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        //Code here.. e.g. print their titles to see the array setup;
        
        if ([controller isKindOfClass: [self.mParent class]]) {
            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }

    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onNext:(id)sender {
    [PublicationOBJ sharedInstance].publicationTxt = self.strTitle;
   [PublicationOBJ sharedInstance].mediaType = self.mediaType;

    //ggtt
    [PublicationOBJ sharedInstance].removeGeo = ![PublicationOBJ sharedInstance].enableGeolocation;

    if ([PublicationOBJ sharedInstance].isEditer) {
        [PublicationOBJ sharedInstance].iShare =MDshare;
        [PublicationOBJ sharedInstance].sharingGroupsArray = [[PublicationOBJ sharedInstance] convertArray:sharingGroupsArray forGroup:YES];
        [PublicationOBJ sharedInstance].sharingHuntsArray = [[PublicationOBJ sharedInstance] convertArray:sharingHuntsArray forGroup:NO];
        
        NSMutableArray *arrUser =[NSMutableArray new];
        
        if (arrPersonne) {
            for (NSDictionary *dic in arrPersonne) {
                NSNumber *num = dic[@"status"];
                if ( [num boolValue]) {
                    [arrUser addObject: [NSString stringWithFormat:@"%@", dic[@"id"]]];
                    
                }
            }
            [PublicationOBJ sharedInstance].arrsharingUsers = arrUser;
        }
        
        [[PublicationOBJ sharedInstance]  modifiPosterAll:self];
    }
    else{
        //
        if (self.isEditFavo) {
            //favo
            NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
            NSMutableArray *arrGroup =[NSMutableArray new];
            
            if (sharingGroupsArray) {
                for (NSDictionary *dic in sharingGroupsArray) {
                    NSNumber *num = dic[@"isSelected"];
                    if ( [num boolValue]) {
                        [arrGroup addObject:@{@"id":dic[@"groupID"],@"name":dic[@"categoryName"]}];
                    }
                }
                [dicFavo setValue:arrGroup forKey:@"groups"];
            }
            NSMutableArray *arrHunt =[NSMutableArray new];
            if (sharingHuntsArray) {
                for (NSDictionary *dic in sharingHuntsArray) {
                    NSNumber *num = dic[@"isSelected"];
                    if ( [num boolValue]) {
                        [arrHunt addObject:@{@"id":dic[@"huntID"],@"name":dic[@"categoryName"]}];
                        
                        
                    }
                }
                [dicFavo setValue:arrHunt forKey:@"hunts"];
            }
            NSMutableArray *arrUser =[NSMutableArray new];
            if (arrPersonne) {
                for (NSDictionary *dic in arrPersonne) {
                    NSNumber *num = dic[@"status"];
                    if ( [num boolValue]) {
                        [arrUser addObject:dic];
                        
                    }
                }
                [dicFavo setValue:arrUser forKey:@"sharingUsers"];
            }
            [dicFavo setValue:[NSNumber numberWithInt:MDshare] forKey:@"iSharing"];
            [dicFavo setValue:@{@"share":[NSNumber numberWithInt:MDshare]} forKey:@"sharing"];
            [dicFavo setValue:arrReceivers forKey:@"receivers"];
            [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
            
            [self gotoback];
            
        }
        else
        {

            NSMutableDictionary *dicUpload = [@{@"iSharing":[NSNumber numberWithInt:MDshare]} mutableCopy];
            if (sharingGroupsArray) {
                [dicUpload setObject:sharingGroupsArray forKey:@"sharingGroupsArray"];
            }
            if (sharingHuntsArray) {
                [dicUpload setObject:sharingHuntsArray forKey:@"sharingHuntsArray"];
            }
            if (arrReceivers) {
                [dicUpload setObject:arrReceivers forKey:@"receivers"];
            }
            NSMutableArray *arrTmp = [NSMutableArray new];
            for (NSDictionary *dicPersonne in arrPersonne) {
                [arrTmp addObject:dicPersonne[@"id"]];
            }
            NSString *strUser = @"";
            if (arrTmp.count > 0) {
                strUser = [arrTmp componentsJoinedByString:@","];
            }
            if (strUser) {
                [dicUpload setObject:strUser forKey:@"sharingUser"];
            }
            
            //upload publication
            [[PublicationOBJ sharedInstance] uploadData:dicUpload];
            
            //check if continue do Favorite....Spec updated
            //signed Post favorite -> go to save Fav
            
            if ([PublicationOBJ sharedInstance].isPostFavo) {
                Signaler_Favoris_System *viewController1 = [[Signaler_Favoris_System alloc] initWithNibName:@"Signaler_Favoris_System" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];

            }else{
                //
                //back to Live Hunt...If create from LiveHunt MapGlobalVC
                NSArray * controllerArray = [[self navigationController] viewControllers];
                
                for (UIViewController *controller in controllerArray)
                {
                    //Code here.. e.g. print their titles to see the array setup;
                    
                    if ([controller isKindOfClass: [self.mParent class]]) {
                        [self.navigationController popToViewController:controller animated:NO];
                        return;
                    }
                }
                //Back to Dashboard
                [self.navigationController popToRootViewControllerAnimated:YES];

            }
        }
        
    }
}

-(IBAction)extendMap:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initAlertMap];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        [self.tableControl reloadData];
    }];
    [vc fnSetexpectTarget:self.expectTarget withColor:self.colorNavigation];
    [vc showAlert];

}
-(IBAction)preciserAction:(id)sender
{
    /*
    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
    
    NSArray *listTree = treeDic[@"default"];
    NSMutableDictionary *dicTmp = [NSMutableDictionary new];
    //mld_type
    for (NSDictionary *dic in listTree) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            dicTmp = [dic mutableCopy];
            break;
        }
    }
    Poster_ChoixSpecNiv2 *viewController1 = [[Poster_ChoixSpecNiv2 alloc] initWithNibName:@"Poster_ChoixSpecNiv2" bundle:nil];
    //EG..
    viewController1.myName = dicTmp[@"name"];
    viewController1.myDic = dicTmp;
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    */
    [PublicationOBJ sharedInstance].isEditFavo = NO;
    Signaler_ChoixSpecNiv1 *viewController1 = [[Signaler_ChoixSpecNiv1 alloc] initWithNibName:@"Signaler_ChoixSpecNiv1" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    
}
-(IBAction)formulaireAction:(id)sender
{
    Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
    viewController1.modifiAttachment = YES;
    viewController1.myDic = childCategory;
    viewController1.iSpecific =  [childCategory[@"search"] intValue] ;
    if ([childCategory[@"search"] boolValue] && [[PublicationOBJ sharedInstance] getCacheSpecific_Cards].count > 0) {
        viewController1.myCard = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
        viewController1.id_animal = [childCategory[@"id"] stringValue];
    }
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    
}
//MARK: - PARTAGE
- (IBAction)partageAction:(id)sender {
    Signaler_Choix_Partage *viewController1 = [[Signaler_Choix_Partage alloc] initWithNibName:@"Signaler_Choix_Partage" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];

}
//MARK: - SWITCH
- (IBAction)doSwitch:(id)sender {
    
    UISwitch *ui = (UISwitch*)sender;
//    if (editHaveGeo) {
//        [PublicationOBJ sharedInstance].enableGeolocation = editHaveGeo;
//    }
//    else
//    {
        [PublicationOBJ sharedInstance].enableGeolocation = ui.isOn;
//    }
    [self.tableControl reloadData];
}
- (IBAction)landmarkSwitch:(id)sender {
    
    UISwitch *ui = (UISwitch*)sender;
    [PublicationOBJ sharedInstance].enableLandmark = ui.isOn;
    [self.tableControl reloadData];
}
//MARK: - share extentions
- (void) imageShareExtentionWithInfo:(NSDictionary*)dicInfo
{
    NSString *contentImage = [NSString stringWithFormat:@"%@",@"image"];
    if ([dicInfo[@"type"] isEqualToString:contentImage])
    {
        self.mediaType = TYPE_PHOTO;
        NSString *strName = dicInfo[@"url"];
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
        
        NSURL *url = [NSURL fileURLWithPath:photoFile];
        NSData *dataInfo = [NSData dataWithContentsOfURL:url];
        
        UIImage *tmpimage = [UIImage imageWithData:dataInfo];
        
        //        [self saveImage:tmpimage withInfo:nil];
        
        AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        CLLocation * loc = app.locationManager.location;
        
        NSDictionary *retDic = nil;
        
        if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
            retDic = @{@"image":strName,
                       @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                       @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
        }else{
            retDic =@{@"image":strName};
        }
        [PublicationOBJ sharedInstance].urlPhoto = retDic[@"image"];
        if (retDic[@"Latitude"] != nil) {
            [PublicationOBJ sharedInstance].latitude = [retDic[@"Latitude"] stringValue];
            [PublicationOBJ sharedInstance].longtitude = [retDic[@"Longitude"] stringValue];
        }else{
            [PublicationOBJ sharedInstance].latitude = nil;
            [PublicationOBJ sharedInstance].longtitude = nil;
            
        }
        
    }
    else {
        
        self.mediaType = TYPE_VIDEO;
        NSString *moviePath = dicInfo[@"url"];
        [PublicationOBJ sharedInstance].urlVideo = moviePath;
    }
}
@end
