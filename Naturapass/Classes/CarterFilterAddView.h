//
//  CarterFilterAddView.h
//  Naturapass
//
//  Created by Manh on 10/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "BaseVC.h"
#import "TreeViewNode.h"
#import "CarterFilterFindView.h"
typedef void (^CarterFilterAddViewCallback)(TreeViewNode *nodeParent,TreeViewNode *nodeSub,NSString *strSearch);

@interface CarterFilterAddView : UIView
{
    NSMutableArray * arrData;
    NSMutableArray              *sharingGroupsArray;
    NSMutableArray              *sharingHuntsArray;
    NSMutableArray *arrOwnerOfPublications ;

}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleScreen;
@property (weak, nonatomic) IBOutlet UILabel *lbDescScreen;
@property (weak, nonatomic) IBOutlet UIButton *btnValider;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *vViewSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeaderViewSearch;
@property (weak, nonatomic) IBOutlet UITextField *tfMotCle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintLeading;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeghtCancel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeghtValider;
@property(nonatomic,strong)  UIColor *colorNavigation;

@property (nonatomic,strong) CarterFilterFindView* viewValiderFind;
@property (nonatomic,strong) CarterFilterAddView* viewValiderAdd;
@property (nonatomic, retain) TreeViewNode *nodes;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic, strong) BaseVC *parentVC;
@property (nonatomic,copy) CarterFilterAddViewCallback callback;
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt;
-(void)addContraintSupview:(UIView*)viewSuper;
-(void)fnTreeNode:(TreeViewNode*)node;
-(void)hide:(BOOL)hidden;
-(void)setup;
@end

