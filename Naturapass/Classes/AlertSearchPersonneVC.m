//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertSearchPersonneVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"

static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";
static int  kHeightCell = 40;
static int  kMaxNumberCell = 5;

@implementation AlertSearchPersonneVC
{
}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSearchPersonneVC" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        self.tableControl.hidden  = NO;
        arrData = [NSMutableArray new];
        arrDataSelected = [NSMutableArray new];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    __weak typeof(self) weakVC = self;
    serviceAPI = [WebServiceAPI new];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (errCode == 200) {
            [weakVC performSelectorOnMainThread:@selector(reloadNewData:) withObject:response waitUntilDone:YES];
        }
    };

}
-(void)addContraintSupview:(UIView*)viewSuper
{
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    view.frame = viewSuper.frame;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                               
                                                                        views:NSDictionaryOfVariableBindings(view)]];
}
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect
{
    UIView *view = self;
    [viewSuper addSubview:view];
    view.frame = rect;

}
-(void)fnHide:(BOOL)hide
{
    self.hidden = hide;
    [arrData removeAllObjects];
    [arrDataSelected removeAllObjects];
}
-(void)reloadData:(NSArray*)arrNodes
{
    arrDataSelected = [NSMutableArray arrayWithArray:arrNodes];
    arrData = [arrDataSelected mutableCopy];
}
- (void) GetUsersSearchAction:(NSString*)searchString
{
    if (searchString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];
    }
    else
    {
        [COMMON removeProgressLoading];
        [COMMON addLoadingForView:self];
        [serviceAPI getUsersSearchAction:searchString];
    }
    
    
}
-(void)reloadNewData:(id) response
{
    [self endEditing:YES];
    [arrData removeAllObjects];
    for (NSDictionary*kDic in [response objectForKey:@"users"]) {
        BOOL isCheck = NO;
        for (NSDictionary *dicSelected in arrDataSelected) {
            if ([dicSelected[@"id"] intValue] == [kDic[@"id"] intValue]) {
                isCheck = YES;
            }
        }
        NSMutableDictionary *dicGG = [@{@"id":kDic[@"id"],
                                        @"name":kDic[@"fullname"],
                                        @"status":@(isCheck),
                                        @"type": @(6)} copy];
        [arrData addObject:dicGG];
    }
    [self.tableControl reloadData];
    if (_callback) {
        _callback(nil, arrData.count);
    }
    
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    if (searchText.length > 0) {
        [self GetUsersSearchAction:searchText];
    }
    else
    {
        arrData =[arrDataSelected mutableCopy];
        [self.tableControl reloadData];
    }
}

#pragma callback
-(void)setCallback:(AlertSearchPersonneCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSearchPersonneCallback ) cb
{
    self.callback = cb;
}
#pragma mark -action

-(IBAction)validerAction:(id)sender
{
    //[self removeFromSuperview];
    if (_callback) {
        _callback(arrDataSelected, -1);
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self fnHide:YES];
}
#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kHeightCell;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = arrData[indexPath.row];

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:kHNKDemoSearchResultsCellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"GillSans" size:16.0];
    cell.accessoryType = [dic[@"status"] boolValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    cell.textLabel.text = dic[@"name"];
    
    return cell;
}

#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dicTmp = [arrData[indexPath.row] mutableCopy];
    [dicTmp setObject:@(![dicTmp[@"status"] boolValue]) forKey:@"status"];
    [arrData replaceObjectAtIndex:indexPath.row withObject:dicTmp];

        BOOL isCheck = NO;
        for (int i = 0; i < arrDataSelected.count; i++) {
            NSDictionary *dicSelected = arrDataSelected[i];
            if ([dicSelected[@"id"] intValue] == [dicTmp[@"id"] intValue]) {
                isCheck = YES;
                if ([dicTmp[@"status"] boolValue]) {
                    [arrDataSelected replaceObjectAtIndex:i withObject:dicTmp];
                }
                else
                {
                    [arrDataSelected removeObjectAtIndex:i];
                }
                break;
            }
        }
        if (!isCheck && [dicTmp[@"status"] boolValue]) {
            [arrDataSelected addObject:dicTmp];
        }
        
    //[self.tableControl reloadData];
    [self validerAction: nil];
}



@end
