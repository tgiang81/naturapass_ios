//
//  GroupDetailCell.m
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "GroupDetailCell.h"
#import "Define.h"
@implementation GroupDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_ADMIN_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_NORMAL_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.lbTitle setTextColor:color];
    [self.lblNbSubcribers setTextColor:color];
    [self.btnEnterGroup setBackgroundColor:color];    
}
@end
