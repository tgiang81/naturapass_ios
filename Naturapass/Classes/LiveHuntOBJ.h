//
//  GroupCreateOBJ.h
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
@interface LiveHuntOBJ : NSObject
+ (LiveHuntOBJ *) sharedInstance;

@property(nonatomic,strong) NSDictionary *dicLiveHunt;
@property(nonatomic,strong) NSArray *arrLiveHunt;

-(void) resetParams;
-(void) resetListHunt;

-(NSMutableAttributedString*)formatLiveHuntTimeWithDateBegin:(NSString*)begin withDateEnd:(NSString*)end;
@end
