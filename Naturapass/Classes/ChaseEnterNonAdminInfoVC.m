//
//  ChaseEnterNonAdminInfoVC.m
//  Naturapass
//
//  Created by Giang on 12/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ChaseEnterNonAdminInfoVC.h"
#import "CellKind22.h"
#import "MapLocationVC.h"
#import "AlertMapVC.h"
#import "SAMTextView.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChaseEnterNonAdminInfoVC ()
{
    NSArray * arrData;
    NSString                    *numberOption;
    NSString *str_Owner;
    NSMutableDictionary *dicData;
    IBOutlet UILabel *lbStartDate;
    IBOutlet UILabel *lbStartTime;
    IBOutlet UILabel *lbEndDate;
    IBOutlet UILabel *lbEndTime;
    IBOutlet UILabel *lbAddress;
    IBOutlet UILabel *lbLat;
    IBOutlet UILabel *lbLong;
    IBOutlet UILabel *lblDebut;
    IBOutlet UILabel *lblFin;
    IBOutlet UILabel *lblRendez;
    IBOutlet UIButton *btnSURNATURA;
    IBOutlet UIButton *btnAUTRESGPS;
    IBOutlet UIButton *btnSuivant;
    IBOutlet SAMTextView *textviewCommentPublic;


}
@end

@implementation ChaseEnterNonAdminInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    switch (self.expectTarget) {
            
        case ISCARTE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                [self addMainNav:@"MainNavLiveMap"];
                //SUB
                [self addSubNav:@"livechat"];
                
                [self setThemeTabVC:self];
                
            }
            else
            {
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_TINY_BAR_COLOR);
                //SUB
                [self addSubNav:@"livechat"];
                
            }
            
            
        }
            break;
        default:
            break;
    }
    // Do any additional setup after loading the view from its nib.
    lblDebut.text = str(strDEBUTLE);
    lblFin.text = str(strFINLE);
    lblRendez.text = str(strRendezVousA);
    [btnSURNATURA setTitle:str(strVOIRSURNATURAPASS) forState:UIControlStateNormal];
    [btnAUTRESGPS setTitle:str(strVOIRSURAUTRESGPS) forState:UIControlStateNormal];
    textviewCommentPublic.placeholder = str(strPlaceholder);
    [btnSuivant setTitle:str(strENVOYE) forState:UIControlStateNormal];
    btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    textviewCommentPublic.layer.masksToBounds = YES;
    textviewCommentPublic.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textviewCommentPublic.layer.borderWidth = 0.5;
    [self InitializeKeyboardToolBar];
    [textviewCommentPublic setInputAccessoryView:self.keyboardToolbar];

    NSMutableDictionary *dic1 = [@{@"name":str(strJe_participe),
                                   @"image":@"ic_participate"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strJe_ne_sais_pas),
                                   @"image":@"ic_pre_participate"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strJe_ne_participe_pas),
                                   @"image":@"ic_not_participate"} copy];
    arrData =  [@[dic1,dic2,dic3] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind22" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    dicData  =[NSMutableDictionary dictionaryWithDictionary: [GroupEnterOBJ sharedInstance].dictionaryGroup];
    //
    numberOption =@"-1";
    if ([dicData[@"connected"] isKindOfClass:[NSDictionary class]]) {
        numberOption = dicData[@"connected"][@"participation"];
        textviewCommentPublic.text = dicData[@"connected"][@"publicComment"];
    }
    lbStartDate.text=@"";
    lbStartTime.text=@"";
    lbAddress.text =@"";
    lbLat.text =@"";
    lbLong.text=@"";
    //START DATE
    NSString * startDate = [NSString stringWithFormat:@"%@",dicData[@"meetingDate"]]; //17/10/2015 08:42
    if (startDate.length>0) {
        lbStartDate.text= [startDate substringWithRange:NSMakeRange(0, 10)];
        lbStartTime.text= [NSString stringWithFormat:@"à %@",[startDate substringWithRange:NSMakeRange(11, 5)]];
        
    }
    //END DATE
    NSString *endDate= [NSString stringWithFormat:@"%@",dicData[@"endDate"]];
    if (endDate.length>0) {
        lbEndDate.text= [endDate substringWithRange:NSMakeRange(0, 10)];
        lbEndTime.text= [NSString stringWithFormat:@"à %@",[endDate substringWithRange:NSMakeRange(11, 5)]];
        
    }
    //ADDRESS
    if ([dicData[@"meetingAddress"] isKindOfClass:[NSDictionary class]]) {
        {
            lbAddress.text = dicData[@"meetingAddress"][@"address"];
            lbLat.text =[ NSString stringWithFormat:@"Lat. %.5f" , [dicData[@"meetingAddress"][@"latitude"] floatValue]];
            lbLong.text = [ NSString stringWithFormat:@"Lng. %.5f" , [dicData[@"meetingAddress"][@"longitude"] floatValue]];
        }}
/*
 {
 access = 2;
 connected =     {
 access = 2;
 geolocation = 0;
 participation = 0;
 quiet = 0;
 user =         {
 email = "dinhmanhvp@gmail.com";
 firstname = Valot;
 fullname = "Valot Vincent";
 id = 3608;
 lastname = Vincent;
 profilepicture = "/uploads/users/images/thumb/c4134f0387cfd0ccdbdbe3ed7bbdc101e35f4446.jpeg";
 usertag = "manh-manh";
 };
 };
 description = "Tr\U00e8s belle chasse ds la Meuse gros gibier (sanglier chevreuil cervid\U00e9)";
 endDate = "2016-02-29T17:40:00+0100";
 geolocation = 1;
 id = 282;
 loungetag = vaucouleur;
 meetingAddress =     {
 address = Wormhout;
 latitude = "50.883682";
 longitude = "2.467413";
 };
 meetingDate = "2015-10-17T08:42:00+0200";
 name = Vaucouleur;
 nbSubscribers = 3;
 owner =     {
 firstname = PATRICK;
 fullname = "PATRICK VANBERTEN";
 id = 3895;
 lastname = VANBERTEN;
 profilepicture = "/uploads/users/images/thumb/4568d3644a6e667d9d97690fec7713ed119ea5dd.jpeg";
 usertag = "patrick-vanberten";
 };
 photo = "https://www.naturapass.com/uploads/lounges/images/thumb/536b83f679aec1ac34fdea9731dd4b5e3a625476.jpeg";
 }
 
 */
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    NSDictionary *dic = nil;
    
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
                
            }else{
                //Change status bar color
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            }
            
        }
            break;
        default:
            break;
    }
    
    [self setThemeNavSub:NO withDicOption:dic];
    
    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd/MM/yyyy HH:mm" forFormatter: outputFormatterBis];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}
#pragma mark - callback
-(void)setCallback:(Callback)callback
{
    _callback=callback;
}
-(void)doCallback:(Callback)callback
{
    self.callback=callback;
}
#pragma mark - API
- (void) ParticipeAction
{
    NSString *strID =dicData[@"id"];
    [COMMON addLoading:self];
    NSDictionary * postDict = [NSDictionary dictionaryWithObjectsAndKeys:numberOption,@"participation", nil];
    
    NSString *myID = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putLoungessuscriberParticipateAction:strID user_id:myID withParametersDic:postDict];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (response[@"success"]) {
            
            if ([dicData[@"connected"] isKindOfClass:[NSDictionary class]]) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:dicData[@"connected"]];
                [dic setValue:numberOption forKey:@"participation"];
                [dicData setValue:dic forKey:@"connected"];
                [GroupEnterOBJ sharedInstance].dictionaryGroup = dicData;
            }
            [self.tableControl reloadData];
            if (_callback) {
                self.callback(strID);
            }
        }

    };
    
}
#pragma mark - table delegate
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView* sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    sectionView.backgroundColor= UIColorFromRGB(MAIN_COLOR);
    UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 0, 320, 29)];
    [headerLabel setTextColor:[UIColor blackColor]];
    headerLabel.numberOfLines=2;
    [headerLabel setBackgroundColor:[UIColor clearColor]];
    [headerLabel setFont:FONT_HELVETICANEUE(12)];

    headerLabel.text= str(strSerraiJePresent);
    
    [sectionView addSubview:headerLabel];
    
    return  sectionView;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind22 *cell = nil;
    
    cell = (CellKind22 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.ico_status.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.textLbl.text = dic[@"name"];
    [cell.textLbl setTextColor:[UIColor blackColor]];
    [cell.textLbl setFont:FONT_HELVETICANEUE(15)];
    //
    int option =4;
    switch ([numberOption integerValue]) {
        case 0:
        {
            option=2;
            
        }
            break;
            
        case 1:
        {
            option=0;
            
        }
            break;
        case 2:
        {
            option=1;
        }
            break;
        default:
            break;
    }

    
    //Arrow
    if(option == indexPath.row)
    {
        cell.imgTick.hidden=NO;
        cell.contentView.backgroundColor = UIColorFromRGB(CHASSES_CELL_ACTIVE_COLOR);
    }
    else
    {
        cell.imgTick.hidden=YES;
        cell.contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            numberOption=@"1";
            
        }
            break;
            
        case 1:
        {
            numberOption=@"2";
            
        }
            break;
        case 2:
        {
            numberOption=@"0";
            
        }
            break;
        default:
            break;
    }
    [self ParticipeAction];
}

-(IBAction)NATURAAction:(id)sender
{
    id dic = dicData[@"meetingAddress"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = YES;
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"]};

        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        
    }
}
-(IBAction)GPSAction:(id)sender
{
    //ADDRESS
    NSString *strLat =@"";
    NSString *strLng =@"";
    NSString *strAddress =@"";
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    double lat = appDelegate.locationManager.location.coordinate.latitude;
    double lng = appDelegate.locationManager.location.coordinate.longitude;

    if ([dicData[@"meetingAddress"] isKindOfClass:[NSDictionary class]]) {
        {
            
            
            strAddress = dicData[@"meetingAddress"][@"address"];
            strLat = dicData[@"meetingAddress"][@"latitude"];
            strLng = dicData[@"meetingAddress"][@"longitude"];
            
        }}
    AlertMapVC *vc = [[AlertMapVC alloc] initWithTitle:nil message:nil];
    
    [vc doBlock:^(NSInteger index) {
        if (index==0) {
            // Plans
            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];

//            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
            NSURL *url =[NSURL URLWithString:str];
            
            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                NSLog(@"Can't use applemap://");
            }
        }
        else if(index==1)
        {


            //Maps
            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
//            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
            }

        }
        else if(index==2)
        {
            //Waze
            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"waze://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                // Waze is not installed. Launch AppStore to install Waze app
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
            }
        }
    }];
    [vc showInVC:self];
}
- (void)resignKeyboard:(id)sender
{
    [textviewCommentPublic resignFirstResponder];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)validerAction:(id)sender
{
    NSMutableDictionary *postDict = [NSMutableDictionary new];
    [postDict setValue:numberOption forKey:@"participation"];
    NSString *strContent =textviewCommentPublic.text?textviewCommentPublic.text:@"";
    [postDict setValue:strContent forKey:@"content"];
    
    NSString *strID =dicData[@"id"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI = [WebServiceAPI new];
    NSString * _strid = dicData[@"connected"][@"user"][@"id"];
    [serviceAPI putLoungesuscriberParticipationCommentAction:strID strID:_strid withParametersDic:postDict];
    serviceAPI.onComplete =^(id response, int intCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if(response[@"success"]){
            //Alert success
            
            if ([dicData[@"connected"] isKindOfClass:[NSDictionary class]]) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:dicData[@"connected"]];
                [dic setValue:strContent forKey:@"publicComment"];
                [dicData setValue:dic forKey:@"connected"];
                [GroupEnterOBJ sharedInstance].dictionaryGroup = dicData;
            }
        }
    };
    
}
@end
