//
//  PosterTerminez.m
//  Naturapass
//
//  Created by JoJo on 9/11/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "PosterTerminez.h"
#import "ChassesCreateHeaderCell.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateMapCell.h"
#import "ChassesCreateTextFieldCell.h"
#import "SignalerMediaCell.h"
#import "SignalerPartagerCell.h"
#import "Media_SheetView.h"
#import <AVFoundation/AVFoundation.h>
#import "AlertVC.h"
#import "NetworkCheckSignal.h"
#import "AlertSuggestView.h"
#import "AlertSuggestPersoneView.h"
#import "DatabaseManager.h"
#import "Signaler_Choix_Partage.h"
#import "LiveHuntOBJ.h"
#import "MapGlobalVC.h"
#import "ChassesCreateTextViewCell.h"
#import "Signaler_ChoixSpecNiv2.h"
#import "ChassesCreateSwitchCell.h"
#import "Signaler_ChoixSpecNiv1.h"
#import "Signaler_ChoixSpecNiv5.h"
#import "Signaler_Favoris_System.h"
typedef enum
{
    FIELD_GEO_TITLE,
    FIELD_GEO_MAP,
    FIELD_GEO_SEARCH,
    FIELD_COMMENT_TITLE,
    FIELD_COMMENT,
    FIELD_MEDIA,
    FILED_PARTAGE,
    FIELD_LEGEND,
    FIELD_PRECISER,
    FIELD_FORMULAIRE
}SIGNALER_FIELD_TYPE;

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierTF = @"identifierTF";
static NSString *identifierMedia = @"identifierMedia";
static NSString *identifierPar = @"identifierPar";

static NSString *identifierMap = @"identifierMap";
static NSString *identifierTV = @"identifierTV";
static NSString *identifierSW = @"identifierSW";


@interface PosterTerminez ()<UITextFieldDelegate, GMSMapViewDelegate , WWCalendarTimeSelectorProtocol,UITextViewDelegate>
{
    NSString *strLatitude, *strLongitude, *strAltitude;
    AlertSuggestView *suggestView;
    AlertSuggestPersoneView *suggestViewPersonne;
    NSInteger typeSelected;
    CLLocation *my_Placemark;
    NSMutableArray              *mesArr;
    
    NSMutableArray              *sharingGroupsArray;
    NSMutableArray              *sharingHuntsArray;
    NSMutableArray              *arrReceivers;
    NSMutableArray              *arrPersonne;
    int  MDshare;
    NSString *txtPartage;
    IBOutlet UIButton *btnSuivatnt;
    IBOutlet UIButton *btnAnnuler;
    NSDictionary *childCategory;
    BOOL editHaveGeo;
}
@property(assign) MEDIATYPE mediaType;
@property (nonatomic, strong) ChassesCreateBaseCell *cellMap;
@property (strong, nonatomic)  NSString *strAddress;
@property (strong, nonatomic)  NSString *strTitle;


@end

@implementation PosterTerminez

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MainNavigationBaseView *subview =  [self getSubMainView];
    
    if([PublicationOBJ sharedInstance].isEditer)
    {
        [subview.myTitle setText:@"MODIFIER UNE PUBLICATION..."];
    }else{
        [subview.myTitle setText:@"POSTER UNE PUBLICATION..."];
    }
    
    [subview.myDesc setText:@""];
    self.viewBottom.backgroundColor = self.colorBackGround;
    self.viewBottom1.backgroundColor = self.colorBackGround;

    [btnSuivatnt.layer setMasksToBounds:YES];
    btnSuivatnt.layer.cornerRadius= 25.0;
    btnSuivatnt.backgroundColor = self.colorNavigation;
    [btnSuivatnt setTitle:@"PUBLIER!" forState:UIControlStateNormal];
    [btnAnnuler.layer setMasksToBounds:YES];
    btnAnnuler.layer.cornerRadius= 20.0;
    btnAnnuler.backgroundColor = self.colorAnnuler;

    [self.btnTERMINER.layer setMasksToBounds:YES];
    self.btnTERMINER.layer.cornerRadius= 25.0;
    self.btnTERMINER.backgroundColor = self.colorNavigation;

    
    self.viewBottom.hidden = YES;
    self.viewBottom1.hidden = NO;
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateHeaderCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextFieldCell" bundle:nil] forCellReuseIdentifier:identifierTF];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateMapCell" bundle:nil] forCellReuseIdentifier:identifierMap];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerMediaCell" bundle:nil] forCellReuseIdentifier:identifierMedia];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerPartagerCell" bundle:nil] forCellReuseIdentifier:identifierPar];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextViewCell" bundle:nil] forCellReuseIdentifier:identifierTV];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateSwitchCell" bundle:nil] forCellReuseIdentifier:identifierSW];

    self.tableControl.estimatedRowHeight = 60;
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self InitializeKeyboardToolBar];

    self.arrData = [NSMutableArray new];
    sharingGroupsArray= [NSMutableArray new];
    sharingHuntsArray= [NSMutableArray new];
    
    //Filter
    mesArr =        [NSMutableArray new];
    arrReceivers=  [NSMutableArray new];
    arrPersonne = [NSMutableArray new];
    
    NSDictionary *dicGeoTitle  = @{@"name":@"GÉOLOCALISATION",
                                   @"icon":@"icon_sign_geo",
                                   @"type":@(FIELD_GEO_TITLE)};
    NSDictionary *dicGeoMap  = @{@"type":@(FIELD_GEO_MAP)};
    NSDictionary *dicGeoSearch  = @{
                                    @"type":@(FIELD_GEO_SEARCH),
                                    @"placehold":@"Utiliserma position...",
                                    @"icontf":@"icon_chasses_address"};
    NSDictionary *dicCommentTitle  = @{@"name":@"CONTENU DE LA PUBLICATION",
                                   @"icon":@"icon_pub_pen_green",
                                   @"type":@(FIELD_COMMENT_TITLE)};
    NSDictionary *dicComment  = @{@"type":@(FIELD_COMMENT),
                                  @"placehold":@"Ajouter un commentaire (facultatif)"};
    NSDictionary *dicMedia  =   @{@"nameleft":@"PHOTO...",
                                   @"iconleft":@"icon_sign_photo",
                                  @"nameright":@"VIDÉO...",
                                  @"iconright":@"icon_sign_video",
                                   @"type":@(FIELD_MEDIA)};
//    NSDictionary *dicLegend  =   @{@"nameleft":@"CRÉER UN FAVORI...",
//                                  @"iconleft":@"icon_sign_favori",
//                                  @"nameright":@"LÉGENDE COULEUR...",
//                                  @"iconright":@"icon_sign_legend",
//                                  @"type":@(FIELD_LEGEND)};
    NSDictionary *dicLegend = nil;
    
    if ([PublicationOBJ sharedInstance].isEditer)
    {
        dicLegend  =   @{@"name":@"LÉGENDE COULEUR...",
                         @"icon":@"icon_sign_legend",
                         @"type":@(FIELD_LEGEND)};
    }else{
        dicLegend  =   @{@"nameleft":@"CRÉER UN FAVORI...",
                         @"iconleft":@"icon_sign_favori",
                         @"nameright":@"LÉGENDE COULEUR...",
                         @"iconright":@"icon_sign_legend",
                         @"type":@(FIELD_LEGEND)};
        
    }

    NSDictionary *dicPar  = @{@"name":@"PARTAGER AVEC...",
                              @"placehold":@"Chasse en cours",
                              @"type":@(FILED_PARTAGE)};
    NSDictionary *dicPreciser  = @{@"name":@"PRÉCISER",
                                   @"icon":@"icon_pub_preciser",
                                   @"type":@(FIELD_PRECISER)};
    [self.arrData addObject:dicCommentTitle];
    [self.arrData addObject:dicComment];
    if (![PublicationOBJ sharedInstance].isEditer) {
        [self.arrData addObject:dicMedia];
    }
    [self.arrData addObject:dicPar];
    [self.arrData addObject:dicGeoTitle];
    [self.arrData addObject:dicGeoMap];
//    [self.arrData addObject:dicGeoSearch];
    [self.arrData addObject:dicLegend];
    [self.arrData addObject:dicPreciser];
    suggestView = [[AlertSuggestView alloc] initSuggestView];
    suggestViewPersonne = [[AlertSuggestPersoneView alloc] initSuggestView];
    //mld
    [PublicationOBJ sharedInstance].enableGeolocation = NO;
    sharingHuntsArray = [NSMutableArray new];
    if([LiveHuntOBJ sharedInstance].dicLiveHunt)
    {    [sharingHuntsArray addObject:@{@"categoryName":  [LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"] ,
                                        @"huntID":  [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"],
                                        @"isSelected": [NSNumber numberWithBool:YES]
                                        }];
    }
    if(self.isFromFavo)
        {
            [self fnSetValueIsEditer];
        }
    if([PublicationOBJ sharedInstance].isEditer)
    {
        [btnSuivatnt setTitle:@"APPLIQUER !" forState:UIControlStateNormal];
        [self fnFillDataEditPublication];
    }

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //Auto...type...
    if (self.autoShow == TYPE_PHOTO) {
        [self onClickPhoto:nil];
        
    }else if (self.autoShow == TYPE_VIDEO) {
        [self onClickVideo:nil];
    }
    if (_dicShareExtention != nil) {
        [self imageShareExtentionWithInfo:_dicShareExtention];
    }
    
    NSDictionary *dicPatarge = [[PublicationOBJ sharedInstance].dicPatarge copy];
    if(dicPatarge)
    {
        MDshare =[dicPatarge[@"iSharing"] intValue];
        sharingGroupsArray = [dicPatarge[@"sharingGroupsArray"] mutableCopy];
        sharingHuntsArray = [dicPatarge[@"sharingHuntsArray"] mutableCopy];
        arrReceivers = [dicPatarge[@"receivers"] mutableCopy];
        arrPersonne = [dicPatarge[@"personne"] mutableCopy];

    }
    txtPartage = @"Autres...";
    //set check list hunt
//    [PublicationOBJ sharedInstance].dicPatarge
/*
 
        iSharing = 0;
        personne =     (
        );
        receivers =     (
        );
        sharingGroupsArray =     (

        sharingHuntsArray =     (

        sharingUser = "";
*/
    
    for (int i = 0; i<sharingHuntsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
        
        if ([[PublicationOBJ sharedInstance].huntID  intValue] == [dic[@"huntID"]  intValue] && [dic[@"isSelected"] boolValue])
        {
            txtPartage = @"Chasse en cours";
            [self.tableControl reloadData];
            break;
            
        }
    }
    //Change condition if any
    //•If he lets default sharing = agenda of the live where he comes from then on the button “PARTAGER AVEC” it’s still written “Chasse en cours” (means “share with : live agenda”)
    //•If he changes sthg on the sharing screen then the text inside button will change to : “Autres…”

    BOOL bOtherAction = NO;
    
    int iTestCount = 0;

    for (int i = 0; i<sharingHuntsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];

        if ([dic[@"isSelected"] boolValue] == TRUE ) {
            iTestCount += 1;
        }
    }
    
    //user interactive with Hunt
    if (iTestCount == 0 || iTestCount > 1) {
        bOtherAction = YES;
    }
    
    for (int i = 0; i<sharingGroupsArray.count; i++) {
        NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
        
        if ([dic[@"isSelected"] boolValue] == TRUE ) {
            iTestCount += 1;
        }
    }
    
    if (arrPersonne.count > 0) {
        iTestCount += 1;
    }

    if (MDshare > 0) {
        iTestCount += 1;
    }
    
    if (iTestCount > 1) {
        bOtherAction = YES;
    }
    
    if (bOtherAction) {
        txtPartage = @"Autres...";
    }
    
    [self.tableControl reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)fnFillDataEditPublication
{
    /*
     Chú ý: + Nếu publication đã có geo thì không cho xóa, mà chỉ cho thay đổi vị trí (không cho switch off)
            + Nếu publication chưa có geo cho phép on/off switch
            + UI mới chưa có màn hình update giá trị cho observation chỉ cho phép chọn observation mới
     */
    NSDictionary *dicEditer =  [[PublicationOBJ sharedInstance].dicEditer copy];
    NSString *contentValue = [dicEditer[@"content"] emo_emojiString];
    if(contentValue)
    {
        self.strTitle = contentValue;
    }
    if ([dicEditer[@"geolocation"] isKindOfClass:[NSDictionary class]]) {
        [PublicationOBJ sharedInstance].latitude =dicEditer[@"geolocation"][@"latitude"];
        [PublicationOBJ sharedInstance].longtitude =dicEditer[@"geolocation"][@"longitude"];
        self.strAddress =dicEditer[@"geolocation"][@"address"];
        [PublicationOBJ sharedInstance].address = self.strAddress;
        [PublicationOBJ sharedInstance].location = self.strAddress;
        [PublicationOBJ sharedInstance].enableGeolocation = YES;
        editHaveGeo = TRUE;
    }
    if (dicEditer[@"legend"]) {
        [PublicationOBJ sharedInstance].legend = dicEditer[@"legend"];
    }
    //color
    if ([dicEditer[@"color"] isKindOfClass:[NSDictionary class]]) {
        [PublicationOBJ sharedInstance].publicationcolor = dicEditer[@"color"][@"id"];

    }
    //save sharing option: Amis/Natiz/Other
    if ([dicEditer[@"sharing"] isKindOfClass:[NSDictionary class]]) {
        MDshare =[dicEditer[@"sharing"][@"share"] intValue];

    }
    NSArray *arrGroup = dicEditer[@"groups"];
    sharingGroupsArray = [NSMutableArray new];
    if (arrGroup.count > 0) {
        for (NSDictionary *kDic in arrGroup) {
            [sharingGroupsArray addObject: @{@"categoryName":  kDic[@"name"],
                                             @"groupID":  kDic[@"id"],
                                             @"isSelected": [NSNumber numberWithBool:YES]
                                             }];
        }
    }
    NSArray *arrHunts = dicEditer[@"hunts"];
    sharingHuntsArray = [NSMutableArray new];
    if (arrHunts) {
        for (NSDictionary *kDic in arrHunts) {
            [sharingHuntsArray addObject: @{@"categoryName":  kDic[@"name"],
                                             @"huntID":  kDic[@"id"],
                                             @"isSelected": [NSNumber numberWithBool:YES]
                                             }];
        }
    }
    NSArray *arrOb = dicEditer[@"observations"];
    if (arrOb.count>0) {
       NSArray *arrReceiversValue = [dicEditer[@"observations"][0][@"sharing_receiver"] copy];
        if (arrReceiversValue) {
            arrReceivers = [arrReceiversValue mutableCopy];
        }
        [self fillDataAttachment];
    }
    NSArray *arrUser = dicEditer[@"shareusers"];
    arrPersonne = [NSMutableArray new];
    if (arrUser.count > 0) {
        for (int i = 0; i < arrUser.count; i++) {
            NSMutableDictionary *dicUser = [arrUser[i] mutableCopy];
            [dicUser setObject:@(TRUE) forKey:@"status"];
            [arrPersonne addObject:dicUser];
        }
    }
}
-(void)fillDataAttachment
{
    NSDictionary *myCard = nil;
    NSArray *arrOb = [PublicationOBJ sharedInstance].dicEditer[@"observations"];
    NSMutableDictionary  *dicObservation = [NSMutableDictionary new];
    if (arrOb.count>0) {
        if (arrOb[0][@"category_id"]) {
            [dicObservation setObject:arrOb[0][@"category_id"] forKey:@"category"];
        }
        if (arrOb[0][@"specific"]) {
            [dicObservation setObject:arrOb[0][@"specific"] forKey:@"specific"];
        }
        if (arrOb[0][@"animal"]) {
            [dicObservation setObject:arrOb[0][@"animal"] forKey:@"animal"];
        }
        //get data with categrogy_id
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        for (NSDictionary*nDic in [PublicationOBJ sharedInstance].treeCategory) {
            [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
            if (childCategory != nil) {
                break;
            }
        }
        
        if (childCategory == nil) {
            // neu duyet trong tree default không có thì sẽ duyệt trong các tree còn lại
            BOOL isFinish = false;
            for (NSString *strKey in treeDic.allKeys) {
                if (![strKey isEqualToString:@"default"]) {
                    NSArray *arrTreeCategory = treeDic[strKey];
                    for (NSDictionary*nDic in arrTreeCategory) {
                        [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
                        if (childCategory != nil) {
                            isFinish =TRUE;
                            break;
                        }
                    }
                }
                if (isFinish == TRUE) {
                    break;
                }
            }
        }

        NSArray *listCards = [[PublicationOBJ sharedInstance] getCache_Cards];
        for (NSDictionary*dicCard in listCards) {
            if ([childCategory[@"card"][@"id"] intValue] == [dicCard[@"id"] intValue]) {
                //Edit...Card UP - DOWN
                myCard = dicCard;
                break;
            }
        }
        if (myCard) {
            NSMutableArray *arrTmp = [NSMutableArray new];

                NSMutableArray *arrLabels = [myCard[@"labels"] mutableCopy];
                NSArray *modifiAttachment = arrOb[0][@"attachments"];
                if (modifiAttachment > 0) {
                    for (NSDictionary *itemAttachment in modifiAttachment) {
                        for (NSDictionary *dicLabel in arrLabels) {
                            if ([itemAttachment[@"label"] isEqualToString: dicLabel[@"name"]] ) {
                                NSArray *arrValues = itemAttachment[@"values"];
                                if (arrValues.count > 0) {
                                    for (NSString *strTmp in arrValues) {
                                        [arrTmp addObject:@{
                                                            @"label":dicLabel[@"id"],
                                                            @"value":strTmp
                                                            }];
                                        }
                                    }
                                else
                                {
                                    [arrTmp addObject:@{
                                                        @"label":dicLabel[@"id"],
                                                        @"value":itemAttachment[@"value"]?itemAttachment[@"value"]:@""
                                                        }];
                                }
                            break;
                            }
                        }
                    }
                }
            if (arrTmp.count > 0) {
                [dicObservation setObject:arrTmp forKey:@"attachments"];
                NSDictionary *dicFormulaire  = @{@"name":@"MODIFIER LE FORMULAIRE",
                                                 @"icon":@"newpub_icon_edit",
                                                 @"type":@(FIELD_FORMULAIRE)};
                [self.arrData addObject:dicFormulaire];
            }
        }
        [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
    }
}
-(void) getDataTree:(NSDictionary*) trDic withCategoryID:(int)categoryID
{
    
    if ([trDic[@"id"] intValue] == categoryID ) {
        childCategory = trDic;
        return;
    }else{
        
        for (NSDictionary*nDic in trDic[@"children"]) {
            [self getDataTree:nDic withCategoryID:categoryID];
        }
    }
}
-(void)fnSetValueIsEditer
{
    NSString *strId =@"id";
    // set value ishare
    MDshare =[PublicationOBJ sharedInstance].iShare;

    //set check list group
    for (NSDictionary *dicGroup in [PublicationOBJ sharedInstance].arrGroup) {
        for (int i = 0; i<sharingGroupsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
            
            if ([dicGroup[strId]  intValue] == [dic[@"groupID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingGroupsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //    if (self.isEditFavo) {
    //        strId =@"huntID";
    //    }
    //set check list hunt
    for (NSDictionary *dicHunt in [PublicationOBJ sharedInstance].arrHunt) {
        for (int i = 0; i<sharingHuntsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
            
            if ([dicHunt[strId]  intValue] == [dic[@"huntID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingHuntsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //set check list hunt
    [arrPersonne removeAllObjects];
    for (NSDictionary *dicUser in [PublicationOBJ sharedInstance].arrsharingUsers) {
        NSMutableDictionary *dic=[dicUser mutableCopy];
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        [arrPersonne addObject:dic];
    }
    if ([PublicationOBJ sharedInstance].arrReceivers) {
        arrReceivers = [NSMutableArray new];
        [arrReceivers addObjectsFromArray:[PublicationOBJ sharedInstance].arrReceivers];
    }
    [self.tableControl reloadData];
    
    
}
#pragma mark - TABLEVIEW
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[ChassesCreateBaseCell class]])
    {
        NSDictionary *dicData = self.arrData[indexPath.row];
        ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)cellTmp;
        cell.btnAddress.hidden = TRUE;
        switch ([dicData[@"type"] intValue]) {
            case FIELD_COMMENT_TITLE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = YES;
                cell.btnSelected.hidden = YES;

            }
                break;
            case FIELD_GEO_TITLE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.swFilter.on = [PublicationOBJ sharedInstance].enableGeolocation;
                [cell.swFilter setOnTintColor:self.colorNavigation];
                cell.swFilter.backgroundColor = self.colorAnnuler;
                cell.swFilter.layer.masksToBounds = YES;
                cell.swFilter.layer.cornerRadius = 16.0;
                [cell.swFilter addTarget:self action:@selector(doSwitch:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case FIELD_PRECISER:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = NO;
                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                cell.btnSelected.hidden = NO;
                
                //reset target

                [cell.btnSelected removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];

                [cell.btnSelected addTarget:self action:@selector(preciserAction:) forControlEvents:UIControlEventTouchUpInside];

            }
                break;
            case FIELD_FORMULAIRE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgIcon.tintColor = self.colorNavigation;
                cell.imgArrow.hidden = NO;
                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                cell.btnSelected.hidden = NO;
                
                //reset target
                
                [cell.btnSelected removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnSelected addTarget:self action:@selector(formulaireAction:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
            case FIELD_LEGEND:
            {
                //has 2 sides
                if (dicData[@"nameleft"] != nil)
                {
                    cell.lbMediaLeft.text = dicData[@"nameleft"];
                    cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"iconleft"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgMediaLeft.tintColor = self.colorNavigation;

                    cell.lbMediaRight.text = dicData[@"nameright"];
                    cell.imgMediaRight.image = [[UIImage imageNamed:dicData[@"iconright"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgMediaRight.tintColor = self.colorNavigation;

                    [cell.btnMediaLeft removeTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnMediaRight removeTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnMediaLeft removeTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnMediaRight removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    //https://heolys.atlassian.net/browse/MA-402 hide add to favoris button
                    //But we can uncomment it to test
                    //                        cell.btnMediaLeft.hidden = TRUE;
                    //                        cell.lbMediaLeft.text = @"";
                    //                        cell.imgMediaLeft.image = nil;
                    //
                    if ([PublicationOBJ sharedInstance].isPostFavo) {
                        cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"icon_sign_favori_active"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                        cell.imgMediaLeft.tintColor = self.colorNavigation;
                    }
                    cell.lbMediaLeft.text = @"UTILISER EN TANT QUE FAVORIS...";
                    
                    [cell.btnMediaLeft addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    [cell.btnMediaRight addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                else //just 1 field
                {
                    cell.lbTitle.text = dicData[@"name"];
                    cell.imgIcon.image = [[UIImage imageNamed:dicData[@"icon"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imgIcon.tintColor = self.colorNavigation;
                    cell.imgArrow.hidden = YES;
                    cell.btnSelected.hidden = NO;
                    
                    //reset target
                    [cell.btnSelected removeTarget:self action:@selector(preciserAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    [cell.btnSelected addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                
            }
                break;


            case FIELD_GEO_MAP:
            {
                cell.mapView_.delegate = self;
                cell.ratioMap.active = NO;
                [cell fnSetLocation:[PublicationOBJ sharedInstance].latitude withLongitude:[PublicationOBJ sharedInstance].longtitude];
                if (self.expectTarget == ISMUR) {
                    [cell.btnCurrentLocation setImage:[UIImage imageNamed:@"icon_pub_gps_center"] forState:UIControlStateNormal];
                    [cell.btnCloseMap setImage:[UIImage imageNamed:@"ic_pub_zoom"] forState:UIControlStateNormal];
                }
                else
                {
                    [cell.btnCurrentLocation setImage:[UIImage imageNamed:@"ic_live_gps_center"] forState:UIControlStateNormal];
                    [cell.btnCloseMap setImage:[UIImage imageNamed:@"icon_sign_zoom"] forState:UIControlStateNormal];
                }
                [cell.btnCloseMap addTarget:self action:@selector(extendMap:) forControlEvents:UIControlEventTouchUpInside];
                cell.btnCurrentLocation.hidden= NO;
            }
                break;
            case FIELD_GEO_SEARCH:
            {
                    cell.imageLeftTF.image = [[UIImage imageNamed:dicData[@"icontf"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                    cell.imageLeftTF.tintColor = self.colorNavigation;

                    cell.tfInput.placeholder = dicData[@"placehold"];
                    self.cellMap = cell;
                    cell.tfInput.delegate = self;
                    [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                    cell.tfInput.text = self.strAddress;
                    [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                    cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;

            }
                break;
            case FIELD_MEDIA:
            {
                cell.lbMediaLeft.text = dicData[@"nameleft"];
                cell.imgMediaLeft.image = [[UIImage imageNamed:dicData[@"iconleft"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaLeft.tintColor = self.colorNavigation;
                
                cell.lbMediaRight.text = dicData[@"nameright"];
                cell.imgMediaRight.image = [[UIImage imageNamed:dicData[@"iconright"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgMediaRight.tintColor = self.colorNavigation;
                [cell.btnMediaLeft removeTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnMediaRight removeTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnMediaLeft removeTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];

                [cell.btnMediaRight removeTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];

                if([dicData[@"type"] intValue] == FIELD_MEDIA)
                    {
                        [cell.btnMediaLeft addTarget:self action:@selector(onClickPhoto:) forControlEvents:UIControlEventTouchUpInside];
                        [cell.btnMediaRight addTarget:self action:@selector(onClickVideo:) forControlEvents:UIControlEventTouchUpInside];
                    }
                else
                    {
                        //https://heolys.atlassian.net/browse/MA-402 hide add to favoris button
                        //But we can uncomment it to test
                        cell.btnMediaLeft.hidden = TRUE;
                        cell.lbMediaLeft.text = @"";
                        cell.imgMediaLeft.image = nil;
//                         [cell.btnMediaLeft addTarget:self action:@selector(favoriteAction:) forControlEvents:UIControlEventTouchUpInside];

                        
                        [cell.btnMediaRight addTarget:self action:@selector(chooseColorAction:) forControlEvents:UIControlEventTouchUpInside];

                    }

            }
                break;
                
            case FILED_PARTAGE:
            {
                cell.lbTitle.text = dicData[@"name"];
                cell.lbPartager.text = txtPartage;//dicData[@"placehold"];
                [cell.btnPartager addTarget:self action:@selector(partageAction:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.imgDropdown.image = [[UIImage imageNamed:@"icon_sign_dropdown"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imgDropdown.tintColor = self.colorNavigation;

            }
                break;
            case FIELD_COMMENT:
            {
                cell.textView.placeholder = dicData[@"placehold"];
                cell.textView.text = self.strTitle;
                [cell.textView setInputAccessoryView:self.keyboardToolbar];
                cell.textView.delegate = self;
                int remain = (int)cell.textView.text.length;
                [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
                
                // Check if the count is over the limit
                if(remain >= limitPublication - 10) {
                    // Change the color
                    [cell.numberRemain setTextColor:[UIColor redColor]];
                }
                else if(remain > limitPublication - 50 && remain < limitPublication - 10) {
                    // Change the color to yellow
                    [cell.numberRemain setTextColor:[UIColor orangeColor]];
                }
                else {
                    // Set normal color
                    [cell.numberRemain setTextColor:[UIColor darkGrayColor]];
                }
            }
                break;
            default:
            {
                cell.imageLeftTF.image = [[UIImage imageNamed:dicData[@"icontf"]] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                cell.imageLeftTF.tintColor = self.colorNavigation;
                cell.tfInput.placeholder = dicData[@"placehold"];
                cell.tfInput.text = self.strTitle;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
            }
                break;
        }
    }
    
}
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dicData = self.arrData[indexPath.row];

    if ([PublicationOBJ sharedInstance].enableGeolocation == NO) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_LEGEND:
            case FIELD_GEO_MAP:
            case FIELD_GEO_SEARCH:
            {
                return 0;
            }
                break;
        }
    }
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChassesCreateBaseCell *cell = nil;
    NSDictionary *dicData = self.arrData[indexPath.row];
    switch ([dicData[@"type"] intValue]) {
        case FIELD_COMMENT_TITLE:
        case FIELD_PRECISER:
        case FIELD_FORMULAIRE:
        {
            cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        }
            break;
        case FIELD_GEO_TITLE:
        {
            cell = (ChassesCreateSwitchCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSW forIndexPath:indexPath];

        }
            break;
            
        case FIELD_GEO_MAP:
        {
            cell = (ChassesCreateMapCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMap forIndexPath:indexPath];
            
        }
            break;
        case FIELD_GEO_SEARCH:
        {
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            
        }
            break;
        case FIELD_MEDIA:
        {
            cell = (SignalerMediaCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMedia forIndexPath:indexPath];

        }
            break;
        case FIELD_LEGEND:
        {
            //2 sides
            if (dicData[@"nameleft"] != nil) {
                cell = (SignalerMediaCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMedia forIndexPath:indexPath];
            }else{
                //1 side
                cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
            }
        }
            break;
        case FILED_PARTAGE:
        {
            cell = (SignalerPartagerCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPar forIndexPath:indexPath];
            
        }
            break;
        case FIELD_COMMENT:
        {
            cell = (ChassesCreateTextViewCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTV forIndexPath:indexPath];
            
        }
            break;
        default:
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            break;
    }
    
    if (cell) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    cell.backgroundColor = self.colorCellBackGround;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.hidden = NO;
    if ([PublicationOBJ sharedInstance].enableGeolocation == NO) {
        switch ([dicData[@"type"] intValue]) {
            case FIELD_LEGEND:
            case FIELD_GEO_MAP:
            case FIELD_GEO_SEARCH:
            {
                cell.hidden = TRUE;
            }
                break;
        }
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}
-(IBAction)favoriteAction:(id)sender
{
    //favo
    /*
     NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
     NSMutableArray *arrGroup =[NSMutableArray new];
     NSMutableArray *arrHunt =[NSMutableArray new];
     
     if (sharingGroupsArray) {
     for (NSDictionary *dic in sharingGroupsArray) {
     NSNumber *num = dic[@"isSelected"];
     if ( [num boolValue]) {
     [arrGroup addObject :@{@"groupID":dic[@"groupID"],@"categoryName":dic[@"categoryName"],@"isSelected":dic[@"isSelected"]}];
     }
     }
     [dicFavo setValue:arrGroup forKey:@"sharingGroupsArray"];
     }
     
     if (sharingHuntsArray) {
     for (NSDictionary *dic in sharingHuntsArray) {
     NSNumber *num = dic[@"isSelected"];
     if ( [num boolValue]) {
     [arrHunt addObject :@{@"huntID":dic[@"huntID"],@"categoryName":dic[@"categoryName"],@"isSelected":dic[@"isSelected"]}];
     }
     }
     [dicFavo setValue:arrHunt forKey:@"sharingHuntsArray"];
     }
     if (arrPersonne.count) {
     [dicFavo setValue:arrPersonne forKey:@"sharingUsers"];
     }
     NSMutableArray *arrTmp = [NSMutableArray new];
     for (NSDictionary *dicPersonne in arrPersonne) {
     [arrTmp addObject:dicPersonne[@"id"]];
     }
     NSString *strUser = @"";
     if (arrTmp.count > 0) {
     strUser = [arrTmp componentsJoinedByString:@","];
     }
     [dicFavo setValue:[NSNumber numberWithInt:MDshare] forKey:@"iSharing"];
     [dicFavo setValue:@{@"share":[NSNumber numberWithInt:MDshare]} forKey:@"sharing"];
     [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
     
     AlertVC *vc = [[AlertVC alloc] initInputMessage ];
     [vc doBlock:^(NSInteger index, NSString* str) {
     if (str.length > 0) {
     [self addFavoriteWithName:str withDic:[PublicationOBJ sharedInstance].dicFavoris];
     
     }
     }];
     [vc showAlert];
     */
    AlertVC *vc = [[AlertVC alloc] initConfirmFavorisWithExpectTarget:self.expectTarget];
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        //valider
        if (index == 1) {
            
            Signaler_Favoris_System *viewController1 = [[Signaler_Favoris_System alloc] initWithNibName:@"Signaler_Favoris_System" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
        }
    }];
    [vc showAlert];
}
-(void)addFavoriteWithName:(NSString*)strName withDic:(NSDictionary*)dicFavo
{
    if ([strName isEqualToString:@""] || strName == nil) {
        [KSToastView ks_showToast: [NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] duration:2.0f completion: ^{
        }];
        return;
    }
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    NSArray *arrHunt =nil;
    NSArray *arrGroup =nil;
    
    NSMutableDictionary *dicPost = [NSMutableDictionary new];
    if (strName) {
        [dicPost setValue:strName forKey:@"name"];
    }
    
    if (dicFavo[@"card_id"]) {
        [dicPost setValue:dicFavo[@"card_id"] forKey:@"card"];
    }
    
    if (dicFavo[@"legend"]) {
        [dicPost setValue:dicFavo[@"legend"] forKey:@"legend"];
    }
    
    if (dicFavo[@"iSharing"]) {
        [dicPost setValue:@{@"share":dicFavo[@"iSharing"]} forKey:@"sharing"];
    }
    
    if (dicFavo[@"sharingHuntsArray"]) {
        arrHunt =[self convertArray2ArrayID:dicFavo[@"sharingHuntsArray"] forGroup:NO];
        [dicPost setValue:arrHunt forKey:@"hunts"];
    }
    
    if (dicFavo[@"sharingGroupsArray"]) {
        arrGroup= [self convertArray2ArrayID:dicFavo[@"sharingGroupsArray"] forGroup:YES];
        [dicPost setValue:arrGroup forKey:@"groups"];
    }
    if (dicFavo[@"sharingUsers"]) {
        arrGroup= [self convertPersonneArray2ArrayID:dicFavo[@"sharingUsers"]];
        [dicPost setValue:arrGroup forKey:@"users"];
    }
    if (dicFavo[@"color"]) {
        [dicPost setValue:dicFavo[@"color"][@"id"] forKey:@"publicationcolor"];
    }
    
    //Default
    [dicPost setValue:@"0" forKey:@"specific"];
    
    if (dicFavo[@"observation"]) {
        if (dicFavo[@"observation"][@"attachments"]) {
            NSMutableDictionary *dicObservation = [dicFavo[@"observation"]  mutableCopy];
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{@"label": dic[@"label"],
                                       @"value": dic[@"value"]}];
            }
            [dicPost setValue:newAttach forKey:@"attachments"];
        }
        
        if (dicFavo[@"observation"][@"category"]) {
            [dicPost setValue:dicFavo[@"observation"][@"category"] forKey:@"category"];
        }
        if (dicFavo[@"observation"][@"card"]) {
            [dicPost setValue:dicFavo[@"observation"][@"card"] forKey:@"card"];
        }
        if (dicFavo[@"observation"][@"animal"]) {
            [dicPost setValue:dicFavo[@"observation"][@"animal"] forKey:@"animal"];
        }
        if (dicFavo[@"observation"][@"specific"]) {
            [dicPost setValue:dicFavo[@"observation"][@"specific"] forKey:@"specific"];
        }
    }
    
    [COMMON addLoading:self];
    
    [serviceObj fnPOST_PUBLICATION_FAVORITES:@{@"favorite":dicPost}];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        
        //reset
        [COMMON removeProgressLoading];
        
        if ([response[@"favorite"] isKindOfClass:[NSDictionary class]]) {
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];
                    
                }
            }];
            
            [KSToastView ks_showToast: @" Post favorite successful!!!" duration:2.0f completion: ^{
            }];
            
        }
        
        
    };
}
-(NSArray*) convertArray2ArrayID:(NSArray*)arr forGroup:(BOOL) isGroup
    {
        
        NSMutableArray *array = [NSMutableArray new];
        
        for (NSDictionary*dic in arr) {
            
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                if (isGroup) {
                    [array addObject:dic[@"groupID"]];
                    
                }else{
                    [array addObject:dic[@"huntID"]];
                }
            }
        }
        return array;
    }
-(NSArray*) convertPersonneArray2ArrayID:(NSArray*)arr
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
//        NSNumber *num = dic[@"status"];
//        if ( [num boolValue]) {
            [array addObject:dic[@"id"]];
//        }
    }
    return array;
}
-(IBAction)chooseColorAction:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initChooseColor ];
    [vc doBlockColor:^(NSInteger index, NSString* strTmp,NSString *strColor) {
        NSString *trimmedString = [strTmp stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceCharacterSet]];
        [PublicationOBJ sharedInstance].legend = trimmedString;
            [PublicationOBJ sharedInstance].publicationcolor = strColor;

    }];
    [vc fnSetexpectTarget:self.expectTarget withColor:self.colorNavigation];
    [vc showAlert];
}
#pragma mark - SELECT MEDIA

- (IBAction)onClickPhoto:(id)sender {
    
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_PHOTO withParent:self target:self.expectTarget];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_PHOTO;
            
//            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
//            UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
            
            
            [PublicationOBJ sharedInstance].urlPhoto = data[@"image"];
//            if (data[@"Latitude"] != nil) {
//                [PublicationOBJ sharedInstance].latitude = [data[@"Latitude"] stringValue];
//                [PublicationOBJ sharedInstance].longtitude = [data[@"Longitude"] stringValue];
//            }else{
//                [PublicationOBJ sharedInstance].latitude = nil;
//                [PublicationOBJ sharedInstance].longtitude = nil;
//            }

        }
    }];
    
    if (self.autoShow == TYPE_PHOTO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    
    self.autoShow = TYPE_RESET;
    
}

- (IBAction)onClickVideo:(id)sender {
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_VIDEO withParent:self target:self.expectTarget];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_VIDEO;
            
            [PublicationOBJ sharedInstance].urlVideo = data[@"videourl"];
            
//            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: [NSURL fileURLWithPath: data[@"videourl"] ] options:nil];
//            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
//            generate1.appliesPreferredTrackTransform = YES;
//            NSError *err = NULL;
//            CMTime time = CMTimeMake(1, 2);
//            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
//            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
        }
    }];
    if (self.autoShow == TYPE_VIDEO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    self.autoShow = TYPE_RESET;
}
//MARK: - MAPVIEW DELEGATE

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = mapView.camera.target.latitude;
    float lng = mapView.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    [PublicationOBJ sharedInstance].latitude =strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;
    [PublicationOBJ sharedInstance].address = self.strAddress;
    [PublicationOBJ sharedInstance].location = self.strAddress;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(fnGetAddress) withObject:nil afterDelay:1];
}
-(void)fnGetAddress
{
    if(strLatitude.length == 0 && strLongitude.length == 0)
    {
        return;
    }
    __weak typeof(self) wself = self;
    if ([COMMON isReachable])
    {
        [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
            
            //i got the address
            [wself fnSetAddress:mAddress];
            
        } withData:@{@"latitude" : strLatitude,
                     @"longitude" : strLongitude}];
    }
}
-(void)fnSetAddress:(NSString*)mAddress
{
    //i got the address
    self.strAddress = mAddress;
    self.cellMap.tfInput.text = self.strAddress;
    [PublicationOBJ sharedInstance].address = self.strAddress;
    [PublicationOBJ sharedInstance].location = self.strAddress;
    //    [self.tableControl reloadData];
}
//MARK: - TextView Delegate
-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    UIView *parent = [a superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    int remain = (int) (limitPublication - (a.text.length + c.length));
    if (remain < 0) {
        NSString *strFullText = [a.text stringByAppendingString:c];
        NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitPublication)];
        a.text = subString;
        [cell.numberRemain setTextColor:[UIColor redColor]];
        [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
    }
    return remain >= 0;
}

- (void)textViewDidChange:(UITextView *)textView
{
    UIView *parent = [textView superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    int remain = (int)textView.text.length;
    [cell.numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitPublication:remain,limitPublication]];
    
    // Check if the count is over the limit
    if(remain >= limitPublication - 10) {
        // Change the color
        [cell.numberRemain setTextColor:[UIColor redColor]];
    }
    else if(remain > limitPublication - 50 && remain < limitPublication - 10) {
        // Change the color to yellow
        [cell.numberRemain setTextColor:[UIColor orangeColor]];
    }
    else {
        // Set normal color
        [cell.numberRemain setTextColor:[UIColor darkGrayColor]];
    }
    self.strTitle = textView.text;
}
-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    
    NSString *strsearchValues= searchText.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    searchText.text=strsearchValues;
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:
            
        {
            [self processLoungesSearchingURL:strsearchValues];
        }
            break;
            
        default:
            {
                self.strTitle = strsearchValues;
            }
            break;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *strsearchValues= textField.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    textField.text=strsearchValues;
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:
            
        {
            [self processLoungesSearchingURL:strsearchValues];
        }
            break;
            
        default:
        {
            self.strTitle = strsearchValues;
        }
            break;
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSDictionary *dicData = self.arrData[indexPath.row];
    typeSelected = [dicData[@"type"] intValue];
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        case FILED_PARTAGE:

        {
            CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
            CGRect rectTf = cell.viewInput.frame;
            
            CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
            [self createSuggestViewWithSuperView:self.tableControl withRect:rectSubView withIndexPath:indexPath];
            [self.tableControl bringSubviewToFront:cell.viewInput];
            
        }
            break;

        default:
            break;
    }

    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [self removeSuggestView];
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        {
            self.strAddress = searchText;
            
            [suggestView processLoungesSearchingURL:searchText];
        }
            break;
        case FILED_PARTAGE:
        {
//            self.strPersonne = searchText;
            [suggestViewPersonne processLoungesSearchingURL:searchText];
        }
            break;
        default:
            break;
    }
}
-(void)createSuggestViewWithSuperView:(UIView*)view withRect:(CGRect)rect withIndexPath:(NSIndexPath*)indexPath
{
    switch (typeSelected) {
        case FIELD_GEO_SEARCH:
        {
            [suggestView fnDataInput:self.strAddress];
            [suggestView doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
                [self resignKeyboard:nil];
                if (m_Placemark) {
                    my_Placemark = m_Placemark;
                    self.strAddress = addressString;
                    
                    float lat = m_Placemark.coordinate.latitude;
                    float lng = m_Placemark.coordinate.longitude;
                    
                    [PublicationOBJ sharedInstance].address = self.strAddress ;
                    [PublicationOBJ sharedInstance].latitude =[NSString stringWithFormat:@"%f",lat];
                    [PublicationOBJ sharedInstance].longtitude = [NSString stringWithFormat:@"%f",lng];
                    
                    [self.tableControl reloadData];
                }
                
            }];
            [suggestView showAlertWithSuperView:view withRect:rect];
        }
            break;
        case FILED_PARTAGE:
        {
//            [suggestViewPersonne fnDataInput:self.strPersonne withArrSelected:arrPersonne];
//            [suggestViewPersonne doBlock:^(NSArray *arrResult) {
//                [self resignKeyboard:nil];
//                [arrPersonne removeAllObjects];
//                if(arrResult)
//                {
//                    [arrPersonne addObjectsFromArray:arrResult];
//                }
//                [self.tableControl reloadData];
//            }];
//            [suggestViewPersonne showAlertWithSuperView:view withRect:rect];
            
        }
            break;
        default:
            break;
    }
    
}
-(void)updateSuggestView:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    
    [suggestView showAlertWithSuperView:self.tableControl withRect:rectSubView];
}
-(void)removeSuggestView
{
    if([suggestViewPersonne isDescendantOfView:self.tableControl]) {
        [suggestViewPersonne closeAction:nil];
    }
    if([suggestView isDescendantOfView:self.tableControl]) {
        [suggestView closeAction:nil];
    }
}
//MARK:- searBar
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    [self removeSuggestView];
    
}
- (void)keyboardWillHide:(NSNotification*)notification {
    [self removeSuggestView];
}
-(IBAction)annulerAction:(id)sender
{
    [self doRemoveObservation];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onNext:(id)sender {
    [PublicationOBJ sharedInstance].publicationTxt = self.strTitle;
   [PublicationOBJ sharedInstance].mediaType = self.mediaType;

    if ([PublicationOBJ sharedInstance].isEditer) {
        [PublicationOBJ sharedInstance].iShare =MDshare;
        [PublicationOBJ sharedInstance].sharingGroupsArray = [[PublicationOBJ sharedInstance] convertArray:sharingGroupsArray forGroup:YES];
        [PublicationOBJ sharedInstance].sharingHuntsArray = [[PublicationOBJ sharedInstance] convertArray:sharingHuntsArray forGroup:NO];
        
        NSMutableArray *arrUser =[NSMutableArray new];
        
        if (arrPersonne) {
            for (NSDictionary *dic in arrPersonne) {
                NSNumber *num = dic[@"status"];
                if ( [num boolValue]) {
                    [arrUser addObject: [NSString stringWithFormat:@"%@", dic[@"id"]]];
                    
                }
            }
            [PublicationOBJ sharedInstance].arrsharingUsers = arrUser;
        }
        
        [[PublicationOBJ sharedInstance]  modifiPosterAll:self];
    }
    else{
        //
        if (self.isEditFavo) {
            //favo
            NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
            NSMutableArray *arrGroup =[NSMutableArray new];
            
            if (sharingGroupsArray) {
                for (NSDictionary *dic in sharingGroupsArray) {
                    NSNumber *num = dic[@"isSelected"];
                    if ( [num boolValue]) {
                        [arrGroup addObject:@{@"id":dic[@"groupID"],@"name":dic[@"categoryName"]}];
                    }
                }
                [dicFavo setValue:arrGroup forKey:@"groups"];
            }
            NSMutableArray *arrHunt =[NSMutableArray new];
            if (sharingHuntsArray) {
                for (NSDictionary *dic in sharingHuntsArray) {
                    NSNumber *num = dic[@"isSelected"];
                    if ( [num boolValue]) {
                        [arrHunt addObject:@{@"id":dic[@"huntID"],@"name":dic[@"categoryName"]}];
                        
                        
                    }
                }
                [dicFavo setValue:arrHunt forKey:@"hunts"];
            }
            NSMutableArray *arrUser =[NSMutableArray new];
            if (arrPersonne) {
                for (NSDictionary *dic in arrPersonne) {
                    NSNumber *num = dic[@"status"];
                    if ( [num boolValue]) {
                        [arrUser addObject:dic];
                        
                    }
                }
                [dicFavo setValue:arrUser forKey:@"sharingUsers"];
            }
            [dicFavo setValue:[NSNumber numberWithInt:MDshare] forKey:@"iSharing"];
            [dicFavo setValue:@{@"share":[NSNumber numberWithInt:MDshare]} forKey:@"sharing"];
            [dicFavo setValue:arrReceivers forKey:@"receivers"];
            [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
            
            [self gotoback];
            
        }
        else
        {

            NSMutableDictionary *dicUpload = [@{@"iSharing":[NSNumber numberWithInt:MDshare]} mutableCopy];
            if (sharingGroupsArray) {
                [dicUpload setObject:sharingGroupsArray forKey:@"sharingGroupsArray"];
            }
            if (sharingHuntsArray) {
                [dicUpload setObject:sharingHuntsArray forKey:@"sharingHuntsArray"];
            }
            if (arrReceivers) {
                [dicUpload setObject:arrReceivers forKey:@"receivers"];
            }
            NSMutableArray *arrTmp = [NSMutableArray new];
            for (NSDictionary *dicPersonne in arrPersonne) {
                [arrTmp addObject:dicPersonne[@"id"]];
            }
            NSString *strUser = @"";
            if (arrTmp.count > 0) {
                strUser = [arrTmp componentsJoinedByString:@","];
            }
            if (strUser) {
                [dicUpload setObject:strUser forKey:@"sharingUser"];
            }
            [[PublicationOBJ sharedInstance] uploadData:dicUpload];
            
            
            //back to Live Hunt...If create from LiveHunt MapGlobalVC
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
//            for (UIViewController *controller in controllerArray)
//            {
//                //Code here.. e.g. print their titles to see the array setup;
//                
//                    if ([controller isKindOfClass: [MapGlobalVC class]]) {
//                        [self.navigationController popToViewController:controller animated:NO];
//                        return;
//                    }
//            }
            //back to Mur...if create from MUR.
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass: [self.mParent class]]) {
                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }

            //Back to Dashboard
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
        
    }
}

-(IBAction)extendMap:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initAlertMap];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        [self.tableControl reloadData];
    }];
    [vc fnSetexpectTarget:self.expectTarget withColor:self.colorNavigation];
    [vc showAlert];

}
-(IBAction)preciserAction:(id)sender
{
    /*
    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
    
    NSArray *listTree = treeDic[@"default"];
    NSMutableDictionary *dicTmp = [NSMutableDictionary new];
    //mld_type
    for (NSDictionary *dic in listTree) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            dicTmp = [dic mutableCopy];
            break;
        }
    }
    Signaler_ChoixSpecNiv2 *viewController1 = [[Signaler_ChoixSpecNiv2 alloc] initWithNibName:@"Signaler_ChoixSpecNiv2" bundle:nil];
    //EG..
    viewController1.myName = dicTmp[@"name"];
    viewController1.myDic = dicTmp;
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    */

    Signaler_ChoixSpecNiv1 *viewController1 = [[Signaler_ChoixSpecNiv1 alloc] initWithNibName:@"Signaler_ChoixSpecNiv1" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    
}
-(IBAction)formulaireAction:(id)sender
{
    Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
    viewController1.modifiAttachment = YES;
    viewController1.myDic = childCategory;
    viewController1.iSpecific =  [childCategory[@"search"] intValue] ;
    if ([childCategory[@"search"] boolValue] && [[PublicationOBJ sharedInstance] getCacheSpecific_Cards].count > 0) {
        viewController1.myCard = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
        viewController1.id_animal = [childCategory[@"id"] stringValue];
    }
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
    
}
//MARK: - PARTAGE
- (IBAction)partageAction:(id)sender {
    Signaler_Choix_Partage *viewController1 = [[Signaler_Choix_Partage alloc] initWithNibName:@"Signaler_Choix_Partage" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
    
}
//MARK: - SWITCH
- (IBAction)doSwitch:(id)sender {
    
    UISwitch *ui = (UISwitch*)sender;
    if (editHaveGeo) {
        [PublicationOBJ sharedInstance].enableGeolocation = editHaveGeo;
    }
    else
    {
        [PublicationOBJ sharedInstance].enableGeolocation = ui.isOn;
    }
    [self.tableControl reloadData];
}
//MARK: - share extentions
- (void) imageShareExtentionWithInfo:(NSDictionary*)dicInfo
{
    NSString *contentImage = [NSString stringWithFormat:@"%@",@"image"];
    if ([dicInfo[@"type"] isEqualToString:contentImage])
    {
        self.mediaType = TYPE_PHOTO;
        NSString *strName = dicInfo[@"url"];
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
        
        NSURL *url = [NSURL fileURLWithPath:photoFile];
        NSData *dataInfo = [NSData dataWithContentsOfURL:url];
        
        UIImage *tmpimage = [UIImage imageWithData:dataInfo];
        
        //        [self saveImage:tmpimage withInfo:nil];
        
        AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        CLLocation * loc = app.locationManager.location;
        
        NSDictionary *retDic = nil;
        
        if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
            retDic = @{@"image":strName,
                       @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                       @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
        }else{
            retDic =@{@"image":strName};
        }
        [PublicationOBJ sharedInstance].urlPhoto = retDic[@"image"];
        if (retDic[@"Latitude"] != nil) {
            [PublicationOBJ sharedInstance].latitude = [retDic[@"Latitude"] stringValue];
            [PublicationOBJ sharedInstance].longtitude = [retDic[@"Longitude"] stringValue];
        }else{
            [PublicationOBJ sharedInstance].latitude = nil;
            [PublicationOBJ sharedInstance].longtitude = nil;
            
        }
        
    }
    else {
        
        self.mediaType = TYPE_VIDEO;
        NSString *moviePath = dicInfo[@"url"];
        [PublicationOBJ sharedInstance].urlVideo = moviePath;
    }
}
@end
