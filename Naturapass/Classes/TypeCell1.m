//
//  MesSalonCustomCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 3/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "TypeCell1.h"
#import "CommonHelper.h"
#import "MDStatusButton.h"
@implementation TypeCell1
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    [self.Img1.layer setMasksToBounds:YES];
    self.Img1.layer.cornerRadius= 30;
    self.Img1.layer.borderWidth =0;
    
    
    [self.view1.layer setMasksToBounds:YES];
    self.view1.layer.cornerRadius= 5.0;
    self.view1.layer.borderWidth =0.5;
    self.view1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    UIView *tmpV = [self viewWithTag:POP_MENU_VIEW_TAG2];
    tmpV.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    [self.imgBackGroundSetting setImage:[UIImage imageNamed:@"chasse_bg_setting"]];
    [self.imgSettingSelected setImage:[UIImage imageNamed:@"ic_admin_setting"]];
    [self.imgSettingNormal setImage:[UIImage imageNamed:@"ic_chasse_setting_active"]];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            [self.button1 setBackgroundImage:[UIImage imageNamed:@"btn_blue_bg"] forState:UIControlStateNormal];
            [self.button2 setBackgroundImage:[UIImage imageNamed:@"btn_blue_bg"] forState:UIControlStateNormal];
            self.Img6.image =[UIImage imageNamed:@"ic_close"];
            [self.button1 setTitle:@"PARTICIPANTS" forState:UIControlStateNormal];
            [self.Img7 setHidden:YES];
            [self.button3 setHidden:YES];
            [self.view3 setHidden:YES];
            [self.view4 setHidden:YES];
            [self.Img4 setHidden:YES];

            self.constraintHeight.constant =0;
            self.constraintHeight2.constant =0;
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            [self.button2 setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            [self.label12 setTextColor:UIColorFromRGB(BUTTON_DESINSCRIRE_COLOR)];

            [self.Img7 setHidden:NO];
            [self.button3 setHidden:NO];
            [self.view3 setHidden:NO];
            [self.view4 setHidden:NO];
            self.constraintHeight.constant =23;
            self.constraintHeight2.constant =20;
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            [self.button2 setBackgroundColor:UIColorFromRGB(BUTTON_DESINSCRIRE_COLOR)];

            [self.Img7 setHidden:YES];
            [self.button3 setHidden:YES];
            [self.view3 setHidden:YES];
            [self.view4 setHidden:YES];
            
            self.constraintHeight.constant =0;
            self.constraintHeight2.constant =0;
            
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
//            [self.button2 setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            self.Img6.image =[UIImage imageNamed:@"ic_close"];
            [self.Img7 setHidden:YES];
            [self.button3 setHidden:YES];
            [self.view3 setHidden:YES];
            [self.view4 setHidden:YES];
            
            self.constraintHeight.constant =0;
            self.constraintHeight2.constant =0;
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
    [self.label3 setTextColor: color];
    [self.label9 setTextColor:color];
    
    [self.button1 setBackgroundColor:color];
//    [self.button3 setBackgroundColor:color];
}
@end
