//
//  Home_News.h
//  Naturapass
//
//  Created by Giang on 9/23/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Home_News : UIView

@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *myContent;
@property (weak, nonatomic) IBOutlet UIButton *btnClick;
@end
