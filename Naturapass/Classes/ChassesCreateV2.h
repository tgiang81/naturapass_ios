//
//  ChassesCreateV2.h
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateBaseVC.h"

@interface ChassesCreateV2 : ChassesCreateBaseVC

@property (assign) BOOL isModify;
@property (nonatomic, strong) NSMutableArray *arrData;

@property (nonatomic,strong) IBOutlet UIView *viewFooter;

@property (nonatomic,strong) IBOutlet UIView *viewNom;
@property (nonatomic,strong) IBOutlet UITextField *tfNom;
@property (nonatomic,strong) IBOutlet UIView *viewBottom;
@property (nonatomic,strong) IBOutlet UIView *viewTop;
@property (nonatomic,strong) IBOutlet UIButton *btnSAUVEGARDER;
@property (nonatomic,strong) IBOutlet UIButton *btnENVOYER;

@property (nonatomic,strong) IBOutlet UIImageView *imgPhotoSelected;
@property (nonatomic,strong) IBOutlet UIImageView *imgClosePhoto;
@property (nonatomic,strong) IBOutlet UIButton *btnClosePhoto;
@property (nonatomic, strong) UIDatePicker       *datePicker;

@end
