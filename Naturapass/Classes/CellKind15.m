//
//  CellKind15.m
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "CellKind15.h"

@implementation CellKind15

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.imgProfile.layer setMasksToBounds:YES];
    self.imgProfile.layer.cornerRadius= 30;
    self.imgProfile.layer.borderWidth =0;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.nameGroup.preferredMaxLayoutWidth = CGRectGetWidth(self.nameGroup.frame);

    self.contentGroup.preferredMaxLayoutWidth = CGRectGetWidth(self.contentGroup.frame);
    self.lbNameGroupAdmin.preferredMaxLayoutWidth = CGRectGetWidth(self.lbNameGroupAdmin.frame);

}

@end
