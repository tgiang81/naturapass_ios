//
//  AmisAddVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisAddVC.h"

#import "CellKind2.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "AKTagsInputView.h"
#import "AddressAmisCell.h"
#import "MDCheckBox.h"
#import "AmisAddScreen2.h"

#import "Amis_Demand_Invitation.h"
#import "AmisAddScreen1.h"
#import "AmisSearchVC.h"
#import "AmisAddContent.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface AmisAddVC ()
{
    NSMutableArray *arrFriendList;
    AKTagsInputView * mtokenInputView;
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescriptionScreen;

}

@property (strong, nonatomic) IBOutlet UIView *viewContent;

@property (strong, nonatomic) IBOutlet UIView *tokenInputView;
@property (strong, nonatomic) IBOutlet UIButton *btnAjouter;

@property (strong, nonatomic) NSMutableArray *contacts;

@property (strong, nonatomic) NSMutableArray *selectedContacts;

@end


@implementation AmisAddVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strINVITEZVOSAMIS);
    lblDescriptionScreen.text = str(strDescriptionAmisAddScreen);
    [self.btnAjouter setTitle:str(strValider_votre_liste) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.

    //[_tagsInputView.selectedTags componentsJoinedByString:@", "]
    
    [self.btnAjouter setBackgroundImage:[UIImage imageNamed:@"ajouter-suivant-backgroundbutton-color"] forState:UIControlStateSelected];
    [self.btnAjouter setBackgroundImage: [UIImage imageNamed:@"ajouter-suivant-backgroundbutton-Gray"] forState:UIControlStateNormal];
    [self fnTextPeoples];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AddressAmisCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
}

-(void)fnTextPeoples
{
    mtokenInputView = [[AKTagsInputView alloc] initWithFrame:self.tokenInputView.frame];
    
    mtokenInputView.backgroundColor = [UIColor redColor];//UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
    
    mtokenInputView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    mtokenInputView.lookupTags = nil;//@[@""];
    mtokenInputView.enableTagsLookup = NO;
    [self.viewContent addSubview: mtokenInputView ];
    if (self.filteredContacts.count==0) {
        mtokenInputView.selectedTags = nil;
        self.btnAjouter.selected=NO;
    }else if (self.filteredContacts.count==1) {
        mtokenInputView.selectedTags = [NSMutableArray arrayWithArray:@[self.filteredContacts[0][@"email"]]];
        self.btnAjouter.selected=YES;

    }else{
        mtokenInputView.selectedTags = [NSMutableArray arrayWithArray:@[self.filteredContacts[0][@"email"],[NSString stringWithFormat:@"_+ %d %@",(int)[self.filteredContacts count]-1,str(strOther_peoples)]]];
        self.btnAjouter.selected=YES;

    }
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.filteredContacts.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressAmisCell *cell = nil;
    
    cell = (AddressAmisCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = self.filteredContacts[indexPath.row];
    if ([dic[@"image"] isKindOfClass:[UIImage class]]) {
        cell.imageIcon.image = dic[@"image"];
    }
    else
    {
        cell.imageIcon.image = nil;

    }
    [cell.imageIcon.layer setMasksToBounds:YES];
    cell.imageIcon.layer.cornerRadius= 30;
    
    cell.label1.text =dic[@"name"];
    cell.label2.text =dic[@"email"];
    
    cell.label1.numberOfLines=1;
    //ic_remove_contact
    cell.btnCheck.tag=indexPath.row+100;
    
    [cell.viewControl setSelected:YES];
    [cell.btnCheck addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.viewControl setTypeCheckBox:UI_AMIS_DELETE];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
-(IBAction)deleteAction:(id)sender
{
    NSInteger index =[sender tag] -100;
    [self.filteredContacts removeObjectAtIndex:index];
    [self.tableControl reloadData];
    [self fnTextPeoples];
}
- (IBAction)envoyerAction:(id)sender
{
    if (!self.btnAjouter.selected) {
        return;
    }
   self.listEmails =[NSMutableArray new];
    for (NSDictionary *dic in self.filteredContacts) {
        if ([dic[@"check"] boolValue]) {
            [self.listEmails addObject:dic];
        }
    }

    
    NSMutableArray *mutDic = [NSMutableArray new];
    
    for (NSDictionary*dic in self.listEmails) {
        [mutDic addObject:dic[@"email"] ];
    }
    
    if (mutDic.count > 0) {
        AmisAddContent *viewController1 = [[AmisAddContent alloc] initWithNibName:@"AmisAddContent" bundle:nil];
        viewController1.listEmails = mutDic;
        [self pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:NO];
        
    }
    
    
}
@end
