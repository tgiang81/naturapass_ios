//
//  TheProjectCell.h
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface PublicationControlCheckboxCell :UITableViewCell
@property (retain, nonatomic) IBOutlet UILabel *lbTittle;
@property (retain, nonatomic) IBOutlet UIButton *btnCheckBox;
@end
