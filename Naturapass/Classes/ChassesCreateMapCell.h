//
//  ChassesCreateMapCell.h
//  Naturapass
//
//  Created by JoJo on 8/13/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChassesCreateBaseCell.h"

@interface ChassesCreateMapCell : ChassesCreateBaseCell
{

    __weak IBOutlet UIView *vContour;
}
@end
