//
//  MurParameter_Publication.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Parameter_Publication.h"
#import "Parameter_BlackListVC.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_Publication ()
{
    NSArray * arrData;
    int filterControl;
    int iSharing;
    NSMutableArray              *mesArr;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation Parameter_Publication

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lbTitle.text = str(strGENERAL);
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
//    NSMutableDictionary *dic1 = [@{@"name": str(strPartage_par_defaut),
//                                   @"image":@"mur_st_ic_share"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strAmis_visible),
                                   @"image":@"mur_st_ic_amis"} copy];

    NSMutableDictionary *dic3 = [@{@"name":str(strMembres_blacklister),
                                   @"image":@"ic_blacklist_param"} copy];

    
    arrData =  [@[/*dic1,*/dic2,dic3] copy];
    
    filterControl =-1;
    iSharing =-1;
    
    [self getUserParametersFriendsAction];
    
    NSNumber *num = [[NSUserDefaults standardUserDefaults] valueForKey:@"CONFIGPARTAGE"];
    iSharing =[num intValue];
    [self getUserShare];
    
    //Filter
    NSMutableDictionary *dic6 = [@{@"categoryName":str(strMYFRIEND),
                                   @"categoryImage":@"mes2_icon"} copy];
    
    NSMutableDictionary *dic7 = [@{@"categoryName":str(strMEMBER),
                                   @"categoryImage":strIcon_All_Member} copy];
    
    mesArr =  [@[dic6,dic7] copy];
}

#pragma mark - API
-(void)getUserParametersFriendsAction
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    WebServiceAPI *serverAPI =[WebServiceAPI new];
    [serverAPI getUserParametersFriendsAction];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        //170:      *      "friend": [0 => OFF, 1 => ON]
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (response) {
           filterControl= [response[@"friend"] intValue];
            [self.tableControl reloadData];
        }
    };
}

-(void)putUserParametersFriendsAction
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    NSDictionary *putDic = @{@"friend":[NSNumber numberWithInt:1-filterControl] };
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    [serverAPI putUserParametersFriendsActionWithParametersDic:putDic];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

        if (response) {
            if ([response[@"success"] boolValue]) {
                filterControl= [response[@"friend"] intValue];
            }
        }
        [self.tableControl reloadData];
    };
}
-(void)getUserShare
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    [serverAPI getUserParam];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        //170:      *      "friend": [0 => OFF, 1 => ON]
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (response) {
            [[NSUserDefaults standardUserDefaults]setValue: [NSNumber numberWithInt:[response[@"sharing"] intValue] ] forKey:@"CONFIGPARTAGE"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            iSharing =[response[@"sharing"] intValue];
            [self.tableControl reloadData];
        }
    };
}
-(void)putUserParameterAction
{
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    NSString*  strTag=[NSString stringWithFormat:@"%ld",(long)iSharing];
    NSDictionary *postDict=@{@"parameters":
                                 @{@"publication_sharing":
                                       @{@"share":strTag}}};
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj putUserParameterAction:@"" withParametersDic:postDict];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                
                if (!response) {
                    return ;
                }
                if([response isKindOfClass: [NSArray class] ]) {
                    return ;
                }
                
//                ASLog(@"response=%@",response);
                
                [[NSUserDefaults standardUserDefaults]setValue: [NSNumber numberWithInt:(int)iSharing ] forKey:@"CONFIGPARTAGE"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self.tableControl reloadData];

            });
        };
    
}

-(void)selectTickAction:(id)sender{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[CellKind2 class]]) {
        parent = parent.superview;
    }
    
    CellKind2 *cell = (CellKind2 *)parent;
    CGRect offset = self.tableControl.bounds;
    offset.origin.y+=48;
    // Hide already showing popover
    [cell show:self.tableControl offset:offset];
    [cell.menuPopover fnColorTheme:UIColorFromRGB(PARAM_MAIN_BAR_COLOR)];
    __weak Parameter_Publication *wself =self;
    [cell setCallBackGroup:^(NSInteger ind)
     {
             switch (ind) {
                 case 0:
                     iSharing = 1;
                     break;
                 case 1:
                     iSharing = 3;
                     break;
                 default:
                     break;
             }
         [wself putUserParameterAction];
     }];

}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.label1.numberOfLines=2;
    [cell layoutIfNeeded];
    
    //Control
    cell.constraint_image_width.constant = cell.constraint_image_height.constant = 24;

    switch (indexPath.row) {
            /*
        case 0:
        {
            cell.constraint_control_width.constant = 100;
            cell.constraintRight.constant = 5;
            cell.constraint_control_height.constant = 30;
            
            
            //Arrow
            if (iSharing>=0)
            {
                UIButton *btnFilterControl = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant)] ;
                btnFilterControl.tag =indexPath.row+100;
                
                if (iSharing==1)
                {
                    [btnFilterControl setTitle:mesArr[0][@"categoryName"] forState:UIControlStateNormal];
                }else if (iSharing==3) {
                    [btnFilterControl setTitle:mesArr[1][@"categoryName"] forState:UIControlStateNormal];
                    
                }

                [btnFilterControl setTintColor:[UIColor whiteColor]];
                [btnFilterControl setBackgroundColor:UIColorFromRGB(PARAM_MAIN_BAR_COLOR)];
                [btnFilterControl addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
                [btnFilterControl.titleLabel setFont:FONT_HELVETICANEUE_MEDIUM(13)];
                [cell.viewControl addSubview:btnFilterControl];
            }
            [cell createMenuList:@[mesArr[0][@"categoryName"], mesArr[1][@"categoryName"]]];
        }
            break;
            */
        case 0:
        {
            cell.constraint_control_width.constant = 60;
            cell.constraintRight.constant = 5;
            cell.constraint_control_height.constant = 30;
            UISwitch *btnFilterControl = (UISwitch*)[cell.viewControl viewWithTag:100];
            if (!btnFilterControl) {

                
                btnFilterControl = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant)] ;
                btnFilterControl.tag = 100;
                [btnFilterControl addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
                [cell.viewControl addSubview:btnFilterControl];
            }
            //Arrow
            [btnFilterControl setOnTintColor:UIColorFromRGB(ON_SWITCH_PARAM)];
            btnFilterControl.transform = CGAffineTransformMakeScale(0.75, 0.75);
            //Status
            [btnFilterControl setOn:filterControl];


        }
            break;
        case 1:
        {
        }
            break;
            
        default:
            break;
    }
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 1) {
        Parameter_BlackListVC  *viewController1 = [[Parameter_BlackListVC alloc] initWithNibName:@"Parameter_BlackListVC" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISPARAMTRES];

    }
}

- (IBAction)switchValueChanged:(id)sender
{
    [self putUserParametersFriendsAction];
}


@end
