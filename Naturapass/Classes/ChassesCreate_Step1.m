//
//  ChassesCreate_Step1.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step1.h"
#import "ChassesCreate_Step2.h"
#import "ChassesCreateOBJ.h"
#import "PhotoSheetVC.h"
#import "Config.h"
#import "AppCommon.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ASSharedTimeFormatter.h"
#import "ImageWithCloseButton.h"
#import "SAMTextView.h"
#import "ImageWithCloseButton.h"
#import "DPTextField.h"
#import "NSDate+Extensions.h"
#import "ChassesCreate_Publication_Discussion_Allow.h"
#import "AlertVC.h"
#define FIELDS_COUNT                    3
@interface ChassesCreate_Step1 ()<UITextFieldDelegate,UITextViewDelegate,WebServiceAPIDelegate>
{
    IBOutlet UITextField *textfieldName;
    
    IBOutlet SAMTextView *textviewComment;
    
    IBOutlet UILabel *labelPlaceHolder;
    IBOutlet UILabel *lblTitleScreen;

    UIToolbar *keyboardToolBar;
    
}
@property (weak, nonatomic) IBOutlet DPTextField *textfieldFin;
@property (weak, nonatomic) IBOutlet DPTextField *textfieldDebut;


@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPhotoSelected;
@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
//setcolor
@property (strong, nonatomic)   IBOutlet UIImageView *imgPhoto;
@property (strong, nonatomic)   IBOutlet UIImageView *img1;
@property (strong, nonatomic)   IBOutlet UIImageView *img2;
@property (strong, nonatomic)   IBOutlet UIImageView *img3;
@property (strong, nonatomic)   IBOutlet UIImageView *img4;
@property (strong, nonatomic)   IBOutlet UIImageView *img5;
@property (strong, nonatomic)   IBOutlet UIButton *btnINSERR;
@property (strong, nonatomic)   IBOutlet UIButton *btnENSAVOIRPLUS;
@end

@implementation ChassesCreate_Step1

#pragma mark -setColor
-(void)fnColorTheme
{
    [_imgPhoto setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    [_img1 setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
//    [_img4 setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    [self.btnSuivant setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    [_btnINSERR setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    [_btnENSAVOIRPLUS setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];

}
#pragma mark - viewdid
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strINFORMATIONS);
    [_btnENSAVOIRPLUS setTitle: str(strENSAVOIRPLUS) forState:UIControlStateNormal];

    textfieldName.placeholder = str(strNom);
    _textfieldFin.placeholder = str(strDateDeFin);
    _textfieldDebut.placeholder = str(strDateDeDebut);
    [self.btnSuivant setTitle:  str(strSUIVANT) forState:UIControlStateNormal];

    textviewComment.placeholder = str(strDescriptionPlaceholder);
    
    [self fnColorTheme];
    [self InitializeKeyboardToolBar];
    
    [textviewComment setInputAccessoryView:self.keyboardToolbar];
    //Photo
    self.viewPhotoSelected.hidden=YES;
    [self.viewPhotoSelected setMyCallback: ^(){
        //Close
        self.viewPhotoSelected.hidden=YES;
        self.viewPhotoSelected.imageContent.image = nil;
    }];
    __weak ChassesCreate_Step1 *wkVC = self;
    
    
    [self.textfieldDebut setCallBack:^(){

        //Auto set for textField Fin if empty
        if (wkVC.textfieldFin.text.length >0) {
            if([wkVC.textfieldFin.datePicker.date compare: wkVC.textfieldDebut.datePicker.date] == NSOrderedAscending) // if start is later in time than end
            {
                wkVC.textfieldFin.datePicker.date = wkVC.textfieldDebut.datePicker.date;
                wkVC.textfieldFin.text = wkVC.textfieldDebut.text;
            }
        }
        else
        {
            wkVC.textfieldFin.datePicker.date = wkVC.textfieldDebut.datePicker.date;
            wkVC.textfieldFin.text = wkVC.textfieldDebut.text;
        }

    }];
    [self.textfieldFin setCallBack:^(){
        
        //Auto set for textField Fin if empty
        if (wkVC.textfieldDebut.text.length >0) {
            if([wkVC.textfieldFin.datePicker.date compare: wkVC.textfieldDebut.datePicker.date] == NSOrderedAscending) // if start is later in time than end
            {
                wkVC.textfieldDebut.datePicker.date = wkVC.textfieldFin.datePicker.date;
                wkVC.textfieldDebut.text = wkVC.textfieldFin.text;
            }
        }
        else
        {
            wkVC.textfieldDebut.datePicker.date = wkVC.textfieldFin.datePicker.date;
            wkVC.textfieldDebut.text = wkVC.textfieldFin.text;
        }
    }];

    //modifi
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
        ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
        textfieldName.text =obj.strName;
        textviewComment.text =obj.strComment;
        
        self.textfieldFin.text =obj.strFin;
        self.textfieldDebut.text =obj.strDebut;
        
        self.textfieldFin.delegate = self;
        self.textfieldFin.delegate = self;
        
        self.viewPhotoSelected.hidden=NO;
        self.viewPhotoSelected.imageContent.image = [UIImage imageWithData:obj.imgData];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}


-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)resignKeyboard:(id)sender
{
    [textviewComment resignFirstResponder];
    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(IBAction)hellpAction:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strAnnulerAgenda) message:str(strDescHelpAgenda) cancelButtonTitle:nil otherButtonTitles:str(strOK)];
    
    [vc doBlock:^(NSInteger index, NSString *str) {
        if (index==0) {
            // NON
        }
        else if(index==1)
        {
            //OUI
        }
    }];
    [vc showAlert];
}
#pragma mark - Goto Step2

-(IBAction)gotoStep2:(id)sender{
    
    if(![textfieldName.text isEqualToString:@""]&&![self.textfieldFin.text isEqualToString:@""]&&![self.textfieldDebut.text isEqualToString:@""]) {
        
        //Convert to GMT+2

        NSDateFormatter *dateFormatter = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
        
        NSString *strEndDate= self.textfieldFin.text;
        NSDate *finDate = [dateFormatter dateFromString: strEndDate];
        //
        NSString *strStartDate= self.textfieldDebut.text;
        NSDate *DebutDate = [dateFormatter dateFromString: strStartDate];

        /**/
        if([DebutDate compare: finDate] == NSOrderedDescending) // if start is later in time than end
        {
            self.textfieldFin.text = @"";
            // do something
            UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strAlertTimeIssue) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [salonAlert show];
            return;
        }

        //to UNICODE
        
        NSString *strName = [textfieldName.text emo_emojiString];
        NSString *strComment = [textviewComment.text emo_emojiString];

        [ChassesCreateOBJ sharedInstance].strName =strName;
        [ChassesCreateOBJ sharedInstance].strComment =strComment;
        
        [ChassesCreateOBJ sharedInstance].strFin = [dateFormatter stringFromDate:finDate];
        [ChassesCreateOBJ sharedInstance].strDebut = [dateFormatter stringFromDate:DebutDate];
        if (imageData) {
            [ChassesCreateOBJ sharedInstance].imgData =imageData;
        }
        
        if ([ChassesCreateOBJ sharedInstance].isModifi) {
            [[ChassesCreateOBJ sharedInstance] modifiChassesWithVC:self];
        }
        else
        {

//            ChassesCreate_Step2 *Step2 = [[ChassesCreate_Step2 alloc]initWithNibName:@"ChassesCreate_Step2" bundle:nil];
//            [self pushVC:Step2 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

            
            ChassesCreate_Publication_Discussion_Allow *viewController1 = [[ChassesCreate_Publication_Discussion_Allow alloc] initWithNibName:@"ChassesCreate_Publication_Discussion_Allow" bundle:nil];
            viewController1.myTypeView = PUBLICATION_ALLOW_AGENDA;
            
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            
        }

    }
    else {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
    }
}

- (IBAction)profileImageOptions:(id)sender{
    [textfieldName resignFirstResponder];
    [self.textfieldFin resignFirstResponder];
    [textviewComment resignFirstResponder];
    [self.textfieldDebut resignFirstResponder];
    
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.viewPhotoSelected.imageContent.image = imgData;
        
        self.viewPhotoSelected.hidden = NO;
        userImage = imgData;
        _imgPhoto.image = userImage;
        
        //resize image

        imageData = UIImageJPEGRepresentation([[AppCommon common] resizeImage:imgData targetSize:CGSizeMake(128, 128)], 1.0);
    }];
    
    [self presentViewController:vc animated:NO completion:^{}];
    
}

@end


