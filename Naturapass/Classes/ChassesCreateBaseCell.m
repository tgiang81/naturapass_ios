//
//  ChassesCreateBaseCell.m
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateBaseCell.h"
#import "Define.h"
#import "AlertVC.h"
#import "AppDelegate.h"
@implementation ChassesCreateBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewInput.layer setMasksToBounds:YES];
    self.viewInput.layer.cornerRadius= 2.0;
    self.tokenField.dataSource = self;
    self.tokenField.delegate = self;
    iZoomDefaultLevel = ZOOM_DEFAULT;
    self.mapView_.mapType = kGMSTypeHybrid;//
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)tokenDeleteButtonPressed:(UIButton *)tokenButton
{
    NSUInteger index = [self.tokenField indexOfTokenView:tokenButton.superview];
    if (index != NSNotFound) {
        if (_callbackTokenDelete) {
            _callbackTokenDelete(index);
        }
    }
}
-(void)setCallbackTokenDelete:(tokenDeleteViewCallback)callback
{
    _callbackTokenDelete=callback;
}

-(void)doBlockTokenDelete:(tokenDeleteViewCallback ) cb
{
    _callbackTokenDelete = cb;
    
}
-(void)addPersone:(NSArray*)arr
{
    _tokens = arr;
    [self.tokenField reloadData];
}
#pragma mark - ZFTokenField DataSource

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 30;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return self.tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:nil options:nil];
    UIView *view = nibContents[0];
    UILabel *label = (UILabel *)[view viewWithTag:2];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    
    [button addTarget:self action:@selector(tokenDeleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    label.text = self.tokens[index][@"fullname"];
    CGSize size = [label sizeThatFits:CGSizeMake(1000, 30)];
    view.frame = CGRectMake(0, 0, size.width + 50, 30);
    return view;
}
-(void)fnSetLocation:(NSString*)latitude withLongitude:(NSString*)longitude
{
    self.mapView_.mapType = kGMSTypeHybrid;//
    //From photo or has location data...
    if ( [latitude doubleValue] != 0 && [longitude doubleValue]!=0 )
    {
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [latitude doubleValue];
        simpleCoord.longitude= [longitude doubleValue];
        
        self.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        
    }else{
        if([self locationCheckStatusDenied])
        {
            [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
            return;
        }
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
        
    }
}
#pragma mark - locationManagerDelegates

-(BOOL)locationCheckStatusDenied
{
    if(![CLLocationManager locationServicesEnabled] ||
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strTitle_app) message:@"La localisation est inactive. Souhaitez-vous l'activer ?"  cancelButtonTitle:str(strAnuler) otherButtonTitles:str(strOK)];
        [vc doBlock:^(NSInteger index, NSString *str) {
            if (index==0) {
                // NON
            }
            else if(index==1)
            {
                //OUI
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }
        }];
        [vc showAlert];
        return YES;
    }
    return NO;
}

-(IBAction)fnTracking_CurrentPos:(id)sender
{
    if([self locationCheckStatusDenied])
    {
        [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
        return;
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:self.mapView_.camera.zoom]];
    
}

-(IBAction)fnIncrease
{    
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomIn];
    [self.mapView_ animateWithCameraUpdate:zoomCamera];
}

-(IBAction)fnDecrease
{
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomOut];
    [self.mapView_ animateWithCameraUpdate:zoomCamera];
}

@end
