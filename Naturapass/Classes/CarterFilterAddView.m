//
//  CarterFilterAddView.m
//  Naturapass
//
//  Created by Manh on 10/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "CarterFilterAddView.h"
#import "Filter_Cell_Type1.h"
#import "Filter_Cell_Type2.h"
#import "Filter_Cell_Type4.h"
#import "Filter_Cell_Type5.h"
#import "TreeViewNode.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";
static NSString *identifierSection4 = @"MyTableViewCell4";
static NSString *identifierSection5 = @"MyTableViewCell5";

@implementation CarterFilterAddView

#pragma mark - INIT

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0] ;
    if (self) {
        _expectTarget = expectTarget;
        _parentVC = vc;
        _isLiveHunt = isLiveHunt;
        [self instance];
    }
    return self;
}

-(void)instance
{
    [self.btnCancel setTitle:str(strAnuler) forState:UIControlStateNormal];
    [self.btnValider setTitle:str(strValider) forState:UIControlStateNormal];
    switch (_expectTarget) {
        case ISMUR:
        {
            self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            self.vViewSearch.hidden= YES;
            self.contraintHeaderViewSearch.constant = 0;

        }
            break;
        case ISGROUP:
        {
            self.colorNavigation = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            self.vViewSearch.hidden= YES;
            self.contraintHeaderViewSearch.constant = 0;

        }
            break;
        case ISLOUNGE:
        {
            if (_isLiveHunt) {
                self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                self.lbTitleScreen.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                self.vViewSearch.hidden= YES;
                self.contraintHeaderViewSearch.constant = 0;
 
            }
            else
            {
                self.colorNavigation = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                self.lbTitleScreen.textColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                self.vViewSearch.hidden= YES;
                self.contraintHeaderViewSearch.constant = 0;

            }
            
        }
            break;
        default:
        {
            self.colorNavigation = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            self.vViewSearch.hidden= YES;
            self.contraintHeaderViewSearch.constant = 0;

            
        }
            break;
    }
    [self.tfMotCle.layer setMasksToBounds:YES];
    self.tfMotCle.layer.cornerRadius= 4.0;
    self.tfMotCle.layer.borderWidth =0.5;
    self.tfMotCle.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type1" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection2];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type4" bundle:nil] forCellReuseIdentifier:identifierSection4];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type5" bundle:nil] forCellReuseIdentifier:identifierSection5];
    
    self.tableControl.estimatedRowHeight = 66;
    arrData = [NSMutableArray new];
    
}
-(void)addContraintSupview:(UIView*)viewSuper
{
    UIView *view = self;
    view.translatesAutoresizingMaskIntoConstraints = NO;
//    view.frame = viewSuper.frame;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeLeading
                               relatedBy:NSLayoutRelationEqual
                               toItem:viewSuper
                               attribute:NSLayoutAttributeLeading
                               multiplier:1.0 constant:0]];
    [viewSuper addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeTrailing
                               relatedBy:NSLayoutRelationEqual
                               toItem:viewSuper
                               attribute:NSLayoutAttributeTrailing
                               multiplier:1.0 constant:0] ];
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
}
-(void)hide:(BOOL)hidden
{
    [self endEditing:YES];
    if (hidden) {
        _contraintLeading.constant = 375;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"HIDE");
             self.hidden = hidden;
         }];
    }
    else
    {
        self.hidden = hidden;
        _contraintLeading.constant = 70;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"SHOW");
             [self addSubViewValiderFind];
             [self addSubViewValiderAdd];
         }];
    }
    
}
-(void)setup
{
    self.hidden = YES;
    _contraintLeading.constant = 375;
}
-(void)buttonValiderWithHide:(BOOL)hide
{
    if (hide) {
        _contraintHeghtValider.constant = 0;
        _btnValider.hidden = YES;
    }
    else
    {
        _contraintHeghtValider.constant = 40;
        _btnValider.hidden = NO;
    }
}
-(void) addSubViewValiderFind
{
    [self.viewValiderFind removeFromSuperview];
    __weak CarterFilterAddView *wself = self;
    self.viewValiderFind = [[CarterFilterFindView alloc] initWithEVC:_parentVC expectTarget:self.expectTarget isLiveHunt:_isLiveHunt];
    [self.viewValiderFind addContraintSupview:self];
    [self.viewValiderFind setCallback:^(TreeViewNode *nodeSub)
     {
         [wself fnSubTreeChoose:nodeSub];
     }];

    [self.viewValiderFind setup];
}

-(void)fnSubTreeChoose:(TreeViewNode*)nodeSub
{
    if (nodeSub) {
        //reload data
        if (_callback) {
            _callback(_nodes,nodeSub,_tfMotCle.text);
        }
    }
    [self hide:YES];
}
//MARK: -DATA
-(void)fnTreeNode:(TreeViewNode*)node
{
    _nodes = node;
    [arrData removeAllObjects];
    if([_nodes.nodeObject[@"filter_date"] intValue] == DATE_PERSONNALISER)
    {
        NSMutableDictionary *dicChild = [_nodes.nodeObject mutableCopy];
        [dicChild setObject:@(1) forKey:@"status"];
        [dicChild setObject:@(4) forKey:@"type"];
        _nodes.nodeObject = dicChild;
        [self buttonValiderWithHide: NO];
        self.lbTitleScreen.text = str(strFiltrerSurQUAND);//@"Personnaliser";
        self.lbDescScreen.text = str(strDescriptionQUAND);
        [arrData addObject:_nodes];
    }
    else
    {
        switch ([node.nodeObject[@"filter_type"] intValue]) {
            case FILTER_QUI:
            {
                [self buttonValiderWithHide: NO];
                self.lbTitleScreen.text = str(strFiltrerSurQUI);
                self.lbDescScreen.text = str(strDescriptionQUI);
                [self fillNodeWithChildrenArray:_nodes.nodeChildren];
                [self.tableControl reloadData];
            }
                break;
            case FILTER_QUOI:
            {
                [self buttonValiderWithHide: NO];
                self.lbTitleScreen.text = str(strFiltrerSurQUOI);
                self.lbDescScreen.text = str(strDescriptionQUOI);
                [self fillChildrenQUOI:_nodes.nodeChildren];
                [self.tableControl reloadData];
            }
                break;
            case FILTER_QUAND:
            {
                [self buttonValiderWithHide: NO];
                self.lbTitleScreen.text = str(strFiltrerSurQUAND);
                self.lbDescScreen.text = str(strDescriptionQUAND);
                [self fillChildrenQUAND:_nodes.nodeChildren];
                [self.tableControl reloadData];
            }
                break;
            default:
                break;
        }
    }
}
//MARK: - GEN DATA QUI
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    [arrData removeAllObjects];
    for (TreeViewNode *node in childrenArray) {
        
        [arrData addObject:node];
        NSMutableDictionary *dic = [node.nodeObject mutableCopy];
        if ([dic[@"status"] boolValue]) {
        }
    }
}
//MARK: - GEN DATA QUOI
- (void)fillChildrenQUOI:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [arrData addObject:node];
        if ([node.nodeObject[@"status"] boolValue]) {
            [self fillChildrenQUOI:node.nodeChildren];
        }
    }
}
//MARK: - GEN DATA QUAND
- (void)fillChildrenQUAND:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        [arrData addObject:node];
    }
}

//MARK: - CALLBACK
-(void)setCallback:(CarterFilterAddViewCallback)callback
{
    _callback = callback;
}
//MARK: - ACTION
-(IBAction)bacButtonAction:(id)sender
{
    if([_nodes.nodeObject[@"filter_date"] intValue] == DATE_PERSONNALISER)
    {
        NSMutableDictionary *dicChild = [_nodes.nodeObject mutableCopy];
        [dicChild setObject:@(0) forKey:@"status"];
        [dicChild setObject:@(2) forKey:@"type"];
        [dicChild setObject:@"" forKey:@"fin"];
        [dicChild setObject:@"" forKey:@"debut"];

        _nodes.nodeObject = dicChild;
    }
    [self hide:YES];
}
-(IBAction)validerAction:(id)sender
{
    if ([_nodes.nodeObject[@"filter_type"] intValue] == FILTER_QUOI ||[_nodes.nodeObject[@"filter_type"] intValue] == FILTER_QUAND) {

            //callback
            if (_callback) {
                _callback(_nodes,nil,_tfMotCle.text);
            }
        [self hide:YES];
    }
    else if ([_nodes.nodeObject[@"filter_date"] intValue] == DATE_PERSONNALISER)
    {
        NSMutableDictionary *dicChild = [_nodes.nodeObject mutableCopy];
        NSString *strFin = dicChild[@"fin"];
        NSString *strDebut = dicChild[@"debut"];

        if (strFin.length > 0 && strDebut.length > 0) {
            //callback
            if (_callback) {
                _callback(_nodes,nil,_tfMotCle.text);
            }
            [self hide:YES];
        }
        else
        {
            UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [salonAlert show];
        }

    }
    else
    {
        [self hide:YES];
    }
}
//MARK: - ACTION QUOI
- (IBAction)expandAction:(id)sender
{
    UIButton *sv = (UIButton*)sender;
    int index = (int)sv.tag - 500;
    TreeViewNode *nodeSelected = arrData[index];

    NSMutableDictionary *dic = [nodeSelected.nodeObject mutableCopy];
    [dic setObject:@(![dic[@"status"] boolValue]) forKey:@"status"];
    nodeSelected.nodeObject = dic;
    
    [self fnTreeNode:_nodes];
    [self.tableControl reloadData];
}

- (IBAction)selectAction:(id)sender
{
    UIButton *sv = (UIButton*)sender;
    int index = (int)sv.tag - 300;
    TreeViewNode *nodeSelected = arrData[index];
    NSMutableDictionary *dic = [nodeSelected.nodeObject mutableCopy];
    int statusNode = [dic[@"check"] intValue];
    switch (statusNode) {
        case NON_CHECK:
        {
            statusNode = IS_CHECK;
        }
            break;
        case UN_CHECK:
        {
            statusNode = IS_CHECK;
            
        }
            break;
        case IS_CHECK:
        {
            statusNode = NON_CHECK;
            
        }
            break;
        default:
            break;
    }
    [dic setObject:@(statusNode) forKey:@"check"];
    nodeSelected.nodeObject = dic;
    
    [self checkFromParent:nodeSelected withStatusNode:statusNode];
    [self checkFromChild:nodeSelected];
    [self.tableControl reloadData];
    
}
- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
    [dic setObject:@(statusNode) forKey:@"check"];
    parent.nodeObject = dic;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        NSMutableDictionary *dic = [node.nodeObject mutableCopy];
        [dic setObject:@(statusNode) forKey:@"check"];
        node.nodeObject = dic;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        NSMutableDictionary *dic = [treenode.nodeObject mutableCopy];
        if ([dic[@"check"] intValue] == IS_CHECK) {
            countCheck++;
        }
        if ([dic[@"check"] intValue] == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(IS_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(UN_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
        
    }
    else
    {
        NSMutableDictionary *dic = [parent.nodeObject mutableCopy];
        [dic setObject:@(NON_CHECK) forKey:@"check"];
        parent.nodeObject = dic;
        
    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }
    
}
//MARK: - ACTION DATE_PERSONNALISER
-(void) addSubViewValiderAdd
{
    [self.viewValiderAdd removeFromSuperview];
    __weak typeof(self) wself = self;
    self.viewValiderAdd = [[CarterFilterAddView alloc] initWithEVC:_parentVC expectTarget:self.expectTarget isLiveHunt:_isLiveHunt];
    [self.viewValiderAdd addContraintSupview:self];
    [self.viewValiderAdd setCallback:^(TreeViewNode *node,TreeViewNode *nodeSub,NSString *strSearch)
     {
         if([node.nodeObject[@"filter_date"] intValue] == DATE_PERSONNALISER)
         {
             NSMutableDictionary *dicChild = [node.nodeObject mutableCopy];
             [dicChild setObject:@(1) forKey:@"status"];
             if([dicChild[@"filter_date"] intValue] == DATE_PERSONNALISER)
             {
                [dicChild setObject:@(4) forKey:@"type"];

             }
             TreeViewNode *nodeTmp = [TreeViewNode new];
             nodeTmp.nodeChildren = node.nodeChildren;
             nodeTmp.nodeObject = dicChild;
             nodeTmp.parent = node.parent;
             
             TreeViewNode *nodeParent = node.parent;
             NSMutableDictionary *dicParent = [nodeParent.nodeObject mutableCopy];
             [dicParent setObject:@[nodeTmp] forKey:@"listchoose"];
             nodeParent.nodeObject = dicParent;
             [wself validerAction:nil];

         }
     }];
    [self.viewValiderAdd setup];
}
-(void)settingValiderAddWith:(TreeViewNode*)node
{
    [self.viewValiderAdd fnTreeNode:node];
    [self.viewValiderAdd hide:NO];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TreeViewNode *node = arrData[indexPath.row];
    NSMutableDictionary  *dic = [node.nodeObject mutableCopy];
    if ([dic[@"type"] intValue] == 2)
    {
        Filter_Cell_Type2 *cell = (Filter_Cell_Type2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
        cell.lbTitle.text = dic[@"name"];
//        if ([dic[@"filter_date"] intValue]==DATE_LAST30) {
//            cell.lbDescription.text = str(strDerniersJours);
//            
//        }
//        else
//        {
//            cell.lbDescription.text = nil;
//        }
        if ([dic[@"arrow"] boolValue] ||[dic[@"filter_date"] intValue] == DATE_PERSONNALISER) {
            cell.imgArrow.hidden = NO;
        }
        else
        {
            cell.imgArrow.hidden = YES;
        }
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else if ([dic[@"type"] intValue] == 4)
    {
        Filter_Cell_Type4 *cell = (Filter_Cell_Type4 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection4 forIndexPath:indexPath];
        cell.lbTitle.text = dic[@"name"];
        cell.btnClose.hidden =YES;
        cell.txtDebut.placeholder = str(strDateDeDebut);
        cell.txtFin.placeholder = str(strDateDeFin);
        
        
        __weak typeof(cell) wkVC = cell;
        cell.txtDebut.text = dic[@"debut"];
        cell.txtDebut.parentVC = self.parentVC;
        [cell.txtDebut setCallBack:^(){
            
            if ([wkVC.txtFin.text isEqualToString:@""]) {
                
                wkVC.txtFin.date = wkVC.txtDebut.date;
                wkVC.txtFin.text = wkVC.txtDebut.text;
            }
            [dic setObject:wkVC.txtDebut.text forKey:@"debut"];
            [dic setObject:wkVC.txtFin.text forKey:@"fin"];
            node.nodeObject = dic;
            
        }];
        
        cell.txtFin.text = dic[@"fin"];
        cell.txtFin.parentVC = self.parentVC;
        [cell.txtFin setCallBack:^(){
            [dic setObject:wkVC.txtFin.text forKey:@"fin"];
            node.nodeObject = dic;
        }];
        
        //Status
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else if ([dic[@"type"] intValue] == 5)
    {
        Filter_Cell_Type5 *cell = (Filter_Cell_Type5 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection5 forIndexPath:indexPath];
        
        cell.treeNode = node;
        
        cell.cellLabel.text = dic[@"name"];
        if (node.nodeChildren.count==0) {
            [cell setTheButtonBackgroundImage:nil];
            
        }
        else
        {
            if ([dic[@"status"] boolValue]) {
                [cell.cellButton setBackgroundImage:[[UIImage imageNamed:@"arrow_down"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
            }
            else {
                [cell.cellButton setBackgroundImage:[[UIImage imageNamed:@"arrow_up"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];

            }
            cell.cellButton.tintColor = self.colorNavigation;
        }
        switch ([dic[@"check"] intValue]) {
            case NON_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor whiteColor] withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            case UN_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor lightGrayColor] withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            case IS_CHECK:
            {
                [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:self.colorNavigation withColorBoderActive:[UIColor lightGrayColor]];
            }
                break;
            default:
                break;
        }
        cell.btnCheckBox.selected = TRUE;
        [cell.btnCheckBox setTypeButton:NO];

        [cell.btnCheckBox addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnCheckBox.tag =indexPath.row+300;
        [cell.btnExpand addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnExpand.tag =indexPath.row+500;
        if(node.lineBottom)
        {
            cell.lineBottom.hidden = NO;
        }
        else
        {
            cell.lineBottom.hidden = YES;
        }
        //check_inactive // ic_check_active_pink// check
        [cell setNeedsDisplay];
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else
    {
        NSLog(@"node nil: %@",dic);
        return nil;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TreeViewNode *node = arrData[indexPath.row];
    if ([_nodes.nodeObject[@"filter_type"] intValue] == FILTER_QUI) {
        if ([node.nodeObject[@"input_type"] intValue] == INPUT_TYPE_FIND) {
            [self.viewValiderFind hide: NO];
            [self.viewValiderFind fnTreeNode:node];
            
        }
        else
        {
            //select
            NSMutableDictionary *dicParent = [node.nodeObject mutableCopy];
            [dicParent setObject:@(1) forKey:@"status"];
            node.nodeObject = dicParent ;
            //callback
            if (_callback) {
                _callback(_nodes,node,_tfMotCle.text);
            }
            [self hide:YES];
            
        }
    }
    else if ([_nodes.nodeObject[@"filter_type"] intValue] == FILTER_QUAND)
    {
        
        NSMutableDictionary *dicChild = [node.nodeObject mutableCopy];
        if([dicChild[@"filter_date"] intValue] == DATE_PERSONNALISER)
        {
//            [dicChild setObject:@(4) forKey:@"type"];
            //goto
            [self settingValiderAddWith:node];
            return;
        }
        [dicChild setObject:@(1) forKey:@"status"];
        TreeViewNode *nodeTmp = [TreeViewNode new];
        nodeTmp.nodeChildren = node.nodeChildren;
        nodeTmp.nodeObject = dicChild;
        nodeTmp.parent = node.parent;
        
        TreeViewNode *nodeParent = node.parent;
        NSMutableDictionary *dicParent = [nodeParent.nodeObject mutableCopy];
        [dicParent setObject:@[nodeTmp] forKey:@"listchoose"];
        nodeParent.nodeObject = dicParent;
//        [self fnTreeNode:_nodes];
//        [self.tableControl reloadData];
        [self validerAction:nil];
    }


}

@end
