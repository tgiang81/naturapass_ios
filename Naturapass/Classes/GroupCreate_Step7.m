//
//  GroupCreate_Step7.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step7.h"
#import "GroupCreate_Step8.h"

@interface GroupCreate_Step7 ()

@end

@implementation GroupCreate_Step7

- (void)viewDidLoad {
    [super viewDidLoad];
    self.needChangeMessageAlert = YES;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    GroupCreate_Step8 *viewController1 = [[GroupCreate_Step8 alloc] initWithNibName:@"GroupCreate_Step8" bundle:nil];
    [self pushVC:viewController1 animate:YES];
    
}


@end
