//
//  ListAMIS.m
//  Naturapass
//
//  Created by Giang on 9/15/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ListAMIS.h"
#import "SubNavigation_PRE_General.h"
#import "CellKind8.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface UIButton (ext)
{
    
}

-(void) setType :(ISSCREEN) iType;
@end

@implementation UIButton (ext)

-(void) setType :(ISSCREEN) iType
{
    switch (iType) {
        case ISMUR:
        {
            [self setImage: [UIImage imageNamed:@"mur_ic_amis_add_friend"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"mur_ic_amis_add_friend"] forState:UIControlStateNormal];

        }
            break;
        case ISGROUP:
        {
            [self setImage: [UIImage imageNamed:@"group_ic_add_friend"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"group_ic_add_friend"] forState:UIControlStateNormal];
            
        }
            break;
        case ISLOUNGE:
        {
            [self setImage: [UIImage imageNamed:@"ic_chasse_add_friend"] forState:UIControlStateSelected];
            [self setImage: [UIImage imageNamed:@"ic_chasse_add_friend"] forState:UIControlStateNormal];
            
        }
            break;
            
        default:
            break;
    }

}

@end

@interface ListAMIS ()
{
    NSMutableArray * arrData;
    NSString *sender_id;
}
@end

@implementation ListAMIS
static NSString *identifierSection1 = @"MyTableViewCell1";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addSubNav:@"SubNavigation_PRE_General"];
    
    SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
            
        }
            break;
        case ISGROUP:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(GROUP_BACK);
            
        }
            break;
        case ISLOUNGE:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCHANTIERS)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
            
        }
            break;
        default:
            //HOME
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
            
        }
            break;
    }

    // Do any additional setup after loading the view from its nib.
    arrData = [NSMutableArray new];
    
    [self.lbName setText:  [NSString stringWithFormat:@"%@ %@",str(strAmis_de), self.myDic[@"fullname"] ] ];

    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind8" bundle:nil] forCellReuseIdentifier:identifierSection1];
    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];


    [self getMyFriends];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    

    
}

/*
 https://naturapass.e-conception.fr/app_dev.php/api/v2/users/161/friends?mutual=1 => lay ve so friend chung cua current user voi friend 161
 
 tuyen [9:10 AM]
 https://naturapass.e-conception.fr/app_dev.php/api/v2/users/161/friends -> get all friend cua 161
 */


/*
 {
 courtesy = 0;
 firstname = Trublin;
 fullname = "Trublin Yves";
 id = 33;
 lastname = Yves;
 photo = "/img/interface/default-avatar.jpg";
 relation =             {
 friendship =                 {
 state = 2;
 way = 3;
 };
 mutualFriends = 0;
 };
 usertag = "trublin-yves-1";
 },
 */

-(void) getMyFriends
{
    [COMMON addLoading:self];

    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI getTheFriends:YES withID:self.myDic[@"id"]];
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];

        if (response!=nil) {
            if ([response isKindOfClass: [NSDictionary class]]) {
                
                NSArray *arrTmp = response[@"friends"] ;
                
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                             ascending:YES];
                //
                NSArray *results = [arrTmp sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                
                
                
                for (NSDictionary*dic in results) {
                    [arrData addObject:dic];
                }
                
                [self.tableControl reloadData];
            }
        }
        
    };
}

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];

    [self gotoback];

}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind8 *cell = nil;
    
    cell = (CellKind8 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];

    NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,arrData[indexPath.row][@"photo"]];
    
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"]];
    
    //FONT
    cell.label1.text = dic[@"fullname"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.label1.numberOfLines=2;
    [cell layoutIfNeeded];
    
    BOOL isAddFriendHide = YES;
    if ([dic[@"id"] integerValue] ==[sender_id integerValue]) {
        isAddFriendHide= YES;
    }
    else
    {
        NSDictionary *dicRelation =dic[@"relation"];
        if ([dicRelation isKindOfClass:[NSDictionary class]]) {
            
            if ([dicRelation[@"friendship"] isKindOfClass:[NSDictionary class]]) {
                int stateFriend =[dicRelation[@"friendship"][@"state"] intValue];
                switch (stateFriend) {
                    case NOT_FRIEND:
                    {
                        
                        isAddFriendHide=NO;
                    }
                        break;
                    case WAIT_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    default:
                    {
                        isAddFriendHide=NO;
                        
                    }
                        break;
                }
            }else{
                isAddFriendHide=NO;
            }
        }else{
            isAddFriendHide=NO;
        }
    }


    [cell.rightIcon setImage: [UIImage imageNamed:@"mur_ic_amis_add_friend"] ];
    
    cell.rightIcon.hidden=isAddFriendHide;
     
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dic = arrData[indexPath.row];
    
    BOOL isAddFriendHide = YES;
    if ([dic[@"id"] integerValue] ==[sender_id integerValue]) {
        isAddFriendHide= YES;
    }
    else
    {
        NSDictionary *dicRelation =dic[@"relation"];
        if ([dicRelation isKindOfClass:[NSDictionary class]]) {
            
            if ([dicRelation[@"friendship"] isKindOfClass:[NSDictionary class]]) {
                int stateFriend =[dicRelation[@"friendship"][@"state"] intValue];
                switch (stateFriend) {
                    case NOT_FRIEND:
                    {
                        
                        isAddFriendHide=NO;
                    }
                        break;
                    case WAIT_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_REJECTED:
                    {
                        
                        isAddFriendHide=NO;
                    }
                        break;
                    default:
                    {
                        isAddFriendHide=NO;
                        
                    }
                        break;
                }
            }else{
                isAddFriendHide=NO;
            }
        }else{
            isAddFriendHide=NO;
        }
    }
    
    if (isAddFriendHide) {
        
        //no processing
        return;
    }
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];

        if ([response[@"relation"] isKindOfClass:[NSDictionary class]]) {

        NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
        [mulDic removeObjectForKey:@"relation"];
        [mulDic setObject:response[@"relation"] forKey:@"relation"];
        [arrData replaceObjectAtIndex:indexPath.row withObject:mulDic];
        //reload item
        NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
        [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];

        }
    };

}
/*
 relation =     {
 friendship =         {
 state = 1;
 way = 3;
 };
 mutualFriends = 0;
 };
 */

@end
