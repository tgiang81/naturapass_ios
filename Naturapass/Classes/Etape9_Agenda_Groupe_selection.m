//
//  Etape9_Agenda_Groupe_selection.m
//  Naturapass
//
//  Created by Giang on 10/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Etape9_Agenda_Groupe_selection.h"

#import "TypeCell41.h"
#import "ChassesCreate_Step6.h"
#import "DatabaseManager.h"
static NSString *typecell41 =@"TypeCell41";
static NSString *typecell41ID =@"TypeCell41ID";
@interface Etape9_Agenda_Groupe_selection ()
{
    NSMutableArray *arrData;
    IBOutlet UIButton *suivant;
    NSInteger countCheck;
}

@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@property(nonatomic,strong) IBOutlet UILabel *label3;

@property(nonatomic,strong) IBOutlet UILabel *labelWarning;
@property (nonatomic, strong) TypeCell41 *prototypeCell;

@end

@implementation Etape9_Agenda_Groupe_selection


- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];
    suivant.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    self.needChangeMessageAlert = YES;

}

//membres ~ number subscriber.
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell41 bundle:nil] forCellReuseIdentifier:typecell41ID];
    arrData = [NSMutableArray new];
    /*
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE c_admin=1 AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            
            //compare to get defaul
            for (NSDictionary*dic in arrTmp) {
                if ([dic[@"groupID"] intValue ]== strID) {
                    [arrData addObject:@{@"id":dic[@"group"][@"id"],
                                         @"name":dic[@"group"][@"name"],
                                         @"photo":dic[@"group"][@"photo"],
                                         @"nbSubscribers": dic[@"group"][@"nbSubscribers"],
                                         @"status":[NSNumber numberWithBool:NO]
                                         }];
                    break;
                }
            }
        }
    }];
*/
    [COMMON addLoadingForView:self.view];
    WebServiceAPI *service = [WebServiceAPI new];
    [service getAllGroupsAdmin];
    service.onComplete = ^(NSDictionary *response, int errCode)
    {
        [COMMON removeProgressLoading];
        //compare to get defaul
        NSArray *arrTmp = response[@"groups"];
        for (NSDictionary*dic in arrTmp) {
            [arrData addObject:@{@"id":dic[@"id"],
                                 @"name":dic[@"name"],
                                 @"photo":dic[@"photo"],
                                 @"nbSubscribers": dic[@"nbSubscribers"],
                                 @"status":[NSNumber numberWithBool:NO]
                                 }];
        }
        if (arrData.count ==0 && [COMMON isReachable]) {
            self.labelWarning.hidden = NO;
        }else{
            self.labelWarning.hidden = YES;
        }
        
        [self.tableControl reloadData];

    };
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
            [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    [self statusButtonSuivant];
}

#pragma mark - TableView datasource & delegate
- (TypeCell41 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:typecell41ID];
    }
    return _prototypeCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height+1;
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[TypeCell41 class]])
    {
        TypeCell41 *cell = (TypeCell41 *)cellTmp;
        //photo
        NSDictionary *dic = arrData[indexPath.row];
        
        NSString* imgUrl  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        
        [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
        
        NSString*strMembres = nil;
        
        cell.label1.text = dic[@"name"];
    if ([dic[@"nbSubscribers"] intValue] > 1) {
        strMembres = [NSString stringWithFormat:@"%d %@s",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
        
    }else{
        strMembres = [NSString stringWithFormat:@"%d %@",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
    }
        
        cell.constraint_control_width.constant=20;
        [cell.checkbox setTypeCheckBox:UI_CHASSES_MUR_NORMAL];
        NSNumber *num = dic[@"status"];
        
        if ( [num boolValue] ) {
            [cell.checkbox setSelected:TRUE];
            
        }else{
            [cell.checkbox setSelected:FALSE];
            
        }
        cell.checkbox.tag = indexPath.row;
        [cell.checkbox addTarget:self action:@selector(checkbox:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell.label2.text = strMembres;
        [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
        [cell layoutIfNeeded];

    }

}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeCell41 *cell = [tableView dequeueReusableCellWithIdentifier:typecell41ID forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self checkboxStatusWithIndex:indexPath.row];
}
-(IBAction)checkbox:(id)sender
{
    NSInteger index =[sender tag];
    [self checkboxStatusWithIndex:index];
}
-(void)checkboxStatusWithIndex:(NSInteger)index
{
    NSDictionary *dic=[[arrData objectAtIndex:index] mutableCopy];
    
    if ([dic[@"status"] boolValue] == YES)
    {
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"status"];
        countCheck--;
    }
    else
    {
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        countCheck++;
    }
    
    [arrData replaceObjectAtIndex:index withObject:dic];
    
    
    NSRange range = NSMakeRange(0,1);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    [self statusButtonSuivant];
}
-(void)statusButtonSuivant
{
    if (countCheck >0) {
        self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    }
    else
    {
        self.btnSuivant.backgroundColor = [UIColor lightGrayColor];
    }
}
//Suivant
//http://doc-naturapass.e-conception.fr/source-class-Api.ApiBundle.Controller.v2.Hunts.HuntsController.html#37-89

-(IBAction)onNext:(id)sender
{
    
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    for (NSDictionary*dic in arrData) {
        if ([dic[@"status"] boolValue] == YES) {
            [arrTmp addObject:dic[@"id"]];
        }
    }
    
    if (arrTmp.count == 0) {
        return;
    }
    
    [COMMON addLoading:self];
    //Request

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_AGENDA_GROUP_TO_HUNT:@{ @"groups": arrTmp }
                                        loungeID:[ChassesCreateOBJ sharedInstance].strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];

        if (response[@"success"]) {
            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                [self backEditListChasse];
            }
            else
            {
                //
                ChassesCreate_Step6 *viewController1 = [[ChassesCreate_Step6 alloc] initWithNibName:@"ChassesCreate_Step6" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            }
            
        }else{
            //message timeout...
            
            [KSToastView ks_showToast:str(strRequestTimeout)  duration:3.0f completion: ^{
            }];
            
//            if([self fnCheckResponse:response]) return;
        }
    };
    
}

@end
