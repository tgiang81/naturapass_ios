//
//  MurPeopleLikes.h
//  Naturapass
//
//  Created by Giang on 12/10/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MurBaseVC.h"
typedef void (^callBackAlert) (NSInteger index, NSDictionary *dic);

@interface MurPeopleLikes : MurBaseVC
@property(nonatomic,strong) NSMutableArray *arrMembers;
@property(nonatomic,strong) NSString       *strID;
@property (nonatomic, copy) callBackAlert CallBack;

- (void)setData:(NSMutableArray*)arrMembers;

@end
