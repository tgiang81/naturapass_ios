//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

//ic_not_participate
//"ic_participate"
//ic_pre_participate"
#import "TypeCell11.h"

@implementation TypeCell11
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
    self.lbParticipe.textColor =UIColorFromRGB(PARTICIPE_COLOR);
    self.lbNeSaisPas.textColor =UIColorFromRGB(NE_SAIS_PAS_COLOR);
    self.lbNeParticipePas.textColor =UIColorFromRGB(NE_PARTICIPE_PAS_COLOR);


}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
}

@end
