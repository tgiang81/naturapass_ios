//
//  ChassesCreateDateCell.m
//  Naturapass
//
//  Created by JoJo on 8/3/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateDateCell.h"

@implementation ChassesCreateDateCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewDebut.layer setMasksToBounds:YES];
    self.viewDebut.layer.cornerRadius= 15.0;
    
    [self.viewFin.layer setMasksToBounds:YES];
    self.viewFin.layer.cornerRadius= 15.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
