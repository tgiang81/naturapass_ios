//
//  UploadProgress.m
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AlertViewMembersLike.h"
#import "CellKind12.h"
#import "AppCommon.h"
#import "MembersCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "CCMPopupTransitioning.h"

@interface AlertViewMembersLike ()
{
    IBOutlet UITableView *tableControl;
    IBOutlet UIView  *viewHearder;
    UIColor *color;
    UIImage *imgAdd;
    NSString *sender_id;
}
@end

static NSString *identifier = @"CellKind12ID";

@implementation AlertViewMembersLike
- (id)initWithArrayMembers:(NSMutableArray*)arrMembers
{
    self = [super initWithNibName:@"AlertViewMembersLike" bundle:nil];
    if (self) {
        // Custom initialization
        _arrMembers=arrMembers;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];

    [self.view.layer setMasksToBounds:YES];
    self.view.layer.cornerRadius= 4.0;
    self.view.layer.borderWidth =0.5;
    self.view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    NSString *strImg =strEMPTY;
    switch (self.expectTarget) {
        case ISMUR://MUR
        {
            color = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            strImg=@"mur_ic_amis_add_friend";
        }
            break;
        case ISCARTE://CARTE
        {
            color = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            
        }
            break;
        case ISDISCUSS://DISCUSSION
        {
            color = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP://GROUP
        {
            color = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            strImg=@"ic_add_member_group";

        }
            break;
        case ISLOUNGE://CHASSES
        {
            color = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            strImg=@"chasse_ic_add_friend";

        }
            break;
        case ISAMIS://AMIS
        {
            color = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
            
        }
            break;
        case ISPARAMTRES://PARAMETERS
        {
            color = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
            
        }
            break;
        default:
            break;
    }
    viewHearder.backgroundColor = color;
    imgAdd =[UIImage imageNamed:strImg];
    [tableControl registerNib:[UINib nibWithNibName:@"MembersCell" bundle:nil] forCellReuseIdentifier:identifier];
    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrMembers count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
     {
     likes =     (
     {
     courtesy = 1;
     firstname = Valot;
     fullname = "Valot Vincent";
     id = 3608;
     lastname = Vincent;
     parameters =             {
     friend = 1;
     };
     photo = "/uploads/users/images/thumb/c4134f0387cfd0ccdbdbe3ed7bbdc101e35f4446.jpeg";
     usertag = "manh-manh";
     }
     );
     }
     */
    MembersCell *cell = nil;
    
    cell = (MembersCell *)[tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = _arrMembers[indexPath.row];
    cell.label1.text = dic[@"fullname"];
    //image
    NSString *imgUrl;
    //profilepicture
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:imgUrl]];
    [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    
    
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];
    
    //Arrow
    // neu la normal thi an nut add friend con neu la admin thic check da co moi quan he thi an con khong thi show ra cho nguoi dung click
            BOOL isAddFriendHide =YES;
            if ([dic[@"id"] integerValue] ==[sender_id integerValue]) {
                isAddFriendHide= YES;
            }
            else
            {
                /*
                 relation =     {
                 friendship = 0;
                 mutualFriends = 0;
                 };
                 
                 
                 */
                if (dic[@"relation"]) {
                    if ([dic[@"relation"][@"friendship"] isKindOfClass:[NSDictionary class]])
                    {
                        isAddFriendHide =YES;
                    }else{
                        isAddFriendHide=NO;
                    }
                }

            }
    if (isAddFriendHide) {
        cell.btnAddFriend.hidden =YES;
        cell.imgAddFriend.hidden=YES;
        
        [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        cell.btnAddFriend.hidden=NO;
        cell.imgAddFriend.hidden=NO;
        
        [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.btnAddFriend.tag= indexPath.row;
    cell.imgAddFriend.image =imgAdd;
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _arrMembers[indexPath.row];

    if ([dic[@"id"] integerValue] !=[sender_id integerValue]) {

    NSDictionary *dic =_arrMembers[indexPath.row];
    if (_CallBack) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
        _CallBack(indexPath.row, dic);
    }
    }
}

#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    int index =(int)[sender tag];
    [COMMON addLoading:self];
    NSDictionary *dic = self.arrMembers[index];
        
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            //co qh
            
            NSMutableDictionary *userDic =[NSMutableDictionary dictionaryWithDictionary:dic];
            if (userDic[@"relation"]) {
                [userDic removeObjectForKey:@"relation"];

            }
            [userDic addEntriesFromDictionary:response];
            for (int i=0; i<self.arrMembers.count; i++) {
                NSDictionary *temp =self.arrMembers[i];
                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
                    [self.arrMembers replaceObjectAtIndex:i withObject:userDic];
                    break;
                }
            }
            //reload item
            NSArray *indexArray =[tableControl indexPathsForVisibleRows];
            [tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }
    };
}
-(IBAction)fnDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}
#pragma mark -
-(void)showInVC:(UIViewController*)vc
{
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = self;
    popup.presentingController = vc;
    [vc presentViewController:self animated:YES completion:nil];
}
@end
