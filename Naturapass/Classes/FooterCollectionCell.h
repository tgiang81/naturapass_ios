//
//  FooterCollectionCell.h
//  Naturapass
//
//  Created by Giang on 6/25/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBrand;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
