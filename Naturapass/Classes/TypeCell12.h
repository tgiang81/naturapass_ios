//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"

@interface TypeCell12: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;
@property (nonatomic,retain) IBOutlet UIButton       *btnAddFriend;

//image
@property (nonatomic,retain) IBOutlet UIImageView       *imgAvatar;
@property (nonatomic,retain) IBOutlet UIImageView       *imgCrown;
@property (nonatomic,retain) IBOutlet UIImageView       *imgParticipant;
@property (nonatomic,retain) IBOutlet UIImageView       *imgAddFriend;
//label
@property (nonatomic,retain) IBOutlet UILabel           *lbTitle;

@property (nonatomic,retain) IBOutlet UILabel           *lbParticipant;
@property (nonatomic,retain) IBOutlet UILabel           *lbDescription;
//funtion
-(void)fnSettingCell:(int)type;
-(void)getParticipationName:(int)participationId;
@end
