//
//  GroupSettingVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupEnterSettingVC.h"
#import "GroupAdminListVC.h"
#import "GroupSettingOBJ.h"
#import "GroupSettingMembres.h"
#import "CellKind2.h"
#import "GroupCreateOBJ.h"
#import "GroupEnterOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChasseSettingMembres.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreate_Step1.h"
#import "ASSharedTimeFormatter.h"
#import "ChassesEnterInfoVC.h"
#import "SettingNotificationEmailMobileVC.h"
#import "GroupParameter_Notification_Smartphones.h"
static NSString *cellIdentifier = @"CellKind2";
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface GroupEnterSettingVC ()
{

    NSMutableArray *arrGroupSetting;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation GroupEnterSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strParametresUtilisateurs);
    [self.tableControl registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:identifierSection1];
    

    USER_KIND   m_userKind = USER_NORMAL;

    if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
        switch ([    [GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"] intValue]) {
            case 0:
            case 2:
            {
                if (self.expectTarget ==ISLOUNGE) {
                    
                    arrGroupSetting =[NSMutableArray new];
                    //
                    GroupSettingOBJ *obj1= [GroupSettingOBJ new];
                    obj1.Title = str(strInfosAgenda);
                    obj1.UrlImgAvatar =strIC_chasse_info;
                    obj1.indexSetting = SETTING_IS_INFOS;
                    [arrGroupSetting addObject:obj1];
                    //
                    GroupSettingOBJ *obj6= [GroupSettingOBJ new];
                    obj6.Title = @"Notifications";
                    obj6.UrlImgAvatar =strIC_chasse_notification;
                    obj6.indexSetting = SETTING_IS_NOTIFI;
                    [arrGroupSetting addObject:obj6];
                    
                    self.vContainer.hidden=NO;
                    m_userKind = USER_NORMAL;
                }
                else
                {
                    arrGroupSetting =[NSMutableArray new];
                    //
                    GroupSettingOBJ *obj1= [GroupSettingOBJ new];
                    obj1.Title = str(strInfosGroupe);
                    obj1.UrlImgAvatar =strIC_group_setting_info;
                    obj1.indexSetting = SETTING_IS_INFOS;
                    [arrGroupSetting addObject:obj1];
                    self.vContainer.hidden=NO;
                    //
                    GroupSettingOBJ *obj2= [GroupSettingOBJ new];
                    obj2.Title = str(strLesMenbres);
                    obj2.UrlImgAvatar =strIC_group_amis;
                    obj2.indexSetting = SETTING_IS__MEMBRES;
                    [arrGroupSetting addObject:obj2];

                    GroupSettingOBJ *obj6= [GroupSettingOBJ new];
                    obj6.Title = @"Notifications";
                    obj6.UrlImgAvatar =strIC_group_setting_notification;
                    obj6.indexSetting = SETTING_IS_NOTIFI;
                    [arrGroupSetting addObject:obj6];

                    m_userKind = USER_NORMAL;
                    
                }
            }
                break;
            case 3:
            {
                if (self.expectTarget ==ISLOUNGE) {
                    arrGroupSetting =[NSMutableArray new];
                    //
                    GroupSettingOBJ *obj1= [GroupSettingOBJ new];
                    obj1.Title = str(strInfosAgenda);
                    obj1.UrlImgAvatar =strIC_chasse_info;
                    obj1.indexSetting = SETTING_IS_INFOS;
                    [arrGroupSetting addObject:obj1];
                    
                    GroupSettingOBJ *obj3= [GroupSettingOBJ new];
                    obj3.Title = str(strGererLesAdmin);
                    obj3.UrlImgAvatar =strIC_chasse_admin_setting;
                    obj3.indexSetting = SETTING_IS_ADMIN;
                    [arrGroupSetting addObject:obj3];
                    //
                    GroupSettingOBJ *obj6= [GroupSettingOBJ new];
                    obj6.Title = @"Notifications";
                    obj6.UrlImgAvatar =strIC_chasse_notification;
                    obj6.indexSetting = SETTING_IS_NOTIFI;
                    [arrGroupSetting addObject:obj6];

                    m_userKind = USER_ADMIN;
                    self.vContainer.hidden=NO;
                }
                else
                {
                    arrGroupSetting =[NSMutableArray new];
                    //
                    GroupSettingOBJ *obj1= [GroupSettingOBJ new];
                    obj1.Title = str(strInfosGroupe);
                    obj1.UrlImgAvatar =strIC_group_setting_info;
                    obj1.indexSetting = SETTING_IS_INFOS;
                    [arrGroupSetting addObject:obj1];
                    //
                    GroupSettingOBJ *obj2= [GroupSettingOBJ new];
                    obj2.Title = str(strLesMenbres);
                    obj2.UrlImgAvatar =strIC_group_amis;
                    obj2.indexSetting = SETTING_IS__MEMBRES;
                    [arrGroupSetting addObject:obj2];
                    //
                    GroupSettingOBJ *obj3= [GroupSettingOBJ new];
                    obj3.Title = str(strGererLesAdmin);
                    obj3.UrlImgAvatar =strIC_group_setting_admin;
                    obj3.indexSetting = SETTING_IS_ADMIN;
                    [arrGroupSetting addObject:obj3];
                    //
                    GroupSettingOBJ *obj4= [GroupSettingOBJ new];
                    obj4.Title = str(strExclureDeMembres);
                    obj4.UrlImgAvatar =strIC_group_setting_bannir;
                    obj4.indexSetting = SETTING_IS_BANNIR;
                    [arrGroupSetting addObject:obj4];

                    GroupSettingOBJ *obj6= [GroupSettingOBJ new];
                    obj6.Title = @"Notifications";
                    obj6.UrlImgAvatar =strIC_group_setting_notification;
                    obj6.indexSetting = SETTING_IS_NOTIFI;
                    [arrGroupSetting addObject:obj6];

                    m_userKind = USER_ADMIN;
                    self.vContainer.hidden=NO;
                }
                
                
            }
                break;
            default:
            {
                arrGroupSetting =nil;
                self.vContainer.hidden=YES;
                m_userKind = USER_NORMAL;
            }
                break;
        }

    }else{
        arrGroupSetting =nil;
        self.vContainer.hidden=YES;
        m_userKind = USER_NORMAL;
    }
    
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrGroupSetting.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    GroupSettingOBJ *obj = arrGroupSetting[indexPath.row];
    cell.imageIcon.image =  [UIImage imageNamed: obj.UrlImgAvatar];
    
    //FONT
    cell.label1.text = obj.Title;
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.constraint_control_width.constant = 20;
    cell.constraintRight.constant = 5;
    cell.constraint_image_width.constant =50;
    cell.constraint_image_height.constant =50;
    btn.frame = CGRectMake(0, 0, btn.image.size.width , btn.image.size.height);
    [cell.viewControl addSubview:btn];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupSettingOBJ *obj = arrGroupSetting[indexPath.row];

    if (self.expectTarget == ISLOUNGE) {
        switch (obj.indexSetting) {
            case SETTING_IS_INFOS:
            {
                [self enterChassesInfoAction];
                
            }
                break;
            case SETTING_IS_ADMIN:
            {
                GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
                //mld
                [vc fnGroupId:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]
                      isAdmin:YES];
                [self pushVC:vc animate:YES expectTarget:ISLOUNGE];

            }
                break;
            case SETTING_IS_NOTIFI:
            {
                SettingNotificationEmailMobileVC *vc = [[SettingNotificationEmailMobileVC alloc] initWithNibName:@"SettingNotificationEmailMobileVC" bundle:nil];
                //mld
                [self pushVC:vc animate:YES expectTarget:ISLOUNGE];
            }
                break;
            default:
                break;
        }
    }
    else if (self.expectTarget == ISGROUP)
    {
        switch (obj.indexSetting) {
            case SETTING_IS_INFOS:
            {
                [self enterGroupInfoAction];
                
            }
                break;
            case SETTING_IS__MEMBRES:
            {
//                if (self.expectTarget==ISLOUNGE) {
//                    ChasseSettingMembres *vc = [[ChasseSettingMembres alloc] initWithNibName:@"ChasseSettingMembres" bundle:nil];
//                    [self pushVC:vc animate:YES expectTarget:ISGROUP];
//                }
//                else
//                {
                    GroupSettingMembres *vc = [[GroupSettingMembres alloc] initWithNibName:@"GroupSettingMembres" bundle:nil];
                    
                    [self pushVC:vc animate:YES expectTarget:ISGROUP];
//                }
                
            }
                break;
            case SETTING_IS_ADMIN:
            {
                GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
                //mld

                [vc fnGroupId:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]
                      isAdmin:YES];
                [self pushVC:vc animate:YES expectTarget:ISGROUP];

            }
                break;
            case SETTING_IS_BANNIR:
            {
                GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
                [vc fnGroupId:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]
                      isAdmin:NO];

                [self pushVC:vc animate:YES expectTarget:ISGROUP];
            }
                break;
            case SETTING_IS_MOBILE:
            {
                GroupParameter_Notification_Smartphones *vc = [[GroupParameter_Notification_Smartphones alloc] initWithNibName:@"GroupParameter_Notification_Smartphones" bundle:nil];
                
                [self pushVC:vc animate:YES expectTarget:ISGROUP];
            }
                break;
            case SETTING_IS_NOTIFI:
            {
                SettingNotificationEmailMobileVC *vc = [[SettingNotificationEmailMobileVC alloc] initWithNibName:@"SettingNotificationEmailMobileVC" bundle:nil];
                //mld
                [self pushVC:vc animate:YES expectTarget:ISGROUP];
            }
                break;
            default:
                break;
        }
    }

}
-(void)enterGroupInfoAction
{
    ChassesEnterInfoVC *vc =[[ChassesEnterInfoVC alloc] initWithNibName:@"ChassesEnterInfoVC" bundle:nil];
    vc.dicInfo =[[GroupEnterOBJ sharedInstance].dictionaryGroup copy];

    //ADMIN
//    if ([vc.dicInfo[@"connected"][@"access"]  intValue] == 3) {
//        vc.needRemoveSubItem = NO;
//    }else{
//        vc.needRemoveSubItem = YES;
//    }
    
    [self pushVC:vc animate:YES expectTarget:ISGROUP];
}

-(void)enterChassesInfoAction
{
    ChassesEnterInfoVC *vc =[[ChassesEnterInfoVC alloc] initWithNibName:@"ChassesEnterInfoVC" bundle:nil];
    vc.dicInfo =[[GroupEnterOBJ sharedInstance].dictionaryGroup copy];

    [self pushVC:vc animate:YES expectTarget:ISLOUNGE];

}

@end
