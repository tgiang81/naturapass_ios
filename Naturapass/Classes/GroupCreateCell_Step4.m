//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GroupCreateCell_Step4.h"
#import "CommonHelper.h"

@implementation GroupCreateCell_Step4
- (void)awakeFromNib {
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];
    [self.imgViewAvatar.layer setMasksToBounds:YES];
    self.imgViewAvatar.layer.cornerRadius= 22;
    self.imgViewAvatar.layer.borderWidth =0;
}
@end
