//
//  ListItemsMenuCarte.m
//  Naturapass
//
//  Created by giangtu on 1/17/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import "ListItemsMenuCarte.h"

//MARK :- Menu Cell
@interface MenuCell : UICollectionViewCell
@property (retain, nonatomic) IBOutlet UIButton *btn;
@property (retain, nonatomic) IBOutlet UILabel *label;

@end

@implementation MenuCell

@end


@interface ListItemsMenuCarte ()
{
    NSMutableArray *currentMenuList;
    
}
@end

@implementation ListItemsMenuCarte

- (void)viewDidLoad {
    [super viewDidLoad];
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    currentMenuList = [NSMutableArray new];
    
    // Do any additional setup after loading the view.
    UICollectionViewFlowLayout *collectionViewFlowLayout = [UICollectionViewFlowLayout new];
    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal] ;
    
    [collectionView setNeedsUpdateConstraints];
    [collectionView setNeedsLayout];
    [collectionView layoutIfNeeded];
    
    collectionViewFlowLayout.minimumInteritemSpacing = 0;
    collectionViewFlowLayout.minimumLineSpacing = 0;
    
    collectionView.collectionViewLayout = collectionViewFlowLayout;
    
    [collectionView.collectionViewLayout invalidateLayout];
    
    //Observation list tree.
    //    It permits to filter quickly differents type of datas :
    //    -TOUT -> All
    //    -ARMURERIES -> Distributor (armory)
    //    -VETERINAIRES -> Veterinary (we’ll have a db of veterinary in france)
    //    -EVENEMENTS -> Events
    //    -ANIMAUX -> publications filtered on category “Animaux”
    //    -EQUIPEMENTS -> publications filtered on category “Equipements/Aménagements”
    //    -HABITATS -> publications filtered on category “Habitats”
    
    [currentMenuList addObjectsFromArray:@[
                                           @{@"name":@"TOUT",
                                             @"tag": @(TOUT),
                                             @"status": @(1)
                                             },
                                           @{@"name":@"ARMURERIES",
                                             @"tag": @(ARMURERIES),
                                             @"status": @(0)
                                             },
                                           @{@"name":@"VETERINAIRES",
                                             @"tag": @(VETERINAIRES),
                                             @"status": @(0)
                                             },
                                           @{@"name":@"EVENEMENTS",
                                             @"tag": @(EVENEMENTS),
                                             @"status": @(0)
                                             },
                                           @{@"name":@"ANIMAUX",
                                             @"tag": @(ANIMAUX),
                                             @"status": @(0)
                                             },
                                           @{@"name":@"EQUIPEMENTS",
                                             @"tag": @(EQUIPEMENTS),
                                             @"status": @(0)
                                             },
                                           @{@"name":@"HABITATS",
                                             @"tag": @(HABITATS),
                                             @"status": @(0)
                                             }
                                           ]];
    [collectionView reloadData];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return currentMenuList.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MenuCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MenuCell" forIndexPath:indexPath];
    
    NSDictionary *dic = currentMenuList[indexPath.row];
    cell.label.text = dic[@"name"];
    
    if ([dic[@"status"] boolValue] == YES) {
        cell.label.textColor = UIColor.whiteColor;
        cell.btn.backgroundColor = UIColorFromRGB(0x0EACBC);
    }else{
        cell.label.textColor = UIColorFromRGB(0xC3C0C5);
        cell.btn.backgroundColor = UIColorFromRGB(0xFBF8F7);
    }
    
    [cell.btn addTarget:self action:@selector(fnButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    cell.btn.tag = indexPath.row;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UIFont *font=[UIFont fontWithName:@"Arial" size:16.f];
    NSDictionary *dic = currentMenuList[indexPath.row];
    
    CGSize size = [dic[@"name"] sizeWithAttributes:@{NSFontAttributeName:font}];
    
    //measure text length -> width
    return CGSizeMake(size.width + 40, 50);
}


-(void) fnResetAll
{
    [self resetStatus];
    [collectionView reloadData];
    _callbackMasterFilter(CLEAR_ALL);
}

-(void) resetStatus{
    for (int i= 0; i < currentMenuList.count; i++) {
        NSMutableDictionary *mutDic = [currentMenuList[i] mutableCopy];
        mutDic[@"status"] = @(0);
        [currentMenuList replaceObjectAtIndex:i withObject:mutDic];
    }
}

-(void) fnButtonPressed :(UIButton*) btn
{
    //reset all status;
    BOOL isClearAll = NO;
    
    
    NSMutableDictionary *mutDic = [currentMenuList[btn.tag] mutableCopy];
    
    switch ([mutDic[@"tag"] intValue]) {
        case TOUT:
        {
            [self resetStatus];

            mutDic[@"status"] = @(1);
            [currentMenuList replaceObjectAtIndex:btn.tag withObject:mutDic];
        }
            break;
            
        case ARMURERIES:
        case VETERINAIRES:
        case EVENEMENTS:
        case ANIMAUX:
        case EQUIPEMENTS:
        case HABITATS:
        {
            //if touch again ->unselect
            if ([mutDic[@"status"] boolValue] == 1) {
                mutDic[@"status"] = @(0);
                isClearAll = YES;
            }else{
                mutDic[@"status"] = @(1);
            }

            [self resetStatus];

            [currentMenuList replaceObjectAtIndex:btn.tag withObject:mutDic];
        }
            break;
        default:
            break;
    }
    
    [collectionView reloadData];
    
    //ping to parent
    
    if (_callbackMasterFilter) {
        if (isClearAll) {
            _callbackMasterFilter(CLEAR_ALL);
            
        }else{
            _callbackMasterFilter( [mutDic[@"tag"] intValue] );
        }
    }
}

@end
