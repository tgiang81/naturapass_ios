//
//  Filter_Cell_SubNode.h
//  Naturapass
//
//  Created by Manh on 10/12/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_SubNode : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightArrow;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;

@end
