//
//  MDMakersCustom.m
//  Naturapass
//
//  Created by Manh on 3/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDMarkersCustom.h"
#import "ASImageView.h"
#import "Config.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ProportionalFill.h"
/*
 else
 {
 CGSize sizeImage = MDMarkersSize;
 sizeImage.width = _heightMarker*sizeImage.width/sizeImage.height;
 sizeImage.height = _heightMarker;
 
 imageViewFrame.size = sizeImage;
 sizeLegend =11;
 }
 [imageViewMarker setFrame:imageViewFrame];
 
 //set text and  size text legend
 if ([self.dicMarker[@"owner"] isKindOfClass: [NSDictionary class] ]) {
 if (![self.dicMarker[@"legend"] isKindOfClass:[NSNull class]] && ![self.dicMarker[@"legend"] isEqualToString:@""] )
 {
 [self buildLegend];
 }
 
 }else{
 if (![self.dicMarker[@"c_legend"] isKindOfClass:[NSNull class]] && ![self.dicMarker[@"c_legend"] isEqualToString:@""] )
 {
 [self buildLegend];
 }
 }
 
 //add
 [view addSubview:lbMarker];
 [view addSubview:imageViewMarker];
 
 //set frame view
 CGRect legendFrame = lbMarker.frame;
 [view setFrame:CGRectMake(0, 0, imageViewFrame.size.width+ legendFrame.size.width, 32)];

 */
@implementation MDMarkersCustom
/*
 e.g : zoom 10 => 8 size, zoom 12 => 9, zoom 14 => 10, zoom 16 => 11, zoom 18 => 12,...
 I put wrong marker size but it's just to have an exemple
 and we will stop change at min/max level
 min zoom level (to change marker size) => 10, max => 20

 */

-(instancetype)initWithDictionary:(NSDictionary*)dicMarker
{
    self = [super init];
    if (self) {
        self.dicMarker = dicMarker;
    }
    return self;
}

- (void)doSetCalloutView {
    
    [self loadImageData];
}

-(void)loadImageData
{
    UIView *view = [[UIView alloc] init];
    
    //set size image makers
    imageViewMarker = [[UIImageView alloc] init];
    CGRect imageViewFrame = [imageViewMarker frame];
    imageViewFrame.size = MDMarkersSize;
    sizeLegend =11;
    [imageViewMarker setFrame:imageViewFrame];
    
    //set text and  size text legend
    if ([self.dicMarker[@"owner"] isKindOfClass: [NSDictionary class] ]) {
        if (![self.dicMarker[@"legend"] isKindOfClass:[NSNull class]] && ![self.dicMarker[@"legend"] isEqualToString:@""] )
        {
            [self buildLegend];
        }
        
    }else{
        if (![self.dicMarker[@"c_legend"] isKindOfClass:[NSNull class]] && ![self.dicMarker[@"c_legend"] isEqualToString:@""] )
        {
            [self buildLegend];
        }
    }
    
    //add
    [view addSubview:lbMarker];
    [view addSubview:imageViewMarker];
    
    //set frame view
    CGRect legendFrame = lbMarker.frame;
    [view setFrame:CGRectMake(0, 0, imageViewFrame.size.width+ legendFrame.size.width, imageViewFrame.size.height)];
    //get og
    percent_arrow = (imageViewFrame.origin.x + imageViewFrame.size.width/2)/view.frame.size.width;
    
    
    //set image marker
    NSString *photoDefault = @"";
    NSString * strImage = nil;
    
    if ([self.dicMarker[@"owner"] isKindOfClass: [NSDictionary class] ]) {
        //normal dic
        if ( [self.dicMarker[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
            //My publication
            photoDefault = @"green_circle_map_icon_photo";
        }else{
            //Other one
            photoDefault = @"green_carre_map_icon_photo";
        }
        
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,self.dicMarker[@"mobile_markers"][@"picto"]];
        
    }else{
        if ( [self.dicMarker[@"c_owner_id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
            //My publication
            photoDefault = @"green_circle_map_icon_photo";
        }else{
            //Other one
            photoDefault = @"green_carre_map_icon_photo";
        }
        
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,self.dicMarker[@"c_marker_picto"]];
    }
    
    
    
    NSURL *url =[NSURL URLWithString:strImage ];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager loadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
        UIImage *imageTmp = nil;
        if (!image) {
            image = [UIImage imageNamed:photoDefault];
        }
        [imageViewMarker setImage: image];
        
        imageTmp = [self imageFromWithImage:view];
        [self returnImage:imageTmp withImage:image];

    }];

}

-(void)returnImage:(UIImage*)image withImage:(UIImage*)nonLegend
{
    if (_CallBackMarkers) {
        _CallBackMarkers(image,nonLegend,percent_arrow);
    }
    
}
- (UIImage *)imageFromWithImage:(UIView*)view
{
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}
-(void)buildLegend
{
    lbMarker = [[UILabel alloc] init];
    lbMarker.backgroundColor = [UIColor whiteColor];
    lbMarker.textAlignment = NSTextAlignmentCenter;
    lbMarker.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:sizeLegend];
    if ([self.dicMarker[@"owner"] isKindOfClass: [NSDictionary class] ]) {
        lbMarker.text = self.dicMarker[@"legend"];
        
    }else{
        lbMarker.text = self.dicMarker[@"c_legend"];
        
    }
    if (lbMarker.text) {
        CGFloat fWidth = [self widthOfString:lbMarker.text withFont:lbMarker.font] + 6;
        lbMarker.frame  = CGRectMake( MDMarkersSizeWidth-3, 5, fWidth, MDMarkersSizeHeight-17);
    }
    
}
@end
