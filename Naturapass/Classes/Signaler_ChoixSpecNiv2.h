//
//  Signaler_ChoixSpecNiv2.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "SignalerBaseVC.h"


//RECURSIVE
@interface Signaler_ChoixSpecNiv2 : SignalerBaseVC

@property (nonatomic, strong) NSDictionary *myDic;
@property(nonatomic,strong) NSString * myName;

@end
