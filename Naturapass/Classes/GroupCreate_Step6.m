//
//  GroupCreate_Step6.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step6.h"
#import "GroupCreate_Step7.h"
#import "CLTokenInputView.h"
#import "APContact.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SAMTextView.h"
#import "MDCheckBox.h"
#import "APAddressBook.h"
#import "GroupCreateCell_Step4.h"
#import "TPKeyboardAvoidingScrollView.h"
//tokenVS
#import "UserListViewController.h"
#import "VSTokenInputView.h"
#import "VSToken.h"
static CGFloat kMinInputViewHeight = 43;
static CGFloat kMaxInputViewHeight = 94;
static CGFloat kLeftPadding = 15;
static CGFloat kTopPadding = 10;
static CGFloat kRightPadding = 15;
static CGFloat kBottomPadding = 10;
static NSString *kEmailKey = @"email";
static NSString *kNameKey = @"name";
//
static NSString *cellkind3 =@"GroupCreateCell_Step4";
static NSString *cellkin3ID =@"CellKind3ID";


@interface GroupCreate_Step6 ()<InputViewDelegate>
{
    IBOutlet UISearchBar    *toussearchBar;
    NSMutableArray *tourMemberArray;
    
    __weak IBOutlet UIView *viewEmail;
    __weak IBOutlet UIButton *btnEn;
    BOOL isViewEmail;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UILabel *lbDescription3;
    __weak IBOutlet UIButton *btnAucun;

}


@property (strong, nonatomic) IBOutlet SAMTextView *emailContent;

@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
//tokenVS
@property (weak, nonatomic) IBOutlet UILabel        *placehoderInputView;
@property (weak, nonatomic) IBOutlet VSTokenInputView *addedMembersInputView;
@property (weak, nonatomic) IBOutlet UIView *userListContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addedMemberViewHeight;
@property (nonatomic) UserListViewController *userListController;
@property (nonatomic) NSMutableArray *addedMembers;
@property (nonatomic) NSMutableDictionary *tokensCache;
//
@end

@implementation GroupCreate_Step6
#pragma mark - viewdid and init
- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strINVITERDESMEMBRESNATURAPASS);
    lbDescription2.text = str(strRentrezLeurNome);
    lbDescription3.text = str(strLaPersonneQueVousRecherchez);
    [btnAucun setTitle:  str(strAucunResultat) forState:UIControlStateNormal];
    self.needChangeMessageAlert = YES;

    [self checkIsViewEmail:NO];
    // Do any additional setup after loading the view from its nib.
    [self initialization];
    //tokenVS
    self.addedMembersInputView.inPutViewDelegate = self;
    self.userListContainerView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.placehoderInputView.text = str(strTokenPlaceholder);
    //

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //tockenVS
    [self configureUserListViewController];
    
}
-(void)initialization
{
    self.emailContent.placeholder = str(strEmailContentPlaceholder);

    [self.tableControl registerNib:[UINib nibWithNibName:cellkind3 bundle:nil] forCellReuseIdentifier:cellkin3ID];
    
    [toussearchBar setPlaceholder:str(strRechercherUnMembre)];
    tourMemberArray = [NSMutableArray new];
    
    //for email view
    [self initialiseVariable];
    [self InitializeKeyboardToolBar];
    [self.emailContent setInputAccessoryView:self.keyboardToolbar];
    
}
-(void)checkIsViewEmail:(BOOL)isEmail
{
    isViewEmail =isEmail;

    if (isViewEmail) {
        [self.view bringSubviewToFront:viewEmail];
        viewEmail.hidden = NO;
        if ([GroupCreateOBJ sharedInstance].isModifi) {
            [btnEn setTitle:str(strValider) forState:UIControlStateNormal ];
        }
        else
        {
            [btnEn setTitle:str(strEnvoyer) forState:UIControlStateNormal];
        }
    }
    else
    {
        viewEmail.hidden = YES;
        if ([GroupCreateOBJ sharedInstance].isModifi) {
            [btnEn setTitle:str(strValider) forState:UIControlStateNormal ];
        }
        else
        {
            [btnEn setTitle:str(strEnvoyer) forState:UIControlStateNormal];
        }


    }
}
#pragma mark -
- (void) initialiseVariable
{

}
- (void)resignKeyboard:(id)sender
{
    //tokenVS
    [self.view endEditing:YES];
    self.userListContainerView.hidden = YES;
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tourMemberArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSDictionary*dic = tourMemberArray[indexPath.row];

    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:cellkin3ID forIndexPath:indexPath];
        [cell.btnInvite setTypeCheckBox:UI_GROUP_MUR_ADMIN];

    [cell.lblTitle setText:dic[@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([dic[@"check"] boolValue] == YES) {
        [cell.btnInvite setSelected:YES];
        
    }else{
        [cell.btnInvite setSelected:NO];
        
    }
    [cell.btnInviteOver addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - action
-(IBAction)addFriend:(UIButton *)sender{
    
    NSDictionary * dic = tourMemberArray[sender.tag];
    int i=(int)sender.tag;
    if ([dic[@"check"] boolValue]) {
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }else{
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:YES] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }
    
    [self.tableControl reloadData];
}
-(IBAction)fnSend:(id)sender
{
    if (isViewEmail) {
        //tokenVS
        NSString *listEmail =self.addedMembersInputView.text;
        if (listEmail.length==0) {
            UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strAdresseEmailNonValide)
                                                              message:str(strEMPTY)
                                                             delegate:self
                                                    cancelButtonTitle:str(strOui)
                                                    otherButtonTitles:nil];
            [alert show];
            return;
        }
        if (self.emailContent.text.length==0) {
            UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strContentNonValide)
                                                              message:str(strEMPTY)
                                                             delegate:self
                                                    cancelButtonTitle:str(strOui)
                                                    otherButtonTitles:nil];
            [alert show];
            return;
        }

            [COMMON addLoading: self];
            WebServiceAPI *serviceAPI =[WebServiceAPI new];
            [serviceAPI postGroupInvitationByEmailWithGroupID: [GroupCreateOBJ sharedInstance].group_id subject:@"Invitation" toEmails:listEmail withContent:self.emailContent.text ];
            serviceAPI.onComplete =^(NSDictionary *response, int errCode)
            {
                [COMMON removeProgressLoading];
                [self gotoback];
            };
    }
    else
    {
        NSMutableArray *mutArr = [NSMutableArray new];
        
        for (int i=0; i < tourMemberArray.count; i++ )
        {
            NSDictionary*dic = tourMemberArray[i];
            if ([dic[@"check"] boolValue]) {
                [mutArr addObject:dic[@"id"]];
            }
        }
        
        if (mutArr.count > 0)
        {
            
            NSString *strMsg = @"";
            if ((int)mutArr.count  == 1) {
                strMsg = [NSString stringWithFormat:str(strVous_avez_envoye_invitation),(int)mutArr.count ];
            }else{
                strMsg = [NSString stringWithFormat:str(strVous_avez_envoyer_invitations),(int)mutArr.count ];
            }
            [UIAlertView showWithTitle:str(strTitle_app) message:strMsg
                     cancelButtonTitle:str(strOK)
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  
                                  [COMMON addLoading:self];
                                  //Request
                                  WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                                  [serviceObj fnPOST_JOIN_USER_GROUP:@{@"group": @{@"subscribers": mutArr}} groupID:[GroupCreateOBJ sharedInstance].group_id];
                                  serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                      [COMMON removeProgressLoading];
                                      /*
                                       subscribers =     (
                                       {
                                       access = 0;
                                       added = 1;
                                       "user_id" = 219;
                                       },
                                       {
                                       access = 0;
                                       added = 1;
                                       "user_id" = 161;
                                       },
                                       {
                                       access = 0;
                                       added = 1;
                                       "user_id" = 168;
                                       }
                                       );
                                       
                                       */
                                      [self gotoback];
                                  };
                              }];
            
        }
    }
    

}

#pragma mark - TextField Delegate
- (void)textViewDidEndEditing:(UITextView *)textView {
        [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [toussearchBar setText:@""];
    
    [searchBar resignFirstResponder];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    
    [theSearchBar resignFirstResponder];
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [self goLoungesSearchURL];
}

- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self goLoungesSearchURL];
    
}
#pragma mark - WebService
- (void) goLoungesSearchURL
{
    NSString *encodedString = [toussearchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if (encodedString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];

    }
    else
    {
        [COMMON addLoadingForView:self.view];
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj getUsersSearchAction:encodedString];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            NSArray *arrFriends = response[@"users"] ;
            
            [COMMON removeProgressLoading];
            
            if (arrFriends.count == 0) {
                //display error..other while-> hidden it...
                [self checkIsViewEmail:YES];
                
            }else{
                [self checkIsViewEmail:NO];
                
            }
            
            [tourMemberArray removeAllObjects];
            [tourMemberArray addObjectsFromArray:response[@"users"] ];
            for (int i=0;i<tourMemberArray.count; i++) {
                NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
                [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
                [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
            }
            [self.tableControl reloadData];
        };

    }
  }
//MARK: -tokenVS
- (NSMutableArray *)addedMembers
{
    if (!_addedMembers)
    {
        _addedMembers = [NSMutableArray array];
    }
    return _addedMembers;
}

- (NSDictionary *)tokensCache
{
    if (!_tokensCache)
    {
        _tokensCache = [NSMutableDictionary dictionary];
    }
    return _tokensCache;
}

- (void)configureUserListViewController
{
    if (!self.userListController)
    {
        self.userListController = [UserListViewController userListController];
        //        [self addChildViewController:self.userListController];
        self.userListController.view.frame = self.userListContainerView.bounds;
        [self.userListContainerView addSubview:self.userListController.view];
        //        [self.userListController didMoveToParentViewController:self];
    }
    
    __weak GroupCreate_Step6 *weakSelf = self;
    self.userListController.didScrollBlock = ^{
        [weakSelf.view endEditing:YES];
        weakSelf.userListContainerView.hidden = YES;
    };
}
- (void)layoutViewsBasedOnComposerHeight
{
    self.addedMemberViewHeight.constant = kMinInputViewHeight;
    self.addedMembersInputView.textContainerInset = UIEdgeInsetsMake(kTopPadding, kLeftPadding, kBottomPadding, kRightPadding);
}
- (void)searchUsersForText:(NSString *)searchText
{
    self.userListContainerView.hidden = NO;
    __weak GroupCreate_Step6 *weakSelf = self;
    
    [self.userListController searchUserForUserName:searchText addedUsers:self.addedMembers withSelectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        weakSelf.userListContainerView.hidden = YES;
        if (succes)
        {
            [weakSelf addUser:dicUser updateTokenView:YES];
        }
    } deselectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        if (succes)
        {
            [weakSelf removeUser:dicUser];
        }
    }];
}
- (void)addUser:(NSDictionary *)dicUser updateTokenView:(BOOL)update
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (!token)
    {
        token = [[VSToken alloc] init];
        token.name      = dicUser[kNameKey];
        token.uniqueId  = dicUser[kEmailKey];
        token.sEmail    = dicUser[kEmailKey];
        
        [self.tokensCache setValue:token forKey:token.uniqueId];
    }
    token.timeStamp = [NSDate date];
    
    [self.addedMembers addObject:dicUser];
    [self.addedMembersInputView addToken:token needsLayout:update];
}

- (void)removeUser:(NSDictionary *)dicUser
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (token)
    {
        [self.addedMembers removeObject:dicUser];
        [self.tokensCache removeObjectForKey:token.uniqueId];
        [self.addedMembersInputView removeToken:token needsLayout:YES];
    }
}

- (NSDictionary *)userForToken:(VSToken *)token
{
    NSDictionary *userForToken = nil;
    for (NSDictionary *dicUser in self.addedMembers)
    {
        if ([dicUser[kEmailKey] isEqualToString:token.uniqueId])
        {
            userForToken = dicUser;
            break;
        }
    }
    return userForToken;
}
#pragma mark InputViewDelegate
- (void)keyboardWillHide:(NSNotification*)notification {
    self.userListContainerView.hidden = YES;
}
- (void)textDidChange:(NSString *)text
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if ([text length])
    {
        [self performSelector:@selector(searchUsersForText:) withObject:text afterDelay:0.3];
    }
    else
    {
        [self.userListController resetUserListScreen];
    }
    if (self.addedMembersInputView.text.length == 0) {
        self.placehoderInputView.hidden = NO;
        
    }
    else
    {
        self.placehoderInputView.hidden = YES;
    }
}

- (void)didRemoveToken:(VSToken *)token
{
    NSDictionary *dicUser = [self userForToken:token];
    if (dicUser)
    {
        [self removeUser:dicUser];
        [self.userListController resetUserListScreen];
    }
}


- (void)didUpdateSize:(CGSize)size
{
    CGFloat height = [self correctedHeight:size.height];
    [UIView animateWithDuration:0.2 animations:^{
        self.addedMemberViewHeight.constant = height;
        [self.view layoutIfNeeded];
    }];
}

- (CGFloat)correctedHeight:(CGFloat)height
{
    CGFloat correctedHeight = 0;
    if (height < kMinInputViewHeight)
        correctedHeight = kMinInputViewHeight;
    else if (height > kMaxInputViewHeight)
        correctedHeight = kMaxInputViewHeight;
    else
        correctedHeight = height;
    
    return correctedHeight;
}

@end
