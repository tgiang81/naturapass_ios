//
//  ChassesCreateV2.m
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "ChassesCreateV2.h"
#import "ChassesCreateHeaderCell.h"
#import "ChassesCreateBaseCell.h"
#import "ChassesCreateDateCell.h"
#import "ChassesCreateTextFieldCell.h"
#import "PhotoSheetVC.h"
#import "NSDate+Extensions.h"
#import "DPTextField.h"
#import "AlertSearchMapVC.h"
#import "Publication_Carte.h"
#import "AlertSuggestView.h"
#import "HNKGooglePlacesAutocomplete.h"
#import "ChassesCreateOBJ.h"
#import "MapLocationVC.h"
#import "Define.h"
#import "ChassesCreatePersonneCell.h"
#import "AlertSuggestPersoneView.h"
#import "ChassesCreateAccessCell.h"
#import "ChassesCreateDroitsCell.h"
#import "ChassesCreateLieuCell.h"
#import "ChassesCreateMapCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Naturapass-Swift.h"
#import "NetworkCheckSignal.h"
#import "LiveHuntOBJ.h"
#import "ChassesCreateNoGroupCell.h"
#import "AlertSuggestGroupView.h"
#import "ChassesCreateTreeCell.h"
#import "AlertVC.h"
#import "AlertVosGroupesVC.h"
#import "TPKeyboardAvoidingScrollView.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierDate = @"identifierDate";
static NSString *identifierTF = @"identifierTF";

static NSString *identifierPersonne = @"identifierPersonne";
static NSString *identifierAccess = @"identifierAccess";
static NSString *identifierDroits = @"identifierDroits";

static NSString *identifierLieu = @"identifierLieu";
static NSString *identifierMap = @"identifierMap";
static NSString *identifierNoGroup = @"identifierNoGroup";
static NSString *identifierChooseGroup = @"identifierChooseGroup";
static NSString *identifierTree = @"identifierTree";
static NSString *identifierMesGroups = @"identifierMesGroups";
static double numberOfSecondsIn48h = 172800.0;



typedef enum
{
    debut_date,
    fin_date,
    
}SELECT_DATE;

typedef enum
{
    FIELD_DATE,
    FIELD_PERSONE,
    FIELD_CARTE,
    FIELD_ACCESS,
    FIELD_DROITS,
    FIELD_LIEU,
    FIELD_IMPORTER
}CHASSES_FIELD_TYPE;
typedef enum
{
    DROITS_NONE,
    DROITS_TOUT,
    DROITS_ADMIN
}CHASSES_DROITS;

@interface ChassesCreateV2 ()<UITextFieldDelegate, GMSMapViewDelegate , WWCalendarTimeSelectorProtocol>
{
    SELECT_DATE iSelectDate;
    
    NSInteger indexExpand;
    NSData                      *imageData;
    AlertSuggestView *suggestView;
    AlertSuggestPersoneView *suggestViewPersonne;
    AlertSuggestGroupView *suggestViewGroup;
    AlertVosGroupesVC *vosGroupVC;

    CLLocation *my_Placemark;
    NSMutableArray *arrPersonne;
    NSInteger typeSelected;
    BOOL inviterPresonne;
    ACCESS_KIND   m_accessKind;
    
    CHASSES_DROITS   m_droits_pub_add;
    CHASSES_DROITS   m_droits_pub_show;
    CHASSES_DROITS   m_droits_chat_add;
    CHASSES_DROITS   m_droits_chat_show;
    NSMutableArray *dicPhoto;
    NSString *strLatitude, *strLongitude, *strAltitude;
    BOOL isViewMap;
    WebServiceAPI *serviceAddress;
    NSString *strTmpDebut;
    NSString *strTmpFin;
    
    NSString *strHeadDate;
    NSMutableArray *arrGroup;
    NSDictionary *dicInvolve;
    NSMutableArray *arrTree;
    BOOL isWaitPost;
    BOOL isWaitForLoadingGroup;
    
    ChassesCreateHeaderCell *cellLieu;
    double lastTextUpdateTime;
    __weak IBOutlet TPKeyboardAvoidingScrollView *scrViewTPKeyboard;
    
    NSString *strTempCheckNom;
}
@property (strong, nonatomic)  NSString *strAddress;
@property (strong, nonatomic)  NSString *strPersonne;
@property (nonatomic, strong) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (nonatomic, strong) ChassesCreateBaseCell *cellMap;
@property (nonatomic, strong) NSMutableDictionary *dicRoot;

@end

@implementation ChassesCreateV2

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //add sub navigation
    [self.subview removeFromSuperview];
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview removeFromSuperview];
    [self addMainNav:@"MainNavChasses"];
    
    scrViewTPKeyboard.isLivehuntCreating = YES;
    
    //Set title
    subview =  [self getSubMainView];
    //
    if (self.isModify) {
        [subview.myTitle setText:@"MODIFIER UNE CHASSE"];
    }else{
        [subview.myTitle setText:@"CRÉER UNE CHASSE"];

    }
    //Change background color
    subview.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
    
    
    [self addSubNav:nil];
    
    [self.btnSAUVEGARDER.layer setMasksToBounds:YES];
    self.btnSAUVEGARDER.layer.cornerRadius= 20.0;
    
    [self.btnENVOYER.layer setMasksToBounds:YES];
    self.btnENVOYER.layer.cornerRadius= 25.0;
    
    [self.btnENVOYER setTitle:  @"DÉMARRER !" forState:UIControlStateNormal];
    
    
    self.vContainer.backgroundColor = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);
    self.viewTop.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateHeaderCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateDateCell" bundle:nil] forCellReuseIdentifier:identifierDate];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTextFieldCell" bundle:nil] forCellReuseIdentifier:identifierTF];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreatePersonneCell" bundle:nil] forCellReuseIdentifier:identifierPersonne];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateAccessCell" bundle:nil] forCellReuseIdentifier:identifierAccess];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateDroitsCell" bundle:nil] forCellReuseIdentifier:identifierDroits];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateLieuCell" bundle:nil] forCellReuseIdentifier:identifierLieu];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateMapCell" bundle:nil] forCellReuseIdentifier:identifierMap];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateNoGroupCell" bundle:nil] forCellReuseIdentifier:identifierNoGroup];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreatePersonneCell" bundle:nil] forCellReuseIdentifier:identifierChooseGroup];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateTreeCell" bundle:nil] forCellReuseIdentifier:identifierTree];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreateLieuCell" bundle:nil] forCellReuseIdentifier:identifierMesGroups];

    self.tableControl.estimatedRowHeight = 60;
    self.tableControl.estimatedSectionHeaderHeight = 40;
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.arrData = [NSMutableArray new];
    
    [_imgPhotoSelected.layer setMasksToBounds:YES];
    _imgPhotoSelected.layer.cornerRadius= 35.0;
    _imgPhotoSelected.image = [UIImage imageNamed:@"icon_chasses_placehoder"];
    NSString *currentDate = [NSDate currentStandardUTCTime];
    NSString *strDayMonth = @"";
    NSString *strHour = @"";
    
    //17/08/2018 21:13
    
    if (currentDate.length > 10) {
        strDayMonth = [currentDate substringWithRange:NSMakeRange(0, 10)];
        strHour = [currentDate substringWithRange:NSMakeRange(11, 5)];
    }
    
//    PLZ BE CAREFUL, if user already changed the title then don't touch it anymore, even if he changes the dates.
    

    _tfNom.delegate = self;
    _tfNom.tag = 100;
    
    _tfNom.text = [NSString stringWithFormat:@"CHASSE DU %@ %@",[strDayMonth stringByReplacingOccurrencesOfString:@"/" withString:@"-"], strHour];
    //save nom to check
    strTempCheckNom = _tfNom.text;
    
    
    indexExpand = 0;
    arrPersonne = [NSMutableArray new];
    inviterPresonne = TRUE;
    serviceAddress = [WebServiceAPI new];
    
    [self InitializeKeyboardToolBar];
    if([strHour intValue] <= 19 )
    {
        strTmpDebut = [NSString stringWithFormat:@"%@ 08:00",[currentDate substringWithRange:NSMakeRange(0, 10)]];
        strTmpFin = [NSString stringWithFormat:@"%@ 19:00",[currentDate substringWithRange:NSMakeRange(0, 10)]];
        
    }
    else
    {
        strTmpDebut = currentDate;
        strTmpFin = currentDate;
    }
    strHeadDate = [NSDate convertDateHeaderAgenda:currentDate];
    m_accessKind = ACCESS_SEMIPRIVATE;
    NSDictionary *dicDate   = @{@"name":@"DATE ET HEURE",
                                @"icon":@"icon_chasses_date",
                                @"type":@(FIELD_DATE)};
    NSDictionary *dicPerson = @{@"name":@"INVITER DES PERSONNES",
                                @"icon":@"icon_chasses_persone",
                                @"type":@(FIELD_PERSONE),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_search_user"
                                };
    /*
     NSDictionary *dicCarte  = @{@"name":@"DÉFINIR LE TERRITOIRE",
     @"icon":@"icon_chasses_carte",
     @"type":@(FIELD_CARTE),
     @"placehold":@"Rechercher une personne...",
     @"icontf":@"icon_chasses_address"
     };
     */
    NSDictionary *dicShare  = @{@"name":@"GESTION DES ACCÈS",
                                @"icon":@"icon_chasses_access",
                                @"type":@(FIELD_ACCESS),
                                @"placehold":@"Rechercher une personne...",
                                @"icontf":@"icon_chasses_address",
                                @"contents":@[
                                        @{@"name":@"PUBLIC",
                                          @"desc":@"Tout le monde peut rejoindre cet événement",
                                          @"icon":@"icon_chasses_lock_open",
                                          @"access":@(ACCESS_PUBLIC)
                                          },
                                        @{@"name":@"SEMI-PRIVÉ",
                                          @"desc":@"Tout le monde peut demander \nI'accès à cet événement",
                                          @"icon":@"icon_chasses_lock_close",
                                          @"access":@(ACCESS_SEMIPRIVATE)
                                          },
                                        @{@"name":@"PRIVÉ",
                                          @"desc":@"ll faut être invité pour avoir connaissance de cet événement",
                                          @"icon":@"icon_chasses_lock_close",
                                          @"access":@(ACCESS_PRIVATE)
                                          }
                                        ]
                                };
    NSDictionary *dicDroits  = @{@"name":@"GESTION DES DROITS",
                                 @"icon":@"icon_chasses_share",
                                 @"type":@(FIELD_DROITS),
                                 @"placehold":@"Rechercher une personne...",
                                 @"icontf":@"icon_chasses_address",
                                 @"contents":@[
                                         @{@"name":@"Discussions",
                                           @"access":@(0),
                                           },
                                         @{@"name":@"Publications",
                                           @"access":@(1),
                                           },
                                         ]
                                 };
    //default
    m_droits_pub_add = DROITS_TOUT;
    m_droits_pub_show = DROITS_TOUT;
    m_droits_chat_add = DROITS_TOUT;
    m_droits_chat_show = DROITS_TOUT;
    
    [self.arrData addObject:@{}];

    NSDictionary *dicLieu  = @{@"name":@"LIEU DU RDV",
                               @"icon":@"icon_chasses_lieu",
                               @"type":@(FIELD_LIEU),
                               @"placehold":@"Entrez une adresse...",
                               @"icontf":@"icon_chasses_address"};
    NSDictionary *dicImporter  = @{@"name":@"IMPORTER UNE CARTE",
                                   @"icon":@"agenda_icon_import",
                                   @"type":@(FIELD_IMPORTER),
                                   @"placehold":@"Vous devez créer/rejoindre au minimum un groupe pour avoir accès à cette fonctionnalité.",
                                   @"icontf":@"icon_sign_dropdown-1"};
    [self.arrData addObject:dicDate];
    [self.arrData addObject:dicPerson];
    if (![ChassesCreateOBJ sharedInstance].isModifi) {
        [self.arrData addObject:dicImporter];
    }
    [self.arrData addObject:dicLieu];
    //    [self.arrData addObject:dicCarte];
    [self.arrData addObject:dicDroits];
    [self.arrData addObject:dicShare];
    
    //get group
    arrGroup = [NSMutableArray new];
    
    isWaitForLoadingGroup = NO;
    
    WebServiceAPI *service = [WebServiceAPI new];

    [service getAllGroupsAdmin];
    
    service.onComplete = ^(NSDictionary *response, int errCode)
    {
        isWaitForLoadingGroup = YES;
        
        //compare to get defaul
        NSArray *arrTmp = response[@"groups"];
        for (NSDictionary*dic in arrTmp) {
            [arrGroup addObject:@{@"id":dic[@"id"],
                                  @"name":dic[@"name"],
                                  @"photo":dic[@"photo"],
                                  @"nbSubscribers": dic[@"nbSubscribers"],
                                  @"status":[NSNumber numberWithBool:NO]
                                  }];
        }
        [self.tableControl reloadData];
    };
    
    suggestView = [[AlertSuggestView alloc] initSuggestView];
    suggestViewPersonne = [[AlertSuggestPersoneView alloc] initSuggestView];
    suggestViewGroup = [[AlertSuggestGroupView alloc] initSuggestView];
    vosGroupVC = [[AlertVosGroupesVC alloc] initWithVosGroupes];
    //if tree category is nil, then get tree default
    arrTree = [NSMutableArray new];
    
    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
    NSMutableArray *arrTreeFull = [NSMutableArray new];
    for (NSDictionary*dic in treeDic[@"default"]) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            [arrTreeFull addObject:dic];
        }
    }
    [self fillDataTree:arrTreeFull];
    
    //modifi
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
        ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
        _tfNom.text =obj.strName;
        strTmpDebut = obj.strDebut;
        strTmpFin = obj.strFin;
        if(obj.imgData)
        {
            self.imgPhotoSelected.image = [UIImage imageWithData:obj.imgData];
            _btnClosePhoto.hidden = NO;
            _imgClosePhoto.hidden = NO;
        }
        m_accessKind = obj.accessKind;
        
        m_droits_pub_add = obj.allow_add ?DROITS_TOUT:DROITS_ADMIN;
        m_droits_pub_show = obj.allow_show ?DROITS_TOUT:DROITS_ADMIN;
        m_droits_chat_add = obj.allow_chat_add ?DROITS_TOUT:DROITS_ADMIN;
        m_droits_chat_show = obj.allow_chat_show ?DROITS_TOUT:DROITS_ADMIN;
        self.strAddress = obj.address;
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR) ];
    self.strAddress = [ChassesCreateOBJ sharedInstance].address;
    [self.tableControl reloadData];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//MARK: - TABLE HEADER
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    //
    if (section == 0)
    {
        return 116;
    }

    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.viewTop;
    }

    NSDictionary *dicData = self.arrData[section];
    ChassesCreateHeaderCell *cell = (ChassesCreateHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
    cell.lbTitle.text = dicData[@"name"];
    cell.imgIcon.image = [UIImage imageNamed:dicData[@"icon"]];
    if (indexExpand == section) {
        cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_up"];
    }
    else
    {
        cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow"];
    }
    cell.lbDesc.text = @"";
    switch ([dicData[@"type"] intValue]) {
        case FIELD_DATE:
        {
            cell.lbDesc.text = [NSDate convertDateHeaderAgenda:strTmpDebut];
            
        }
            break;
        case FIELD_PERSONE:
        {
            cell.lbDesc.text = [NSString stringWithFormat:@"%lu Pers.",(unsigned long)arrPersonne.count];
            
        }
            break;
        case FIELD_IMPORTER:
        {
            cell.lbDesc.text = @"";//[NSString stringWithFormat:@"%lu Pts.",(unsigned long)arrGroup.count];
            
        }
            break;
        case FIELD_ACCESS:
        {
            
            switch (m_accessKind) {
                case ACCESS_PRIVATE:
                {
                    cell.lbDesc.text = @"Privé";
                    
                }
                    break;
                case ACCESS_PUBLIC:
                {
                    cell.lbDesc.text = @"Public";
                    
                }
                    break;
                default:
                {
                    cell.lbDesc.text = @"Semi-Privé";
                    
                }
                    break;
            }
        }
            break;
        case FIELD_DROITS:
        {
            int countdroits = 0;
            //add
            if(m_droits_pub_add == DROITS_TOUT)
            {
                countdroits += 1;
            }
            
            //Conssulter
            if(m_droits_pub_show == DROITS_TOUT)
            {
                countdroits += 1;
            }
            
            //add
            if(m_droits_chat_add == DROITS_TOUT)
            {
                countdroits += 1;
            }
            //Conssulter
            if(m_droits_chat_show == DROITS_TOUT)
            {
                countdroits += 1;
            }
            if(countdroits == 4)
            {
                cell.lbDesc.text = @"Tt. le monde";
                
            }
            else if (countdroits == 0)
            {
                cell.lbDesc.text = @"Admin";
                
            }
            else
            {
                cell.lbDesc.text = @"Autre";
            }
            
        }
            break;
        case FIELD_LIEU:
        {
            cellLieu = cell;
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            float lat = appDelegate.locationManager.location.coordinate.latitude;
            float lng = appDelegate.locationManager.location.coordinate.longitude;
            if (([[ChassesCreateOBJ sharedInstance].latitude floatValue] == lat) &&
                ([[ChassesCreateOBJ sharedInstance].longitude floatValue] == lng)) {
                cell.lbDesc.text = @"Ma position";
                
            }
            else
            {
                cell.lbDesc.text = @"Lieu choisi";
                
            }
        }
            break;
        default:
            break;
    }
    [cell.btnSelected addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnSelected.tag = section;
    cell.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == self.arrData.count-1)
    {
        return 70;
    }

    return 0.5;
}
- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == self.arrData.count-1)
    {
        return self.viewFooter;
    }
    
        CGRect rect = tableView.frame;
        UIView *viewFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 0.5)];
        viewFooter.backgroundColor = [UIColor whiteColor];
        return viewFooter;
}


//Mark:- Delegate Calendar

-(void) fnChooseTime:(NSDate *)date
{
    NSLog(@"%@",date);
    //    Sat Aug 18 15:00:00 2018
    
    //Convert date local -> date Paris
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSString *date_Local = [formatter_local stringFromDate:date];
    
    ASLog(@"%@", date_Local);
    
    switch (iSelectDate) {
        case debut_date:
        {
            strTmpDebut = date_Local;
            //Auto set for textField Fin if empty
            if (strTmpFin.length >0) {
                if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                {
                    strTmpFin = strTmpDebut;
                }
            }
            else
            {
                strTmpFin = strTmpDebut;
            }
            strHeadDate = [NSDate convertDateHeaderAgenda:date_Local];
            [self.tableControl reloadData];
        }
            break;
        default:
        {
            strTmpFin = date_Local;
            
            //Auto set for textField Fin if empty
            if (strTmpDebut.length >0) {
                if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                {
                    strTmpDebut = strTmpFin;
                }
            }
            else
            {
                strTmpDebut = strTmpFin;
            }
            [self.tableControl reloadData];
        }
            break;
    }
    
}



-(void) WWCalendarTimeSelectorDone:(WWCalendarTimeSelector *)selector date:(NSDate *)date
{
    NSLog(@"%@",date);
    //    Sat Aug 18 15:00:00 2018
    
    //Convert date local -> date Paris
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSString *date_Local = [formatter_local stringFromDate:date];
    
    NSDate *compareDate = [self convertNSStringDateToNSDate:date_Local];
    
    //reset to current time-today
    if ([[NSDate date] compare: compareDate] == NSOrderedDescending)
    {
        date_Local = [formatter_local stringFromDate:[NSDate date]];
    }
    ASLog(@"%@", date_Local);
    
    switch (iSelectDate) {
        case debut_date:
        {
            strTmpDebut = date_Local;
            
            //user not yet changed the name chase -> change it automatically and reset strTmpCheckNom
            if ([strTempCheckNom isEqualToString:_tfNom.text]) {
                //change it
                NSString *strDayMonth = @"";
                NSString *strHour = @"";

                if (strTmpDebut.length > 10) {
                    strDayMonth = [strTmpDebut substringWithRange:NSMakeRange(0, 10)];
                    strHour = [strTmpDebut substringWithRange:NSMakeRange(11, 5)];
                }
                
                _tfNom.delegate = self;
                _tfNom.tag = 100;
                
                _tfNom.text = [NSString stringWithFormat:@"CHASSE DU %@ %@",[strDayMonth stringByReplacingOccurrencesOfString:@"/" withString:@"-"], strHour];
                
                //reset
                strTempCheckNom = _tfNom.text;
            }
            
            
            
            //Auto set for textField Fin if empty
            if (strTmpFin.length >0) {
                if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                {
                    strTmpFin = strTmpDebut;
                }
            }
            else
            {
                strTmpFin = strTmpDebut;
            }
            strHeadDate = [NSDate convertDateHeaderAgenda:date_Local];
            [self.tableControl reloadData];
        }
            break;
        default:
        {
            strTmpFin = date_Local;
            
            //Auto set for textField Fin if empty
            if (strTmpDebut.length >0) {
                if([[self convertNSStringDateToNSDate:strTmpFin] compare: [self convertNSStringDateToNSDate:strTmpDebut]] == NSOrderedAscending) // if start is later in time than end
                {
                    strTmpDebut = strTmpFin;
                }
            }
            else
            {
                strTmpDebut = strTmpFin;
            }
            
            //user not yet changed the name chase -> change it automatically and reset strTmpCheckNom
            if ([strTempCheckNom isEqualToString:_tfNom.text]) {
                //change it
                NSString *strDayMonth = @"";
                NSString *strHour = @"";
                
                if (strTmpDebut.length > 10) {
                    strDayMonth = [strTmpDebut substringWithRange:NSMakeRange(0, 10)];
                    strHour = [strTmpDebut substringWithRange:NSMakeRange(11, 5)];
                }
                
                _tfNom.delegate = self;
                _tfNom.tag = 100;
                
                _tfNom.text = [NSString stringWithFormat:@"CHASSE DU %@ %@",[strDayMonth stringByReplacingOccurrencesOfString:@"/" withString:@"-"], strHour];
                
                //reset
                strTempCheckNom = _tfNom.text;
            }
            [self.tableControl reloadData];
        }
            break;
    }
    
}
-(NSDate *)convertNSStringDateToNSDate:(NSString *)string
{
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    NSDate *convertedDate = [dateFormatter dateFromString:dateString];
    //[dateFormatter release];
    return convertedDate;
}
-(IBAction)fnDebutDate:(id)sender
{
    iSelectDate = debut_date;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
    }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = TRUE;
    viewController1.optionStyles.showTime = FALSE;
    if(strTmpDebut.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpDebut];
        
    }
}
-(IBAction)fnDebutHour:(id)sender
{
    iSelectDate = debut_date;
    // Get the selected date
    NSDate * selectedDate = [self convertNSStringDateToNSDate:strTmpDebut];
    
    
    NSDateComponents *componentsDay = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay) fromDate:selectedDate];
    NSInteger days = [componentsDay day];
    NSDateComponents *componentsMonth = [[NSCalendar currentCalendar] components:(NSCalendarUnitMonth) fromDate:selectedDate];
    NSInteger month = [componentsMonth month];
    NSDateComponents *componentsYear = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear) fromDate:selectedDate];
    NSInteger year = [componentsYear year];
    
    // Set the end time to 23:59:59
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:days];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    NSDate * endTime = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    // Set the start time to 00:00:00
    [comps setDay:days];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    NSDate * startTime = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    DateTimePicker *picker =  [DateTimePicker createWithMinimumDate:startTime maximumDate:endTime];
    
    picker.includeMonth = TRUE;
    if(strTmpDebut.length > 0)
    {
        picker.selectedDate =  [self convertNSStringDateToNSDate:strTmpDebut];
    }
    
    picker.completionHandler = ^(NSDate * date) {
        ASLog(@"%@", date);
        //         Fri Oct 19 06:40:52 2018
        [self fnChooseTime:date];
    };
    [picker show];
    
}

-(IBAction)fnFinDate:(id)sender
{
    iSelectDate = fin_date;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WWCalendarTimeSelector" bundle:[NSBundle mainBundle]];
    
    WWCalendarTimeSelector *viewController1 = [sb instantiateViewControllerWithIdentifier:@"WWCalendarTimeSelector"];
    viewController1.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:viewController1 animated:TRUE completion:^{
    }];
    
    viewController1.delegate = self;
    viewController1.optionStyles.showDateMonth = TRUE;
    viewController1.optionStyles.showTime = FALSE;
    if(strTmpFin.length > 0)
    {
        viewController1.optionCurrentDate = [self convertNSStringDateToNSDate:strTmpFin];
        
    }
}

-(IBAction)fnFinHour:(id)sender
{
    iSelectDate = fin_date;
    // Get the selected date
    NSDate *selectedDate = [self convertNSStringDateToNSDate:strTmpFin];
    
    // Get the day, month, year of the selected date
    NSDateComponents *componentsDay = [[NSCalendar currentCalendar] components:(NSCalendarUnitDay) fromDate:selectedDate];
    NSInteger days = [componentsDay day];
    NSDateComponents *componentsMonth = [[NSCalendar currentCalendar] components:(NSCalendarUnitMonth) fromDate:selectedDate];
    NSInteger month = [componentsMonth month];
    NSDateComponents *componentsYear = [[NSCalendar currentCalendar] components:(NSCalendarUnitYear) fromDate:selectedDate];
    NSInteger year = [componentsYear year];
    
    // Set end time to 23:59:59
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:days];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setHour:23];
    [comps setMinute:59];
    [comps setSecond:59];
    NSDate * endTime = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSLog(@"TIMETestEndTime = %@",endTime);
    
    // Set start time to 00:00:00
    [comps setDay:days];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setHour:0];
    [comps setMinute:0];
    [comps setSecond:0];
    
    NSDate * startTime = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSLog(@"TIMETest StartTime = %@",startTime);
    
    DateTimePicker *picker =  [DateTimePicker createWithMinimumDate:startTime maximumDate:endTime];
    
    picker.includeMonth = TRUE;
    if(strTmpFin.length > 0)
    {
        picker.selectedDate =  [self convertNSStringDateToNSDate:strTmpFin];
    }
    
    picker.completionHandler = ^(NSDate * date) {
        ASLog(@"%@", date);
        //         Fri Oct 19 06:40:52 2018
        [self fnChooseTime:date];
    };
    [picker show];
}

#pragma mark - TABLEVIEW
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[ChassesCreateBaseCell class]])
    {
        NSDictionary *dicData = self.arrData[indexPath.section];
        ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)cellTmp;
        cell.tfInput.delegate = self;
        
        cell.btnAddress.hidden = TRUE;
        cell.tfInput.text = @"";
        [cell.tfInput removeTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
        cell.lbTitle.attributedText = nil;
        switch ([dicData[@"type"] intValue]) {
            case FIELD_DATE:
            {
                NSDictionary *dicDebut = [self convertDateToNSTring:strTmpDebut];
                NSDictionary *dicFin = [self convertDateToNSTring:strTmpFin];
                
                cell.lbDebutDate.text = dicDebut[@"date"];
                cell.lbDebutTime.text = dicDebut[@"time"];
                
                cell.lbFinDate.text = dicFin[@"date"];
                cell.lbFinTime.text = dicFin[@"time"];
                
                [cell.btnDebutDate addTarget:self action:@selector(fnDebutDate:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnDebutHour addTarget:self action:@selector(fnDebutHour:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnFinDate addTarget:self action:@selector(fnFinDate:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnFinHour addTarget:self action:@selector(fnFinHour:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
            case FIELD_PERSONE:
            {
                if (indexPath.row == 1) {
                    [cell.btnOpenMap addTarget:self action:@selector(openMesGroups:) forControlEvents:UIControlEventTouchUpInside];
                    
                    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName: FONT_HELVETICANEUE(13)};
                    NSMutableAttributedString *mMoreNumberUser = [[NSMutableAttributedString alloc] initWithString:@"Rechercher dans mes groupes" attributes:attrs1];
                    
                    NSMutableAttributedString *mFull = [NSMutableAttributedString new];
                    [mFull appendAttributedString:mMoreNumberUser];
                    [mFull addAttribute:NSUnderlineStyleAttributeName
                                  value:[NSNumber numberWithInt:1]
                                  range:(NSRange){0,[mFull length]}];
                    cell.lbTitle.attributedText = mFull;
                    cell.imgIcon.image = [UIImage imageNamed:@"detail_icon_group"];
                }
                else
                {
                [cell addPersone:[arrPersonne copy]];
                [cell doBlockTokenDelete:^(NSInteger index) {
                    NSDictionary *dicTmp = [arrPersonne[index] copy];
                    
                    [arrPersonne removeObjectAtIndex:index];
                    [cell addPersone:[arrPersonne copy]];
                    [self.tableControl reloadData];
                    //                    [self updateSuggestView:cell.tfInput];
                    [suggestViewPersonne removeItemSelected:dicTmp];
                }];
                cell.tfInput.delegate = self;
                [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                cell.tfInput.text = self.strPersonne;
                [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                //                cell.lbTitleAmis.text = [NSString stringWithFormat:@"Inviter tous mes amis (%lu):",(unsigned long)arrPersonne.count];
                //                cell.swAmis.layer.cornerRadius = 16;
                //                [cell.swAmis setBackgroundColor:UIColorFromRGB(OFF_SWITCH)];
                //                cell.swAmis.on = inviterPresonne;
                //                [cell.swAmis addTarget:self action:@selector(swChangeAction:) forControlEvents:UIControlEventValueChanged];
                cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                }
                
                
            }
                break;
                //            case FIELD_CARTE:
                //            {
                //            }
                //                break;
            case FIELD_IMPORTER:
            {
                if ([COMMON isReachable] == NO) {
                    cell.lbDesc.text = @"Pas de connexion internet...";
                }else if (isWaitForLoadingGroup == NO)
                {
                    cell.lbDesc.text = @"Chargement...";
                }else {
                    
                    
                    if (arrGroup.count > 0)
                    {
                        if (indexPath.row == 0) {
                            cell.tfInput.delegate = self;
                            [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
                            cell.tfInput.text = dicInvolve[@"name"];
                            cell.tfInput.placeholder = @"--Choisissez un groupe--";
                            [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                            cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                            cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                            cell.contraintWidthImageLeft.constant = 10;
                        }
                        //                    else if (indexPath.row == 0) {
                        //                        cell.lbTitle.text = @"TOUS";
                        //                    }
                        else
                        {
                            NSDictionary *dicItem = arrTree[indexPath.row-1];
                            cell.lbTitle.text = dicItem[@"name"];
                            
                            [cell.btnExpandNode addTarget:self action:@selector(expandCollapseNode:) forControlEvents:UIControlEventTouchUpInside];
                            cell.btnExpandNode.tag = indexPath.row;
                            
                            [cell.btnSelectedNode addTarget:self action:@selector(selectNodeAction:) forControlEvents:UIControlEventTouchUpInside];
                            cell.btnSelectedNode.tag = indexPath.row;
                            
                            int indentation = (int)[dicItem[@"level"] intValue] * 15;
                            cell.contraintPaddingLeft.constant=indentation;
                            if ([dicItem[@"expand"] boolValue] == TRUE) {
                                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow"];
                                
                            }
                            else {
                                cell.imgArrow.image = [UIImage imageNamed:@"icon_chasses_arrow_right"];
                                
                            }
                            NSArray *childTree = dicItem[@"child"];
                            if (childTree.count == 0) {
                                cell.imgArrow.image = nil;
                            }
                            
                            switch ([dicItem[@"status"] intValue]) {
                                case NON_CHECK:
                                {
                                    [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor whiteColor] withColorBoderActive:[UIColor lightGrayColor]];
                                }
                                    break;
                                case UN_CHECK:
                                {
                                    [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:[UIColor blackColor] withColorBoderActive:[UIColor lightGrayColor]];
                                }
                                    break;
                                case IS_CHECK:
                                {
                                    [cell.btnCheckBox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:UIColorFromRGB(CHASSES_CREATE_NAV_COLOR) withColorBoderActive:[UIColor lightGrayColor]];
                                }
                                    break;
                                default:
                                    break;
                            }
                            cell.btnCheckBox.selected = TRUE;
                            [cell.btnCheckBox setTypeButton:NO];
                        }
                    }
                    else
                    {
                        cell.lbDesc.text = dicData[@"placehold"];
                    }

                }
            }
                break;
            case FIELD_ACCESS:
            {
                NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
                cell.lbTitle.text = dicItem[@"name"];
                cell.lbDesc.text = dicItem[@"desc"];
                cell.imgIcon.image = [UIImage imageNamed:dicItem[@"icon"]];
                if([dicItem[@"access"] intValue] == m_accessKind)
                {
                    cell.imgCheckbox.image = [UIImage imageNamed:@"ic_chasse_create_checkbox_active"];
                }
                else
                {
                    cell.imgCheckbox.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                }
                
            }
                break;
            case FIELD_DROITS:
            {
                NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
                cell.lbTitle.text = dicItem[@"name"];
                cell.lbTout.text = @"Tout le\nmonde";
                cell.lbAdmin.text = @"Admin";
                cell.lbDiscuter.text = @"Publier";
                cell.lbConsulter.text = @"Consulter";
                [cell.btnToutDiscuter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnAdminDiscuter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                [cell.btnToutConsulter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                [cell.btnAdminConsulter addTarget:self action:@selector(droitsAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                cell.imgToutDiscuter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgAdminDiscuter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgToutConsulter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                cell.imgAdminConsulter.image = [UIImage imageNamed:@"ic_chasse_checkbox_inactive"];
                UIImage *imageActive =[UIImage imageNamed:@"ic_chasse_create_checkbox_active"];
                //Publication
                if([dicItem[@"access"] intValue] == 1)
                {
                    cell.btnToutDiscuter.tag = 5;
                    cell.btnAdminDiscuter.tag = 6;
                    cell.btnToutConsulter.tag = 7;
                    cell.btnAdminConsulter.tag = 8;
                    
                    cell.lbDiscuter.text = @"Publier";
                    //add
                    if(m_droits_pub_add == DROITS_TOUT)
                    {
                        cell.imgToutDiscuter.image = imageActive;
                    }
                    else if (m_droits_pub_add == DROITS_ADMIN)
                    {
                        cell.imgAdminDiscuter.image = imageActive;
                    }
                    
                    //Conssulter
                    if(m_droits_pub_show == DROITS_TOUT)
                    {
                        cell.imgToutConsulter.image = imageActive;
                        
                    }
                    else if (m_droits_pub_show == DROITS_ADMIN)
                    {
                        cell.imgAdminConsulter.image = imageActive;
                    }
                }
                //Discussion
                else
                {
                    cell.lbDiscuter.text = @"Discuter";
                    cell.btnToutDiscuter.tag = 1;
                    cell.btnAdminDiscuter.tag = 2;
                    cell.btnToutConsulter.tag = 3;
                    cell.btnAdminConsulter.tag = 4;
                    //add
                    if(m_droits_chat_add == DROITS_TOUT)
                    {
                        cell.imgToutDiscuter.image = imageActive;
                    }
                    else if (m_droits_chat_add == DROITS_ADMIN)
                    {
                        cell.imgAdminDiscuter.image = imageActive;
                    }
                    
                    //Conssulter
                    if(m_droits_chat_show == DROITS_TOUT)
                    {
                        cell.imgToutConsulter.image = imageActive;
                        
                    }
                    else if (m_droits_chat_show == DROITS_ADMIN)
                    {
                        cell.imgAdminConsulter.image = imageActive;
                    }
                }
                
            }
                break;
            case FIELD_LIEU:
            {
                if(indexPath.row == 0)
                {
                    cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                    cell.tfInput.placeholder = dicData[@"placehold"];
                    [cell.btnOpenMap addTarget:self action:@selector(currentLocationAction:) forControlEvents:UIControlEventTouchUpInside];
                    self.cellMap = cell;
                    cell.tfInput.delegate = self;
                    [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
//                    cell.tfInput.text = self.strAddress;
                    [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
                    cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
                }
                else
                {
                    
                    cell.mapView_.delegate = self;
                    cell.ratioMap.active = NO;
                    [cell fnSetLocation:[PublicationOBJ sharedInstance].latitude withLongitude:[PublicationOBJ sharedInstance].longtitude];
                    [cell.btnCurrentLocation setImage:nil forState:UIControlStateNormal];
                    cell.btnCurrentLocation.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
                    cell.imgCatCenter.image = [UIImage imageNamed:@"ic_cat_center"];
                    
                    [cell.btnCloseMap setImage:[[UIImage imageNamed:@"ic_cat_zoom"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                    cell.btnCloseMap.tintColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
                    cell.btnCloseMap.backgroundColor = [UIColor whiteColor];
                    
                    [cell.btnCloseMap addTarget:self action:@selector(extendMap:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnCurrentLocation.hidden= NO;
                    

                    //
//                    if (isViewMap) {
//                        [cell.btnCloseMap addTarget:self action:@selector(closeLieuMap:) forControlEvents:UIControlEventTouchUpInside];
//                        cell.mapView_.delegate = self;
//                        [cell fnSetLocation:[ChassesCreateOBJ sharedInstance].latitude withLongitude:[ChassesCreateOBJ sharedInstance].longitude];
//
//                    }
//                    else
//                    {
//                        [cell.btnOpenMap addTarget:self action:@selector(openLieuMap:) forControlEvents:UIControlEventTouchUpInside];
//                        self.cellMap = cell;
//                    }
                    
                }
            }
                break;
            default:
                cell.imageLeftTF.image = [UIImage imageNamed:dicData[@"icontf"]];
                cell.tfInput.placeholder = dicData[@"placehold"];
                break;
        }
    }
    
}
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.arrData.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 0;
    }
    
    
    if (indexExpand == section)
    {
        NSDictionary *dicData = self.arrData[section];
        
        if([dicData[@"type"] intValue] == FIELD_ACCESS)
        {
            return 3;
        }
        if([dicData[@"type"] intValue] == FIELD_DROITS)
        {
            return 2;
        }
        if([dicData[@"type"] intValue] == FIELD_LIEU)
        {
            return 2;
        }
        if([dicData[@"type"] intValue] == FIELD_PERSONE)
        {
            return 2;
        }
        if([dicData[@"type"] intValue] == FIELD_IMPORTER)
        {
            if (arrGroup.count > 0) {
                //if user not yet selected any group => Don't show Tree.
                if([dicData[@"type"] intValue] == FIELD_IMPORTER && arrGroup.count > 0)
                {
                    if(dicInvolve )
                    {
                        return 1 + arrTree.count;

                    }
                }
                //else
                //only show text field to input group to search/select
                return 1;

            }
            else
            {
                return 1;
            }
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ChassesCreateBaseCell *cell = nil;
    NSDictionary *dicData = self.arrData[indexPath.section];
    switch ([dicData[@"type"] intValue]) {
        case FIELD_DATE:
        {
            cell = (ChassesCreateDateCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierDate forIndexPath:indexPath];
            
        }
            break;
        case FIELD_PERSONE:
        {
            if (indexPath.row == 1) {
                cell = (ChassesCreateLieuCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMesGroups forIndexPath:indexPath];
            }
            else
            {
                cell = (ChassesCreatePersonneCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPersonne forIndexPath:indexPath];
            }
            
        }
            break;
        case FIELD_IMPORTER:
        {
            
            //Check if user selected any group?
            
            if (arrGroup.count > 0)
            {
                if (indexPath.row == 0) {
                    cell = (ChassesCreatePersonneCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierChooseGroup forIndexPath:indexPath];
                    
                }
                else
                {
                    cell = (ChassesCreateTreeCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTree forIndexPath:indexPath];
                }
            }
            else
            {
                cell = (ChassesCreateNoGroupCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierNoGroup forIndexPath:indexPath];
            }
            
        }
            break;
        case FIELD_ACCESS:
        {
            cell = (ChassesCreateAccessCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierAccess forIndexPath:indexPath];
            
        }
            break;
        case FIELD_DROITS:
        {
            cell = (ChassesCreateDroitsCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierDroits forIndexPath:indexPath];
            
        }
            break;
        case FIELD_LIEU:
        {
            if(indexPath.row == 0)
            {
                cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            }
            else
            {
                if(isViewMap)
                {
                    cell = (ChassesCreateMapCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMap forIndexPath:indexPath];
                    
                }
                else
                {
                    cell = (ChassesCreateLieuCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierLieu forIndexPath:indexPath];
                }
                
            }
            
        }
            break;
        default:
            cell = (ChassesCreateTextFieldCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierTF forIndexPath:indexPath];
            break;
    }
    
    if (cell) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    if([dicData[@"type"] intValue] == FIELD_IMPORTER && arrGroup.count > 0 && indexPath.row > 0)
    {
        if(dicInvolve)
        {
            cell.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
        }
        else
        {
            cell.backgroundColor = UIColorFromRGB(0x4C4C4C);
            
        }
    }
    else
    {
        cell.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self resignKeyboard:nil];
    NSDictionary *dicData = self.arrData[indexPath.section];
    if([dicData[@"type"] intValue] == FIELD_ACCESS)
    {
        NSDictionary *dicItem = dicData[@"contents"][indexPath.row];
        m_accessKind = [dicItem[@"access"] intValue];
        [self.tableControl reloadData];
    }
    else if([dicData[@"type"] intValue] == FIELD_DROITS)
    {
    }
}
-(IBAction)expandAction:(id)sender
{
    [self resignKeyboard:nil];
    if (indexExpand == [sender tag]) {
        indexExpand = -1;
    }
    else
    {
        indexExpand = [sender tag];
        NSDictionary *dicData = self.arrData[indexExpand];
        if ([dicData[@"type"] intValue] == FIELD_LIEU) {
            //show MAP as default when click on RDV
            isViewMap = TRUE;
        }
    }
    [self.tableControl reloadData];

}
- (IBAction)profileImageOptions:(id)sender{
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    vc.expectTarget = ISCREATEAGENDA;
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.imgPhotoSelected.image = imgData;
        _btnClosePhoto.hidden = NO;
        _imgClosePhoto.hidden = NO;
        //resize image
        imageData = UIImageJPEGRepresentation([[AppCommon common] resizeImage:imgData targetSize:CGSizeMake(128, 128)], 1.0);
    }];
    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:vc animated:NO completion:^{}];
    
}
- (IBAction)closePhotoAction:(id)sender{
    _imgPhotoSelected.image = [UIImage imageNamed:@"icon_chasses_placehoder"];
    _btnClosePhoto.hidden = YES;
    _imgClosePhoto.hidden = YES;
    imageData = nil;
}
-(NSDictionary *)convertDateToNSTring:(NSString *)string
{
    
    //- check parameter validity
    if( string == nil )
        return nil;
    
    //- process request
    NSString *dateString = string;
    
    
    NSDateFormatter *inputDateFormatter = [[NSDateFormatter alloc] init];
    
    [inputDateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [inputDateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [inputDateFormatter setDateFormat: @"dd/MM/yyyy HH:mm"];
    NSDate *date = [inputDateFormatter dateFromString:dateString];
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [dateFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [dateFormatter setDateFormat: @"dd MMMM yyyy-HH:mm"];
    NSString *convertedDate = [dateFormatter stringFromDate:date];
    if (convertedDate) {
        NSArray *arrDate = [convertedDate componentsSeparatedByString:@"-"];
        return @{@"date":arrDate[0],@"time":arrDate[1]};
    }
    return nil;
}
-(IBAction)showAlertSearch:(id)sender
{
    /*
     AlertSearchMapVC *vcSearch = [[AlertSearchMapVC alloc] init];
     
     [vcSearch doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
     //enable request
     self.strAddress = addressString;
     [self.tableControl reloadData];
     }];
     [vcSearch showAlert];
     */
    
}
-(void)createSuggestViewWithSuperView:(UIView*)view withRect:(CGRect)rect withIndexPath:(NSIndexPath*)indexPath
{
    switch (typeSelected) {
        case FIELD_CARTE:
        case FIELD_LIEU:
        {
            [suggestView fnDataInput:self.strAddress];
            [suggestView doBlock:^(CLLocation *m_Placemark, NSString *addressString) {
                [self resignKeyboard:nil];
                if (m_Placemark) {
                    my_Placemark = m_Placemark;
                    self.strAddress = addressString;
                    
                    float lat = m_Placemark.coordinate.latitude;
                    float lng = m_Placemark.coordinate.longitude;
                    
                    [ChassesCreateOBJ sharedInstance].address = self.strAddress ;
                    [ChassesCreateOBJ sharedInstance].latitude =[NSString stringWithFormat:@"%f",lat];
                    [ChassesCreateOBJ sharedInstance].longitude = [NSString stringWithFormat:@"%f",lng];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableControl reloadData];
                    });
                    
                }
                
            }];
            [suggestView showAlertWithSuperView:view withRect:rect];
        }
            break;
        case FIELD_PERSONE:
        {
            [suggestViewPersonne fnDataInput:self.strPersonne withArrSelected:arrPersonne];
            [suggestViewPersonne doBlock:^(NSArray *arrResult) {
                [self resignKeyboard:nil];
                
                //reset input text
                //                ChassesCreateBaseCell *cell = [self.tableControl cellForRowAtIndexPath:indexPath];
                //                cell.tfInput.text = self.strPersonne;
                
                self.strPersonne = @"";

                //check if exist person
                
                for (NSDictionary*dDic in arrResult) {
                    
                    BOOL isExist = NO;
                    
                    for (NSDictionary*dExist in arrPersonne) {
                        if([dDic[@"id"] intValue] == [dExist[@"id"] intValue])
                        {
                            isExist = YES;
                            break;
                        }

                    }
                    
                    if (isExist == NO)
                    {
                        [arrPersonne addObject:dDic];
                    }
                }
                
//                if(arrResult)
//                {
//                    [arrPersonne addObjectsFromArray:arrResult];
//                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableControl reloadData];
                });
                
            }];
            [suggestViewPersonne showAlertWithSuperView:view withRect:rect];
            
        }
            break;
        case FIELD_IMPORTER:
        {
            [suggestViewGroup fnDataInput:dicInvolve[@"name"] withSelected:dicInvolve withArrFullData:arrGroup];
            [suggestViewGroup doBlock:^(NSDictionary *dicResult) {
                [self resignKeyboard:nil];
                dicInvolve = [dicResult copy];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableControl reloadData];
                });
                
            }];
            [suggestViewGroup showAlertWithSuperView:view withRect:rect];
            
        }
            break;
        default:
            break;
    }
    
}
//-(void)updateSuggestView:(UITextField *)textField
//{
//    UIView *parent = [textField superview];
//    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
//        parent = parent.superview;
//    }
//    
//    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
//    
//    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
//    
//    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
//    CGRect rectTf = cell.viewInput.frame;
//    
//    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
//    
//    [suggestView showAlertWithSuperView:self.tableControl withRect:rectSubView];
//}
-(void)removeSuggestView
{
    if([suggestViewPersonne isDescendantOfView:self.tableControl]) {
        [suggestViewPersonne closeAction:nil];
    }
    if([suggestView isDescendantOfView:self.tableControl]) {
        [suggestView closeAction:nil];
    }
    if([suggestViewGroup isDescendantOfView:self.tableControl]) {
        [suggestViewGroup closeAction:nil];
    }
}

-(IBAction)extendMap:(id)sender
{
    AlertVC *vc = [[AlertVC alloc] initAlertMap];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        [self.tableControl reloadData];
    }];
    [vc fnSetexpectTarget:self.expectTarget withColor:UIColorFromRGB(CHASSES_CREATE_NAV_COLOR)];
    [vc showAlert];
    
}

//MARK:- searBar
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    [self removeSuggestView];
    
}
-(IBAction)swChangeAction:(UISwitch*)sender
{
    inviterPresonne = sender.on;
    [self.tableControl reloadData];
}
-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    NSString *strsearchValues= searchText.text;
    strsearchValues= [strsearchValues
                      stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    searchText.text=strsearchValues;
    
    dispatch_queue_t serialQueue = dispatch_queue_create("dispatch_queue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(serialQueue, ^{
        NSLog(@"Tap detected");
        lastTextUpdateTime = [[NSDate date] timeIntervalSince1970] * 1000;
        NSLog(@"Time last (textChange) %f",lastTextUpdateTime);
        [NSThread sleepForTimeInterval:1.0];
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self checkTypingUserSearchFinish:searchText.text lastTime:lastTextUpdateTime];
            NSLog(@"Tap finish");
        });
    });
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //100 is field name
    if (textField.tag != 100) {
        NSString *strsearchValues= textField.text;
        strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        textField.text=strsearchValues;
        [self processLoungesSearchingURL:strsearchValues];
    }
    
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{    
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    NSDictionary *dicData = self.arrData[indexPath.section];
    typeSelected = [dicData[@"type"] intValue];
    [self createSuggestViewWithSuperView:self.tableControl withRect:rectSubView withIndexPath:indexPath];
    [self.tableControl bringSubviewToFront:cell.viewInput];
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [self removeSuggestView];
}

-(void) checkTypingUserSearchFinish: (NSString *)searchText lastTime: (double) lastTime {
    double nowTime = [[NSDate date] timeIntervalSince1970] * 1000;
    NSLog(@"Time now %f",nowTime);
    NSLog(@"Time last (checkTyping) %f",lastTime);
    if (nowTime > lastTime + 1000 - 500) {
        NSLog(@"Time ok");
        [self processLoungesSearchingURL:searchText];
    } else {
        NSLog(@"Time NOK");
    }
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    switch (typeSelected) {
        case FIELD_CARTE:
        case FIELD_LIEU:
        {
            self.strAddress = searchText;
            
            [suggestView processLoungesSearchingURL:searchText];
        }
            break;
        case FIELD_PERSONE:
        {
            self.strPersonne = searchText;
            [suggestViewPersonne processLoungesSearchingURL:searchText];
        }
            break;
        case FIELD_IMPORTER:
        {
            [suggestViewGroup processLoungesSearchingURL:searchText];
        }
            break;
        default:
            break;
    }
}
-(IBAction)openMapLocation:(id)sender
{
    Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
    if ([ChassesCreateOBJ sharedInstance].latitude && [ChassesCreateOBJ sharedInstance].longitude) {
        viewController1.simpleDic = @{@"latitude":  [ChassesCreateOBJ sharedInstance].latitude,
                                      @"longitude":  [ChassesCreateOBJ sharedInstance].longitude};
    }
    viewController1.expectTarget = ISCREATEAGENDA;
    [self pushVC:viewController1 animate:YES expectTarget:ISCREATEAGENDA iAmParent:NO];
    
}
-(IBAction)openLieuMap:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    //    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    //    NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:0 inSection:indexPath.section];
    
    isViewMap = !isViewMap;
    if (!isViewMap) {
        //hide suggest
        [self resignKeyboard:nil];
    }
    [self.tableControl reloadData];
    //    [self.tableControl scrollToRowAtIndexPath:lastIndexPath
    //                         atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
-(IBAction)closeLieuMap:(id)sender
{
    isViewMap = FALSE;
    [self.tableControl reloadData];
}
-(IBAction)currentLocationAction:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    strLatitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.latitude];
    
    strLongitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.longitude];
    [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
    [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
    [self.tableControl reloadData];
}
//MARK: - DROITS
-(IBAction)droitsAction:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    NSInteger tag = [sender tag];
    switch (tag) {
            //ToutDiscuter Chat
        case 1:
        {
            m_droits_chat_add = DROITS_TOUT;
        }
            break;
            //AdminDiscuter Chat
        case 2:
        {
            m_droits_chat_add = DROITS_ADMIN;
            
        }
            break;
            //ToutConsulter Chat
        case 3:
        {
            m_droits_chat_show = DROITS_TOUT;
        }
            break;
            //AdminConsulter Chat
        case 4:
        {
            m_droits_chat_show = DROITS_ADMIN;
        }
            break;
            //ToutDiscuter Pub
        case 5:
        {
            m_droits_pub_add = DROITS_TOUT;
        }
            break;
            //AdminDiscuter Pub
        case 6:
        {
            m_droits_pub_add = DROITS_ADMIN;
            
        }
            break;
            //ToutConsulter Pub
        case 7:
        {
            m_droits_pub_show = DROITS_TOUT;
            
        }
            break;
            //AdminConsulter Pub
        case 8:
        {
            m_droits_pub_show = DROITS_ADMIN;
            
        }
            break;
        default:
            break;
    }
    [self.tableControl reloadData];
}
-(void)fnSetTextDescLieu
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    float lat = appDelegate.locationManager.location.coordinate.latitude;
    float lng = appDelegate.locationManager.location.coordinate.longitude;
    if (([[ChassesCreateOBJ sharedInstance].latitude floatValue] == lat) &&
        ([[ChassesCreateOBJ sharedInstance].longitude floatValue] == lng)) {
        cellLieu.lbDesc.text = @"Ma position";
        
    }
    else
    {
        cellLieu.lbDesc.text = @"Lieu choisi";
        
    }
}
//MARK: - MAPVIEW DELEGATE

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = mapView.camera.target.latitude;
    float lng = mapView.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
    [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(fnGetAddress) withObject:nil afterDelay:1];
    [self fnSetTextDescLieu];
}
-(void)fnGetAddress
{
    if(strLatitude.length == 0 && strLongitude.length == 0)
    {
        return;
    }
    /*
     __weak typeof(self) wself = self;
     [serviceAddress cancelTask];
     [serviceAddress getInfoLocation:strLatitude withLng:strLongitude];
     
     serviceAddress.onComplete = ^(NSDictionary*response, int errCode){
     NSLog(@"address : %@",response);
     if ([response[@"results"] isKindOfClass: [NSArray class]] && [response[@"status"] isEqualToString:@"OK"])
     {
     
     NSArray *retArr = response[@"results"];
     
     NSString * strLocation = @"";
     
     BOOL alreadyGotAddress = NO;
     
     for (NSDictionary*mDic in retArr)
     {
     NSArray*tmpArr = mDic[@"types"];
     
     if ([tmpArr containsObject :@"administrative_area_level_1"])
     {
     alreadyGotAddress = YES;
     
     strLocation =  mDic[@"formatted_address"];
     
     break;
     }
     }
     
     if (alreadyGotAddress == NO) {
     if (retArr.count > 0) {
     NSDictionary*mDic = retArr[0];
     strLocation =  mDic[@"formatted_address"];
     }
     }
     wself.strAddress = strLocation;
     wself.cellMap.tfInput.text = wself.strAddress;
     }
     
     };
     */
    __weak typeof(self) wself = self;
    if ([COMMON isReachable])
    {
        [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
            
            //i got the address
            [wself fnSetAddress:mAddress];
            
        } withData:@{@"latitude" : strLatitude,
                     @"longitude" : strLongitude}];
    }
}
-(void)fnSetAddress:(NSString*)mAddress
{
    //i got the address
    self.strAddress = mAddress;
    
    //NN-347: 1st remove the automatic address, address should be empty in LIEU DU RDV => reset
    self.cellMap.tfInput.text = @"";
//    self.cellMap.tfInput.text = self.strAddress;
    //    [self.tableControl reloadData];
}
//MARK: - CREATE AGENDA
- (IBAction)annulerAgendaAction:(id)sender {
    [self gotoback];
}
- (IBAction)createAgendaAction:(id)sender {
    // lay data hien thi len giao dien
    /*
     NSArray *allKey = self.dicRoot.allKeys;
     allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
     for (id key in allKey) {
     NSDictionary *dicCategory = self.dicRoot[key];
     if ([dicCategory[@"status"] intValue] == IS_CHECK) {
     [arrObsevation addObject:@{@"id": dicCategory[@"id"],
     @"name": dicCategory[@"name"]
     }];
     }
     }
     */
    strLatitude = [ChassesCreateOBJ sharedInstance].latitude;
    strLongitude = [ChassesCreateOBJ sharedInstance].longitude;
    
    if (strLatitude == nil || strLongitude == nil) {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        strLatitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.latitude];
        
        strLongitude=[NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.longitude];
    }
    
    NSString *strName = [_tfNom.text emo_emojiString];
    
    [ChassesCreateOBJ sharedInstance].strName =strName;
    [ChassesCreateOBJ sharedInstance].strComment =@"";
    
    if (imageData) {
        [ChassesCreateOBJ sharedInstance].imgData =imageData;
    }
    
    if(strName.length == 0)
    {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
        
        return;
    }
    
    NSDateFormatter *dateFormatter = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    NSString *strEndDate= strTmpFin;
    NSDate *finDate = [dateFormatter dateFromString: strEndDate];
    //
    NSString *strStartDate= strTmpDebut;
    NSDate *DebutDate = [dateFormatter dateFromString: strStartDate];
    
    // get current date
    NSString *strCurrentDate = [NSDate currentStandardUTCTime];
    NSDate *currentDate = [dateFormatter dateFromString:strCurrentDate];
    
    NSTimeInterval intervalBetweenNowAndStartDate = (NSTimeInterval)[DebutDate timeIntervalSinceDate:currentDate];
    
    
    
    
    /**/
    
    
    if([DebutDate compare: finDate] == NSOrderedDescending) // if start is later in time than end
    {
        strTmpFin = @"";
        [self.tableControl reloadData];
        // do something
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strAlertTimeIssue) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [salonAlert show];
        return;
    }
    [ChassesCreateOBJ sharedInstance].strFin = [dateFormatter stringFromDate:finDate];
    [ChassesCreateOBJ sharedInstance].strDebut = [dateFormatter stringFromDate:DebutDate];
    
    if(strLatitude.length == 0 || strLongitude.length == 0)
    {
        UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRemplirtoutleschamps) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [salonAlert show];
        return;
    }
    
    [ChassesCreateOBJ sharedInstance].allow_add = (m_droits_pub_add == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_show = (m_droits_pub_show == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_chat_add = (m_droits_chat_add == DROITS_TOUT)? YES : NO;
    [ChassesCreateOBJ sharedInstance].allow_chat_show = (m_droits_chat_show == DROITS_TOUT)? YES : NO;
    
    [ChassesCreateOBJ sharedInstance].accessKind =m_accessKind;
    
    ChassesCreateOBJ *obj =[ChassesCreateOBJ sharedInstance];
    
    NSString *strAccess =[NSString stringWithFormat:@"%u",obj.accessKind];
    
    UIImage *image = [UIImage imageWithData:obj.imgData];
    NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
    
    NSDictionary *attachment;
    if(image)
    {
        attachment = @{@"kFileName": @"image.png",
                       @"kFileData": fileData};
    }
    NSDictionary *dicGeo = nil;
    //mld: address
    NSString * strLocation = self.strAddress;
    
    if (strLocation)
    {
        dicGeo =@{@"address":strLocation,
                  @"latitude":strLatitude,
                  @"longitude":strLongitude};
    }else{
        dicGeo =@{@"address":@"",
                  @"latitude":strLatitude,
                  @"longitude":strLongitude};
    }
    //Member
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < arrPersonne.count; i++ )
    {
        NSDictionary*dic = arrPersonne[i];
        [mutArr addObject:dic[@"id"]];
    }
    NSString *strPersonne = [mutArr componentsJoinedByString:@","];
    NSMutableDictionary *dicLounge = [@{@"name":obj.strName,
                                        @"description":obj.strComment,
                                        @"geolocation":@"0", //1 option for admin to allow user select GEO or not... Now is disabled
                                        @"access":strAccess,
                                        @"meetingAddress":dicGeo,
                                        @"meetingDate":obj.strDebut,
                                        @"endDate":obj.strFin,
                                        //ggtt
                                        @"allow_add":  [NSNumber numberWithBool:obj.allow_add]  ,
                                        @"allow_show": [NSNumber numberWithBool:obj.allow_show],
                                        @"allow_add_chat":  [NSNumber numberWithBool:obj.allow_chat_add]  ,
                                        @"allow_show_chat": [NSNumber numberWithBool:obj.allow_chat_show]
                                        
                                        } mutableCopy];
    if (strPersonne.length > 0) {
        [dicLounge setObject:strPersonne forKey:@"invitePersons"];
        
    }
    NSMutableDictionary* postDict = [@{@"lounge":   dicLounge} mutableCopy];
    //TOUS
    if (dicInvolve) {
        [postDict setObject:dicInvolve[@"id"] forKey:@"groups[]"];
        
        NSMutableArray *arrCategorySelected = [NSMutableArray new];
        NSArray *allKey = self.dicRoot.allKeys;
        allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
        
        for (id key in allKey) {
            NSDictionary *dic = self.dicRoot[key];
            if ([dic[@"id"] intValue] != -1 && [dic[@"status"] intValue] == IS_CHECK) {
                [arrCategorySelected addObject:dic[@"id"]];
            }
        }
        NSString *strCategory = [arrCategorySelected componentsJoinedByString:@","];
        if (strCategory.length > 0) {
            [postDict setObject:strCategory forKey:@"categories[]"];
            
        }
        
    }
    [ChassesCreateOBJ sharedInstance].attachment = [attachment copy];
    [ChassesCreateOBJ sharedInstance].address = strLocation;
    [ChassesCreateOBJ sharedInstance].latitude = strLatitude;
    [ChassesCreateOBJ sharedInstance].longitude = strLongitude;
    
    //update
    
    if (obj.isModifi) {
        //put as json...without image
        [[ChassesCreateOBJ sharedInstance]  modifiChassesWithVC:self];
        //auto complete invite persion
        if (mutArr.count > 0) {
            //Request
            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
            [serviceObj fnPOST_JOIN_USER_LOUNGE:@{@"lounge": @{@"subscribers": mutArr}} loungeID:obj.strID];
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            };
        }

    }else{
        [self getTree];
        //post multi-form
        
        dicPhoto =[NSMutableArray new];
        
        //SET DATA FORM
        [self processParsedObjectPhoto: postDict];
        
        //create new
        if(isWaitPost) return;
        isWaitPost = TRUE;
        [COMMON addLoadingForView:self.view];
        WebServiceAPI *serviceAPI =[WebServiceAPI new];
        [serviceAPI postLoungePhotoAction:dicPhoto withAttachmentMediaDic:attachment];
        serviceAPI.onComplete =^(id response, int errCode)
        {
            isWaitPost = FALSE;
            if (response[@"sqlite"] ) {
                
                [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                    NSArray *arrSqls = response[@"sqlite"] ;
                    
                    for (NSString*strQuerry in arrSqls) {
                        //correct API
                        NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                        
                        ASLog(@"%@", tmpStr);
                        
                        [db  executeUpdate:tmpStr];
                        
                    }
                }];
            }
            
            if (response[@"sqlite_agenda"] ) {
                
                [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                    NSArray *arrSqls = response[@"sqlite"] ;
                    
                    for (NSString*strQuerry in arrSqls) {
                        //correct API
                        NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                        
                        ASLog(@"%@", tmpStr);
                        
                        [db  executeUpdate:tmpStr];
                        
                    }
                }];
            }
            
            
            if (response[@"sqlite_carte"] ) {
                
                [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                    NSArray *arrSqls = response[@"sqlite"] ;
                    
                    for (NSString*strQuerry in arrSqls) {
                        //correct API
                        NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                        
                        ASLog(@"%@", tmpStr);
                        
                        [db  executeUpdate:tmpStr];
                        
                    }
                }];
            }
            
            
            if ( [response[@"hunt"] isKindOfClass:[NSDictionary class]]) {
                WebServiceAPI *serviceObjXXX = [[WebServiceAPI alloc]init];
                
                [serviceObjXXX fnGET_DASHBOARD_LIVE_HUNT];;
                
                serviceObjXXX.onComplete = ^(NSDictionary*responseXXX, int errCode){
                    
                    [COMMON removeProgressLoading];

                        //update data
                        if ([responseXXX[@"live"] isKindOfClass: [NSArray class]]) {
                            
                            NSArray *result = responseXXX[@"live"];
                            //save to cache file...will be loaded in Dashboard.
                            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],LIVE_HUNT_HOME_SAVE)   ];
                            // Write array
                            [result writeToFile:strPath atomically:YES];



                            for (NSDictionary*dddDic in result) {
                                if ([dddDic[@"id"] intValue] == [response[@"hunt"][@"id"] intValue]) {
                                    //
                                    [LiveHuntOBJ sharedInstance].dicLiveHunt = dddDic;
                                    [GroupEnterOBJ sharedInstance].dictionaryGroup = dddDic;
                                    break;
                                }
                            }
                            
                            //continue ...
                            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                            //                        //update DB.
                            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                            //                        [app updateAllowShowAdd:response];
                            //
                            [app fnScheduleWithListLivehunt: result];

                            [ChassesCreateOBJ sharedInstance].strID  = response[@"hunt"][@"id"];
                            
                            //
                            //check if The Hunt's startdate is more than 48h -> show Alert.
                            if(intervalBetweenNowAndStartDate > numberOfSecondsIn48h ) {
                                
                                AlertVC *vc = [[AlertVC alloc] initAlert48h];
                                
                                [vc doBlock:^(NSInteger index, NSString *strContent) {
                                    
                                    switch ((int)index) {
                                            //ACCUEIL -> Homepage
                                        case 0:
                                        {
                                            [self.navigationController popToRootViewControllerAnimated:YES];
                                        }
                                            break;
                                            
                                        case 1:
                                        {
                                            //show my agenda
                                            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                            [appDelegate showAgenda];
                                            
                                        }
                                            break;
                                            
                                        default:
                                            break;
                                    }
                                }];
                                [vc showAlert];
                                
                                return;
                            } else {
                                //Get live hunt and go...to save time.
                                
                                
                                [app showLiveHunt];
                            }

                        }else if (responseXXX == nil) {
                        }
                        
                    
                    
                };

            }
            
        };
        
    }
}

-(void)processParsedObjectPhoto:(id)object{
    [self processParsedObjectPhoto:object depth:0 parent:nil path:nil];
}

-(void)processParsedObjectPhoto:(id)object depth:(int)depth parent:(id)parent path:(NSString*)strPath{
    
    if([object isKindOfClass:[NSDictionary class]]){
        
        for(NSString * key in [object allKeys]){
            id child = [object objectForKey:key];
            if (!strPath) {
                [self processParsedObjectPhoto:child depth:depth+1 parent:object path: key];
            }else{
                [self processParsedObjectPhoto:child depth:depth+1 parent:object path: [NSString stringWithFormat:@"%@[%@]",strPath,key ]];
            }
        }
    }else if([object isKindOfClass:[NSArray class]]){
        
        for(id child in object){
            [self processParsedObjectPhoto:child depth:depth+1 parent:object path:strPath];
        }
        
    }
    else{
        
        [dicPhoto addObject:@{@"path": strPath, @"value": [NSString stringWithFormat:@"%@",[object description]] }];
        
    }
}
-(void)getTree
{
    
    if (strLatitude.length>0 && strLongitude.length>0 && [COMMON isReachable])
    {
        //But bad network?
        if (self.expectTarget != ISLOUNGE) {
            [COMMON addLoading:self];
        }
        
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj getCategoriesGeolocated:strLatitude strLng:strLongitude];
        
        serviceObj.onComplete = ^(NSDictionary*response1, int errCode){
            //Has Data
            if (![response1[@"model"] isKindOfClass: [NSNull class]])
            {
                
                if ([response1[@"model"] isKindOfClass: [NSString class]])
                {
                    if ([response1[@"model"] isEqualToString:@"default"])
                    {
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                        // get tree/card from tree cache default
                    }
                    else
                    {
                        // Receiver_1....                                     "model": "receiver_7"
                        NSString *strReceiver = response1[@"model"];
                        
                        //check in the cache
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        if (treeDic[strReceiver] != [NSNull class] && treeDic[strReceiver])
                        {
                            [PublicationOBJ sharedInstance].treeCategory = treeDic[strReceiver];
                            
                        }
                        else //doesn't exist...
                        {
                            strReceiver = [strReceiver stringByReplacingOccurrencesOfString:@"_" withString:@"="];
                            
                            //receiver_7
                            
                            WebServiceAPI *serviceGetTree =[WebServiceAPI new];
                            serviceGetTree.onComplete =^(id response2, int errCode){
                                if ([response2 isKindOfClass: [NSDictionary class]]) {
                                    
                                    //if not contained -> request get new tree... then merge result to the cache
                                    [[PublicationOBJ sharedInstance] doMergeTreeWithNew:response2];
                                    //Get needed tree
                                    
                                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                                    
                                    [PublicationOBJ sharedInstance].treeCategory = treeDic[response1[@"model"]];
                                    
                                }
                                else
                                {
                                    [COMMON removeProgressLoading];
                                }
                                
                            };
                            
                            [serviceGetTree fnGET_CATEGORIES_BY_RECEIVER:strReceiver];
                            
                        }
                    }
                }
                else if ([response1[@"model"] isKindOfClass: [NSDictionary  class]])
                {
                    //a real tree. => merge..
                    [COMMON removeProgressLoading];
                    
                }
                //A TREE
                else if ([response1[@"tree"] isKindOfClass: [NSArray  class]])
                {
                    [PublicationOBJ sharedInstance].treeCategory = response1[@"tree"];
                    
                    return;
                }else{
                    //Offline
                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                    
                    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                    
                }
            }else{
                //Offline
                NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                
                [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                
            }
        };
        
    }
    else
    {
        [AZNotification showNotificationWithTitle:str(strNETWORK) controller:self notificationType:AZNotificationTypeError];
    }
}
-(void)inviteMemberWitdChasses:(NSString*)strID
{
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < arrPersonne.count; i++ )
    {
        NSDictionary*dic = arrPersonne[i];
        [mutArr addObject:dic[@"id"]];
    }
    
    //Request
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_USER_LOUNGE:@{@"lounge": @{@"subscribers": mutArr}} loungeID:strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self gotoback];
    };
}
- (void)keyboardWillHide:(NSNotification*)notification {
    [self removeSuggestView];
}
//MARK: - TREE
-(void)fillDataTree:(NSArray*)listTree
{
    self.dicRoot = [NSMutableDictionary new];
    //TOUS
    NSMutableDictionary *node = [NSMutableDictionary new]; //[dic mutableCopy];
    [node setValue:@(-1) forKey:@"id"];
    [node setValue:@"TOUS" forKey:@"name"];
    [node setValue:@(1) forKey:@"level"];
    [node setValue:@(5) forKey:@"type"];
    [node setValue:@(0) forKey:@"lineBottom"];
    [node setValue:@(FALSE) forKey:@"expand"];
    [self.dicRoot setObject:node forKey:@(-1)];
    
    [self fillData:listTree nodeLevel:1 nodeParent:nil];
    
    // lay data hien thi len giao dien
    NSArray *allKey = self.dicRoot.allKeys;
    allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
    for (id key in allKey) {
        NSDictionary *dic = self.dicRoot[key];
        if ([dic[@"level"] intValue] == 1) {
            [arrTree addObject:dic];
        }
    }
    
}
- (void)fillData:(NSArray *)childrenArray nodeLevel:(NSUInteger)nodeLevel nodeParent:(NSDictionary*)nodeParent
{
    for (NSDictionary *dic in childrenArray) {
        
        NSMutableDictionary *node = [NSMutableDictionary new]; //[dic mutableCopy];
        [node setValue:dic[@"id"] forKey:@"id"];
        [node setValue:dic[@"name"] forKey:@"name"];
        [node setValue:@(nodeLevel) forKey:@"level"];
        [node setValue:@(5) forKey:@"type"];
        [node setValue:@(0) forKey:@"lineBottom"];
        [node setValue:@(FALSE) forKey:@"expand"];
        //        [node setValue:@(NON_CHECK) forKey:@"status"];
        
        if (nodeParent) {
            NSMutableDictionary *dicParentTMP = [NSMutableDictionary new];
            if (self.dicRoot[nodeParent[@"id"]]) {
                dicParentTMP = [self.dicRoot[nodeParent[@"id"]] mutableCopy];
            }
            else
            {
                dicParentTMP = [nodeParent mutableCopy];
            }
            // add parent
            NSMutableArray *arrParent = [NSMutableArray new];
            if (dicParentTMP[@"parent"]) {
                arrParent = [dicParentTMP[@"parent"] mutableCopy];
            }
            [arrParent addObject:dicParentTMP[@"id"]];
            [node setValue:arrParent forKey:@"parent"];
            
            // add child
            
            
            NSMutableArray *arrChild = [NSMutableArray new];
            if (dicParentTMP[@"child"]) {
                arrChild = [dicParentTMP[@"child"] mutableCopy];
            }
            [arrChild addObject:dic[@"id"]];
            [dicParentTMP setValue:arrChild forKey:@"child"];
            [self.dicRoot setObject:dicParentTMP forKey:nodeParent[@"id"]];
            
            
        }
        
        [self.dicRoot setObject:node forKey:dic[@"id"]];
        if (dic[@"children"]) {
            [self fillData:dic[@"children"] nodeLevel:nodeLevel+1 nodeParent:node];
        }
        
    }
}
- (IBAction)selectNodeAction:(id)sender
{
    // if not choose group then can't choose tree
    if(!dicInvolve)
    {
        return;
    }
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSInteger index = indexPath.row - 1;
    NSMutableDictionary *dic = [arrTree[index] mutableCopy];
    
    int status = NON_CHECK;
    switch ([dic[@"status"] intValue]) {
        case NON_CHECK:
        {
            status = IS_CHECK;
            
        }
            break;
        case UN_CHECK:
        {
            status = IS_CHECK;
        }
            break;
        default:
            break;
    }
    //TOUS
    if ([dic[@"id"] intValue] == -1) {
        [self selectedTOUS:status];
        [self.tableControl reloadData];
        return;
    }
    
    [dic setObject:@(status) forKey:@"status"];
    [self.dicRoot setObject:dic forKey:dic[@"id"]];
    
    [self reChangeStatusChild:dic status:status];
    [self reChangeStatusParent:dic];
    [arrTree removeAllObjects];
    NSArray *allKey = self.dicRoot.allKeys;
    allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
    
    int countCheck = 0;
    for (id key in allKey) {
        NSDictionary *dic = self.dicRoot[key];
        //CHECK TOUS
        if ([dic[@"id"] intValue] != -1 && [dic[@"status"] intValue] == IS_CHECK) {
            countCheck += 1;
        }
        if ([dic[@"level"] intValue] == 1) {
            [self reFillData:dic];
        }
    }
    int checkTOUS = (countCheck == allKey.count - 1)?IS_CHECK:NON_CHECK;
    id idTous = @(-1);
    NSMutableDictionary *dicTmp = [self.dicRoot[idTous] mutableCopy];
    [dicTmp setObject:@(checkTOUS) forKey:@"status"];
    [self.dicRoot setObject:dicTmp forKey:idTous];
    [arrTree replaceObjectAtIndex:0 withObject:dicTmp];
    
    [self.tableControl reloadData];
}
-(void)reChangeStatusParent:(NSDictionary *)node
{
    // add parent
    if (node[@"parent"]) {
        NSMutableArray *arrParent = [node[@"parent"] mutableCopy];
        for (NSInteger i = arrParent.count - 1; i >= 0; i--) {
            id indexParent = arrParent[i];
            if (self.dicRoot[indexParent]) {
                NSMutableDictionary *dicTMP = [self.dicRoot[indexParent] mutableCopy];
                NSArray *childNode = dicTMP[@"child"];
                if (childNode) {
                    //Count Status
                    int countCheck = 0;
                    int countUnCheck = 0;
                    for (id indexChild in childNode) {
                        NSDictionary *dicChild = self.dicRoot[indexChild];
                        if ([dicChild[@"status"] intValue] == IS_CHECK) {
                            countCheck++;
                        }
                        else if ([dicChild[@"status"] intValue] == UN_CHECK) {
                            countUnCheck++;
                        }
                    }
                    //Change Status
                    int status = NON_CHECK;
                    
                    if(countCheck == childNode.count)
                    {
                        status = IS_CHECK;
                    }
                    else if ((countCheck>0 && countCheck< childNode.count) || (countUnCheck>0 && countUnCheck < childNode.count))
                    {
                        status = UN_CHECK;
                    }
                    // re-Change status
                    [dicTMP setObject:@(status) forKey:@"status"];
                    [self.dicRoot setObject:dicTMP forKey:dicTMP[@"id"]];
                }
                
            }
        }
    }
}
-(void)reChangeStatusChild:(NSDictionary *)node status:(int)status
{
    NSMutableDictionary *dicTMP = [node mutableCopy];
    [dicTMP setObject:@(status) forKey:@"status"];
    [self.dicRoot setObject:dicTMP forKey:dicTMP[@"id"]];
    
    NSArray *childrenArray = dicTMP[@"child"];
    for (id indexNode in childrenArray) {
        NSDictionary *dic = self.dicRoot[indexNode];
        [self reChangeStatusChild:dic status:status];
    }
}
-(void)reFillData:(NSDictionary *)node
{
    [arrTree addObject:node];
    if ([node[@"expand"] boolValue]) {
        NSArray *childrenArray = node[@"child"];
        for (id indexNode in childrenArray) {
            NSDictionary *dic = self.dicRoot[indexNode];
            [self reFillData:dic];
        }
        
    }
    
}
-(void)selectedTOUS:(CARTER_CHECK)isCheck
{
    NSArray *allKey = self.dicRoot.allKeys;
    allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
    for (id key in allKey) {
        NSMutableDictionary *dic = [self.dicRoot[key] mutableCopy];
        [dic setObject:@(isCheck) forKey:@"status"];
        [self.dicRoot setObject:dic forKey:key];
        
    }
    [arrTree removeAllObjects];
    for (id key in allKey) {
        NSDictionary *dic = self.dicRoot[key];
        if ([dic[@"level"] intValue] == 1) {
            [self reFillData:dic];
        }
    }
}
- (void)expandCollapseNode:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSInteger index =indexPath.row - 1;
    NSMutableDictionary *dicMul = [arrTree[index] mutableCopy];
    BOOL blExpand = ![dicMul[@"expand"] boolValue];
    [dicMul setObject:@(blExpand) forKey:@"expand"];
    
    [self.dicRoot setObject:dicMul forKey:dicMul[@"id"]];
    
    [arrTree removeAllObjects];
    NSArray *allKey = self.dicRoot.allKeys;
    allKey = [allKey sortedArrayUsingSelector: @selector(compare:)];
    
    for (id key in allKey) {
        NSDictionary *dic = self.dicRoot[key];
        if ([dic[@"level"] intValue] == 1) {
            [self reFillData:dic];
        }
    }
    [self.tableControl reloadData];
}
//MARK: - OPEN MES GROUPS
-(IBAction)openMesGroups:(id)sender
{
    [vosGroupVC doBlock:^(NSArray *arrResult) {
        [self resignKeyboard:nil];
        if(arrResult)
        {
            arrPersonne = [arrResult mutableCopy];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableControl reloadData];
        });
    }];
    [vosGroupVC showAlertWithArrSelected:arrPersonne];
}
@end
