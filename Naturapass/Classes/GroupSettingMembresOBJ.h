//
//  GroupSettingOBJ.h
//  Naturapass
//
//  Created by Manh on 8/5/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GroupSettingMembresOBJ : NSObject
@property (nonatomic, strong)  NSString *Title;
@property (nonatomic, strong)  NSString *UrlImgAvatar;
@property (nonatomic, strong)  NSString *UrlImgCrown;

@end
