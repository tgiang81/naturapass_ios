//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "Define.h"
typedef void (^GroupSearchAlertViewCallback)(NSInteger index);
@interface GroupSearchAlertView : UIViewController
{
    IBOutlet UIView *subAlertView;
    IBOutlet UIButton                   *btnClose;
}
@property (nonatomic,copy) GroupSearchAlertViewCallback callback;
@property (nonatomic,strong) IBOutlet UILabel *titleLabel;
@property (nonatomic,assign) ISSCREEN exp;

-(void)doBlock:(GroupSearchAlertViewCallback ) cb;
-(void)showInVC:(UIViewController*)vc;
- (id)initWithTitle:(NSString*)title Desc:(NSString*)desc;
@end
