//
//  GroupSettingVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChasseSettingMembres.h"
#import "GroupAdminListVC.h"
#import "GroupSettingOBJ.h"
#import "GroupSettingMembresCell2.h"
#import "GroupMembersHeader.h"
//#import "CellKind2.h"
#import "TypeCell51.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GroupEnterOBJ.h"
#import "CommonHelper.h"
#import "MDCheckDel.h"
#import "TypeCell11.h"
#import "TypeCell12.h"
#import "ChassesSettingMembersParticipeVC.h"
#import "ChassesMemberOBJ.h"
#import "ChassesCreate_Step5.h"
#import "ChasseCreatePersonneFictif.h"
#import "DatabaseManager.h"

static NSString *cellIdentifier2 = @"Cell2";
static NSString *GroupesCellAdmin2 = @"GroupSettingMembresCell2";
//
static NSString *cell12 = @"TypeCell12";
static NSString *cellID12 = @"cellID12";
//
static NSString *cellIdentifier3 = @"TypeCell51";
static NSString *identifierSection3 = @"TypeCell51ID";
//
static NSString *cell11 = @"TypeCell11";
static NSString *cellID11 = @"cellID11";
@interface ChasseSettingMembres ()
{


    NSArray        *HeaderTitle;
    NSMutableArray *groupMemberArray;
    NSMutableArray *arrNontMembers;
    NSMutableArray *arrOwner;
    NSMutableArray *arrAdmin;
    NSMutableArray *arrMembers;
    NSMutableArray *arrInvite;
    NSMutableArray *arrWatting;
    int owner_id;
    int sender_id;

    NSMutableDictionary *dicCountPar;
    IBOutlet UIButton *btnInvite;
    SELECT_PARTICIPATION filterParticipation;
}
@property (nonatomic, strong) TypeCell12 *prototypeCell;
@property (nonatomic, strong) IBOutlet UIButton *btnCreateGroup;
@end

@implementation ChasseSettingMembres

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableControl registerNib:[UINib nibWithNibName:cell12 bundle:nil] forCellReuseIdentifier:cellID12];
    [self.tableControl registerNib:[UINib nibWithNibName:GroupesCellAdmin2 bundle:nil] forCellReuseIdentifier:cellIdentifier2];
    [self.tableControl registerNib:[UINib nibWithNibName:cellIdentifier3 bundle:nil] forCellReuseIdentifier:identifierSection3];
    [self.tableControl registerNib:[UINib nibWithNibName:cell11 bundle:nil] forCellReuseIdentifier:cellID11];



    HeaderTitle =@[@"",str(strProprietaire),str(strAdministrateur),str(strMMembres),str(strIInvites),str(strNon_membres),str(strEn_attente_de_validation)];

    [[CommonHelper sharedInstance] setRoundedView:self.btnCreateGroup toDiameter:self.btnCreateGroup.frame.size.height /2];

    groupMemberArray =[NSMutableArray new];
    arrNontMembers =[NSMutableArray new];
    
    arrOwner =[NSMutableArray new];
    arrAdmin =[NSMutableArray new];
    arrMembers =[NSMutableArray new];
    arrInvite =[NSMutableArray new];
    arrWatting =[NSMutableArray new];
    owner_id =(int)[[GroupEnterOBJ sharedInstance].dictionaryGroup[@"owner"][@"id"] intValue];
    sender_id= [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];

    dicCountPar =[NSMutableDictionary new];
    
    if (self.isNotifi) {
        [self.subview removeFromSuperview];
        [self addSubNav:@"SubNavigationMUR"];
        
        
        self.isNotifi =!self.isNotifi;
    }

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.isNotifi) {
        self.isNotifi=!self.isNotifi;
    }
    filterParticipation = SELECT_NON;
    [self getSubcribeMember:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
    if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
        switch ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"] intValue]) {
            case 3:
            {
                btnInvite.hidden=NO;
            }
                break;
            case 0:
            case 2:
            {
                btnInvite.hidden =YES;
            }
                break;
            default:
                break;
        }
    }else{
        btnInvite.hidden =YES;

    }

}
/*
 "non_members" =     (
 {
 firstname = Nicolas;
 fullname = "Nicolas Mendez";
 id = 114;
 lastname = Mendez;
 participation = 1;
 privateComment = prive;
 publicComment = public;
 }
 
 subscribers =     (
 {
 access = 3;
 geolocation = 0;
 participation = 2;
 quiet = 0;
 user =             {
 courtesy = 0;
 firstname = Ba;
 fullname = "Ba Ba";
 id = 3529;
 lastname = Ba;
 parameters =                 {
 friend = 1;
 };
 photo = "/uploads/users/images/thumb/ce4fb951593326c0f7d3a09fb52ad055358ec276.jpeg";
 relation =                 {
 friendship =                     {
 state = 2;
 way = 3;
 };
 mutualFriends = 0;
 };
 usertag = "ba-ba";
 };
 },
 */
#pragma mark - get SubcriberMember
-(void)getSubcribeMember:(NSString*)mykind_id
{

    [groupMemberArray removeAllObjects];
    [arrOwner removeAllObjects];
    [arrAdmin removeAllObjects];
    [arrMembers removeAllObjects];
    [arrInvite removeAllObjects];
    [arrNontMembers removeAllObjects];
    [arrWatting removeAllObjects];

    [dicCountPar setObject:@"0" forKey:@"Par"];
    [dicCountPar setObject:@"0" forKey:@"Sais"];
    [dicCountPar setObject:@"0" forKey:@"Pas"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if(self.expectTarget ==ISLOUNGE)
    {
        //mld
        [serviceObj getLoungeSubscribeAction:mykind_id];
        
    }
    else
    {
        [serviceObj getGroupToutesSubscribeAction:mykind_id];
        
    }
    __weak typeof(self) wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        NSMutableArray *friendArray = [[NSMutableArray alloc] init];
        
        friendArray=[response objectForKey:@"subscribers"];
        [arrNontMembers addObjectsFromArray:response[@"non_members"]];
        if (arrNontMembers.count>0) {
            for (NSDictionary *dic in arrNontMembers) {
                [self countParticipion:dic];
            }
        }
        if(friendArray.count>0)
        {
            [groupMemberArray addObjectsFromArray:[friendArray copy]];
            [self fillData];
        }
        [self.tableControl reloadData];
    };
}
-(void)fillData
{
    [arrOwner removeAllObjects];
    [arrAdmin removeAllObjects];
    [arrMembers removeAllObjects];
    [arrInvite removeAllObjects];
    [arrNontMembers removeAllObjects];
    [arrWatting removeAllObjects];
    [dicCountPar removeAllObjects];
    for (NSDictionary *dic in groupMemberArray) {
        // dem so participation
        [self countParticipion:dic];
        BOOL isAddValue = NO;
        if (filterParticipation == SELECT_NON) {
            isAddValue = YES;
        }
        else
        {
            int countPar=(int)[dic[@"participation"] integerValue];
            if ((countPar == 0 && filterParticipation == SELECT_NEPARTICIPEPAS)||//Ne partticipe pas
                (countPar == 1 && filterParticipation == SELECT_PARTICIPE)||//Participe
                (countPar == 2 && filterParticipation == SELECT_NESAISPAS))//sais pas
            {
                isAddValue = YES;
            }
        }
        if (isAddValue) {
            //new chinh la owner
            if ([dic[@"user"][@"id"] integerValue] ==owner_id ) {
                [arrOwner addObject:dic];
            }
            else
            {
                switch ([dic[@"access"] intValue]) {
                    case USER_ADMIN:
                    {
                        [arrAdmin addObject:dic];
                    }
                        break;
                    case USER_NORMAL:
                    {
                        [arrMembers addObject:dic];
                    }
                        break;
                    case USER_INVITED:
                    {
                        [arrInvite addObject:dic];
                    }
                        break;
                    case USER_WAITING_APPROVE:
                    {
                        [arrWatting addObject:dic];
                    }
                        break;
                        
                    default:
                        break;
                }
            }
        }
    }

}
-(void)countParticipion:(NSDictionary*)dic
{
    // dem so participation
    int countPar=(int)[dic[@"participation"] integerValue];
    switch (countPar) {
        case 0:// Ne partticipe pas
        {
            int temp = [dicCountPar[@"Pas"] intValue];
            temp =temp+1;
            [dicCountPar removeObjectForKey:@"Pas"];
            [dicCountPar setObject:[NSString stringWithFormat:@"%d",temp] forKey:@"Pas"];
        }
            break;
        case 1://Participe
        {
            int temp = [dicCountPar[@"Par"] intValue];
            temp =temp+1;
            [dicCountPar removeObjectForKey:@"Par"];
            [dicCountPar setObject:[NSString stringWithFormat:@"%d",temp] forKey:@"Par"];
        }
            break;
        case 2://sais pas
        {
            int temp = [dicCountPar[@"Sais"] intValue];
            temp =temp+1;
            [dicCountPar removeObjectForKey:@"Sais"];
            [dicCountPar setObject:[NSString stringWithFormat:@"%d",temp] forKey:@"Sais"];
        }
            break;
        default:
            break;
    }

}
/*"subscribersNotMember": [
                         {
                             "id": 114,
                             "fullname": "Nicolas Mendez",
                             "firstname": "Nicolas",
                             "lastname": "Mendez",
                             "publicComment": "public",
                             "privateComment": "prive",
                             "participation": 1
                         },]
 */
-(void)getLoungeSubscriberNotMember
{
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceAPI = [WebServiceAPI new];
    [serviceAPI getLoungeSubscriberNotMember:@""];
    serviceAPI.onComplete = ^(NSDictionary*response, int errCode){
        if([wself fnCheckResponse:response]) return;
    };
}
#pragma mark - gotinvition
-(IBAction)gotoInvition:(id)sender
{
    [[ChassesMemberOBJ sharedInstance] resetParams];
    [ChassesMemberOBJ sharedInstance].strMyKindId = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];

    ChassesCreate_Step5 *viewController1 = [[ChassesCreate_Step5 alloc] initWithNibName:@"ChassesCreate_Step5" bundle:nil];
    viewController1.arrSubcriber =groupMemberArray;
    viewController1.fromSetting=YES;
    [ChassesCreateOBJ sharedInstance].strID=[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    [self pushVC:viewController1 animate:YES];
    

}
#pragma mark - validerAction
-(IBAction)validerAction:(UIButton *)sender{
    
    NSMutableArray *arr =[ NSMutableArray new];
    arr= arrWatting;
    int tag = (int)[sender tag];
    int index =tag-1000;
    NSString *groupid= [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    NSString *UserId= arr[index][@"user"][@"id"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE mykind_id:groupid strUserID:UserId ];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        //update db
        
        if (response[@"sqlite"] ) {
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    ASLog(@"%@", tmpStr);
                    
                    [db  executeUpdate:tmpStr];
                    
                }
            }];
        }

        if (response[@"sqlite_agenda"] ) {
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    ASLog(@"%@", tmpStr);
                    
                    [db  executeUpdate:tmpStr];
                    
                }
            }];
        }

        [self getSubcribeMember:groupid];
    };
}
#pragma mark - doAction
-(IBAction)deleteJoinAction:(UIButton *)sender {
    NSMutableArray *arr =[ NSMutableArray new];
    int tag = (int)[sender tag];
    int index =tag-1100;

    arr= arrWatting;
    NSString *groupid= [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    NSString *UserId= arr[index][@"user"][@"id"];
    
    [COMMON addLoading:self];
    WebServiceAPI *deleteLoungAction = [[WebServiceAPI alloc]init];
    [deleteLoungAction deleteJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE UserID:UserId mykind_id:groupid];
    deleteLoungAction.onComplete =^(NSDictionary *response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        [self getSubcribeMember:groupid];
    };
    
    
}
#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    NSMutableArray *arr =[ NSMutableArray new];
    int tag = (int)[sender tag];
    int index =0;
    if (tag<2000) {
        arr= arrOwner;
        index =tag-1000;
    }
    else if (tag<3000 && tag>=2000)
    {
        arr= arrAdmin;
        index =tag-2000;
    }
    else if (tag<4000 && tag>=3000)
    {
        arr= arrMembers;
        index =tag-3000;
        
    }
    else if (tag<5000 && tag>=4000)
    {
        arr= arrInvite;
        index =tag-4000;
        
    }
    [COMMON addLoading:self];
    NSDictionary *dic = arr[index];
    
    NSString *groupid= [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"user"][@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            [self getSubcribeMember:groupid];

//            //co qh
//            NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
//            NSMutableDictionary *userDic = [mulDic[@"user"] mutableCopy];
//            
//            [userDic removeObjectForKey:@"relation"];
//            [userDic setObject:response[@"relation"] forKey:@"relation"];
//            [mulDic setObject:userDic forKey:@"user"];
//            
//            for (int i=0; i<groupMemberArray.count; i++) {
//                NSDictionary *temp =groupMemberArray[i];
//                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
//                    [groupMemberArray replaceObjectAtIndex:i withObject:mulDic];
//                    break;
//                }
//            }
//            //reload item
//            NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
//            [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }
    };
}
#pragma mark - Filter patici
-(IBAction)filterParticipe:(UIButton*)sender
{
    [self filterPaticipation:SELECT_PARTICIPE];
}
-(IBAction)filterNeSaisPas:(UIButton*)sender
{
    [self filterPaticipation:SELECT_NESAISPAS];
}
-(IBAction)filterNeParticipePas:(UIButton*)sender
{
    [self filterPaticipation:SELECT_NEPARTICIPEPAS];
}
-(void)filterPaticipation:(SELECT_PARTICIPATION)local_filterPartici
{
    if (filterParticipation == local_filterPartici) {
        filterParticipation = SELECT_NON;
    }
    else
    {
        filterParticipation = local_filterPartici;
    }
    [self fillData];
    [self.tableControl reloadData];
}
#pragma mark -  TableView Delegates
- (TypeCell12 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:cellID12];
    }
    return _prototypeCell;
}
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[TypeCell12 class]])
    {
        TypeCell12 *cell = (TypeCell12 *)cellTmp;
        NSMutableArray *arr =[ NSMutableArray new];
        //owner
        if (indexPath.section==1) {
            arr= arrOwner;
            cell.btnAddFriend.tag=1000+indexPath.row;
        }
        //admin
        else if (indexPath.section==2)
        {
            arr= arrAdmin;
            cell.btnAddFriend.tag=2000+indexPath.row;
        }
        //members
        else if (indexPath.section ==3)
        {
            arr= arrMembers;
            cell.btnAddFriend.tag=3000+indexPath.row;

        }
        //invited
        else if (indexPath.section ==4)
        {
            arr= arrInvite;
            cell.btnAddFriend.tag=4000+indexPath.row;

        }
        
        if(indexPath.section== 1)
        {
            //crown
            cell.imgCrown.hidden=NO;
        }
        else
        {
            cell.imgCrown.hidden=YES;
        }
        
        int indexArr =(int)indexPath.row;
        NSDictionary *dic = arr[indexArr];
        BOOL isAddFriendHide =YES;
        if ([dic[@"user"][@"id"] integerValue] ==sender_id) {
            isAddFriendHide= YES;
        }
        else
        {
            NSDictionary *dicRelation =dic[@"user"][@"relation"];
            if ([dicRelation isKindOfClass:[NSDictionary class]]) {
                
                if ([dicRelation[@"friendship"] isKindOfClass:[NSDictionary class]])
                {
                    isAddFriendHide =YES;
                }else{
                    isAddFriendHide=NO;
                }
            }else{
                isAddFriendHide=NO;
            }
        }

        [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];

        if (isAddFriendHide) {
            cell.btnAddFriend.hidden =YES;
            cell.imgAddFriend.hidden=YES;

        }else{
            cell.btnAddFriend.hidden=NO;
            cell.imgAddFriend.hidden=NO;

            [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];

        }

        
        cell.lbTitle.text = dic[@"user"][@"fullname"];
        //image
        NSString *strImage;
        
        if (dic[@"user"][@"profilepicture"] != nil) {
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"profilepicture"]];
        }else{
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"photo"]];
        }
        
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        [cell.imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
        [cell getParticipationName:[dic[@"participation"] intValue]];
        cell.lbDescription.text =dic[@"publicComment"];
        [cell layoutIfNeeded];
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
}
- (void)configureCellNonMembers:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[TypeCell12 class]])
    {
        TypeCell12 *cell = (TypeCell12 *)cellTmp;
        NSMutableArray *arr =[ NSMutableArray new];
        arr =arrNontMembers;
        int indexP = (int)indexPath.row;
        NSString *strFullName = arr[indexP][@"fullname"];
        strFullName = [strFullName stringByReplacingOccurrencesOfString:@"\r\n"
                                             withString:@""];
        cell.lbTitle.text = strFullName;
        
        int Partic =(int)[arr[indexP][@"participation"] integerValue];
        [cell getParticipationName:Partic];
        
        cell.lbDescription.text =arr[indexP][@"publicComment"];
        
        cell.imgCrown.hidden=YES;
        cell.btnAddFriend.hidden =YES;
        cell.imgAddFriend.hidden=YES;
        cell.imgAvatar.image =[UIImage imageNamed:@"profile"];
        [cell layoutIfNeeded];
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
}
//Ne participe pas
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *arr =[ NSMutableArray new];
    if (indexPath.section==0) {
        //is participants
        TypeCell11 *cell = (TypeCell11 *)[self.tableControl dequeueReusableCellWithIdentifier:cellID11 forIndexPath:indexPath];
        cell.lbParticipe.text = dicCountPar[@"Par"];
        cell.lbNeSaisPas.text = dicCountPar[@"Sais"];
        cell.lbNeParticipePas.text = dicCountPar[@"Pas"];
        cell.btnParticipe.tag= indexPath.row+2000;
        [cell.btnParticipe addTarget:self action:@selector(filterParticipe:) forControlEvents:UIControlEventTouchUpInside];

        cell.btnNeSaisPas.tag= indexPath.row+3000;
        [cell.btnNeSaisPas addTarget:self action:@selector(filterNeSaisPas:) forControlEvents:UIControlEventTouchUpInside];

        cell.btnNeParticipePas.tag= indexPath.row+4000;
        [cell.btnNeParticipePas addTarget:self action:@selector(filterNeParticipePas:) forControlEvents:UIControlEventTouchUpInside];

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }else if (indexPath.section== 6) {
        //is invite
        arr= arrWatting;
        GroupSettingMembresCell2 *cell1 = (GroupSettingMembresCell2 *)[self.tableControl dequeueReusableCellWithIdentifier:cellIdentifier2 forIndexPath:indexPath];
        //title
        cell1.lbTitle.text = arr[indexPath.row][@"user"][@"fullname"];
        //image
        
        NSString *strImage;
        
        if (arr[indexPath.row][@"user"][@"profilepicture"] != nil) {
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,arr[indexPath.row][@"user"][@"profilepicture"]];
        }else{
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,arr[indexPath.row][@"user"][@"photo"]];
        }
        
        
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        [cell1.imgViewAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"UserList"] ];
        //button
        cell1.btnValider.tag= indexPath.row+1000;
        [cell1.btnValider addTarget:self action:@selector(validerAction:) forControlEvents:UIControlEventTouchUpInside];
        //button
        cell1.btnRefuser.tag= indexPath.row+1100;
        [cell1.btnRefuser addTarget:self action:@selector(deleteJoinAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell1 setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell1;
    }
    else if (indexPath.section == 5)
    {
        TypeCell12 *cell = [self.tableControl dequeueReusableCellWithIdentifier:cellID12];
        [self configureCellNonMembers:cell cellForRowAtIndexPath:indexPath];
        return cell;
    }
    else
    {
        TypeCell12 *cell = [self.tableControl dequeueReusableCellWithIdentifier:cellID12];
        [self configureCell:cell cellForRowAtIndexPath:indexPath];
        return cell;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0 && dicCountPar.count>0) {
        //is participants
        return 1;
    }
    //owner
    if (section==1) {
        return arrOwner.count;
    }
    //admin
    else if (section==2)
    {
        return arrAdmin.count;
    }
    //members
    else if (section ==3)
    {
        return arrMembers.count;
    }
    //invited
    else if (section ==4)
    {
        return arrInvite.count;
    }
    //non-members
    else if (section ==5)
    {
        return arrNontMembers.count;
    }
    //waiting
    else if (section ==6)
    {
        return arrWatting.count;
    }
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==6) {
        return 80;
    }
    else if (indexPath.section ==0) {
        return 100;
    }
    else  if (indexPath.section == 5) {
        [self configureCellNonMembers:self.prototypeCell cellForRowAtIndexPath:indexPath];
        
        self.prototypeCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableControl.bounds), CGRectGetHeight(self.prototypeCell.bounds));
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
    }
    else
    {
        [self configureCell:self.prototypeCell cellForRowAtIndexPath:indexPath];
        
        self.prototypeCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableControl.bounds), CGRectGetHeight(self.prototypeCell.bounds));
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        return size.height+1;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    if ((section==1 && arrOwner.count>0)||
        (section==2 && arrAdmin.count>0)||
        (section ==3 && arrMembers.count>0)||
        (section ==4 && arrInvite.count>0)||
        (section ==5 && arrNontMembers.count>0)||
        (section ==6 && arrWatting.count>0)) {
        return 40;
    }
    return 0;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *strTitle =HeaderTitle[section];
    TypeCell51 *cell = (TypeCell51 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3];
    cell.label1.text=strTitle;
    cell.backgroundColor =UIColorFromRGB(MAIN_COLOR);
    cell.view1.backgroundColor =[UIColor clearColor];
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[ChassesMemberOBJ sharedInstance] resetParams];

    if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
        switch ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"] intValue]) {
            case 3:
            {
                NSMutableArray *arr =[ NSMutableArray new];
                if (indexPath.section==1) {
                    arr= arrOwner;
                    if (indexPath.row == 0) {
                        if (owner_id !=sender_id) {
                            return;
                        }
                        
                    }
                }
                else if (indexPath.section==2)
                {
                    arr= arrAdmin;
                }
                else if (indexPath.section==3)
                {
                    arr= arrMembers;
                    
                }
                else if (indexPath.section==4)
                {
                    arr= arrInvite;
                    [ChassesMemberOBJ sharedInstance].isUserInvite= YES;
                    
                    
                }
                else if (indexPath.section ==5)
                {
                    arr =arrNontMembers;
                    [ChassesMemberOBJ sharedInstance].isNonMember= YES;
                }
                else
                {
                    return;
                }

                
                NSDictionary *dic =arr[indexPath.row];
                [ChassesMemberOBJ sharedInstance].dictionaryMyKind =dic;
                [ChassesMemberOBJ sharedInstance].strMyKindId = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
                
                ChassesSettingMembersParticipeVC  *viewController1=[[ChassesSettingMembersParticipeVC alloc]initWithNibName:@"ChassesSettingMembersParticipeVC" bundle:nil];
                viewController1.mParent = self;
                [viewController1 doCallback:^(NSString *strID) {
                }];
                [self pushVC:viewController1 animate:YES];
            }
                break;
            case 2:
            {
            }
                break;
            default:
                break;
        }

    }

}
//#pragma mark ----
//-(void)gotoNotifi_leaves
//{
//    __weak typeof(self) wself = self;
//    [COMMON addLoading:self];
//    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
//    [serviceObj getLounge:self.IdNotifi];
//    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//        [COMMON removeProgressLoading];
//        if ([wself fnCheckResponse:response]) return;
//        if (response) {
//            NSDictionary *dic = response[@"lounge"];
//            [[GroupEnterOBJ sharedInstance] resetParams];
//            [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
//            [self getSubcribeMember:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
//
//        }
//    };
//}
@end
