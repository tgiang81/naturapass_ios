//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertColorDrawShapeVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import "ColorCollectionCell.h"
#import "FileHelper.h"
#import "PublicationOBJ.h"
#import "AlertShareDrawShapeVC.h"

static int minHeight =30;
static int maxHeight =30*2;
static int detal =2;

@implementation AlertColorDrawShapeVC
{
    IBOutlet  UICollectionView  *myCollection;
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;

    NSMutableArray * arrData;
    NSDictionary * dataInput;

    int indexSelect;

}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertColorDrawShapeVC" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        [COMMON listSubviewsOfView:self];
        
        [myCollection registerNib:[UINib nibWithNibName:@"ColorCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"ColorCollectionCell"];
        [self InitializeKeyboardToolBar];
        
        [self.imgBorderDesc.layer setMasksToBounds:YES];
        self.imgBorderDesc.layer.cornerRadius= 3;
        self.imgBorderDesc.layer.borderWidth =0.5;
        self.imgBorderDesc.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.textView.placeholder = @"Description";
        
        [self.textView setInputAccessoryView:self.keyboardToolbar];

        [self.imgBorderTitle.layer setMasksToBounds:YES];
        self.imgBorderTitle.layer.cornerRadius= 3;
        self.imgBorderTitle.layer.borderWidth =0.5;
        self.imgBorderTitle.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.txtSearch.placeholder = @"Titre";

        constraintMessageHeight.constant = maxHeight+detal;

    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertColorDrawShapeVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertColorDrawShapeVCCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithDic:(NSDictionary*)dicInput
{
    dataInput = dicInput;
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    ///
    arrData = [NSMutableArray new];
    indexSelect = -1;

    NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
    
    NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
    
    
    [arrData addObjectsFromArray:dic[@"colors"] ];
    
    if (dicInput) {
        _txtSearch.text = dicInput[@"c_title"];
        _textView.text = dicInput[@"c_description"];
        [self.textView sizeToFit];
        [self textViewDidChange:self.textView];
        
        indexSelect = [dicInput[@"index"] intValue];
        for (int i = 0; i < arrData.count; i++) {
            NSDictionary *dicColor = arrData[i];
            if ([dicColor[@"color"] isEqualToString:dicInput[@"c_data"][@"options"][@"color"]]) {
                indexSelect = i;
                break;
            }
        }
    }
//    if ([dic[@"color"] isEqualToString:@"FFFFFF"]) {

}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.txtSearch resignFirstResponder];
    return YES;
}
-(IBAction)validerAction:(id)sender
{
    self.hidden = TRUE;
    AlertShareDrawShapeVC *vcSearch = [[AlertShareDrawShapeVC alloc] init];
    
    [vcSearch doBlock:^(NSString *type,NSDictionary *dic) {
        if ([type isEqualToString:@"dissmiss"]) {
            [self removeFromSuperview];
        }
        else if ([type isEqualToString:@"back"])
        {
            self.hidden = FALSE;
        }
        else if([type isEqualToString:@"valider"])
        {
            [self removeFromSuperview];
            [self fnGetValueValider:dic];
        }

        
    }];
    [vcSearch showAlertWithDic:dataInput];

    /*
    [self removeFromSuperview];

    NSMutableDictionary *dic = [NSMutableDictionary new];
    if (_txtSearch.text.length > 0) {
        [dic setObject:_txtSearch.text forKey:@"title"];
    }
    if (indexSelect >= 0) {
        NSDictionary *dicTmp = arrData[indexSelect];
        [dic setObject:dicTmp[@"color"] forKey:@"color"];
    }
    if (_callback) {
        _callback(dic);
    }
     */
}
-(void)fnGetValueValider:(NSDictionary*)dicResult
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    if (_txtSearch.text.length > 0) {
        [dic setObject:_txtSearch.text forKey:@"title"];
    }
    if (_textView.text.length > 0) {
        [dic setObject:_textView.text forKey:@"description"];
    }
    if (indexSelect >= 0) {
        NSDictionary *dicTmp = arrData[indexSelect];
        [dic setObject:dicTmp[@"color"] forKey:@"color"];
    }
    
    if (dicResult) {
        [dic addEntriesFromDictionary:dicResult];
    }
    if (_callback) {
        _callback(dic);
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(nil);
    }
}

#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrData.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    indexSelect = (int)indexPath.row;
    [myCollection reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"ColorCollectionCell";
    
    ColorCollectionCell *cell = (ColorCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:dic[@"color"]];
    
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&result];
    
    [cell.vColor setBackgroundColor:UIColorFromRGB(result)];
    
    if (indexPath.row == indexSelect) {
        cell.vColor.layer.borderWidth = 2.0;
        cell.vColor.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        if ([dic[@"color"] isEqualToString:@"FFFFFF"]) {
            
            cell.imgCheck.image = [UIImage imageNamed:@"CheckBlack"];
            
        }
        else
        {
            cell.imgCheck.image = [UIImage imageNamed:@"CheckColor"];
            
        }
    }else{
        cell.vColor.layer.borderWidth = 0.0;
        cell.imgCheck.image = nil;
        
    }
    
    return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
   return CGSizeMake((CGRectGetWidth(myCollection.frame ) - 10)/4, (CGRectGetWidth(myCollection.frame ) - 10)/4);

}
//
- (void)textViewDidChange:(UITextView *)textView
{
//    [self refreshHeight];
}

-(void)refreshHeight
{
    NSInteger newSizeH = [self measureHeight];
    if (newSizeH < minHeight || !_textView.hasText) {
        newSizeH = minHeight; //not smalles than minHeight
    }
    else if (maxHeight && newSizeH > maxHeight) {
        newSizeH = maxHeight; // not taller than maxHeight
    }
    if (_textView.frame.size.height != newSizeH)
    {
        if (newSizeH <= maxHeight)
        {
            [UIView animateWithDuration:0.1f
                                  delay:0
                                options:(UIViewAnimationOptionAllowUserInteraction|
                                         UIViewAnimationOptionBeginFromCurrentState)
                             animations:^(void) {
                                 [self resizeTextView:newSizeH];
                             }
                             completion:^(BOOL finished) {
                                 [self resizeTextView:newSizeH];
                                 
                             }];
        }
    }
    
}

- (CGFloat)measureHeight
{
    return self.textView.contentSize.height;
}

-(void)resizeTextView:(NSInteger)newSizeH
{
    constraintMessageHeight.constant = newSizeH+detal;
}
-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (self.keyboardToolbar == nil)
    {
        self.keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 38.0f)];
        self.keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:self
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strOK)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard:)];
        
        [self.keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                        spaceBarItem,
                                        doneBarItem, nil]];
        
        
    }
    //    [self.emailContent setInputAccessoryView:keyboardToolbar];
    
}
- (void)resignKeyboard:(id)sender
{
    [self.textView resignFirstResponder];
    
}

@end
