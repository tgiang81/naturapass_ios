//
//  BaseView.m
//  Naturapass
//
//  Created by Manh on 6/16/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "BaseView.h"
#import "SVPullToRefresh.h"
@implementation BaseView
-(void) initRefreshControl
{
    __weak BaseView *weakSelf = self;
    
    [self.tableControl addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    [self.tableControl addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    
}
-(void)startRefreshControl
{
    [self.tableControl triggerPullToRefresh];
    
}
-(void)moreRefreshControl
{
    [self.tableControl triggerInfiniteScrolling];
    
}
-(void)stopRefreshControl
{
    [self.tableControl.pullToRefreshView stopAnimating];
    [self.tableControl.infiniteScrollingView stopAnimating];
}
-(void)isRemoveScrollingViewHeight:(BOOL)enable
{
    self.tableControl.infiniteScrollingView.isRemoveScrollingViewHeight = enable;
    
}
//overwrite
- (void)insertRowAtTop {
}

//overwrite
- (void)insertRowAtBottom {
}

@end
