//
//  TestTileLayer.m
//  Naturapass
//
//  Created by Giang on 7/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "TestTileLayer.h"
#import "UIImage+alpha.h"
#import "CommonHelper.h"
#import "Config.h"

@implementation TestTileLayer

//transparent

- (UIImage *)tileForX:(NSUInteger)x y:(NSUInteger)y zoom:(NSUInteger)zoom {
    
    NSString* userAgent = @"iOS";
    NSURL *url = [ NSURL URLWithString:  [NSString stringWithFormat:@"http://wxs.ign.fr/%@/geoportail/wmts?LAYER=CADASTRALPARCELS.PARCELS&FORMAT=image/png&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal&TILEMATRIXSET=PM&TILEMATRIX=%lu&TILEROW=%lu&TILECOL=%lu",KEY_IGN ,
                                                  (unsigned long)zoom, (unsigned long)y, (unsigned long)x]];
    
    
    if ([[SDImageCache sharedImageCache] imageFromDiskCacheForKey: url.absoluteString] != nil) {

        UIImage *newImage = [[[SDImageCache sharedImageCache] imageFromDiskCacheForKey: url.absoluteString] imageByApplyingAlpha:125/225];
        return newImage;
    }
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url] ;
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse* response = nil;
    NSError* error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    
    if (data == nil){
        return nil;
        
    } else{
        UIImage *image = [UIImage imageWithData:data];
        
        UIImage *newImage = [image imageByApplyingAlpha:125/225];

        return newImage;
        
    }
    
}
@end
