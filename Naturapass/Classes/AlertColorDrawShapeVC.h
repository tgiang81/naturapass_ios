//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
#import "SAMTextView.h"
typedef void (^AlertColorDrawShapeVCCallback)(NSDictionary *dic);

@interface AlertColorDrawShapeVC : UIView<UITextFieldDelegate,UITextViewDelegate>
{
    IBOutlet UIView *subAlertView;
    __weak IBOutlet NSLayoutConstraint *constraintMessageHeight;
    NSString *strTitle;
    NSString *strDescriptions;
    
}
@property (nonatomic,copy) AlertColorDrawShapeVCCallback callback;
@property (nonatomic,strong) IBOutlet UIButton *validerButton;
@property(nonatomic,strong) IBOutlet UITextField *txtSearch;
@property(nonatomic,strong) IBOutlet UIView *borderColor;
@property (nonatomic,strong)   IBOutlet SAMTextView  *textView;
@property (nonatomic, strong)  UIToolbar                   *keyboardToolbar;
@property(nonatomic,strong) IBOutlet UIImageView *imgBorderTitle;
@property(nonatomic,strong) IBOutlet UIImageView *imgBorderDesc;

-(void)doBlock:(AlertColorDrawShapeVCCallback ) cb;
-(IBAction)validerAction:(id)sender;
-(void)showAlertWithDic:(NSDictionary*)dicInput;
-(instancetype)init;
@end
