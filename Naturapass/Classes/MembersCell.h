//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLKMenuPopover.h"
#import "AppCommon.h"

typedef void (^callBackGroup) (NSInteger);
@interface MembersCell : UITableViewCell<MLKMenuPopoverDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (weak, nonatomic) IBOutlet UIImageView *imageLine;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_height;

@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property (nonatomic, copy) callBackGroup CallBackGroup;
@property (nonatomic,retain) IBOutlet UIButton       *btnAddFriend;
@property (nonatomic,retain) IBOutlet UIImageView       *imgAddFriend;
-(void) createMenuList:(NSArray*)arr;

-(void) show:(UIView*)view;
-(void) show:(UIView*)view offset:(CGRect)offset;
@end
