//
//  TheProjectCell.m
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "SignalerFavorisSystemCell.h"

@interface SignalerFavorisSystemCell()
@end
@implementation SignalerFavorisSystemCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.cellLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.cellLabel.frame);
}
@end
