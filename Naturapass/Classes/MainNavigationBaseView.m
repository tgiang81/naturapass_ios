//
//  MainNavigationBaseView.m
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MainNavigationBaseView.h"

@implementation MainNavigationBaseView

-(IBAction)fnMainNavClick:(id)sender
{
    //Left -> Right... 10 11 12
    UIButton *btn = (UIButton*) sender;
    //set selected
    self.myMainNavCallback(btn);
    
}

-(void) badgingBtn:(UIImageView*)btn count:(int) iCount
{
    btn.badgeView.font = [UIFont boldSystemFontOfSize:10.0];
    [btn.badgeView setBadgeValue:iCount];
    [btn.badgeView setOutlineWidth:1.0];
    [btn.badgeView setPosition:MGBadgePositionTopRight];
    [btn.badgeView setOutlineColor:[UIColor whiteColor]];
    [btn.badgeView setBadgeColor:[UIColor redColor]];
    [btn.badgeView setTextColor:[UIColor whiteColor]];
}

@end
