//
//  MyAlertView.h
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"


@interface VoisChiens_Document_WebView_Alert : UIViewController<UIWebViewDelegate>
{
    IBOutlet UIWebView *wvWeb;
}
-(instancetype)initWithURL:(NSString*)url;
@end


