//
//  MKMapView+ZoomLevel.m
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MKMapView+ZoomLevel.h"

@implementation MKMapView (ZoomLevel)

-(double) zoomLevel {
    return log2(360 * ((self.frame.size.width/256) / self.region.span.longitudeDelta));
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate
                  zoomLevel:(NSUInteger)zoom animated:(BOOL)animated
{
    MKCoordinateSpan span = MKCoordinateSpanMake(180 / pow(2, zoom) *
                                                 self.frame.size.height / 256, 0);
    if (span.latitudeDelta < 220) {
        [self setRegion:MKCoordinateRegionMake(coordinate, span) animated:animated];
    }
}

@end
