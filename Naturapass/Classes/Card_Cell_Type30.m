//
//  Card_Cell_Type10.m
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Card_Cell_Type30.h"
#import "PublicationControlCheckboxCell.h"
static NSString *identifierCard = @"CardCheckboxCellID";
static NSString *tableCell = @"PublicationControlCheckboxCell";

@implementation Card_Cell_Type30

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)prepareForReuse {
    
    [super prepareForReuse];
}

@end
