//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"


@interface TypeCell71: UITableViewCell
{
    __weak IBOutlet NSLayoutConstraint *contraintHeightTable;
    int intType;
}
//view
@property (weak, nonatomic) IBOutlet UIView * view1;

//image
@property (nonatomic,strong) IBOutlet UIImageView       *img1;
@property (nonatomic,strong) IBOutlet UIImageView       *img2;
@property (nonatomic,strong) IBOutlet UIImageView       *img3;
@property (nonatomic,strong) IBOutlet UIImageView       *imgArrow;

//label
@property (nonatomic,retain) IBOutlet UILabel           *label1;
@property (nonatomic,retain) IBOutlet UILabel           *label2;
@property (nonatomic,retain) IBOutlet UILabel           *label3;
@property (nonatomic,retain) IBOutlet UILabel           *label4;

//button
@property (nonatomic,retain) IBOutlet UIButton           *button1;
@property (nonatomic,retain) IBOutlet UIButton           *button2;
@property (nonatomic,retain) IBOutlet UITableView           *tblControl;
@property (nonatomic,strong) NSMutableArray *tourMemberArray;
@property (nonatomic,strong) NSString *strID;

@property (nonatomic,strong) UIViewController *parent;
//funtion
-(void)fnSettingCell:(int)type expectTarget:(ISSCREEN)target;
-(void)fnShowMemberWithArray:(NSArray*)arr;
@end
