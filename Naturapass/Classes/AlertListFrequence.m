//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertListFrequence.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Extensions.h"
#import "UIImage+Utils.h"
#import "AlertListFrequenceCell.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@implementation AlertListFrequence
{
}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertListFrequence" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertListFrequenceCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
        self.tableControl.estimatedRowHeight = 70;
        [self.viewBorder.layer setMasksToBounds:YES];
        self.viewBorder.layer.cornerRadius= 10;
        self.viewBorder.layer.borderWidth =0;
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertListFrequenceCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertListFrequenceCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    [self.tableControl reloadData];
    [self caculatorHeighAlert];
}
-(void)caculatorHeighAlert
{
    if (self.arrFrequence.count > 7) {
        _constraintHeight.constant = 44+ 8 * 44;
    }
    else
    {
        _constraintHeight.constant = 44+ (self.arrFrequence.count+1) * 44;
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
}
#pragma mark Search Helpers

#pragma mark UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrFrequence.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AlertListFrequenceCell *cell = nil;
    
    cell = (AlertListFrequenceCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = self.arrFrequence[indexPath.row];
    cell.lbTitle.text = dic[@"label"];
    cell.btnRadio.selected = [dic[@"active"] boolValue];
    if (self.expectTarget == ISLOUNGE) {
        [cell.btnRadio fnColorNormal:[UIColor clearColor] withColorActive:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
    }
    else
    {
        [cell.btnRadio fnColorNormal:[UIColor clearColor] withColorActive:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
    }
    cell.btnRadio.tag=4000+indexPath.row;
    [cell.btnRadio addTarget:self action:@selector(chooseRadio:) forControlEvents:UIControlEventTouchUpInside];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dissmiss:nil];
    NSDictionary *dicFrequence = self.arrFrequence[indexPath.row];
    if (_callback) {
        _callback(dicFrequence);
    }
}
-(void)chooseRadio:(id)sender
{
    NSInteger tag =((UIButton*)sender).tag;
    NSInteger index = tag-4000;
    [self dissmiss:nil];
    NSDictionary *dicFrequence = self.arrFrequence[index];
    if (_callback) {
        _callback(dicFrequence);
    }
}
@end
