//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AllerMURGPS.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"

@implementation AllerMURGPS
{
    
}

-(instancetype)initWithData:(NSDictionary*) data
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AllerMURGPS" owner:self options:nil] objectAtIndex:0] ;

    if (self) {

        myDic = data;
        
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;

        [COMMON listSubviewsOfView:self];
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];

}

#pragma callback
-(void)setCallback:(AllerMURGPSCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AllerMURGPSCallback ) cb
{
    self.callback = cb;
    
}

#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
}

-(IBAction)closeAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0);
    }
}


-(IBAction)PlansAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1);
    }
}
-(IBAction)MapsAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(2);
    }
}
-(IBAction)WazeAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(3);
    }
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
