//
//  AgendaDetailCell.h
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDStatusButton.h"

@interface AgendaDetailCell2 : UITableViewCell
@property(nonatomic,strong) IBOutlet UILabel *lbStartDate;
@property(nonatomic,strong) IBOutlet UILabel *lbStartTime;
@property(nonatomic,strong) IBOutlet UILabel *lbEndDate;
@property(nonatomic,strong) IBOutlet UILabel *lbEndTime;
@property(nonatomic,strong) IBOutlet UILabel *lbAddress;
@property(nonatomic,strong) IBOutlet UILabel *lbLatLong;
@property(nonatomic,strong) IBOutlet UILabel *lblDebut;
@property(nonatomic,strong) IBOutlet UILabel *lblFin;
@property(nonatomic,strong) IBOutlet UILabel *lblRendez;
@property(nonatomic,strong) IBOutlet MDStatusButton *btnSURNATURA;
@property(nonatomic,strong) IBOutlet UIButton *btnAUTRESGPS;

@property(nonatomic,strong) IBOutlet UILabel *lbNameHunt;
@property(nonatomic,strong) IBOutlet UILabel *lbDescription;

@property(nonatomic,strong) IBOutlet UILabel *lbAccess;
@property(nonatomic,strong) IBOutlet UIImageView *logo;
@end
