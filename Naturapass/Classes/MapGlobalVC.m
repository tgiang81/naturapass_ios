//
//  MapGlobalVC.m
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MapGlobalVC.h"
#import "AZNotification.h"
#import "Publication_FavoriteAddress.h"
#import "CommonObj.h"
#import "LiveHuntOBJ.h"
#import "DatabaseManager.h"
#import<QuartzCore/QuartzCore.h>
//shapes

#import "UIView+MGBadgeView.h"
#include <sys/types.h>
#include <sys/sysctl.h>
#import "NSString+HTML.h"
#import "AlertSearchMapVC.h"

#import "AlertVC.h"
#import "NSDate+Extensions.h"
#import "MDMarkersAgenda.h"
#import "AmisAddScreen1.h"
#import "GroupCreateOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateV2.h"
#import "Publication_FavoriteAddress.h"
#import "ChatListe.h"
#import "Publication_Carte.h"
#import "AlertListMarker.h"
#import "MarqueeLabel.h"
#import "ServiceHelper.h"
#import "ChatVC.h"
#import "CommentDetailVC.h"

#import "PublicationNew_Signalement.h"
#import "RecordingDB.h"
#import "SignalerTerminez.h"
#import "ListItemsMenuCarte.h"

//GMSMap
//612, 792

#define ARC4RANDOM_MAX      0x100000000
extern BOOL bFilterON;
static int widthChat = 100;

#define kSizeMarkerZoom      10


@interface MapGlobalVC ()
{
    
    __weak IBOutlet NSLayoutConstraint *topButtonConstraint;
    __weak IBOutlet NSLayoutConstraint *constraintTOPMOST;
    __weak IBOutlet UIView *vHeadContent;
    NSMutableArray *arrCategoryIDs;
    
    __weak IBOutlet UIButton *btnPedometer;
    
    
    __weak IBOutlet UIImageView *imgVSetting;
    __weak IBOutlet UIButton *btnSetting;
    
    __weak IBOutlet NSLayoutConstraint *topConstraintCallout;
    __weak IBOutlet UIView *vContainerLeft;
    __weak IBOutlet NSLayoutConstraint *constraintLEFT;
    __weak IBOutlet UIButton *btnShow;
    __weak IBOutlet UIButton *btnHide;
    //callout
    __weak IBOutlet UIView *vCallOut;
    __weak IBOutlet UIImageView *vArrow;
    
    __weak IBOutlet UILabel *callName;
    __weak IBOutlet UILabel *callText;
    __weak IBOutlet UILabel *callTime;
    __weak IBOutlet UIButton *btnCloseCallout;
    __weak IBOutlet UILabel *callObs;
    __weak IBOutlet UIImageView *imgObsCallObs;
    __weak IBOutlet NSLayoutConstraint *heightConstraintCallout;
    
    //New CARTE
    
    __weak IBOutlet UIButton *btnTOP;
    
    ListItemsMenuCarte * masterFilterView;
    MASTER_FILTER_TYPE iMasterFilterType;
    //if choose from Filterview -> disable Master Filter
    BOOL isEnableMasterFilter;
    
    __weak IBOutlet MarqueeLabel *liveHunt_Title;
    __weak IBOutlet UILabel *liveHunt_Time;
    NSDictionary *dicSelectedLeftItem;
    ChatLiveHuntView *vcLiveHunt;
}

@property (strong, nonatomic) NSTimer *continuousPressTimer;

@end

@implementation MapGlobalVC

//recording -> Pause
//recording -> Stop

//Pause -> Continue recording
//Pause -> Stop

//Stop -> Continue Last recording
//Stop -> Start brand New recording.

-(IBAction)fnPedometer:(id)sender
{
    switch (_pedometerStatus) {
            //current status is Recording
        case PEDOMETER_RECORDING:
        {
            [self gotoAlertPedometerRecording];
        }
            break;
            
        case PEDOMETER_PAUSE:
        {
            [self gotoAlertPedometerPaused];
        }
            break;
            
        case PEDOMETER_STOP:
        {
            [self gotoAlertPedometerStopped];
        }
            break;
            
        default:
            break;
    }
}


-(void)gotoAlertPedometerRecording
{
    AlertVC *vc = [[AlertVC alloc] initPedometerRECORDING: [self queryPedometerData] ];
    //    [vc fnSetNameButtonPedometer:@"METTRE EN PAUSE" wittNameButtonTerminal:@"TERMINER L'ENREGISTREMENT" withDesc:nil];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        if (index == 0) {
            //do Fn Pause recording
            _pedometerStatus = PEDOMETER_PAUSE;
            [self doFnPedometer];
            
            //save current data...to DB
            //Store information : from StartDate -> Now (query pedometer to get information)
            [RecordingDB queryPedometerFromDate:  RecordingDB.sharedInstance.startDate ];
            
            float distance = [RecordingDB.sharedInstance.myPedoMeterData.distance floatValue];
            float average = [RecordingDB.sharedInstance.myPedoMeterData.averageActivePace floatValue];
            
            NSString * strTmpAgendaID = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
            
            
            NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
            [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
            [formatter_local setDateFormat:@"dd/MM/yyyy HH:mm"];
            
            NSString *strStartTime = [formatter_local stringFromDate:RecordingDB.sharedInstance.startDate];
            NSString *strEndTime = [formatter_local stringFromDate: [NSDate date]];
            
            [RecordingDB addWithAgenda:strTmpAgendaID
                             startTime:strStartTime
                               endTime:strEndTime
                              distance:distance
                               average:average];
            
        } else if (index == 1) {
            //confirm if really want End recording
            [self fnConfirmEndRecording];
        } else if (index == 2) {
            //continue recording...i am recording...
        }
    }];
    
    [vc showAlert];
}

-(void)gotoAlertPedometerPaused
{
    AlertVC *vc = [[AlertVC alloc] initPedometerPAUSE: [self queryPedometerData] ];
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        if (index == 0) {
            //paused -> Continue recording
            //Update Startdate
            RecordingDB.sharedInstance.startDate = [NSDate date];
            _pedometerStatus = PEDOMETER_RECORDING;
            
            [self doFnPedometer];
            
        } else if (index == 1) {
            //Ending, don't need to confirm.
            _pedometerStatus = PEDOMETER_STOP;
            [self doFnPedometer];
        } else if (index == 2) {
            //I am in Pause status -> let it be.
        }
    }];
    
    [vc showAlert];
}

-(void)gotoAlertPedometerStopped
{
    //if Pedometer recored before and had been stoped
    
    if ([RecordingDB ifDataFoAgendaExist:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]]) {
        AlertVC *vc = [[AlertVC alloc] initPedometerSTOP2: [self queryPedometerData] ];
        
        [vc doBlock:^(NSInteger index, NSString *strContent) {
            if (index == 0) { //anular
            }
            else if (index == 1) {
                //show next Alert: continue or record new
                [self fnConfirmRecNewOrContinueLastTime];
            }
            
        }];
        
        [vc showAlert];
        
    }else{
        
        //or record new
        AlertVC *vc = [[AlertVC alloc] initPedometerSTOP: [self queryPedometerData] ];
        [vc doBlock:^(NSInteger index, NSString *strContent) {
            
            if (index == 0) {
                //Annular ...
            } else if (index == 3) {
                //Stop -> CLEAR old data of this hunt, create from beginning
                NSString * strTmpAgendaID = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
                
                [RecordingDB removeWithAgenda: strTmpAgendaID ];
                [RecordingDB removeTrackingWithAgenda:strTmpAgendaID ];
                
                RecordingDB.sharedInstance.startDate =  [NSDate date];
                _pedometerStatus = PEDOMETER_RECORDING;
                
                [self doFnPedometer];
            }
            
        }];
        
        [vc showAlert];
        
    }
}

-(void) fnConfirmRecNewOrContinueLastTime
{
    AlertVC *vc = [[AlertVC alloc] initAlertConfirmRecordNewOrContinueLastTime];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        
        if (index == 2)
        {
            //continue last time
            RecordingDB.sharedInstance.startDate = [NSDate date];
            _pedometerStatus = PEDOMETER_RECORDING;
            
            [self doFnPedometer];
            
        }
        else if (index == 1)
        {
            //record New
            //-> CLEAR old data of this hunt, create from beginning
            NSString * strTmpAgendaID = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
            
            [RecordingDB removeWithAgenda: strTmpAgendaID ];
            [RecordingDB removeTrackingWithAgenda:strTmpAgendaID ];
            
            RecordingDB.sharedInstance.startDate =  [NSDate date];
            _pedometerStatus = PEDOMETER_RECORDING;
            
            [self doFnPedometer];
            
        }
        
    }];
    
    [vc showAlert];
}


#pragma mark - CONFIRM END RECORDING

-(void) fnConfirmEndRecording
{
    AlertVC *vc = [[AlertVC alloc] initConfirmEndRecordingPedometerAlert];
    [vc fnSetNameButtonPedometer:@"TERMINER L'ENREGISTREMENT" wittNameButtonTerminal:@"ANNULER" withDesc:
     @"ÊTES-VOUS sûr(E) DE VOULOIR TERMINER L'ENREGISTREMENT DE VOS STATISTIQUES.".uppercaseString   ];
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        if (index == 0) {
            //fn END recording ...temporary...so just stop it...dont reset time.
            _pedometerStatus = PEDOMETER_STOP;
            [self doFnPedometer];
        } else if (index == 1) {
            //do nothing...don't want to end recording
            
        }
    }];
    
    [vc showAlert];
    
}

#pragma mark - CONTINUE
//cache status

-(void) doFnPedometerUpdateStatus
{
    switch (_pedometerStatus) {
        case PEDOMETER_RECORDING:
        {
            [btnPedometer setBackgroundImage:[UIImage imageNamed:@"map_ic_pedometer_recording"] forState:UIControlStateNormal];
        }
            break;
        case PEDOMETER_PAUSE:
        {
            [btnPedometer setBackgroundImage:[UIImage imageNamed:@"map_ic_pedometer_stop"] forState:UIControlStateNormal];
        }
            break;
        case PEDOMETER_STOP:
        {
            [btnPedometer setBackgroundImage:[UIImage imageNamed:@"map_ic_pedometer"] forState:UIControlStateNormal];
        }
            break;
            
        default:
            break;
    }
}


-(void) doFnPedometer
{
    switch (_pedometerStatus) {
        case PEDOMETER_RECORDING:
        {
            [RecordingDB startPedometerUpdates];
        }
            break;
        case PEDOMETER_PAUSE:
        {
            //stop pedometer update
            [RecordingDB stopPedometerUpdates];
            
        }
            break;
        case PEDOMETER_STOP:
        {
            [RecordingDB stopPedometerUpdates];
        }
            break;
            
        default:
            break;
    }
    //UI
    [self doFnPedometerUpdateStatus];
    
    NSString * strID = [NSString stringWithFormat:@"%@_recording_status", [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] ];
    
    [[NSUserDefaults standardUserDefaults] setInteger: _pedometerStatus forKey: strID ];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
}

/*
 -(void)gotoAlertPedometerBegin
 {
 AlertVC *vc = [[AlertVC alloc] initPedometerAlert: [self queryPedometerData] ];
 [vc fnSetNameButtonPedometer:@"DEMARRER L'ENREGISTREMENT" wittNameButtonTerminal:@"ANNULER" withDesc:nil];
 [vc doBlock:^(NSInteger index, NSString *strContent) {
 if (index == 0) {
 //begin -> live: enable live
 [self fnPedometerLive];
 
 } else if (index == 1) {
 //do nothing ....
 }
 }];
 
 [vc showAlert];
 }
 
 
 -(void)gotoAlertPedometerConfirmLive
 {
 
 AlertVC *vc = [[AlertVC alloc] initPedometerAlert: [self queryPedometerData] ];
 [vc fnSetNameButtonPedometer:@"DEMARRER L'ENREGISTREMENT" wittNameButtonTerminal:@"ANNULER" withDesc:nil];
 [vc doBlock:^(NSInteger index, NSString *strContent) {
 if (index == 0) {
 //goto screen confirm
 
 } else if (index == 1) {
 //do nothing ....
 }
 }];
 
 [vc showAlert];
 }
 */

//query database to get sum data
-(NSDictionary*) queryPedometerData
{
    NSString * strTmpAgendaID = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
    
    //fetch sum data from DB for this livehunt.
    
    return [RecordingDB getSumaryWithAgenda:strTmpAgendaID];
}

//Is it possible to send a *push notification* to every member when admin change dates and when system received THIS notification, we cancel the autosaving for the stats. And we re-enabled it *only* if user clicks on the notif / open the app.


/*
 {
 "results": [
 {
 "latitude": 41.161758,
 "elevation": 117,
 "longitude": -8.583933
 }
 ]
 }
 */

-(void) fnGetAltitude_elevation{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getRequest: [NSString stringWithFormat: @"https://api.open-elevation.com/api/v1/lookup?locations=%@,%@" ,
                             [NSString stringWithFormat:@"%f" ,                     appDelegate.locationManager.location.coordinate.latitude] ,
                             [NSString stringWithFormat:@"%f" , appDelegate.locationManager.location.coordinate.longitude]
                             ] ];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        if (response[@"results"] &&  [response[@"results"]  isKindOfClass: [NSArray class]] )
        {
            
            //            float tmpAltitude = [response[@"results"][0][@"elevation"] floatValue];
            
            //            //special number
            //            if (  tmpAltitude > latestAltitudeValue && latestAltitudeValue != -1000000) {
            //                //positive
            //                sumPositiveAltitude += tmpAltitude - latestAltitudeValue;
            //            }else if (  tmpAltitude < latestAltitudeValue && latestAltitudeValue != -1000000) {
            //                sumNegativeAltitude += latestAltitudeValue - tmpAltitude;
            //            }
            //
            //            //
            //            latestAltitudeValue = tmpAltitude;
            //
        }
    };
    
}

- (void)_cancelContinousPressIfNeeded
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fnGetAltitude_elevation) object:nil];
    
    NSTimer *timer = self.continuousPressTimer;
    if (timer) {
        [timer invalidate];
        
        self.continuousPressTimer = nil;
    }
}

-(void) fnStartCounting
{
    //    latestAltitudeValue = -1000000;
    //    sumPositiveAltitude = 0;
    //    sumNegativeAltitude = 0;
    //
    //    //30s request get altitude...input is current lat/lon
    //    self.continuousPressTimer =  [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(fnGetAltitude_elevation) userInfo:nil repeats:YES];
    //
    //    if ([CMPedometer isStepCountingAvailable]) {
    //        [pedometer startPedometerUpdatesFromDate:[NSDate date]
    //                                          withHandler:^(CMPedometerData *pedometerData, NSError *error) {
    //
    //                                              dispatch_async(dispatch_get_main_queue(), ^{
    //
    //                                                  distance = [pedometerData.distance floatValue];
    //                                                  average = [pedometerData.averageActivePace floatValue];
    //
    //// distance parcourue -- distance traveled
    //// positive difference
    //// denivele negatif--negative level
    //// vitesse moyenne --- average speed : pace estimation ...
    //                                              });
    //                                          }];
    //
    //        //start counter
    //
    //        NSDate *to   = [NSDate date];
    //        NSDate *from = [to dateByAddingTimeInterval:-(24. * 3600.)];
    //
    //        [pedometer queryPedometerDataFromDate:from
    //                                            toDate:to
    //                                       withHandler:
    //         ^(CMPedometerData *pedometerData, NSError *error) {
    //
    //             distance = [pedometerData.distance floatValue];
    //             average = [pedometerData.averageActivePace floatValue];
    //         }];
    //    }
    
}


- (void) viewDidLoad
{
    [super viewDidLoad];
    
    jtSwitch.transform = CGAffineTransformMakeScale(0.65, 0.65);
    jtSwitch.on = bFilterON;
    self.vViewFilter.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUICarte) name:UPDATE_OBJ_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideIndicator) name:HIDE_INDICATOR_OBJ_NOTIFICATION object:nil];
    
    [Flurry logEvent:@"carte.MapFragment" timed:YES];
    
    
    vContainerLeft.layer.cornerRadius= 10.0;
    vContainerLeft.layer.borderWidth =0.5;
    vContainerLeft.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    
    //hide callout
    vCallOut.alpha = 0;
    vArrow.alpha = 0;
    [vcLiveHunt unSelectedItem];
    btnCloseCallout.hidden = TRUE;
    
    
    arrNodes = [NSMutableArray new];
    arrToRedraw = [NSMutableArray new];
    arrToRedrawAgenda = [NSMutableArray new];
    arrCategoryIDs =  [NSMutableArray new];

    arrToRedrawDistributor = [NSMutableArray new];
    arrOwnerOfPublications = [NSMutableArray new];
    
    [self.btnRecenter.layer setMasksToBounds:YES];
    self.btnRecenter.layer.cornerRadius= 15.0;
    
    self.strMesGroupFilter = nil;
    self.strMesHuntFilter = nil;
    
    vTmpMap.warning.text = @"";
    vTmpMap.warning.hidden = TRUE;
    
    self.constraintBtnRDV_width.constant = 0;
    self.constraintBtnRDV_hori.constant = 0;
    
    // here comes the interesting part
    // get a handle to the map scale view of our mapView (by eventually installing one first)
    
    self.mapScaleView = [[LXMapScaleView_Google alloc] initWithMapView:vTmpMap.mapView_];
    
    // adjust visual settings if necessary
    self.mapScaleView.position = kLXMapScalePositionTopLeft;
    self.mapScaleView.style = kLXMapScaleStyleBar;
    
    iMasterFilterType =  TOUT;
    
    _lbRDV.hidden = YES;
    
    if (self.expectTarget == ISLOUNGE) {
        if (self.isLiveHunt) {
            
            NSString * strID = [NSString stringWithFormat:@"%@_recording_status", [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] ];
            
            int iVal = (int)[[NSUserDefaults standardUserDefaults] integerForKey: strID ];
            
            switch (iVal) {
                case 0:
                    _pedometerStatus = PEDOMETER_RECORDING;
                    break;
                case 1:
                    _pedometerStatus = PEDOMETER_PAUSE;
                    break;
                case 2:
                    _pedometerStatus = PEDOMETER_STOP;
                    break;
                    
                default:
                    _pedometerStatus = PEDOMETER_STOP;
                    break;
            }
            //update UI
            [self doFnPedometerUpdateStatus];

            // set icon status
            NSString *startDate = [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"];
            
            if ([NSDate compareLocalDateWithInput:startDate] == NSOrderedDescending) {
                //live hunt is inprogress..auto recording
                RecordingDB.sharedInstance.startDate = [NSDate date];
                [self doFnPedometer];
            }
            //else...let user click on button record manually
            
            self.constraintBtnRDV_width.constant = 38;
            self.constraintBtnFavorite.constant = 0;
            self.constraintBtnRDV_hori.constant = 8;
            _lbRDV.hidden = NO;
        }
    }
    
    [self settingBlurBackGround];
    
    [vTmpMap.btnShortCutSetting setBackgroundImage:[UIImage imageNamed:@"ic_carter_cole"] forState:UIControlStateNormal];
    
    switch (self.expectTarget) {
            
        case ISMUR:
        {
            self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationMUR"];
            [self.toussearchBar setTintColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            
            [vTmpMap.btnShortCut setImage:  [UIImage imageNamed:@"mur_ic_add_publication"] forState:UIControlStateNormal];
            
            vTmpMap.btnCarteFiltreAction.hidden = YES;
            vTmpMap.btnCarteFiltreCategory.hidden = YES;
            
            vTmpMap.image_Category.hidden = YES;
            vTmpMap.image_RealFilter.hidden = YES;
            vTmpMap.lbQui.hidden = YES;
            vTmpMap.indicator.color =UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP:
        {
            self.colorNavigation = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);

            [self checkAllow];
            
            //No need filter...
            vTmpMap.btnCarteFiltreAction.hidden = YES;
            vTmpMap.lbQui.hidden = YES;
            
            vTmpMap.btnCarteFiltreCategory.hidden = NO;
            vTmpMap.image_Category.hidden = NO;
            vTmpMap.image_RealFilter.hidden = YES;
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [self.toussearchBar setTintColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
            [vTmpMap.btnShortCut setImage:  [UIImage imageNamed:@"ic_group_floating_btn"] forState:UIControlStateNormal];
            [self.btnHuntRefresh setImage:  [UIImage imageNamed:@"btn_refresh_map_group"] forState:UIControlStateNormal];
            [self.btnFav setImage:  [UIImage imageNamed:@"ic_favarite_map_group"] forState:UIControlStateNormal];
            
            
            [self.btnLocationAddress setImage:  [UIImage imageNamed:@"ic_search_location_group"] forState:UIControlStateNormal];
            
            
            vTmpMap.indicator.color =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            self.vViewFilter.hidden = NO;
            
        }
            break;
            
        case ISLOUNGE:
        {
            [self checkAllow];
            
            if (self.isLiveHunt) {
                self.colorNavigation = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);

                //LIVEHUNT
                float i = 50;
                vTmpMap.mapView_.padding = UIEdgeInsetsMake(0,i, 0, i); //top left bottom right
                
                [Flurry logEvent:@"livecarte.HMapFragment" timed:YES];
                
                //                UINavigationBar *navBar=self.navigationController.navigationBar;
                //                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                //
                //                [self addMainNav:@"MainNavLiveMap"];
                //                //SUB
                //                [self addSubNav:@"livechat"];
                //                [self setThemeTabVC:self];
                
                [self addMainNav:@"CARTER"];
                [self addSubNav:@"livechat"];
                
                if ([LiveHuntOBJ sharedInstance].dicLiveHunt) {
                    NSString *strName = [[LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"] emo_emojiString];
                    
                    liveHunt_Title.text = strName.uppercaseString;
                    
                    if ([LiveHuntOBJ sharedInstance].dicLiveHunt[@"meetingDate"]) {
                        [liveHunt_Time setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meetingDate"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"endDate"]]];

                    }else{
                        [liveHunt_Time setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_end"]]];

                    }
                }
                //
                vTmpMap.btnCarteFiltreAction.hidden = NO;
                vTmpMap.lbQui.hidden = NO;
                
                vTmpMap.btnCarteFiltreCategory.hidden = NO;
                vTmpMap.image_Category.hidden = NO;
                vTmpMap.image_RealFilter.hidden = NO;
                
                vTmpMap.btnShortCut.hidden = YES;
                vTmpMap.btnShortCutSetting.hidden = YES;
                vTmpMap.btnZoomIn.hidden = YES;
                vTmpMap.btnZoomOUt.hidden = YES;
                
                //                [vTmpMap.btnShortCut setImage:  [UIImage imageNamed:@"livehunt_ic_add"] forState:UIControlStateNormal];
                //                [vTmpMap.btnShortCutSetting setBackgroundImage:  [UIImage imageNamed:@"ic_livehunt_cole"] forState:UIControlStateNormal];
                //                [self.btnHuntRefresh setImage:  [UIImage imageNamed:@"live_btn_refresh_map"] forState:UIControlStateNormal];
                [self.btnFav setImage:  [UIImage imageNamed:@"live_ic_carte_favorite_address"] forState:UIControlStateNormal];
                
                [self.btnLocationAddress setImage:  [UIImage imageNamed:@"ic_search_location_livehunt"] forState:UIControlStateNormal];
                
                [self.toussearchBar setTintColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
                
                vTmpMap.indicator.color =UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                
                //set show/hie button Setting Chasse
                NSDictionary *dic = [LiveHuntOBJ sharedInstance].dicLiveHunt;
                //I am admin ?
                if (dic && [dic[@"connected"] isKindOfClass:[NSDictionary class]])
                {
                    
                    if ([dic[@"connected"][@"access"]isEqual:@3])
                    {
                        imgVSetting.hidden = NO;
                        btnSetting.hidden = NO;
                    }else{
                        imgVSetting.hidden = YES;
                        btnSetting.hidden = YES;
                        
                    }
                }else{
                    imgVSetting.hidden = YES;
                    btnSetting.hidden = YES;
                }
            }
            else
            {
                self.colorNavigation = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);

                vTmpMap.btnCarteFiltreAction.hidden = YES;
                vTmpMap.btnCarteFiltreCategory.hidden = NO;
                vTmpMap.image_Category.hidden = NO;
                vTmpMap.image_RealFilter.hidden = YES;
                vTmpMap.lbQui.hidden = YES;
                
                UINavigationBar *navBar=self.navigationController.navigationBar;
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
                //add sub navigation
                [self addMainNav:@"MainNavMUR"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strCHANTIERS)];
                
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.toussearchBar setTintColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                
                [self addSubNav:@"SubNavigationGROUPENTER"];
                
                [vTmpMap.btnShortCut setImage:  [UIImage imageNamed:@"ic_add_new_chasse"] forState:UIControlStateNormal];
                [vTmpMap.btnShortCutSetting setBackgroundImage:  [UIImage imageNamed:@"ic_carter_cole"] forState:UIControlStateNormal];
                [self.btnHuntRefresh setImage:  [UIImage imageNamed:@"btn_refresh_map_hunt"] forState:UIControlStateNormal];
                [self.btnFav setImage:  [UIImage imageNamed:@"ic_favorite_map_hunt"] forState:UIControlStateNormal];
                
                [self.btnLocationAddress setImage:  [UIImage imageNamed:@"ic_search_location_hunt"] forState:UIControlStateNormal];
                
                vTmpMap.indicator.color =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            }
            self.vViewFilter.hidden = NO;
            //allow_add = [0 => ADMIN, 1 => ALL_MEMBERS]
        }
            break;
            
        case ISCARTE:
        {
            self.colorNavigation = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            //CARTE
            float i = 60, j = 10;
            vTmpMap.mapView_.padding = UIEdgeInsetsMake(j,i, j, i); //top left bottom right
            
            vTmpMap.btnCarteFiltreAction.hidden = NO;
            vTmpMap.lbQui.hidden = NO;
            
            vTmpMap.btnCarteFiltreCategory.hidden = NO;
            vTmpMap.image_Category.hidden = NO;
            vTmpMap.image_RealFilter.hidden = NO;
            
            //            UINavigationBar *navBar=self.navigationController.navigationBar;
            //            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
            //add sub navigation
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            
            [self addMainNav:@"CARTER"];
            [self addSubNav:@"CARTER"];
            
            //            subviewCount.
            
            //Set title
            //            MainNavigationBaseView *subview =  [self getSubMainView];
            //            [subview.myTitle setText:str(strCARTE)];
            //
            //            //Change background color
            //            subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.toussearchBar setTintColor:UIColorFromRGB(CARTE_MAIN_BAR_COLOR)];
            
            [vTmpMap.btnShortCut setImage:  [UIImage imageNamed:@"btn_add_on_map"] forState:UIControlStateNormal];
            [self.btnLocationAddress setImage:  [UIImage imageNamed:@"ic_search_location"] forState:UIControlStateNormal];
            
            vTmpMap.indicator.color =UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            self.vViewFilter.hidden = NO;
            
            [self addShortCutSetting];
            [self addShortCut];
        }
            break;
            
        default:
            break;
    }
    [self addSubViewFilter];
    
    canRequestMedia = NO;
    
    self.indexTracking= 0 ; //show current location
    
    //Default => type 14...If not...this session using cached type.
    
    iMapType = (int) [[NSUserDefaults standardUserDefaults] integerForKey:@"defaultMapType" ];
    [self doMapType:iMapType]; //
    
    //Cart ..displai last point
    //    if (self.expectTarget == ISCARTE || self.isLiveHunt)
    //    {
    //        //livehunt -> reset...default view all
    //        if (self.isLiveHunt)
    //        {
    //            bFilterON = NO;
    //        }
    //
    [self getFilter];
    //    }
    //    else
    //    {
    //        bFilterON = NO;
    //    }
    
    //    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
    //
    //        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_distributor"];
    //
    //        FMResultSet *set_querry = [db  executeQuery:strQuerry];
    //
    //        BOOL iCountCarte = NO;
    //        while ([set_querry next])
    //        {
    //            iCountCarte = YES;
    //        }
    //
    //        //update distribution to db
    //        if (iCountCarte) {
    //            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_distributor]];
    //        }
    //
    //    }];
    
    //time to get distr
    
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_distributor,kSQL_publication, kSQL_shape,kSQL_agenda]];
    
    [self fnCategory];
    
    [self fnFilterChangeImage];
    
    [self.vViewFilter.layer setMasksToBounds:YES];
    self.vViewFilter.layer.cornerRadius= 4.0;
    self.vViewFilter.layer.borderWidth =0.5;
    self.vViewFilter.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    // drop shadow
    [self.vViewFilter.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.vViewFilter.layer setShadowOpacity:0.8];
    [self.vViewFilter.layer setShadowRadius:3.0];
    [self.vViewFilter.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    
    //    near soucia
    //ggtt testing Patrick Region
    
    //    [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:46.528037 longitude:5.776024 zoom:iZoomDefaultLevel]];
    
    //    46.528037  5.776024 around this
    
    //    return;
    
    
    
    //From photo or has location data...
    if (self.expectTarget == ISLOUNGE && self.isLiveHunt) {
        //On naturalive, the view should be centered on your position when you open it. It's not like general map
        if([self locationCheckStatusDenied])
        {
            [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
            return;
        }
        
        
        //Default Zoom level
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
        
        //focus to my location
        self.indexTracking = 0;
        //        [self doFnTracking:NO];
        
        
    }else if ( [[self.simpleDic objectForKey:@"latitude"] doubleValue] != 0 && [[self.simpleDic objectForKey:@"longitude"] doubleValue]!=0 )
    {
        //        MKCoordinateSpan span = MKCoordinateSpanMake(0.001, 0.001);
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [[self.simpleDic objectForKey:@"latitude"] doubleValue];
        simpleCoord.longitude= [[self.simpleDic objectForKey:@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
    }else{
        
        //Load latest point & data cached
        NSDictionary * theCenter = [[NSUserDefaults standardUserDefaults] objectForKey:concatstring([COMMON getUserId],@"CACHE_LAST_POINT")];
        if (theCenter != nil) {
            
            self.simpleDic = @{
                               @"latitude":  theCenter[@"mapLat"],
                               @"longitude":  theCenter[@"mapLong"]
                               };
        }
        
        
        if ( [[self.simpleDic objectForKey:@"latitude"] doubleValue] != 0 && [[self.simpleDic objectForKey:@"longitude"] doubleValue]!=0 )
        {
            CLLocationCoordinate2D simpleCoord;
            simpleCoord.latitude = [[self.simpleDic objectForKey:@"latitude"] doubleValue];
            simpleCoord.longitude= [[self.simpleDic objectForKey:@"longitude"] doubleValue];
            
            vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
            
        }else{
            if([self locationCheckStatusDenied])
            {
                [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
                return;
            }
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
            
        }
        
    }
}

-(void) fnUpdateItinerary
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString*lat = [NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.latitude];
    NSString*lon = [NSString stringWithFormat:@"%f",appDelegate.locationManager.location.coordinate.longitude];
    
    NSString*hunt_id = [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //get latest Lat/Lon by order
        FMResultSet *set_querry = [db  executeQuery:[NSString stringWithFormat:@"SELECT * from tb_location_points WHERE c_livehunt_id=%@ ORDER BY c_id DESC LIMIT 1",hunt_id ] ];
        
        NSString *strLat = nil;
        NSString *strLon = nil;
        
        while ([set_querry next])
        {
            strLat = [set_querry stringForColumn:@"c_lat"];
            strLon = [set_querry stringForColumn:@"c_lon"];
        }
        
        //check delta location
        
        //latest location
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:strLat.floatValue  longitude:strLon.floatValue];
        
        // calculate distance between them
        CLLocationDistance distance = [location distanceFromLocation:appDelegate.locationManager.location];
        NSLog(@">>>>>>>>>>   %f",distance);
        if (distance > 2.0f)
        {
            //the change for distance is significantly -> update new location...Draw on the map. new point
            NSString *strSql = [NSString stringWithFormat: @"INSERT OR REPLACE INTO `tb_location_points` (`c_livehunt_id`,`c_lat`,`c_lon`) VALUES ('%@','%@','%@')", hunt_id, lat, lon ] ;
            //add version number to tble version.
            
            [db  executeUpdate:strSql];
            
            
            //main thread
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSDictionary *dicDrawShape =     @{
                                                   @"c_lat": lat,
                                                   @"c_lon": lon,
                                                   };
                [self addMarkerDrawLines:dicDrawShape];
                
            });
        }
    }];
    
    
}

-(void) willMoveToParentViewController:(UIViewController *)parent
{
    [super willMoveToParentViewController:parent];
    if (!parent) {
        
        NSTimer *timer = self.continuousPressTimer;
        if (timer) {
            [timer invalidate];
            self.continuousPressTimer = nil;
        }
        
        
        
        //        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_REFRESH_MES_NEW object:nil];
    }
}


-(void)fnSetSwitchFilter
{
    NSDictionary   *filterDic;
    switch (self.expectTarget) {
        case ISGROUP:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_GROUP"];
        }
            break;
        case ISLOUNGE:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_LOUNGE"];
            
        }
            break;
        default:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
        }
            break;
    }
    NSString *strFilter2 =@"";
    
    if (bFilterON) {
        int count = 0;
        if ([filterDic[@"filterQUI"] boolValue]) {
            count +=1;
            strFilter2 = @" QUI ?";
        }
        if ([filterDic[@"filterQUOI"] boolValue]) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" QUOI ?"];
        }
        if ([filterDic[@"filterQUAND"] boolValue]) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" QUAND ?"];
        }
        if (self.myC_Search_Text.length > 0) {
            count +=1;
            strFilter2 = [NSString stringWithFormat:@"%@%@",strFilter2,@" MOTS-CLES"];
        }
        if (count > 1) {
            lbFilter1.text = @"Filtres actifs :";
        }
        else if (count == 1) {
            lbFilter1.text = @"Filtre actif :";
        }
        else
        {
            lbFilter1.text = @"Filtre actif";
            
        }
    }
    else
    {
        lbFilter1.text = @"Filtre Désactivé";
        
    }
    
    lbFilter2.text = strFilter2;
    jtSwitch.on = bFilterON;
    jtSwitch.transform = CGAffineTransformMakeScale(0.65, 0.65);
    switch (self.expectTarget) {
        case ISMUR:
        {
            [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH)];
        }
            break;
        case ISGROUP:
        {
            [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH_GROUP)];
        }
            break;
        case ISLOUNGE:
        {
            if (self.isLiveHunt) {
                [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
            }
            else
            {
                [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH_CHASSES)];
            }
        }
            break;
        default:
        {
            [jtSwitch setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
        }
            break;
    }
    [jtSwitch setBackgroundColor:UIColorFromRGB(OFF_SWITCH)];
    jtSwitch.layer.cornerRadius = 16;
}
-(IBAction)filterSwitchAction:(id)sender
{
    NSDictionary   *filterDic;
    switch (self.expectTarget) {
        case ISGROUP:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_GROUP"];
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic addEntriesFromDictionary:filterDic];
            UISwitch *sv = (UISwitch*)sender;
            [dic setObject:[NSNumber numberWithBool:sv.on] forKey:@"bFilterON"];
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"OBJECT_FILTER_GROUP"];
            
        }
            break;
        case ISLOUNGE:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_LOUNGE"];
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic addEntriesFromDictionary:filterDic];
            UISwitch *sv = (UISwitch*)sender;
            [dic setObject:[NSNumber numberWithBool:sv.on] forKey:@"bFilterON"];
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:@"OBJECT_FILTER_LOUNGE"];
            
        }
            break;
        default:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic addEntriesFromDictionary:filterDic];
            UISwitch *sv = (UISwitch*)sender;
            [dic setObject:[NSNumber numberWithBool:sv.on] forKey:@"bFilterON"];
            [[NSUserDefaults standardUserDefaults]setObject:dic forKey:
             [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]] ];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        }
            break;
    }
    
    [self getFilter];
    [self fnSetSwitchFilter];
    
    //set data selected
    self.arrCategoriesFilter = filterDic[@"listID"];
    [self removeAllPublication];
    [self removeAllAgenda];
    [self.dicOverlays removeAllObjects];
    
    [vTmpMap.mapView_ clear];
    
    [self doMapType:iMapType];
    
    [self fnCategory];
    
    [self forceRefresh];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSelector:@selector(refreshCircle) withObject:nil afterDelay:0.75];
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fnSetSwitchFilter];
    isGoOutScreen = NO;
    
    NSDictionary *dic = nil;
    
    switch (self.expectTarget) {
            
        case ISMUR:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            [vTmpMap.btnPrint setImage: [UIImage imageNamed:@""] forState:UIControlStateNormal];
            
        }
            break;
        case ISGROUP:
        {
            vTmpMap.btnPrint.hidden = YES;
            
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            //            [vTmpMap.btnPrint setImage: [UIImage imageNamed:@"ic_imprint_group"] forState:UIControlStateNormal];
            self.constraintButtonLeftMenu.constant = 0;
            imgLeftMenu.hidden = YES;
            
        }
            break;
            
        case ISLOUNGE:
        {
            
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            
            vTmpMap.btnPrint.hidden = YES;
            self.constraintButtonLeftMenu.constant = 0;
            imgLeftMenu.hidden = YES;
            
            [self checkAllow];
            if (!allow_show_chat) {
                [self removeItem:3];
            }
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
            
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }
            
            if (self.isLiveHunt) {
                //                [vTmpMap.btnPrint setImage: [UIImage imageNamed:@"ic_imprint_live"] forState:UIControlStateNormal];
                [self addMainNav:@"CARTER"];
                UINavigationBar *navBar=self.navigationController.navigationBar;
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
            }else{
                
                //                [vTmpMap.btnPrint setImage: [UIImage imageNamed:@"ic_imprint_hunt"] forState:UIControlStateNormal];
                
            }
            
        }
            break;
            
            break;
        case ISCARTE:
        {
            [vTmpMap.btnPrint setImage: [UIImage imageNamed:@"ic_imprint"] forState:UIControlStateNormal];
            vTmpMap.btnPrint.hidden = NO;
            
            self.constraintButtonLeftMenu.constant = 38;
            imgLeftMenu.hidden = NO;
            
            //            UINavigationBar *navBar=self.navigationController.navigationBar;
            //            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
            [self addMainNav:@"CARTER"];
            [self addSubNav:@"CARTER"];
            
        }
            break;
        default:
            break;
    }
    
    if (self.isSubVC == NO || self.isLiveHunt) {
        
        [self setThemeNavSub:NO withDicOption:dic];
        
        UIButton *btn = [self returnButton];
        
        [btn setSelected:YES];
        [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
    }
    
    [self forceRefresh];
    
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    //TOP menu
    
    //    constraintTOP.constant = 0;
    
    //Hide left bubble..
    
    constraintLEFT.constant = - CGRectGetWidth(vContainerLeft.frame) + 5;
    btnShow.hidden = NO;
    btnHide.hidden = YES;
    [self.view setNeedsDisplay];
    
    vPopup.hidden = YES;
    
    [self hideIndicator];
    //radar
    _arViewController = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    isGoOutScreen = YES;
    
    //reset position
    constraintLEFT.constant = - CGRectGetWidth(vContainerLeft.frame) + 5;
    btnShow.hidden = NO;
    btnHide.hidden = YES;
    [self.view setNeedsDisplay];
    [self fnCloseVPop:nil];
    
}

-(ISSCREEN) getTypeOfRequestScreen
{
    return ISCARTE;
}

- (IBAction)doLeftMenu:(id)sender {
    
    constraintLEFT.constant =0;
    [self.view setNeedsDisplay];
    
    
    //release Map
    [self exitMapView];
    
    [self doRemoveObservation];
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}

-(void) updateRightToChat:(NSNotification*)notif
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //refresh param...
        [self checkAllow];
        //refresh UI
        if (self.expectTarget == ISLOUNGE)
        {
            [self.viewShortCut fnAllowAdd:allow_add];
            
            //            if (self.viewChat) {
            //                self.viewChat.allow_add_chat = allow_add_chat;
            //                self.viewChat.allow_show_chat = allow_show_chat;
            //
            //                self.viewChat.allow_add = allow_add;
            //                self.viewChat.allow_show = allow_show;
            //
            //
            //                //refresh input text
            //                [self.viewChat refreshTextInput];
            //                [self.viewChat getLiveChatWithMore: NO];
            //            }
            //            else
            {
                //hid/display subview discustion
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strCHANTIERS)];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self addSubNav:@"SubNavigationGROUPENTER"];
                
                if (!allow_show_chat)
                {
                    [self removeItem:3];
                }
                if (self.needRemoveSubItem)
                {
                    [self removeItem:4];
                }
                
                [self setThemeNavSub:NO withDicOption:@{@"admin":@0}];
                UIButton *btn = [self returnButton];
                [btn setSelected:YES];
                [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
                
            }
        }
        else if (self.expectTarget == ISGROUP)
        {
            [self.viewShortCut fnAllowAdd:allow_add];
        }
        
    });
}


//in main thread
-(void)updateUICarte
{
    //Processing Agenda filter.
    //check to draw Agenda
    if (vTmpMap.mapView_.camera.zoom >= MINZOOM) {
        //condition: masterfilter + (agenda only OR All)
        if (( isEnableMasterFilter && (iMasterFilterType == EVENEMENTS || iMasterFilterType == TOUT))
            || !isEnableMasterFilter )
        {
            [self doShowAgendas];
        }
    }
    
    //Stop load view...
    //If Filter ON, ma ko chon gi -> msg
    if (bWarningNoFilterSelected == YES) {
        
        [self hideIndicator];
        return;
    }
    
    if (vTmpMap.mapView_.camera.zoom < MINZOOM) {
        
        [self hideIndicator];
        if (!checkRedraw) {
            checkRedraw = YES;
            if (arrToRedraw.count > 0) {
                [self performSelector:@selector(fnRedrawPointWithNewZoom:) withObject:arrToRedraw];
            }
            //remove distribution
            [self hideShowDistribution:YES];
        }
        
        return;
    }
    else
    {
        checkRedraw = NO;
    }
    
    [self showIndicator];
    
    // check to draw Publications
    if (( isEnableMasterFilter && (iMasterFilterType == ANIMAUX || iMasterFilterType == EQUIPEMENTS ||
                                   iMasterFilterType == HABITATS || iMasterFilterType == TOUT))
        || !isEnableMasterFilter )
    {
        
        [self doPublication];
        //check to draw shapes

        [self fnGetShapes];
    }

    //check to draw distribution
    if (( isEnableMasterFilter && (iMasterFilterType == ARMURERIES || iMasterFilterType == TOUT))
        || !isEnableMasterFilter )
    {
        [self doDistribution];
    }

}

#pragma mark - GMS marker

-(void)GMSaddMakeAgenda:(NSDictionary*)dic
{
    //check marker exist again?!
    if (self.myDIC_MarkerAgenda[dic[@"c_id"]] != nil) {
        
        return;
    }
    
    CLLocationCoordinate2D position;
    position.latitude = [dic[@"c_meeting_lat"] doubleValue];
    position.longitude= [dic[@"c_meeting_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    __block  NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
    [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
    marker.userData = dicMul;
    //    marker.infoWindowAnchor = CGPointMake(0.5, 0.25);
    
    MDMarkersAgenda *tmpImage= [[MDMarkersAgenda alloc] initWithDictionary:dicMul];
    [tmpImage setCallBackMarkers:^(UIImage *image,UIImage *image_nonLegend, float percent_arrow)
     {
         //
         [dicMul setObject:image forKey:@"image"];
         [dicMul setObject:@(percent_arrow) forKey:@"percent_arrow"];
         
         marker.userData = dicMul;
         marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
         
         marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_AGENDA];
         
         //
         marker.map = vTmpMap.mapView_;
         //add arr
         [self addAgenda:marker];
     }];
    [tmpImage doSetCalloutView];
    
    [self addAgenda:marker];
}

-(void)GMSaddMakeMap:(NSDictionary*)dic
{
    //check marker exist again?!
    if (self.myDIC_MarkerPublication[dic[@"c_id"]] != nil) {
        
        return;
    }
    
    CLLocationCoordinate2D position;
    position.latitude = [dic[@"c_lat"] doubleValue];
    position.longitude= [dic[@"c_lon"] doubleValue];
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    
    __block  NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
    [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
    marker.userData = dicMul;
    //    marker.infoWindowAnchor = CGPointMake(0.5, 0.25);
    
    MDMarkersCustom *tmpImage= [[MDMarkersCustom alloc] initWithDictionary:dicMul];
    [tmpImage setCallBackMarkers:^(UIImage *image,UIImage *image_nonLegend, float percent_arrow)
     {
         //
         [dicMul setObject:image forKey:@"image"];
         [dicMul setObject:@(percent_arrow) forKey:@"percent_arrow"];
         NSString *strLegend = dicMul[@"c_legend"];
         if (strLegend.length>0) {
             [dicMul setObject:image_nonLegend forKey:@"image_nonLegend"];
             [dicMul setObject:@(0.5) forKey:@"percent_arrow_nonLegend"];
         }
         marker.userData = dicMul;
         if (iZoomDefaultLevel < kSizeMarkerZoom && strLegend.length>0) {
             marker.groundAnchor = CGPointMake(0.5, 1.0);
             marker.icon = [self resizeImage:image_nonLegend withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
             
         }
         else
         {
             marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
             marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
         }
         
         //
         marker.map = vTmpMap.mapView_;
         //add arr
         [self addPublication:marker];
     }];
    [tmpImage doSetCalloutView];
    [self addPublication:marker];
}

-(void)GMSaddMarkerDistributionMap:(NSDictionary*)dic
{
    //check distribution exist again?!
    if (self.dicMarkerDistribution[dic[@"c_id"]] != nil) {
        return;
    }
    
    CLLocationCoordinate2D position;
    position.latitude = [dic[@"c_lat"] doubleValue];
    position.longitude= [dic[@"c_lon"] doubleValue];
    
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.infoWindowAnchor = CGPointMake(0.5, 0.5);
    marker.groundAnchor = CGPointMake(0.0, 1.0);
    
    __block NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
    [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
    marker.userData = dicMul;
    
    MDMarkersDistribution *tmpImage= [[MDMarkersDistribution alloc] initWithDictionary:dicMul];
    [tmpImage setCallBackMarkers:^(UIImage *image)
     {
         [dicMul setObject:image forKey:@"image"];
         marker.userData = dicMul;
         
         marker.icon = [self resizeImageDistribution:image withLevelZoom:iZoomDefaultLevel];
         marker.map = vTmpMap.mapView_;
         //add arr
         [self.dicMarkerDistribution setObject:marker forKey:marker.userData[@"c_id"]];
     }];
    
    [tmpImage doSetCalloutView];
    [self.dicMarkerDistribution setObject:marker forKey:marker.userData[@"c_id"]];
}

-(void) enablebFlagBusy
{
    bFlagBusy = NO;
}

#pragma mark - GESTURE
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - OVERWRITE MAPVIEW

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    if (isGoOutScreen) return;
    
    visibleRegion = vTmpMap.mapView_.projection.visibleRegion;
    
    [super mapView:mapView idleAtCameraPosition:cameraPosition];
    
    //Set warning
    if (vTmpMap.mapView_.camera.zoom < MINZOOM) {
        //warning message
        [vTmpMap.warningZoom setText:str(strWarningZoom)];
    }else{
        [vTmpMap.warningZoom setText:@""];
    }
    
    //CENTER
    float lat = vTmpMap.mapView_.camera.target.latitude;
    float lng = vTmpMap.mapView_.camera.target.longitude;
    
    //continue
    strLatitude=[NSString stringWithFormat:@"%f",lat];
    strLongitude=[NSString stringWithFormat:@"%f",lng];
    [self refreshCircle];
    
    if ( self.indexTracking == 1) { //mean is heading...mode
        
        if (bFlagBusy == NO) {
            [self forceRefresh];
            [NSTimer scheduledTimerWithTimeInterval:7 target:self selector:@selector(enablebFlagBusy) userInfo:nil repeats:NO];
            
        }
        //do nothing
    }else{
        //if end drag...normal mode...
        [self doReloadRefresh];
        
    }
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@{@"mapLat":[NSNumber numberWithFloat:lat],
                                                       @"mapLong":[NSNumber numberWithFloat:lng]} forKey:concatstring([COMMON getUserId],@"CACHE_LAST_POINT")];
    [[NSUserDefaults standardUserDefaults]  synchronize];
}


#pragma mark - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    [super onSubNavClick:btn];
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        default:
            break;
    }
}

#pragma mark -get Filter

-(void) getFilter
{
    //    if (self.expectTarget != ISCARTE && self.isLiveHunt == NO) {
    //        return;
    //    }
    
    bWarningNoFilterSelected = NO;
    
    //only carter can use Filter...group/hunt
    
    NSDictionary   *filterDic;
    switch (self.expectTarget) {
        case ISGROUP:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_GROUP"];
        }
            break;
        case ISLOUNGE:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : @"OBJECT_FILTER_LOUNGE"];
            
        }
            break;
        default:
        {
            filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
        }
            break;
    }
    self.strFilterMoi = [filterDic[@"iSharingMoi"] stringValue];
    self.strFilterAmis = [filterDic[@"iSharingAmis"] stringValue];
    
    self.strFilterDebut = filterDic[@"debut"];
    self.strFilterFin = filterDic[@"fin"];
    self.myC_Search_Text = filterDic[@"c_search"];
    self.arrCategoriesFilter = filterDic[@"listID"];
    
    if ([filterDic[@"bFilterON"] boolValue] ) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }
    if( filterDic !=nil)
    {
        if (bFilterON)
        {
            //Personne
            NSString *strPath_Personne = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_PERSONNE_SAVE)  ];
            NSArray *arrTmp_Personne = [NSArray arrayWithContentsOfFile:strPath_Personne];
            
            NSMutableArray *childPerson = [NSMutableArray new];
            for (NSDictionary *dicPersonne in arrTmp_Personne) {
                if ([dicPersonne[@"status"] boolValue]) {
                    [childPerson addObject:dicPersonne[@"id"]];
                }
            }
            self.strFilterPersonne = [childPerson componentsJoinedByString:@","];
            //groupID
            NSString *strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_MES_GROUP_SAVE)  ];
            NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
            
            NSMutableArray *arrMut = [NSMutableArray new];
            
            if (arrTmp.count > 0) {
                
                for (NSDictionary*dic in arrTmp) {
                    if ([dic[@"status"] boolValue] == YES) {
                        [arrMut addObject:dic[@"id"]];
                    }
                }
            }
            
            self.strMesGroupFilter = [arrMut componentsJoinedByString:@","];
            
            //HUNT
            
            strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],FILTER_MES_HUNT_SAVE)  ];
            arrTmp = [NSArray arrayWithContentsOfFile:strPath];
            
            [arrMut removeAllObjects];
            
            if (arrTmp.count > 0) {
                
                for (NSDictionary*dic in arrTmp) {
                    if ([dic[@"status"] boolValue] == YES) {
                        [arrMut addObject:dic[@"id"]];
                    }
                }
            }
            
            self.strMesHuntFilter = [arrMut componentsJoinedByString:@","];
        }
    }
    
    [self fnFilterChangeImage];
    
    if (bFilterON == YES)
    {
        //ALARM
        if ((self.strMesGroupFilter == nil || [self.strMesGroupFilter isEqualToString:@""]) &&
            (self.strMesHuntFilter == nil || [self.strMesHuntFilter isEqualToString:@""]) &&
            (self.strFilterPersonne == nil || [self.strFilterPersonne isEqualToString:@""])&&
            (self.strFilterMoi.intValue == 0)&&
            (self.strFilterAmis.intValue == 0)&&
            self.strFilterDebut == nil &&
            self.strFilterFin == nil&&
            self.arrCategoriesFilter == nil&&
            (self.myC_Search_Text == nil || [self.myC_Search_Text isEqualToString:@""])) {
            
            vTmpMap.warning.hidden = NO;
            vTmpMap.warning.text = str(strWarningONWithoutSelection);
            bWarningNoFilterSelected = YES;
            
            [vTmpMap.warning setNeedsDisplay];
        }else{
            vTmpMap.warning.hidden = TRUE;
            vTmpMap.warning.text = @"";
            
            [vTmpMap.warning setNeedsDisplay];
            
        }
        
    }else{
        vTmpMap.warning.hidden = TRUE;
        vTmpMap.warning.text = @"";
        [vTmpMap.warning setNeedsDisplay];
        
    }
    
}

-(void)fnCategory
{
    NSString *strNameImage =@"";
    switch (self.expectTarget) {
        case ISCARTE:
        {
            strNameImage = @"ic_map_filter_on";
        }
            break;
        case ISGROUP:
        {
            strNameImage = @"group_ic_filter_category_on";
        }
            break;
        case ISLOUNGE:
        {
            if (self.isLiveHunt) {
                strNameImage = @"live_carte_filtre_repere_on";
            }
            else
            {
                strNameImage = @"chasses_ic_filter_category_on";
            }
        }
            break;
        default:
            break;
    }
    if (bFilterON == NO) {
        [vTmpMap.image_Category setImage: [UIImage imageNamed:@"ic_map_filter"]];
    }else{
        [vTmpMap.image_Category setImage: [UIImage imageNamed:strNameImage]];
        
    }
}

-(void)fnFilterChangeImage
{
    if (bFilterON == NO) {
        [vTmpMap.image_RealFilter setImage: [UIImage imageNamed:@"ic_map_filter_amis"]];
    }else{
        if (self.isLiveHunt) {
            
            [vTmpMap.image_RealFilter setImage: [UIImage imageNamed:@"live_carte_ic_type_contenu_on"] ];
        }
        else
        {
            [vTmpMap.image_RealFilter setImage: [UIImage imageNamed:@"carter_filter_on"] ];
            
        }
    }
    
    
}

#pragma mark - alert

-(IBAction)CarteStatusAction:(id)sender
{
    CarteStatusVC *vc = [[CarteStatusVC alloc] initFromParent];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
        
    }];
    [vc showInVC:self];
}

//Category filtering
-(IBAction)CarteTypeContentAction:(id)sender
{
    [self cancelCurrentTasks];
    
    //    isChooseAction = YES;
    CarteTypeContentVC *vc = [[CarteTypeContentVC alloc] initFromParent];
    vc.expectTarget = self.expectTarget;
    //set def value from current
    vc.arrNodes =arrNodes;
    [vc setMyCallback:^(NSDictionary*dicMul){
        arrNodes = dicMul[@"nodes"];
        
        NSDictionary *data = dicMul[@"filter"];
        
        
        //set data selected
        self.arrCategoriesFilter = data[@"listID"];//@[@"101",@"106",@"107"];
        [self removeAllPublication];
        [self removeAllAgenda];
        [self.dicOverlays removeAllObjects];
        
        [vTmpMap.mapView_ clear];
        
        [self doMapType:iMapType];
        
        [self getFilter];
        [self fnCategory];
        
        [self forceRefresh];
    }];
    [vc showInVC:self];
}

-(IBAction)CarteVentAction:(id)sender
{
    double lat = vTmpMap.mapView_.camera.target.latitude;
    double lng = vTmpMap.mapView_.camera.target.longitude;
    
    AlertVC *vc = [[AlertVC alloc] initVent: @{@"latitude":[NSNumber numberWithFloat:lat], @"longitude" : [NSNumber numberWithFloat:lng]} withColor:self.colorNavigation];
    
    [vc doBlock:^(NSInteger index, NSString *str) {
        
    }];
    [vc showAlert];
    
    
}

-(IBAction)CarteFiltreparticiantsAction:(id)sender
{
    CarteFiltreparticiants *vc = [[CarteFiltreparticiants alloc] initFromParent];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
    }];
    [vc showInVC:self];
}

//only show when Carter...
//Group -> hidden.
-(IBAction)CarteFiltreAction:(id)sender
{
    [self cancelCurrentTasks];
    
    //    isChooseAction = YES;
    CarteFiltreVC *vc = [[CarteFiltreVC alloc] initFromParent];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
        //filter... -> Clear map...reload data
        //        [arrPublication removeAllObjects];
        [self removeAllPublication];
        [self removeAllAgenda];
        [self.dicOverlays removeAllObjects];
        
        //        Clears all markup that has been added to the map, including markers, polylines and ground overlays.
        [vTmpMap.mapView_ clear];
        
        [self doMapType:iMapType];
        
        [self getFilter];
        
        //alert if any
        
        
        [self forceRefresh];
    }];
    [vc showInVC:self];
}

-(IBAction)CarteMeteoAction:(id)sender
{
    double lat = vTmpMap.mapView_.camera.target.latitude;
    double lng = vTmpMap.mapView_.camera.target.longitude;
    
    AlertVC *vc = [[AlertVC alloc] initMapMeteo: @{@"latitude":[NSNumber numberWithFloat:lat], @"longitude" : [NSNumber numberWithFloat:lng]} withColor:self.colorNavigation];
    
    [vc doBlock:^(NSInteger index, NSString *str) {
        
    }];
    [vc showAlert];
}

-(void)showIndicator
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    vTmpMap.indicator.hidden =NO;
}

-(void)hideIndicator
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    vTmpMap.indicator.hidden =YES;
}

-(void) cancelCurrentTasks
{
    [self hideIndicator];
    
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(TimerTick:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(TimerDistribution:) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(updateUICarte) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(fnRedrawPointWithNewZoom:) object:nil];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
}

-(IBAction)fnAdressFavorite:(id)sender
{
    [self hideBlurBackGround];
    
    [self cancelCurrentTasks];
    
    //    isChooseAction = YES;
    
    Publication_FavoriteAddress *viewController1 = [[Publication_FavoriteAddress alloc] initWithNibName:@"Publication_FavoriteAddress" bundle:nil];
    
    [viewController1 setMyCallback: ^(NSDictionary*dicRet){
        
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [dicRet[@"latitude"] doubleValue];
        simpleCoord.longitude= [dicRet[@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: vTmpMap.mapView_.camera.zoom];
        
        [self removeAllPublication];
        [self removeAllAgenda];
        [self.dicOverlays removeAllObjects];
        
        //        Clears all markup that has been added to the map, including markers, polylines and ground overlays.
        [vTmpMap.mapView_ clear];
        
        [self doMapType:iMapType];
        
        [self forceRefresh];
    }];
    
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
    
}

//Temp only refresh Hunt's map
- (IBAction)fnRefreshHuntMap:(id)sender {
    int alertRefreshCount= [[[NSUserDefaults standardUserDefaults] valueForKey:@"alertRefreshCount"] intValue];
    if (alertRefreshCount < 3) {
        AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strActualiserLaCarte) message:str(strChooseRefreshMap) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
        
        [vc doBlock:^(NSInteger index, NSString *str) {
            if (index==0) {
                // NON
            }
            else if(index==1)
            {
                //OUI
                [self showIndicator];
                
                [[MapDataDownloader sharedInstance] resetParam];
                [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_publication]];
            }
        }];
        [vc showAlert];
        [[NSUserDefaults standardUserDefaults] setValue:@(alertRefreshCount + 1) forKey:@"alertRefreshCount"];
        
    }
    else
    {
        [self showIndicator];
        [[MapDataDownloader sharedInstance] resetParam];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_publication, kSQL_shape]];
    }
    
}

-(void) forceRefresh
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [self performSelector:@selector(updateUICarte) withObject:nil afterDelay:2];
    
}

-(void)doReloadRefresh
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    
    [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(updateUICarte) userInfo:nil repeats:NO];
    
}

#pragma mark -Radar
-(IBAction)showRadarAction:(id)sender
{
    _arViewController = [[ARViewController alloc] initWithDelegate:self];
    _arViewController.showsCloseButton = false;
    [_arViewController setRadarRange:1000.0];
    [_arViewController setOnlyShowItemsWithinRadarRange:YES];
    _arViewController.strFilter = [self genStringFilter];
    [_arViewController setModalTransitionStyle: UIModalTransitionStyleCrossDissolve];
    
    [self presentViewController:_arViewController animated:YES completion:nil];
}
-(NSString*) genStringFilter{
    
    //Load publication
    NSMutableString *mutStr = [NSMutableString new];
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    [mutStr setString:@"SELECT DISTINCT * FROM tb_carte "];
    [mutStr appendString: [NSString stringWithFormat:@" WHERE ((c_lat BETWEEN  [southWestLat] AND [northEastLat]) AND (c_lon BETWEEN [southWestLon] AND [northEastLon])) "]];
    [mutStr appendString:[ NSString stringWithFormat:@" AND ( c_user_id=%@ OR c_user_id= %d)", sender_id, SPECIAL_USER_ID]];
    
    //c_search
    if (self.myC_Search_Text && ![self.myC_Search_Text isEqualToString:@""]) {
        [mutStr appendString: [NSString stringWithFormat:@" AND c_search LIKE \"%%%@%%\"  ",self.myC_Search_Text]];
    }
    
    [mutStr appendString:[self stringFilterWithTableCarte:YES forShape:NO]];
    return mutStr;
}


- (void)locationClicked:(ARGeoCoordinate *)coordinate{
    NSLog(@"%@", coordinate);
}

- (NSMutableArray *)geoLocations{
    NSMutableArray *locationArray = [[NSMutableArray alloc] init];
    ARGeoCoordinate *tempCoordinate;
    CLLocation       *tempLocation;
    NSArray*keys=[self.myDIC_MarkerPublication allKeys];
    for (id c_id in keys) {
        GMSMarker *marker = self.myDIC_MarkerPublication[c_id];
        NSDictionary *dic = marker.userData;
        tempLocation = [[CLLocation alloc] initWithLatitude:
                        [dic[@"c_lat"] floatValue]
                                                  longitude:[dic[@"c_lon"] floatValue]];
        
        tempCoordinate = [ARGeoCoordinate coordinateWithLocation:tempLocation dicPublication:dic];
        
        [locationArray addObject:tempCoordinate];
        
    }
    return locationArray;
}
#pragma -mark get shapes

-(void)fnGetShapes
{
    ASLog(@"WRITE TO MAPPP>>>>>");
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    // Create and enqueue an operation using the previous method
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //1
        //    //check if visible publication has been deleted?
        //
        //    //check all? exist
        
        
        NSMutableArray *discardedItems = [NSMutableArray array];
        
        //MARK: If shape not in DB... -> del
        NSArray*keys=[self.dicOverlays allKeys];
        for (id c_id in keys) {
            id overlay = self.dicOverlays[c_id];
            NSString *strCid =overlay[@"c_id"];
            if (strCid.length > 0)
            {
                NSString *strQuerry = [NSString stringWithFormat:@"SELECT c_id FROM tb_shape WHERE c_id==%@ ORDER BY c_updated DESC LIMIT 1",strCid];
                
                FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
                
                BOOL isExist = NO;
                
                while ([set_querry1 next])
                {
                    isExist = YES;
                }
                
                //point in map not exist in DB -> Delete
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (isExist == NO)
                    {
                        
                        if ([overlay[@"shape"] isKindOfClass: [GMSPolyline class]]) {
                            [discardedItems addObject:overlay];
                            
                            GMSPolyline *polyline = (GMSPolyline*)overlay[@"shape"];
                            polyline.map = nil;
                            
                            if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                shapeLegendDel.map = nil;
                            }
                        }
                        else if ([overlay[@"shape"] isKindOfClass: [GMSPolygon class]]) {
                            [discardedItems addObject:overlay];
                            
                            GMSPolygon *polygon = (GMSPolygon*)overlay[@"shape"];
                            polygon.map = nil;
                            
                            if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                shapeLegendDel.map = nil;
                            }
                            
                        }
                        else if ([overlay[@"shape"] isKindOfClass: [GMSCircle class]]) {
                            [discardedItems addObject:overlay];
                            GMSCircle *pvcircle = (GMSCircle*)overlay[@"shape"];
                            pvcircle.map = nil;
                            
                            if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                shapeLegendDel.map = nil;
                            }
                        }
                        [self.dicOverlays removeObjectForKey:c_id];
                    }
                });
                
            }
        }
        
        
        //select All
        NSMutableString *mutStr = [NSMutableString new];
        
        
        // WHERE 0 OR ...correct
        // WHERE TRUE ...wrong
        [mutStr setString:@"SELECT DISTINCT * FROM tb_shape "];
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithRegion: visibleRegion];
        
        CLLocationCoordinate2D m_MOBILE_NE = bounds.northEast;
        CLLocationCoordinate2D m_MOBILE_SW = bounds.southWest;
        
        [mutStr appendString: [NSString stringWithFormat:@" WHERE (    (\
                               (%f BETWEEN  c_ne_lat AND c_sw_lat) OR\
                               (%f BETWEEN  c_ne_lat AND c_sw_lat) OR\
                               (c_ne_lat BETWEEN  %f AND %f) OR\
                               (c_sw_lat BETWEEN  %f AND %f)\
                               ) AND (\
                               (%f BETWEEN c_ne_lng AND c_sw_lng) OR\
                               (%f BETWEEN c_ne_lng AND c_sw_lng) OR\
                               (c_ne_lng BETWEEN %f AND %f) OR \
                               (c_sw_lng BETWEEN %f AND %f)\
                               )\
                               ) ",
                               m_MOBILE_SW.latitude ,
                               m_MOBILE_NE.latitude ,
                               m_MOBILE_SW.latitude ,m_MOBILE_NE.latitude, m_MOBILE_SW.latitude ,m_MOBILE_NE.latitude,
                               m_MOBILE_SW.longitude, m_MOBILE_NE.longitude ,m_MOBILE_SW.longitude , m_MOBILE_NE.longitude,
                               m_MOBILE_SW.longitude , m_MOBILE_NE.longitude ] ];
        
        
        
        [mutStr appendString:[ NSString stringWithFormat:@" AND c_user_id=%@ ", sender_id]];
        
        //2 checking: if atleast 1 point is in the visible area...or ...center of it in...=> 5 points.
        [mutStr appendString:[self stringFilterWithTableCarte:NO forShape:NO]];
        //format data to shape object
        
        FMResultSet *set_querry = [db  executeQuery:mutStr];
        
        NSMutableArray *arrShape = [NSMutableArray new];
        
        while ([set_querry next]) {
            NSDictionary *c_data =(NSDictionary*)[NSJSONSerialization JSONObjectWithData:[set_querry dataForColumn:@"c_data"] options:0 error:nil];
            [arrShape addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                   @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                   @"c_type":[set_querry stringForColumn:@"c_type"],
                                   @"c_title":[set_querry stringForColumn:@"c_title"],
                                   @"c_description":[set_querry stringForColumn:@"c_description"],
                                   
                                   @"c_sharing":[set_querry stringForColumn:@"c_sharing"],
                                   @"c_groups":[set_querry stringForColumn:@"c_groups"],
                                   @"c_hunts":[set_querry stringForColumn:@"c_hunts"],
                                   @"c_friend":[set_querry stringForColumn:@"c_friend"],
                                   @"c_member":[set_querry stringForColumn:@"c_member"],
                                   
                                   @"c_lat_center":[set_querry stringForColumn:@"c_lat_center"],
                                   @"c_lng_center":[set_querry stringForColumn:@"c_lng_center"],
                                   
                                   @"c_data":c_data,
                                   @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                   //                                   @"c_user_id":[set_querry stringForColumn:@"c_user_id"],
                                   
                                   }];
        };
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            for (NSDictionary*obj in arrShape) {
                
                BOOL isExist = NO;
                
                //check exist?
                NSArray*keys=[self.dicOverlays allKeys];
                for (id c_id in keys) {
                    id overlay = self.dicOverlays[c_id];
                    
                    if ([obj[@"c_id"] intValue] == [overlay[@"c_id"] intValue] )
                    {
                        //isChange...
                        if ([obj[@"c_updated"] intValue] != [overlay[@"c_updated"] intValue] )
                        {
                            if ([overlay[@"shape"] isKindOfClass: [GMSPolyline class]]) {
                                GMSPolyline *polyline = (GMSPolyline*)overlay[@"shape"] ;
                                polyline.map = nil;
                                
                                if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                    GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                    shapeLegendDel.map = nil;
                                }
                                
                                [self loadOptionShapes:@[obj]];
                                break;
                                
                            }
                            else if ([overlay[@"shape"] isKindOfClass:[GMSPolygon class]]) {
                                GMSPolygon *polygon = (GMSPolygon*)overlay[@"shape"] ;
                                
                                polygon.map = nil;
                                
                                if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                    GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                    shapeLegendDel.map = nil;
                                }
                                
                                [self loadOptionShapes:@[obj]];
                                break;
                                
                            }
                            else if ([overlay[@"shape"] isKindOfClass:[GMSCircle class]]) {
                                GMSCircle *pvcircle = (GMSCircle*)overlay[@"shape"] ;
                                pvcircle.map = nil;
                                
                                if ([overlay[@"shapeLegend"] isKindOfClass: [GMSMarker class]]) {
                                    GMSMarker *shapeLegendDel = (GMSMarker*)overlay[@"shapeLegend"];
                                    shapeLegendDel.map = nil;
                                }
                                
                                [self loadOptionShapes:@[obj]];
                                break;
                            }
                            
                        }
                        //
                        isExist = YES;
                        break;
                    }
                }
                if (!isExist)
                {
                    //by pass
                    [mutArr addObject:obj];
                }
                
            }
            
            
        });
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            //add new
            if (mutArr.count > 0) {
                [self performSelector:@selector(ShapesTimerTick:) withObject:mutArr];
            }else{
                [self hideIndicator];
            }
        });
        
    }];
}


-(void) fnRedrawAgendaWithNewZoom :(NSArray*)arrIn{
    
    if (arrIn.count> 0) {
        
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        GMSMarker *marker = (GMSMarker*)mutArr[0];
        
        __block NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:marker.userData];
        [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
        marker.userData = dicMul;
        
        if (dicMul[@"image"]) {
            float percent_arrow = [dicMul[@"percent_arrow"] floatValue];
            marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
            UIImage *image =  (UIImage*)dicMul[@"image"];
            marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_AGENDA];
            //update dic points
            [self.myDIC_MarkerAgenda setObject:marker forKey:marker.userData[@"c_id"]];
            
        }
        
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(fnRedrawAgendaWithNewZoom:) withObject:mutArr afterDelay:0.01];
    }else{
        [self hideIndicator];
    }
}


//MARK: PUBLICATION

-(void) fnRedrawPointWithNewZoom :(NSArray*)arrIn{
    
    //    if (vTmpMap.mapView_.camera.zoom < MINZOOM){
    //
    //        return ;
    //    }
    
    if (arrIn.count> 0) {
        
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        GMSMarker *marker = (GMSMarker*)mutArr[0];
        
        __block NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:marker.userData];
        [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
        marker.userData = dicMul;
        
        if (dicMul[@"image"]) {
            NSString *strLegend = dicMul[@"c_legend"];
            if (iZoomDefaultLevel < kSizeMarkerZoom && strLegend.length >0) {
                marker.groundAnchor = CGPointMake(0.5, 1.0);
                UIImage *image =  (UIImage*)dicMul[@"image_nonLegend"];
                marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
                
            }
            else
            {
                float percent_arrow = [dicMul[@"percent_arrow"] floatValue];
                marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
                UIImage *image =  (UIImage*)dicMul[@"image"];
                marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
                
            }
            
            //update dic points
            [self.myDIC_MarkerPublication setObject:marker forKey:marker.userData[@"c_id"]];
            
        }
        else
        {
            MDMarkersCustom *tmpImage= [[MDMarkersCustom alloc] initWithDictionary:dicMul];
            [tmpImage setCallBackMarkers:^(UIImage *image,UIImage *image_nonLegend, float percent_arrow)
             {
                 [dicMul setObject:image forKey:@"image"];
                 [dicMul setObject:@(percent_arrow) forKey:@"percent_arrow"];
                 NSString *strLegend = dicMul[@"c_legend"];
                 if (strLegend.length >0) {
                     [dicMul setObject:image_nonLegend forKey:@"image_nonLegend"];
                     [dicMul setObject:@(0.5) forKey:@"percent_arrow_nonLegend"];
                 }
                 marker.userData = dicMul;
                 if (iZoomDefaultLevel < kSizeMarkerZoom && strLegend.length>0) {
                     marker.groundAnchor = CGPointMake(0.5, 1.0);
                     marker.icon = [self resizeImage:image_nonLegend withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
                     
                 }
                 else
                 {
                     marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
                     marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
                 }
                 marker.map = vTmpMap.mapView_;
                 //add arr
                 //update dic points
                 [self.myDIC_MarkerPublication setObject:marker forKey:marker.userData[@"c_id"]];
             }];
            [tmpImage doSetCalloutView];
            
        }
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(fnRedrawPointWithNewZoom:) withObject:mutArr afterDelay:0.01];
    }else{
        //continue distribution drawing...
        [self doDistribution];
    }
}

-(void) doShowAgendas
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithRegion: visibleRegion];
    //area
    northEastFilter= bounds.northEast;
    southWestFilter =bounds.southWest;
    
    NSMutableArray *arrFilterAgenda = [NSMutableArray new];
    
    //points in visible view from Database
    //AGENDA
    if (  self.expectTarget == ISCARTE) {
        NSString *strQueryAgenda = [self stringFilterAgendaWithswLat:southWestFilter.latitude swlon:southWestFilter.longitude neLat:northEastFilter.latitude neLon:northEastFilter.longitude];
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
            FMResultSet *set_querry = [db  executeQuery:strQueryAgenda];
            [arrToRedrawAgenda removeAllObjects];
            
            //Defile Marker for Agenda...static image.
            while ([set_querry next])
            {
                //marker not exist...add new...
                if (self.myDIC_MarkerAgenda[[set_querry stringForColumn:@"c_id"]] == nil) {
                    [arrFilterAgenda addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                                  @"c_name":[set_querry stringForColumn:@"c_name"],
                                                  
                                                  @"c_subscribers": [NSNumber numberWithInt: [set_querry intForColumn:@"c_subscribers"]],
                                                  @"c_photo":[set_querry stringForColumn:@"c_photo"],
                                                  
                                                  
                                                  @"c_access": [NSNumber numberWithInt: [set_querry intForColumn:@"c_access"]],
                                                  @"c_description":[set_querry stringForColumn:@"c_description"],
                                                  @"c_nb_participant": [NSNumber numberWithInt: [set_querry intForColumn:@"c_nb_participant"]],
                                                  @"c_start_date": [set_querry stringForColumn:@"c_start_date"],
                                                  @"c_end_date": [set_querry stringForColumn:@"c_end_date"],
                                                  @"c_meeting_address": [set_querry stringForColumn:@"c_meeting_address"],
                                                  @"c_meeting_lat": [set_querry stringForColumn:@"c_meeting_lat"],
                                                  @"c_meeting_lon": [set_querry stringForColumn:@"c_meeting_lon"],
                                                  @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                                  @"c_marker":@(MARKER_AGENDA),
                                                  }];
                    
                }else{
                    //exist... check update???
                    //if update...remove...add to arrFilter again...!!!!! think it new.
                    
                    GMSMarker *marker =  self.myDIC_MarkerAgenda[[set_querry stringForColumn:@"c_id"]] ;
                    
                    if ([set_querry longForColumn :@"c_updated"] != [marker.userData[@"c_updated"] floatValue]){
                        
                        marker.map = nil;
                        
                        //need to be updated
                        [self removeAgendaWithKey:[set_querry stringForColumn:@"c_id"]];
                        
                        //add new
                        [arrFilterAgenda addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                                      @"c_name":[set_querry stringForColumn:@"c_name"],
                                                      
                                                      @"c_subscribers": [NSNumber numberWithInt: [set_querry intForColumn:@"c_subscribers"]],
                                                      @"c_photo":[set_querry stringForColumn:@"c_photo"],
                                                      
                                                      @"c_access": [NSNumber numberWithInt: [set_querry intForColumn:@"c_access"]],
                                                      
                                                      @"c_description":[set_querry stringForColumn:@"c_description"],
                                                      @"c_nb_participant": [NSNumber numberWithInt: [set_querry intForColumn:@"c_nb_participant"]],
                                                      @"c_start_date": [set_querry stringForColumn:@"c_start_date"],
                                                      @"c_end_date": [set_querry stringForColumn:@"c_end_date"],
                                                      @"c_meeting_address": [set_querry stringForColumn:@"c_meeting_address"],
                                                      @"c_meeting_lat": [set_querry stringForColumn:@"c_meeting_lat"],
                                                      @"c_meeting_lon": [set_querry stringForColumn:@"c_meeting_lon"],
                                                      @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                                      @"c_marker":@(MARKER_AGENDA),
                                                      }];
                        
                    }else{
                        //exist and no data changed, only check if need to reload image...
                        //redraw markers
                        
                        if ([marker.userData[@"c_zoom"]intValue] != iZoomDefaultLevel) {
                            [arrToRedrawAgenda addObject:marker];
                            
                        }
                        
                        
                    }
                    
                }
                
            }
            
            dispatch_async( dispatch_get_main_queue(), ^{
                //add new
                if (arrFilterAgenda.count > 0) {
                    NSLog(@"aff .... count : %lu",(unsigned long)arrFilterAgenda.count);
                    [self performSelector:@selector(TimerTickAgenda:) withObject:arrFilterAgenda];
                }else{
                    if (arrToRedrawAgenda.count > 0) {
                        [self performSelector:@selector(fnRedrawAgendaWithNewZoom:) withObject:arrToRedrawAgenda];
                    }else{
                        [self hideIndicator];
                    }
                    
                    //
                }
                
            });
            
            
        }];
    }
    
}
-(NSString*)stringFilterAgendaWithswLat:(double)swlat swlon:(double)swlon neLat:(double)neLat neLon:(double)neLon
{
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSMutableString *strQueryAgenda = [NSMutableString new];
    //AGENDA allow_show + c_end_date
    NSDate* todayGMT2 = [NSDate convertDateToDateGMT2: [NSDate date]];
    
    NSTimeInterval ti = [todayGMT2 timeIntervalSince1970];
    
    //Show Agenda Map
    [strQueryAgenda setString:@"SELECT DISTINCT * FROM tb_agenda "];
    [strQueryAgenda appendString: [NSString stringWithFormat:@" WHERE ((c_meeting_lat BETWEEN  %f AND %f) AND (c_meeting_lon BETWEEN %f AND %f)) ",swlat,neLat,swlon,neLon] ];
    [strQueryAgenda appendString:[ NSString stringWithFormat:@" AND c_user_id=%@ AND (%f <= c_end_date) ", sender_id, ti]];
    return strQueryAgenda;
}
//MARK: - GET DATA PUBLICATION
-(void) doPublication{
    
    //Load publication
    
    
    NSMutableString *mutStr = [NSMutableString new];
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc]initWithRegion: visibleRegion];
    //area
    northEastFilter= bounds.northEast;
    southWestFilter =bounds.southWest;
    
    //POINTS
    [mutStr appendString:[self stringFilterPublicationWithswLat:southWestFilter.latitude swlon:southWestFilter.longitude neLat:northEastFilter.latitude neLon:northEastFilter.longitude]];
    NSMutableArray *arrFilter = [NSMutableArray new];
    
    
    //POINTS
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        FMResultSet *set_querry = [db  executeQuery:mutStr];
        
        [arrToRedraw removeAllObjects];
        
        while ([set_querry next])
        {
            
            //marker not exist...add new...
            if (self.myDIC_MarkerPublication[[set_querry stringForColumn:@"c_id"]] == nil) {
                [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                        @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                        @"c_legend":[set_querry stringForColumn:@"c_legend"],
                                        @"c_marker_picto":[set_querry stringForColumn:@"c_marker_picto"],
                                        @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                        @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                        @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                        @"c_marker":@(MARKER_PUBLICATION),
                                        }];
                
            }else{
                //exist... check update???
                //if update...remove...add to arrFilter again...!!!!! think it new.
                
                GMSMarker *marker =  self.myDIC_MarkerPublication[[set_querry stringForColumn:@"c_id"]] ;
                
                if ([set_querry longForColumn :@"c_updated"] != [marker.userData[@"c_updated"] floatValue]){
                    
                    //remove on MAP
                    
                    marker.map = nil;
                    
                    //need to be updated
                    [self removePublicationWithKey:[set_querry stringForColumn:@"c_id"]];
                    
                    //add new
                    [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                            @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                            @"c_legend":[set_querry stringForColumn:@"c_legend"],
                                            @"c_marker_picto":[set_querry stringForColumn:@"c_marker_picto"],
                                            @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                            @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                            @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                            @"c_marker":@(MARKER_PUBLICATION),
                                            }];
                    
                }else{
                    //exist and no data changed, only check if need to reload image...
                    //redraw markers
                    
                    if ([marker.userData[@"c_zoom"]intValue] != iZoomDefaultLevel) {
                        [arrToRedraw addObject:marker];
                        
                    }
                    
                    
                }
                
            }
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            //add new
            if (arrFilter.count > 0) {
                NSLog(@"aff .... count : %lu",(unsigned long)arrFilter.count);
                [self performSelector:@selector(TimerTick:) withObject:arrFilter];
            }else{
                if (arrToRedraw.count > 0) {
                    [self performSelector:@selector(fnRedrawPointWithNewZoom:) withObject:arrToRedraw];
                }else{
                    [self hideIndicator];
                }
                
                //
            }
            
        });
        
        
    }];
}


/*In square 10m => at the point clicked...get list points*/
//
-(void)coordinateBoundsWithLocation:(CLLocationCoordinate2D)coordinate withDistance:(CLLocationDistance)distance
{
    MKCoordinateRegion region  =  MKCoordinateRegionMakeWithDistance(coordinate, distance, distance);
    CLLocationDegrees northEastLatitude = coordinate.latitude + region.span.latitudeDelta;
    CLLocationDegrees northEastLongitude = coordinate.longitude + region.span.longitudeDelta;
    
    CLLocationDegrees southWestLatitude = coordinate.latitude - region.span.latitudeDelta;
    CLLocationDegrees southWestLongitude = coordinate.longitude - region.span.longitudeDelta;
    
    m10MetersNE = CLLocationCoordinate2DMake(northEastLatitude, northEastLongitude);
    
    m10MetersSW = CLLocationCoordinate2DMake(southWestLatitude, southWestLongitude);
}

-(NSString*)stringFilterPublicationWithswLat:(double)swlat swlon:(double)swlon neLat:(double)neLat neLon:(double)neLon
{
    
    
    NSMutableString *mutStr = [NSMutableString new];
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    [mutStr setString:@"SELECT DISTINCT * FROM tb_carte "];
    [mutStr appendString: [NSString stringWithFormat:@" WHERE ((c_lat BETWEEN  %f AND %f) AND (c_lon BETWEEN %f AND %f)) ",swlat,neLat,swlon,neLon] ];
    [mutStr appendString:[ NSString stringWithFormat:@" AND (c_user_id=%@ OR c_user_id=%d)", sender_id, SPECIAL_USER_ID]];
    
    
    [mutStr appendString:[self stringFilterWithTableCarte:YES forShape: NO]];
    return mutStr;
}

-(NSString*)stringFilterWithTableCarte:(BOOL)isTableCarte forShape:(BOOL) bShape
{
    NSMutableString *mutStr = [NSMutableString new];

    //categories
    if (isEnableMasterFilter )
    {
        if (self.arrCategoriesFilter.count > 0) {
            NSString * combinedStuff = [self.arrCategoriesFilter componentsJoinedByString:@","];
            [mutStr appendString:[ NSString stringWithFormat:@" AND (c_category_id IN (%@)) ", combinedStuff]];
        }else{
            mutStr = [@"" mutableCopy];
        }
        
        return mutStr;
    }
    
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    if(bFilterON == NO)
    {
        //
        //***/
        
        //if is in a group
        switch (self.expectTarget) {
            case ISGROUP:
            {
                [mutStr appendString: [NSString stringWithFormat:@"AND ( "]];
                
                NSString*tmp = [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue];
                if (![tmp isEqualToString:@""]) {
                    [mutStr appendString: [NSString stringWithFormat:@"  c_groups LIKE \"%%[%@]%%\" ",tmp] ];
                }
                [mutStr appendString: [NSString stringWithFormat:@") "]];
                
            }
                break;
            case ISLOUNGE:
            {
                if (self.isLiveHunt) {
                    //do the same carte...show all points in live hunt...not just point in this group.
                }
                else
                {
                    [mutStr appendString: [NSString stringWithFormat:@"AND ( "]];
                    
                    NSString*tmp = [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue];
                    if (![tmp isEqualToString:@""]) {
                        [mutStr appendString: [NSString stringWithFormat:@" c_hunts LIKE \"%%[%@]%%\" ",tmp] ];
                    }
                    [mutStr appendString: [NSString stringWithFormat:@") "]];
                }
                
            }
            default:
                break;
        }
        
    }
    else
    {
        //c_search Only for Publication search
        
        if (!bShape && self.myC_Search_Text && ![self.myC_Search_Text isEqualToString:@""]) {
            [mutStr appendString: [NSString stringWithFormat:@" AND c_search LIKE \"%%%@%%\"  ",self.myC_Search_Text]];
        }
        
        //***/
        NSMutableString *mutSub = [NSMutableString new];
        
        //if is in a group
        switch (self.expectTarget) {
            case ISGROUP:
            {
                
                NSString*tmp = [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue];
                if (![tmp isEqualToString:@""]) {
                    [mutSub appendString: [NSString stringWithFormat:@" OR c_groups LIKE \"%%[%@]%%\" ",tmp] ];
                }
                
            }
                break;
            case ISLOUNGE:
            {
                //                if (self.isLiveHunt) {
                //                    //do the same carte
                //                }
                //                else
                //                {
                
                NSString*tmp = [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue];
                if (![tmp isEqualToString:@""]) {
                    [mutSub appendString: [NSString stringWithFormat:@"OR c_hunts LIKE \"%%[%@]%%\" ",tmp] ];
                }
                break;
                //                }
            }
            case ISCARTE:
            {
                
                if ([self.strFilterMoi isEqualToString:@"1"])
                {
                    [mutSub appendString: [NSString stringWithFormat:@"  OR (c_owner_id=%@) ",sender_id]];
                    
                    
                }
                
                if ([self.strFilterAmis isEqualToString:@"1"])
                {
                    //share user
                    [mutSub appendString: [NSString stringWithFormat:@" OR ( c_friend=1 AND c_owner_id !=%@ )",sender_id]];
                    
                }
                //group
                NSArray *arrGroup = [self.strMesGroupFilter componentsSeparatedByString:@","];
                for (NSString*mGroup in arrGroup) {
                    
                    NSString*tmp = [mGroup stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if (![tmp isEqualToString:@""]) {
                        [mutSub appendString: [NSString stringWithFormat:@" OR (c_groups LIKE \"%%[%@]%%\") ",tmp] ];
                    }
                    
                }
                //hunts
                NSArray *arrHunt = [self.strMesHuntFilter componentsSeparatedByString:@","];
                for (NSString*mHunt in arrHunt) {
                    
                    NSString*tmp = [mHunt stringByReplacingOccurrencesOfString:@" " withString:@""];
                    if (![tmp isEqualToString:@""]) {
                        [mutSub appendString: [NSString stringWithFormat:@" OR (c_hunts LIKE \"%%[%@]%%\") ",tmp] ];
                    }
                    
                }
                
                //personne
                if (self.strFilterPersonne.length > 0) {
                    [mutSub appendString:[ NSString stringWithFormat:@" OR (c_owner_id IN (%@)) ", self.strFilterPersonne]];
                }
                
            }
                break;
            default:
                break;
        }
        
        if (isTableCarte) {
            //categories
            if (self.arrCategoriesFilter.count > 0) {
                NSString * combinedStuff = [self.arrCategoriesFilter componentsJoinedByString:@","];
                [mutStr appendString:[ NSString stringWithFormat:@" AND (c_category_id IN (%@)) ", combinedStuff]];
            }
        }
        
        // AND
        if (mutSub.length > 0) {
            [mutStr appendString: [NSString stringWithFormat:@"AND ( "]];
            [mutStr appendString: @"  (0) "];
            [mutStr appendString: mutSub];
            [mutStr appendString: [NSString stringWithFormat:@") "]];
            
        }
        //Debut -> Fin
        if (self.strFilterFin && self.strFilterDebut) {
            [mutStr appendString: [NSString stringWithFormat:@" AND (c_created BETWEEN %@ AND %@) ",self.strFilterDebut,self.strFilterFin]];
        }
    }
    
    
    //
    ASLog(@"%@", mutStr);
    return mutStr;
}
-(void)TimerTickAgenda:(NSMutableArray *)arrIn
{
    //printing...
    if (vTmpMap.mapView_.camera.zoom < MINZOOM){
        return ;
    }
    
    if (arrIn.count> 0) {
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        NSDictionary *dic =mutArr[0];
        //add marker
        
        [self GMSaddMakeAgenda:dic];
        
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(TimerTickAgenda:) withObject:mutArr afterDelay:0.01];
        
        
    }else{
        //draw resize icon if need...
        if (arrToRedrawAgenda.count > 0) {
            [self performSelector:@selector(fnRedrawAgendaWithNewZoom:) withObject:arrToRedrawAgenda];
        }else{
        }
    }
}

-(void)TimerTick:(NSMutableArray *)arrIn
{
    //printing...
    if (vTmpMap.mapView_.camera.zoom < MINZOOM){
        [self hideIndicator];
        return ;
    }
    
    if (arrIn.count> 0) {
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        NSDictionary *dic =mutArr[0];
        //add marker
        
        [self GMSaddMakeMap:dic];
        
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(TimerTick:) withObject:mutArr afterDelay:0.01];
        
        
    }else{
        //draw resize icon if need...
        if (arrToRedraw.count > 0) {
            [self performSelector:@selector(fnRedrawPointWithNewZoom:) withObject:arrToRedraw];
        }else{
            [self hideIndicator];
        }
    }
}
//MARK: DISTRIBUTION

-(void ) doDistribution
{
    NSMutableString *mutStrDistributor = [NSMutableString new];
    
    [mutStrDistributor setString:@"SELECT DISTINCT * FROM tb_distributor "];
    
    [mutStrDistributor appendString: [NSString stringWithFormat:@" WHERE ((c_lat BETWEEN  %f AND %f) AND (c_lon BETWEEN %f AND %f)) ",southWestFilter.latitude,northEastFilter.latitude,southWestFilter.longitude,northEastFilter.longitude] ];
    [self doGetDataWDistributorWithStr: mutStrDistributor];
}

-(void) doGetDataWDistributorWithStr:(NSString*)mutStr
{
    
    [arrToRedrawDistributor removeAllObjects];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        ASLog(@"distribution-------");
        NSMutableArray *arrFilter = [NSMutableArray new];
        
        //get data voa filter result
        FMResultSet *set_querry = [db  executeQuery:mutStr];
        
        while ([set_querry next])
        {
            if([set_querry doubleForColumn:@"c_lat"] == 0 && [set_querry doubleForColumn:@"c_lon"] == 0)
            {
                continue;
            }
            
            if (self.dicMarkerDistribution[[set_querry stringForColumn:@"c_id"]] == nil) {
                [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                        @"c_name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                        @"c_address":[set_querry stringForColumn:@"c_address"],
                                        @"c_cp":[set_querry stringForColumn:@"c_cp"],
                                        @"c_city":[set_querry stringForColumn:@"c_city"],
                                        @"c_tel":[set_querry stringForColumn:@"c_tel"],
                                        @"c_email":[set_querry stringForColumn:@"c_email"],
                                        @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                        @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                        @"c_brands":[set_querry stringForColumn:@"c_brands"],
                                        @"c_updated":[set_querry stringForColumn:@"c_updated"],
                                        @"c_logo":[set_querry stringForColumn:@"c_logo"],
                                        @"c_marker":@(MARKER_DISTRIBUTRION),
                                        }];
            }
            else
            {
                GMSMarker *marker = self.dicMarkerDistribution[[set_querry stringForColumn:@"c_id"]];
                if ([set_querry longForColumn :@"c_updated"] != [marker.userData[@"c_updated"] floatValue])
                {
                    marker.map = nil;
                    [self.dicMarkerDistribution removeObjectForKey:[set_querry stringForColumn:@"c_id"]];
                    
                    [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                            @"c_name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                            @"c_address":[set_querry stringForColumn:@"c_address"],
                                            @"c_cp":[set_querry stringForColumn:@"c_cp"],
                                            @"c_city":[set_querry stringForColumn:@"c_city"],
                                            @"c_tel":[set_querry stringForColumn:@"c_tel"],
                                            @"c_email":[set_querry stringForColumn:@"c_email"],
                                            @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                            @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                            @"c_brands":[set_querry stringForColumn:@"c_brands"],
                                            @"c_updated":[set_querry stringForColumn:@"c_updated"],
                                            @"c_logo":[set_querry stringForColumn:@"c_logo"],
                                            @"c_marker":@(MARKER_DISTRIBUTRION),
                                            }];
                }
                else
                {
                    if ([marker.userData[@"c_zoom"]intValue] != iZoomDefaultLevel) {
                        [arrToRedrawDistributor addObject:marker];
                        
                    }
                    
                }
            }
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            //add new
            if (arrFilter.count > 0) {
                [self performSelector:@selector(TimerDistribution:) withObject:arrFilter];
            }else{
                if (arrToRedrawDistributor.count > 0) {
                    [self performSelector:@selector(fnRedrawPointWithNewZoomDistribution:) withObject:arrToRedrawDistributor];
                }
                else
                {
                    [self hideIndicator];
                }
            }
            
        });
        
        
    }];
    
}

-(void)TimerDistribution:(NSMutableArray *)arrIn
{
    if (vTmpMap.mapView_.camera.zoom < MINZOOM){
        
        return ;
    }
    //Overt limited number
    if (arrIn.count> 0) {
        
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        NSDictionary *dic =mutArr[0];
        
        //add marker distribution
        [self GMSaddMarkerDistributionMap:dic];
        
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(TimerDistribution:) withObject:mutArr afterDelay:0.01];
        
    }else{
        [self hideIndicator];
        
    }
}
-(void) fnRedrawPointWithNewZoomDistribution :(NSArray*)arrIn{
    
    if (vTmpMap.mapView_.camera.zoom < MINZOOM){
        
        return ;
    }
    
    if (arrIn.count> 0) {
        
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        GMSMarker *marker = (GMSMarker*)mutArr[0];
        
        __block NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:marker.userData];
        [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
        marker.userData = dicMul;
        ///
        if (dicMul[@"image"]) {
            UIImage *image =  (UIImage*)dicMul[@"image"];
            marker.icon = [self resizeImageDistribution:image withLevelZoom:iZoomDefaultLevel];
            //add arr
            [self.dicMarkerDistribution setObject:marker forKey:marker.userData[@"c_id"]];
        }
        else
        {
            MDMarkersDistribution *tmpImage= [[MDMarkersDistribution alloc] initWithDictionary:marker.userData];
            [tmpImage setCallBackMarkers:^(UIImage *image)
             {
                 [dicMul setObject:image forKey:@"image"];
                 marker.userData = dicMul;
                 
                 marker.icon = [self resizeImageDistribution:image withLevelZoom:iZoomDefaultLevel];
                 marker.map = vTmpMap.mapView_;
                 //add arr
                 [self.dicMarkerDistribution setObject:marker forKey:marker.userData[@"c_id"]];
             }];
            [tmpImage doSetCalloutView];
        }
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(fnRedrawPointWithNewZoomDistribution:) withObject:mutArr afterDelay:0.01];
    }
    else
    {
        [self hideIndicator];
    }
    
}
-(void)ShapesTimerTick:(NSMutableArray *)arrIn
{
    if (arrIn.count> 0) {
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        NSDictionary *dic =mutArr[0];
        [self loadOptionShapes:@[dic]];
        [mutArr removeObjectAtIndex:0];
        [self performSelector:@selector(ShapesTimerTick:) withObject:mutArr afterDelay:0.04];
        
    }else{
        //no more shape...->
        [self hideIndicator];
    }
}

//MARK:- search location pop up view
-(IBAction)fnLocationSearch:(id)sender
{
    [self hideBlurBackGround];
    [self showAlertSearch];
}


-(IBAction)fnRDV:(id)sender
{
    NSDictionary* myDic = [LiveHuntOBJ sharedInstance].dicLiveHunt;
    
    id dic = myDic[@"meeting"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        
        NSDictionary *local = (NSDictionary*)dic[@"address"];
        
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [local[@"latitude"] doubleValue];
        simpleCoord.longitude= [local[@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: vTmpMap.mapView_.camera.zoom];
        
    }
}

- (NSString *) platform{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

- (NSInteger) ppiForDevice {
    NSString *platform = [self platform];
    if ([platform isEqualToString:@"iPhone1,1"])    return 163;
    if ([platform isEqualToString:@"iPhone1,2"])    return 163;
    if ([platform isEqualToString:@"iPhone2,1"])    return 163;
    if ([platform isEqualToString:@"iPhone3,1"])    return 326;
    if ([platform isEqualToString:@"iPhone3,3"])    return 326;
    if ([platform isEqualToString:@"iPhone4,1"])    return 326;
    
    if ([platform isEqualToString:@"iPhone5,1"])    return 326;
    if ([platform isEqualToString:@"iPhone5,2"])    return 326;
    if ([platform isEqualToString:@"iPhone5,3"])    return 326;
    if ([platform isEqualToString:@"iPhone5,4"])    return 326;
    if ([platform isEqualToString:@"iPhone6,1"])    return 326;
    if ([platform isEqualToString:@"iPhone6,2"])    return 326;
    
    if ([platform isEqualToString:@"iPod1,1"])      return 163;
    if ([platform isEqualToString:@"iPod2,1"])      return 163;
    if ([platform isEqualToString:@"iPod3,1"])      return 163;
    if ([platform isEqualToString:@"iPod4,1"])      return 326;
    if ([platform isEqualToString:@"iPad1,1"])      return 132;
    if ([platform isEqualToString:@"iPad2,1"])      return 132;
    if ([platform isEqualToString:@"iPad2,2"])      return 132;
    if ([platform isEqualToString:@"iPad2,3"])      return 132;
    
    if ([platform isEqualToString:@"iPad3,1"])      return 264;
    if ([platform isEqualToString:@"iPad3,4"])      return 326;
    if ([platform isEqualToString:@"iPad2,5"])      return 163;
    if ([platform isEqualToString:@"iPad4,1"])      return 326;
    if ([platform isEqualToString:@"iPad4,2"])      return 326;
    if ([platform isEqualToString:@"iPad4,4"])      return 326;
    if ([platform isEqualToString:@"iPad4,5"])      return 326;
    
    if ([platform isEqualToString:@"i386"])         return 163;
    if ([platform isEqualToString:@"x86_64"])       return 163;
    
    return 326;
}

- (CGSize) pixelForPage {
    
    NSInteger ppi = [self ppiForDevice];
    
    CGSize sizePage;
    
    sizePage = CGSizeMake((A4_W/25.4) * ppi, (A4_H/25.4) * ppi);
    
    return sizePage;
}

- (CGSize)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width : (float) i_height
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    if (newHeight > i_height) {
        scaleFactor = i_height / newHeight;
        newWidth = newWidth * scaleFactor;
        newHeight = newHeight * scaleFactor;
    }
    return CGSizeMake(newWidth, newHeight);
}

-(void) captureScreenOnMainThread
{
    //get image
    UIGraphicsBeginImageContextWithOptions(vTmpMap.mapView_.bounds.size, 1, 0.0);
    
    [vTmpMap.mapView_ drawViewHierarchyInRect:vTmpMap.mapView_.bounds afterScreenUpdates:NO];
    UIImage *image_logo = [UIImage imageNamed:strLogo_print];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    //    [vTmpMap.mapView_.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIGraphicsEndImageContext();
    
    
    //to pdf
    NSMutableData *pdfData = [NSMutableData data];
    CGSize sizePage = [self pixelForPage];
    CGSize newSize = [self imageWithImage:image scaledToWidth:sizePage.width-20:sizePage.height-80];
    //draw logo
    CGRect rectImage_Logo = CGRectMake((sizePage.width - newSize.width)/2+ newSize.width -(1143+10), (sizePage.height - newSize.height)/2+newSize.height-(300+10), 1143, 300);
    
    
    CGRect rectImage = CGRectMake((sizePage.width - newSize.width)/2, (sizePage.height - newSize.height)/2, newSize.width, newSize.height);
    
    UIGraphicsBeginPDFContextToData(pdfData, CGRectZero, nil);
    
    UIGraphicsBeginPDFPageWithInfo(CGRectMake(0, 0, sizePage.width, sizePage.height), nil);
    
    CGContextRef pdfContext = UIGraphicsGetCurrentContext();
    
    CGContextTranslateCTM(pdfContext, 0, sizePage.height);
    CGContextScaleCTM(pdfContext, 1.0, -1.0);
    
    CGContextDrawImage(pdfContext, rectImage, image.CGImage);
    
    CGContextDrawImage(pdfContext, rectImage_Logo, image_logo.CGImage);
    UIGraphicsEndPDFContext();
    
    /*
     NSArray* documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES);
     
     NSString* documentDirectory = [documentDirectories objectAtIndex:0];
     NSString* documentDirectoryFilename = [documentDirectory stringByAppendingPathComponent:@"test.pdf"];
     
     [pdfData writeToFile:documentDirectoryFilename atomically:YES];
     */
    
    
    //to printer
    UIPrintInteractionController* pic = [UIPrintInteractionController sharedPrintController];
    
    if (pic && [UIPrintInteractionController canPrintData:pdfData])
    {
        pic.delegate = self;
        UIPrintInfo* printInfo = [UIPrintInfo printInfo];
        printInfo.outputType = UIPrintInfoOutputPhoto;
        printInfo.jobName = @"PrintingImage";
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        pic.printInfo = printInfo;
        pic.showsPageRange = YES;
        pic.printingItem = pdfData;
        
        void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *printController, BOOL completed, NSError *error) {
            
            isPrinting = NO;
            self.mapScaleView.position =kLXMapScalePositionTopLeft;
            [self resizeAllAnnotaions];
            [self refreshCircle];
            
            //draw distributor again...
            [self hideShowDistribution:isPrinting];
            //            [self refreshDistributor];
            
            
            
            if (!completed && error) {
                
            }
        };
        
        [pic presentAnimated:YES completionHandler:completionHandler];
    }
    
}

-(void) refreshDistributor
{
    //    DISTRIBUTION
    
    NSMutableString *mutStrDistributor = [NSMutableString new];
    
    [mutStrDistributor setString:@"SELECT DISTINCT * FROM tb_distributor "];
    
    [mutStrDistributor appendString: [NSString stringWithFormat:@" WHERE ((c_lat BETWEEN  %f AND %f) AND (c_lon BETWEEN %f AND %f)) ",southWestFilter.latitude,northEastFilter.latitude,southWestFilter.longitude,northEastFilter.longitude] ];
    
    [self doGetDataWDistributorWithStr: mutStrDistributor];
}

-(void)resizeAllAnnotaions
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:visibleRegion];
    
    
    //POINTS
    NSArray*keys=[self.myDIC_MarkerPublication allKeys];
    for (id c_id in keys) {
        GMSMarker *marker = self.myDIC_MarkerPublication[c_id];
        if ([bounds containsCoordinate:marker.position]) {
            NSDictionary *dic = marker.userData;
            //clear
            marker.map =nil;
            //new
            CLLocationCoordinate2D position;
            position.latitude = [dic[@"c_lat"] doubleValue];
            position.longitude= [dic[@"c_lon"] doubleValue];
            
            marker.position = position;
            marker.userData = dic;
            marker.infoWindowAnchor = CGPointMake(0.5, 0.25);
            
            NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
            [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
            if (dicMul[@"image"]) {
                NSString *strLegend = dicMul[@"c_legend"];
                if (iZoomDefaultLevel < kSizeMarkerZoom && strLegend.length >0) {
                    marker.groundAnchor = CGPointMake(0.5, 1.0);
                    UIImage *image =  (UIImage*)dicMul[@"image_nonLegend"];
                    marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:isPrinting withC_Marker:MARKER_PUBLICATION];
                    
                }
                else
                {
                    float percent_arrow = [dicMul[@"percent_arrow"] floatValue];
                    marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
                    UIImage *image =  (UIImage*)dicMul[@"image"];
                    marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:isPrinting withC_Marker:MARKER_PUBLICATION];
                    
                }
                marker.map = vTmpMap.mapView_;
                
            }
            else
            {
                MDMarkersCustom *tmpImage= [[MDMarkersCustom alloc] initWithDictionary:dicMul];
                [tmpImage setCallBackMarkers:^(UIImage *image,UIImage *image_nonLegend, float percent_arrow)
                 {
                     [dicMul setObject:image forKey:@"image"];
                     [dicMul setObject:@(percent_arrow) forKey:@"percent_arrow"];
                     NSString *strLegend = dicMul[@"c_legend"];
                     if (strLegend.length >0) {
                         [dicMul setObject:image_nonLegend forKey:@"image_nonLegend"];
                         [dicMul setObject:@(0.5) forKey:@"percent_arrow_nonLegend"];
                     }
                     marker.userData = dicMul;
                     
                     if (iZoomDefaultLevel < kSizeMarkerZoom && strLegend.length>0) {
                         marker.groundAnchor = CGPointMake(0.5, 1.0);
                         marker.icon = [self resizeImage:image_nonLegend withLevelZoom:iZoomDefaultLevel withResize:isPrinting withC_Marker:MARKER_PUBLICATION];
                         
                     }
                     else
                     {
                         marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
                         marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:isPrinting withC_Marker:MARKER_PUBLICATION];
                     }
                     marker.map = vTmpMap.mapView_;
                 }];
                [tmpImage doSetCalloutView];
            }
        }
    }
    
    //AGENDA
    NSArray*keysX=[self.myDIC_MarkerAgenda allKeys];
    for (id c_id in keysX) {
        GMSMarker *marker = self.myDIC_MarkerAgenda[c_id];
        if ([bounds containsCoordinate:marker.position]) {
            NSDictionary *dic = marker.userData;
            //clear
            marker.map =nil;
            //new
            CLLocationCoordinate2D position;
            position.latitude = [dic[@"c_meeting_lat"] doubleValue];
            position.longitude= [dic[@"c_meeting_lon"] doubleValue];
            
            marker.position = position;
            marker.userData = dic;
            marker.infoWindowAnchor = CGPointMake(0.5, 0.25);
            
            NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
            [dicMul setValue:@(iZoomDefaultLevel) forKey:@"c_zoom"];
            if (dicMul[@"image"]) {
                float percent_arrow = [dicMul[@"percent_arrow"] floatValue];
                marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
                UIImage *image =  (UIImage*)dicMul[@"image"];
                marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:isPrinting withC_Marker:MARKER_PUBLICATION];
                marker.map = vTmpMap.mapView_;
                
            }
        }
    }
    
    
}
-(void)hideShowDistribution:(BOOL)isPrint
{
    if (isPrint) {
        //remove distribution
        NSArray*keys=[self.dicMarkerDistribution allKeys];
        for (id c_id in keys) {
            GMSMarker *marker = self.dicMarkerDistribution[c_id];
            marker.map = nil;
        }
    }
    else
    {
        //remove distribution
        NSArray*keys=[self.dicMarkerDistribution allKeys];
        for (id c_id in keys) {
            GMSMarker *marker = self.dicMarkerDistribution[c_id];
            marker.map = vTmpMap.mapView_;
        }
        
    }
}
-(IBAction)fnPrint:(id)sender
{
    [self cancelCurrentTasks];
    
    self.mapScaleView.position =kLXMapScalePositionBottomRight;
    isPrinting = YES;
    
    [self resizeAllAnnotaions];
    [self removeCircle];
    
    //remove distribution
    [self hideShowDistribution:isPrinting];
    
    [self performSelector:@selector(showFormPrint) withObject:nil afterDelay:0.75];
}

-(IBAction) showFormPrint
{
    [self performSelectorOnMainThread:@selector(captureScreenOnMainThread) withObject:nil waitUntilDone: YES];
    
}

- (UIPrintPaper *)printInteractionController:(UIPrintInteractionController *)printInteractionController choosePaper:(NSArray<UIPrintPaper *> *)paperList
{
    CGSize pageSize = CGSizeMake(612, 792);
    return [UIPrintPaper bestPaperForPageSize:pageSize withPapersFromArray:paperList];
}
//MARK: blurbackground
-(void)settingBlurBackGround
{
    if (self.blurSearch == nil) {
        return;
    }
    
    [self.view sendSubviewToBack:self.blurSearch];
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:self.blurSearch attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContainer
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0 constant:44] ];
    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:self.blurSearch attribute:NSLayoutAttributeLeading
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContainer
                               attribute:NSLayoutAttributeLeading
                               multiplier:1.0 constant:0] ];
    
    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:self.blurSearch attribute:NSLayoutAttributeTrailing
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContainer
                               attribute:NSLayoutAttributeTrailing
                               multiplier:1.0 constant:0] ];
    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:self.blurSearch attribute:NSLayoutAttributeBottom
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContainer
                               attribute:NSLayoutAttributeBottom
                               multiplier:1.0 constant:0] ];
    
}
-(void)hideBlurBackGround
{
    [self.view sendSubviewToBack:self.blurSearch];
    [self.toussearchBar resignFirstResponder];
    
}
-(void)showBlurBackGround
{
    [self.view insertSubview:self.blurSearch belowSubview:self.toussearchBar];
    
}

//MARK: view filter
-(void) addSubViewFilter
{
    __weak typeof(self) wself = self;
    
    __weak typeof(BaseMapUI*) mMap = vTmpMap;
    
    self.viewFilter = [[CarterFilterView alloc] initWithEVC:self expectTarget:self.expectTarget isLiveHunt: self.isLiveHunt];
    [self.viewFilter addContraintSupview:self.view];
    [self.viewFilter setCallback:^(NSString *strSearch)
     {
         
         //reset flag MasterFilter
         isEnableMasterFilter = NO;
         
         //unselect all Master filter -(Reset)
         [masterFilterView fnResetAll];
         
         
         //         wself.myC_Search_Text = strSearch;
         [wself getFilter];
         [wself fnSetSwitchFilter];

         [wself doMapType:iMapType];
         
         [wself fnCategory];
         
         [wself forceRefresh];
         dispatch_async(dispatch_get_main_queue(), ^{
             [self performSelector:@selector(refreshCircle) withObject:nil afterDelay:0.75];
         });
     }];
    [self.viewFilter setup];
}
//MARK: short cut
-(void) addShortCutSetting
{
    vTmpMap.btnShortCutSetting.hidden = NO;
    __weak typeof(self) wself = self;
    self.viewShortCutSetting = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:self.expectTarget];
    
    self.viewShortCutSetting.pointBtnShortcut = vTmpMap.btnShortCutSetting.center;
    
    [self.viewShortCutSetting setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         vTmpMap.btnShortCutSetting.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_VENT:
             {
                 [wself CarteVentAction:nil];
             }
                 break;
             case SHORTCUT_ACTION_METEO:
             {
                 [wself CarteMeteoAction:nil];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 [wself fnAdressFavorite:nil];
             }
                 break;
             case SHORTCUT_ACTION_MAPTYPE:
             {
                 [wself CarteTypeAction:nil];
             }
                 break;
             case SHORTCUT_ACTION_PRINT:
             {
                 [wself fnPrint:nil];
             }
                 break;
             case SHORTCUT_ACTION_SEARCH:
             {
                 [wself fnLocationSearch:nil];
             }
                 break;
             case SHORTCUT_ACTION_RADAR:
             {
                 [wself showRadarAction:nil];
             }
                 break;
             case SHORTCUT_ACTION_DRAWSHAPE:
             {
                 [wself enableDrawShape];
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCutSetting.constraintBottomCloseButton.constant = 75;
    self.viewShortCutSetting.constraintTraillingCloseButton.constant = 2;
    [self.viewShortCutSetting addContraintSupview:self.view];
    self.viewShortCutSetting.hidden = YES;
    
}
-(IBAction)fnShortCutSetting:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        //        CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI/2);
        //        vTmpMap.btnShortCut.transform = transform;
        vTmpMap.btnShortCutSetting.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        vTmpMap.btnShortCutSetting.hidden = YES;
        [self.viewShortCutSetting hide:NO];
        [self returnRotationSetting];
    }];
}

-(void) returnRotationSetting{
    [UIView animateWithDuration:0.1 animations:^{
        vTmpMap.btnShortCutSetting.transform = CGAffineTransformIdentity;
    }];
}
//MARK: short cut
-(void) addShortCut
{
    __weak typeof(self) wself = self;
    vTmpMap.btnShortCut.hidden = NO;
    self.viewShortCut = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:ISMUR];
    [self.viewShortCut setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         vTmpMap.btnShortCut.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_ADD_FIREND:
             {
                 AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_AGENDA:
             {
                 [[ChassesCreateOBJ sharedInstance] resetParams];
                 ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_GROUP:
             {
                 [[GroupCreateOBJ sharedInstance] resetParams];
                 GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:YES];
                 
             }
                 break;
             case SHORTCUT_ACTION_DISCUSTION:
             {
                 ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISDISCUSS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
                 viewController1.isFromSetting = YES;
                 
                 float lat = vTmpMap.mapView_.camera.target.latitude;
                 float lng = vTmpMap.mapView_.camera.target.longitude;
                 
                 viewController1.dicCenterMap = @{@"lat": @(lat),@"lon":@(lng)};
                 [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_PUBLICATION:
             {
                 //[IOS] keep old design for new publication from general map
                 
                 if(self.expectTarget == ISLOUNGE && self.isLiveHunt)
                 {
                     [wself fnAddPublicationSignal: nil];
                 }else{
                     [wself fnAddPublication:nil];
                 }
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCut.constraintBottomCloseButton.constant = 5;
    self.viewShortCut.constraintTraillingCloseButton.constant = 2;
    [self.viewShortCut addContraintSupview:self.view];
    self.viewShortCut.hidden = YES;
    if (self.expectTarget == ISLOUNGE || self.expectTarget == ISGROUP) {
        [self.viewShortCut fnAllowAdd:allow_add];
    }
    else
    {
        [self.viewShortCut fnAllowAdd:YES];
    }
}
-(IBAction)fnShortCut:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        vTmpMap.btnShortCut.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        vTmpMap.btnShortCut.hidden = YES;
        [self.viewShortCut hide:NO];
        [self returnRotation];
    }];
}

-(void) returnRotation{
    [UIView animateWithDuration:0.1 animations:^{
        vTmpMap.btnShortCut.transform = CGAffineTransformIdentity;
    }];
}

-(IBAction) fnShowFilterView
{
    [self.viewFilter fnGetDataWithStringSearch:self.myC_Search_Text];
    [self.viewFilter hide:NO];
    
}
//MARK: - SHOW ALL POINT TOGETHER
-(void)showAllPointTogetherPositionWithDidTapMarker:(GMSMarker *)marker
{
    NSMutableString *mutStr = [NSMutableString new];
    CLLocationCoordinate2D position;
    switch ([marker.userData[@"c_marker"] intValue]) {
        case  MARKER_AGENDA:
        {
            position.latitude = [marker.userData[@"c_meeting_lat"] doubleValue];
            position.longitude= [marker.userData[@"c_meeting_lon"] doubleValue];
            
        }
            break;
        case MARKER_PUBLICATION:
        {
            position.latitude = [marker.userData[@"c_lat"] doubleValue];
            position.longitude= [marker.userData[@"c_lon"] doubleValue];
        }
    }
    
    //get NE/SW around 5 meters
    [self coordinateBoundsWithLocation:position withDistance:5];
    
    //PUBLICATION
    [mutStr appendString:[self stringFilterPublicationWithswLat:m10MetersSW.latitude swlon:m10MetersSW.longitude neLat:m10MetersNE.latitude neLon:m10MetersNE.longitude]];
    NSMutableArray *arrFilter = [NSMutableArray new];
    //AGENDA
    NSString *strQueryAgenda = [self stringFilterAgendaWithswLat:m10MetersSW.latitude swlon:m10MetersSW.longitude neLat:m10MetersNE.latitude neLon:m10MetersNE.longitude];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //QUERY AGENDA
        FMResultSet *set_querry_Agenda = [db  executeQuery:strQueryAgenda];
        while ([set_querry_Agenda next])
        {
            [arrFilter addObject:@{ @"c_id":[set_querry_Agenda stringForColumn:@"c_id"],
                                    @"c_name":[set_querry_Agenda stringForColumn:@"c_name"],
                                    
                                    @"c_subscribers": [NSNumber numberWithInt: [set_querry_Agenda intForColumn:@"c_subscribers"]],
                                    @"c_photo":[set_querry_Agenda stringForColumn:@"c_photo"],
                                    
                                    
                                    @"c_access": [NSNumber numberWithInt: [set_querry_Agenda intForColumn:@"c_access"]],
                                    @"c_description":[set_querry_Agenda stringForColumn:@"c_description"],
                                    @"c_nb_participant": [NSNumber numberWithInt: [set_querry_Agenda intForColumn:@"c_nb_participant"]],
                                    @"c_start_date": [set_querry_Agenda stringForColumn:@"c_start_date"],
                                    @"c_end_date": [set_querry_Agenda stringForColumn:@"c_end_date"],
                                    @"c_meeting_address": [set_querry_Agenda stringForColumn:@"c_meeting_address"],
                                    @"c_meeting_lat": [set_querry_Agenda stringForColumn:@"c_meeting_lat"],
                                    @"c_meeting_lon": [set_querry_Agenda stringForColumn:@"c_meeting_lon"],
                                    @"c_updated":  [NSNumber numberWithLong:  [set_querry_Agenda longForColumn :@"c_updated"]],
                                    @"c_marker":@(MARKER_AGENDA),
                                    }];
        }
        
        //QUERY PUBLICATION
        FMResultSet *set_querry = [db  executeQuery:mutStr];
        while ([set_querry next])
        {
            [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                    @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                    @"c_owner_name":[set_querry stringForColumn:@"c_owner_name"],
                                    @"c_legend":[set_querry stringForColumn:@"c_legend"],
                                    @"c_marker_picto":[set_querry stringForColumn:@"c_marker_picto"],
                                    @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                    @"c_created":[NSNumber numberWithLong:  [set_querry longForColumn :@"c_created"]],
                                    @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                    @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                    @"c_marker":@(MARKER_PUBLICATION),
                                    @"c_updated":[NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                    }];
        }
        
        
        NSLog(@"COUNT LIST %lu",(unsigned long)arrFilter.count);
        dispatch_async( dispatch_get_main_queue(), ^{
            
            if (arrFilter.count > 1) {
                //show list
                AlertListMarker *vcListMarker = [[AlertListMarker alloc] init];
                
                [vcListMarker doBlock:^(NSDictionary *userData) {
                    CLLocationCoordinate2D position;
                    position.latitude = [userData[@"c_meeting_lat"] doubleValue];
                    position.longitude= [userData[@"c_meeting_lon"] doubleValue];
                    GMSMarker *marker = [GMSMarker markerWithPosition:position];
                    marker.userData = userData;
                    switch ([marker.userData[@"c_marker"] intValue]) {
                        case  MARKER_AGENDA:
                        {
                            [self gotoAgendaDidTapMarker:marker];
                            
                        }
                            break;
                        case MARKER_PUBLICATION:
                        {
                            [self gotoPublicationDidTapMarker:marker];
                        }
                    }
                    
                }];
                vcListMarker.arrMarker = [arrFilter mutableCopy];
                [vcListMarker showAlert];
                
            }
            else
            {
                switch ([marker.userData[@"c_marker"] intValue]) {
                    case  MARKER_AGENDA:
                    {
                        [self gotoAgendaDidTapMarker:marker];
                        
                    }
                        break;
                    case MARKER_PUBLICATION:
                    {
                        [self gotoPublicationDidTapMarker:marker];
                    }
                }
            }
        });
    }];
}

-(void) recursiveFromTree:(NSDictionary*)dicTree
{
//    ASLog(@"%@", dicTree[@"id"]);
    
    [arrCategoryIDs addObject:dicTree[@"id"] ];
    
    NSArray*arrChildren = dicTree[@"children"];
    
    if ([arrChildren isKindOfClass: [NSArray class]]) {
        //is array -> has child
        if (arrChildren.count > 0) {
            
            for (NSDictionary*dic in arrChildren)
            {
                [self recursiveFromTree:dic];
            }
        }
    }
}

-(void) fnKilall_RefreshOne_withCategory :(BOOL) isReLoadData
{
//    self.arrCategoriesFilter
    [self cancelCurrentTasks];
    
    //clear the map
    [self removeAllDistribution];
    [self removeAllPublication];
    [self removeAllAgenda];
 
    [self.dicOverlays removeAllObjects];
    
    [vTmpMap.mapView_ clear];
    
    if (isReLoadData) {
        [self forceRefresh];
    }
}

//ggtt
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue destinationViewController] isKindOfClass: [ ListItemsMenuCarte class]]) {
        //callback...
        masterFilterView =  (ListItemsMenuCarte*) [segue destinationViewController];
        
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        NSArray *arrTree = treeDic[@"default"];

        masterFilterView.callbackMasterFilter = ^(MASTER_FILTER_TYPE iType) {

//            CLEAR_ALL
            isEnableMasterFilter = YES;

            [arrCategoryIDs removeAllObjects];

            //clear MAP markers...
            iMasterFilterType = iType;
            
            switch ( iType) {
                case TOUT:
                {}
                    break;
                    
                case ARMURERIES:
                {
                    //distributor only
                    [self doDistribution];
                }
                    break;
                case VETERINAIRES:
                {
                    //??? db

                }
                    break;

                case EVENEMENTS:
                {
                    //show Hunts only.
                    [self doShowAgendas];
                }
                    break;
                    //all kinds of publications
                case ANIMAUX:
                {
                    //filter Animaux only
                    for (NSDictionary*dic in arrTree) {
                        if ([dic[@"id"] intValue] == 104) {
                            //Animaux
                            [self recursiveFromTree:dic];
                            break;
                        }
                    }
                }
                    break;
                case EQUIPEMENTS:
                {
                    //Equipments only
                    for (NSDictionary*dic in arrTree) {
                        if ([dic[@"id"] intValue] == 112) {
                            //Equipements/Aménagements
                            [self recursiveFromTree:dic];
                            break;
                        }
                    }

                }
                    break;

                case HABITATS:
                {
                    //Habitats only
                    for (NSDictionary*dic in arrTree) {
                        if ([dic[@"id"] intValue] == 603) {
                            //Habitats
                            [self recursiveFromTree:dic];
                            break;
                        }
                    }
                }
                    break;
                default:
                    break;
            }
            //get string concat
            self.arrCategoriesFilter = [arrCategoryIDs copy];
            
            [self fnKilall_RefreshOne_withCategory : !(iMasterFilterType == CLEAR_ALL)];
//            //test
//            if (self.arrCategoriesFilter.count > 0) {
//                NSString * combinedStuff = [self.arrCategoriesFilter componentsJoinedByString:@","];
//                ASLog(@">>> %@", combinedStuff);
//            }
            
        };
    }else if ([[segue destinationViewController] isKindOfClass: [ ChatLiveHuntView class]]) {
        
        //callback...
        vcLiveHunt =  (ChatLiveHuntView*) [segue destinationViewController];
        vcLiveHunt.callbackLiveChatview = ^(NSDictionary *dic) {
            
            if ([dic[@"type"] isEqualToString:@"hide"]) {
                ASLog(@">>>>>>> AAAAAAA >>>>>>>>>>> ");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self fnCloseVPop:nil];
                    
                });
                return;
            }
            
            ASLog(@"%@", dic);
            
            //auto show if has new message/pub
            constraintLEFT.constant = 0;
            [UIView animateWithDuration:0.5
                             animations:^{
                                 
                                 [self.vContainer layoutIfNeeded];
                             }
                             completion:^(BOOL finished) {
                                 if (constraintLEFT.constant == 0) {
                                     btnShow.hidden = YES;
                                     btnHide.hidden = NO;
                                 }
                             }];
            
            
            
            
            dicSelectedLeftItem = dic;
            NSString *strContent = dic[@"data"][@"content"];
            NSString *strName = [dic[@"data"][@"owner"][@"fullname"] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            callName.text = [strName emo_emojiString];
            callText.text = [strContent emo_emojiString];
            
            //just take the hour         created = "2018-10-16T01:47:51+02:00";
            
            NSDateFormatter *inputFormatter =[[NSDateFormatter alloc] init];
            [inputFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
            
            NSDate * inputDate = [ inputFormatter dateFromString: dic[@"data"][@"created"] ];
            
            
            NSDateFormatter *outputFormat = [[NSDateFormatter alloc]init];
            outputFormat.dateFormat = @"HH:mm:ss";
            NSString *dateString = [outputFormat stringFromDate: inputDate];
            
            callTime.text = dateString;
            
            //
            topConstraintCallout.constant = [dic[@"pos"] floatValue];
            
            if ([dic[@"selected"] boolValue]) {
                //show callout
                vCallOut.alpha = 1;
                vArrow.alpha = 1;
                btnCloseCallout.hidden = NO;
            }else{
                vCallOut.alpha = 0;
                vArrow.alpha = 0;
                [vcLiveHunt unSelectedItem];
                btnCloseCallout.hidden = YES;
                
            }
            
            heightConstraintCallout.constant = 81;
            callObs.text = @"";
            imgObsCallObs.hidden = TRUE;
            if(dic[@"data"][@"observations"])
            {
                //observation
                NSArray *arrObservation = dic[@"data"][@"observations"];
                
                if (arrObservation.count > 0)
                {
                    NSDictionary*dicObservation = arrObservation[0];
                    
                    NSArray*arrTree = dicObservation[@"tree"];
                    
                    NSMutableString *mutString = [NSMutableString new];
                    
                    for (int i = 0;  i< arrTree.count; i++) {
                        [mutString appendString:[arrTree[i] emo_emojiString]];
                        
                        if (i != arrTree.count - 1) {
                            [mutString appendString:@"/"];
                        }
                    }
                    NSString *strContent = dic[@"data"][@"content"];
                    if(strContent.length > 0)
                    {
                        heightConstraintCallout.constant = 94;
                    }
                    callObs.text = mutString;
                    imgObsCallObs.hidden = NO;
                }
            }
            [self.vContainer layoutIfNeeded];
            if ([dic[@"socket"] boolValue]) {
                [self forceRefresh];
            }
        };
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)fnHideShowTOP:(id)sender {
//    constraintTOPMOST;
    
    if (constraintTOPMOST.constant == 0) {
        constraintTOPMOST.constant = -120;
        topButtonConstraint.constant = 40;
    }else{
        constraintTOPMOST.constant = 0;
        topButtonConstraint.constant = 20;
    }
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         [self.vContainer layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         if (constraintTOPMOST.constant == 0) {
                             [btnTOP setBackgroundImage: [UIImage imageNamed:@"map_new_ic_menu_open"] forState:UIControlStateNormal ];
                         }else{
                             [btnTOP setBackgroundImage: [UIImage imageNamed:@"map_new_ic_menu_close"] forState:UIControlStateNormal ];
                             
                         }
                         
                     }];
    
}

- (IBAction)fnHideShow:(id)sender {
    
    if (constraintLEFT.constant == 0) {
        constraintLEFT.constant = - CGRectGetWidth(vContainerLeft
                                                   .frame) + 5;
    }else{
        constraintLEFT.constant = 0;
    }
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         //                         [self.vContainer layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         if (constraintLEFT.constant == 0) {
                         }else{
                             //hide callout
                             vCallOut.alpha = 0;
                             vArrow.alpha = 0;
                             [vcLiveHunt unSelectedItem];
                             btnCloseCallout.hidden = TRUE;
                         }
                         
                     }];
    
    
    
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         
                         [self.vContainer layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         if (constraintLEFT.constant == 0) {
                             btnShow.hidden = YES;
                             btnHide.hidden = NO;
                         }else{
                             
                             btnShow.hidden = NO;
                             btnHide.hidden = YES;
                         }
                         
                     }];
    
}

- (IBAction)fnAddMessage:(id)sender {
    AlertVC *vc = [[AlertVC alloc] initInputMessage];
    [vc doBlock:^(NSInteger index, NSString* txtStr) {
        
        NSString *strMessageUnicode = [txtStr emo_UnicodeEmojiString];
        ASLog(@">>>>>>> %@", strMessageUnicode);
        
        
        if ([COMMON isReachable] == NO)
        {
            [KSToastView ks_showToast: NSLocalizedString(kWarnChatMessage, @"") duration:2.0f completion: ^{
            }];
            
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //push in cache
            
            NSDate *date=[NSDate date];
            
            //save
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
            [localContext MR_saveToPersistentStoreAndWait];
            
            if ([COMMON isReachable]) {
                [[ServiceHelper sharedInstance] postingChatMessageONLINE:@{
                                                                           @"idgroup":  [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue],
                                                                           @"message":strMessageUnicode,
                                                                           @"date":date,
                                                                           @"targetScreen": [NSNumber numberWithInt:ISLOUNGE],
                                                                           @"joinStr": @""
                                                                           }];
                
                
            }else{
                [[ServiceHelper sharedInstance] prepareSendingMessage:@{
                                                                        @"idgroup":  [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue],
                                                                        @"message":strMessageUnicode,
                                                                        @"date":date,
                                                                        @"targetScreen": [NSNumber numberWithInt:ISLOUNGE],
                                                                        @"joinStr": @""
                                                                        }];
                
                
            }
            
            
        });
        
    }];
    [vc showAlert];
    
}

- (IBAction)fnClosePage:(id)sender {
    [self gotoback];
}
- (IBAction)fnCloseVPop:(id)sender {
    //hide callout
    if (btnCloseCallout.hidden == FALSE) {
        vCallOut.alpha = 0;
        vArrow.alpha = 0;
        //        [vcLiveHunt unSelectedItem];
        btnCloseCallout.hidden = TRUE;
        
    }
}

- (IBAction)fnGoDetailFromCallout:(id)sender {
    //hide callout
    vCallOut.alpha = 0;
    vArrow.alpha = 0;
    [vcLiveHunt unSelectedItem];
    btnCloseCallout.hidden = TRUE;
    
    ASLog(@">>>> %@",dicSelectedLeftItem);
    
    if ( [ dicSelectedLeftItem[@"data"][@"type"] isEqualToString:@"message" ]) {
        
        ChatVC * vc = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        vc.expectTarget = ISLOUNGE;
        
        vc.isLiveHunt = YES;
        
        //        [[GroupEnterOBJ sharedInstance] resetParams];
        //        [GroupEnterOBJ sharedInstance].dictionaryGroup = dicSelectedLeftItem[@"data"];
        [self pushVC:vc animate:TRUE];
    }else{
        //Show detail
        CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
        commentVC.commentDic = dicSelectedLeftItem[@"data"];
        commentVC.isLiveHuntDetail = self.isLiveHunt;
        
        [self pushVC:commentVC animate:YES expectTarget:ISCARTE];
        
        
    }
    
}


#pragma mark - add

- (IBAction)fnAddPublication:(id)sender {
    [[PublicationOBJ sharedInstance]  resetParams];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
    PublicationNew_Signalement *viewController1 = [sb instantiateViewControllerWithIdentifier:@"PublicationNew_Signalement"];
    viewController1.iType = PUB1;
    //Set MAP position...Mes group auto select...
    
    [PublicationOBJ sharedInstance].latitude = strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;
    [PublicationOBJ sharedInstance].altitude = [NSString stringWithFormat:@"%f", vTmpMap.mapView_.myLocation.altitude];
    
    [self pushVC:viewController1 animate:YES expectTarget:ISCARTE];
    
}

- (IBAction)fnAddPublicationSignal:(id)sender {
    
    [[PublicationOBJ sharedInstance]  resetParams];
    NSString * strTmp = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
    [[PublicationOBJ sharedInstance]  resetParams];
    [PublicationOBJ sharedInstance].huntID = strTmp;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
    PublicationNew_Signalement *viewController1 = [sb instantiateViewControllerWithIdentifier:@"PublicationNew_Signalement"];
    viewController1.iType = PUB1;
    //Set MAP position...Mes group auto select...
    
    [PublicationOBJ sharedInstance].latitude = strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;
    [PublicationOBJ sharedInstance].altitude = [NSString stringWithFormat:@"%f", vTmpMap.mapView_.myLocation.altitude];
    
    if (self.expectTarget == ISLOUNGE)
    {
        [self pushVC:viewController1 animate:YES expectTarget:ISLIVE];
    } else {
        [self pushVC:viewController1 animate:YES expectTarget:ISCARTE];
    }
    
}

-(IBAction)fnModifyHunt:(id)sender
{
    
    NSDictionary *dic = [LiveHuntOBJ sharedInstance].dicLiveHunt ;
    
    NSMutableDictionary * mutDic = [dic mutableCopy];
    
    //I am admin ?
    if (dic && [dic[@"connected"][@"access"]isEqual:@3]) {
        
        //reformat
        if ([dic[@"meeting"] isKindOfClass: [NSDictionary class]]) {
            
            mutDic[@"meetingDate"] = dic[@"meeting"][@"date_begin"] ;
            mutDic[@"endDate"] = dic[@"meeting"][@"date_end"] ;
            mutDic[@"meetingAddress"] = @{
                                          @"latitude": dic[@"meeting"][@"address"][@"latitude"],
                                          @"latitude": dic[@"meeting"][@"address"][@"longitude"]
                                          };
            
        }
        if(!dic[@"limitation"])
        {
            mutDic[@"limitation"] = @{
                                      @"allow_add":dic[@"allow_add"],
                                      @"allow_add_chat":dic[@"allow_add_chat"],
                                      @"allow_show":dic[@"allow_show"],
                                      @"allow_show_chat": dic[@"allow_show_chat"]
                                      };
        }
        [[ChassesCreateOBJ sharedInstance] fnSetData:mutDic];
        ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
        viewController1.isModify = TRUE;
        
        [self pushVC:viewController1 animate:YES];
        
    }
}

@end
