//
//  SettingNotificationCell.m
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import "SettingNotificationCell.h"

@implementation SettingNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.vBorder.layer.masksToBounds = YES;
    self.vBorder.layer.cornerRadius = 4;
    self.vBorder.layer.borderWidth  = 0.5;
    self.vBorder.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
