//
//  MDMarkersLegend.h
//  Naturapass
//
//  Created by Manh on 3/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^callBackMarkers) (UIImage *image);
@interface MDMarkersText : UIView
@property (nonatomic,strong) NSString *strText;
@property (nonatomic, copy) callBackMarkers CallBackMarkers;
-(instancetype)initWithText:(NSString*)text;
- (void)doSetCalloutView;
@end
