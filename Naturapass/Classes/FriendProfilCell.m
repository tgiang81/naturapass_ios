//
//  FriendProfilCell.m
//  Naturapass
//
//  Created by JoJo on 12/17/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "FriendProfilCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AppCommon.h"
@implementation FriendProfilCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.viewMain.layer.masksToBounds = YES;
    self.viewMain.layer.cornerRadius= 5;
    
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.cornerRadius= 30;
    
    self.btnLeft.layer.masksToBounds = YES;
    self.btnLeft.layer.cornerRadius= 15;

    _btnRight.layer.masksToBounds = YES;
    _btnRight.layer.cornerRadius= 15;

    
    self.imgSettingBackground.layer.masksToBounds = YES;
    self.imgSettingBackground.layer.cornerRadius= 15;
    
    self.viewSetting.layer.masksToBounds = YES;
    self.viewSetting.layer.cornerRadius= 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)fnSetFriendInfo:(NSDictionary*)dic
{
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    BOOL isAddFriendHide =YES;
    int stateFriend = NOT_FRIEND;
    if ([dic[@"id"] integerValue] ==[my_id intValue]) {
        isAddFriendHide= YES;
    }
    else
    {
        NSDictionary *dicRelation = dic[@"relation"];
        if ([dicRelation isKindOfClass:[NSDictionary class]]) {
            
            if ([dicRelation[@"friendship"] isKindOfClass:[NSDictionary class]])
            {
                 stateFriend =[dicRelation[@"friendship"][@"state"] intValue];
                switch (stateFriend) {
                    case NOT_FRIEND:
                    {
                        
                        isAddFriendHide=NO;
                    }
                        break;
                    case WAIT_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_REJECTED:
                    {
                        isAddFriendHide=NO;
                        
                    }
                        break;
                    default:
                    {
                        isAddFriendHide=NO;
                        
                    }
                        break;
                }
                
            }else{
                isAddFriendHide=NO;
            }
        }else{
            /*
             friendship =     {
             state = 2;
             way = 2;
             };
             */
            if ([dic[@"friendship"] isKindOfClass:[NSDictionary class]])
            {
                 stateFriend =[dic[@"friendship"][@"state"] intValue];
                switch (stateFriend) {
                    case NOT_FRIEND:
                    {
                        
                        isAddFriendHide=NO;
                    }
                        break;
                    case WAIT_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    case IS_FRIEND:
                    {
                        isAddFriendHide=YES;
                        
                    }
                        break;
                    default:
                    {
                        isAddFriendHide=NO;
                        
                    }
                        break;
                }
                
            }else{
                isAddFriendHide=NO;
            }
        }
    }
    [self checkAjouterButton:stateFriend];
    
    
    _lbTitle.text = [NSString stringWithFormat:@"%@",dic[@"fullname"]];

    NSString *strImage=@"";
    if (dic[@"owner"][@"profilepicture"] != nil) {
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
    }else{
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    
    [_imgAvatar sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];


}

-(void)checkAjouterButton:(ISFRIEND_KIND)friendKind
{
    _btnLeft.backgroundColor =_colorAnnuler;
    _btnRight.backgroundColor =UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    switch (friendKind) {
        case IS_FRIEND:
        case WAIT_FRIEND:

            {
                [_btnLeft setTitle:str(strENLEVER) forState:UIControlStateNormal];
                [_btnRight setTitle:str(strMESSAGE) forState:UIControlStateNormal];

            }
            break;

        case IS_REJECTED:
        default:
        {
            [_btnLeft setTitle:str(strMESSAGE) forState:UIControlStateNormal];
            [_btnRight setTitle:str(strAJOUTER) forState:UIControlStateNormal];

        }
            break;
    }
}
-(void)fnSetColorWithExpectTarget:(ISSCREEN)expTarget
{
    _expectTarget = expTarget;
    _colorAnnuler = UIColorFromRGB(POSTER_ANNULER_COLOR);
    switch (self.expectTarget) {
        case ISLOUNGE:
        {
            _colorCommon = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            _colorCommon = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        default:
        {
            _colorCommon = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
    }

    _btnRight.backgroundColor = _colorCommon;
    self.lbTitle.textColor = _colorCommon;
    
    _btnLeft.backgroundColor = _colorAnnuler;
    self.lbAmis.textColor = _colorAnnuler;
    self.lbAmisCommon.textColor = _colorAnnuler;
    self.imgLine.backgroundColor = _colorAnnuler;
    self.imgSettingBackground.backgroundColor  = _colorAnnuler;
    self.viewSetting.backgroundColor = _colorAnnuler;
}
@end
