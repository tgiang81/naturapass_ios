//
//  ChassesSearchVC.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ChassesBaseVC.h"
//@protocol ChassesToutesDelegate <NSObject>
//-(void) pushViewController :(UIViewController*)vc;
//@end
@interface ChassesSearchVC : ChassesBaseVC
{

    BOOL                isLoading;
    NSInteger           joinInteger;
    
    //FooterView
    UIView                  *refreshFooterView;
    UILabel                 *refreshFooterLabel;
    UILabel                 *timeFooterLabel;
    UIImageView             *refreshFooterArrow;
    UIActivityIndicatorView *refreshFooterSpinner;
    NSString                 *textFooterLoading;
    NSString                 *textFooterPull;
    NSString                 *textFooterRelease;
    BOOL                     isFooterDragging;
    BOOL                     isDragging;
    //check refreshHeader or refreshFooter
    BOOL isFooter;
    NSInteger               searchInteger;
    NSString                *strLoungeID;
    //searchBar
    IBOutlet UIView         *memberListView;
    
    //array
    NSMutableArray                      *aryHeader;
    
    NSMutableArray                      *tourMemberArray;
    
    NSString                            *strTourUserID;
    UIButton                    *btnSelect;
    UIButton                    *btnOnBig;
    BOOL                        isTypeSelected;
    NSString                    *strExpandTable;
    NSNumber                    *numberOption;
    NSString                    *strAccess;
    //table
    
    NSInteger                           tourSuscriberInteger;
    NSInteger                           requestTag;
    NSMutableDictionary                 *tourDict;
    
    IBOutlet UIImageView                *tourLoungeImage;
    IBOutlet UILabel                    *tourTitleLabel;
    IBOutlet UILabel                    *tourDescLabel;
    IBOutlet UILabel                    *toursuscriberCountLabel;
    IBOutlet UIView                     *suscriberView;
    BOOL                                isOpen;
    
    UIActionSheet               *actionSheet;
    
}

@property (nonatomic,retain) NSString *strDate;
//@property (nonatomic,retain) NSMutableArray *mesIDArray;
//@property (nonatomic,retain)id<ChassesToutesDelegate>delegateVC;
//-(void)clearTextSearchBar;
@end