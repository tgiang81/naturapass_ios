//
//  ChasseSettingMembresComment.m
//  Naturapass
//
//  Created by Manh on 9/22/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChasseCreatePersonneFictif.h"
#import "SAMTextView.h"
#import "ChassesMemberOBJ.h"
#import "ChassesCreaePersonneFictifParticipe.h"
@interface ChasseCreatePersonneFictif ()
{
    IBOutlet SAMTextView *textviewCommentPublic;
    IBOutlet SAMTextView *textviewCommentPrive;
    IBOutlet UITextField *tfFirstName;//Premon
    IBOutlet UITextField *tfLastName;//Nom
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;

}
@end

@implementation ChasseCreatePersonneFictif

- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strINVITEZ_DES_NON_MEMBRES_NATURAPASS);
    lbDescription2.text = str(strPour_les_personnes_qui_ne_sont_pas_inscrites);
    tfFirstName.placeholder = str(strPrenom);
    tfLastName.placeholder = str(strNom);

    // Do any additional setup after loading the view from its nib.
    [self.btnSuivant setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    [self InitializeKeyboardToolBar];

    textviewCommentPublic.placeholder = str(strPlaceholder);
    [tfFirstName setInputAccessoryView:self.keyboardToolbar];
    [tfLastName setInputAccessoryView:self.keyboardToolbar];
    [textviewCommentPublic setInputAccessoryView:self.keyboardToolbar];
    [textviewCommentPrive setInputAccessoryView:self.keyboardToolbar];

}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    
}
-(IBAction)onNext:(id)sender
{
    if ([tfFirstName.text isEqualToString:@""]) {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(SilvousplaitentrezPrenomValide) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOui), nil];
        [alert show];
        return;
    }
//    else if ([tfLastName.text isEqualToString:@""]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"NaturaPass" message:@"S'il vous plaît entrez Nom valide" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Oui", nil];
//        [alert show];
//        return;
//    }
    else
    {
        NSString *strFirtName =tfFirstName.text?tfFirstName.text:@"";
        NSString *strLastName =tfLastName.text?tfLastName.text:@"";
        NSString *strCommentPublic = textviewCommentPublic.text?textviewCommentPublic.text:@"";
        NSString *strComentPrive =textviewCommentPrive.text?textviewCommentPrive.text:@"";
        
        [ChassesMemberOBJ sharedInstance].strFirtName= strFirtName;
        [ChassesMemberOBJ sharedInstance].strLastName= strLastName;
        [ChassesMemberOBJ sharedInstance].strCommentPublic= strCommentPublic;
        [ChassesMemberOBJ sharedInstance].strComentPrive= strComentPrive;
        [ChassesMemberOBJ sharedInstance].strMyKindId = [ChassesCreateOBJ sharedInstance].strID;

        ChassesCreaePersonneFictifParticipe  *viewController1=[[ChassesCreaePersonneFictifParticipe alloc]initWithNibName:@"ChassesCreaePersonneFictifParticipe" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
    
}
@end
