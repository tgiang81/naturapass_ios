//
//  Publication_Color.m
//  Naturapass
//
//  Created by Giang on 11/18/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Publication_Color.h"
#import "Publication_Specification.h"
#import "ColorCollectionCell.h"

@interface Publication_Color ()
{
    NSMutableArray * arrData;
    int indexSelect;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UIButton *btnSuivant;

}
@end

@implementation Publication_Color

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {

    if (indexSelect > -1) {
        NSDictionary *dic = arrData[indexSelect];
        [PublicationOBJ sharedInstance].publicationcolor = dic[@"id"];
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        [dicFavo setValue:dic forKey:@"color"];
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];

    }
    if ([PublicationOBJ sharedInstance].isEditer) {
        [[PublicationOBJ sharedInstance] modifiPublication:self withType:EDIT_COLOR];
    }
    else
    {
        if (self.isEditFavo) {
            [self gotoback];
        }
        else
        {
            Publication_Specification *viewController1 = [[Publication_Specification alloc] initWithNibName:@"Publication_Specification" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }

    }


}

- (NSString *) stringToHex:(NSString *)str
{
    NSUInteger len = [str length];
    unichar *chars = malloc(len * sizeof(unichar));
    [str getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for(NSUInteger i = 0; i < len; i++ )
    {
        // [hexString [NSString stringWithFormat:@"%02x", chars[i]]]; /*previous input*/
        [hexString appendFormat:@"%02x", chars[i]]; /*EDITED PER COMMENT BELOW*/
    }
    free(chars);
    
    return hexString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strSelectionnez_la_couleur_de_votre_repere);
    lbDescription2.text = str(strFacultatif);
    [btnSuivant setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    arrData = [NSMutableArray new];
    indexSelect = -1;

    NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
    
    NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
    
    [PublicationOBJ sharedInstance].dicColor = dic;
    
    [arrData addObjectsFromArray:[PublicationOBJ sharedInstance].dicColor[@"colors"] ];

    
    
    [myCollection registerNib:[UINib nibWithNibName:@"ColorCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"ColorCollectionCell"];

//    NSArray*arrAddressFav = [[NSUserDefaults standardUserDefaults] objectForKey: @"COLOR_PATTERN"];
//    
//    [arrData addObjectsFromArray:arrAddressFav];
    //reset
    if (!self.isEditFavo) {
//        [PublicationOBJ sharedInstance].dicFavoris =nil;
    }
    else
    {
        NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
        if([dicFavo[@"color"] isKindOfClass:[NSDictionary class]])
        {
            for (int i =0; i<arrData.count; i++) {
                NSDictionary *dicData = arrData[i];
                if ( [dicData[@"id"] intValue] == [dicFavo[@"color"][@"id"] intValue]) {
                    indexSelect = i;
                    break;
                }
            }
        }
    }
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrData.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    indexSelect = (int)indexPath.row;
    [myCollection reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"ColorCollectionCell";
    
    ColorCollectionCell *cell = (ColorCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
        NSDictionary *dic = arrData[indexPath.row];
    
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:dic[@"color"]];
    
        [scanner setScanLocation:0]; // bypass '#' character
        [scanner scanHexInt:&result];
    
        [cell.vColor setBackgroundColor:UIColorFromRGB(result)];
    
    if (indexPath.row == indexSelect) {
        cell.vColor.layer.borderWidth = 2.0;
        cell.vColor.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        if ([dic[@"color"] isEqualToString:@"FFFFFF"]) {
            
            cell.imgCheck.image = [UIImage imageNamed:@"CheckBlack"];
            
        }
        else
        {
            cell.imgCheck.image = [UIImage imageNamed:@"CheckColor"];
            
        }
    }else{
        cell.vColor.layer.borderWidth = 0.0;
        cell.imgCheck.image = nil;

    }
    
    return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(70, 70);
}
@end
