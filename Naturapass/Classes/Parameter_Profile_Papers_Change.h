//
//  GroupCreate_Step1.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ParameterBaseVC.h"
#import "DPTextField.h"

@interface Parameter_Profile_Papers_Change : ParameterBaseVC
{        
        //textfield
    IBOutlet UILabel        *descTitleLabel;
    IBOutlet UITextView     *descTextView;
    IBOutlet UITextField    *nameTextField;
    IBOutlet UIView         *viewTitle;
    IBOutlet UIView         *viewDescription;

}
@property(nonatomic,assign) BOOL isModifi;
@property(nonatomic,strong) NSDictionary *dicModifi;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewDesc;

@end
