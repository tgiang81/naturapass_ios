//
//  AlertListFrequenceCell.h
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDRadioButton.h"
@interface AlertListFrequenceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet MDRadioButton *btnRadio;

@end
