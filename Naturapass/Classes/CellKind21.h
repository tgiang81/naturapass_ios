//
//  CellKind21.h
//  Naturapass
//
//  Created by Giang on 11/18/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface CellKind21 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *color;
@property (weak, nonatomic) IBOutlet UIImageView *select;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
