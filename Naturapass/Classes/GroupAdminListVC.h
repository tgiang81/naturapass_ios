//
//  ConfigurationMessageViewController.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 2/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

@protocol CONFIGURATIONDELEGATE;


#import <UIKit/UIKit.h>
#import "GroupEnterBaseVC.h"
typedef void (^GroupAdminListVCCallback)(NSString *group_id);

@interface GroupAdminListVC : GroupEnterBaseVC
{
    //tableview
    
    //array
    NSMutableArray                  *arrDataRoot;

    NSMutableArray                  *sharingListArray;
    NSString                        *Group_Id;
    BOOL                             is_Admin;

    //textField
    IBOutlet UIButton               *deleteButton;

}
@property (nonatomic,copy) GroupAdminListVCCallback callback;
-(void)doCallback:(GroupAdminListVCCallback)callback;

-(void)fnGroupId:(NSString*)group_id isAdmin:(BOOL)isAdmin;
@property (nonatomic,retain)NSString *strDefaultText;
@property (nonatomic,retain)NSString *strPersonal;@property (nonatomic,retain)id<CONFIGURATIONDELEGATE>configureDelegate;
@end

@protocol CONFIGURATIONDELEGATE <NSObject>
@optional
- (void)loadConfigure:(NSMutableArray*)array;

-(void)loadSlideValues;
-(void) backAndRefreshGroupAdminWithMur:(BOOL)isRootMur;
@end