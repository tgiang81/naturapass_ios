//
//  CellKind2.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CellKind5.h"

@implementation CellKind5

-(void) awakeFromNib
{
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];

    self.iValue = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
