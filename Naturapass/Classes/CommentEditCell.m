//
//  CommentEditCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 1/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "CommentEditCell.h"
#import "AppCommon.h"

#import "ASSharedTimeFormatter.h"
#import "OHASBasicHTMLParser.h"
#import "NSString+Extensions.h"
#import "Define.h"
#import "NSString+EMOEmoji.h"
@implementation CommentEditCell
{
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
    UIBarButtonItem             *previousBarItem;
    UIBarButtonItem             *nextBarItem;
    __weak IBOutlet UILabel *numberRemain;
    
}
@synthesize likeButton;
@synthesize likeImageButton;
@synthesize commentButton;
@synthesize unlikeImageButton;
@synthesize sharebutton;
@synthesize shareimageButton;
@synthesize commentView;
@synthesize mainView;
@synthesize userImage;
@synthesize murTitleLabel;
@synthesize dateLabel;
@synthesize likeCountLabel;
@synthesize unlikeCountLabel;
@synthesize commentCountLabel;
@synthesize commentlikeCountButton;
@synthesize commentunlikeCountButton;
@synthesize commentTitleLabel;
@synthesize commentdateLabel;
@synthesize commentliketextButton;
@synthesize commentUnliketextButton;
@synthesize descriptionImage;
@synthesize commentCountView;
@synthesize commentaireLabel;
@synthesize locationBtn, locationImageBtn;
@synthesize coverTapBtn,locationCommentBtn;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.mainView.layer setMasksToBounds:YES];
    self.mainView.layer.cornerRadius= 4.0;
    
    [self.imgBackGroundSetting setImage:[UIImage imageNamed:@"bg_setting_post"]];
    [self.imgSettingSelected setImage:[UIImage imageNamed:@"ic_admin_setting"]];
    [self.imgSettingNormal setImage:[UIImage imageNamed:@"mur_ic_action_setting_post"]];

    [self.commentUserImage.layer setMasksToBounds:YES];
    self.commentUserImage.layer.cornerRadius=  20;
    self.commentUserImage.layer.borderWidth =0;
    [self.imgBackGroundSetting.layer setMasksToBounds:YES];
    self.imgBackGroundSetting.layer.cornerRadius=  13;
    
    [self.tvEditText.layer setMasksToBounds:YES];
    self.tvEditText.layer.cornerRadius=  5;
    self.tvEditText.backgroundColor = UIColorFromRGB(COMMENT_EDIT_TEXTVIEW_COLOR);
    [self.btnAnnuler.layer setMasksToBounds:YES];
    self.btnAnnuler.layer.cornerRadius=  20;
    self.btnAnnuler.backgroundColor = UIColorFromRGB(COMMENT_EDIT_ANNULER_COLOR);
    [self.btnAnnuler setTitle:[str(strAnuler) uppercaseString] forState:UIControlStateNormal];

    [self.btnModifier.layer setMasksToBounds:YES];
    self.btnModifier.layer.cornerRadius=  20;
    self.btnModifier.backgroundColor = UIColorFromRGB(COMMENT_EDIT_MODIFIER_COLOR);
    [self.btnModifier setTitle:[str(strMODIFIER) uppercaseString] forState:UIControlStateNormal];
    [self InitializeKeyboardToolBar];

    
}
-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (self.keyboardToolbar == nil)
    {
        self.keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 38.0f)];
        self.keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:self
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strOK)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard:)];
        
        [self.keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                        spaceBarItem,
                                        spaceBarItem,
                                        spaceBarItem,
                                        doneBarItem, nil]];
        
        
    }
    [_tvEditText setInputAccessoryView:self.keyboardToolbar];
    _tvEditText.delegate = self;
}
- (void)resignKeyboard:(id)sender
{
    [_tvEditText resignFirstResponder];
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    _commentLabel.preferredMaxLayoutWidth = CGRectGetWidth(_commentLabel.frame);
}

-(void)setTitleLabel:(NSString *)_text
{
    murTitleLabel.text=_text;
}
-(void)setmessageView:(NSString *)_text
{
//    messagesTextView.text=_text;
}
-(void)setImageView:(NSString *)_text
{
    userImage.image=[UIImage imageNamed:_text];
}

-(void)assignValuesHeaderSection:(NSDictionary *)dic{
    NSString *strIconLike =@"";
    
    
    [likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    if (self.iAmScreen ==ISLOUNGE) {
        strIconLike =@"chasse_ic_like";
        [likeButton setTitleColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)  forState:UIControlStateSelected];
    }
    else if (self.iAmScreen ==ISGROUP) {
        [likeButton setTitleColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)  forState:UIControlStateSelected];
        strIconLike =@"group_ic_like";
    }else if (self.iAmScreen ==ISLIVEMAP) {
        [likeButton setTitleColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)  forState:UIControlStateSelected];
        strIconLike =@"live_ic_like";
    } else
    {
        [likeButton setTitleColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)  forState:UIControlStateSelected];
        strIconLike =@"mur_ic_post_like";
    }
    
    NSString *strUserName=[NSString stringWithFormat:@"%@ %@",[[dic  valueForKey:@"owner"]valueForKey:@"firstname"],[[dic valueForKey:@"owner"]valueForKey:@"lastname"]];
    
    //set time of Mur

	NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
	
	NSDateFormatter *outputFormatter = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
	
	[ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
	
	[ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy HH:mm" forFormatter: outputFormatter];
	//Current - created...
    
    NSString *relativeTime = [dic valueForKey:@"created"];
    NSDate * inputDate = [ inputFormatter dateFromString:relativeTime ];
    NSString * outputString = [ outputFormatter stringFromDate:inputDate ];
    
    
    [murTitleLabel setText:strUserName];
    [likeButton setTitle:[NSString stringWithFormat:@"%@ J'aime  ",[dic  valueForKey:@"likes"]] forState:UIControlStateNormal];
    NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];

    [dateLabel setText: strDateLocation];
    NSString *messageContent = [dic valueForKey:@"content"];
    messagesTextLabel.htmlText =messageContent;

    if((([[dic valueForKey:@"isUserLike"] integerValue]==0) && ([[dic valueForKey:@"isUserUnlike"] integerValue]==0))) {
        [likeButton setSelected:NO];
        [self.iconLike setImage: [UIImage imageNamed:@"mur_ic_post_unlike"] ];
        
    }
    else if([[dic valueForKey:@"isUserLike"] integerValue]==1) {
        [likeButton setSelected:YES];
        [self.iconLike setImage: [UIImage imageNamed:strIconLike]];


    }

}


-(IBAction)likebuttonAction:(UIButton *)sender
{
}

-(IBAction)shareButtonAction:(UIButton *)sender
{
    if([sender isSelected]) {
        [shareimageButton setSelected:NO];
        [sharebutton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    else {
        [shareimageButton setSelected:YES];
        [sharebutton setTitleColor:[UIColor colorWithRed:138.0/255 green:189.0/255 blue:21.0/255 alpha:1.0] forState:UIControlStateNormal];
    }
}
-(void)setCount:(NSDictionary *)CountDict {
    NSString *strLikes=[[CountDict valueForKey:@"likes"] stringValue];
    NSString *strunLikes=[[CountDict valueForKey:@"unlikes"] stringValue];
    
    if((([[CountDict valueForKey:@"isUserLike"] integerValue]==0) && ([[CountDict valueForKey:@"isUserUnlike"] integerValue]==0))) {
        [likeButton setSelected:NO];
        [likeImageButton setSelected:NO];
        [unlikeImageButton setSelected:NO];
    }
    else if([[CountDict valueForKey:@"isUserLike"] integerValue]==1) {
        [likeButton setSelected:YES];
        [likeImageButton setSelected:YES];
        
    }
    else if([[CountDict valueForKey:@"isUserUnlike"] integerValue]==1) {
        [unlikeImageButton setSelected:YES];
    }
    
    [likeCountLabel setText:strLikes];
    [unlikeCountLabel setText:strunLikes];

}

-(void) setLikeStatus:(BOOL) isLike
{
    if (isLike) {
        [likeButton setTitleColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) forState:UIControlStateNormal];

        [self.iconLike setImage: [UIImage imageNamed:@"mur_ic_post_like"]];

    }else{
        
        [likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.iconLike setImage: [UIImage imageNamed:@"mur_ic_post_unlike"] ];
    }
}

-(void)Localization
{
    //Button
    [likeButton setTitle:NSLocalizedString(@"LIKE", @"") forState:UIControlStateNormal];
    [commentButton setTitle:NSLocalizedString(@"COMMAND", @"") forState:UIControlStateNormal];
    [sharebutton setTitle:NSLocalizedString(@"SHARE", @"") forState:UIControlStateNormal];
    [commentliketextButton setTitle:NSLocalizedString(@"SHARE", @"") forState:UIControlStateNormal];
    
    ///Label
    jaimeLabel.text=NSLocalizedString(@"LIKE", @"");
    jaimepas.text=NSLocalizedString(@"DISLIKE", @"");

    [commentliketextButton setTitle:NSLocalizedString(@"LIKE", @"") forState:UIControlStateNormal];
    [commentUnliketextButton setTitle:NSLocalizedString(@"DISLIKE", @"") forState:UIControlStateNormal];

    
}
-(void)setCommentText:(NSString *)strText{

    //TO EMOJI
    strText = [strText emo_emojiString];
    _commentLabel.htmlText = strText;
//    NSMutableAttributedString* attrStr = [commentLabel.attributedText mutableCopy];
//    [attrStr modifyParagraphStylesWithBlock:^(OHParagraphStyle *paragraphStyle) {
//        paragraphStyle.textAlignment = kCTJustifiedTextAlignment;
//        paragraphStyle.lineBreakMode = kCTLineBreakByWordWrapping;
//        paragraphStyle.paragraphSpacing = 0.f;
//        paragraphStyle.lineSpacing = 0.0f;
//    }];
//    [attrStr setFontName:@"HelveticaNeue" size:13];
//    if ([strText hasSubString:@"<a href="]) {
//        commentLabel.attributedText = [OHASBasicHTMLParser attributedStringByProcessingMarkupInAttributedString:attrStr];
//    } else {
//        commentLabel.attributedText = attrStr;
//    }
//    commentLabel.automaticallyAddLinksForType = NSTextCheckingTypeDate|NSTextCheckingTypeAddress|NSTextCheckingTypeLink|NSTextCheckingTypePhoneNumber;
}

#pragma mark - OHAttributedLabelDelegate

//-(UIColor*)attributedLabel:(OHAttributedLabel*)attrLabel colorForLink:(NSTextCheckingResult*)link underlineStyle:(int32_t*)pUnderline
//{
//    *pUnderline = attrLabel.linkUnderlineStyle; // use default value
//    return attrLabel.textColor; // use default value
//}


-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo
{
    [attributedLabel setNeedsRecomputeLinksInText];
    
    if ([[UIApplication sharedApplication] canOpenURL:linkInfo.extendedURL])
    {
        // use default behavior
        return YES;
    }
    
    return NO;
}
#pragma callback
-(void)setCallback:(CommentEditCellCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(CommentEditCellCallback ) cb
{
    self.callback = cb;
    
}
-(IBAction)annulerAction:(id)sender
{
    NSDictionary *dic =@{@"index":@0};
    if (_callback) {
        _callback(dic);
    }
}
-(IBAction)modifierAction:(id)sender
{
    NSString *strText =self.tvEditText.text?self.tvEditText.text:strEMPTY;
    NSDictionary *dic =@{@"index":@1,@"comment":strText};
    if (_callback) {
        _callback(dic);
    }
}
//MARK: - Text View Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    id view = [self superview];
    
    while (view && [view isKindOfClass:[UITableView class]] == NO) {
        view = [view superview];
    }
    
    UITableView *tableView = (UITableView *)view;
    NSIndexPath *indexPath = [tableView indexPathForCell: self];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    return TRUE;
}
//MARK: - Text View Delegate
-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    int remain = (int) (limitComment - (a.text.length + c.length));
    if (remain < 0) {
        NSString *strFullText = [a.text stringByAppendingString:c];
        NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitComment)];
        a.text = subString;
        [numberRemain setTextColor:[UIColor redColor]];
        [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitComment:remain,limitComment]];
    }
    
    return remain >= 0;
}
- (void)textViewDidChange:(UITextView *)textView
{
    [self fnNumberRemain:textView];
}
-(void)fnNumberRemain:(UITextView *)textView
{
    int remain = (int)textView.text.length;
    [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitComment:remain,limitComment]];
    
    // Check if the count is over the limit
    if(remain >= 4990) {
        // Change the color
        [numberRemain setTextColor:[UIColor redColor]];
    }
    else if(remain > 4950 && remain < 4990) {
        // Change the color to yellow
        [numberRemain setTextColor:[UIColor orangeColor]];
    }
    else {
        // Set normal color
        [numberRemain setTextColor:[UIColor darkGrayColor]];
    }
}
@end
