//
//  Publication_GeolocationVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_GeolocationVC.h"
#import "Publication_Carte.h"

#import "Publication_Partage.h"
#import "Publication_Specification.h"
@interface Publication_GeolocationVC ()

@end

@implementation Publication_GeolocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    
    //Prerequisite --> En Carte -> Spec...
    
    UIButton *btn = (UIButton*)sender;
    if (btn.tag == 10) {
        [PublicationOBJ sharedInstance].enableGeolocation = NO;
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        Publication_Specification *viewController1 = [[Publication_Specification alloc] initWithNibName:@"Publication_Specification" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

        //POST -> notify...
        
//        Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
//        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

    }else{
        //Enable GEO -> Can user Specification...
        
        [PublicationOBJ sharedInstance].enableGeolocation = YES;
        
        Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
        viewController1.expectTarget = ISMUR;
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

    }
    
    
}

@end
