//
//  MKMapView+ZoomLevel.h
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MKMapView (ZoomLevel)

-(double) zoomLevel;
- (void)setCenterCoordinate:(CLLocationCoordinate2D)coordinate
                  zoomLevel:(NSUInteger)zoom animated:(BOOL)animated;

@end
