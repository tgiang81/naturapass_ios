//
//  Signaler_ChoixSpecNiv4_elargi.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Signaler_ChoixSpecNiv4_elargi.h"
#import "Signaler_ChoixSpecNiv5.h"
#import "SubNavigationPRE_Search.h"
#import "CellKind23.h"
#import "AnimalsEntity.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Signaler_ChoixSpecNiv4_elargi () <UITextFieldDelegate>
{
    NSMutableArray * arrData;
    IBOutlet UIButton *btnSuivatnt;
    IBOutlet UIButton *btnAnnuler;
    IBOutlet UIView *viewBottom;

    IBOutlet UITextField *tfSearch;
    IBOutlet UIButton *btnSearch;

    __weak IBOutlet UILabel *lbWarning;
    IBOutlet UIView *viewSearch;
}

@property (nonatomic, strong) CellKind23 *prototypeCell;

@end

@implementation Signaler_ChoixSpecNiv4_elargi

- (void)viewDidLoad {
    [super viewDidLoad];
    MainNavigationBaseView *subview =  [self getSubMainView];
    NSString *strNameScreen = @"";
    self.vContainer.backgroundColor = self.colorAttachBackGround;
    switch (self.expectTarget) {
        case ISLIVE:
        case ISCARTE:
        {
            strNameScreen =@"Autres Animaux...";
        }
            break;
        default:
        {
            strNameScreen = @"Autres Animaux...";
        }
            break;
    }
    [subview.myDesc setText:strNameScreen];
    viewBottom.backgroundColor = self.colorBackGround;
    
    [btnSuivatnt.layer setMasksToBounds:YES];
    btnSuivatnt.layer.cornerRadius= 25.0;
    
    [btnAnnuler.layer setMasksToBounds:YES];
    btnAnnuler.layer.cornerRadius= 20.0;
    
    viewSearch.backgroundColor = [UIColor whiteColor];
    viewSearch.layer.masksToBounds = TRUE;
    viewSearch.layer.cornerRadius = 3;
    
    tfSearch.delegate = self;
    tfSearch.autocorrectionType = UITextAutocorrectionTypeNo;
    [self InitializeKeyboardToolBar];
    [tfSearch setInputAccessoryView:self.keyboardToolbar];

    arrData = [NSMutableArray new];

    lbWarning.text = @"";
    [lbWarning setNeedsDisplay];

    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [super viewWillDisappear:animated];
}
- (void)resignKeyboard:(id)sender
{
    [self fnSearch];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)annulerAction:(id)sender
{
    [self doRemoveObservation];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
- (IBAction)onNext:(id)sender {
    Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
}

- (CellKind23 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
        
    }
    
    return _prototypeCell;
    
}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellKind23 *cell=(CellKind23 *) cellTmp;
    [self setDataForGroupCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

-(void) setDataForGroupCell:(CellKind23*) cell withIndex:(NSIndexPath *)indexPath
{
    AnimalsEntity *dic = arrData[indexPath.row];
    
    //FONT
    cell.label1.text = dic.name;
    [cell.label1 setTextColor:[UIColor whiteColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.rightIcon setImage: [UIImage imageNamed:@"icon_sign_arrow_right" ]];
    
    cell.backgroundColor=self.colorNavigation;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    [cell layoutIfNeeded];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind23 *cell = (CellKind23 *)[tableView dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AnimalsEntity *dic = arrData[indexPath.row];

    //specific
    Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
    viewController1.iSpecific = 1;
    viewController1.animalNameSpecific =    dic.name;
    viewController1.id_animal = [dic.myid stringValue];
    viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic.name];

    //he has specific card ID
    //specific only 1
    
    if ([[PublicationOBJ sharedInstance] getCacheSpecific_Cards].count > 0) {
        viewController1.myCard = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
    }
    
    viewController1.myDic = self.myDic;
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];

}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //Validate next step
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self fnSearch];
    return YES;
}
-(IBAction)searchAction:(id)sender
{
    [self fnSearch];
}
-(void)fnSearch
{
    if ([tfSearch.text isEqualToString:@""]){
        
        //        arrFilter = [arrData copy];
    }
    else
    {
        //        NSArray *ab= [AnimalsEntity MR_findAll];
        NSString *strSearch = tfSearch.text;
        NSString *code = [strSearch substringFromIndex: [strSearch length] - 1];

        if (strSearch.length > 4 && [code isEqualToString:@"s"]) {

            strSearch = [strSearch substringToIndex:[strSearch length] - 1];
        }
        NSArray *arrTmp = [AnimalsEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"name contains[cd] %@",strSearch]];
        
        if (arrTmp.count <= 0) {
            lbWarning.text = str(strIl_ny_a_pas_danimaux_dans_notre_base);
        }else{
            lbWarning.text = strEMPTY;
        }
        [lbWarning setNeedsDisplay];
        
        [arrData removeAllObjects];
        [arrData addObjectsFromArray:arrTmp];
        [self.tableControl reloadData];
    }
    
    [tfSearch resignFirstResponder];
}
@end
