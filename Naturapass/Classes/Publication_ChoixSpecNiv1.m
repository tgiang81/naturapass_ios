//
//  Publication_ChoixSpecNiv1.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_ChoixSpecNiv1.h"
#import "Publication_ChoixSpecNiv2.h"
#import "Publication_ChoixSpecNiv5.h"
#import "Publication_Partage.h"
#import "Publication_ChoixSpecNiv4.h"
#import "CellKind2.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Publication_ChoixSpecNiv1 ()
{
    NSMutableArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation Publication_ChoixSpecNiv1

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isFromNewSignal) {
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myDesc setText:@"Autres..."];
        lbTitle.text = str(strPrecisez_sur_quoi_porte_votre_observation);
        lbTitle.textColor = [UIColor whiteColor];
    }else{
        lbTitle.text = str(strPrecisez_sur_quoi_porte_votre_observation);

    }
    

    arrData = [NSMutableArray new];

    [self reloadTable];

    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    [self.tableControl addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
}

-(void) reloadTable
{
    //If edit -> default
    if ([PublicationOBJ sharedInstance].isEditer)
    {
        //Get tree from geo...
        
        if ([PublicationOBJ sharedInstance].latitude.length>0 && [PublicationOBJ sharedInstance].longtitude.length>0 && [COMMON isReachable])
        {
            //But bad network?
            
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [WebServiceAPI new];
            [serviceObj getCategoriesGeolocated:[PublicationOBJ sharedInstance].latitude strLng:[PublicationOBJ sharedInstance].longtitude];
            
            serviceObj.onComplete = ^(NSDictionary*response1, int errCode){
                //Has Data
                if (![response1[@"model"] isKindOfClass: [NSNull class]])
                {
                    
                    if ([response1[@"model"] isKindOfClass: [NSString class]])
                    {
                        if ([response1[@"model"] isEqualToString:@"default"])
                        {
                            [COMMON removeProgressLoading];
                            
                            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                            
                            [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                            
                            [self doNext];
                            // get tree/card from tree cache default
                        }
                        else
                        {
                            // Receiver_1....                                     "model": "receiver_7"
                            NSString *strReceiver = response1[@"model"];
                            
                            //check in the cache
                            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                            
                            if (treeDic[strReceiver] != [NSNull class] && treeDic[strReceiver])
                            {
                                [COMMON removeProgressLoading];
                                
                                [PublicationOBJ sharedInstance].treeCategory = treeDic[strReceiver];
                                [self doNext];
                            }
                            else //doesn't exist...
                            {
                                strReceiver = [strReceiver stringByReplacingOccurrencesOfString:@"_" withString:@"="];
                                
                                //receiver_7
                                
                                WebServiceAPI *serviceGetTree =[WebServiceAPI new];
                                serviceGetTree.onComplete =^(id response2, int errCode){
                                    [COMMON removeProgressLoading];
                                    
                                    if ([response2 isKindOfClass: [NSDictionary class]]) {
                                        
                                        //if not contained -> request get new tree... then merge result to the cache
                                        [[PublicationOBJ sharedInstance] doMergeTreeWithNew:response2];
                                        //Get needed tree
                                        
                                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                                        
                                        [PublicationOBJ sharedInstance].treeCategory = treeDic[response1[@"model"]];
                                        [self doNext];
                                    }
                                    
                                };
                                
                                [serviceGetTree fnGET_CATEGORIES_BY_RECEIVER:strReceiver];
                                
                            }
                        }
                    }
                    else if ([response1[@"model"] isKindOfClass: [NSDictionary  class]])
                    {
                        //a real tree. => merge..
                        [COMMON removeProgressLoading];
                        
                    }
                    //A TREE
                    else if ([response1[@"tree"] isKindOfClass: [NSArray  class]])
                    {
                        [COMMON removeProgressLoading];
                        [PublicationOBJ sharedInstance].treeCategory = response1[@"tree"];
                        [self doNext];
                        
                        return;
                    }else{
                        [COMMON removeProgressLoading];
                        //Offline
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                        [self doNext];
                        
                    }
                }else{
                    [COMMON removeProgressLoading];
                    
                    //Offline
                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                    
                    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                    [self doNext];
                    
                }

            };
            
        }else{
            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
            
            [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
            [self doNext];

        }

    }else{
        [self doNext];

    }
    
}

-(void) doNext
{
    NSArray *listTree = [PublicationOBJ sharedInstance].treeCategory;
    
    //Reset
    [PublicationOBJ sharedInstance].arrReceivers = nil;
    
    
    for (NSDictionary *dic in listTree) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            [arrData addObject:dic];
        }
    }
    
    [self.tableControl reloadData];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (![@"contentSize" isEqualToString:keyPath])
        return;
    
    [self.tableControl removeObserver:self forKeyPath:@"contentSize" context:NULL];
    
    CGSize contentSize = [[change valueForKey:NSKeyValueChangeNewKey] CGSizeValue];
    
    self.tableControl.contentSize = contentSize;
    
    
    float contentHeight=self.tableControl.contentSize.height;
    CGRect rectTmp = self.tableControl.frame;
    
    self.tableControl.frame=CGRectMake(0, rectTmp.origin.y , rectTmp.size.width, contentHeight);
    
    [self.view layoutIfNeeded];
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.constraint_image_width.constant = cell.constraint_image_height.constant = 40;
    
    /*
     children =                     {
     };
     id = 183;
     name = "Rivi\U00e8re";
     photo = "/img/interface/default-observation.jpg";
     search = 1;
     */

//    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
//
//    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString: imgUrl ] ];
    
    if (self.isFromNewSignal) {
        //FONT
        cell.label1.text = dic[@"name"];
        [cell.label1 setTextColor:[UIColor whiteColor]];
        [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
        
        //Arrow
        UIImageView *btn = [UIImageView new];
        [btn setImage: [UIImage imageNamed:@"icon_sign_arrow_right" ]];
        
        
        btn.contentMode = UIViewContentModeScaleAspectFit;
        cell.constraint_image_width.constant = 0;
        cell.constraint_control_width.constant = 10;
        cell.constraint_control_height.constant = 20;
        
        btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
        
        
        cell.constraintRight.constant = 15;
        
        [cell.viewControl addSubview:btn];
        
        cell.backgroundColor=UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
        
    }else{
            //FONT
            cell.label1.text = dic[@"name"];
            [cell.label1 setTextColor:[UIColor blackColor]];
            [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
            
            //Arrow
            UIImageView *btn = [UIImageView new];
            [btn setImage: [UIImage imageNamed:@"arrow_icon" ]];
            
            
            btn.contentMode = UIViewContentModeScaleToFill;
            cell.constraint_image_width.constant = 0;
            cell.constraint_control_width.constant = 8;
            cell.constraint_control_height.constant = 13;
            
            btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
            
            
            cell.constraintRight.constant = 15;
            
            [cell.viewControl addSubview:btn];
            
            cell.backgroundColor=[UIColor whiteColor];
            
        }

    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrData[indexPath.row];
    //Specific
    if ( ( (NSArray*)dic[@"children"]).count > 0 ) {
        
        //has search option
        if ([dic[@"search"] intValue] ==  1)
        {
            Publication_ChoixSpecNiv4 *viewController1 = [[Publication_ChoixSpecNiv4 alloc] initWithNibName:@"Publication_ChoixSpecNiv4" bundle:nil];
            
            viewController1.myDic = dic;
            viewController1.myName = dic[@"name"];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
        }else{
            Publication_ChoixSpecNiv2 *viewController1 = [[Publication_ChoixSpecNiv2 alloc] initWithNibName:@"Publication_ChoixSpecNiv2" bundle:nil];
            viewController1.isFromNewSignal = self.isFromNewSignal;
            //EG..
            viewController1.myName = dic[@"name"];
            
            viewController1.myDic = dic;
            if (self.expectTarget == ISLIVE){
                [self pushVC:viewController1 animate:YES expectTarget:ISLIVE iAmParent:NO];
            } else {
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
            
        }
    }else{
        //Show card....
        if ( dic[@"card"] ) {
            //LEAF
            Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
            viewController1.myDic = dic;
            viewController1.myName = dic[@"name"];
            viewController1.iSpecific =  0;//[self.myDic[@"search"] intValue] ;
            if (self.expectTarget == ISLIVE) {
                [self pushVC:viewController1 animate:YES expectTarget:ISLIVE iAmParent:NO];
            } else {
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
            

        }else{
            //favo
            NSString *strPrecision =[NSString stringWithFormat:@"%@",dic[@"name"] ];
            NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
            if (strPrecision) {
                [dicFavo setValue:strPrecision forKey:@"category_tree"];
            }

            NSDictionary *dicObservation = @{
                                             @"specific": [NSNumber numberWithInt: 0 ],
                                             @"category" : dic[@"id"]
                                             };
            
            [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
            [dicFavo setValue:dicObservation forKey:@"observation"];
            if ([dic[@"receivers"] isKindOfClass: [NSArray class]]) {
                [PublicationOBJ sharedInstance].arrReceivers = dic[@"receivers"];
                [dicFavo setValue:dic[@"receivers"] forKey:@"receivers"];

            }
            [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];

            //edit ob
            if ([PublicationOBJ sharedInstance].isEditer) {
                
                //                [[PublicationOBJ sharedInstance] modifiObservationWithVC:self];
                [[PublicationOBJ sharedInstance]  modifiPublication:self  withType:EDIT_PRECISIONS];
                
            }
            else if ([PublicationOBJ sharedInstance].isEditFavo)
            {
                [self backEditFavo];
            }
            else
            {
                Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
        }
        
    }

}

@end
