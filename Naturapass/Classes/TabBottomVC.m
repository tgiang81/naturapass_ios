//
//  TabVC.m
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "TabBottomVC.h"

@interface TabBottomVC ()

@end

@implementation TabBottomVC

-(void) badgingBtn:(UIButton*)btn count:(int) iCount
{
    btn.badgeView.font = [UIFont boldSystemFontOfSize:10.0];
    [btn.badgeView setBadgeValue:iCount];
    [btn.badgeView setOutlineWidth:1.0];
    [btn.badgeView setPosition:MGBadgePositionTopRight];
    [btn.badgeView setOutlineColor:[UIColor whiteColor]];
    [btn.badgeView setBadgeColor:[UIColor redColor]];
    [btn.badgeView setTextColor:[UIColor whiteColor]];
}
-(IBAction)fnSubNavClick:(id)sender
{
    //Left -> Right... 10 11 12
    UIButton *btn = (UIButton*) sender;
    
    //Set selected color
    
    for (UIView*view in self.subviews) {
        if ([view isKindOfClass: [UIButton class] ]) {
            
            UIButton*bTmp = (UIButton*)view;
            if (bTmp == btn) {
                [bTmp setSelected:YES];
                continue;
            }else{
                //Change color
                [bTmp setSelected:NO];
            }
            
        }
    }
    
    
    //set selected
    if (self.myCallBack) {
        self.myCallBack(btn);
        
    }
    
}

@end
