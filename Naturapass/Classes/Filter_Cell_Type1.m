//
//  Filter_Cell_Type1.m
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Filter_Cell_Type1.h"
#import "Define.h"
@implementation Filter_Cell_Type1

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setBackgroundColor:UIColorFromRGB(FILTER_VIEW_CELL_COLOR)];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
