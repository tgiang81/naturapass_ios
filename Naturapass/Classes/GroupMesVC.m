//
//  GroupMesVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupMesVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SignalerTerminez.h"
#import "GroupesViewBaseCell.h"
#import "GroupCreate_Step1.h"
#import "GroupEnterMurVC.h"
#import "Define.h"
#import "GroupCreate_Step3.h"
#import "GroupEnterOBJ.h"
#import "GroupSettingMembres.h"
#import "GroupEnterSettingVC.h"
#import "GroupCreateOBJ.h"
#import "CommonHelper.h"
#import "groupEditListVC.h"
#import "MapDataDownloader.h"
#import "AmisAddScreen1.h"
#import "GroupCreateOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateV2.h"
#import "Publication_FavoriteAddress.h"
#import "ChatListe.h"
#import "Publication_Carte.h"

static NSString *GroupesCellAdmin =@"GroupesViewCellAdmin";
static NSString *GroupesCellID1 =@"GroupesViewCellID1";

static NSString *GroupesCellNormal =@"GroupesViewCellNormal";
static NSString *GroupesCellID2 =@"GroupesViewCellID2";

@interface GroupMesVC ()<WebServiceAPIDelegate>
{
    // array
    NSMutableArray                  *groupsActionArray;
    NSMutableArray                  *groupsActionArrayTmp;
    NSMutableArray                  *groupsActionArraySearch;
    IBOutlet UISearchBar                *toussearchBar;
    IBOutlet UILabel  *lbTitle;
}
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;
@property (nonatomic, strong) GroupesViewBaseCell *prototypeCell;
@property (nonatomic, assign) BOOL shouldBeginEditing;

@end

@implementation GroupMesVC
#pragma mark - initialization and viewload
- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMESGROUPES);
    toussearchBar.placeholder = str(strSearchGroup);
    [self initRefreshControl];
    
    self.isGoin = YES;
    _lbMessage.text=EMPTY_GROUP_MES;
    [self addShortCut];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRefresh) name: NOTIFY_REFRESH_GROUP object:nil];
    
    [Flurry logEvent:@"MesGroupFragment" timed:YES];
    
    if (self.isGoin) {
        self.isGoin = NO;
        [self insertRowAtTop];
        [self initialization];
        
    }
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
    
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group]];
}


-(void)doRefresh
{
    //get data from database...refresh view
    [self startRefreshControl];
}

- (GroupesViewBaseCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:GroupesCellID1];
    }
    return _prototypeCell;
}

-(void)initialization
{
    // init tableviewcell
    [self.tableControl registerNib:[UINib nibWithNibName:GroupesCellAdmin bundle:nil] forCellReuseIdentifier:GroupesCellID1];
    
    [self.tableControl registerNib:[UINib nibWithNibName:GroupesCellNormal bundle:nil] forCellReuseIdentifier:GroupesCellID2];
    self.tableControl.estimatedRowHeight = 100;
    //
    [toussearchBar setTintColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
    
    
    //init array
    groupsActionArray=[[NSMutableArray alloc]init];
    groupsActionArraySearch = [NSMutableArray new];
    groupsActionArrayTmp = [NSMutableArray new];
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_GROUP_MUR_SAVE)   ];
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        [groupsActionArray  addObjectsFromArray:arrTmp];
        if (!self.shouldBeginEditing) {
            groupsActionArrayTmp =[groupsActionArray mutableCopy];
        }
        [self.tableControl reloadData];
    }
}

-(void)checkEmpty:(NSMutableArray*)arr msg:(NSString*)strMsg
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
    }
    else
    {
        [self.lbMessage setText:strMsg];
        [self.lbMessage setNeedsDisplay];
        
        self.lbMessage.hidden=NO;
        [self.view bringSubviewToFront:self.lbMessage];
    }
}

#pragma mark - WebServiceAPI

- (void)insertRowAtTop {
    
    //request first page
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        //offline -> don't show the mesage "you have no groups"
        self.lbMessage.hidden=YES;
        
        [KSToastView ks_showToast: str(strYouHaveNoChasse) duration:2.0f completion: ^{
        }];
        
        return;
    }
    //dang search
    if (self.shouldBeginEditing) {
        [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
        return;
    }
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:@"0"];
    
    __weak GroupMesVC *wself = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        dispatch_async( dispatch_get_main_queue(), ^{
            [self stopRefreshControl];
            
            [COMMON removeProgressLoading];
            if (self.shouldBeginEditing) {
                return;
            }
            if (errCode == -1001)
            {
                //timeout
                [self checkEmpty:groupsActionArray msg:str(strAlertCheckEmptyGroupMes)];
                
                return;
            }
            else
            {
                
                if ([wself fnCheckResponse:response]) return ;
            }
            if (!response) {
                [self checkEmpty:groupsActionArray msg:EMPTY_GROUP_MES];
                
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                [self checkEmpty:groupsActionArray msg:EMPTY_GROUP_MES];
                return ;
            }
            NSArray*arrGroups = response[@"groups"];
            if ((int)arrGroups.count >= 0) {
                
                [groupsActionArray removeAllObjects];
                [groupsActionArray addObjectsFromArray:response[@"groups"] ];
                if (!self.shouldBeginEditing) {
                    groupsActionArrayTmp =[groupsActionArray mutableCopy];
                }
                [wself.tableControl reloadData];
                
                // Path to save array data
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_GROUP_MUR_SAVE)   ];
                // Write array
                [groupsActionArray writeToFile:strPath atomically:YES];
            }
            [self checkEmpty:groupsActionArray msg:EMPTY_GROUP_MES];
            
        });
        
    };
}

- (void)insertRowAtBottom {
    
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        //offline -> don't show the mesage "you have no groups"
        self.lbMessage.hidden=YES;
        
        [KSToastView ks_showToast: str(strYouHaveNoChasse) duration:2.0f completion: ^{
        }];
        return;
    }
    
    //dang search
    if (self.shouldBeginEditing) {
        NSString *offset =[NSString stringWithFormat:@"%ld",(long)[groupsActionArray count]];
        [self goLoungesSearchURL:toussearchBar.text offset:offset];
        return;
    }
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:[NSString stringWithFormat:@"%ld",(long)groupsActionArray.count]];
    
    __weak GroupMesVC *wself = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        [self stopRefreshControl];
        if (self.shouldBeginEditing) {
            return;
        }
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSArray*arrGroups = response[@"groups"];
        
        if (arrGroups.count > 0) {
            
            //check if exist... abort.
            
            for (NSDictionary*mDic in arrGroups) {
                if ([groupsActionArray containsObject:mDic]) {
                    continue;
                }
                //else
                [groupsActionArray addObject:mDic];
            }
            
            if (!self.shouldBeginEditing) {
                groupsActionArrayTmp =[groupsActionArray mutableCopy];
            }
            [wself.tableControl reloadData];
            
            // Path to save array data
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_GROUP_MUR_SAVE)   ];
            // Write array
            [groupsActionArray writeToFile:strPath atomically:YES];
        }
        
    };
}

-(void)updateGroupItem:(NSInteger)index
{
    [[GroupCreateOBJ sharedInstance] resetParams];
    
    NSDictionary *dic = [groupsActionArray objectAtIndex:index];
    
    NSString    *strPhotoUrl = dic[@"photo"];
    strPhotoUrl = [[strPhotoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSURL * url = [[NSURL alloc] initWithString:strPhotoUrl];
    
    //load image async
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            [GroupCreateOBJ sharedInstance].imgData =data;
        }
    }];
    [task resume];
    
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch ([dic[@"access"] integerValue]) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
        }
            break;
        default:
            break;
    }
    [GroupCreateOBJ sharedInstance].group_id =dic[@"id"];
    [GroupCreateOBJ sharedInstance].strName =dic[@"name"];
    [GroupCreateOBJ sharedInstance].strComment =dic[@"description"];
    [GroupCreateOBJ sharedInstance].accessKind =m_accessKind;
    [GroupCreateOBJ sharedInstance].isModifi =YES;
    groupEditListVC *viewController1 = [[groupEditListVC alloc] initWithNibName:@"groupEditListVC" bundle:nil];
    [self pushVC:viewController1 animate:YES];
    
}
- (void) goLoungesSearchURL:(NSString*)encodedString offset:(NSString*)offset
{
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    
    
    [serviceAPI fnGET_MES_GROUP_SEARCH:encodedString offset:offset];
    
    serviceAPI.onComplete=^(NSDictionary *response, int errCode)
    {
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        if ([response isKindOfClass:[NSDictionary class]]) {
            NSMutableArray *lougesArr = [response objectForKey:@"groups"];
            if (lougesArr) {
                if([lougesArr count]<=0 && [offset intValue]==0)
                {
                    [KSToastView ks_showToast: str(str_NO_GROUP_MATCH_SEARCH) duration:4.0f completion: ^{
                    }];
                    
                    return ;
                }
                if ([offset intValue]==0) {
                    [groupsActionArraySearch removeAllObjects];
                }
                for (NSDictionary *dicGroup in lougesArr) {
                    switch ([dicGroup[@"connected"][@"access"] intValue]) {
                        case USER_ADMIN:
                        case USER_NORMAL:
                        {
                            [groupsActionArraySearch addObject:dicGroup];
                        }
                            break;
                        default:
                            break;
                    }
                    
                }
                groupsActionArray = [groupsActionArraySearch mutableCopy];
            }
        }else{
            [KSToastView ks_showToast: str(str_NO_GROUP_MATCH_SEARCH) duration:4.0f completion: ^{
            }];
            
        }
        
        [self.tableControl reloadData];
    };
}
#pragma mark - Action
-(void)enterGroupMurAction:(id)sender
{
    //Expect target...
    
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    //admin
    if (tag < 2000) {
        index = tag - 1000;
    }
    //normal
    else {
        index = tag - 2000;
    }
    NSDictionary *dic = [groupsActionArray objectAtIndex:index];
    
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    
    viewController1.needRemoveSubItem = NO;
    
    //
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    
    [self pushVC:viewController1 animate:YES];
}

-(void)enterGroupInfoAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    index = tag - 3100;
    [self updateGroupItem:index];
}
-(void)enterGroupValidationAction:(id)sender
{
    NSInteger tag =((UIButton*)sender).tag;
    NSInteger index =0;
    index=tag-4000;
    NSDictionary *dic =[groupsActionArray objectAtIndex:index];
    NSString *nbpending=dic[@"nbPending"];
    if (nbpending.intValue>0) {
        GroupSettingMembres *viewController1 = [[GroupSettingMembres alloc] initWithNibName:@"GroupSettingMembres" bundle:nil];
        
        [self pushVC:viewController1 animate:YES];
        //
        NSDictionary *dic = [groupsActionArray objectAtIndex:index];
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    }
}

-(void)closeGroupAction:(NSInteger)index
{
    
    NSDictionary *dic = [groupsActionArray objectAtIndex:index];
    NSString *strContent = [ NSString stringWithFormat: str(strDelete_group_content), dic[@"name"] ];
    [UIAlertView showWithTitle:NSLocalizedString(str(strDelete_group_title), @"")
                       message:strContent
             cancelButtonTitle:str(strNon)
             otherButtonTitles:@[str(strOui)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                          }
                          else
                          {
                              
                              [COMMON addLoading:self];
                              
                              WebServiceAPI *serviceObj = [WebServiceAPI new];
                              [serviceObj deleteGroupAction: dic[@"id"]];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  
                                  if (!response) {
                                      return ;
                                  }
                                  
                                  if([response isKindOfClass: [NSArray class] ]) {
                                      return ;
                                  }
                                  
                                  //delete file...cache.
                                  NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
                                  
                                  NSString *catStr =  [NSString stringWithFormat:@"%@_%@_",[COMMON getUserId], strGroup_id];
                                  
                                  NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(catStr ,FILE_SETTING_GROUP_SAVE)];
                                  [FileHelper removeFileAtPath:strPath];
                                  
                                  //
                                  if ([response[@"success"] integerValue]==1) {
                                      
                                      [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                                      AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                      [app updateAllowShowAdd:response];
                                      
                                      [groupsActionArray removeObjectAtIndex:index];
                                      if (!self.shouldBeginEditing) {
                                          groupsActionArrayTmp =[groupsActionArray mutableCopy];
                                      }
                                      [self.tableControl reloadData];
                                      
                                      [self checkEmpty:groupsActionArray msg:EMPTY_GROUP_MES];
                                      
                                      [self moreRefreshControl];
                                  }
                              };
                          }
                      }];
}

-(void)doEmailNotify:(id)sender
{
    UIButton *btn = (UIButton*) sender;
    
    NSInteger index = 0;
    index = btn.tag - 4100;
    
    NSDictionary *dic = [groupsActionArray objectAtIndex:index];
    
    //if success...set NO;
    WebServiceAPI *serviceObj = [WebServiceAPI new];
    
    //    if (btn.isSelected) {
    //        [serviceObj putMailable: dic[@"id"] mailable:0];
    //    }else{
    //        [serviceObj putMailable: dic[@"id"] mailable:1];
    //    }
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if ([response[@"success"] integerValue]==1) {
            NSIndexPath *indexPath = [[NSIndexPath alloc]init];
            indexPath = [NSIndexPath indexPathForItem:0 inSection:index];
            GroupesViewBaseCell *cell = (GroupesViewBaseCell*)[self.tableControl cellForRowAtIndexPath:indexPath];
            [btn setSelected:!btn.isSelected];
            cell.checkbox.selected =btn.isSelected;
        }
    };
}
-(void)unSubscribeGroupAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    index = tag - 3000;
    NSDictionary *dic = [groupsActionArray objectAtIndex:index];
    
    NSString *strContent = [ NSString stringWithFormat: str(strLeave_group_content), dic[@"name"] ];
    
    [UIAlertView showWithTitle:str(strDesinscription_Du_Group)
                       message:strContent
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[str(strNon)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              
                              [COMMON addLoading:self];
                              
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj unsubscribeGroupAction:dic[@"connected"][@"user"][@"id"] :dic[@"id"]];
                              
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  
                                  [COMMON removeProgressLoading];
                                  if (!response) {
                                      return ;
                                  }
                                  if([response isKindOfClass: [NSArray class] ]) {
                                      return ;
                                  }
                                  else {
                                      if ([response[@"success"] integerValue]==1) {
                                          [groupsActionArray removeObjectAtIndex:index];
                                          if (!self.shouldBeginEditing) {
                                              groupsActionArrayTmp =[groupsActionArray mutableCopy];
                                          }
                                          [self.tableControl reloadData];
                                          [self moreRefreshControl];
                                      }
                                  }
                              };
                          }
                          else
                          {
                              
                          }
                      }];
}
//MENU popover
#pragma mark -
- (IBAction)showMenuPopOver:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    index = tag - 5000;
    //
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[GroupesViewBaseCell class]]) {
        parent = parent.superview;
    }
    
    GroupesViewBaseCell *cell = (GroupesViewBaseCell *)parent;
    // Hide already showing popover
    [cell show];
    //
    __weak GroupMesVC *wself =self;
    [cell setCallBackGroup:^(NSInteger ind)
     {
         if (ind==1) {
             [wself closeGroupAction:index];
         }
         else if(ind==0)
         {
             [self updateGroupItem:index];
         }
     }];
    
    
}

#pragma mark - TableView datasource & delegate

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupesViewBaseCell *cell=(GroupesViewBaseCell *) cellTmp;
    [self setDataForGroupCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

-(void) setDataForGroupCell:(GroupesViewBaseCell*) cell withIndex:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [groupsActionArray objectAtIndex:indexPath.section];
    
    int         accessType = [dic[@"access"] intValue];
    NSString *strDescription = [dic[@"description"] emo_emojiString];
    
    NSString *strName = [dic[@"name"] emo_emojiString];
    
    int         nbPending = [[[groupsActionArray objectAtIndex:indexPath.section] objectForKey:@"nbPending"] intValue];
    int         nbSubscribers = [[[groupsActionArray objectAtIndex:indexPath.section] objectForKey:@"nbSubscribers"] intValue];
    NSString    *strOwner = [NSString stringWithFormat:@"%@ %@",[[[groupsActionArray objectAtIndex:indexPath.section] valueForKey:@"owner"]valueForKey:@"firstname"],[[[groupsActionArray objectAtIndex:indexPath.section] valueForKey:@"owner"]valueForKey:@"lastname"]];
    
    NSString    *strPhotoUrl = [[groupsActionArray objectAtIndex:indexPath.section] objectForKey:@"photo"];
    
    NSDictionary *connectedDict = [[groupsActionArray objectAtIndex:indexPath.section] objectForKey:@"connected"];
    
    USER_KIND   m_userKind = USER_NORMAL;
    
    /*
     acess = 0: current user is invited
     1: request to access and pending to be validate by admin
     2: normal user
     3: admin
     */
    
    if (connectedDict.allKeys.count > 0)
    {
        switch ([connectedDict[@"access"] intValue]) {
            case 0:
            {
                m_userKind = USER_INVITED;
            }
                break;
            case 1:
            {
                m_userKind = USER_WAITING_APPROVE;
            }
                break;
            case 2:
            {
                m_userKind = USER_NORMAL;
            }
                break;
            case 3:
            {
                m_userKind = USER_ADMIN;
            }
                break;
            default:
                break;
        }
    }
    NSAttributedString *attStrAdminGroup;
    NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",str(strAdministre_par)]];
    NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Vous "];
    NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"et "];
    NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:strOwner];
    
    [str1 addAttribute:NSFontAttributeName
                 value:[UIFont systemFontOfSize:13.0f]
                 range:NSMakeRange(0, str1.length-1)];
    [str2 addAttribute:NSFontAttributeName
                 value:[UIFont boldSystemFontOfSize:13.0f]
                 range:NSMakeRange(0, str2.length-1)];
    [str3 addAttribute:NSFontAttributeName
                 value:[UIFont systemFontOfSize:13.0f]
                 range:NSMakeRange(0, str3.length-1)];
    [str4 addAttribute:NSFontAttributeName
                 value:[UIFont boldSystemFontOfSize:13.0f]
                 range:NSMakeRange(0, str4.length-1)];
    
    //(2:public, 1:semi, 0:protected)
    
    if (m_userKind == USER_ADMIN) {
        if ([dic[@"connected"][@"user"][@"id"] intValue] == [dic[@"owner"][@"id"] intValue]) {
            [str1 appendAttributedString:str4];
            
        }
        else
        {
            [str1 appendAttributedString:str2];
            [str1 appendAttributedString:str3];
            [str1 appendAttributedString:str4];
            
        }
    }
    //    else  {
    //        [str1 appendAttributedString:str4];
    //    }
    attStrAdminGroup = str1;
    
    if (m_userKind == USER_ADMIN)
    {
        //Enter
        cell.btnEnterGroup.tag = 1000 + indexPath.section;
        [cell.btnEnterGroup addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnLabel.tag = 1000 + indexPath.section;
        [cell.btnLabel addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //close if is Admin/un-subscribe if normal user.
        cell.btnCloseGroup.tag = 3000 + indexPath.section;
        [cell.btnCloseGroup addTarget:self action:@selector(closeGroupAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //btn validation
        cell.btnValidation.tag=4000+indexPath.section;
        [cell.btnValidation addTarget:self action:@selector(enterGroupValidationAction:) forControlEvents:UIControlEventTouchUpInside];
        //btn admin
        cell.btnAdminstrator.tag = 3100 + indexPath.section;
        [cell.btnAdminstrator addTarget:self action:@selector(enterGroupInfoAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //btn email notify
        cell.btnEmailNotify.tag = 4100+indexPath.section;
        [cell.btnEmailNotify addTarget:self action:@selector(doEmailNotify:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([ dic[@"connected"][@"mailable"] intValue] == 1) {
            [cell.checkbox setSelected:YES];
            [cell.btnEmailNotify setSelected:YES];
            
        }else{
            [cell.checkbox setSelected:NO];
            [cell.btnEmailNotify setSelected:NO];
            
        }
        [cell.checkbox setTypeCheckBox:UI_GROUP_MUR_ADMIN];
        [cell createMenuList:@[str(strAdministrer), str(strFermer_le_groupe)] ];
        
        cell.btnSetting.tag = 5000+indexPath.section;
        [cell.btnSetting addTarget:self action:@selector(showMenuPopOver:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lblAdminGroup.attributedText = attStrAdminGroup;
        
        cell.txtDescription.text = strDescription;
        [cell layoutIfNeeded];
        cell.lblName.text = strName;
        if (nbPending >0) {
            cell.lblNbPending.hidden=NO;
            cell.imgMember.hidden =NO;
            cell.btnValidation.hidden =NO;
        }
        else
        {
            cell.lblNbPending.hidden=YES;
            cell.imgMember.hidden =YES;
            cell.btnValidation.hidden =YES;
        }
        
        if ( nbPending == 0) {
            cell.lblNbPending.hidden = YES;
        }else if ( nbPending > 1) {
            [cell.lblNbPending setText:[NSString stringWithFormat:@"%d %@",nbPending,str(strPersonnes_en_attente_de_validation)]];
        }else{
            [cell.lblNbPending setText:[NSString stringWithFormat:@"%d %@",nbPending,str(strPersonne_en_attente_de_validation)]];
        }
        
        //can pressed on
        
        if (accessType == 0) {
            cell.lblAccess.text = str(strAccessPrivate);
        } else if (accessType == 1) {
            cell.lblAccess.text = str(strAccessSemiPrivate);
        } else if (accessType == 2) {
            cell.lblAccess.text = str(strAccessPublic);
        }
        
        strPhotoUrl = [[strPhotoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSURL * url = [[NSURL alloc] initWithString:strPhotoUrl];
        [cell.imgViewAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_avatar"]];
        [cell fnSettingCell:UI_GROUP_MUR_ADMIN];
        
    }
    else if (m_userKind == USER_NORMAL)
    {
        
        //Enter
        cell.btnEnterGroup.tag = 2000 + indexPath.section;;
        [cell.btnEnterGroup addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnLabel.tag = 2000 + indexPath.section;
        [cell.btnLabel addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //Leave / un-subscribe
        [cell.btnUnsubscribe addTarget:self action:@selector(unSubscribeGroupAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnUnsubscribe.tag = 3000 + indexPath.section;;
        
        //btn email notify
        cell.btnEmailNotify.tag = 4100+indexPath.section;
        [cell.btnEmailNotify addTarget:self action:@selector(doEmailNotify:) forControlEvents:UIControlEventTouchUpInside];
        [cell.checkbox setTypeCheckBox:UI_GROUP_MUR_ADMIN];
        if ([ dic[@"connected"][@"mailable"] intValue] == 1) {
            [cell.checkbox setSelected:YES];
            [cell.btnEmailNotify setSelected:YES];
            
        }else{
            [cell.checkbox setSelected:NO];
            [cell.btnEmailNotify setSelected:NO];
            
        }
        
        cell.lblAdminGroup.attributedText = attStrAdminGroup;
        cell.txtDescription.text = strDescription;
        [cell layoutIfNeeded];
        cell.lblName.text = strName;
        
        
        if (accessType == 0) {
            cell.lblAccess.text = str(strAccessPrivate);
        } else if (accessType == 1) {
            cell.lblAccess.text = str(strAccessSemiPrivate);
        } else if (accessType == 2) {
            cell.lblAccess.text = str(strAccessPublic);
        }
        strPhotoUrl = [[strPhotoUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSURL * url = [[NSURL alloc] initWithString:strPhotoUrl];
        
        [cell.imgViewAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_avatar"]];
        [cell fnSettingCell:UI_GROUP_MUR_NORMAL];
    }
    if (nbSubscribers ==1) {
        cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        
    }
    else
    {
        cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@s", nbSubscribers,str(strMMembre)];
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return groupsActionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *connectedDict = [[groupsActionArray objectAtIndex:indexPath.section] objectForKey:@"connected"];
    USER_KIND   m_userKind = USER_NORMAL;
    /*
     acess = 0: current user is invited
     1: request to access and pending to be validate by admin
     2: normal user
     3: admin
     */
    
    if (connectedDict.allKeys.count > 0)
    {
        if ([connectedDict[@"access"] intValue] ==3) {
            m_userKind = USER_ADMIN;
            
        }
        else
        {
            m_userKind = USER_NORMAL;
        }
    }
    if (m_userKind == USER_ADMIN)
    {
        GroupesViewCellAdmin *cell = (GroupesViewCellAdmin *)[tableView dequeueReusableCellWithIdentifier:GroupesCellID1 forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    } else if (m_userKind == USER_NORMAL)
    {
        GroupesViewCellNormal *cell = (GroupesViewCellNormal *)[tableView dequeueReusableCellWithIdentifier:GroupesCellID2 forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
}

- (IBAction)onMakePublication:(id)sender {
    [[GroupCreateOBJ sharedInstance] resetParams];
    GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
    
    [self pushVC:viewController1 animate:YES];
    
}

-(void)getItemWithKind:(NSString*)myid
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getItemWithKind:MYGROUP myid:myid];
    serviceAPI.onComplete=^(NSDictionary *response, int errCode)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        NSMutableDictionary *nsDic =[NSMutableDictionary dictionaryWithDictionary: response[@"group"]];
        for (int i=0; i<groupsActionArray.count; i++) {
            NSDictionary *dic = [groupsActionArray objectAtIndex:i];
            if ([nsDic[@"id"] isEqual:dic[@"id"]]) {
                [groupsActionArray replaceObjectAtIndex:i withObject:nsDic];
                if (!self.shouldBeginEditing) {
                    groupsActionArrayTmp =[groupsActionArray mutableCopy];
                }
                NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    };
}
#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    //    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.shouldBeginEditing = NO;
    groupsActionArray = [groupsActionArrayTmp mutableCopy];
    //    [self.searchDisplayController setActive:NO];
    [self.tableControl reloadData];
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar setShowsCancelButton:YES animated:YES];
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [COMMON addLoading:self];
    [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
    [theSearchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    [groupsActionArray removeAllObjects];
    [self.tableControl reloadData];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
//MARK: short cut
-(void) addShortCut
{
    __weak typeof(self) wself = self;
    self.btnShortCut.hidden = NO;
    self.viewShortCut = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:ISMUR];
    [self.viewShortCut setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         wself.btnShortCut.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_ADD_FIREND:
             {
                 AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_AGENDA:
             {
                 [[ChassesCreateOBJ sharedInstance] resetParams];
                 ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_GROUP:
             {
                 [[GroupCreateOBJ sharedInstance] resetParams];
                 GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:YES];
                 
             }
                 break;
             case SHORTCUT_ACTION_DISCUSTION:
             {
                 ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISDISCUSS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
                 viewController1.isFromSetting = YES;
                 [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_PUBLICATION:
             {
                 [[PublicationOBJ sharedInstance]  resetParams];
                 
                 SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCut.constraintBottomCloseButton.constant = 74;
    self.viewShortCut.constraintTraillingCloseButton.constant = 17;
    [self.viewShortCut addContraintSupview:self.view];
    self.viewShortCut.hidden = YES;
    [self.viewShortCut fnAllowAdd:YES];
}
-(IBAction)fnShortCut:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.btnShortCut.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        self.btnShortCut.hidden = YES;
        [self.viewShortCut hide:NO];
        [self returnRotation];
    }];
}

-(void) returnRotation{
    [UIView animateWithDuration:0.1 animations:^{
        self.btnShortCut.transform = CGAffineTransformIdentity;
    }];
}

@end
