//
//  PublicationOBJ.h
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
#import "BaseVC.h"
#import "AppCommon.h"
#import "UIAlertView+Blocks.h"
@interface PublicationOBJ : NSObject
+ (PublicationOBJ *) sharedInstance;

//GET using
@property (nonatomic, strong) NSArray *treeCategory;

//GET publication color
@property (nonatomic, strong) NSDictionary *dicColor;

//Public
@property(assign) MEDIATYPE mediaType;

@property(assign) BOOL enableGeolocation;
@property(assign) BOOL enableLandmark;

@property (nonatomic, strong) NSString *urlVideo;
@property (nonatomic, strong) NSString *urlPhoto;
@property (nonatomic, strong) NSArray *arrMultiplePhoto;

@property (nonatomic, strong) NSString *publicationTxt;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longtitude;
@property (nonatomic, strong) NSString *altitude;

@property (nonatomic, strong) NSString *address;


@property (nonatomic, strong) NSString *location;

@property (nonatomic, strong) NSString *legend;

@property (nonatomic, strong) NSString *groupID;

@property (nonatomic, strong) NSString *huntID;

@property (nonatomic, strong) NSMutableDictionary *observation;
@property (nonatomic, strong) NSString *observationID;

@property (nonatomic, strong) NSString *publicationID;

@property (nonatomic,assign) BOOL removeGeo;

@property (nonatomic,assign) BOOL isEditer;
@property (nonatomic,assign) int iShare;
@property (nonatomic,strong) NSDictionary *dicGeolocation;
@property (nonatomic,strong) NSArray *arrHunt;
@property (nonatomic,strong) NSArray *arrGroup;
@property (nonatomic,strong) NSArray *arrsharingUsers;
@property (nonatomic, strong) NSDictionary *dicMedia;

@property(nonatomic,weak) UIViewController *mParentVC;

@property (nonatomic,strong) NSMutableDictionary *dicEditer;
@property (nonatomic, strong) NSArray *sharingGroupsArray;
@property (nonatomic, strong) NSArray *sharingHuntsArray;
@property (nonatomic, strong) NSArray *sharingReceiversArray;
@property (nonatomic, strong) NSString *publicationcolor;
@property (nonatomic, strong) NSArray *arrReceivers;
@property (nonatomic, strong) NSDictionary *dicFavoris;
@property (nonatomic, strong) NSDictionary *dicPatarge;
@property (nonatomic,assign) BOOL isPostFavo;

@property(nonatomic,assign) BOOL isEditFavo;

@property(nonatomic,strong) NSMutableArray *arrCacheFavorites;


-(void) resetParams;
-(void)fnSetValueForKeyWithPublication:(NSDictionary*)dic;
-(void) uploadData :(NSDictionary *) dicSharing;
-(NSArray*) convertArray:(NSArray*)arr forGroup:(BOOL) isGroup;
-(void) modifiPublication:(UIViewController *)vc withType:(int) mType;
-(void)modifiPosterAll:(UIViewController*)viewcontroller;
-(BOOL) checkShowCategory:(id)arrGroupOfCategory;

-(NSDictionary*) getCache_Tree;
-(NSArray*) getCache_Cards;
-(NSArray*) getCacheSpecific_Cards;
-(void) doMergeTreeWithNew:(NSDictionary*) response;
-(void) doRefreshTree:(NSDictionary*) response;

@end
