//
//  CellSelectListnaturalive.h
//  Naturapass
//
//  Created by giangtu on 1/20/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CellSelectListnaturalive : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *mDate;

@end

NS_ASSUME_NONNULL_END
