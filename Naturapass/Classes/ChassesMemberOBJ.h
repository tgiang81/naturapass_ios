//
//  ChassesCreateOBJ.h
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
@interface ChassesMemberOBJ : NSObject
+ (ChassesMemberOBJ *) sharedInstance;
@property (nonatomic, assign) int fromScreen;
@property (nonatomic, strong) NSString *strMyKindId;
@property (nonatomic, strong) NSDictionary *dictionaryMyKind;
@property (nonatomic, assign) BOOL isNonMember;
@property (nonatomic, assign) BOOL isUserInvite;

@property (nonatomic,strong) NSString *strParticipe;

@property (nonatomic,strong)    NSString *strFirtName;
@property (nonatomic,strong)    NSString *strLastName;
@property (nonatomic,strong)    NSString *strCommentPublic;
@property (nonatomic,strong)    NSString *strComentPrive;
-(void) resetParams;
@end
