//
//  PublicationVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PublicationVC.h"
#import "Publication_GeolocationVC.h"
#import "PhotoSheetVC.h"
#import "VideoSheetVC.h"
#import "ImageWithCloseButton.h"
#import <AVFoundation/AVFoundation.h>
#import "PublicationOBJ.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "SAMTextView.h"
#import "MapDataDownloader.h"


#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "AppDelegate.h"
#import "Define.h"
#import "FileHelper.h"
#import "CommonHelper.h"

#import "Publication_DocumentPDF.h"
#import "Media_SheetView.h"
#import "MultipleChooseMediaCell.h"
static NSString *MultipleChooseID = @"MultipleChooseMediaCellID";

@interface PublicationVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet SAMTextView *textView;
    IBOutlet SAMTextView *textViewEdit;
    
    __weak IBOutlet UIButton *suivantBtn;
    NSMutableArray *arrMultipleMedia;
    __weak IBOutlet UILabel *numberRemain;

}
@property (strong, nonatomic)   IBOutlet UIScrollView *scrollviewKeyboard;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPhotoSelected;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewVideoSelected;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPDFSelected;

@property (weak, nonatomic)     IBOutlet UIView *viewPhoto;
@property (weak, nonatomic)     IBOutlet UIView *viewVideo;
@property (weak, nonatomic)     IBOutlet UIView *viewPDF;

@property (weak, nonatomic) IBOutlet UIButton *selectPhoto;
@property (weak, nonatomic) IBOutlet UIButton *selectVideo;
@property (weak, nonatomic) IBOutlet UIButton *selectPDF;

@property (weak, nonatomic)     IBOutlet UIView *viewCreateNews;
@property (weak, nonatomic)     IBOutlet UIView *viewModifi;
@property (weak, nonatomic)     IBOutlet UILabel *lbTitle;
@property (weak, nonatomic)     IBOutlet UIView *viewContentMedia;
@property (weak, nonatomic)     IBOutlet UIView *viewContentNamePdf;
@property (weak, nonatomic)     IBOutlet UIImageView *imgBackGroundNamePdf;
@property (weak, nonatomic)     IBOutlet UIImageView *imgButtonClosePdf;
@property (weak, nonatomic)     IBOutlet UILabel *lbNamePdf;
@property (weak, nonatomic)     IBOutlet UIButton *btnAddMultipleMedia;
@property (weak, nonatomic)     IBOutlet UIView *viewAddMorePhoto;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewContentMedia;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightAddMore;


@property(assign) MEDIATYPE mediaType;

@end

@implementation PublicationVC

-(void) reset2Views
{
    self.lbNamePdf.text = nil;

    self.viewPhotoSelected.hidden=YES;
    self.viewPhotoSelected.imageContent.image = nil;
    
    self.viewPhoto.userInteractionEnabled = YES;
    self.viewPhoto.alpha =1;
    
    self.viewVideoSelected.hidden=YES;
    self.viewVideoSelected.imageContent.image = nil;
    self.viewVideo.userInteractionEnabled = YES;
    self.viewVideo.alpha =1;
    
    self.viewPDFSelected.hidden=YES;
    self.viewPDFSelected.imageContent.image = nil;
    self.viewPDF.userInteractionEnabled = YES;
    self.viewPDF.alpha =1;
    
    
    
    
    self.mediaType = TYPE_MESSAGE;
    
    //[PublicationOBJ sharedInstance].urlPhoto = nil;
    [PublicationOBJ sharedInstance].arrMultiplePhoto = nil;
    [PublicationOBJ sharedInstance].urlVideo = nil;
    [self fnShowViewNamePdfWithMediaType:self.mediaType];

}

-(void)isEditer
{
    if ([PublicationOBJ sharedInstance].isEditer) {
        [suivantBtn setTitle:str(strValider) forState:UIControlStateNormal ];
        
        textViewEdit.text = [PublicationOBJ sharedInstance].publicationTxt;
        self.viewPhotoSelected.hidden=YES;
        self.viewPhotoSelected.imageContent.image = nil;
        
        self.viewPhoto.userInteractionEnabled = NO;
        self.viewPhoto.alpha =1;
        
        self.viewVideoSelected.hidden=YES;
        self.viewVideoSelected.imageContent.image = nil;
        
        self.viewVideo.userInteractionEnabled = NO;
        self.viewVideo.alpha =1;
        self.selectPhoto.enabled =NO;
        self.selectVideo.enabled =NO;
        
        
        
        self.viewPDFSelected.hidden=YES;
        self.viewPDFSelected.imageContent.image = nil;
        
        self.viewPDF.userInteractionEnabled = NO;
        self.viewPDF.alpha =1;
        self.selectPDF.enabled =NO;
        self.selectPDF.enabled =NO;
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.lbTitle.text = str(strPUBLIER_UN_MESSAGE);
    self.imgBackGroundNamePdf.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    self.imgButtonClosePdf.backgroundColor = UIColorFromRGB(MUR_CANCEL);
//    self.btnAddMultipleMedia.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
//
//    [self.tableControl registerNib:[UINib nibWithNibName:@"MultipleChooseMediaCell" bundle:nil] forCellReuseIdentifier:MultipleChooseID];
//    self.tableControl.estimatedRowHeight = 50;
//    arrMultipleMedia = [NSMutableArray new];
    [PublicationOBJ sharedInstance].isEditFavo= NO;
    if ([PublicationOBJ sharedInstance].isEditer) {
        self.viewCreateNews.hidden =YES;
        self.viewModifi.hidden =NO;
    }
    else
    {
        self.viewCreateNews.hidden =NO;
        self.viewModifi.hidden =YES;
    }
    //Preload Category...
    
    // Do any additional setup after loading the view     WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    //No message content -> disable next
    //    suivantBtn.enabled = NO;
    //    suivantBtn.alpha = 0.4;
    
    textView.placeholder = str(strTapez_votre_texte);
    //modifi
    textViewEdit.placeholder = str(strTapez_votre_texte);
    
    
    //Photo
    [self.viewPhotoSelected setMyCallback: ^(){
        //Close
        [self reset2Views];
    }];
    
    //Video
    [self.viewVideoSelected setMyCallback: ^(){
        //Close
        [self reset2Views];
    }];
    
    //PDF
    [self.viewPDFSelected setMyCallback: ^(){
        //Close
        [self reset2Views];
    }];
    
    
    [self reset2Views];
    
    //Add toolbar
    [self InitializeKeyboardToolBar];
    [self isEditer];
    [textView setInputAccessoryView:self.keyboardToolbar];
    //modifi
    [textViewEdit setInputAccessoryView:self.keyboardToolbar];
    
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
    
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group, kSQL_hunt, kSQL_agenda]];
    [self fnShowViewNamePdfWithMediaType:self.mediaType];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //    [self.navigationController.navigationBar layoutIfNeeded];
    
    //Auto...type...
    if (self.autoShow == TYPE_PHOTO) {
        [self onClickPhoto:nil];
        
    }else if (self.autoShow == TYPE_VIDEO) {
        [self onClickVideo:nil];
    }
    if (_dicShareExtention != nil) {
        [self imageShareExtentionWithInfo:_dicShareExtention];
    }
    
}

#pragma mark  -  PICKER DELEGATE


- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType type:(CFStringRef)mType
{
    //iPhone
    UIImagePickerController *imagePicker;
    imagePicker = [[UIImagePickerController alloc] init];
    
    
    if (mType == kUTTypeMovie) {
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    }else{
        imagePicker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
    }
    
    imagePicker.delegate = (id)self;
    imagePicker.sourceType = sourceType;
    
    [self presentViewController:imagePicker animated:NO completion:Nil];
    
}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}

- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
    //    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data
    
    
    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CLLocation * loc = app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        
        
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    self.autoShow = TYPE_RESET;
    
    if([info objectForKey:@"UIImagePickerControllerOriginalImage"])
    {
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            UIImage *tmpimage = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            NSString *strName = [CommonHelper  generatorString];
            
            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
            NSData *imageData = UIImageJPEGRepresentation(tmpimage,1);
            [imageData writeToFile:photoFile atomically:YES];
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
            {
                self.mediaType = TYPE_PHOTO;
                
//                [self saveImage:tmpimage withInfo:info];
                
                AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                CLLocation * loc = app.locationManager.location;
                
                NSDictionary *retDic = nil;
                
                if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
                    retDic = @{@"image":strName,
                               @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                               @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
                }else{
                    retDic =@{@"image":strName};
                }
                
                
                NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:retDic[@"image"]];
                UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
                
                self.viewPhotoSelected.imageContent.image = imgData;
                self.viewPhotoSelected.hidden = NO;
                
                [PublicationOBJ sharedInstance].urlPhoto = retDic[@"image"];
                if (retDic[@"Latitude"] != nil) {
                    [PublicationOBJ sharedInstance].latitude = [retDic[@"Latitude"] stringValue];
                    [PublicationOBJ sharedInstance].longtitude = [retDic[@"Longitude"] stringValue];
                }else{
                    [PublicationOBJ sharedInstance].latitude = nil;
                    [PublicationOBJ sharedInstance].longtitude = nil;
                    
                }
                
                //Disable Video
                self.viewVideoSelected.hidden=YES;
                self.viewVideo.userInteractionEnabled = NO;
                self.viewVideo.alpha =0.4;
                
                //PDF
                self.viewPDFSelected.hidden=YES;
                self.viewPDF.userInteractionEnabled = NO;
                self.viewPDF.alpha =0.4;
                
                
                
            }
        }
    } else {
        
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [videoUrl path];
            
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                
                self.mediaType = TYPE_VIDEO;
                
                [PublicationOBJ sharedInstance].urlVideo = moviePath;
                
                AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: [NSURL fileURLWithPath: moviePath ] options:nil];
                AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
                generate1.appliesPreferredTrackTransform = YES;
                NSError *err = NULL;
                CMTime time = CMTimeMake(1, 2);
                CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
                UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
                
                self.viewVideoSelected.imageContent.image = one;
                self.viewVideoSelected.hidden = NO;
                
                //Disable Photo
                self.viewPhotoSelected.hidden=YES;
                self.viewPhoto.userInteractionEnabled = NO;
                self.viewPhoto.alpha =0.4;
                
                //PDF
                self.viewPDFSelected.hidden=YES;
                self.viewPDF.userInteractionEnabled = NO;
                self.viewPDF.alpha =0.4;
                
                
            }
        }
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    self.autoShow = TYPE_RESET;
    
    [picker dismissViewControllerAnimated: YES completion: NULL];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - SELECT MEDIA

- (IBAction)onClickPhoto:(id)sender {
    
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_PHOTO withParent:self target:ISMUR];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_PHOTO;
            
            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
            UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
            
            self.viewPhotoSelected.imageContent.image = imgData;
            self.viewPhotoSelected.hidden = NO;
            
            [PublicationOBJ sharedInstance].urlPhoto = data[@"image"];
            if (data[@"Latitude"] != nil) {
                [PublicationOBJ sharedInstance].latitude = [data[@"Latitude"] stringValue];
                [PublicationOBJ sharedInstance].longtitude = [data[@"Longitude"] stringValue];
            }else{
                [PublicationOBJ sharedInstance].latitude = nil;
                [PublicationOBJ sharedInstance].longtitude = nil;
            }
            
            //        data[@"Latitude"];
            //
            //        data[@"Longitude"];
            
            //Disable Video
            self.viewVideoSelected.hidden=YES;
            self.viewVideo.userInteractionEnabled = NO;
            self.viewVideo.alpha =0.4;
            
            //pdf
            self.viewPDFSelected.hidden=YES;
            self.viewPDF.userInteractionEnabled = NO;
            self.viewPDF.alpha =0.4;
            
//            [self fnShowViewNamePdfWithMediaType:self.mediaType];
//            [self dataMultipleMediaWithArray:data[@"photo"]];
        }
    }];
    
    if (self.autoShow == TYPE_PHOTO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    
    self.autoShow = TYPE_RESET;

}

- (IBAction)onClickVideo:(id)sender {
    Media_SheetView *vc = [[Media_SheetView alloc] initWithType:TYPE_VIDEO withParent:self target:ISMUR];
    
    [vc doBlock:^(NSDictionary*data){
        if (data) {
            self.mediaType = TYPE_VIDEO;
                        
            [PublicationOBJ sharedInstance].urlVideo = data[@"videourl"];
            
            AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: [NSURL fileURLWithPath: data[@"videourl"] ] options:nil];
            AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
            generate1.appliesPreferredTrackTransform = YES;
            NSError *err = NULL;
            CMTime time = CMTimeMake(1, 2);
            CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
            UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
            
            self.viewVideoSelected.imageContent.image = one;
            self.viewVideoSelected.hidden = NO;
            
            //Disable Photo
            self.viewPhotoSelected.hidden=YES;
            self.viewPhoto.userInteractionEnabled = NO;
            self.viewPhoto.alpha =0.4;
            
            //pdf
            self.viewPDFSelected.hidden=YES;
            self.viewPDF.userInteractionEnabled = NO;
            self.viewPDF.alpha =0.4;
            
        }
    }];
    if (self.autoShow == TYPE_VIDEO) {
        [vc autoShowCamera];
    }
    else
    {
        [vc showAlert];
    }
    self.autoShow = TYPE_RESET;
}

/*
 fileName = "testPDF.pdf";
 filePath = "/var/mobile/Containers/Data/Application/2BB2E165-D6FA-48DA-BAE4-2AB2CED5097C/Library/Naturapass/NATURA_PDF_INBOX/testPDF.pdf";
 type = 2;
 */

- (IBAction)onClickPDF:(id)sender {
    self.autoShow = TYPE_RESET;
    
    //only allow select 1 pdf.
    
    Publication_DocumentPDF *viewController1 = [[Publication_DocumentPDF alloc] initWithNibName:@"Publication_DocumentPDF" bundle:nil];
    [self pushVC:viewController1 animate:YES];
    [viewController1 doBlock:^(NSDictionary *objPdf) {
        self.mediaType = TYPE_PDF;
        
        //Thumb image
        NSURL* pdfFileUrl = [NSURL fileURLWithPath: objPdf[@"filePath"]];
        CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
        
        self.viewPDFSelected.imageContent.image = [[CommonHelper sharedInstance] buildThumbnailImage:pdf];
        self.viewPDFSelected.hidden = NO;
        
        //Disable Video
        self.viewVideoSelected.hidden=YES;
        self.viewVideo.userInteractionEnabled = NO;
        self.viewVideo.alpha =0.4;
        
        //Photo
        self.viewPhotoSelected.hidden=YES;
        self.viewPhoto.userInteractionEnabled = NO;
        self.viewPhoto.alpha =0.4;
        
        self.lbNamePdf.text = objPdf[@"fileName"];
        
        [self fnShowViewNamePdfWithMediaType:self.mediaType];
//        [arrMultipleMedia addObjectsFromArray:@[@{@"name":objPdf[@"fileName"]}]];
//        [PublicationOBJ sharedInstance].arrMultiplePhoto = arrMultipleMedia;
//        [self.tableControl reloadData];
    }];
    
}
- (IBAction)onClickCloseViewNamePdf:(id)sender {
    [self reset2Views];
}
-(void)fnShowViewNamePdfWithMediaType:(MEDIATYPE )mediaType
{
    if (mediaType == TYPE_PDF) {
        self.viewContentMedia.hidden = YES;
        self.viewContentNamePdf.hidden = NO;
        self.constraintHeightViewContentMedia.constant = 80;
    }
    else
    {
        self.viewContentMedia.hidden = NO;
        self.viewContentNamePdf.hidden = YES;
        self.constraintHeightViewContentMedia.constant = 175;
    }


}
- (void)textViewDidBeginEditing:(UITextView *)textView {
    //        [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 100) animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)m_textView {
    //    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    //Validate next step
    
    //    if ((int) textView.text.length > 0) {
    //            suivantBtn.alpha = 1;
    //            suivantBtn.enabled = YES;
    //    }else{
    //            suivantBtn.enabled = NO;
    //            suivantBtn.alpha = 0.4;
    //    }
}
-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    int remain = (int) (limitPublication - (a.text.length + c.length));
    if (remain < 0) {
        NSString *strFullText = [a.text stringByAppendingString:c];
        NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitPublication)];
        a.text = subString;
    }
    [numberRemain setText:[NSString stringWithFormat:@"(%d)", remain]];
    
    // Check if the count is over the limit
    if(remain < 0) {
        // Change the color
        [numberRemain setTextColor:[UIColor redColor]];
    }
    else if(remain < 10) {
        // Change the color to yellow
        [numberRemain setTextColor:[UIColor orangeColor]];
    }
    else {
        // Set normal color
        [numberRemain setTextColor:[UIColor darkGrayColor]];
    }
    return remain >= 0;
}
- (void)resignKeyboard:(id)sender
{
    [textView resignFirstResponder];
    //modifi
    [textViewEdit resignFirstResponder];
    
    
}

- (IBAction)onNext:(id)sender {
    [PublicationOBJ sharedInstance].mediaType = self.mediaType;
    
    
    //editer
    if ([PublicationOBJ sharedInstance].isEditer) {
        
        if([textViewEdit.text length] <= 20000){
            
            [PublicationOBJ sharedInstance].publicationTxt = textViewEdit.text;
        }
        
        [[PublicationOBJ sharedInstance]  modifiPublication:self withType:EDIT_TEXT];
    }
    else
    {
        if([textView.text length] <= 20000){
            
            [PublicationOBJ sharedInstance].publicationTxt = textView.text;
        }
        //create
        Publication_GeolocationVC *viewController1 = [[Publication_GeolocationVC alloc] initWithNibName:@"Publication_GeolocationVC" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    }
}

//MARK: - share extentions
- (void) imageShareExtentionWithInfo:(NSDictionary*)dicInfo
{
    NSString *contentImage = [NSString stringWithFormat:@"%@",@"image"];
    if ([dicInfo[@"type"] isEqualToString:contentImage])
    {
        self.mediaType = TYPE_PHOTO;
        NSString *strName = dicInfo[@"url"];
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
        
        NSURL *url = [NSURL fileURLWithPath:photoFile];
        NSData *dataInfo = [NSData dataWithContentsOfURL:url];
        
        UIImage *tmpimage = [UIImage imageWithData:dataInfo];
        
        //        [self saveImage:tmpimage withInfo:nil];
        
        AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        CLLocation * loc = app.locationManager.location;
        
        NSDictionary *retDic = nil;
        
        if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
            retDic = @{@"image":strName,
                       @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                       @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
        }else{
            retDic =@{@"image":strName};
        }
        
        
        //        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:retDic[@"image"]];
        //        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.viewPhotoSelected.imageContent.image = tmpimage;
        self.viewPhotoSelected.hidden = NO;
        
        [PublicationOBJ sharedInstance].urlPhoto = retDic[@"image"];
        if (retDic[@"Latitude"] != nil) {
            [PublicationOBJ sharedInstance].latitude = [retDic[@"Latitude"] stringValue];
            [PublicationOBJ sharedInstance].longtitude = [retDic[@"Longitude"] stringValue];
        }else{
            [PublicationOBJ sharedInstance].latitude = nil;
            [PublicationOBJ sharedInstance].longtitude = nil;
            
        }
        //Disable Video
        self.viewVideoSelected.hidden=YES;
        self.viewVideo.userInteractionEnabled = NO;
        self.viewVideo.alpha =0.4;
        
        //pdf
        self.viewPDFSelected.hidden=YES;
        self.viewPDF.userInteractionEnabled = NO;
        self.viewPDF.alpha =0.4;
        
    }
    else {
        
        self.mediaType = TYPE_VIDEO;
        NSString *moviePath = dicInfo[@"url"];
        
        [PublicationOBJ sharedInstance].urlVideo = moviePath;
        
        AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: [NSURL fileURLWithPath: moviePath ] options:nil];
        AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
        generate1.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 2);
        CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
        UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
        
        self.viewVideoSelected.imageContent.image = one;
        self.viewVideoSelected.hidden = NO;
        
        //Disable Photo
        self.viewPhotoSelected.hidden=YES;
        self.viewPhoto.userInteractionEnabled = NO;
        self.viewPhoto.alpha =0.4;
        
        //pdf
        self.viewPDFSelected.hidden=YES;
        self.viewPDF.userInteractionEnabled = NO;
        self.viewPDF.alpha =0.4;
        
    }
}

@end
