//
//  ViewFooter.m
//  DoTest
//
//  Created by GS-Soft on 6/6/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import "ViewFooter.h"
#import "FooterCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Config.h"
static NSString *identifierCard = @"FooterCellID";
static NSString *tableCell = @"FooterCell";

#define heightTitleCell 35
#define heightImage 40

@implementation ViewFooter

-(void)awakeFromNib{
    [super awakeFromNib];

    [self.tableControl registerNib:[UINib nibWithNibName:tableCell bundle:nil] forCellReuseIdentifier:identifierCard];
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
}
-(void) reloadWithData:(NSArray*)arr withBlock:(returnHight)bl
{
    if (self.isObserving == NO) {
        [self.tableControl addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionOld context:NULL];
        self.isObserving = YES;
    }
    
    
    self.block = bl;
    
    if (!arrFetchData) {
        arrFetchData = [NSMutableArray new];
    }
    [arrFetchData removeAllObjects];
    [arrFetchData addObjectsFromArray:arr];

        [self.tableControl reloadData];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary  *)change context:(void *)context
{
    //Whatever you do here when the reloadData finished
    float newHeight = self.tableControl.contentSize.height;
    if (newHeight > 0) {
        self.isObserving = NO;
        
        [self.tableControl removeObserver:self forKeyPath:@"contentSize" context:NULL];
        CGRect rect= self.tableControl.frame;
        rect.size.height=newHeight;
        self.tableControl.frame=rect;
        self.block(newHeight+heightTitleCell );

    }
}

-(void) releaseObserver
{
    if (self.isObserving) {
        self.isObserving = NO;
        [self.tableControl removeObserver:self forKeyPath:@"contentSize" context:NULL];

    }
}

-(void)dealloc
{
    if (self.isObserving) {
        [self.tableControl removeObserver:self forKeyPath:@"contentSize" context:NULL];
        
    }
}
///
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrFetchData count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 70;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FooterCell *cell = nil;
    cell = (FooterCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierCard forIndexPath:indexPath];
    cell.backgroundColor=[UIColor whiteColor];
    NSDictionary *dic = arrFetchData[indexPath.row];
    UIImage *image = dic[@"image"];
    UIImage *tempImg = [self fnResizeFixHeight:image :heightImage];

    cell.imgBrand.image = tempImg;
    cell.constraintWidthImage.constant =tempImg.size.width+5;
    [cell.label setText:dic[@"name"]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(UIImage*) fnResizeFixHeight :(UIImage*)img :(float)hHeight
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixHeight = hHeight;
    
    imgRatio = fixHeight / actualHeight;
    actualWidth = imgRatio * actualWidth;
    actualHeight= fixHeight;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}
@end
