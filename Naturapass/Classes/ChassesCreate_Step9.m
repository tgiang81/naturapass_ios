//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step9.h"
#import "ChassesCreate_Step11.h"
#import "TypeCell31.h"
#import "Typecell51.h"
#import "TypeCell61.h"

static NSString *typecell31 =@"TypeCell31";
static NSString *typecell31ID =@"TypeCell31ID";

static NSString *typecell51 =@"TypeCell51";
static NSString *typecell51ID =@"TypeCell51ID";

static NSString *typecell61 =@"TypeCell61";
static NSString *typecell61ID =@"TypeCell61ID";
@interface ChassesCreate_Step9 ()
{
    NSMutableArray *arr;
    NSArray        *arrImg;
    NSMutableArray *arr1;
    NSArray        *arrImg1;
}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@end

@implementation ChassesCreate_Step9
#pragma mark -setColor
-(void)fnColorTheme
{
    _label1.text =str(strTypeDePointGeo);
    _label2.text =str(strType_point_geolocaliser_que_vous_souhaitez_importer);
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self fnColorTheme];
    [self initialization];
    self.needChangeMessageAlert = YES;

}
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell31 bundle:nil] forCellReuseIdentifier:typecell31ID];
    [self.tableControl registerNib:[UINib nibWithNibName:typecell51 bundle:nil] forCellReuseIdentifier:typecell51ID];
    [self.tableControl registerNib:[UINib nibWithNibName:typecell61 bundle:nil] forCellReuseIdentifier:typecell61ID];
    arr = [NSMutableArray arrayWithObjects: str(strVivant),
           str(strTraces_empreintes),
           str(strBlesses),
           nil];
    arrImg =@[@"pig",@"pig",@"pig"];
    
    arr1 = [NSMutableArray arrayWithObjects: str(strVivant),
           str(strTraces_empreintes),
           str(strBlesses),
           nil];
    arrImg1 =@[@"pig",@"pig",@"pig"];
}

#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section ==0) {
        return arr.count;

    }
    else
    {
        return arr1.count;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0;
    }
    else
    {
        return 50;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return nil;
    }
    else
    {
        TypeCell51 *cell = (TypeCell51 *)[tableView dequeueReusableCellWithIdentifier:typecell51ID];
        cell.label1.text =str(strSelectionner_une_partie_de_cette_specification);
        cell.view1.backgroundColor =[UIColor clearColor];
        return cell;
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section==0) {
        TypeCell31 *cell = (TypeCell31 *)[tableView dequeueReusableCellWithIdentifier:typecell31ID];
        cell.img1.image=  [UIImage imageNamed:arrImg[indexPath.row]];
        cell.label1.text =arr[indexPath.row];
        cell.button1.tag =indexPath.row;
        [cell.button1 addTarget:self action:@selector(choosesInvite:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else
    {
        TypeCell61 *cell = (TypeCell61 *)[tableView dequeueReusableCellWithIdentifier:typecell61ID];
        cell.view1.backgroundColor =[UIColor clearColor];
        cell.img1.image=  [UIImage imageNamed:arrImg1[indexPath.row]];
        cell.label1.text =arr1[indexPath.row];
        cell.button1.tag =indexPath.row;
//        [cell.button1 addTarget:self action:@selector(choosesInvite:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(IBAction)choosesInvite:(id)sender
{
    NSInteger index =[sender tag];
    switch (index) {
        case 0:
        {
            
        }
            break;
        case 1:
        {

        }
            break;
        case 2:
        {
        }
        case 3:
        {
            
        }
            break;
        default:
            break;
    }
    
}
#pragma mark - onNext
- (IBAction)onNext:(id)sender {
    ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    
}
@end
