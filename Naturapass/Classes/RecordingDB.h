//
//  RecordingDB.h
//  Naturapass
//
//  Created by giangtu on 12/15/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMotion/CoreMotion.h>

@interface RecordingDB : NSObject

@property (strong, nonatomic) NSString*  m_id;
@property (strong, nonatomic) NSString*  userId;
@property (strong, nonatomic) NSString*  agendaId;
@property (strong, nonatomic) NSString*  startTime;

@property (strong, nonatomic) NSString*  endTime;
@property (strong, nonatomic) NSString*  status;

@property (strong, nonatomic) CMPedometerData * myPedoMeterData;

@property (strong, nonatomic)  NSDate* startDate;

+ (RecordingDB *) sharedInstance;

-(void) initPedometer;

+(void) addWithAgenda:(NSString*) agendaId
   startTime:(NSString*) startTime
   endTime:(NSString*) endTime
     distance:(float) distance
      average: (float) average;

+(BOOL) ifDataFoAgendaExist:(NSString*) agendaId;

+(void) removeWithAgenda:(NSString*) agendaId;
+(NSDictionary*) getSumaryWithAgenda:(NSString*) agendaId;



+(void) startPedometerUpdates;
+(void) stopPedometerUpdates;

+(void) queryPedometerFromDate: (NSDate*) stDate;

//
+(void) addAltitudeWithAgenda:(NSString*) agendaId andValue:(float) mValueAltitude;
+(void) removeTrackingWithAgenda:(NSString*) agendaId;

@end
