//
//  ResearchVC.m
//  Naturapass
//
//  Created by Giang on 8/5/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ResearchVC.h"
#import "CellKind2.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"

#import "CommonHelper.h"

#import "CellKind12.h"
#import "AppCommon.h"
#import "LikeListCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "CCMPopupTransitioning.h"
#import "SubNavigation_PRE_General.h"
#import "MurVC.h"
static NSString *identifier = @"CellKind12ID";



//static NSString *identifierSection1 = @"MyTableViewCell1";
@interface ResearchVC ()<UISearchBarDelegate>
{
    IBOutlet UIView *vHeader;
    NSMutableArray                  *searchedArray;
    NSMutableArray                  *searchedArrayGroup;
    NSMutableArray                  *searchedArrayAgenda;

    IBOutlet UISearchBar                *toussearchBar;
    WebServiceAPI *serviceAPI;
    UIImage *imgAdd;
    NSString* sender_id;
}
@property (nonatomic, assign) BOOL shouldBeginEditing;

@end

@implementation ResearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    toussearchBar.placeholder = str(strRechercher);
    
    // Do any additional setup after loading the view.
//    [self fnSetColor];
    [self addMainNav:@"MainNavSignaler"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];

    //Change background color
//    subview.backgroundColor = _colorNavigation;
    subview.imgBackground.image = [UIImage imageNamed:@"bg_nav_mur_v31"];
    subview.imgClose.image = nil;
    subview.btnClose.hidden = YES;
    
    [self addSubNav:nil];
    
//    self.vContainer.backgroundColor = _colorBackGround;

    self.vContainer.backgroundColor = UIColorFromRGB(0x1B1F26);
    self.view.backgroundColor = UIColorFromRGB(0x1B1F26);
    
    self.showTabBottom = YES;

    serviceAPI = [WebServiceAPI new];
    
    __weak ResearchVC *weakVC = self;
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        [weakVC performSelectorOnMainThread:@selector(reloadNewData:) withObject:response waitUntilDone:YES];

    };

    NSString *strImg =@"";

    strImg=@"mur_new_addfriend";

    imgAdd =[UIImage imageNamed:strImg];

    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];

    
    searchedArray= [NSMutableArray new];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"LikeListCell" bundle:nil] forCellReuseIdentifier:identifier];
    [self.tableControl setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    MainNavigationBaseView *subview =  [self getSubMainView];
    
    [subview.myTitle setText:@"RECHERCHER UNE PERSONNE"];
    [subview.myDesc setText:@""];
    
    [toussearchBar setTintColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
    
}

-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://HOME
        {
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
        default:
            break;
    }
    
}
-(void)killTextFocus {
    
    [toussearchBar resignFirstResponder];
}

-(void)reloadNewData:(id) response
{
    [searchedArray removeAllObjects];
    [searchedArray addObjectsFromArray:[response objectForKey:@"users"]];
    [self.tableControl reloadData];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}


#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self GetUsersSearchAction:toussearchBar.text];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.shouldBeginEditing = NO;
    [self.searchDisplayController setActive:NO];
    [self.tableControl reloadData];
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [self GetUsersSearchAction:toussearchBar.text];
    [theSearchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    [searchedArray removeAllObjects];
    [self.tableControl reloadData];
}

#pragma mark -API
- (void) GetUsersSearchAction:(NSString*)searchString
{
    if (searchString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];

    }
    else
    {
        [COMMON removeProgressLoading];        
        [COMMON addLoading:self];
        [serviceAPI getUsersSearchAction:searchString];
    }

    
}
-(IBAction)friendButtonAction:(UIButton *)sender {
    [self.searchDisplayController.searchBar isFirstResponder];
    [COMMON addLoading:self];
//    NSDictionary * postDict = [NSDictionary dictionaryWithObjectsAndKeys:@1,@"type", nil];
    NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:searchedArray[[sender tag]]];
    
    NSString *strId=searchedArray[[sender tag]][@"id"];
    
    WebServiceAPI *serviceAPIX =[ WebServiceAPI new];
    
    [serviceAPIX postUserFriendShipAction:strId];
    serviceAPIX.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        alert.tag = 50;
        [alert show];
        
        if (mulDic[@"state"]!=nil) {
            [mulDic removeObjectForKey:@"state"];
        }
        [mulDic setValue:@(1) forKey:@"state"];
        
        [searchedArray replaceObjectAtIndex:[sender tag] withObject:mulDic];
        [self.tableControl reloadData];
    };
}
#pragma mark -  TableView Delegates
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 136;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    UIView* sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
//    sectionView.backgroundColor= UIColorFromRGB(MAIN_COLOR);
//    UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 320, 29)];
//    [headerLabel setTextColor:[UIColor blackColor]];
//    headerLabel.numberOfLines=2;
//    [headerLabel setBackgroundColor:[UIColor clearColor]];
//    [headerLabel setFont:[UIFont boldSystemFontOfSize:17]];
//
//
//    [sectionView addSubview:headerLabel];
//
    vHeader.frame = [[UIScreen mainScreen] bounds ];
    return  vHeader;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchedArray.count;
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LikeListCell *cell = nil;
    
    cell = (LikeListCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = searchedArray[indexPath.row];
    cell.lbTitle.text = dic[@"fullname"];
    //image
    NSString *imgUrl;
    //profilepicture
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:imgUrl]];
    [cell.imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    [cell layoutIfNeeded];
    
    //Arrow
    // neu la normal thi an nut add friend con neu la admin thic check da co moi quan he thi an con khong thi show ra cho nguoi dung click
    NSInteger mutualFriend = 0;
    BOOL isAddFriendHide =YES;

    if ([dic[@"id"] integerValue] ==[sender_id integerValue]) {
        isAddFriendHide= YES;
    }
    else
    {
        /*
         {
         firstname = Mathis;
         friendship = 0;
         fullname = "Mathis Foulquier";
         id = 9036;
         lastname = Foulquier;
         mutualFriends = 0;
         profilepicture = "/uploads/users/images/thumb/ab95637ca784e484ca790448e1e67cb3370b2b5c.jpeg";
         state = 0;
         usertag = "mathis-foulquier";
         }
         */
            if ([dic[@"friendship"] isKindOfClass:[NSDictionary class]])
            {
                isAddFriendHide =YES;
            }else{
                isAddFriendHide=NO;
            }
            mutualFriend = [dic[@"mutualFriends"] integerValue];
    }
    
    //    3 amis en commun
    //    If you have common friends, it displays “n amis en commun” (without ‘s’ at the end of “commun” it’s design error).
    //    if there is only 1 common friends then text is “1 ami en commun.
    
    if (mutualFriend >0)
    {
        if (mutualFriend > 1) {
            cell.lbDesc.text = [NSString stringWithFormat:@"%ld amis en commun",mutualFriend];
        }else{
            cell.lbDesc.text = [NSString stringWithFormat:@"1 ami en commun"];
        }
    }
    else
    {
        cell.lbDesc.text = @"";
    }
    
    if (isAddFriendHide) {
        cell.btnAddFriend.hidden =YES;
        cell.imgAddFriend.hidden=YES;
        [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        cell.btnAddFriend.hidden=NO;
        cell.imgAddFriend.hidden=NO;
        [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if([dic[@"state"]integerValue] ==0 || [dic[@"state"]integerValue] == 3)
    {
        cell.imgAddFriend.image =imgAdd;
    }
    else
    {
        cell.imgAddFriend.image =nil;
    }
    
    cell.btnAddFriend.tag= indexPath.row;
    
    cell.backgroundColor = UIColorFromRGB(0x353B41);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = searchedArray[indexPath.row];
    
    if ([dic[@"id"] integerValue] !=[sender_id integerValue]) {
        
        NSDictionary *dic =searchedArray[indexPath.row];
        FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
        [friend setFriendDic:searchedArray[indexPath.row]];

        [self pushVC:friend animate:YES expectTarget:ISMUR];
    }
}

#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    int index =(int)[sender tag];
    [COMMON addLoading:self];
    NSDictionary *dic = searchedArray[index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            //co qh
            
            NSMutableDictionary *userDic =[NSMutableDictionary dictionaryWithDictionary:dic];
            if (userDic[@"relation"]) {
                [userDic removeObjectForKey:@"relation"];
                
            }
            [userDic addEntriesFromDictionary:response];
            for (int i=0; i<searchedArray.count; i++) {
                NSDictionary *temp =searchedArray[i];
                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
                    [searchedArray replaceObjectAtIndex:i withObject:userDic];
                    break;
                }
            }
            //reload item
            NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
            [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }
    };
}

@end
