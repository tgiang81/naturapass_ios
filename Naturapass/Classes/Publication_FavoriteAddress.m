//
//  Publication_FavoriteAddress.m
//  Naturapass
//
//  Created by Giang on 10/9/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Publication_FavoriteAddress.h"
#import "CellKind18.h"
#import "FRHyperLabel.h"
#import "MurParameter_Favorites.h"
#import "SubNavigationPRE_ANNULER.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Publication_FavoriteAddress ()
{
    NSMutableArray * arrData;
    
    __weak IBOutlet UIView *myLine;
    IBOutlet UILabel *lbDescription1;


}
@property (nonatomic, weak) IBOutlet FRHyperLabel *label;

@end

@implementation Publication_FavoriteAddress

- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strMES_ADRESSES_FAVORITES);
    //    [self.subview removeFromSuperview];
    //add subview
    [self addSubNav:@"SubNavigation_PRE_General"];
    
    arrData = [NSMutableArray new];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind18" bundle:nil] forCellReuseIdentifier:identifierSection1];
}

-(void) reloadData
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite_address  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrData removeAllObjects];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"title":[[set_querry stringForColumn:@"c_title"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"],
                                      @"latitude":[set_querry stringForColumn:@"c_lat"],
                                      @"longitude":[set_querry stringForColumn:@"c_lon"]
                                      
                                      };
            [arrData addObject:objDic];
        }

        [self.tableControl reloadData];
        
        if (self.isFromSetting) {
            //hidden bottom
            myLine.hidden = YES;
            self.label.text = @"";
            [self.label setNeedsDisplay];
        }else{
            myLine.hidden = NO;
            FRHyperLabel *label = self.label;
            
            void(^handler)(FRHyperLabel *label, NSString *substring) = ^(FRHyperLabel *label, NSString *substring){
                //goto setting.
                MurParameter_Favorites *viewController1 = [[MurParameter_Favorites alloc] initWithNibName:@"MurParameter_Favorites" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];
                
            };
            
            //Pour gérer vos adresses favorites, rendez-vous dans les paramètres en cliquant ici
            [label setLinksForSubstrings:@[@"cliquant ici"] withLinkHandler:handler];
            
        }
    }];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self reloadData];
    
    switch (self.expectTarget)
    {
        case ISLOUNGE:
        {
            SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
            
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            
            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
        }
            break;
            
        case ISPARAMTRES:
        case ISMUR:
        {
            SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
            
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            
            btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
        }
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind18 *cell = nil;
    
    cell = (CellKind18 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    
    switch (self.expectTarget)
    {
        case ISLOUNGE:
        {
            cell.imageIcon.image =  [UIImage imageNamed: @"chasse_st_ic_adresses"];
        }
            break;
            
        case ISPARAMTRES:
        case ISMUR:
        {
            cell.imageIcon.image =  [UIImage imageNamed: @"mur_st_ic_adresses"];
        }
            break;
        default:
            cell.imageIcon.image =  [UIImage imageNamed: @"mur_st_ic_adresses"];
            
            break;
    }
    
    
    //FONT
    cell.label1.text = dic[@"title"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.label1.numberOfLines=2;
    [cell layoutIfNeeded];
    
    //    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrData[indexPath.row];
    self.myCallback(@{    @"latitude":dic[@"latitude"],
                          @"longitude":dic[@"longitude"]
                          });
    [self gotoback];
}


@end
