//
//  SubNavBaseView.h
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+MGBadgeView.h"

typedef void (^callBackSubNav) (UIButton*);

@interface SubNavBaseView : UIView
{

}

@property (nonatomic, copy) callBackSubNav myCallBack;

-(IBAction)fnSubNavClick:(id)sender;
-(void) badgingBtn:(UIView*)btn count:(int) iCount;
@property (strong, nonatomic) IBOutlet UIButton *btnLeMur;
@property (strong, nonatomic) IBOutlet UIButton *btnMonMur;

@end
