//
//  GroupMesVC.h
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupBaseVC.h"
#import "GroupesViewCellAdmin.h"
#import "GroupesViewCellNormal.h"
#import "ShortcutScreenVC.h"
@protocol GroupMesViewDelegate <NSObject>

//-(void)enterGroupMurAction:(BOOL)isAdmin withGroupDic:(NSDictionary*)groupDic index:(NSInteger)index;
//-(void)enterGroupInfoAction:(BOOL)isAdmin withGroupDic:(NSDictionary*)groupDic withCell :(GroupesViewCell*)cell index:(NSInteger)index;
//-(void)enterGroupInfoActionWithGroupDic:(NSDictionary*)groupDic;
//
//-(void)enterGroupValidationAction:(BOOL)isAdmin withGroupDic:(NSDictionary*)groupDic;
//
@end
@interface GroupMesVC : GroupBaseVC
{}
@property (nonatomic,retain)id<GroupMesViewDelegate>delegate;
@property (nonatomic,strong) ShortcutScreenVC* viewShortCut;
@property (weak, nonatomic) IBOutlet UIButton *btnShortCut;
-(void)getItemWithKind:(NSString*)myid;
-(void)doRefresh;
@end
