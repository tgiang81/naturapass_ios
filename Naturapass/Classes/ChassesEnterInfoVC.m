//
//  ChassesToutesView.m
//  Naturapass
//
//  Created by Giang on 7/16/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesEnterInfoVC.h"
#import "Config.h"
#import "AppCommon.h"

#import "WebServiceAPI.h"
#import "ASSharedTimeFormatter.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Define.h"
#import "UIAlertView+Blocks.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "ChasseInfoCell.h"
#import "MLKMenuPopover.h"
#import "GroupEnterMurVC.h"
#import "GroupEnterOBJ.h"
static NSString *typecell1 = @"ChasseInfoCell";
static NSString *typecell1ID = @"TypeCell1ID";


#define FIELDS_COUNT                    3

@interface ChassesEnterInfoVC () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,WebServiceAPIDelegate>{

}
@property (nonatomic, strong) ChasseInfoCell *prototypeCell;

@end
@implementation ChassesEnterInfoVC

- (ChasseInfoCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:typecell1ID];
    }
    return _prototypeCell;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self intialization];
}
-(void)intialization{
    //init tableview cell
    [self.tableControl registerNib:[UINib nibWithNibName:typecell1 bundle:nil] forCellReuseIdentifier:typecell1ID];
    self.tableControl.estimatedRowHeight = 100;
    
}
#pragma mark -  TableView Delegates
-(void) setDataForMessCell:(ChasseInfoCell*) cell withIndex:(NSIndexPath *)indexPath
{
    if(self.dicInfo){
        //IMAGE
        NSString *strImage;
        
        if (self.dicInfo[@"profilepicture"] != nil) {
            strImage=self.dicInfo[@"profilepicture"];
        }else{
            strImage=self.dicInfo[@"photo"];
        }
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        
        [cell.Img1 sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_photo"] ];
        
        // NAME
        NSString *strName=[NSString stringWithFormat:@"%@",self.dicInfo[@"name"]];
        [cell.label1 setText:strName];
        
        // DESC
        NSString *strText=self.dicInfo[@"description"];
        cell.label10.text=strText;

        
        //ACCESS
        NSString *strAccess=[NSString stringWithFormat:@"%@",self.dicInfo[@"access"]];
        if([strAccess isEqualToString:@"0"])
            cell.label2.text=str(strAccessPrivate);
        else if([strAccess isEqualToString:@"1"])
            cell.label2.text=str(strAccessSemiPrivate);
        
        else if([strAccess isEqualToString:@"2"])
            cell.label2.text=str(strAccessPublic);
        
        // setting theme
        if (self.expectTarget == ISLOUNGE) {
            //title
            NSString *strAdminLabelBlack = [NSString stringWithFormat:@"%@ %@",str(strAdministre_par), self.dicInfo[@"owner"][@"fullname"]];
            [cell.label11 setText:strAdminLabelBlack];
            
            if([self.dicInfo[@"connected"][@"access"]isEqual:@3])
            {
                [cell fnSettingCell:UI_CHASSES_MUR_ADMIN];
            }else if([self.dicInfo[@"connected"][@"access"]isEqual:@2])
            {
                [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
            }else{
                //hidden...
                [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
            }
            //
            cell.Img3.hidden =YES;
            cell.label3.hidden =YES;
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"] intValue]==USER_ADMIN)
                {
                    if ( [self.dicInfo[@"nbParticipants"] intValue] > 1) {
                        cell.Img3.hidden =NO;
                        cell.label3.hidden =NO;

                        cell.label3.text =[NSString stringWithFormat:@"%d participants",[self.dicInfo[@"nbParticipants"] intValue]];
                    }else if( [self.dicInfo[@"nbParticipants"] intValue] ==1){
                        cell.Img3.hidden =NO;
                        cell.label3.hidden =NO;

                        cell.label3.text =[NSString stringWithFormat:@"%d participant",[self.dicInfo[@"nbParticipants"] intValue]];
                    }
                }
            }

        }
        else if(self.expectTarget == ISGROUP)
        {
            //title
            NSString    *strOwner = [NSString stringWithFormat:@"%@ %@",self.dicInfo[@"owner"][@"firstname"],self.dicInfo[@"owner"][@"lastname"]];
            
            NSAttributedString *attStrAdminGroup;
            NSMutableAttributedString *str1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",str(strAdministre_par)]];
            NSMutableAttributedString *str2 = [[NSMutableAttributedString alloc] initWithString:@"Vous "];
            NSMutableAttributedString *str3 = [[NSMutableAttributedString alloc] initWithString:@"et "];
            NSMutableAttributedString *str4 = [[NSMutableAttributedString alloc] initWithString:strOwner];
            
            [str1 addAttribute:NSFontAttributeName
                         value:[UIFont systemFontOfSize:13.0f]
                         range:NSMakeRange(0, str1.length-1)];
            [str2 addAttribute:NSFontAttributeName
                         value:[UIFont boldSystemFontOfSize:13.0f]
                         range:NSMakeRange(0, str2.length-1)];
            [str3 addAttribute:NSFontAttributeName
                         value:[UIFont systemFontOfSize:13.0f]
                         range:NSMakeRange(0, str3.length-1)];
            [str4 addAttribute:NSFontAttributeName
                         value:[UIFont boldSystemFontOfSize:13.0f]
                         range:NSMakeRange(0, str4.length-1)];
            
            if([self.dicInfo[@"connected"][@"access"]isEqual:@3])
            {
                [str1 appendAttributedString:str2];

                [cell fnSettingCell:UI_GROUP_MUR_ADMIN];
            }else if([self.dicInfo[@"connected"][@"access"]isEqual:@2])
            {
                [str1 appendAttributedString:str4];

                [cell fnSettingCell:UI_GROUP_MUR_NORMAL];
            }else{
            //hidden...
                [cell fnSettingCell:UI_GROUP_MUR_NORMAL];
            }
            attStrAdminGroup = str1;
            cell.label11.attributedText =attStrAdminGroup;

            //
            if ( [self.dicInfo[@"nbSubscribers"] intValue] > 1) {
                cell.label3.text =[NSString stringWithFormat:@"%@ %@s",self.dicInfo[@"nbSubscribers"],str(strMMembre)];
            }else if( [self.dicInfo[@"nbSubscribers"] intValue] ==1){
                cell.label3.text =[NSString stringWithFormat:@"%@ %@",self.dicInfo[@"nbSubscribers"],str(strMMembre)];
            }
            else
            {
                cell.Img3.hidden =YES;
                cell.label3.hidden =YES;
            }
        }

        [cell layoutIfNeeded];
        
    }
}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChasseInfoCell *cell=(ChasseInfoCell *) cellTmp;
    [self setDataForMessCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dicInfo?1:0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.dicInfo){
        ChasseInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:typecell1ID forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    }
    return nil;
}

@end
