//
//  CarterFilterFindView.h
//  Naturapass
//
//  Created by Manh on 10/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "BaseVC.h"
#import "TreeViewNode.h"
typedef void (^CarterFilterFindViewCallback)(TreeViewNode *nodeSub);

@interface CarterFilterFindView : UIView<UITextFieldDelegate>
{
    NSMutableArray * arrDataSelected;
    NSMutableArray * arrData;
    NSMutableArray * arrDataFull;
    WebServiceAPI *serviceAPI;

}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleScreen;
@property (weak, nonatomic) IBOutlet UIButton *btnValider;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeghtCancel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeghtValider;
@property(nonatomic,strong)  UIColor *colorNavigation;

@property (weak, nonatomic) IBOutlet UITextField *tfMotCle;
@property (nonatomic, retain) TreeViewNode *node;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic, strong) BaseVC *parentVC;
@property (nonatomic,copy) CarterFilterFindViewCallback callback;
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt;
-(void)addContraintSupview:(UIView*)viewSuper;
-(void)fnTreeNode:(TreeViewNode*)node;
-(void)hide:(BOOL)hidden;
-(void)setup;
@end
