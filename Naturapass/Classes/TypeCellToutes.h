//
//  MesSalonCustomCell.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 3/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBase.h"
#import "Define.h"
#import "SOLabel.h"
#import "MDStatusButton.h"
@interface TypeCellToutes : CellBase

//view
@property (nonatomic, retain) IBOutlet UIView *view1;
@property (nonatomic, retain) IBOutlet UIView *view2;
@property (nonatomic, retain) IBOutlet UIView *view3;
@property (nonatomic, retain) IBOutlet UIView *view4;

//image
@property (nonatomic, retain) IBOutlet UIImageView *Img1;
@property (nonatomic, retain) IBOutlet UIImageView *Img2;
@property (nonatomic, retain) IBOutlet UIImageView *Img3;
@property (nonatomic, retain) IBOutlet UIImageView *Img4;
@property (nonatomic, retain) IBOutlet UIImageView *Img5;
@property (nonatomic, retain) IBOutlet UIImageView *Img6;
@property (nonatomic, retain) IBOutlet UIImageView *Img7;
@property (nonatomic, retain) IBOutlet UIImageView *Img8;

//lable
@property (nonatomic, retain) IBOutlet UILabel *label1;
@property (nonatomic, retain) IBOutlet UILabel *label2;
@property (nonatomic, retain) IBOutlet UILabel *label3;
@property (nonatomic, retain) IBOutlet UILabel *label4;
@property (nonatomic, retain) IBOutlet UILabel *label5;
@property (nonatomic, retain) IBOutlet UILabel *label6;
@property (nonatomic, retain) IBOutlet UILabel *label7;
@property (nonatomic, retain) IBOutlet UILabel *label8;

@property (nonatomic, retain) IBOutlet SOLabel *label9;

@property (nonatomic, retain) IBOutlet UILabel *label10;
@property (nonatomic, retain) IBOutlet UILabel *label11;
@property (nonatomic, retain) IBOutlet UILabel *label12;

//button
@property (nonatomic, retain) IBOutlet MDStatusButton *button2;
@property (nonatomic, retain) IBOutlet UIButton *button3;
@property (nonatomic, retain) IBOutlet UIButton *button4;
@property (nonatomic, retain) IBOutlet UIButton *button5;

@property (nonatomic, retain) IBOutlet UIButton *btnLocation;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight2;


//funtion
-(void)fnSettingCell:(int)type;
@end
