//
//  MyCustomTitleView.swift
//  Naturapass
//
//  Created by giangtu on 11/3/17.
//  Copyright © 2017 Heolys. All rights reserved.
//

import UIKit

class MyCustomTitleView: UIView {
    
    @IBOutlet var viewNavigation1: UIView!
    @IBOutlet weak var constraintWidth: NSLayoutConstraint!
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
    override func awakeFromNib() {
        self.constraintWidth.constant = UIScreen.main.bounds.size.width
        
    }
}

