//
//  BaseMapVC.h
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>

#import "ImageMapAnnotationView.h"
#import "ImageMapViewAnnotation.h"
//#import "LegendAnnotation.h"
//#import "LegendAnnotationView.h"

#import "DistributionAnnotation.h"
#import "DistributionView.h"

#import "AppDelegate.h"
#import "MainNavigationController.h"
#import "Config.h"

#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>


#import <CoreLocation/CLPlacemark.h>
#import "HNKGooglePlacesAutocomplete.h"
#import <MapKit/MapKit.h>

#import "CLPlacemark+HNKAdditions.h"
#import "CommentDetailVC.h"
#import "PublicationVC.h"

#import "CarteStatusVC.h"
#import "CarteTypeContentVC.h"
#import "CarteFiltreparticiants.h"
#import "CarteFiltreVC.h"

#import "CommonHelper.h"
#import "MapDataDownloader.h"
#import "MKMapView+ZoomLevel.h"
#import "DBMapSelectorOverlayRenderer.h"
#import "DBMapSelectorOverlay.h"
//#import "LXMapScaleView.h"


#import <GoogleMaps/GoogleMaps.h>

#import "LXMapScaleView_Google.h"
#import "InfoWindow.h"
#import "DatabaseManager.h"
#import "ASImageView.h"
#import "MDMarkersDistribution.h"
#import "MDMarkersCustom.h"
#import "MDDistributionView.h"
#import "MDMarkersLegend.h"
#import "Define.h"
#import "BaseMapUI.h"

#import "TestTileLayer.h"
#import "TestTileLayerIGN.h"
#import "TestTileLayerParcel.h"
#import "AlertColorDrawShapeVC.h"
#import "AlertEditDrawShapeVC.h"
#define kRegion 500
#define MERCATOR_RADIUS 200
#define RADIUS_CIRCLE 50.0
#define MERCATOR_RADIUS 200
#define SCROLL_UPDATE_DISTANCE          0.02
#define CLEAN_MAP_UPDATE_DISTANCE       5

static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";

@interface BaseMapVC : BaseVC <
UISearchBarDelegate, UIAlertViewDelegate,
UIActionSheetDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,WebServiceAPIDelegate,
UITableViewDataSource, UITableViewDelegate,
CLLocationManagerDelegate, GMSMapViewDelegate, BaseMapUIDelegate , BaseMapUIDatasource>

{
    BOOL canRequestMedia;
    MKCircle *circle;
    DBMapSelectorOverlayRenderer    *_selectorOverlayRenderer;
    DBMapSelectorOverlay            *_selectorOverlay;
    
    NSMutableArray                  *arrDistributorAnnotations;
    NSMutableArray                  *arrPublication;
    
    CLLocationManager *locationManager;
    
    MKPointAnnotation * centerPointAnnotation;
    
    MKAnnotationView            *markView;
    
    CLPlacemark *placemark;
    
    NSString                        *strLatitude;
    NSString                        *strLongitude;
    
    MKAnnotationView            *centerAnnotationView;
    
    //
    CLLocationCoordinate2D northEastFilter;
    CLLocationCoordinate2D southWestFilter;

    CLLocationCoordinate2D m10MetersNE;
    CLLocationCoordinate2D m10MetersSW;

    //for checking movement
    CLLocationCoordinate2D lastLocationCoordinate;
    
    BOOL isCategoryON;
    
    BOOL isUsingLocation;
    WebServiceAPI *myServiceAPI;
    
    int iCount;
    int iZoomDefaultLevel;
    BOOL iCanRequest;
    BOOL isDragging;
    
    BOOL isFirsTime;
    BaseMapUI *vTmpMap;
    TestTileLayerIGN    *layerIGN;
    
    TestTileLayer       *layerOverlay;
    
    TestTileLayerParcel *layerParcel;
    int iMapType;

}
@property (nonatomic,strong) IBOutlet UIButton *btnRecenter;
@property (nonatomic,strong) IBOutlet UIImageView *imgRecenter;

@property (nonatomic,strong) IBOutlet UIButton *btnFollowHeading;


@property (weak, nonatomic) IBOutlet UIView *baseMapView;

@property (nonatomic, strong) NSString *myC_Search_Text;

@property (nonatomic, assign) BOOL isShowMylocation;

@property (nonatomic, assign) double currentDist;

@property (nonatomic, strong) NSMutableArray* arrTitleShapeAnnotations;

@property (nonatomic, strong) GMSMarker *selectedPlaceAnnotation;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (nonatomic, assign) BOOL shouldBeginEditing;

@property (nonatomic, assign) BOOL isHeading;
@property (weak, nonatomic) IBOutlet UIImageView *icCompass;



@property(nonatomic,strong) NSString *strFilterMoi;
@property(nonatomic,strong) NSString *strFilterAmis;
@property(nonatomic,strong) NSString *strFilterPersonne;
@property(nonatomic,strong) NSString *strFilterDebut;
@property(nonatomic,strong) NSString *strFilterFin;

@property(nonatomic,strong) NSString *strMesGroupFilter;
@property(nonatomic,strong) NSString *strMesHuntFilter;

@property(nonatomic,strong) NSString *strMesCategoryFilter;

@property(nonatomic,strong) IBOutlet UISearchBar                *toussearchBar;
@property(nonatomic,strong) IBOutlet UISearchBar                *searchAddress;
@property(nonatomic,strong) IBOutlet UIButton *btnLeftMenu;

@property(nonatomic,strong) NSMutableDictionary *dicMarkerDistribution;
@property(nonatomic,strong) NSMutableDictionary *dicOverlays;

//GGTT
@property(nonatomic,strong) NSMutableDictionary *myDIC_MarkerPublication;
@property(nonatomic,strong) NSMutableDictionary *myDIC_MarkerAgenda;


@property (nonatomic,assign) int indexTracking;
@property(nonatomic,strong) LXMapScaleView_Google* mapScaleView;
@property (nonatomic,assign) BOOL drawingShape;
@property (nonatomic, strong) NSMutableDictionary* dicDrawShape;
@property (nonatomic, strong) NSMutableDictionary* dicPolygon;
@property(nonatomic,strong)  UIColor *colorNavigation;

-(void)showAlertColorDrawShape;
-(void)enableDrawShape;
-(IBAction)fnTracking_CurrentPos:(id)sender;

-(void) exitMapView;

-(void) refreshCircle;
-(void) removeCircle;
-(IBAction)fnDecrease;
-(IBAction)fnIncrease;

-(void) doReloadRefresh;

-(void) refreshChatlive;
-(void)loadOptionShapes:(NSArray*)mapShapes;
-(void)addPublication:(GMSMarker*)marker;
-(void)removeAllPublication;
//-(void)removePublicationWithIndex:(int)index;
-(void)removePublicationWithKey:(NSString*) key;

-(void)addAgenda:(GMSMarker*)marker;
-(void)removeAllAgenda;
-(void)removeAgendaWithKey:(NSString*) key;


-(void)addDistribution:(GMSMarker*)marker;
-(void)removeAllDistribution;


-(void) updateRightToChat:(NSNotification*)notif;
-(void)showAlertSearch;
-(void) doPublication;
-(void) cancelCurrentTasks;
-(void)settingBlurBackGround;
-(void)hideBlurBackGround;
-(void)showBlurBackGround;
-(UIImage*)resizeImage:(UIImage*)image withLevelZoom:(NSInteger)leveZoom withResize:(BOOL)isResize withC_Marker:(int)c_marker;
-(UIImage*)resizeImageDistribution:(UIImage*)image withLevelZoom:(NSInteger)leveZoom;

-(void)downloadTitleLayer;
-(void) doMapType:(int) mType;
-(void)showAllPointTogetherPositionWithDidTapMarker:(GMSMarker *)marker;
-(void)gotoAgendaDidTapMarker:(GMSMarker *)marker;
-(void)gotoPublicationDidTapMarker:(GMSMarker *)marker;
-(BOOL)locationCheckStatusDenied;
-(void) doFnTracking: (BOOL)isHeading;
-(IBAction)fnCenter:(id)sender;
-(void)showIndicator;

-(void)addMarkerDrawShape:(NSDictionary*)dic;
-(void)addMarkerDrawLines:(NSDictionary*)dic;

@end
