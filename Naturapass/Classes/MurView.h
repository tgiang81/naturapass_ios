//
//  MurView.h
//  Naturapass
//
//  Created by Manh on 6/16/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaCell.h"
#import "BaseVC.h"
#import "Define.h"
#import "BaseView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "SettingActionView.h"

typedef void (^MurViewCallback)(VIEW_ACTION_TYPE type, NSArray *arrData);
@interface MurView : BaseView<UIDocumentInteractionControllerDelegate, IDMPhotoBrowserDelegate>
{
    NSMutableArray                  *publicationArray;
    int widthImage;
    SettingActionView *shareDetail;
}
@property (nonatomic, strong) MediaCell *prototypeCell;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic, strong) BaseVC *parentVC;
@property (nonatomic,copy) MurViewCallback callback;
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;


-(void)addContraintSupview:(UIView*)viewSuper;
-(void)fnSetDataPublication:(NSArray*)arrPublication;
-(void)fnPublicationNew:(NSArray*)arrPublication;
-(void)fnAddMorePublication:(NSArray*)arrPublication;
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget;
@end
