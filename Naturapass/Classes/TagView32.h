//
//  TagView32.h
//  Naturapass
//
//  Created by Giang on 3/2/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^callBackAlert) (NSInteger index, NSDictionary *dic);

@interface TagView32 : UIView
{
    
}
@property (nonatomic, copy) callBackAlert CallBack;
@property (nonatomic,strong) IBOutlet UIView *viewBoder;

-(void) refreshWithData:(NSArray*)arr selected:(NSString*)strID;

-(void) showViewWithData:(NSArray*)arrContent withArrSelected:(NSArray*)arrSelected withIndex:(NSInteger)index;
-(void)doBlock:(callBackAlert ) cb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBorder;

@end
