//
//  ForgetPasswordViewController.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 5/28/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPGTextField.h"

@interface ForgetPasswordViewController : UIViewController
{
    IBOutlet MPGTextField *forgetEmailTextField;
    
    
    //   toolBar
    UIToolbar                   *keyboardToolbar;
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
 
}
@end
