//
//  DocumentViewCell.h
//  Naturapass
//
//  Created by GiangTT on 12/26/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DocumentViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconThumb;
@property (weak, nonatomic) IBOutlet UILabel *nameFile;

@end
