//
//  Etape10_ChoixTypePoint1.m
//  Naturapass
//
//  Created by Giang on 10/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Etape10_ChoixTypePoint1.h"
#import "Etape10_ChoixTypePoint2.h"
#import "CellKind14.h"
#import "ChassesCreate_Step11.h"
#import "DatabaseManager.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Etape10_ChoixTypePoint1 ()
{
    NSMutableArray * arrDataSelection, *arrData;
    IBOutlet UIButton *btnTous;
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDescription;
    IBOutlet UILabel *lbTous;
    NSInteger countCheck;
}
@end

@implementation Etape10_ChoixTypePoint1


- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strFiltrezLesPointsQueVousImprotez);
    lbDescription.text = str(strChoisissezLesPointsdeGeo);
    lbTous.text = str(strTOUS);
    self.needChangeMessageAlert = YES;

    arrData = [NSMutableArray new];
    arrDataSelection = [NSMutableArray new];
    //if tree category is nil, then get tree default
    if ([PublicationOBJ sharedInstance].treeCategory == nil) {
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        
    }
    NSArray *listTree = [PublicationOBJ sharedInstance].treeCategory;
    for (NSDictionary*dic in listTree) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {

            [arrData addObject:dic];
        
            [arrDataSelection addObject:@{@"id":dic[@"id"],
                                      @"name":dic[@"name"],
                                      @"status":[NSNumber numberWithBool:NO]
                                      }];
        }
        
    }
    
    
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind14" bundle:nil] forCellReuseIdentifier:identifierSection1];
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    [self statusButtonSuivant];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellKind14 *cell = nil;
    
    cell = (CellKind14 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrDataSelection[indexPath.row];
    NSDictionary *dicFull = arrData[indexPath.row];
    //Mes group
    
    [cell.slideTitleLabel setText :dic[@"name"]];
    if ( ((NSArray*)dicFull[@"children"]).count > 0) {
        cell.imgArrow.hidden = NO;
    }else{
        cell.imgArrow.hidden = YES;
    }
    [cell.btnOver addTarget:self action:@selector(fnCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnOver.tag = indexPath.row;
    
    NSNumber *num = dic[@"status"];
    
    if ( [num boolValue] ) {
        [cell.checkbox setSelected:TRUE];
        
    }else{
        [cell.checkbox setSelected:FALSE];
        
    }
    [cell.checkbox setTypeCheckBox:UI_CHASSES_MUR_NORMAL];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

        NSDictionary *dic = arrData[indexPath.row];
    if ( ( (NSArray*)dic[@"children"]).count > 0 ) {
        Etape10_ChoixTypePoint2 *viewController1 = [[Etape10_ChoixTypePoint2 alloc] initWithNibName:@"Etape10_ChoixTypePoint2" bundle:nil];
        viewController1.myDic = dic;
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
    else
    {
        [self selectedRowWithIndex:indexPath.row];
    }
}


-(void) fnCheckBox:(UIButton*)btn
{
    [self selectedRowWithIndex:btn.tag];
    //
}
-(void)selectedRowWithIndex:(NSInteger)index
{
    NSDictionary *dic=[arrDataSelection[index] mutableCopy];
    
    if ([dic[@"status"] boolValue] == YES)
    {
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"status"];
        countCheck--;
    }
    else
    {
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        countCheck++;
    }
    
    [arrDataSelection replaceObjectAtIndex:index withObject:dic];
    
    NSRange range = NSMakeRange(0,1);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    [self statusButtonSuivant];
}
-(void)statusButtonSuivant
{
    if (countCheck >0) {
        self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    }
    else
    {
        self.btnSuivant.backgroundColor = [UIColor lightGrayColor];
    }
}
-(IBAction)onNext:(id)sender
{
    
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    for (NSDictionary*dic in arrDataSelection) {
        if ([dic[@"status"] boolValue] == YES) {
            [arrTmp addObject:dic[@"id"]];
        }
    }
    
    if (arrTmp.count == 0) {
        return;
    }
    
    //allow next if select atleast 1
    [COMMON addLoading:self];
    //Request

    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_GROUP_TO_HUNT:@{@"groups":@[[ChassesCreateOBJ sharedInstance].group_id_involve],
                                            @"categories": arrTmp
                                            }
                                 loungeID:[ChassesCreateOBJ sharedInstance].strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
//        ASLog(@"%@", response);
        if (errCode == -1001)
        {
            //timeout
            [KSToastView ks_showToast: str(strTryAgainRequest) duration:4.0f completion: ^{
            }];
            
            return;
        }
        else
        {
            
            if ([self fnCheckResponse:response])
            {
                [KSToastView ks_showToast: response[@"message"] duration:4.0f completion: ^{
                }];
                return;
            }
        }
        
        if (response[@"success"])
        {
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];

                }
            }];
            
            

            //
            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                [self backEditListChasse];
            }
            else
            {
            ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            }
        }
        else
        {
//            [KSToastView ks_showToast:NSLocalizedString(@"INVALID_ERROR", @"")  duration:4.0f completion: ^{
//            }];
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: str(strTitle_app)
//                                                                message: [NSString stringWithFormat:@"groups: %@ \n categories: %@ \n chasseID: %@",@[[ChassesCreateOBJ sharedInstance].group_id_involve],arrTmp,[ChassesCreateOBJ sharedInstance].strID]
//                                                               delegate: nil
//                                                      cancelButtonTitle: str(strOK)
//                                                      otherButtonTitles: nil];
//            
//            [alertView show];
        }
    };
    
}

-(IBAction)fnSelectTOUS:(id)sender
{
    [COMMON addLoading:self];
    //Request
    
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_GROUP_TO_HUNT:@{@"groups":@[[ChassesCreateOBJ sharedInstance].group_id_involve]
                                            }
                                 loungeID:[ChassesCreateOBJ sharedInstance].strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
//        ASLog(@"%@", response);
        
        if (response[@"success"])
        {
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];

                }
            }];
            
            

            //
            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                [self backEditListChasse];
            }
            else
            {
            ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            }
        }
    };
}

@end
