//
//  GroupSearchVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupSearchVC.h"
#import "GroupMesVC.h"
#import "GroupSettingVC.h"
#import "NotificationVC.h"
#import "GroupToutesCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GroupSearchAlertView.h"
#import "MapDataDownloader.h"
#import "GroupEnterMurVC.h"
#import "GroupEnterOBJ.h"
#import "AlertViewMembers.h"
#import "FriendInfoVC.h"
#import "CCMPopupTransitioning.h"
static NSString *ToutesCell =@"GroupToutesCell";
static NSString *ToutesCellID =@"ToutesID";

//static NSString *cellkind3 =@"CellKind3";
//static NSString *cellkin3ID =@"CellKind3ID";

@interface GroupSearchVC ()
{
    IBOutlet UISearchBar                *toussearchBar;
    IBOutlet UILabel                    *labelRejoinSalon;
    IBOutlet UIButton                   *btnClose;

    //
    NSMutableArray                      *loungesArray;
    NSMutableArray                      *tourMemberArray;
    NSMutableArray                      *searchLoungesArray;


    
    //
    int                                 secIndex;
    BOOL                                isFooter;
    NSString                            *strName;
    NSString                            *strAccess;
    NSString                            *strLoungeID;
    NSString                            *accessValue;
    NSString                            *strTourUserID;
    NSNumber                            *numberOption;
    BOOL                                isLoading;

    //
    NSMutableArray                      *validerBtnClickedArray;
    UIButton                            *tempRejoindreButton;
    IBOutlet UILabel                    *lbTitle;

}
@property (nonatomic, assign) BOOL shouldBeginEditing;
@property (nonatomic, strong) GroupToutesCell *prototypeCell;

@end

@implementation GroupSearchVC

- (GroupToutesCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:ToutesCellID];
    }
    
    return _prototypeCell;
}

#pragma mark - viewdid and init
- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strTOUS_LES_GROUPES);
    toussearchBar.placeholder = str(strSearchGroup);
    // Do any additional setup after loading the view from its nib.
    [self initialization];
    
    self.tableControl.estimatedRowHeight = 100;
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_GROUP_SEARCH" ] ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        if (arrTmp.count > 0) {
            [loungesArray  addObjectsFromArray:arrTmp];
            
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.tableControl reloadData];
        });
    });
    
    if ([COMMON isReachable]) {
        [self performSelector:@selector(startRefreshControl) withObject:nil afterDelay:0.5];
    }
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    

}
-(void)initialization
{
    loungesArray=[[NSMutableArray alloc]init];
    tourMemberArray = [[NSMutableArray alloc]init];
    //aabb
    [self.tableControl registerNib:[UINib nibWithNibName:ToutesCell bundle:nil] forCellReuseIdentifier:ToutesCellID];
    self.tableControl.estimatedRowHeight = 100;
    [self initRefreshControl];
    
//    secIndex=-1;
    [toussearchBar setTintColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];

}

//overwrite
- (void)insertRowAtTop {
    if (self.shouldBeginEditing) {
        [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
    }
    else
    {
        WebServiceAPI *loungeAction = [WebServiceAPI new];
        [loungeAction getGroupToutesAction:lounge_limit withOffset:@"0"];
        loungeAction.onComplete = ^(NSDictionary *response,int errCode)
        {
            
            [self stopRefreshControl];
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            if (response[@"groups"]) {
                NSArray *array = [response objectForKey:@"groups"];
                [loungesArray removeAllObjects];
                [loungesArray addObjectsFromArray:array];
                //Save
                NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_GROUP_SEARCH" ] ];
                [loungesArray writeToFile:strPath atomically:YES];
                [self.tableControl reloadData];

            }

        };
    }

}

//overwrite
- (void)insertRowAtBottom {

    if (self.shouldBeginEditing) {
        NSString *offset =[NSString stringWithFormat:@"%ld",(long)[searchLoungesArray count]];

        [self goLoungesSearchURL:toussearchBar.text offset:offset];
    }
    else
    {
        WebServiceAPI *loungeAction = [WebServiceAPI new];
        [loungeAction getGroupToutesAction:lounge_limit withOffset:[NSString stringWithFormat:@"%ld",(long)[loungesArray count]]];
        loungeAction.onComplete = ^(NSDictionary *response,int errCode)
        {
            
            [self stopRefreshControl];
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            NSArray *array = [response objectForKey:@"groups"];

            if ([array count] > 0) {
                [loungesArray addObjectsFromArray:array];
                
                //Save
                NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_GROUP_SEARCH" ] ];
                [loungesArray writeToFile:strPath atomically:YES];
                
            }
            [self.tableControl reloadData];
        };
    }

}
#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{

    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];

    secIndex=-1;
    self.shouldBeginEditing = NO;
    [self.searchDisplayController setActive:NO];
    [self.tableControl reloadData];
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar setShowsCancelButton:YES animated:YES];
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [COMMON addLoading:self];
    [self goLoungesSearchURL:toussearchBar.text offset:@"0"];
    [theSearchBar resignFirstResponder];

}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{

    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    secIndex=-1;
    [tourMemberArray removeAllObjects];
    [searchLoungesArray removeAllObjects];
    [self.tableControl reloadData];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - WebService
-(void)getLounchesOffset:(NSInteger)offset{
    WebServiceAPI *loungeAction = [WebServiceAPI new];
    [loungeAction getGroupToutesAction:lounge_limit withOffset:[NSString stringWithFormat:@"%ld",(long)offset]];
    loungeAction.onComplete = ^(NSDictionary *response,int errCode)
    {

        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        NSArray *array = [response objectForKey:@"groups"];
        if (isFooter == NO) {
            [loungesArray removeAllObjects];
        }
        if ([array count] > 0) {
            [loungesArray addObjectsFromArray:array];
            
            //Save
            NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_GROUP_SEARCH" ] ];
            [loungesArray writeToFile:strPath atomically:YES];

        }
        [self.tableControl reloadData];
    };
}
- (void) goLoungesURL {
    WebServiceAPI *loungeCount = [[WebServiceAPI alloc]init];
    [loungeCount getGroupToutesAction:lounge_limit withOffset:@"0"];
    loungeCount.onComplete = ^(NSDictionary *response,int errCode)
    {
        isLoading=YES;
        if (!response) {
            return ;
        }
        [loungesArray removeAllObjects];
        loungesArray=[[response objectForKey:@"groups"] mutableCopy];
        [self.tableControl reloadData];
    };

}
- (void) goLoungesSearchURL:(NSString*)encodedString offset:(NSString*)offset
{
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getGroupSearch:encodedString offset:offset];
    serviceAPI.onComplete=^(NSDictionary *response, int errCode)
    {
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        isLoading=YES;

        if ([response isKindOfClass:[NSDictionary class]]) {
            NSMutableArray *lougesArr = [response objectForKey:@"groups"];
            if (lougesArr) {
                if([lougesArr count]<=0)
                    return ;
                [searchLoungesArray removeAllObjects];
                searchLoungesArray=[lougesArr mutableCopy];
            }
        }
        [self.tableControl reloadData];
    };
}
-(void)getLoungeSuscriber {
    [COMMON addLoading:self];
    WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
    
    [loungeSubscribeAction getGroupToutesSubscribeFriendAction:strLoungeID];
    loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        [tourMemberArray removeAllObjects];
        [tourMemberArray addObjectsFromArray:[response objectForKey:@"subscribers"]];
        for (int i=0; i<tourMemberArray.count; i++) {
            NSDictionary *dic =[tourMemberArray objectAtIndex:i];
            if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                [tourMemberArray removeObjectAtIndex:i];
            }
        }
        [self.tableControl reloadData];
        [COMMON removeProgressLoading];
    };
    
}
#pragma mark -  TableView Delegates
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSMutableArray *arrData = [NSMutableArray new];
    if (self.shouldBeginEditing) {
        arrData =[searchLoungesArray copy];
    }
    else
    {
        arrData =[loungesArray copy];
    }
    GroupToutesCell *cell=(GroupToutesCell *) cellTmp;
    if([arrData count]>0){
        NSDictionary *dic =arrData[indexPath.row];
        strName = [dic[@"name"] emo_emojiString];

        if([strName isEqualToString:@"(null)"]||strName==nil)
        {
            strName = [dic[@"lounge"][@"name"]  emo_emojiString];
        }
        cell.titleNameLabel.text=strName;
        NSString *strText = [dic[@"description"]  emo_emojiString];

        if([strText isEqualToString:@"(null)"]||strText==nil)
        {
            strText = [dic[@"lounge"][@"description"]  emo_emojiString];
        }

        [cell.descLabel setText:strText];
        
        cell.btnRejoindre.tag=indexPath.row+2;
        strAccess= [dic[@"connected"][@"access"] stringValue];
        NSInteger iAccess = 1000;
        if (strAccess && ![strAccess isEqualToString:@""]) {
            iAccess=[strAccess intValue];
        }
        [cell.btnRejoindre setColorEnable:UIColorFromRGB(GROUP_TOUS_COLOR)];
        
        
        [cell.btnRejoindre removeTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRejoindre removeTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRejoindre removeTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];

        
        switch (iAccess)
        {
            case USER_INVITED:
                
            {
                [cell.btnRejoindre setTitle:str(strTo_Commit) forState:UIControlStateNormal];
                [cell.btnRejoindre setEnabled:YES];
                [cell.btnRejoindre addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.btnRejoindre setTitle:str(strOn_hold) forState:UIControlStateNormal];
                [cell.btnRejoindre setEnabled:NO];
            }
                break;
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.btnRejoindre setTitle:str(strAAccess) forState:UIControlStateNormal];
                [cell.btnRejoindre setEnabled:YES];
                [cell.btnRejoindre addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                
                [cell.btnRejoindre setTitle:str(strSinscrire) forState:UIControlStateNormal];
                [cell.btnRejoindre setEnabled:YES];
                [cell.btnRejoindre addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
                break;
        }
        NSString *strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"access"]];
        if([strAccessGroup isEqualToString:@"(null)"]||strAccessGroup==nil)
            strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"access"]];
        
        if(strAccessGroup.intValue == ACCESS_PRIVATE)
            cell.publicAccessLabel.text=str(strPRIVATE);
        else if(strAccessGroup.intValue ==ACCESS_SEMIPRIVATE)
            cell.publicAccessLabel.text=str(strSEMIPRIVATE);
        else if(strAccessGroup.intValue==ACCESS_PUBLIC)
            cell.publicAccessLabel.text=str(strPUBLIC);
        
        NSString *strImage=dic[@"photo"];
        
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        [cell.usericonImages sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"]];
        cell.btnMember.tag=indexPath.row+1;
        [cell.btnMember addTarget:self action:@selector(MemberButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMember setTitle:NSLocalizedString(@"Members", @"") forState:UIControlStateNormal];
        
        [cell.btnMember setColorEnable:UIColorFromRGB(GROUP_TOUS_COLOR)];        
//        if (strAccessGroup.intValue==ACCESS_PUBLIC) {
//            [cell.btnMember setEnabled:YES];
//        }
//        else
//        {
//            [cell.btnMember setEnabled:NO];
//        }

    }
    [cell fnSettingCell:UI_GROUP_TOUTE];
    [cell layoutIfNeeded];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.shouldBeginEditing) {
        return [searchLoungesArray count];
    }
    else
    {
        return [loungesArray count];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GroupToutesCell *cell=nil;
    cell = (GroupToutesCell *)[tableView dequeueReusableCellWithIdentifier:ToutesCellID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}
#pragma mark -action
-(void)rejoindreCommitAction:(UIButton *)sender{
    [toussearchBar resignFirstResponder];
    
    [UIAlertView showWithTitle:str(strTitle_app) message:str(strVous_avez_ete_invite_dans_ce_group)
             cancelButtonTitle:str(strAnuler)
             otherButtonTitles:@[str(strValider)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}
- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    NSMutableArray *arrData = [NSMutableArray new];
    if (self.shouldBeginEditing) {
        arrData =[searchLoungesArray mutableCopy];
    }
    else
    {
        arrData =[loungesArray mutableCopy];
    }
    NSInteger index = sender.tag-2;
    strLoungeID=arrData[index][@"id"];
    NSDictionary *dic =arrData[index];
    NSInteger accessValueNumber = [dic[@"access"] integerValue];
    
    accessValue = [NSString stringWithFormat: @"%ld", (long) accessValueNumber];
    NSString *desc=@"";
    NSString *strNameGroup=dic[@"name"];
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        desc =[NSString stringWithFormat:str(strRejoindre_un_group_public),strNameGroup];

    }
    else if (accessValueNumber == ACCESS_PRIVATE || accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoindre_un_group),strNameGroup];

    }
    else
    {
        NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
    }
    
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [LoungeJoinAction putJoinWithKind:MYGROUP mykind_id:strLoungeID strUserID:UserId ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [COMMON removeProgressLoading];
        if([respone isKindOfClass: [NSArray class] ]) {
            return ;
        }
        BOOL access= [respone[@"success"] boolValue];
        if (access) {
            
            NSDictionary *oldDict = dic[@"connected"];
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(2) forKey:@"access"];
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            if (self.shouldBeginEditing) {
                [searchLoungesArray replaceObjectAtIndex:index withObject:newDictConnect];
            }
            else
            {
                [loungesArray replaceObjectAtIndex:index withObject:newDictConnect];

            }
            //nav
            [self fngotoGroup:index];
        }
//         GroupSearchAlertView *vc = [[GroupSearchAlertView alloc] initWithNibName:@"GroupSearchAlertView" bundle:nil];
//        vc.exp = ISGROUP;
//        [vc.titleLabel setText:desc];
//         [vc doBlock:^(NSInteger index) {
//             //
//         }];
//         [self presentViewController:vc animated:NO completion:^{
//         
//         }];
        [UIAlertView showWithTitle:str(strTitle_app) message:desc
                 cancelButtonTitle:str(strOui)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
        [self.tableControl reloadData];
    };

}
-(void) gotoGroup:(UIButton*)sender
{
    NSInteger tag = ((UIButton *)sender).tag-2;
    [self fngotoGroup:tag];
}
- (void)cellRejoindre:(UIButton *)sender
{
    NSMutableArray *arrData = [NSMutableArray new];
    if (self.shouldBeginEditing) {
        arrData =[searchLoungesArray copy];
    }
    else
    {
        arrData =[loungesArray copy];
    }
    [toussearchBar resignFirstResponder];
    
    if ([sender isSelected]) {
        return;
    }
    else{
        NSInteger btn_index=sender.tag-2;
        strLoungeID=[[arrData objectAtIndex:btn_index]valueForKey:@"id"];
        
        
        NSInteger accessValueNumber = [arrData[btn_index][@"access"] integerValue];
        accessValue = [NSString stringWithFormat: @"%ld", (long) accessValueNumber];
        NSString *desc=@"";
        NSString *strNameGroup=arrData[btn_index][@"name"];
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[NSString stringWithFormat:str(strRejoindre_un_group_public),strNameGroup];

            
        }
        else if (accessValueNumber == ACCESS_PRIVATE || accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoindre_un_group),strNameGroup];

        }
        else
        {
            NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
        }
        [COMMON addLoading:self];
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        [LoungeJoinAction postGroupJoinAction:strLoungeID];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];

            [COMMON removeProgressLoading];
            if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
                [self.tableControl reloadData];
                return ;
            }
            NSInteger access= [respone[@"access"] integerValue];
            if (access==NSNotFound) {
                [self.tableControl reloadData];
                return ;
            }
//            GroupSearchAlertView *vc = [[GroupSearchAlertView alloc] initWithTitle:@"" Desc:desc];
//            [vc doBlock:^(NSInteger index) {
//                //
//            }];
//            vc.exp =ISGROUP;
//            [vc showInVC:self];
            [UIAlertView showWithTitle:str(strTitle_app) message:desc
                     cancelButtonTitle:@"Oui"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              }];
            //set value with key =@"access"
            NSDictionary*dic = arrData[btn_index];
            NSDictionary *oldDict = dic[@"connected"];
            
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(access) forKey:@"access"];
            
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            if (self.shouldBeginEditing) {
                [searchLoungesArray replaceObjectAtIndex:btn_index withObject:newDictConnect];

            }
            else
            {
                [loungesArray replaceObjectAtIndex:btn_index withObject:newDictConnect];

            }
            [self.tableControl reloadData];
        };
    }
}
-(void)fngotoGroup:(NSInteger)index
{
    NSMutableArray *arrData = [NSMutableArray new];
    if (self.shouldBeginEditing) {
        arrData =[searchLoungesArray copy];
    }
    else
    {
        arrData =[loungesArray copy];
    }
    NSDictionary *dic= [arrData objectAtIndex:index];

    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
     
    [self pushVC:viewController1 animate:YES];
    //
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
}
#pragma mark - MemberAction
-(IBAction)MemberButtonAction:(UIButton *)sender {
    NSInteger index =  sender.tag -1;
    [toussearchBar resignFirstResponder];
    NSMutableArray *arrData =[NSMutableArray new];
    if(self.shouldBeginEditing)
    {
        arrData =[searchLoungesArray copy];
    }
    else{
        arrData =[loungesArray copy];
        
    }
    strLoungeID=arrData[index][@"id"];
    [COMMON addLoading:self];
    WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
    [loungeSubscribeAction getGroupToutesSubscribeFriendAction:strLoungeID];
    loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"subscribers"]];
        
        for (int i=0; i<arrMembers.count; i++) {
            NSDictionary *dic =[arrMembers objectAtIndex:i];
            if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                [arrMembers removeObjectAtIndex:i];
            }
        }
        [self showPopupMembers:arrMembers];
    };
    
}

-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    AlertViewMembers *viewController1 = [[AlertViewMembers alloc]initWithArrayMembers:arrMembers];
    viewController1.expectTarget =ISGROUP;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         
         [self pushVC:friend animate:YES expectTarget:self.expectTarget];
     }];
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = viewController1;
    popup.presentingController = self;
    [self presentViewController:viewController1 animated:YES completion:nil];
}

@end
