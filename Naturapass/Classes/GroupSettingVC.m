//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupSettingVC.h"
#import "GroupParameter_Notification_Email.h"
#import "GroupParameter_Notification_Smartphones.h"
#import "CellKind2.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface GroupSettingVC ()
{
    NSArray * arrData;
}
@end

@implementation GroupSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name":str(strNotifications_email),
                                   @"image":@"ic_group_notify_email"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strNotifications_smartphones),
                                   @"image":@"ic_group_notify_smartphone"} copy];

    arrData =  [@[dic1,dic2] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];

}

#pragma mark - API

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];

    //FONT
    cell.label1.text = dic[@"name"];

    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"ic_arrow" ]];

    cell.constraint_control_width.constant = 10;
    cell.constraint_control_height.constant = 20;
    cell.constraint_image_width.constant =40;
    cell.constraint_image_height.constant =40;
    cell.constraintRight.constant = 5;
    btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant , cell.constraint_control_height.constant);
    [cell.viewControl addSubview:btn];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            GroupParameter_Notification_Email *viewController1 = [[GroupParameter_Notification_Email alloc] initWithNibName:@"GroupParameter_Notification_Email" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 1:
        {
            GroupParameter_Notification_Smartphones *viewController1 = [[GroupParameter_Notification_Smartphones alloc] initWithNibName:@"GroupParameter_Notification_Smartphones" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
        default:
            break;
    }
}
@end
