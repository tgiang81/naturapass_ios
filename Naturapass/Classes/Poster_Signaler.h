//
//  ChassesCreateV2.h
//  Naturapass
//

#import "PosterBaseVC.h"
#import "Define.h"

@interface Poster_Signaler : PosterBaseVC

@property (nonatomic, assign) BOOL isHideFavorite;

@property (nonatomic, assign) NEW_PUBSCREEN_TYPE iType;
@property (nonatomic, strong) NSMutableArray *arrData;

@property (nonatomic, strong) IBOutlet UIView *vBottom;
-(void) fnHideFavorite;

@end
