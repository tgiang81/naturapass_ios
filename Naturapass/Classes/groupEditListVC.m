//
//  chassesEditListVC.m
//  Naturapass
//
//  Created by Manh on 11/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "groupEditListVC.h"
#import "CellKind20.h"

#import "GroupCreate_Step1.h"
#import "GroupCreate_Step2.h"
#import "GroupCreate_Step3.h"
#import "GroupAdminListVC.h"
#import "GroupCreate_AddHunts.h"
#import "GroupCreate_Publication_Discussion_Allow.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface groupEditListVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
    
}
@end

@implementation groupEditListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strAdministrationDuGroupe);
    [self.btnTerminer setTitle:  str(strTERMINER) forState:UIControlStateNormal];

    self.btnTerminer.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name":str(kItem_1)} copy];
    NSMutableDictionary *dic2 = [@{@"name":str(kItem_2)} copy];
    NSMutableDictionary *dic3 = [@{@"name":str(kItem_3)} copy];
    NSMutableDictionary *dic4 = [@{@"name":str(kItem_4)} copy];
    NSMutableDictionary *dic5 = [@{@"name":str(strInviterDeMembres)} copy];
    NSMutableDictionary *dic6 = [@{@"name":str(kItem_9)} copy];
    NSMutableDictionary *dic7 = [@{@"name":str(strExclureDesMembres)} copy];
    arrData =  [@[dic1, dic3, dic2,dic4,dic5,dic6,dic7] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind20" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind20 *cell = nil;
    
    cell = (CellKind20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    
    //FONT
    cell.name.text = dic[@"name"];

    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSDictionary *dic = arrData[indexPath.row];
    if ([dic[@"name"] isEqualToString:str(strInformations)]) {
        GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
        [self pushVC:viewController1 animate:YES];

    }else if ([dic[@"name"] isEqualToString:str(kItem_2)]) {
        GroupCreate_Publication_Discussion_Allow *viewController1 = [[GroupCreate_Publication_Discussion_Allow alloc] initWithNibName:@"GroupCreate_Publication_Discussion_Allow" bundle:nil];
        viewController1.myTypeView = PUBLICATION_ALLOW_GROUP;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];

    }else if ([dic[@"name"] isEqualToString:str(kItem_3)]) {
        GroupCreate_Publication_Discussion_Allow *viewController1 = [[GroupCreate_Publication_Discussion_Allow alloc] initWithNibName:@"GroupCreate_Publication_Discussion_Allow" bundle:nil];
        viewController1.myTypeView = CHAT_ALLOW_GROUP;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];

    }else if ([dic[@"name"] isEqualToString:str(strTypeDacces)]) {
        GroupCreate_Step2 *viewController1 = [[GroupCreate_Step2 alloc] initWithNibName:@"GroupCreate_Step2" bundle:nil];
        [self pushVC:viewController1 animate:YES];

    }else if ([dic[@"name"] isEqualToString:str(strInviterDeMembres)]) {
        GroupCreate_Step3 *viewController1 = [[GroupCreate_Step3 alloc] initWithNibName:@"GroupCreate_Step3" bundle:nil];
        [self pushVC:viewController1 animate:YES];

    }else if ([dic[@"name"] isEqualToString:str(strGererLesAdminstrateours)]) {
        GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
        //mld
        [vc fnGroupId:[GroupCreateOBJ sharedInstance].group_id
              isAdmin:YES];
        [self pushVC:vc animate:YES expectTarget:ISGROUP];

    }else if ([dic[@"name"] isEqualToString:str(strExclureDesMembres)]) {
        GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
        //mld
        [vc fnGroupId:[GroupCreateOBJ sharedInstance].group_id
              isAdmin:NO];
        [self pushVC:vc animate:YES expectTarget:ISGROUP];

    }
    
}

-(IBAction)modifiAction:(id)sender
{
    [self gotoback];
}
@end
