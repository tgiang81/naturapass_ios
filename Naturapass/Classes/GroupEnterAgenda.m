//
//  ChassesMessView.m
//  Naturapass
//
//  Created by Giang on 7/16/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupEnterAgenda.h"
#import "Config.h"
#import "AppCommon.h"
#import "WebServiceAPI.h"
#import "Define.h"
#import "UIAlertView+Blocks.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TypeCellToutes.h" // neu muon back up tro lai doi TypeCellToutes ->TypeCell1
#import "GroupEnterOBJ.h"
#import "GroupSearchAlertView.h"
#import "GroupEnterMurVC.h"
#import "AlertViewMembers.h"
#import "CCMPopupTransitioning.h"
#import "FriendInfoVC.h"

#import "ChassesCreateV2.h"
#import "ChassesParticipeVC.h"
static NSString *typecell1 = @"TypeCellToutes";
static NSString *typecell1ID = @"TypeCell1ID";

@interface GroupEnterAgenda ()<UITableViewDelegate>
{
    NSString                    *strAccess;
    NSInteger                    indexLounge;
    WebServiceAPI *agendaServiceAPI;
}
@property (nonatomic, strong) TypeCellToutes *prototypeCell;
@property (nonatomic, strong) NSMutableArray              *messalonArray;;
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;

@end
@implementation GroupEnterAgenda

- (TypeCellToutes *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:typecell1ID];
    }
    
    return _prototypeCell;
}
-(void)intialization{
    
    //init table view cell
    [self.tableControl registerNib:[UINib nibWithNibName:typecell1 bundle:nil] forCellReuseIdentifier:typecell1ID];
    //
    self.messalonArray=[[NSMutableArray alloc]init];
    [self initRefreshControl];
    [self startRefreshControl];
    _lbMessage.text=EMPTY_GROUP_AGENDA;

    
}

//overwrite
- (void)insertRowAtTop {
    [self getAgendaGroup];
}

//overwrite
- (void)insertRowAtBottom {
    [self stopRefreshControl];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self intialization];
}
/*
 "hunts": [
 {
 "id": 532,
 "owner": {
 "id": 160,
 "fullname": "Manh ManhManh",
 "firstname": "Manh",
 "lastname": "ManhManh",
 "usertag": "manh-manhmanh",
 "courtesy": 0,
 "photo": "/uploads/users/images/thumb/3699cad9e4240bf3508bad7958dca8f1d0fcec34.jpeg",
 "relation": false
 },
 "geolocation": 0,
 "meeting": {
 "address": {
 "id": 47956,
 "latitude": "37.785834",
 "longitude": "-122.406417",
 "address": "San Francisco",
 "created": "2015-09-14T04:48:41+02:00"
 },
 "date_begin": "2015-09-14T04:48:00+02:00",
 "date_end": "2015-10-02T09:48:00+02:00"
 },
 "access": 1,
 "name": "mfrhtr",
 "loungetag": "mfrhtr",
 "description": "mfthrm",
 "nbSubscribers": 1,
 "photo": "/uploads/lounges/images/thumb/9956f58fe6150c2c06e4a0f2c65813e09520d854.jpeg",
 "connected": {
 "user": {
 "id": 160,
 "fullname": "Manh ManhManh",
 "firstname": "Manh",
 "lastname": "ManhManh",
 "usertag": "manh-manhmanh",
 "courtesy": 0,
 "photo": "/uploads/users/images/thumb/3699cad9e4240bf3508bad7958dca8f1d0fcec34.jpeg",
 "relation": false
 },
 "quiet": 0,
 "geolocation": false,
 "participation": 0,
 "access": 3
 }
 }
 ]
 */
#pragma mark - Webservice
-(void)getAgendaGroup
{
    NSString *group_id =[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    __weak typeof(self) wself = self;
    if (!agendaServiceAPI) {
        agendaServiceAPI =[WebServiceAPI new];
    }
    [agendaServiceAPI getGroupAgendaWithGroupID:group_id];
    agendaServiceAPI.onComplete =^(id response, int intCode)
    {
        [wself stopRefreshControl];
        if ([wself fnCheckResponse:response]) {
            [wself checkEmpty:wself.messalonArray];
            return ;
        }
        if (!response) {
            [wself checkEmpty:wself.messalonArray];
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            [wself checkEmpty:wself.messalonArray];
            return ;
        }
        if([response isKindOfClass:[NSDictionary class]]){
            [wself.messalonArray removeAllObjects];
            [wself.messalonArray addObjectsFromArray: response[@"group"][@"hunts"]];
            [wself.tableControl reloadData];
            [wself checkEmpty:wself.messalonArray];

        }

    };
}
-(void)checkEmpty:(NSMutableArray*)arr
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
    }
    else
    {
        self.lbMessage.hidden=NO;
    }
}
#pragma mark - TableView
-(void) setDataForMessCell:(TypeCellToutes*) cell withIndex:(NSIndexPath *)indexPath
{
    if([self.messalonArray count]>0){
        NSDictionary *dic = self.messalonArray[indexPath.row];
        //avatar
        NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        [cell.Img1 sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"]];
        //name
        NSString *strName=[NSString stringWithFormat:@"%@",dic[@"name"]];
        [cell.label1 setText:strName];
        //description
        NSString *strText=dic[@"description"];
        cell.label10.text=strText;
        //access
        strAccess=[NSString stringWithFormat:@"%@",dic[@"access"]];
        if([strAccess isEqualToString:@"0"])
            cell.label2.text=str(strPRIVATE);
        else if([strAccess isEqualToString:@"1"])
            cell.label2.text=str(strSEMIPRIVATE);
        
        else if([strAccess isEqualToString:@"2"])
            cell.label2.text=str(strPUBLIC);
        
        //start date
        NSString * dateStrings = [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_begin"]]];
        if(dateStrings !=nil)
            [cell.label5 setText:[NSString stringWithFormat:@"%@",dateStrings]];
        else
            [cell.label5 setText:@""];
        
        //number personnes validation<neu so nguoi bang 0 thi an di>
        if ( [dic[@"nbPending"] intValue] == 0) {
            cell.label12.hidden = YES;
        }else if ( [dic[@"nbPending"] intValue] > 1) {
            [cell.label12 setText:[NSString stringWithFormat:@"%@ %@",dic[@"nbPending"],str(strPersonnes_en_attente_de_validation)]];
        }else{
            [cell.label12 setText:[NSString stringWithFormat:@"%@ %@",dic[@"nbPending"],str(strPersonne_en_attente_de_validation)]];
        }
        
        //address
        NSString*strLocation;
        if(dic[@"meeting"][@"address"][@"address"]!=nil){
            strLocation=[NSString stringWithFormat:@"%@",dic[@"meeting"][@"address"][@"address"]];
            [cell.label9 setText:strLocation];
        }
        else {
            [cell.label9 setText:@""];
        }

        //start date
        NSString *outputStrings= [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_end"]]];
        if(outputStrings !=nil)
            cell.label7.text = [NSString stringWithFormat:@"%@", outputStrings];
        else
            cell.label7.text = @"";
        //set trang thai cho button subcribers:
        NSString *strConnectAccess= @"";
        if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
            strConnectAccess= [NSString stringWithFormat:@"%@",dic[@"connected"][@"access"]];
        }
        else
        {
            strConnectAccess=@"4";
        }
        NSInteger iAccess = 1000;
        if (strConnectAccess && ![strConnectAccess isEqualToString:@""]) {
            iAccess=[strConnectAccess intValue];
        }
        [cell.button2 setColorEnable:UIColorFromRGB(GROUP_TOUS_COLOR)];
        
        [cell.button2 removeTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button2 removeTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button2 removeTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];

        switch (iAccess)
        {
            case USER_INVITED:
                
            {
                
                
                
                
                [cell.button2 setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
                [cell.button2 setEnabled:YES];
                [cell.button2 addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.button2 setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
                [cell.button2 setBackgroundImage:[UIImage imageNamed:@"ajouter_inactivebg.png"] forState:UIControlStateNormal];
                [cell.button2 setEnabled:NO];
            }
                break;
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.button2 setTitle:str(strACCEDER) forState:UIControlStateNormal];
                [cell.button2 setEnabled:YES];
                [cell.button2 addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                
                [cell.button2 setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
                [cell.button2 setEnabled:YES];
                [cell.button2 addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
                break;
        }
        //button members
//        [cell.button1 setColorEnable:UIColorFromRGB(GROUP_TOUS_COLOR)];
//        [cell.button1 addTarget:self action:@selector(MemberButtonAction:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.button1 setTitle:@"PARTICIPANTS" forState:UIControlStateNormal];
        //
        if (iAccess == USER_ADMIN) {
            //number participants
            cell.label3.text =[NSString stringWithFormat:@"%d participants",[dic[@"nbParticipants"] intValue]];
            cell.label3.hidden =NO;
            cell.Img3.hidden =NO;


        }
        else
        {
            cell.label3.hidden =YES;
            cell.Img3.hidden =YES;
        }

        //tag
        cell.button2.tag=indexPath.row+2;
//        cell.button1.tag=indexPath.row+1;
        // setting theme
        [cell fnSettingCell:UI_GROUP_MUR_ADMIN];
        cell.imgBackGroundSetting.hidden=YES;
        cell.imgSettingSelected.hidden=YES;
        cell.imgSettingNormal.hidden=YES;
        //setIcon
        cell.Img5.image =[UIImage imageNamed:@"ic_members_white"];
        cell.Img6.image =[UIImage imageNamed:@"ic_apply"];

        [cell layoutIfNeeded];
    }
}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
        TypeCellToutes *cell=(TypeCellToutes *) cellTmp;
        [self setDataForMessCell:cell withIndex:(NSIndexPath *)indexPath ];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.messalonArray count];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    int height =0;
    if([self.messalonArray count]>0){
        [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
        CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
        
        height = size.height+1;
    }

    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([self.messalonArray count]>0){
            TypeCellToutes *cell = [tableView dequeueReusableCellWithIdentifier:typecell1ID forIndexPath:indexPath];
            [self configureCell:cell forRowAtIndexPath:indexPath];
            return cell;
    }
    return nil;
}
#pragma mark ---invite agenda
-(void)rejoindreCommitAction:(UIButton *)sender{
    if ([sender isSelected]) {
        return;
    }
    int index =(int)sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[self.messalonArray copy];
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),arrData[index][@"name"]];
    
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}
- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    int index =(int)sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[self.messalonArray copy];
    
    NSString *strLoungeID=arrData[index][@"id"];
    
    NSInteger accessValueNumber = [arrData[index][@"access"] integerValue];
    
    NSString *desc=@"";
    
    NSString *strNameGroup=arrData[index][@"name"];
    
    NSString *strTile=@"";
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        desc =[NSString stringWithFormat:str(strRejoinLoungPublic),strNameGroup];
        strTile = str(strRejoinAcceptPublic);
        
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
        
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
        
    }
    else
    {
        NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
    }
    
    
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];

    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [COMMON removeProgressLoading];
        if([respone isKindOfClass: [NSArray class] ]) {
            return ;
        }
        BOOL access= [respone[@"success"] boolValue];
        if (access) {
            NSDictionary*dic = arrData[index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(2) forKey:@"access"];
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [self.messalonArray replaceObjectAtIndex:index withObject:newDictConnect];
        }
        
        [self.tableControl reloadData];

        ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
        viewController1.dicChassesItem =arrData[index];
        viewController1.isInvitionAttend =YES;
        [viewController1 doCallback:^(NSString *strID) {
            GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
            
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup = arrData[index];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        }];
    };
}
#pragma mark - goto mur chasse
-(void) gotoGroup:(UIButton*)sender
{
    NSInteger tag = ((UIButton *)sender).tag-2;
    [self fngotoGroup:tag];
}
-(void)fngotoGroup:(NSInteger)index
{
    NSDictionary *dic = self.messalonArray[index];
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    if([dic[@"connected"][@"access"]isEqual:@3])
    {
        //admin
        viewController1.needRemoveSubItem = NO;
        
    }else{
        viewController1.needRemoveSubItem = YES;
        
    }
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
    
    
}
#pragma mark - rejoin
- (void)cellRejoindre:(UIButton *)sender
{
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[self.messalonArray copy];
    if ([sender isSelected]) {
        return;
    }
    else{
        NSInteger btn_index=sender.tag-2;
       NSString *strLoungeID=arrData[btn_index][@"id"];
        NSInteger accessValueNumber = [arrData[btn_index][@"access"] integerValue];
        NSString *desc=@"";
        NSString *strNameGroup=arrData[btn_index][@"name"];
        
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[NSString stringWithFormat:str(strRejoinLoungPublic),strNameGroup];
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
            
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
            
        }
        else
        {
            NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
        }
        [COMMON addLoading:self];
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        [LoungeJoinAction postLoungeJoinAction:strLoungeID];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            [COMMON removeProgressLoading];
            if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
                [self.tableControl reloadData];
                return ;
            }
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            NSInteger access= [respone[@"access"] integerValue];
            if (access==NSNotFound) {
                [self.tableControl reloadData];
                return ;
            }
            
            //set value with key =@"access"
            NSDictionary*dic = arrData[btn_index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(access) forKey:@"access"];
            
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [self.messalonArray replaceObjectAtIndex:btn_index withObject:newDictConnect];
            
            [self.tableControl reloadData];
//            GroupSearchAlertView *vc = [[GroupSearchAlertView alloc] initWithTitle:@"" Desc:desc ];
//            [self presentViewController:vc animated:NO completion:^{
//                
//            }];
            [UIAlertView showWithTitle:strTile message:desc
                     cancelButtonTitle:str(strYES)
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              }];
        };
    }
}
/*
 myfriends =     (
 );
 subscribers =     (
 {
 access = 3;
 geolocation = 0;
 isFriend =             {
 state = 1;
 type = 1;
 };
 participation = 1;
 quiet = 0;
 user =             {
 email = "a1@a1.com";
 firstname = A1;
 fullname = "A1 A1";
 id = 213;
 lastname = A1;
 profilepicture = "/img/interface/default-avatar.jpg";
 usertag = "a1-a1";
 };
 },
 */
#pragma mark - lismember
-(IBAction)MemberButtonAction:(UIButton *)sender {
    NSInteger index =  sender.tag -1;
    NSMutableArray *arrData =[NSMutableArray new];
        arrData =[self.messalonArray copy];

   NSString *strLoungeID=arrData[index][@"id"];
    [COMMON addLoading:self];
    __weak typeof(self) wself = self;
    WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
    [loungeSubscribeAction getLoungeSubscribeFriendAction:strLoungeID];
    loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"subscribers"]];
        
        for (int i=0; i<arrMembers.count; i++) {
            NSDictionary *dic =[arrMembers objectAtIndex:i];
            if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                [arrMembers removeObjectAtIndex:i];
            }
        }
        [self showPopupMembers:arrMembers];
    };
    
}

-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    AlertViewMembers *viewController1 = [[AlertViewMembers alloc]initWithArrayMembers:arrMembers];
    viewController1.expectTarget =ISGROUP;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         
         [self pushVC:friend animate:YES expectTarget:self.expectTarget];
     }];
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = viewController1;
    popup.presentingController = self;
    [self presentViewController:viewController1 animated:YES completion:nil];
}

-(IBAction)createAction:(id)sender
{
    [[ChassesCreateOBJ sharedInstance] resetParams];
    ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
    [self pushVC:viewController1 animate:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
@end
