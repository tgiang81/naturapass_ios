//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "HNKGooglePlacesAutocomplete.h"
#import "KSToastView.h"
#import "CLPlacemark+HNKAdditions.h"
typedef void (^AlertSearchMapCallback)(CLLocation *m_Placemark, NSString *addressString);
@interface AlertSearchMapVC : UIView<UITextFieldDelegate>
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
}
@property (nonatomic,copy) AlertSearchMapCallback callback;
@property (nonatomic,strong) IBOutlet UIButton *validerButton;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (nonatomic, assign) BOOL shouldBeginEditing;
@property(nonatomic,strong) IBOutlet UITextField *txtSearch;
@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;

-(void)doBlock:(AlertSearchMapCallback ) cb;
-(IBAction)validerAction:(id)sender;
-(void)showAlert;
-(instancetype)init;
@end
