//
//  ChassesMessView.h
//  Naturapass
//
//  Created by Giang on 7/16/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

//#import "MesSalonCustomCell.h"
//#import "MesSalonAdminCustomCell.h"

#import <MapKit/MapKit.h>
#import "ChassesBaseVC.h"
#import "ShortcutScreenVC.h"

@interface ChassesMurPassVC : ChassesBaseVC
{
    IBOutlet UIButton           *salonAdminButton;
    NSMutableArray              *messalonArray;
    NSString                    *strAccess;
    NSString                    *strParticipate;
    NSInteger                    deleteSaloninteger;
    NSString                    * outputString;
    NSArray                     *participatearray;
    NSString                    *strSalonText;
    IBOutlet UILabel            *salonLabel;
    NSInteger                    indexLounge;
    
    //check refreshHeader or refreshFooter
    BOOL isFooter;
    
    //Geoloaction
    
    double                          gpsLatitude;
    double                          gpsLongitude;
    double                          gpsAltitude;
    
}

@property (nonatomic,retain)NSString *strPush;
@property (nonatomic,retain)NSString *push_ID;
@property (nonatomic,retain)NSString *pushType;
@property (nonatomic,strong) ShortcutScreenVC* viewShortCut;
@property (weak, nonatomic) IBOutlet UIButton *btnShortCut;

-(void)getMesSalon;
-(void)getItemWithKind:(NSString*)mykind myid:(NSString*)myid;

@end
