//
//  CommentDetailVC.m
//  Naturapass
//
//  Created by Manh on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CommentDetailVC.h"
#import "WebServiceAPI.h"
#import "CommentCustomCell.h"
#import "MediaCell+Color.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "ASSharedTimeFormatter.h"

//mur
#import "MurVC.h"
#import "MurVC.h"
#import "MurFilterVC.h"
#import "MurSettingVC.h"
#import "SubNavigationMUR.h"

//gr
#import "GroupMesVC.h"
#import "GroupSettingVC.h"
#import "GroupSearchVC.h"
#import "NotificationVC.h"
#import "GroupEnterMurVC.h"
#import "FriendInfoVC.h"
#import "NSDate+Extensions.h"
#import "MapLocationVC.h"
#import "SAMTextView.h"
#import "MurEditPublicationVC.h"
#import "AlertEditCommentVC.h"
#import "MurPeopleLikes.h"
#import "LiveHuntOBJ.h"
#import "SVPullToRefresh.h"
#import "AllerMUR.h"
#import "AllerMURGPS.h"
#import "MapGlobalVC.h"

//group and agenda
#import "AgendaAndGroupCell.h"
#import "AgendaFromMapCell.h"
#import "GroupFromMapCell.h"
#import "GroupDetailCell.h"
#import "AgendaDetailCell.h"
#import "NSString+HTML.h"
//#import "AlertMapVC.h"
#import "ChassesParticipeVC.h"
#import "DetailFromMapVC.h"
#import "SignalerTerminez.h"
#import "AlertVC.h"
#import "CommentEditCell.h"
static NSString *MediaCellID = @"MediaCell";
static NSString *commentCustomCellID = @"commentCustomCellID";
static NSString *AgendaAndGroupID = @"AgendaAndGroupID";
static NSString *CellAgendaID = @"CellAgendaID";
static NSString *CellGroupID = @"CellGroupID";
static NSString *GroupDetailID = @"GroupDetailID";
static NSString *AgendaDetailID = @"AgendaDetailID";
static NSString *commentEditID = @"CommentEditCellID";

static int minHeight =55;
static int maxHeight =33*7;
static int detal =5;

@interface CommentDetailVC ()<UITextViewDelegate,IDMPhotoBrowserDelegate>
{
    __weak IBOutlet UILabel *lbLoadMore;
    IBOutlet UIView *vFoot;
    IBOutlet UIView *vFootContent;

    float iKeyboardHeight;
    
    NSInteger                       LIKECOUNT;
    NSInteger                       UNLIKECOUNT;
    NSInteger                       intText;
    
    BOOL                            isShowBufferCell;

    BOOL                            isCommentLoading;
    BOOL                            isCommentLike;
    BOOL                            isCommentUnLike;
    int                             kbHeight;
    int                             clickedbtntag;
    BOOL                            isLikeLoading;
    BOOL                            isUnLikeLoading;
    NSString                        *strLikes;
    NSString                        *strunLikes;
    NSDictionary                    *likesDict;
    //
    NSMutableArray                  *commentPostArray;
    NSMutableArray                  *commentViewGetArray;
    NSMutableDictionary             *likeUnlikeDict;
    IBOutlet UIView                     *messageView;
    //
    __weak IBOutlet NSLayoutConstraint *constraintMessageView;
    __weak IBOutlet NSLayoutConstraint *constraintMessageHeight;

    __weak IBOutlet UIButton *btnSend;
    __weak IBOutlet UIImageView *logoMsg;
    BOOL animateHeightChange;
    NSTimeInterval animationDuration;
    int widthImage;
//    NSMutableArray                  *arrAgenda;
//    NSMutableArray                  *arrGroup;
//    NSMutableArray                  *arrSection3;
    __weak IBOutlet UILabel *numberRemain;
    BOOL isCommenting;
    NSString *strEditComment;
    NSInteger scrollOffsetY;

}
@property (nonatomic,strong)   IBOutlet SAMTextView  *textView;
@property (nonatomic,assign) ARROW_TYPE arrowType;

@end
#pragma mark - viewdid
@implementation CommentDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.vContainer.backgroundColor = UIColorFromRGB(NEW_MUR_BACKGROUND_COLOR);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    vFoot.backgroundColor = [UIColor clearColor];
    [vFootContent.layer setMasksToBounds:YES];
    vFootContent.layer.cornerRadius= 4.0;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [numberRemain setText:[NSString stringWithFormat:@"%d/%d", 0,limitComment]];
    [numberRemain setTextColor:[UIColor darkGrayColor]];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRefresh) name: NOTIFY_REFRESH_MES object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPublicationEdit:) name: NOTIFY_REFRESH_MES_EDIT object:nil];

    widthImage = [UIScreen mainScreen].bounds.size.width * 286/304;
    [self intialization];
    
    [Flurry logEvent:@"PostDetaiFragment" timed:YES];

    UINavigationBar *navBar=self.navigationController.navigationBar;
    shareDetail = [[SettingActionView alloc] initSettingActionView];

    self.showTabBottom = TRUE;
    self.murV3 = TRUE;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_TINY_BAR_COLOR);
                
                //SUB
                [self addSubNav:nil];
            }else{
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                
                // Do any additional setup after loading the view.
                [self addMainNav:@"MainNavSignaler"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:@"COMMENTAIRES"];
                [subview.myDesc setText:@"Ajouter..."];
                subview.imgClose.hidden = TRUE;
                subview.btnClose.hidden = TRUE;
                subview.imgBackground.image = [UIImage imageNamed:@"mur_top_bar_bg"];
                //Change status bar color
                UINavigationBar *navBar=self.navigationController.navigationBar;
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
                
                [self addSubNav:nil];
                
                //change button image
                //            [logoMsg setImage: [UIImage imageNamed:@"ic_chasse_hangout"]];
                [btnSend setImage: [UIImage imageNamed:@"mur_ic_send"] forState:UIControlStateNormal];

            }

        }
            break;
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                [self addMainNav:@"MainNavLiveMap"];
                //SUB
                [self addSubNav:@"livechat"];
                
                [self setThemeTabVC:self];
                
                if ([LiveHuntOBJ sharedInstance].dicLiveHunt) {
                    [self.navLiveMap.lbHuntName setText:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"]];
                    [self.navLiveMap.lbHuntTime setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_end"]]];
                }
                
                [btnSend setImage:[[UIImage imageNamed:@"mur_ic_send"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                btnSend.tintColor =  UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
//                [logoMsg setImage:[[UIImage imageNamed:@"live_ic_send_chat"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
                
                
            }
            else
            {
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_TINY_BAR_COLOR);
                //SUB
                [self addSubNav:@"livechat"];
                [btnSend setImage: [UIImage imageNamed:@"mur_ic_send"] forState:UIControlStateNormal];

            }
            
            
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
            //            [logoMsg setImage: [UIImage imageNamed:@"ic_group_hangout"]];
            [btnSend setImage: [UIImage imageNamed:@"send_group"] forState:UIControlStateNormal];
            
        }
            break;
        case ISLOUNGE:
        {
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    self.dicOption =@{@"admin":@1};
                    
                }else{
                    self.dicOption =@{@"admin":@0};
                    
                }
                
            }else{
                self.dicOption =@{@"admin":@0};
            }
            
            
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                [self addMainNav:@"MainNavLiveMap"];
                //SUB
                [self addSubNav:@"livechat"];

                [self setThemeTabVC:self];
                
                if ([LiveHuntOBJ sharedInstance].dicLiveHunt) {
                    [self.navLiveMap.lbHuntName setText:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"]];
                    [self.navLiveMap.lbHuntTime setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_end"]]];
                }

                [btnSend setImage:[UIImage imageNamed:@"live_ic_send_chat"] forState:UIControlStateNormal];
                [logoMsg setImage:[UIImage imageNamed:@"live_ic_hangout_chat"]];
                

            }
            else
            {
                
                //add sub navigation
                    [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
                    [self addMainNav:@"MainNavMUR"];
                    
                    //Set title
                    MainNavigationBaseView *subview =  [self getSubMainView];
                    [subview.myTitle setText:str(strCHANTIERS)];
                    
                    //Change background color
                    subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                    
                    
                    [self addSubNav:@"SubNavigationGROUPENTER"];
                    
                    
                    if (self.needRemoveSubItem) {
                        [self removeItem:4];
                    }
                    
                    //change button image
                    
                    [btnSend setImage: [UIImage imageNamed:@"send_chasse"] forState:UIControlStateNormal];


            }
            
            
        }
            break;
        default:
            break;
    }
    //Refresh/loadmore
    [self initRefreshControl];

    _textView.placeholder =str(strAjouterUnCommentaire);

//    [self InitializeKeyboardToolBar];
//    [_textView setInputAccessoryView:self.keyboardToolbar];

    
    
    if (self.isNotifi) {
        [self.subview removeFromSuperview];
        [self addSubNav:@"SubNavigationMUR"];

        
        self.isNotifi =!self.isNotifi;
        [self gotoNotifi_leaves];
    }
    else
    {
        [self setDataWithPublication:_commentDic];
        //Get comment
        [self doGetCommentViewURL:NO];
    }
    
    //at first, just keep the shot item...then if user expand...let it be
    //keep 4 last comments.
    //if 4 + n
    //if n=1 (text 1)
    //if n > 1 (text 2)
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    iKeyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
}

#pragma mark -keyboar tool bar
- (void)resignKeyboard:(id)sender
{   
//    self.constraintBottom.constant = 0;
    [_textView resignFirstResponder];
}

-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://back
        {
            if ([self.mParent isKindOfClass: [ARViewController class]]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    
                }];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;
        case 1://back
        {
            if ([self.mParent isKindOfClass: [ARViewController class]]) {
                [self dismissViewControllerAnimated:YES completion:^{
                    
                }];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;

    }
}

-(void) initRefreshControl
{
    __weak CommentDetailVC *weakSelf = self;
    
    [self.tableControl addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
}

-(void) insertRowAtTop
{
    [self.tableControl reloadData];
    [self stopRefreshControl];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    CGRect frame = self.tableControl.tableHeaderView.frame;
    frame.size.height = 1;
    UIView *headerView = [[UIView alloc] initWithFrame:frame];
    [self.tableControl setTableHeaderView:headerView];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    NSDictionary *dic = nil;

    switch (self.expectTarget) {
        case ISMUR:
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
                
            }else{
            //Change status bar color
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            }
            
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            
        }
            break;
        case ISLOUNGE:
        {

            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];

            }else{
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];

            }
        }
            break;
    default:
            break;
    }
    
    [self setThemeNavSub:NO withDicOption:dic];
    
    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([_textView canBecomeFirstResponder]) {
        [_textView becomeFirstResponder];  // surely this line is called
    }
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    constraintMessageView.constant = 0;
    //
    //    if ([self.commentDelegate respondsToSelector:@selector(loadingCommentCount:)]) {
    //        [self.commentDelegate loadingCommentCount:commentDic];
    //    }
}

#pragma SUB - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    
    switch (self.expectTarget) {
            //special
        case ISCARTE:
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        default:
            break;
    }
}

- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: MPMoviePlayerPlaybackDidFinishNotification object: nil];
    [self dismissMoviePlayerViewControllerAnimated];
}
- (IBAction)fnShowFullComments:(id)sender {
    isShowBufferCell = NO;
    [self.tableControl reloadData];
}

#pragma mark - Intialize

-(void)intialization{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethod:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethodHide:) name:UIKeyboardDidHideNotification object:nil];
    intText= [[_commentDic valueForKey:@"totalComments"] integerValue];
    
    commentPostArray=[[NSMutableArray alloc]init];
    commentViewGetArray=[[NSMutableArray alloc]init];
    
//    LIKECOUNT = [[_commentDic valueForKey:@"likes"] integerValue];
//    UNLIKECOUNT =  [[_commentDic valueForKey:@"unlikes"] integerValue];
    
    isCommentLoading =YES;
    
    isCommentLike=YES;
    isCommentUnLike=YES;
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"MediaCell" bundle:nil] forCellReuseIdentifier:MediaCellID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CommentCustomCell" bundle:nil] forCellReuseIdentifier:commentCustomCellID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CommentEditCell" bundle:nil] forCellReuseIdentifier:commentEditID];

    self.tableControl.estimatedRowHeight = 160;
    
}
- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    kbHeight = CGRectGetHeight(keyboardFrameBeginRect);
    if (isCommenting) {
        constraintMessageView.constant = kbHeight - 44;
    }
    else
    {
        [self.tableControl setContentOffset:CGPointMake(0, scrollOffsetY + kbHeight + 44) animated: YES];
    }

//    [self scrollToBottom];
    
}
- (void)myNotificationMethodHide:(NSNotification*)notification
{
    constraintMessageView.constant = 0;
    [self.tableControl setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
}

-(void)setDataWithPublication:(NSDictionary*)dic
{
    _commentDic = dic;
}
-(void)refreshPublicationEdit:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    if (dic[@"publication"]) {
            if ([_commentDic[@"id"] intValue] == [dic[@"publication"][@"id"] intValue]) {
                _commentDic = [dic[@"publication"] copy];
                [self.tableControl reloadData];
            }
    }
}
-(void) doRefresh
{
    [self getPublictionItem:self.commentDic[@"id"]];
}
#pragma mark -  WebSevices
-(void)setDataComment:(NSArray*)arrTmp
{
    commentViewGetArray=[arrTmp mutableCopy];
    if([commentViewGetArray count] > 0)
    {
        commentViewGetArray=[(NSMutableArray *)[[NSSet setWithArray:commentViewGetArray] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO], nil]] mutableCopy];
        
        if([commentViewGetArray count] > 4)
        {
            //Show buffer cell: Afficher les n autres commentaires
            //==5 => Afficher l'autre commentaire
            isShowBufferCell = YES;
        }
    }
}
- (void)doGetCommentViewURL :(BOOL)isScroll {
    if (!_isHiddenComment) {
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj getPublicationCommentsAction:self.commentDic[@"id"]];
        __weak typeof(self) wself = self;
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (errCode == 404) {
                [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                }];
            }
            if ([wself fnCheckResponse:response]) return ;
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            NSMutableDictionary *tempdic = (NSMutableDictionary*)[_commentDic mutableCopy];
            [tempdic setValue:[NSNumber numberWithInt: [[response valueForKey:@"loaded" ] intValue] ] forKey:@"totalComments"];
            [self setDataWithPublication:tempdic];
            [self setDataComment: response[@"comments"]];
            [self.tableControl reloadData];
            
            if(isScroll)
            {
                NSInteger numberOfRows = [self.tableControl numberOfRowsInSection:1];
                
                if (numberOfRows) {
                    [self.tableControl scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:1] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                }
            }
        };
    }
    else
    {
        
    }
}
- (void)postCommentViewURL
{
    [COMMON addLoading:self];
    
    NSDictionary * postDict = @{ comment : @{@"content":[_textView.text emo_UnicodeEmojiString]}  };
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    //    if([_strPush isEqualToString:@"PUSH"])
    //        [serviceObj postPublicationCommentAction:_push_ID withParametersDic:postDict];
    //
    //    else
    [serviceObj postPublicationCommentAction:self.commentDic[@"id"] withParametersDic:postDict];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        //200...201 success response from RESTFUL
        [COMMON removeProgressLoading];
        if (errCode == 404) {
            
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if (!response) {
            return;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if([response[@"comment"] isKindOfClass:[NSDictionary class]])
        {
            _textView.text=@"";
            int remain = (int)_textView.text.length;
            [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain>limitComment?limitComment:remain,limitComment]];
            constraintMessageHeight.constant =minHeight;
            NSMutableDictionary *singleMesDic = [_commentDic mutableCopy];
            
            NSMutableArray *commentsArr = [singleMesDic[@"comments"][@"data"] mutableCopy];
//            ASLog(@"%@",response[@"comment"]);
            if([response valueForKey:@"comment"] != nil)
                [commentsArr addObject:[response valueForKey:@"comment"]];
            else if([response valueForKey:@"comments"] != nil)
            {
                commentsArr = [[response valueForKey:@"comments"] mutableCopy];
            }else{
                //Error
                return;
            }
            NSMutableDictionary *dic = [NSMutableDictionary new];
            [dic setObject:commentsArr forKey:@"data"];
            [singleMesDic setObject:dic forKey:@"comments"];
            [self setDataWithPublication:[singleMesDic mutableCopy]];
            // reload data mur
            if (_callback) {
                self.callback(self.commentDic[@"id"]);
            }
            _isHiddenComment = NO;
            [self doGetCommentViewURL:NO];
        }
        
    };
    
}
#pragma mark -  TextField delegate

- (BOOL)textFieldShouldReturn:(UITextField*)aTextField
{
    [aTextField resignFirstResponder];
    return YES;
}
#pragma mark -delete
-(void)deletePublicationAction
{
    NSString *publicationID = [_commentDic[@"id"] stringValue];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI deletePublicationAction: publicationID];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (errCode == 404) {
            
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if ([response[@"success"] boolValue]) {
            //update sqldb
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                
                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];

                NSString *strQuerry = [NSString stringWithFormat:@"DELETE FROM `tb_carte` WHERE `c_id` IN (%@) AND ( c_user_id=%@ OR c_user_id= %d);", publicationID, sender_id, SPECIAL_USER_ID];
                NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                
                [db  executeUpdate:tmpStr];

            }];
            
            //update in cache Mur pub
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_MUR_SAVE)   ];
            NSArray *arr = [[NSArray arrayWithContentsOfFile:strPath] mutableCopy];
            NSMutableArray *mutArr = [arr mutableCopy];
            for (NSDictionary*mDic in arr) {
                if ([mDic[@"id"] intValue] == [publicationID intValue]) {
                    [mutArr removeObject:mDic];
                    [mutArr writeToFile:strPath atomically:YES];
                    
                    if (_callbackWithDelItem) {
                        _callbackWithDelItem(_commentDic);
                    }
                    break;
                }
            }
            
            [self gotoback];
        }
    };

}
#pragma mark -  comment Actions

-(IBAction)commentLikeAction:(UIButton *)sender
{
    
    [COMMON addLoading:self];
    clickedbtntag= (int)[sender tag];
    NSMutableArray *commentLikeViewArray = [commentViewGetArray mutableCopy];
    NSMutableDictionary *likedic = [[commentLikeViewArray objectAtIndex:clickedbtntag] mutableCopy];
    NSString *commentID=[[commentViewGetArray objectAtIndex:clickedbtntag]valueForKey:@"id"];
    
    if(sender.selected == NO){
        [likedic setValue:[NSNumber numberWithInt:1] forKey:@"isUserLike"];
        [likedic setValue:[NSNumber numberWithInt:0] forKey:@"isUserUnlike"];
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj postPublicationCommentLikeAction:commentID];
        serviceObj.onComplete = ^(id response, int errCode){
            [COMMON removeProgressLoading];
            if (errCode == 404) {
                
                [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                }];
            }
            if (!response) {
                return ;
            }
            [self getCommentLikeItem:commentID];
        };
    }
    else{
        [self deleteCommentAction:clickedbtntag string:commentID];
    }
}

- (void)getCommentLikeItem:(NSString*)comment_ID {
    //request first page
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    __weak typeof(self) wself = self;
    [serviceObj getPublicationCommentsAction:self.commentDic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        if (errCode == 404) {
            
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableDictionary *tempdic = (NSMutableDictionary*)[_commentDic mutableCopy];
        [tempdic setValue:[NSNumber numberWithInt: [[response valueForKey:@"loaded" ] intValue] ] forKey:@"totalComments"];
        [self setDataWithPublication:tempdic];
        [self setDataComment: response[@"comments"]];
        // Add them in an index path array
        [self.tableControl reloadData];
        
    };
}

-(void)deleteCommentAction:(NSInteger )sendervalue string:(NSString *)comment_id
{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj deletePublicationCommentActionAction:comment_id];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (errCode == 404) {
            
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if (!response) {
            return ;
        }
        [self getCommentLikeItem:comment_id];
    };
}
-(IBAction)commentViewAction:(UIButton *)sender
{
    _isHiddenComment = !_isHiddenComment;
    [self.tableControl reloadData];
}
#pragma mark - LikeAction

-(IBAction)likeButtonAction:(UIButton *)sender
{
    
    [COMMON addLoading:self];
    NSString *pubicationID=[[_commentDic valueForKey:@"id"] stringValue];
    
    if([[_commentDic valueForKey:@"isUserLike"] integerValue]==1)
    {
        //Current like -> unlike
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj deletePublicationActionAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (errCode == 404) {
                
                [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                }];
            }
            if (!response) {
                return ;
            }
            
//            [self getPublictionItem:pubicationID];
            [self updateLike:0 withDicLike:response];
        };
    }
    else
    {
        //Current unlike
        
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj postPublicationLikeAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (errCode == 404) {
                
                [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                }];
            }

            if (!response) {
                return ;
            }
//            [self getPublictionItem:pubicationID];
            [self updateLike:0 withDicLike:response];
        };
    }
}
-(void)updateLike:(NSInteger)index withDicLike:(NSDictionary*)dicLike
{
    if (dicLike[@"likes"]) {
        NSMutableDictionary *dic = [self.commentDic mutableCopy];
        [dic setObject:dicLike[@"likes"] forKey:@"likes"];
        [dic setObject:dicLike[@"likedusers"] forKey:@"likedusers"];
        BOOL isUserLike = ![dic[@"isUserLike"] boolValue];
        [dic setObject:@(isUserLike) forKey:@"isUserLike"];
        self.commentDic = [dic copy];
        [self.tableControl reloadData];
        if (_callback) {
            self.callback(self.commentDic[@"id"]);
        }
    }
}



- (void)getPublictionItem:(NSString*)public_id {
    //request first page
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getPublicationsActionItem:public_id];
    __weak typeof(self) wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        if (errCode == 404) {
            
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if([wself fnCheckResponse:response]) return;

        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        NSArray*arrGroups = response[@"publication"];
        
        
        if (arrGroups.count > 0) {
            //reload header
            [self setDataWithPublication:response[@"publication"]];
            // Add them in an index path array
            [self.tableControl reloadData];
        }
    };
}

- (void)LoadLikeandUnLike {
    [self getPublictionItem:self.commentDic[@"id"]];
    
    
    if (_callback) {
        self.callback(self.commentDic[@"id"]);
    }
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
    
}
#pragma mark - action
-(IBAction)sendButtonAction:(id)sender
{
    [_textView resignFirstResponder];
    
    if([_textView.text length] > 0 && [_textView.text length] <= 5000){
        
        NSString *capitalized = [[[_textView.text substringToIndex:1] uppercaseString] stringByAppendingString:[_textView.text substringFromIndex:1]];
        _textView.text=capitalized;
        
        isCommentLoading = YES;
        
        [self postCommentViewURL];
        //        if([_strPush isEqualToString:@"PUSH"])
        //
        //            intText= [[_commentDic valueForKey:@"totalComments"] integerValue];
        
        intText = intText + 1;
    }
    
}

//https://naturapass.e-conception.fr/uploads/publications/videos/mp4/774441ec1fea69696f3394976095e24005d5cc34.mp4
//photo = "/uploads/users/images/thumb/8b2692d0b602cdf988a29c01ccf91a866303b58a.jpeg";


#pragma mark - action tableview
-(IBAction)commentDetailAction:(id)sender{
    if ([_commentDic[@"media"] isKindOfClass:[NSDictionary class]])
    {
        
        NSString *strVideo= [_commentDic[@"media"][@"type"] stringValue];
        
        if ([strVideo isEqualToString:@"101"] )
        {
            NSString *strVideoURL=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[_commentDic valueForKey:@"media"]valueForKey:@"path"]];
            strVideoURL=[strVideoURL stringByReplacingOccurrencesOfString:@"videos/resize" withString:@"videos/mp4"];
            strVideoURL=[strVideoURL stringByReplacingOccurrencesOfString:@".qt" withString:@".mp4"];
            strVideoURL=[strVideoURL stringByReplacingOccurrencesOfString:@".jpeg" withString:@".mp4"];
            
            NSURL *videoURL =[NSURL URLWithString:strVideoURL];
            MPMoviePlayerViewController *  moviePlayer = nil;
            
            //
            if ([COMMON isReachable]) {
                moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
            }
            else
            {
                if (_commentDic[@"kFileData"]) {
                    NSString *videoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:_commentDic[@"kFileData"]];
                    moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL: [NSURL fileURLWithPath:videoFile ]];

                }
            }
            [[moviePlayer moviePlayer] prepareToPlay];
            
            ASLog(@"play movie");
            
            ASLog(@"movie URL: %@", strVideoURL);
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(moviePlayBackDidFinish:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:moviePlayer];
            
            [self presentMoviePlayerViewControllerAnimated:moviePlayer];
            return;
        } else {   // photo 100
            
            // Create an array to store IDMPhoto objects
            NSMutableArray *photos = [NSMutableArray new];
            
            // url
            NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[_commentDic valueForKey:@"media"] valueForKey:@"path"]];
            strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".jpeg"];
            strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4" withString:@".jpeg"];
            strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".jpeg"];
            
            strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
            NSURL *urlImage = [NSURL URLWithString:strImage];
            
            // timer
            NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            NSDateFormatter *outputFormatter = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
            [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
            [ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy HH:mm" forFormatter: outputFormatter];
            NSString *relativeTime = [_commentDic valueForKey:@"created"];
            NSDate * inputDate = [ inputFormatter dateFromString:relativeTime ];
            NSString * outputString = [ outputFormatter stringFromDate:inputDate ];
            NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];
            
            IDMPhoto *aphoto = nil;
            if ([COMMON isReachable]) {
                aphoto = [IDMPhoto photoWithURL:urlImage];
            }
            else
            {
                if (_commentDic[@"kFileData"]) {
                    NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:_commentDic[@"kFileData"]];
                    aphoto = [IDMPhoto photoWithURL:[NSURL fileURLWithPath:photoFile ]];
                }else{
                    aphoto = [IDMPhoto photoWithURL:urlImage];
                }
            }

            
            
            aphoto.caption = strDateLocation;
            [photos addObject:aphoto];
            // Create and setup browser
            IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
            browser.delegate = self;
            
            // Show
            [self presentViewController:browser animated:YES completion:nil];
        }
    }
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void) fnAller
{
    NSDictionary *dic = _commentDic;
    
    AllerMUR *vc = [[AllerMUR alloc] initWithData:dic];
    
    [vc doBlock:^(NSInteger index) {
        switch (index) {
            case 0:
            {
                //cancel
            }
                break;
            case 1:
            {
                MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
                mapVC.isSubVC = YES;
                mapVC.simpleDic = dic;
                mapVC.isLiveHuntDetail = YES;
                [self pushVC:mapVC animate:YES expectTarget:self.expectTarget];
                
                //to naturapass
                /*
                MapGlobalVC *mapVC = [[MapGlobalVC alloc]initWithNibName:@"MapGlobalVC" bundle:nil];
                mapVC.simpleDic = @{@"latitude":dic[@"geolocation"][@"latitude"],
                                    @"longitude":dic[@"geolocation"][@"longitude"]};
                
                [self pushVC:mapVC animate:YES expectTarget:ISCARTE];
                */
            }
                break;
            case 2:
            {
                //self add new VIEW.
                NSString *strLat =@"";
                NSString *strLng =@"";
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                double lat = appDelegate.locationManager.location.coordinate.latitude;
                double lng = appDelegate.locationManager.location.coordinate.longitude;
                
                strLat = dic[@"geolocation"][@"latitude"];
                strLng = dic[@"geolocation"][@"longitude"];
                
                AllerMURGPS *vc = [[AllerMURGPS alloc] initWithData:nil];
                
                [vc doBlock:^(NSInteger index) {
                    
                    switch (index) {
                            
                        case 0:
                        {}break;
                            
                        case 1:
                        {
                            //cancel
                            // Plans
                            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
                            NSURL *url =[NSURL URLWithString:str];
                            
                            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                                [[UIApplication sharedApplication] openURL:url];
                            } else {
                                NSLog(@"Can't use applemap://");
                            }
                            
                        }
                            break;
                        case 2:
                        {
                            //Maps
                            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
                            
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
                            }
                            
                        }
                            break;
                        case 3:
                        {
                            //Waze
                            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"waze://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                // Waze is not installed. Launch AppStore to install Waze app
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
                            }
                            
                        }
                            break;
                            
                    }
                    
                }];
                [vc showAlert];
                
            }
                break;
            default:
                break;
        }
    }];
    [vc fnColorWithExpectTarget:self.expectTarget];
    [vc showAlert];
}


#pragma mark -  TableView Delegates

- (void)configureCellHeader:(UITableViewCell *)cellTmp
{

    if ([cellTmp isKindOfClass:[MediaCell class]])
    {
        MediaCell *cell = (MediaCell *)cellTmp;
        [cell setThemeWithFromScreen:self.isLiveHuntDetail?ISLIVEMAP:self.expectTarget];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

        NSDictionary *dic = _commentDic ;

                [cell.btnTitle addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.btnProfile addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
                
                
                NSString *strImage=@"";
                if (dic[@"owner"][@"profilepicture"] != nil) {
                    strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                }else{
                    strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
                }
                NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
                
                [cell.imageProfile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
                cell.contraintHeightImageContent.constant = 0;
                cell.imageContent.contentMode = UIViewContentModeScaleToFill;
                if   ( (dic[@"media"] != nil) &&   [dic[@"media"] isKindOfClass: [ NSDictionary class]] )
                {
                    if ([dic[@"media"] isKindOfClass:[NSDictionary class]]) {
                        NSString *strVideo= [dic[@"media"][@"type"] stringValue];
                        
                        //video
                        if ([strVideo isEqualToString:@"101"] ){
                            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"media"][@"path"]];
                            strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".jpeg"];
                            strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4" withString:@".jpeg"];
                            strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4.mp4" withString:@".jpeg"];
                            strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".mp4.jpeg"];
                            
                            cell.imgIcon.hidden = NO;
                            
                        } else {
                            //image
                            cell.imgIcon.hidden = YES;
                            
                            strImage=[NSString stringWithFormat:@"%@%@.jpeg",IMAGE_ROOT_API,dic[@"media"][@"path"]];
                            strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".jpeg"];
                        }
                        
                        url = [NSURL URLWithString:strImage];
                        
//                        __weak MediaCell *weakCell = cell;
//                        __weak typeof(self) weakSelf = self;
                        float heightImg = [self fnResize:CGSizeMake([dic[@"media"][@"size"][@"width"] floatValue], [dic[@"media"][@"size"][@"height"] floatValue]) width:widthImage].height;
//                        if (heightImg > widthImage*9/16) {
//                            heightImg = widthImage*9/16;
//                        }
                        cell.contraintHeightImageContent.constant = heightImg;

                        [cell.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                            /*
                            //ok reload
                            if (image) {
                                    UIImage *tempImg = [self fnResizeFixWidth:image :widthImage];
                                    
                                    weakCell.imageContent.image = tempImg;
                            }
                            
                            if ( [weakSelf.tableControl.visibleCells containsObject:weakCell]) {
                                [weakSelf.tableControl reloadData];
                            }
                             */
                            

                        }];
                        
                        cell.imageContent.indexPath =0;
                        [cell.imageContent setOncallback:^(NSInteger index)
                         {
                             [self commentDetailAction:nil];
                             
                         }];
                    }
                }
                else
                {
                    //text
                    cell.imgIcon.hidden = YES;
                    cell.imageContent.image = nil;
                    [cell.imageContent fnRemoveClick];
                }
                
                [cell.location addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                //Assign Values in Cell
                [cell assignValues:dic];
        
        [cell setCbClickObs:^(UIButton*theBtn)
         {
             NSMutableDictionary *dicTmp = [_commentDic mutableCopy];
             BOOL readmore = ![dicTmp[@"readobs"] boolValue];
             [dicTmp setObject:@(readmore) forKey:@"readobs"];
             _commentDic = [dicTmp copy];
             [self.tableControl beginUpdates];
             NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
             [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
             [self.tableControl endUpdates];
         }];
        cell.myContent.lineBreakMode =  NSLineBreakByWordWrapping;
        cell.myContent.numberOfLines = 0;
            [cell.btnLike addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //Is me -> edit + Del
            //friend -> Signal
            [cell.btnSetting addTarget:self action:@selector(settingAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnLikesList addTarget:self action:@selector(displayLikesList:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnComment addTarget:self action:@selector(commentViewAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnComment.tag = COMMENTBUTTONTAG;
        cell.viewArrowComment.hidden = _isHiddenComment;
        [cell.btnShareDetail addTarget:self action:@selector(shareDetailAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell layoutIfNeeded];
        if (self.isLiveHuntDetail) {
            [cell.imgSettingNormal setImage:[[UIImage imageNamed:@"live_ic_setting"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
            cell.imgSettingNormal.tintColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        }
    }
    
}
- (void) userProfileFromTheirNameAction:(id)sender
{
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[_commentDic[@"owner"][@"id"] integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic:[_commentDic objectForKey:@"owner"]];
    [self pushVC:friend animate:YES expectTarget:ISMUR];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (_commentDic) {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        if (_isHiddenComment) {
            return 0;
        }
        else
        {
            if (isShowBufferCell) {
                return 4;
            }
            
            return [commentViewGetArray count];
        }
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)configureEditCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[CommentEditCell class]])
    {
        CommentEditCell *cell = (CommentEditCell *)cellTmp;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.iAmScreen = self.isLiveHuntDetail == YES? ISLIVEMAP : self.expectTarget;
        
        NSDictionary *dic = [commentViewGetArray objectAtIndex:indexPath.row] ;
        [cell assignValuesHeaderSection:dic];
        
            NSString *strCommentUserName;
            
            if(commentViewGetArray.count > 0){
                
                if(![[commentViewGetArray objectAtIndex:indexPath.row] isEqual: @"<null>"]||commentViewGetArray!=nil) {
                    strCommentUserName=[NSString stringWithFormat:@"%@ %@",[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"owner"]valueForKey:@"firstname"],[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"owner"]valueForKey:@"lastname"]];
                    
                    strLikes=[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"likes"] stringValue];
                    strunLikes=[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"unlikes"] stringValue];
                    
                    
                    NSString *date = [[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"created"];
                    
//                    “Posté le”  + dd/MM/yyyy à HH:mm

                    NSString *strDate = [NSDate convertAgendaToNSTring:date];
                    cell.commentdateLabel.text= [NSString stringWithFormat:@"Posté le %@", strDate];

                    
                    [cell.commentTitleLabel setText:strCommentUserName];
                    
                    NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[[commentViewGetArray objectAtIndex:indexPath.row]  valueForKey:@"owner"]valueForKey:@"photo"]];
                    strImage = [[strImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    NSURL * url = [[NSURL alloc] initWithString:strImage];
                    
                    [cell.commentUserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
                    
                    [cell.likeButton addTarget:self action:@selector(commentLikeAction:) forControlEvents:UIControlEventTouchUpInside];
                    cell.likeButton.tag=indexPath.row;
                    
                    switch (self.expectTarget) {
                        case ISMUR:
                        case ISCARTE:
                            if (self.isLiveHuntDetail) {
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
                                
                            }else
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                            
                            break;
                        case ISGROUP:
                            [cell.commentTitleLabel setTextColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
                            
                            break;
                        case ISLOUNGE:
                            if (self.isLiveHuntDetail) {
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
                                
                            }else
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                            
                            break;
                            
                        default:
                            break;
                    }
                    
                    cell.selectionStyle = NO;
                    cell.tvEditText.delegate = self;
                    
                    
                    cell.tvEditText.text = commentViewGetArray[indexPath.row][@"content"];
                    [cell fnNumberRemain:cell.tvEditText];
                    int index = (int)indexPath.row;
                    [cell doBlock:^(NSDictionary *dicBack) {
                        
                        if ([dicBack[@"index"] intValue]==1) {
                            [COMMON addLoading:self];
                            NSDictionary * postDict = @{ comment : @{@"content":[dicBack[@"comment"]emo_UnicodeEmojiString]}  };
                            
                            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                            
                            [serviceObj PUT_COMMENT_PUBLICATION_ACTION:dic[@"id"] withParametersDic:postDict];
                            
                            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                [COMMON removeProgressLoading];
                                if (errCode == 404) {
                                    
                                    [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                                    }];
                                }
                                if (!response) {
                                    return;
                                }
                                
                                if([response isKindOfClass: [NSArray class] ]) {
                                    return ;
                                }
                                if (response[@"success"]) {
                                    NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dic];
                                    [dicMul setObject:@(FALSE) forKey:@"isEdit"];
                                    [dicMul setValue:dicBack[@"comment"] forKey:@"content"];
                                    [commentViewGetArray replaceObjectAtIndex:index withObject:dicMul];
                                    [self.tableControl beginUpdates];
                                    [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                                    [self.tableControl endUpdates];
                                    
                                }
                                
                            };
                            
                            
                        }
                        else
                        {
                            NSMutableDictionary *dicMul = [commentViewGetArray[index] mutableCopy];
                            [dicMul setObject:@(FALSE) forKey:@"isEdit"];
                            [commentViewGetArray replaceObjectAtIndex:index withObject:dicMul];
                            
                            [self.tableControl beginUpdates];
                            [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                            [self.tableControl endUpdates];
                        }
                        
                    }];

                }
            }

    }
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[CommentCustomCell class]])
    {
        CommentCustomCell *cell = (CommentCustomCell *)cellTmp;
        
        cell.iAmScreen = self.isLiveHuntDetail == YES? ISLIVEMAP : self.expectTarget;
        
        NSDictionary *dic = [commentViewGetArray objectAtIndex:indexPath.row] ;
        [cell assignValuesHeaderSection:dic];

        {
            NSString *strCommentUserName;
            
            if(commentViewGetArray.count > 0){
                
                if(![[commentViewGetArray objectAtIndex:indexPath.row] isEqual: @"<null>"]||commentViewGetArray!=nil) {
                    strCommentUserName=[NSString stringWithFormat:@"%@ %@",[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"owner"]valueForKey:@"firstname"],[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"owner"]valueForKey:@"lastname"]];
                    
                    strLikes=[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"likes"] stringValue];
                    strunLikes=[[[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"unlikes"] stringValue];
                    
                    
                    NSString *date = [[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"created"];
                    
                    // Posté le" + date and hour
                    NSString *strDate = [NSDate convertAgendaToNSTring:date];
                    cell.commentdateLabel.text= [NSString stringWithFormat:@"Posté le %@", strDate];

                    
                    [cell.commentTitleLabel setText:strCommentUserName];
                    
                    NSDictionary *attrs = @{ NSForegroundColorAttributeName : UIColorFromRGB(MUR_MAIN_BAR_COLOR),NSFontAttributeName: cell.commentLabel.font};
                    cell.commentLabel.truncationTokenStringAttributes = attrs;
                    cell.commentLabel.truncationTokenString = @"...Voir plus";
                    UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture:)];
                    
                    readMoreGesture.numberOfTapsRequired = 1;
                    [cell.commentLabel addGestureRecognizer:readMoreGesture];
                    cell.commentLabel.tag = indexPath.row + 1001;
                    if ([dic[@"readmore"] boolValue]) {
                        cell.commentLabel.lineBreakMode =  NSLineBreakByWordWrapping;
                        cell.commentLabel.numberOfLines = 0;
                        
                    }
                    else
                    {
                        cell.commentLabel.lineBreakMode =  NSLineBreakByTruncatingTail;
                        cell.commentLabel.numberOfLines = 3;
                        
                    }
                    [cell setCommentText: [[commentViewGetArray objectAtIndex:indexPath.row]valueForKey:@"content"]];
                    
                    NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[[commentViewGetArray objectAtIndex:indexPath.row]  valueForKey:@"owner"]valueForKey:@"photo"]];
                    strImage = [[strImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    NSURL * url = [[NSURL alloc] initWithString:strImage];
                    
                    [cell.commentUserImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
                    
                    [cell.likeButton addTarget:self action:@selector(commentLikeAction:) forControlEvents:UIControlEventTouchUpInside];
                    cell.likeButton.tag=indexPath.row;
                    
                    switch (self.expectTarget) {
                        case ISMUR:
                        case ISCARTE:
                            if (self.isLiveHuntDetail) {
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
                                
                            }else
                            [cell.commentTitleLabel setTextColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                            
                            break;
                        case ISGROUP:
                            [cell.commentTitleLabel setTextColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
                            
                            break;
                        case ISLOUNGE:
                            if (self.isLiveHuntDetail) {
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];

                            }else
                                [cell.commentTitleLabel setTextColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                            
                            break;
                            
                        default:
                            break;
                    }
                    
                    cell.selectionStyle = NO;
                    cell.backgroundColor = [UIColor clearColor];
                    cell.contentView.backgroundColor = [UIColor clearColor];
                }
            }
        }
        
        
        
        if ([dic[@"isUserLike"] integerValue] == 1) {
            cell.iconLike.image = [UIImage imageNamed:@"ic_mur_love"];
            
        }else{
            cell.iconLike.image = [UIImage imageNamed:@"ic_mur_love_inactive"];
        }
        
        cell.btnSetting.tag=indexPath.row+100;
        if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
            [cell createMenuList:@[str(strModifier),str(strSupprimer)] ];
            [cell.btnSetting addTarget:self action:@selector(settingCommentAction:) forControlEvents:UIControlEventTouchUpInside];

        }else{
            cell.imgSettingNormal.hidden=YES;
            [cell.btnSetting removeTarget:self action:@selector(settingCommentAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.viewMenu.backgroundColor = UIColorFromRGB(0x8394A4);
        cell.imgBackGroundSetting.image = nil;
        cell.imgBackGroundSetting.backgroundColor = UIColorFromRGB(0x8394A4);
        [cell.imgSettingSelected setImage:[UIImage imageNamed:@"ic_mur_more"]];
        [cell.imgSettingNormal setImage:[UIImage imageNamed:@"ic_mur_more"]];
    }
}

//FOOTER
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if (isShowBufferCell && section == 1) {
        
        if([commentViewGetArray count] <= 4)
        {
            return 0.1f;
        }
        
        return 44;
    }


    return 0.1f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (isShowBufferCell && section == 1) {
        //set text for content
        
        //Show buffer cell: Afficher les n autres commentaires
        //==5 => Afficher l'autre commentaire
        /*
        int leftComments = mPublication.getTotalcomments() - commentList.size();
        if (leftComments == 0) {
            commentListView.removeFooterView(mFooterView);
        } else if(leftComments > 0){
            if( leftComments == 1){
                Spanned sp = Html.fromHtml(getString(R.string.load_more_1_comment));
                mTextView.setText(sp);
            } else {
                Spanned sp = Html.fromHtml(getString(R.string.load_more_n_comment, leftComments));
                mTextView.setText(sp);
            }
        }
        */
        if([commentViewGetArray count] == 5)
        {
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Afficher l'autre commentaire"];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            lbLoadMore.attributedText = attributeString;

        }
        else if([commentViewGetArray count] <= 4)
        {
            return nil;
        }
        else{
            // > 5
            NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Afficher les %d autres commentaires", (int) commentViewGetArray.count - 4 ]];
            [attributeString addAttribute:NSUnderlineStyleAttributeName
                                    value:[NSNumber numberWithInt:1]
                                    range:(NSRange){0,[attributeString length]}];
            lbLoadMore.attributedText = attributeString;

        }
        
        return vFoot;
    }
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        MediaCell *cell = [tableView dequeueReusableCellWithIdentifier:MediaCellID forIndexPath:indexPath];
        [self configureCellHeader:cell];
        return cell;
        
    }
    else
    {
        NSDictionary *dic = [commentViewGetArray objectAtIndex:indexPath.row] ;
        if ([dic[@"isEdit"] boolValue]) {
            CommentEditCell *cell = [tableView dequeueReusableCellWithIdentifier:commentEditID forIndexPath:indexPath];
            [self configureEditCell:cell forRowAtIndexPath:indexPath];
            return cell;
        }
        else
        {
            CommentCustomCell *cell = [tableView dequeueReusableCellWithIdentifier:commentCustomCellID forIndexPath:indexPath];
            [self configureCell:cell forRowAtIndexPath:indexPath];
            return cell;
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_textView resignFirstResponder];
}
#pragma mark - callback
-(void)setCallback:(CommentDetailVCCallback)callback
{
    _callback=callback;
}
-(void)doCallback:(CommentDetailVCCallback)callback
{
    self.callback=callback;
}

- (IBAction)locationButtonAction:(id)sender
{
    //goback if i am from carte
    if (self.expectTarget == ISCARTE) {
        [self gotoback];
    }else{
        if ([self.commentDic[@"geolocation"] isKindOfClass: [NSDictionary class]]) {
            MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
            mapVC.isSubVC = YES;
            
            mapVC.simpleDic = self.commentDic;
            [self pushVC:mapVC animate:YES];
        }
    }
}

#pragma mark - menu
-(IBAction)settingAction:(UIButton *)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    // Hide already showing popover
    [cell show];
    __weak CommentDetailVC *wself =self;
    [cell setCallBackGroup:^(NSInteger ind)
     {
         if ( [_commentDic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
             switch (ind) {
                 case 0:
                 {
                     [wself editerAction];
                 }
                     break;
                 case 1:
                 {
                     NSString *strContent = str(strVouloirSupprimerCettePublication);
                     
                     [UIAlertView showWithTitle:str(strMessage24)
                                        message:strContent
                              cancelButtonTitle:str(strOui)
                              otherButtonTitles:@[str(strNon)]
                                       tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                           if (buttonIndex == [alertView cancelButtonIndex]) {
                                               [wself deletePublicationAction];
                                           }
                                           else
                                           {
                                               
                                           }
                                       }];
                 }
                     break;
                 case 2:
                 {
                     [self fnAller];
                     
                 }
                     break;
                 default:
                     break;
             }
             
         }else{
             switch (ind) {
                     
                 case 0:
                 {
                     [wself signalerAction];
                     
                 }
                     break;
                 case 1:
                 {
                     [wself blackListerAction];
                 }
                     break;
                 case 2:
                 {
                     [self fnAller];
                     
                 }
                     break;
                 default:
                     break;
             }
         }
         
         
     }];
}
-(void)editerAction
{
    NSDictionary *dic = [_commentDic copy] ;
    [[PublicationOBJ sharedInstance]  fnSetValueForKeyWithPublication:dic];
    
    [PublicationOBJ sharedInstance].mParentVC =self;

    SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR];

}
#pragma mark - sgnaler

- (IBAction)signalerAction{
    
    AlertVC *vc = [[AlertVC alloc] initAlertSignalez];
    
    [vc doBlock: ^(NSInteger index, NSString *strContent) {
        
        switch ((int)index) {
                //ACCUEIL -> Homepage
            case 0:
            {
            }
                break;
                
            case 1:
            {
                NSDictionary * postDict = @{@"publication": @{@"signal":@1, @"explanation":str(strThis_publication_has_content_issue)} };
                
                WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                
                [serviceObj postPublicationSignalAction: _commentDic[@"id"] withParametersDic:postDict];
                
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    if (errCode == 404) {
                        
                        [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                        }];
                    }
                    NSLog(@"%@",response);
                    
                };
            }
                break;
                
            default:
                break;
        }
    }];
    [vc showAlert];
    
    

}
-(void)blackListerAction
{
    NSDictionary *dic = _commentDic ;
    
    NSString *strContent =[NSString stringWithFormat: str(strVoulez_vous_blacklister),dic[@"owner"][@"fullname"]];
    
    [UIAlertView showWithTitle:str(strBlacklister)
                       message:strContent
             cancelButtonTitle:str(strOUI)
             otherButtonTitles:@[str(strNON)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              [COMMON addLoading:self];
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj postUserLock:@{@"id": dic[@"owner"][@"id"]}];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  if (errCode == 404) {
                                      
                                      [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                                      }];
                                  }
                                  [self gotoback];
                                  //refresh 
                                  [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES object: nil userInfo: nil];

                              };
                          }
                          else
                          {
                              
                          }
                      }];
}
#pragma mark - menu comment
-(IBAction)settingCommentAction:(UIButton *)sender
{
    NSInteger index =[sender tag]-100;
    NSDictionary *dicComment = [commentViewGetArray objectAtIndex:index] ;

    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[CommentCustomCell class]]) {
        parent = parent.superview;
    }
    
    CommentCustomCell *cell = (CommentCustomCell *)parent;
    // Hide already showing popover
    [cell show];
    [cell.viewMenu.layer setMasksToBounds:YES];
    cell.viewMenu.layer.cornerRadius=  7;
    __weak CommentDetailVC *wself =self;
    [cell setCallBackGroup:^(NSInteger ind)
     {
         if ( [dicComment[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
             switch (ind) {
                 case 0:
                 {
                     [wself editerCommentAction:index];
                 }
                     break;
                 case 1:
                 {
                     [wself deleteCommentAction:index];
                 }
                     break;
                     
                 default:
                     break;
             }
             
         }
         
     }];
}

-(void)editerCommentAction:(NSInteger)index
{
    NSMutableDictionary *dicTmp = [commentViewGetArray[index] mutableCopy];
    [dicTmp setObject:@(TRUE) forKey:@"isEdit"];
    [commentViewGetArray replaceObjectAtIndex:index withObject:dicTmp];
    
    [self.tableControl beginUpdates];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:1];
    [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableControl endUpdates];
    
    CommentEditCell *cell = (CommentEditCell*)[self.tableControl cellForRowAtIndexPath:indexPath];

    if ([cell.tvEditText canBecomeFirstResponder]) {
        [cell.tvEditText becomeFirstResponder];  // surely this line is called
    }
    /*
     NSDictionary *dicComment = [commentViewGetArray objectAtIndex:index] ;
    AlertEditCommentVC *vc = [[AlertEditCommentVC alloc] initWithTitle:nil strcomment:[dicComment[@"content"] emo_emojiString]];
    
    [vc doBlock:^(NSDictionary *dicBack) {
        
        if ([dicBack[@"index"] intValue]==1) {
            [COMMON addLoading:self];
            NSDictionary * postDict = @{ comment : @{@"content":[dicBack[@"comment"]emo_UnicodeEmojiString]}  };
            
            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
            
            [serviceObj PUT_COMMENT_PUBLICATION_ACTION:dicComment[@"id"] withParametersDic:postDict];
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [COMMON removeProgressLoading];
                if (errCode == 404) {
                    
                    [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                    }];
                }
                if (!response) {
                    return;
                }
                
                if([response isKindOfClass: [NSArray class] ]) {
                    return ;
                }
                if (response[@"success"]) {
                    NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:dicComment];
                    [dicMul setValue:dicBack[@"comment"] forKey:@"content"];
                    [commentViewGetArray replaceObjectAtIndex:index withObject:dicMul];
                    [self.tableControl reloadData];
                }
                
            };
            

        }

    }];
    [vc showInVC:self];
    */
}

-(void)deleteCommentAction:(NSInteger)index
{
    /*
     {
     content = Aw;
     created = "2015-12-09T08:16:20+01:00";
     id = 10375;
     isUserLike = 1;
     likes = 1;
     owner =     {
     courtesy = 1;
     firstname = Lou;
     fullname = "Lou Chaucer";
     id = 3608;
     lastname = Chaucer;
     parameters =         {
     friend = 0;
     };
     photo = "/uploads/users/images/thumb/b9e6f71e18b790b5ef497a0bcc583e6ec881b801.jpeg";
     usertag = "manh-manh";
     };
     }
     */
    
    NSString *strContent = str(strVouloirSupprimerCetteComment);
    
    [UIAlertView showWithTitle:str(strMessage33)
                       message:strContent
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[str(strNon)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSDictionary *dic = [commentViewGetArray objectAtIndex:index] ;
                              
                              [COMMON addLoadingForView:self.view];
                              WebServiceAPI *serviceAPI =[WebServiceAPI new];
                              [serviceAPI deleteCommentPublicationAction: dic[@"id"]];
                              serviceAPI.onComplete =^(id response, int errCode) {
                                  [COMMON removeProgressLoading];
                                  if (errCode == 404) {
                                      [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                                      }];
                                  }
                                  if (!response) {
                                      return ;
                                  }
                                  if ([response[@"success"] boolValue]) {
                                      
                                      [self getPublictionItem:self.commentDic[@"id"]];
                                      //delete
                                      for (NSDictionary *tmp in commentViewGetArray) {
                                          if ([tmp[@"id"] intValue] == [dic[@"id"] intValue]) {
                                              [commentViewGetArray removeObject:tmp];
                                              break;
                                          }
                                      }
                                      [self.tableControl reloadData];
                                  }
                              };
                          }
                          else
                          {
                              
                          }
                      }];

    
}

#pragma mark - publiction comment
-(void)gotoNotifi_leaves
{
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getPublicationsActionItem:self.IdNotifi];
    __weak typeof(self) wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if([wself fnCheckResponse:response]) {
            if (errCode == 404) {
                [KSToastView ks_showToast: PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
                }];
            }
        }
        else
        {
            NSArray*arrGroups = response[@"publication"];
            if (arrGroups.count > 0) {
                //reload header
                [self setDataWithPublication:response[@"publication"]];
                [self.tableControl reloadData];
                [self doGetCommentViewURL:NO];
            }

        }
    };
}

#pragma mark -- input
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
//    self.constraintBottom.constant = iKeyboardHeight;
    if (textView == self.textView) {
        isCommenting = TRUE;

    }
    else
    {
        isCommenting = FALSE;
        //        // Set the current _scrollOffset, so we can return the user after editing
                scrollOffsetY = self.tableControl.contentOffset.y;
    }
    return TRUE;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    isCommenting = FALSE;
    return TRUE;
}

-(BOOL)textView:(UITextView *)a shouldChangeTextInRange:(NSRange)b replacementText:(NSString *)c
{
    if (a == self.textView) {
        int remain = (int) (limitComment - (a.text.length + c.length));
        if (remain < 0) {
            NSString *strFullText = [a.text stringByAppendingString:c];
            NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitComment)];
            a.text = subString;
            [numberRemain setTextColor:[UIColor redColor]];
            [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain<0?limitComment:remain,limitComment]];
            [self refreshHeight];
        }
        
        return remain >= 0;

    }
    else
    {
        UIView *parent = [a superview];
        while (parent && ![parent isKindOfClass:[CommentEditCell class]]) {
            parent = parent.superview;
        }
        
        CommentEditCell *cell = (CommentEditCell *)parent;
        int remain = (int) (limitComment - (a.text.length + c.length));
        if (remain < 0) {
            NSString *strFullText = [a.text stringByAppendingString:c];
            NSString *subString = [strFullText substringWithRange:NSMakeRange(0, limitComment)];
            a.text = subString;
            [cell fnNumberRemain:a];
        }
        return remain >= 0;
    }

}
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView == self.textView) {
        int remain = (int)textView.text.length;
        [numberRemain setText:[NSString stringWithFormat:@"%d/%d", remain>limitComment?limitComment:remain,limitComment]];
        
        // Check if the count is over the limit
        if(remain >= 4990) {
            // Change the color
            [numberRemain setTextColor:[UIColor redColor]];
        }
        else if(remain > 4950 && remain < 4990) {
            // Change the color to yellow
            [numberRemain setTextColor:[UIColor orangeColor]];
        }
        else {
            // Set normal color
            [numberRemain setTextColor:[UIColor darkGrayColor]];
        }
        [self refreshHeight];

    }
    else
    {
        UIView *parent = [textView superview];
        while (parent && ![parent isKindOfClass:[CommentEditCell class]]) {
            parent = parent.superview;
        }
        
        CommentEditCell *cell = (CommentEditCell *)parent;
        [cell fnNumberRemain:textView];
    }

}
-(void)refreshHeight
{
    NSInteger newSizeH = [self measureHeight];
    if (newSizeH < minHeight || !_textView.hasText) {
        newSizeH = minHeight; //not smalles than minHeight
    }
    else if (maxHeight && newSizeH > maxHeight) {
        newSizeH = maxHeight; // not taller than maxHeight
    }
    if (_textView.frame.size.height != newSizeH)
    {
        if (newSizeH <= maxHeight)
        {
            [UIView animateWithDuration:animationDuration
                                  delay:0
                                options:(UIViewAnimationOptionAllowUserInteraction|
                                         UIViewAnimationOptionBeginFromCurrentState)
                             animations:^(void) {
                                 [self resizeTextView:newSizeH];
                             }
                             completion:^(BOOL finished) {
                                 [self resizeTextView:newSizeH];
                                 
                             }];
        }
    }
    
}

- (CGFloat)measureHeight
{
    return self.textView.contentSize.height;
}
-(void)resizeTextView:(NSInteger)newSizeH
{
    constraintMessageHeight.constant = newSizeH+detal;
}
-(IBAction)displayLikesList:(id)sender
{
    __weak typeof(self) wself = self;
    NSDictionary *dic = _commentDic;
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getPublicationLikesAction:dic[@"id"]];
    serviceAPI.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (errCode == 404) {
            [KSToastView ks_showToast:PUBLICATION_DOES_NOT_EXIST duration:2.0f completion: ^{
            }];
        }
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"likes"]];
        if (arrMembers.count>0) {
            [self showPopupMembers:arrMembers];
        }
    };

}


-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    MurPeopleLikes *viewController1 = [[MurPeopleLikes alloc] initWithNibName:@"MurPeopleLikes" bundle:nil];
    [viewController1 setData:arrMembers];
    
    viewController1.expectTarget =self.expectTarget;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         
         [self pushVC:friend animate:YES expectTarget:self.expectTarget];
     }];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget];
}

-(void)readMoreDidClickedGesture:(UITapGestureRecognizer*)sender
{
    UIView *myContent = sender.view;
    NSInteger index = myContent.tag - 1001;
    NSMutableDictionary *dicTmp = [commentViewGetArray[index] mutableCopy];
    BOOL readmore = ![dicTmp[@"readmore"] boolValue];
    [dicTmp setObject:@(readmore) forKey:@"readmore"];
    [commentViewGetArray replaceObjectAtIndex:index withObject:dicTmp];
     
    [self.tableControl beginUpdates];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:1];
    [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableControl endUpdates];
}

//MARK: - Share detail view
-(IBAction)shareDetailAction:(UIButton *)sender
{
    NSMutableDictionary *dicTmp = [_commentDic mutableCopy];
    
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    [self showShareDetail:self.tableControl withPointView:cell.imgIconShare withData:dicTmp];
}

-(void)showShareDetail:(UIView*)viewSuper withPointView:(UIView*)pointView withData:(NSDictionary*)dic
{
    [shareDetail fnInputData:dic];
    [shareDetail showAlertWithSuperView:viewSuper withPointView:pointView];
    [shareDetail doBlock:^(NSDictionary * _Nonnull dicResult) {
        
    }];
}
-(void)hideShareDetail
{
    if([shareDetail isDescendantOfView:self.tableControl]) {
        [shareDetail hideAlert];
    }
}
-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}
@end
