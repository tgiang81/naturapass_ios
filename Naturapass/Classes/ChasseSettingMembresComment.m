//
//  ChasseSettingMembresComment.m
//  Naturapass
//
//  Created by Manh on 9/22/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChasseSettingMembresComment.h"
#import "ChasseSettingMembres.h"
#import "SAMTextView.h"
#import "ChassesMemberOBJ.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
@interface ChasseSettingMembresComment ()
{
    IBOutlet SAMTextView *textviewComment;
    IBOutlet UIImageView *imgAvatar;
    IBOutlet UILabel      *lbTile;
    IBOutlet UIImageView *imgCrown;
    int owner_id;
    __weak IBOutlet UILabel *lblDescription1;
    __weak IBOutlet UILabel *lblDescription2;

}
@end

@implementation ChasseSettingMembresComment

- (void)viewDidLoad {
    [super viewDidLoad];
    lblDescription1.text = str(strModificationParticipant);
    lblDescription2.text = str(strCommentaire);
    // Do any additional setup after loading the view from its nib.
    [self.btnSuivant setTitle:str(strTERMINER) forState:UIControlStateNormal];
    
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    textviewComment.placeholder = str(strPlaceholder);
    [self InitializeKeyboardToolBar];
    
    [textviewComment setInputAccessoryView:self.keyboardToolbar];
    
    NSDictionary *dic = [NSDictionary new];
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
    }
    else
    {
        dic =obj.dictionaryMyKind;
    }
    NSString *strFullName = dic[@"fullname"];
    strFullName = [strFullName stringByReplacingOccurrencesOfString:@"\r\n"
                                                         withString:@""];
    lbTile.text = strFullName;
    
    [imgAvatar.layer setMasksToBounds:YES];
    imgAvatar.layer.cornerRadius= 22;
    imgAvatar.layer.borderWidth =0;
    
    //image
    NSString *strImage;
    
    if (dic[@"profilepicture"] != nil) {
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    if (obj.isNonMember) {
        imgAvatar.image =[UIImage imageNamed:@"profile"];
    }
    else
    {
        [imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    }
    textviewComment.text =dic[@"publicComment"];
    //

    owner_id =(int)[[GroupEnterOBJ sharedInstance].dictionaryGroup[@"owner"][@"id"] intValue];
    
    if ([dic[@"id"] integerValue] ==owner_id ) {
        imgCrown.hidden=NO;

    }
    else
    {
        imgCrown.hidden=YES;
 
    }
    

}
- (void)resignKeyboard:(id)sender
{
    [textviewComment resignFirstResponder];
    
}
-(IBAction)onNext:(id)sender
{

    NSString *strContent =textviewComment.text?textviewComment.text:@"";
    NSDictionary * postDict = @{@"participation":self.numberOption,@"content":strContent}; //[NSDictionary dictionaryWithObjectsAndKeys:strContent,@"content", nil];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    
    NSDictionary *dic = [NSDictionary new];
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    NSString * strloungeID= obj.strMyKindId;
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
        NSString * _strid = dic[@"id"];
//        [serviceAPI putLoungesuscriberPubliccommentAction:strloungeID strID:_strid withParametersDic:postDict];
                [serviceAPI putLoungesuscriberParticipationCommentAction:strloungeID strID:_strid withParametersDic:postDict];
    }
    else
    {
        dic =obj.dictionaryMyKind;
        NSString * _strid = dic[@"id"];
        [serviceAPI putLoungesuscriberPubliccomment_Nonmembers:strloungeID strID:_strid withParametersDic:postDict];

    }
        serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        NSArray * controllerArray = [[self navigationController] viewControllers];
        
        for (int i=0; i < controllerArray.count; i++) {
            if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                
                [self.navigationController popToViewController:self.mParent animated:YES];
                
                return;
            }
        }
        [self doRemoveObservation];

        [self.navigationController popToRootViewControllerAnimated:YES];
    };
}
-(IBAction)jumProfileInfoAction:(id)sender
{
    NSDictionary *dic = [NSDictionary new];
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
    }
    else
    {
        dic =obj.dictionaryMyKind;
    }
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    if ([dic[@"id"] integerValue] ==[my_id integerValue]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic: dic];
    
    [self pushVC:friend animate:YES expectTarget:ISMUR];
}
@end
