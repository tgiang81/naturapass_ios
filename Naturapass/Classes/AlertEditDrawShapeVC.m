//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertEditDrawShapeVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import "ColorCollectionCell.h"
#import "FileHelper.h"
#import "PublicationOBJ.h"
@implementation AlertEditDrawShapeVC
{
}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertEditDrawShapeVC" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        [COMMON listSubviewsOfView:self];
        
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertEditDrawShapeVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertEditDrawShapeVCCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithDic:(NSDictionary*)dicInput withEdit:(BOOL)isEdit
{
    _isEdit = isEdit;
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    if (_isEdit) {
        _constraintHeight.constant = 105;
        _viewEdit.hidden = NO;
        _viewInfo.hidden = YES;
    }
    else
    {
        _constraintHeight.constant = 55;
        _viewEdit.hidden = YES;
        _viewInfo.hidden = NO;


    }
    if (dicInput) {
        _lbTitle.text = dicInput[@"c_title"];
        _lbDesc.text = dicInput[@"c_description"];
    }

}

-(IBAction)deleteAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(@"delete");
    }
}
-(IBAction)modifyAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(@"modify");
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(nil);
    }
}

@end
