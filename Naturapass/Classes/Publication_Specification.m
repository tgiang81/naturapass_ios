//
//  Publication_Specification.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Specification.h"
#import "Publication_ChoixSpecNiv1.h"
#import "Publication_Partage.h"

@interface Publication_Specification ()
{
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UIButton *btnNON;
    IBOutlet UIButton *btnOUI;
}
@end

@implementation Publication_Specification

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    lbDescription1.text = str(strVoulezvous_preciser_votre_publication);
    lbDescription2.text = str(strExemple_mirador);
    [btnNON setTitle:str(strNON)  forState:UIControlStateNormal];
    [btnOUI setTitle:str(strOUI)  forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    
    Publication_ChoixSpecNiv1 *viewController1 = [[Publication_ChoixSpecNiv1 alloc] initWithNibName:@"Publication_ChoixSpecNiv1" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    
}

- (IBAction)selectNON:(id)sender {
    Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}

@end
