//
//  Filter_Cell_AddMore.h
//  Naturapass
//
//  Created by Manh on 10/12/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_AddMore : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnAddMore;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

@end
