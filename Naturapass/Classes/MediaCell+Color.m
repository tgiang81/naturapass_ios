//
//  MediaCell+Color.m
//  Naturapass
//
//  Created by Manh on 8/12/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MediaCell+Color.h"

@implementation MediaCell (Color)
-(void)fnSetTheme:(UIColor*)color
{
    [self.title setTextColor:color];
    [self.location setTitleColor:color forState:UIControlStateNormal];
    [self.btnLike setTitleColor:color forState:UIControlStateSelected];
    [self.menuPopover fnColorTheme:color];
}
@end
