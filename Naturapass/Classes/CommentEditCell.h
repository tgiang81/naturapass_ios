//
//  CommentEditCell.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 1/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OHAttributedLabel.h"
#import "CellBase.h"
#import "Define.h"
#import "MDHTMLLabel.h"
typedef void (^CommentEditCellCallback)(NSDictionary *dic);

@interface CommentEditCell : CellBase <OHAttributedLabelDelegate,MDHTMLLabelDelegate>
{
    //button
    IBOutlet UIButton   *likeButton;
    IBOutlet UIButton   *likeImageButton;
    IBOutlet UIButton   *unlikeImageButton;
    IBOutlet UIButton   *sharebutton;
    IBOutlet UIButton   *shareimageButton;

    //label
    IBOutlet UILabel        *murTitleLabel;
    IBOutlet UILabel        *dateLabel;
    IBOutlet UILabel        *commentaireLabel;
    IBOutlet UILabel        *jaimeLabel;
    IBOutlet UILabel        *jaimepas;
    IBOutlet UILabel        *commentCountLabel;
    IBOutlet UILabel        *unlikeCountLabel;
    IBOutlet UILabel        *likeCountLabel;
   

    IBOutlet UILabel        *commentTitleLabel;
    IBOutlet UILabel        *commentdateLabel;
    IBOutlet UIButton        *commentunlikeCountButton;
    IBOutlet UIButton       *commentlikeCountButton;
    IBOutlet UIButton       *commentliketextButton;
    IBOutlet UIButton       *commentUnliketextButton;

    IBOutlet UIButton *locationImageBtn;
    IBOutlet UIButton *locationBtn;
    IBOutlet UIButton *locationCommentBtn;
    
    IBOutlet MDHTMLLabel     *messagesTextLabel;
    
    IBOutlet UIView  *actionsView;
    IBOutlet UIView *commentCountView;
}
@property (assign, nonatomic) ISSCREEN iAmScreen;

@property (nonatomic,retain)IBOutlet UIButton   *likeButton;


@property (nonatomic,weak)IBOutlet UIImageView *iconLike;

@property (nonatomic,retain)IBOutlet UIButton   *likeImageButton;
@property (nonatomic,retain)IBOutlet UIButton   *unlikeImageButton;
@property (nonatomic,retain)IBOutlet UIButton   *sharebutton;
@property (nonatomic,retain)IBOutlet UIButton   *shareimageButton;
@property (nonatomic,retain)IBOutlet UIButton   *commentButton;
@property (nonatomic,retain)IBOutlet UIView     *commentView;
//@property (nonatomic,weak)IBOutlet UIView     *mainView;
@property (nonatomic,retain)IBOutlet UIImageView *userImage;
@property (nonatomic,retain)IBOutlet UIImageView *commentUserImage;
@property (nonatomic,retain)IBOutlet UILabel      *murTitleLabel;
@property (nonatomic,retain)IBOutlet UILabel        *dateLabel;
@property (nonatomic,retain)IBOutlet UILabel        *commentCountLabel;
@property (nonatomic,retain)IBOutlet UILabel        *unlikeCountLabel;
@property (nonatomic,retain)IBOutlet UILabel        *likeCountLabel;
@property (nonatomic,retain)IBOutlet UIButton        *commentunlikeCountButton;
@property (nonatomic,retain)IBOutlet UIButton        *commentlikeCountButton;
@property (nonatomic,retain)IBOutlet UIButton       *commentliketextButton;
@property (nonatomic,retain)IBOutlet UIButton       *commentUnliketextButton;
@property (nonatomic,retain)IBOutlet UILabel        *commentTitleLabel;
@property (nonatomic,retain)IBOutlet UILabel        *commentdateLabel;
@property (nonatomic,retain)IBOutlet UIImageView    *descriptionImage;
@property (retain, nonatomic) IBOutlet UIButton *coverTapBtn;


@property (nonatomic,retain)IBOutlet UIView         *commentCountView;
@property (nonatomic,retain)IBOutlet UILabel        *commentaireLabel;
@property (retain, nonatomic) IBOutlet UIButton *locationImageBtn;
@property (retain, nonatomic) IBOutlet UIButton *locationBtn;
@property (retain, nonatomic) IBOutlet UIButton *locationCommentBtn;
@property (retain, nonatomic) IBOutlet MDHTMLLabel        *commentLabel;
@property (weak, nonatomic)     IBOutlet UITextView *tvEditText;
@property (retain, nonatomic) IBOutlet UIButton *btnAnnuler;
@property (retain, nonatomic) IBOutlet UIButton *btnModifier;
@property (nonatomic, strong)  UIToolbar                   *keyboardToolbar;

-(void)setTitleLabel:(NSString *)_text;
-(void)setmessageView:(NSString *)_text;
-(void)setImageView:(NSString *)_text;

-(void)assignValuesHeaderSection:(NSDictionary *)dic;
-(void)setCount:(NSDictionary *)CountDict;

-(void)Localization;

-(void)setCommentText:(NSString *)strText;
@property (nonatomic,copy) CommentEditCellCallback callback;
-(void)doBlock:(CommentEditCellCallback ) cb;
-(void)fnNumberRemain:(UITextView *)textView;
@end
