//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step52.h"
#import "ChassesCreate_Step6.h"
#import "TypeCell31.h"
//
#import "TypeCell71.h"
#import "CellKind3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MDCheckBox.h"
#import "AlertViewMembersInvite.h"
#import "CCMPopupTransitioning.h"
static NSString *Cell_Step5 =@"TypeCell71";
static NSString *Cell_Step5_ID =@"TypeCell71ID";

@interface ChassesCreate_Step52 ()
{
    //
    NSMutableArray *arrMesGroupes;
    NSMutableArray *tourMemberArray;
    int secIndex;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblDescription1;
    __weak IBOutlet UILabel *lblDescription2;
    __weak IBOutlet UILabel *lblDescription3;

}
@property (nonatomic, strong) TypeCell71 *prototypeCell;

@end

@implementation ChassesCreate_Step52


- (TypeCell71 *)prototypeCell

{
    
    if (!_prototypeCell)
        
    {
        
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:Cell_Step5_ID];
        
    }
    
    return _prototypeCell;
    
}


- (IBAction)onNext:(id)sender {
    ChassesCreate_Step6 *viewController1 = [[ChassesCreate_Step6 alloc] initWithNibName:@"ChassesCreate_Step6" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strInviterDesPersonnes);
    lblDescription1.text = str(strIciVousPouvez);
    lblDescription2.text = str(strIensembleDesMembres);
    lblDescription3.text = str(strSeulementCertains);
    self.needChangeMessageAlert = YES;

    [self initialization];
    // Do any additional setup after loading the view from its nib.
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}
-(void)initialization
{
    arrMesGroupes = [NSMutableArray new];
    tourMemberArray = [NSMutableArray new];
    secIndex = -1;
    //
    [self.tableControl registerNib:[UINib nibWithNibName:Cell_Step5 bundle:nil] forCellReuseIdentifier:Cell_Step5_ID];
    [self initRefreshControl];
    [self insertRowAtTop];
}
#pragma mark - Actions

#pragma mark - Actions
/*
 
 groups =     (
 {
 access = 0;
 connected =             {
 access = 3;
 mailable = 1;
 user =                 {
 firstname = Liz;
 fullname = "Liz Manh";
 id = 160;
 lastname = Manh;
 profilepicture = "/uploads/users/images/thumb/2e8c410b78af04a74b8816145cca7a6992f97418.jpeg";
 usertag = "manh-manhmanh";
 };
 };
 description = assa;
 grouptag = aas;
 id = 388;
 name = aas;
 nbAdmins = 1;
 nbPending = 0;
 nbSubscribers = 1;
 owner =             {
 firstname = Liz;
 id = 160;
 lastname = Manh;
 profilepicture = "/uploads/users/images/thumb/2e8c410b78af04a74b8816145cca7a6992f97418.jpeg";
 usertag = "manh-manhmanh";
 };
 photo = "https://naturapass.e-conception.fr/uploads/groups/images/thumb/5d909eb1c6d75c3b4c80d12420cde51a971d7d37.jpeg";
 },
 {
 access = 1;
 connected =             {
 access = 3;
 mailable = 1;
 user =                 {
 firstname = Liz;
 fullname = "Liz Manh";
 id = 160;
 lastname = Manh;
 profilepicture = "/uploads/users/images/thumb/2e8c410b78af04a74b8816145cca7a6992f97418.jpeg";
 usertag = "manh-manhmanh";
 };
 };
 description = Gq;
 grouptag = gq;
 id = 387;
 name = Gq;
 nbAdmins = 1;
 nbPending = 0;
 nbSubscribers = 1;
 owner =             {
 firstname = Liz;
 id = 160;
 lastname = Manh;
 profilepicture = "/uploads/users/images/thumb/2e8c410b78af04a74b8816145cca7a6992f97418.jpeg";
 usertag = "manh-manhmanh";
 };
 photo = "https://naturapass.e-conception.fr/img/interface/default-media.jpg";
 }
 );
 }
 */
- (void)insertRowAtTop {
    //request first page
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:@"0"];
    
    __weak typeof(self) wself = self;

    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        if (!response) {
            
            return ;
        }
        
        if (response[@"groups"]) {
            [arrMesGroupes removeAllObjects];
            [arrMesGroupes addObjectsFromArray:response[@"groups"] ];
            [wself.tableControl reloadData];
        }
        
    };
}

- (void)insertRowAtBottom {
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:[NSString stringWithFormat:@"%ld",(long)arrMesGroupes.count]];
    
    __weak typeof(self) wself = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        if (!response) {
            
            return ;
        }
        NSArray*arrGroups = response[@"groups"];
        
        if (arrGroups.count > 0) {
            [arrMesGroupes addObjectsFromArray:response[@"groups"] ];
            [wself.tableControl reloadData];
        }
        
    };
}

#pragma mark -  TableView Delegates
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary*dic = arrMesGroupes[indexPath.row];
    
    TypeCell71 *cell = (TypeCell71 *)cellTmp;
    cell.parent = self;
    
    if (dic[@"profilepicture"] != nil) {
        [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:dic[@"profilepicture"] ] ];
    }else{
        [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:dic[@"photo"] ] ];
    }
    
    int         accessType = [dic[@"access"] intValue];
    if (accessType == 0) {
        cell.label2.text = str(strAccessPrivate);
    } else if (accessType == 1) {
        cell.label2.text = str(strAccessSemiPrivate);
    } else if (accessType == 2) {
        cell.label2.text = str(strAccessPublic);
    }

    
    [cell.label1 setText:dic[@"name"]];
    int         nbSubscribers = [dic[@"nbSubscribers"] intValue];
    if (nbSubscribers ==1) {
        cell.label3.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];

    }
    else
    {
        cell.label3.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];

    }
    
    [cell.label4 setText:dic[@"description"] ];
    
    cell.button1.tag=indexPath.row+1;
    [cell.button1 addTarget:self action:@selector(MemberButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.button1 setTitle:str(strMEMBERS) forState:UIControlStateNormal];
    
    cell.button2.tag=indexPath.row+1;
    [cell.button2 addTarget:self action:@selector(fnInviteGroupes:) forControlEvents:UIControlEventTouchUpInside];
    //    [cell.btnInviteGroup setTitle:NSLocalizedString(@"Members", @"") forState:UIControlStateNormal];
    if (indexPath.row ==  secIndex) {
        [cell fnShowMemberWithArray:tourMemberArray];
        cell.imgArrow.hidden = NO;
    }
    else
    {
        [cell fnShowMemberWithArray:nil];
        cell.imgArrow.hidden = YES;
    }
    cell.strID =[ChassesCreateOBJ sharedInstance].strID;
    [cell fnSettingCell:UI_CHASSES_MUR_NORMAL expectTarget:ISLOUNGE];
    if (dic[@"sendAll"]) {
        [cell.button2 setSelected:YES];
        cell.button2.backgroundColor = [UIColor lightGrayColor];
        [cell.button2 setTitle:str(strEnvoyer) forState:UIControlStateNormal];
    }
    else
    {
        [cell.button2 setTitle:str(strENVOYERATOUS) forState:UIControlStateNormal];
        [cell.button2 setSelected:NO];
        cell.button2.backgroundColor =UIColorFromRGB(CHASSES_TINY_BAR_COLOR);

    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [arrMesGroupes count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    [self configureCell:self.prototypeCell cellForRowAtIndexPath:indexPath];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TypeCell71 *cell = (TypeCell71 *)[self.tableControl dequeueReusableCellWithIdentifier:Cell_Step5_ID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
}

#pragma mark - MemberAction
-(IBAction)MemberButtonAction:(UIButton *)sender {

    NSInteger index =  [sender tag] -1;
    if (secIndex == index) {
        
        secIndex = -1;
        [tourMemberArray removeAllObjects];
        [self.tableControl reloadData];
        
    }else{
        secIndex = (int)index;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[arrMesGroupes copy];
    NSString *strLoungeID=arrData[index][@"id"];
    [COMMON addLoading:self];
        __weak typeof(self) wself = self;
    WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
    [loungeSubscribeAction getGroupToutesSubscribeAction:strLoungeID];
    loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"subscribers"]];
        
        for (int i=0; i<arrMembers.count; i++) {
            NSDictionary *dic =[arrMembers objectAtIndex:i];
            if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                [arrMembers removeObjectAtIndex:i];
            }
        }
        if (arrMembers.count>0) {
            [tourMemberArray removeAllObjects];
            [tourMemberArray addObjectsFromArray:arrMembers];
            [self.tableControl  reloadData];
//            [self showPopupMembers:arrMembers];
        }
    };
    }

}

-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
        AlertViewMembersInvite *viewController1 = [[AlertViewMembersInvite alloc]initWithArrayMembers:arrMembers wihtID:[ChassesCreateOBJ sharedInstance].strID];
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         // khi click thi invite members
     }];
    viewController1.expectTarget = ISLOUNGE;
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = viewController1;
    popup.presentingController = self;
    [self presentViewController:viewController1 animated:YES completion:nil];
}

-(IBAction)fnInviteGroupes:(UIButton*)sender
{
    int index = (int) sender.tag - 1;
    NSDictionary*dic = arrMesGroupes[index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postLoungeInviteGroup: [ChassesCreateOBJ sharedInstance].strID group:dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            [KSToastView ks_showToast:NSLocalizedString(@"INVALID_ERROR", @"")  duration:2.0f completion: ^{
            }];

            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            
            return ;
        }
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strVotre_demande_envoyee) delegate:nil cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        [alert show];

        if (index<arrMesGroupes.count) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMesGroupes[index]];
            [dic setValue:@1 forKey:@"sendAll"];
            [arrMesGroupes replaceObjectAtIndex:index withObject:dic];
            [self.tableControl reloadData];
        }

        
    };
}
@end
