//
//  DetailFromMapVC.m
//  Naturapass
//
//  Created by manh on 11/28/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "DetailFromMapVC.h"
#import "GroupDetailCell.h"
#import "AgendaDetailCell2.h"
#import "GroupEnterMurVC.h"
#import "NSString+HTML.h"
#import "NSDate+Extensions.h"
#import "AlertMapVC.h"
#import "ChassesParticipeVC.h"

static NSString *GroupDetailID = @"GroupDetailID";
static NSString *AgendaDetailID = @"AgendaDetailID";

@interface DetailFromMapVC ()

@end

@implementation DetailFromMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavDetailMap"];
    MainNavigationBaseView *subview =  [self getSubMainView];
    //Change background color
    //SUB
    [self addSubNav:nil];
    
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                subview.backgroundColor = UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR);
                
            }else{
                //Change status bar color
                subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            }
            
        }
            break;
        case ISLOUNGE:
        {
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        case ISMUR:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        default:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
    }
    //
    [self.tableControl registerNib:[UINib nibWithNibName:@"GroupDetailCell" bundle:nil] forCellReuseIdentifier:GroupDetailID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AgendaDetailCell2" bundle:nil] forCellReuseIdentifier:AgendaDetailID];
    
    self.tableControl.estimatedRowHeight = 160;
    widthImage = [UIScreen mainScreen].bounds.size.width * 286/304;

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
                
            }else{
                //Change status bar color
                [navBar setBarTintColor: UIColorFromRGB(CARTE_MAIN_BAR_COLOR) ];
            }
            
        }
            break;
        case ISLOUNGE:
        {
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_MAIN_BAR_COLOR) ];
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_MAIN_BAR_COLOR) ];
            
        }
            break;
        case ISMUR:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
            
        }
            break;
        default:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
        }
            break;
    }
    
}

-(void)setDataWithPublication:(NSArray*)arr
{
    arrSection3 = [NSMutableArray new];
    arrSection3 = [arr mutableCopy];
    
}
#pragma mark - TABLEVIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

        return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        if (_arrowType == ARROW_GROUP) {
                GroupDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:GroupDetailID forIndexPath:indexPath];
                [self configureGroupDetailCell:cell cellForRowAtIndexPath:indexPath];
                return cell;
        }
        else
        {
                AgendaDetailCell2 *cell = [tableView dequeueReusableCellWithIdentifier:AgendaDetailID forIndexPath:indexPath];
                [self configureAgendaDetailCell:cell cellForRowAtIndexPath:indexPath];
                return cell;
        }
}
- (void)configureGroupDetailCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[GroupDetailCell class]])
    {
        GroupDetailCell *cell = (GroupDetailCell *)cellTmp;
        NSDictionary *dic = arrSection3[indexPath.row];
        //FONT
        cell.lbTitle.text = dic[@"name"];
        cell.lbDescription.text = dic[@"description"];
        int         accessType = [dic[@"access"] intValue];
        if (accessType == 0) {
            cell.lblAccess.text = str(strAccessPrivate);
        } else if (accessType == 1) {
            cell.lblAccess.text = str(strAccessSemiPrivate);
        } else if (accessType == 2) {
            cell.lblAccess.text = str(strAccessPublic);
        }
        
        //        int         nbPending = [dic[@"nbPending"] intValue];
        int         nbSubscribers = [dic[@"nbSubscribers"] intValue];
        if (nbSubscribers ==1) {
            cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        }
        else
        {
            cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@s", nbSubscribers,str(strMMembre)];
        }
        //Enter
        cell.btnEnterGroup.tag = 1000 + indexPath.row;;
        [cell.btnEnterGroup addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString    *strPhotoUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strPhotoUrl]];
        __weak GroupDetailCell *weakCell = cell;
        __weak typeof(self) weakSelf = self;
        
        [cell.imgIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //check if in visible cell => update...
            GroupDetailCell *checkCell = (GroupDetailCell*)[self.tableControl cellForRowAtIndexPath:indexPath];
            //ok reload
            
            if (image) {
                UIImage *tempImg = [self fnResizeFixWidth:image :widthImage];
                
                weakCell.imgIcon.image = tempImg;
            }
            if (checkCell && image) {
                [weakSelf.tableControl reloadData];
            }
        }];
        
        [cell fnSettingCell:UI_GROUP_MUR_ADMIN];
        
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];
        
    }
}
- (void)configureAgendaDetailCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[AgendaDetailCell2 class]])
    {
        AgendaDetailCell2 *cell = (AgendaDetailCell2 *)cellTmp;
        NSDictionary *dic = arrSection3[indexPath.row];
        cell.lblDebut.text = str(strDEBUTLE);
        cell.lblFin.text = str(strFINLE);
        cell.lblRendez.text = str(strRendezVousA);
        
        
        NSString* strPath  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        
        [cell.logo sd_setImageWithURL: [NSURL URLWithString: strPath ] placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
        NSString                    *strAccess;
        
        if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
            strAccess= [NSString stringWithFormat:@"%@",dic[@"connected"][@"access"]];
        }
        else
        {
            strAccess=@"4";
        }
        NSInteger iSubScribe = 10;
        if (strAccess && ![strAccess isEqualToString:@""]) {
            iSubScribe=[strAccess intValue];
        }
        [cell.btnSURNATURA setColorEnable:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        [cell.btnSURNATURA setColorDisEnable:[UIColor lightGrayColor]];
        
        //refresh
        [cell.btnSURNATURA removeTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSURNATURA removeTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSURNATURA removeTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
        
        switch (iSubScribe)
        {
            case USER_INVITED:
            {
                [cell.btnSURNATURA setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.btnSURNATURA setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:NO];
            }
                break;
                
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.btnSURNATURA setTitle:str(strACCEDER) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_REJOINDRE:
            {
                [cell.btnSURNATURA setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                break;
        }
        
        
        cell.lbNameHunt.text = [dic[@"name"] stringByDecodingHTMLEntities];
        
        //
        int iAccess = [dic[@"access"] intValue];
        
        
        if (iAccess == 0)
            cell.lbAccess.text=str(strAccessPrivate);
        else if(iAccess == 1)
            cell.lbAccess.text=str(strAccessSemiPrivate);
        else if(iAccess == 2)
            cell.lbAccess.text=str(strAccessPublic);
        
        cell.lbDescription.text = [dic[@"description"] stringByDecodingHTMLEntities];
        
        NSString *strMeetingDate = [NSDate convertAgendaToNSTring:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_begin"]]];
        
        NSString *strEndDate = [NSDate convertAgendaToNSTring:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_end"]]];
        
        cell.lbStartDate.text=@"";
        cell.lbStartTime.text=@"";
        cell.lbAddress.text =@"";
        cell.lbLatLong.text =@"";
        //START DATE
        
        if (strMeetingDate.length>0) {
            cell.lbStartDate.text= [strMeetingDate substringWithRange:NSMakeRange(0, 10)];
            cell.lbStartTime.text= [NSString stringWithFormat:@"à %@",[strMeetingDate substringWithRange:NSMakeRange(11, 5)]];
        }
        //END DATE
        if (strEndDate.length>0) {
            cell.lbEndDate.text= [strEndDate substringWithRange:NSMakeRange(0, 10)];
            cell.lbEndTime.text= [NSString stringWithFormat:@"à %@",[strEndDate substringWithRange:NSMakeRange(11, 5)]];
        }
        //ADDRESS
        cell.lbAddress.text = [dic[@"meeting"][@"address"][@"address"] stringByDecodingHTMLEntities];
        cell.lbLatLong.text =[ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meeting"][@"address"][@"latitude"] floatValue],[dic[@"meeting"][@"address"][@"longitude"] floatValue]];
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];
        
    }
}
-(void)enterGroupMurAction:(id)sender
{
    //Expect target...
    
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = tag - 1000;
    NSDictionary *dic = [arrSection3 objectAtIndex:index];
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    viewController1.needRemoveSubItem = NO;
    //
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    [self pushVC:viewController1 animate:YES];
}
#pragma mark - API

-(IBAction)NATURAAction:(id)sender
{
    //ACCESS page??? or ....that depend on user type
    
    //        NSDictionary *local = (NSDictionary*)dic;
    //
    //        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
    //        viewController1.isSubVC = YES;
    //        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
    //                                      @"longitude":  local[@"longitude"]};
    //
    //        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

-(IBAction)GPSAction:(id)sender
{
    //ROUTE from Current location to AGENDA position.
    NSDictionary *dic = arrSection3[0];
    NSString *strLat =@"";
    NSString *strLng =@"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    double lat = appDelegate.locationManager.location.coordinate.latitude;
    double lng = appDelegate.locationManager.location.coordinate.longitude;
    strLat = dic[@"meeting"][@"address"][@"latitude"] ;
    strLng = dic[@"meeting"][@"address"][@"longitude"] ;
    
    AlertMapVC *vc = [[AlertMapVC alloc] initWithTitle:nil message:nil];
    
    [vc doBlock:^(NSInteger index) {
        if (index==0) {
            // Plans
            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
            NSURL *url =[NSURL URLWithString:str];
            
            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                NSLog(@"Can't use applemap://");
            }
        }
        else if(index==1)
        {
            
            
            //Maps
            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
            }
            
        }
        else if(index==2)
        {
            //Waze
            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"waze://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                // Waze is not installed. Launch AppStore to install Waze app
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
            }
        }
    }];
    [vc showInVC:self];
}

//JUMP

//1
-(void)rejoindreCommitAction:(UIButton *)sender{
    NSDictionary *dic = arrSection3[0];
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    
    
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),dic[@"name"]];
    
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}


//2
-(void) gotoGroup:(UIButton*)sender
{
    NSDictionary *dic = arrSection3[0];
    
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        
    };
}

//3
- (void)cellRejoindre:(UIButton *)sender
{
    NSDictionary *dic = arrSection3[0];
    
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    NSString *desc=@"";
    NSString *strTile=@"";
    NSString *strNameGroup=dic[@"name"];
    
    int accessValueNumber = [dic[@"access"] intValue];
    
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        
        desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
        
        strTile = str(strRejoinAcceptPublic);
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
    }
    
    [COMMON addLoading:self];
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:dic[@"id"] ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        [COMMON removeProgressLoading];
        if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
            [self.tableControl reloadData];
            return ;
        }
        NSInteger access= [respone[@"access"] integerValue];
        if (access==NSNotFound) {
            [self.tableControl reloadData];
            return ;
        }
        [UIView animateWithDuration:0.3 animations:^{
        } completion:^(BOOL finished){}];
        
        
        [UIAlertView showWithTitle:strTile message:desc
                 cancelButtonTitle:str(strYES)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    };
}

- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    NSDictionary *dic = arrSection3[0];
    
    //get chasse item. set for .dictionaryGroup
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        int accessValueNumber = [dic[@"access"] intValue];
        
        NSString *desc=@"";
        NSString *strNameGroup=dic[@"name"];
        
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
            
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
        }
        
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        NSString *strLoungeID = dic[@"id"];
        [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];

        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            
            [COMMON removeProgressLoading];
            
            if([respone isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
            
            viewController1.dicChassesItem =  [GroupEnterOBJ sharedInstance].dictionaryGroup;
            
            viewController1.isInvitionAttend =YES;
            [viewController1 doCallback:^(NSString *strID) {
                //get chasse item. set for .dictionaryGroup
                
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
            }];
            [self pushVC:viewController1 animate:YES];
        };
        
    };
    
}
@end
