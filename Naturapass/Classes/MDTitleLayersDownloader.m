//
//  MDTitleLayersDownloader.m
//  Naturapass
//
//  Created by Manh on 9/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDTitleLayersDownloader.h"
#import "SDImageCache.h"
static MDTitleLayersDownloader *sharedInstance = nil;
static bool isFinish;
static double count;
@implementation MDTitleLayersDownloader
+ (MDTitleLayersDownloader *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        _myQueue = dispatch_queue_create("com.naturapass.loadImageQueue", DISPATCH_QUEUE_CONCURRENT);
        isFinish = YES;
        return self;
    }
    
    return nil;
}
-(void)cacheMapForNorthEast:(CLLocationCoordinate2D)northEast withSouthWest:(CLLocationCoordinate2D)southWest withZoom:(int)zoom
{
    if (isFinish == NO) {
        return;
    }
    else
    {
        double lat_nortEast  = northEast.latitude;
        double lon_nortEast  = northEast.longitude;
        //
        double lat_southWest = southWest.latitude;
        double lon_southWest = southWest.longitude;
        for (int k = zoom + 2; k<= 19; k++) {
            dispatch_async(_myQueue,
                           ^{
                               //
                               NSDictionary *dic_nortEast = [self calculatorUTM:lon_nortEast  lat:lat_nortEast  zoom:k];
                               NSDictionary *dic_southWest = [self calculatorUTM:lon_southWest lat:lat_southWest zoom:k];
                               //1
                               int x_northWest = [dic_southWest[@"x"] intValue];
                               int y_northWest  = [dic_nortEast[@"y"] intValue];
                               //2
                               int x_nortEast  = [dic_nortEast[@"x"] intValue];
                               //4
                               int y_southWest = [dic_southWest[@"y"] intValue];
                               for (int i =x_northWest-1; i<= x_nortEast ; i++) {
                                   for (int j = y_northWest-1; j<= y_southWest; j++) {
                                       [self cacheImageForX:i y:j zoom:k];
                                   }
                               }
                           }
                           );
        }
    }
}
-(NSDictionary*)calculatorUTM:(double)lon lat:(double)lat zoom:(NSUInteger)zoom
{
    double x;
    double y;
    double lat_rad = lat/180*M_PI;
    double n = pow(2, zoom);
    x = n*(lon + 180)/360;
    y = (1- log(tan(lat_rad) + (1/cos(lat_rad)) ) / M_PI)/2*n;
    return @{@"x": @(x),@"y": @(y)};
}
- (void)cacheImageForX:(NSUInteger)x y:(NSUInteger)y zoom:(NSUInteger)zoom {
    
    NSString* userAgent = @"iOS";
    NSURL *url = [ NSURL URLWithString:  [NSString stringWithFormat:@"http://a.tile.openstreetmap.org/%lu/%lu/%lu.png",
                                          (unsigned long)zoom, (unsigned long)x, (unsigned long)y]];
    ASLog(@"%@",url.absoluteString);
    
    if ([[SDImageCache sharedImageCache] imageFromDiskCacheForKey: url.absoluteString] != nil) {
        
        return ;
    }
    
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url] ;
    [request setValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse* response = nil;
    NSError* error = nil;
    NSData* data = [NSURLConnection sendSynchronousRequest:request
                                         returningResponse:&response
                                                     error:&error];
    dispatch_async( dispatch_get_main_queue(), ^{
        
        UIImage *image = [UIImage imageWithData:data];
        [[SDImageCache sharedImageCache] storeImage:image forKey:url.absoluteString completion:nil];
    });
}

@end
