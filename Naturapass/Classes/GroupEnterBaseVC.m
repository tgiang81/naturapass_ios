//
//  GroupEnterBaseVC.m
//  Naturapass
//
//  Created by Manh on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupEnterBaseVC.h"

#import "GroupMesVC.h"
#import "GroupSettingVC.h"
#import "GroupSearchVC.h"
#import "NotificationVC.h"
#import "GroupEnterMurVC.h"
#import "GroupEnterSettingVC.h"
#import "GroupEnterAgenda.h"
#import "LiveHuntOBJ.h"
#import "DatabaseManager.h"
//if lounge
@interface GroupEnterBaseVC ()

@end

@implementation GroupEnterBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];


    
    //add sub navigation
    [self checkAllow];
    switch (self.expectTarget) {
        case ISMUR:
        {
            [self addMainNav:@"MainNavMUR"];
            
            MainNavigationBaseView *subview =  [self getSubMainView];

            [subview.myTitle setText:str(strMUR)];
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self addSubNav:@"SubNavigationMUR"];
        }
            break;
        case ISGROUP:
        {
            [self addMainNav:@"MainNavMUR"];
            
            MainNavigationBaseView *subview =  [self getSubMainView];

            [subview.myTitle setText:str(strGROUPES)];
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
        }
            break;
            
        case ISLOUNGE:
        {
            if (self.isLiveHunt)
            {
                UINavigationBar *navBar=self.navigationController.navigationBar;
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                [self addMainNav:@"MainNavLiveMap"];
                //SUB
                [self addSubNav:@"livechat"];
                
                [self setThemeTabVC:self];
                
                if ([LiveHuntOBJ sharedInstance].dicLiveHunt) {
                    [self.navLiveMap.lbHuntName setText:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"]];
                    [self.navLiveMap.lbHuntTime setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_end"]]];
                }
            }
            else
            {
                [self addMainNav:@"MainNavMUR"];
                
                MainNavigationBaseView *subview =  [self getSubMainView];
                
                //Non admin
                if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                    if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                    {
                        //admin
                        self.dicOption =@{@"admin":@1};
                        
                    }else{
                        self.dicOption =@{@"admin":@0};
                        
                    }
                    
                }else{
                    self.dicOption =@{@"admin":@0};
                }
                
                [subview.myTitle setText:str(strCHANTIERS)];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                
                [self addSubNav:@"SubNavigationGROUPENTER"];
            }
            
        }
            break;
            
        default:
            break;
    }

    if (self.isNotifi) {
        [self.subview removeFromSuperview];
        [self addSubNav:@"SubNavigationMUR"];
        
        
        self.isNotifi =!self.isNotifi;
    }


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;

    
    NSDictionary *dic = nil;
    [self checkAllow];
    switch (self.expectTarget) {
        case ISMUR:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
        }
            break;
            
        case ISLOUNGE:
        {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRightToChat:) name: UPDATE_RIGHT_CHAT object:nil];


            if (self.isLiveHunt)
            {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
            }
            else
            {
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
                
                if (!allow_show_chat)
                {
                    [self removeItem:3];
                }
                if (self.needRemoveSubItem)
                {
                    [self removeItem:4];
                }

            }
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }

        }
            break;
            
            
        default:
            break;
    }
    [self setThemeNavSub:NO withDicOption:dic];

    UIButton *btn = [self returnButton];
    
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];

}

-(void) viewWillDisappear:(BOOL)animated
{
    //avoid duplicate update publication
    [[NSNotificationCenter defaultCenter]  removeObserver:self];

    [super viewWillDisappear:animated];
}

#pragma SUB - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    [super onSubNavClick:btn];
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        default:
            break;
    }
}
-(void) updateRightToChat:(NSNotification*)notif
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //refresh param...
        [self checkAllow];
        
        //hid/display subview discustion
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myTitle setText:str(strCHANTIERS)];
        //Change background color
        subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        [self addSubNav:@"SubNavigationGROUPENTER"];

        if (!allow_show_chat)
        {
            [self removeItem:3];
        }
        if (self.needRemoveSubItem)
        {
            [self removeItem:4];
        }
        
        [self setThemeNavSub:NO withDicOption:@{@"admin":@0}];
        UIButton *btn = [self returnButton];
        [btn setSelected:YES];
        [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
        
        //hide/display button add
        [self updateSubview];

    });
    
    
}
-(void)updateSubview
{
    
}
@end
