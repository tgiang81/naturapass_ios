//
//  TheProjectCell.m
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "PublicationFavorisSystemCell.h"

@interface PublicationFavorisSystemCell()
@end
@implementation PublicationFavorisSystemCell

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.cellLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.cellLabel.frame);
}


@end
