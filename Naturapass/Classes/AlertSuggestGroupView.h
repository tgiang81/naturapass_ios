//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"

typedef void (^AlertSuggestGroupViewCallback)(NSDictionary *dicResult);

@interface AlertSuggestGroupView : UIView<UITableViewDelegate,UITableViewDataSource>
{
}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic)  UIButton *btnDissmiss;

@property(nonatomic,strong)  NSString *strSearch;

@property (nonatomic,copy) AlertSuggestGroupViewCallback callback;
-(void)doBlock:(AlertSuggestGroupViewCallback ) cb;
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect;
-(instancetype)initSuggestView;
-(IBAction)closeAction:(id)sender;
- (void) processLoungesSearchingURL:(NSString *)searchText;
-(void)updateArraySelected:(NSArray*)arrSelected;
-(void)removeItemSelected:(NSDictionary*)dicRemove;
-(void)fnDataInput:(NSString*)strSearch withSelected:(NSDictionary*)dicSec withArrFullData:(NSArray*)fullData;
@end
