//
//  Group_Hunt_ListShare.m
//  Naturapass
//
//  Created by Giang on 10/8/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Group_Hunt_ListShare.h"
#import "Define.h"
#import "Config.h"

#import "CommonHelper.h"
#import "FileHelper.h"
#import "AppCommon.h"

static Group_Hunt_ListShare *sharedInstance = nil;

@implementation Group_Hunt_ListShare

+ (Group_Hunt_ListShare *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) fnGetListGroup_Hunts
{

    WebServiceAPI *serviceObj = [WebServiceAPI new];
    [serviceObj fnGET_MES_GROUP_HUNT];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //get groups that user joined
        
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (!response) {
            return ;
        }
        
        NSString *strPath = [FileHelper pathForApplicationDataFile: concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        //-> got currentlist
        
        NSArray *arrGroupes = response[@"groups"];
        
        NSMutableArray*writableArrGroup = [NSMutableArray new];
        ////
        NSMutableArray*writableArrHunt = [NSMutableArray new];
        //
        if (arrGroupes.count > 0)
        {
            for (NSDictionary *dic in arrGroupes)
            {
                
                NSDictionary *dic2= @{@"categoryName":dic[@"name"],
                                      @"categoryImage": @"sharechamp",
                                      @"groupID":dic[@"id"],
                                      @"isSelected":[NSNumber numberWithBool:0],
                                      @"group":dic};
                
                [writableArrGroup addObject:dic2];
            }
            
        }
        
        // Write array
        [writableArrGroup writeToFile:strPath atomically:YES];
        
        ////ggtt
        strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE)   ];
        
        //-> got currentlist
        
        NSArray *arrHunts = response[@"hunts"];
        
        if (arrHunts.count > 0) {
            for (NSDictionary *dic in arrHunts)
            {
                
                NSDictionary *dic2= @{@"categoryName":dic[@"name"],
                                      @"categoryImage": @"sharechamp",
                                      @"huntID":dic[@"id"],
                                      @"isSelected":[NSNumber numberWithBool:0],
                                      @"hunt":dic};
                [writableArrHunt addObject:dic2];
            }
        }
        
        //reload
        
        // Write array
        [writableArrHunt writeToFile:strPath atomically:YES];

    };
}

@end
