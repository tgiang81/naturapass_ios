//
//  CellKind15.h
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface CellKind15 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *nameGroup;
@property (weak, nonatomic) IBOutlet UILabel *accessType;
@property (weak, nonatomic) IBOutlet UILabel *numberMember;
@property (weak, nonatomic) IBOutlet UILabel *contentGroup;
@property (weak, nonatomic) IBOutlet UIButton *btnValid;
@property (weak, nonatomic) IBOutlet UIButton *btnRefuse;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UILabel *lbNameGroupAdmin;

@end
