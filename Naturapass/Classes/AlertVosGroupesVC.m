//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertVosGroupesVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "VosGroupesBaseCell.h"
#import "VosGroupesHeaderCell.h"
#import "VosGroupesMemberCell.h"
#import "VosGroupesToutSelectedCell.h"

static NSString *cell10 =@"TypeCell10";
static NSString *cellID10 =@"cellID10";

static NSString *identifierHeader = @"identifierHeader";
static NSString *identifierMember = @"identifierMember";
static NSString *identifierToutSelected = @"identifierToutSelected";

@implementation AlertVosGroupesVC
{
    WebServiceAPI *serviceObj;
}
-(instancetype)initWithVosGroupes
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertVosGroupesVC" owner:self options:nil] objectAtIndex:0] ;

    if (self) {
        _colorNavigation = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
        [_subAlertView.layer setMasksToBounds:YES];
        _subAlertView.layer.cornerRadius= 6;
        _subAlertView.layer.borderWidth =0;
        
        _btnTerminal.layer.masksToBounds = YES;
        _btnTerminal.layer.cornerRadius = 20;
        _btnTerminal.backgroundColor = _colorNavigation;
        
        _imgLineHeader.backgroundColor = _colorNavigation;
        _imgLineBottom.backgroundColor = _colorNavigation;
        
        _lbTitle.textColor = _colorNavigation;
        [self.tableControl registerNib:[UINib nibWithNibName:@"VosGroupesHeaderCell" bundle:nil] forCellReuseIdentifier:identifierHeader];
        [self.tableControl registerNib:[UINib nibWithNibName:@"VosGroupesMemberCell" bundle:nil] forCellReuseIdentifier:identifierMember];
        [self.tableControl registerNib:[UINib nibWithNibName:@"VosGroupesToutSelectedCell" bundle:nil] forCellReuseIdentifier:identifierToutSelected];
        self.tableControl.estimatedRowHeight = 60;
        self.tableControl.estimatedSectionHeaderHeight = 40;
        self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self initRefreshControl];
        _arrMesGroupes = [NSMutableArray new];
        indexExpand = -1;
        serviceObj = [[WebServiceAPI alloc]init];

    }
    return self;
}
-(void)showAlertWithArrSelected:(NSArray*)arrSelected
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    indexExpand = -1;
    _arrMesGroupes = [NSMutableArray new];
    _arrSelected = [NSMutableArray new];
    [_arrSelected addObjectsFromArray:arrSelected];
    [self.tableControl reloadData];
    [self insertRowAtTop];
}
#pragma callback
-(void)setCallback:(AlertVosGroupesVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertVosGroupesVCCallback) cb
{
    self.callback = cb;
    
}
-(IBAction)closeAction:(id)sender
{
    [self removeFromSuperview];
}
-(IBAction)terminalAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(_arrSelected);
    }
}
//MARK: - GET DATA
- (void)insertRowAtTop {
    //request first page
    [serviceObj cancelTask];
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:@"0"];
    
    __weak typeof(self) wself = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [wself stopRefreshControl];
        if (response[@"groups"]) {
            [_arrMesGroupes removeAllObjects];
            //only add group that has nbSubscribers > 1
            
            NSArray *arrGr = response[@"groups"];
            
            for (int i=0; i < arrGr.count; i++) {
                NSDictionary *dDic = arrGr[i];
//                if ([dDic[@"nbSubscribers"] intValue] > 1)
                {
                    [_arrMesGroupes addObject:dDic];
                }
            }

            [wself.tableControl reloadData];
        }
        
    };
}

- (void)insertRowAtBottom {
    
    [serviceObj cancelTask];
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:[NSString stringWithFormat:@"%ld",(long)_arrMesGroupes.count]];
    
    __weak typeof(self) wself = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [wself stopRefreshControl];
        NSArray*arrGroups = response[@"groups"];
        
        if (arrGroups.count > 0) {

            NSArray *arrGr = response[@"groups"];
            
            for (int i=0; i < arrGr.count; i++) {
                NSDictionary *dDic = arrGr[i];
//                if ([dDic[@"nbSubscribers"] intValue] > 1)
                {
                    [_arrMesGroupes addObject:dDic];
                }
            }

            [wself.tableControl reloadData];
        }
        
    };
}
#pragma mark - TABLEVIEW
//MARK: - TABLE HEADER
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.arrMesGroupes.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    NSDictionary *dDic = _arrMesGroupes[section];
    
    if ([dDic[@"nbSubscribers"] intValue] > 1)
    {
        return UITableViewAutomaticDimension;
    }else{
        return 0.1f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dDic = _arrMesGroupes[section];
    
    if ([dDic[@"nbSubscribers"] intValue] > 1)
    {
        NSDictionary *dicData = self.arrMesGroupes[section];
        VosGroupesHeaderCell *cell = (VosGroupesHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierHeader];
        cell.imgAvatar.layer.masksToBounds = YES;
        cell.imgAvatar.layer.cornerRadius = 27;
        
        cell.lbTitle.text = dicData[@"name"];
        cell.lbTitle.textColor = _colorNavigation;
        
        if (dicData[@"profilepicture"] != nil) {
            [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:dicData[@"profilepicture"]] placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
        }else{
            [cell.imgAvatar sd_setImageWithURL:[NSURL URLWithString:dicData[@"photo"]] placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
        }
        
        int         accessType = [dicData[@"access"] intValue];
        if (accessType == 0) {
            cell.lbAccess.text = str(strAccessPrivate);
        } else if (accessType == 1) {
            cell.lbAccess.text = str(strAccessSemiPrivate);
        } else if (accessType == 2) {
            cell.lbAccess.text = str(strAccessPublic);
        }
        int nbSubscribers = [dicData[@"nbSubscribers"] intValue];
        if (nbSubscribers ==1) {
            cell.lbMember.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        }
        else
        {
            cell.lbMember.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        }
        
        [cell.btnSelected addTarget:self action:@selector(expandAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSelected.tag = section;
        if (indexExpand ==  section) {
            cell.backgroundColor = UIColorFromRGB(0xF0EEF0);
        }
        else
        {
            cell.backgroundColor = [UIColor whiteColor];
        }
        return cell;
    }else{
        return nil;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (indexExpand ==  section) {
        NSDictionary *dDic = _arrMesGroupes[section];
        
        if ([dDic[@"nbSubscribers"] intValue] > 1)
        {
            NSArray *arrMember = dDic[@"members"];
            return arrMember.count;
        }else{
            return 0;
        }
        
    }
    else
    {
            return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dicData = self.arrMesGroupes[indexPath.section];
    NSArray *arrMember = dicData[@"members"];
    NSDictionary *dicMember = arrMember[indexPath.row];
    VosGroupesBaseCell *cell = nil;
    if (indexPath.row == 0) {
        cell = (VosGroupesToutSelectedCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierToutSelected forIndexPath:indexPath];
        cell.lbTitle.text = @"Tout Sélectionner";
        cell.lbTitle.textColor = [UIColor darkGrayColor];
    }
    else
    {
        cell = (VosGroupesMemberCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierMember forIndexPath:indexPath];
        cell.lbTitle.text = dicMember[@"fullname"];
        cell.lbTitle.textColor = _colorNavigation;
        NSString *strImage=@"";
        if (dicMember[@"profilepicture"] != nil) {
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicMember[@"profilepicture"]];
        }else{
            strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicMember[@"photo"]];
        }
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        
        [cell.imgAvatar sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
    }
    [cell.btnCheckbox fnColor: [UIColor whiteColor] withColorBoderNormal:[UIColor lightGrayColor] withColorActive:_colorNavigation withColorBoderActive:[UIColor lightGrayColor]];
    [cell.btnCheckbox setTypeButton:NO];
    [cell.btnSelected addTarget:self action:@selector(selectedAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnCheckbox.selected = [dicMember[@"selected"] boolValue];
    cell.btnSelected.tag = indexPath.row;
    cell.imgAvatar.layer.masksToBounds = YES;
    cell.imgAvatar.layer.cornerRadius = 17;
    return cell;

}
-(IBAction)selectedAction:(id)sender
{    
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[VosGroupesBaseCell class]]) {
        parent = parent.superview;
    }
    
    VosGroupesBaseCell *cell = (VosGroupesBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    NSInteger index = indexPath.row;
    NSInteger section = indexPath.section;
    
    NSMutableDictionary *dicData = [self.arrMesGroupes[section] mutableCopy];
    NSMutableArray *arrMember = [dicData[@"members"] mutableCopy];
    NSMutableDictionary *dicRow = [arrMember[index] mutableCopy];
    BOOL blSelected = ![dicRow[@"selected"] boolValue];
    [dicRow setObject:@(blSelected) forKey:@"selected"];
    [arrMember replaceObjectAtIndex:index withObject:dicRow];
    if (index == 0) {
        for (int i = 1; i < arrMember.count; i++) {
            NSMutableDictionary *dicItem = [arrMember[i] mutableCopy];
            [dicItem setObject:@(blSelected) forKey:@"selected"];
            [arrMember replaceObjectAtIndex:i withObject:dicItem];
            [dicData setObject:arrMember forKey:@"members"];
            [self.arrMesGroupes replaceObjectAtIndex:section withObject:dicData];
            [self checkArrSelected:dicItem];
        }
    }
    else
    {
        [self checkArrSelected:dicRow];
        NSInteger coutSelected = 0;
        for (int i = 1; i < arrMember.count; i++) {
            NSMutableDictionary *dicItem = [arrMember[i] mutableCopy];
            if ([dicItem[@"selected"] boolValue] == blSelected) {
                coutSelected++;
            }
        }
        if ((coutSelected == arrMember.count - 1 && blSelected == TRUE) || (coutSelected > 0 && blSelected == FALSE)) {
            NSInteger index0 = 0;
            NSMutableDictionary *dicItem = [arrMember[index0] mutableCopy];
            [dicItem setObject:@(blSelected) forKey:@"selected"];
            [arrMember replaceObjectAtIndex:index0 withObject:dicItem];
        }
    }
    [dicData setObject:arrMember forKey:@"members"];
    [self.arrMesGroupes replaceObjectAtIndex:section withObject:dicData];
    [self.tableControl reloadData];
}
-(void)checkArrSelected:(NSDictionary*)dic
{
    BOOL blSelected = [dic[@"selected"] boolValue];
    NSMutableDictionary *dicRow = [dic mutableCopy];
    //kiem tra trong _arrSelected
    BOOL isExit = FALSE;
    for (int k = 0; k < _arrSelected.count; k++) {
        NSMutableDictionary *dicSelected = _arrSelected[k];
        if ([dicRow[@"id"] intValue] == [dicSelected[@"id"] intValue]) {
            if (!blSelected) {
                [_arrSelected removeObjectAtIndex:k];
                break;
            }
            isExit = TRUE;
        }
    }
    if (blSelected && !isExit) {
        [_arrSelected addObject:dicRow];
    }
}
-(IBAction)expandAction:(id)sender
{
    if (indexExpand == [sender tag]) {
        indexExpand = -1;
    }
    else
    {
        indexExpand = [sender tag];
        [self fnGetMemberList:indexExpand];
    }
    [self.tableControl reloadData];
    
}
#pragma mark - MemberAction
-(void)fnGetMemberList:(NSInteger)index {
    NSMutableDictionary *dicItem = [_arrMesGroupes[index] mutableCopy];
    if (!dicItem[@"members"]) {
        NSString *strLoungeID=_arrMesGroupes[index][@"id"];
        [COMMON addLoadingForView:self];
        __weak typeof(self) wself = self;
        WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
        [loungeSubscribeAction getGroupToutesSubscribeAction:strLoungeID];
        loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (response[@"subscribers"]) {
                NSMutableArray *arrMembers =[NSMutableArray new];
                [arrMembers addObject:@{@"tout":@(1)}];
//                [arrMembers addObjectsFromArray:response[@"subscribers"]];
                NSArray *arrResponse = response[@"subscribers"];
                NSInteger coutResponse = 0;
                for (int i=0; i<arrResponse.count; i++) {
                    NSMutableDictionary *dic = [arrResponse[i][@"user"] mutableCopy];
                    if (![[dic[@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                        [arrMembers addObject:dic];
                    }
                    //kiem tra trong day selected
                    for (NSDictionary *dicSelected in _arrSelected) {
                        if ([dic[@"id"] intValue] == [dicSelected[@"id"] intValue]) {
                            [dic setObject:@(TRUE) forKey:@"selected"];
                            [arrMembers replaceObjectAtIndex:i withObject:dic];
                            coutResponse ++;
                        }
                    }
                }
                if (arrMembers.count > 1) {
                    if (coutResponse == arrMembers.count - 1) {
                        NSMutableDictionary *dic0 = [arrMembers[0] mutableCopy];
                        [dic0 setObject:@(TRUE) forKey:@"selected"];
                        [arrMembers replaceObjectAtIndex:0 withObject:dic0];
                    }
                    
                    [dicItem setObject:arrMembers forKey:@"members"];
                    [_arrMesGroupes replaceObjectAtIndex:index withObject:dicItem];
                    [wself.tableControl  reloadData];
                }
            }
            
        };
    }
}
-(void)awakeFromNib{
    [super awakeFromNib];
}
@end
