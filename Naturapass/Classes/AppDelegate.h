//
//  AppDelegate.h
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainNavigationController.h"

#import "MDLoginVC.h"
#import "Naturapass-Swift.h"
#import "FMDB.h"

@import SocketIO;

@class MDLoginVC;

static NSString * const kAppDelegateGeolocationNotification = @"kAppDelegateGeolocationNotification";
static NSString * const kAppDelegateLoungeMessageNotification = @"kAppDelegateLoungeMessageNotification";
typedef void (^locationCallback)(CLLocation *newLocation);

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    
    SocketIOClient* socket;

}
@property (nonatomic,copy) locationCallback callback;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MainNavigationController *navigationController;
@property (strong, nonatomic) MDLoginVC *loginVC;
@property (strong, nonatomic) FMDatabase *db;
-(void)showErrorViewController;
-(void) showErrMsg :(NSString*)strMsg;
-(void)loadLoginView;
- (void)hideSplashView;
-(void)showSplashView;
-(void)showLiveHunt;
-(void)gotoScreenWithTypeNotification:(NSString*)type IdNoti:(NSString*)IdNoti dicNotif:(NSDictionary*)dic;
-(void) showPostMsgSuccess :(NSString*)strMsg;
-(void) cacheLocalNotify :(NSDictionary*)userInfo;
-(void) forceLogout;
-(void)updateAllowShowAdd:(id)response;
- (void) gotoApplication;
-(void)showAgenda;

-(void) fnScheduleLivehunt: (NSString*)livehuntID withDate:(NSString*) startDate;
-(void) fnScheduleWithListLivehunt: (NSArray*) arrData;

@end

