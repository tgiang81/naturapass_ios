//
//  Media_SheetView.m
//  Naturapass
//
//  Created by GiangTT on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Media_SheetView.h"

#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "CommonHelper.h"
#import "FileHelper.h"

@implementation Media_SheetView
{
    
}

-(instancetype)initWithType:(MEDIATYPE)type withParent:(UIViewController*) parent target:(ISSCREEN)target
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"Media_SheetView" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        
        myType = type;
        
        myParent = parent;
        self.expectTarget = target;
        
        if (myType == TYPE_PHOTO) {
            [_btnPhoto   setTitle:str(strPrendre_une_photo) forState:UIControlStateNormal];
            [_btnLibrary   setTitle:str(strChoisir_dans_la_bibliotheque) forState:UIControlStateNormal];
            [_btnClose  setTitle:str(strAAnnuler) forState:UIControlStateNormal];
            _lbTitle.text = str(strPublier_une_photo);
            
        }else{
            [_btnPhoto   setTitle:str(strFilmer_une_video) forState:UIControlStateNormal];
            [_btnLibrary   setTitle:str(strChoisir_dans_la_bibliotheque) forState:UIControlStateNormal];
            [_btnClose  setTitle:str(strAAnnuler) forState:UIControlStateNormal];
            _lbTitle.text = str(strPOSTVIDEO);
            
        }
        
        // Do any additional setup after loading the view from its nib.
        [self fnSettingCell:self.expectTarget];
        
        
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        
        [COMMON listSubviewsOfView:self];
    }
    return self;
}

-(void)fnSettingCell:(ISSCREEN)type
{
    
    switch (type) {
        case ISMUR:
        {
            [self fnSetColor: UIColorFromRGB(POSTER_NAV_COLOR)];
            self.viewSelected.backgroundColor = UIColorFromRGB(POSTER_BACKGROUND_COLOR);
        }
            break;
        case ISGROUP:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case ISLOUNGE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        }
            break;
        case ISLIVE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR)];
            
        }
            break;
        case ISCARTE:
        {
            [self fnSetColor: UIColorFromRGB(CARTE_NAV_COLOR)];
            
        }
            break;
        default:
            break;
    }
}

-(void)fnSetColor:(UIColor*)color
{
    [_btnPhoto setBackgroundColor:color];
    [_btnLibrary setBackgroundColor:color];
    [_btnClose setBackgroundColor:color];
}


-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(Media_SheetViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(Media_SheetViewCallback ) cb
{
    self.callback = cb;
    
}

#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
}
-(void)autoShowCamera
{
    [self fnCamera:nil];
    [self showAlert];
}
#pragma mark - FUNCTIONS

-(IBAction)closeAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(nil);
        
    }
}

-(IBAction)fnCamera:(id)sender
{
    [self onHide:nil];
    [self onTakePhoto:nil];
}

-(IBAction)fnLibrary:(id)sender
{
    [self onHide:nil];
    [self onTakeLibrary:nil];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - MEDIA


- (void)onTakePhoto:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorryNoCamera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
        [altView show];
        return;
    }
    [self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    
}

- (void)onTakeLibrary:(id)sender {
    [self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}


- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType
{
    //iPhone
    UIImagePickerController *imagePicker;
    
    NSArray *mediaTypes = nil;
    
    if (myType == TYPE_PHOTO) {
        //        mediaTypes = [UIImagePickerController
        //                      availableMediaTypesForSourceType:sourceType];
        mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil];
        
    }else{
        mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    }
    
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.mediaTypes = mediaTypes;
    imagePicker.delegate = (id)self;
    imagePicker.sourceType = sourceType;
    
    [myParent presentViewController:imagePicker animated:NO completion:Nil];
    
}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}


- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
    //    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data
    
    
    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CLLocation * loc = app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        
        
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

#pragma mark  -  PICKER DELEGATE

// FIXME: Rewrite me!

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    //kUTTypeMovie
    NSString *aNSString = (__bridge NSString *)kUTTypeImage;
    
    if ( [info[@"UIImagePickerControllerMediaType"] isEqualToString:aNSString] )
    {
        if([info objectForKey:@"UIImagePickerControllerOriginalImage"])
        {
            NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
            if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
                UIImage *tmpimage = [info objectForKey:UIImagePickerControllerOriginalImage];
                
                NSString *strName = [CommonHelper  generatorString];
                
                NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
                NSData *imageData = UIImageJPEGRepresentation(tmpimage,1);
                [imageData writeToFile:photoFile atomically:YES];
                
                NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
                
                
                //            assetURL.path;
                
                if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                    
//                    [self saveImage:tmpimage withInfo:info];
                    
                    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    
                    CLLocation * loc = app.locationManager.location;
                    
                    
                    if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
                        NSDictionary *retDic = @{@"image":strName,
                                                 @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                                                 @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
                        _callback(retDic);
                    }else{
                        _callback(@{@"image":strName});
                    }
                    [self onCancel:nil];
                    
                }else{
                    void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *) = ^(ALAsset *asset)
                    {
                        NSDictionary *metadata = asset.defaultRepresentation.metadata;
                        //                    ASLog(@"Image Meta Data: %@",metadata);
                        NSNumber *latNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Latitude"];
                        NSNumber *lonNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Longitude"];
                        
                        if (latNum && lonNum) {
                            NSDictionary *retDic = @{@"image":strName,
                                                     @"Latitude":latNum,
                                                     @"Longitude":lonNum};
                            _callback(retDic);
                        }else{
                            _callback(@{@"image":strName});
                        }
                        
                        [self onCancel:nil];
                    };
                    
                    
                    //        fileData   = [ NSData dataWithContentsOfURL: [NSURL fileURLWithPath: dataDic[strPostAttachmentKey][@"kFileData"] ] ]; //Video URL
                    
                    
                    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                    //            NSURL *imageDataURL=[NSURL URLWithString:@"assets-library" ];
                    //            [[UIApplication sharedApplication] openURL:imageDataURL];
                    [library assetForURL:assetURL
                             resultBlock:ALAssetsLibraryAssetForURLResultBlock
                            failureBlock:^(NSError *error) {
                            }];
                }
                
            }
        }
    }else{
        
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
        
        
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            
            //reject file size > 100 MB
            NSError *attributesError;
            NSDictionary *fileAttributes = [[NSFileManager defaultManager] attributesOfItemAtPath:[videoUrl path] error:&attributesError];
            NSNumber *fileSizeNumber = [fileAttributes objectForKey:NSFileSize];
            long long fileSize = [fileSizeNumber longLongValue];
            float fileMBsize = (float)fileSize/1024/1024;
            
//            ASLog(@"SIZE OF VIDEO: %0.2f Mb", (float)fileSize/1024/1024);

            if (fileMBsize > 100) {
                //alert cancel
//
                UIAlertView *salonAlert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:@"La taille de votre photo/vidéo ne doit pas excéder 100MB." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [salonAlert show];

            }else{
                NSString *moviePath = [videoUrl path];
                
                if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                    
                    _callback(@{@"videourl":moviePath});
                }
            }
        }
        
        [self onCancel:nil];
        
    }
    
    
    
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [picker dismissViewControllerAnimated: YES completion: NULL];
    
    [self onCancel:nil];
}

- (IBAction)onCancel:(id)sender {
    [self removeFromSuperview];
}

- (void)onHide:(id)sender {
    [self setHidden:YES];
}


@end
