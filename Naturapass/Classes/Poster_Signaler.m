//
//  ChassesCreateV2.m
//  Naturapass


#import "Poster_Signaler.h"
#import "Define.h"
#import "AlertVC.h"
#import "NetworkCheckSignal.h"
#import "Poster_ChoixSpecNiv1.h"
#import "Poster_ChoixSpecNiv4_elargi.h"
#import "Poster_ListFavorite.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "Poster_ChoixSpecNiv2.h"
#import "Poster_ChoixSpecNiv4.h"
#import "Poster_ChoixSpecNiv5.h"
#import "PosterTerminez.h"

@interface AnimalCollectionSignaler : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *imgAnimal;

@end

@implementation AnimalCollectionSignaler

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

@end

@interface Poster_Signaler ()
{
    __weak IBOutlet UICollectionView *collectionView;
    __weak IBOutlet NSLayoutConstraint *constraintBottom;
    __weak IBOutlet UIButton *btnFavorite;
    
}

@end

@implementation Poster_Signaler

- (void)viewDidLoad {
    [super viewDidLoad];
    
    MainNavigationBaseView *subview =  [self getSubMainView];
    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
    
    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
    
    switch (self.iType) {
        case PUB1:
        {
            [subview.myDesc setText:@"Précisez le signalement..."];
            
            _arrData = [NSMutableArray new];
            [_arrData addObjectsFromArray:
             
             @[ @{ @"OBJECT": @"ANIMAL TUÉ", @"image": @"ic_animal_tue",
                   @"SECOND SCREEN":@[
                           @{@"nametree":@"ANIMAL TUÉ_SANGLIER", @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                           @{@"nametree":@"ANIMAL TUÉ_CHEVREUIL", @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                           @{@"nametree":@"ANIMAL TUÉ_RENARD", @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                           @{@"nametree":@"ANIMAL TUÉ_CERF", @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                           @{@"nametree":@"ANIMAL TUÉ_FAISAN", @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                           @{@"nametree":@"ANIMAL TUÉ_LIEVRE", @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                           @{@"nametree":@"ANIMAL TUÉ_CANARD", @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                           @{@"nametree":@"ANIMAL TUÉ_PERDRIX", @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                           @{@"nametree":@"ANIMAL TUÉ_BECASSE", @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                           @{@"nametree":@"ANIMAL TUÉ_PIGEON", @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                           @{@"nametree":@"ANIMAL TUÉ_LAPIN", @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                           @{@"nametree":@"ANIMAL TUÉ_CORBEAUX", @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                           @{@"nametree":@"ANIMAL TUÉ_CORNEILLE", @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},
                           @{@"nametree":@"ANIMAL TUÉ_CHAMOIS", @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                           @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                           
                           ]
                   }, @{ @"OBJECT":@"ANIMAL VU", @"image": @"ic_animal_vu",
                         @"SECOND SCREEN":@[
                                 @{@"nametree":@"ANIMAL VU_SANGLIER", @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                 @{@"nametree":@"ANIMAL VU_CHEVREUIL", @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                 @{@"nametree":@"ANIMAL VU_RENARD", @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                 @{@"nametree":@"ANIMAL VU_CERF", @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                 @{@"nametree":@"ANIMAL VU_FAISAN", @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                                 @{@"nametree":@"ANIMAL VU_LIEVRE", @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                                 @{@"nametree":@"ANIMAL VU_CANARD", @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                                 @{@"nametree":@"ANIMAL VU_PERDRIX", @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                                 @{@"nametree":@"ANIMAL VU_BECASSE", @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                 @{@"nametree":@"ANIMAL VU_PIGEON", @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                                 @{@"nametree":@"ANIMAL VU_LAPIN", @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                                 @{@"nametree":@"ANIMAL VU_CORBEAUX", @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                                 @{@"nametree":@"ANIMAL VU_CORNEILLE", @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},
                                 @{@"nametree":@"ANIMAL VU_CHAMOIS", @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                 @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                                 
                                 ]
                         }, @{ @"OBJECT":@"ANIMAL LOUPÉ", @"image": @"ic_animal_loupe",
                               
                               @"SECOND SCREEN":@[
                                       @{@"nametree":@"ANIMAL LOUPÉ_SANGLIER", @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_CHEVREUIL", @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_RENARD", @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_CERF", @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_FAISAN", @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_LIEVRE", @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_CANARD", @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_PERDRIX", @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_BECASSE", @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_PIGEON", @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_LAPIN", @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_CORBEAUX", @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                                       @{@"nametree":@"ANIMAL LOUPÉ_CORNEILLE", @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},                                       
                                       @{@"nametree":@"ANIMAL LOUPÉ_CHAMOIS", @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                       @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                                       
                                       ]
                               }, @{ @"OBJECT":@"ANIMAL BLESSÉ", @"image": @"ic_animal_blesse",
                                     @"SECOND SCREEN":@[
                                             @{@"nametree":@"ANIMAL BLESSÉ_SANGLIER", @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                             @{@"nametree":@"ANIMAL BLESSÉ_CERF", @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                             
                                             @{@"nametree":@"ANIMAL BLESSÉ_CHEVREUIL", @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                             @{@"nametree":@"ANIMAL BLESSÉ_RENARD", @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                             
                                             //mising icon
                                             @{@"nametree":@"ANIMAL BLESSÉ_CHIEN", @"OBJECT":@"CHIEN", @"image": @"live_animal_ic_chien"},
                                             @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                                             
                                             ]
                                     },
                                    @{ @"OBJECT":@"TRACES", @"image": @"ic_animal_traces",
                                           
                                           @"SECOND SCREEN":@[
                                                   @{@"nametree":@"TRACES_SANGLIER", @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                                   @{@"nametree":@"TRACES_CERF", @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                                   @{@"nametree":@"TRACES_CHEVREUIL", @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                                   @{@"nametree":@"TRACES_CHAMOIS", @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                                   @{@"nametree":@"TRACES_BECASSE", @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                                   @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                                                   
                                                   ]
                                           } ,
                @{@"TYPE":@(CARD), @"nametree":@"JESUISICI", @"OBJECT":@"JE SUIS ICI", @"image": @"ic_cate_jesuisici"} ,
                @{@"TYPE":@(CARD), @"nametree":@"SOS", @"OBJECT":@"SOS", @"image": @"ic_cate_sos"} ,
                @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                ] ] ;
            
            
        }
            
            break;
        case PUB2:
        {
            [subview.myDesc setText:@"Animaux..."];
            
            [self fnHideFavorite];
            
        }
            
            break;
            
        case PUB3:
        {
            [subview.myDesc setText:@"Animaux..."];
            
            [self fnHideFavorite];
        }
            
            break;
            
        default:
            break;
    }
    
}

-(void) fnHideFavorite
{
    //bottom
    constraintBottom.constant = 0;
    self.vBottom.hidden = TRUE;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: self.colorNavigation ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)favoriteAction:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //seem offline....or no fav...
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
            NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
            
            
            NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE ( c_user_id=%@ AND c_default=%@ ) ORDER BY c_id", sender_id, @"1"];
            FMResultSet *set_querry = [db  executeQuery:strQuerry];
            
            NSMutableArray*mutArr = [NSMutableArray new];
            
            while ([set_querry next])
            {
                [mutArr addObject:@{ @"name": [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ? [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] : @"",
                                     @"id": [set_querry stringForColumn:@"c_id"] ? [set_querry stringForColumn:@"c_id"] : @""
                                     
                                     }];
            }
            
            
            [PublicationOBJ sharedInstance].arrCacheFavorites = mutArr;
            Poster_ListFavorite *viewController1 = [[Poster_ListFavorite alloc] initWithNibName:@"Poster_ListFavorite" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }];
    });
    
}

#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrData.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = _arrData[indexPath.row];
    
    switch ([dic[@"TYPE"] intValue]) {
            
        case OTHER:
        {
            //1 or 3
            
            Poster_ChoixSpecNiv1 *viewController1 = [[Poster_ChoixSpecNiv1 alloc] initWithNibName:@"Poster_ChoixSpecNiv1" bundle:nil];
            if (self.expectTarget == ISLIVE) {
                [self pushVC:viewController1 animate:YES expectTarget:ISLIVE iAmParent:NO];
            } else {
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
            
        }
            break;
            
        case CARD:
        {
            NSString *path = [[NSBundle mainBundle] resourcePath];
            path = [path stringByAppendingPathComponent: [NSString stringWithFormat: @"New_Publication_SubTrees/%@.json",dic[@"nametree"] ]] ;
            NSDictionary *dicSubTree = [NSDictionary dictionaryWithContentsOfFile:path];
            
            //loupe Signaler_ChoixSpecNiv4
            //                    NSString *strName = dic[@"nametree"] ;
            
            if ( ( (NSArray*)dicSubTree[@"children"]).count > 0 ) {
                
                //a sub tree
                //has search option
                if ([dicSubTree[@"search"] intValue] ==  1)
                {
                    Poster_ChoixSpecNiv4 *viewController1 = [[Poster_ChoixSpecNiv4 alloc] initWithNibName:@"Poster_ChoixSpecNiv4" bundle:nil];
                    
                    viewController1.myDic = dicSubTree;
                    //                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
                    
                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                    
                }else{
                     Poster_ChoixSpecNiv2 *viewController1 = [[Poster_ChoixSpecNiv2 alloc] initWithNibName:@"Poster_ChoixSpecNiv2" bundle:nil];
                    viewController1.myDic = dicSubTree;
                    //                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                }
                
            }else{
                //a card
                
                if ( dicSubTree[@"card"] ) {
                    Poster_ChoixSpecNiv5 *viewController1 = [[Poster_ChoixSpecNiv5 alloc] initWithNibName:@"Poster_ChoixSpecNiv5" bundle:nil];
                    viewController1.myDic = dicSubTree;
                    viewController1.iSpecific =  0;
                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                }else{
                    //leaf
                    PosterTerminez *viewController1 = [[PosterTerminez alloc] initWithNibName:@"PosterTerminez" bundle:nil];
                    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    
                }
            }
        }
            break;

        case NORMAL:
        {
            
            switch (self.iType) {
                case PUB1:
                {
                    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
                    Poster_Signaler *viewController1 = [sb instantiateViewControllerWithIdentifier:@"Poster_Signaler"];
                    viewController1.iType = PUB2;
                    viewController1.arrData = dic[@"SECOND SCREEN"];
                    if (self.expectTarget == ISLIVE) {
                        [self pushVC:viewController1 animate:YES expectTarget:ISLIVE iAmParent:NO];
                    } else {
                        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                    }
                }
                    break;
                    
                case PUB2:
                {
                    //depend on sub tree branch...
                    //read content from file:
                    NSString *path = [[NSBundle mainBundle] resourcePath];
                    path = [path stringByAppendingPathComponent: [NSString stringWithFormat: @"New_Publication_SubTrees/%@.json",dic[@"nametree"] ]] ;
                    NSDictionary *dicSubTree = [NSDictionary dictionaryWithContentsOfFile:path];
                    
                    //loupe Poster_ChoixSpecNiv4
                    //                    NSString *strName = dic[@"nametree"] ;
                    
                    if ( ( (NSArray*)dicSubTree[@"children"]).count > 0 ) {
                        
                        //a sub tree
                        //has search option
                        if ([dicSubTree[@"search"] intValue] ==  1)
                        {
                            Poster_ChoixSpecNiv4 *viewController1 = [[Poster_ChoixSpecNiv4 alloc] initWithNibName:@"Poster_ChoixSpecNiv4" bundle:nil];
                            
                            viewController1.myDic = dicSubTree;
                            //                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
                            
                            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                            
                            
                        }else{
                            Poster_ChoixSpecNiv2 *viewController1 = [[Poster_ChoixSpecNiv2 alloc] initWithNibName:@"Poster_ChoixSpecNiv2" bundle:nil];
                            viewController1.myDic = dicSubTree;
                            //                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
                            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                            
                        }
                        
                    }else{
                        //a card
                        
                        if ( dicSubTree[@"card"] ) {
                            Poster_ChoixSpecNiv5 *viewController1 = [[Poster_ChoixSpecNiv5 alloc] initWithNibName:@"Poster_ChoixSpecNiv5" bundle:nil];
                            viewController1.myDic = dicSubTree;
                            viewController1.iSpecific =  0;
                            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                            
                        }else{
                            //leaf
//                            PosterTerminez *viewController1 = [[PosterTerminez alloc] initWithNibName:@"PosterTerminez" bundle:nil];
//                            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                            [self gobackParent];
                            
                        }
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = @"AnimalCollectionSignaler";
    
    //    NSDictionary *dic = _arrData[indexPath.row];
    
    AnimalCollectionSignaler *cell = (AnimalCollectionSignaler *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dic = _arrData[indexPath.row];
    cell.imgAnimal.image = [UIImage imageNamed:dic[@"image"]];
    cell.name.text = dic[@"OBJECT"];
    
    return cell;
}

//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3.05 , collectionView.frame.size.width/3.05);
}

@end
