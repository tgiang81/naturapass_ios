//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCell71.h"
#import "Define.h"
#import "CommonHelper.h"
#import "CellKindInvite.h"
#import "MDCheckBox.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Config.h"
#import "WebServiceAPI.h"
#import "AppCommon.h"
#import "KSToastView.h"

static NSString *Cell_Step3 =@"CellKindInvite";
static NSString *Cell_Step3_ID =@"CellKind3ID";

@implementation TypeCell71
{
    ISSCREEN      expectTarget;

}
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
    self.view1.layer.cornerRadius= 5.0;
    self.view1.layer.borderWidth =0.5;
    self.view1.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [[CommonHelper sharedInstance] setRoundedView:self.img1 toDiameter:self.img1.frame.size.height /2];
    [self.tblControl registerNib:[UINib nibWithNibName:Cell_Step3 bundle:nil] forCellReuseIdentifier:Cell_Step3_ID];
    _tourMemberArray = [NSMutableArray new];
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label4.preferredMaxLayoutWidth = CGRectGetWidth(self.label4.frame);

}
-(void)fnSettingCell:(int)type expectTarget:(ISSCREEN)target
{
    intType = type;
    expectTarget = target;
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
    [self.label3 setTextColor:color];
    [self.button1 setBackgroundColor:color];
    [self.button2 setBackgroundColor:color];
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
      int  noOfRows = (int)[_tourMemberArray count];
    
    return noOfRows;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary*dic = _tourMemberArray[indexPath.row];
    CellKindInvite *cell = (CellKindInvite *)[self.tblControl dequeueReusableCellWithIdentifier:Cell_Step3_ID forIndexPath:indexPath];
    
    if (dic[@"user"][@"profilepicture"] != nil) {
        [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString: [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"user"][@"profilepicture"] ] ] ];
    }else{
        [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString: [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"user"][@"photo"] ] ] ];
    }
    
    
    //FONT
    cell.label1.text = dic[@"user"][@"fullname"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    //Arrow
    cell.btnControl.tag=indexPath.row;
    [cell.btnControl addTarget:self action:@selector(fnInviteMember:) forControlEvents:UIControlEventTouchUpInside];
    [cell fnSettingCell:intType];
    if (dic[@"inviter"]) {
        [cell.btnControl setSelected:YES];
        cell.btnControl.backgroundColor = [UIColor lightGrayColor];
        [cell.btnControl setTitle:str(strENVOYE) forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnControl setTitle:str(strEnvoyer) forState:UIControlStateNormal];
        [cell.btnControl setSelected:NO];
        switch (intType) {
            case UI_GROUP_MUR_ADMIN:
            {
                cell.btnControl.backgroundColor =UIColorFromRGB(GROUP_TINY_BAR_COLOR);
                
            }
                break;
            case UI_GROUP_MUR_NORMAL:
            {
                cell.btnControl.backgroundColor =UIColorFromRGB(GROUP_TINY_BAR_COLOR);

                
            }
                break;
            case UI_CHASSES_MUR_ADMIN:
            {
                cell.btnControl.backgroundColor =UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
                
            }
                break;
            case UI_CHASSES_MUR_NORMAL:
            {
                cell.btnControl.backgroundColor =UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
            }
                break;
            default:
                break;
        }
    }
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(void)fnShowMemberWithArray:(NSArray*)arr
{
    _tourMemberArray = [NSMutableArray arrayWithArray:arr];
    contraintHeightTable.constant = 66*arr.count;

    [_tblControl reloadData];
    [_tblControl setContentSize:CGSizeMake(CGRectGetWidth(_tblControl.frame), CGRectGetHeight(_tblControl.frame))];
    [_tblControl setScrollEnabled:NO];
    
}
-(IBAction)fnInviteMember:(MDCheckBox*)sender
{
    int index = (int) sender.tag;
    NSDictionary * dic = _tourMemberArray[index];
    
    if ([sender isSelected]) {
        NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
        if (mulDic[@"inviter"]) {
            [mulDic removeObjectForKey:@"inviter"];
        }
        [_tourMemberArray replaceObjectAtIndex:index withObject:mulDic];
        [_tblControl reloadData];
    }
    else
    {
        [COMMON addLoading:self.parent];

            WebServiceAPI *serviceObj = [WebServiceAPI new];
            
            if (expectTarget == ISLOUNGE) {
                [serviceObj postUserFriendsAction: _strID andFriend:dic[@"user"][@"id"]];
            }
            else
            {
                [serviceObj postGroupInviteFriendNaturapass: _strID andFriend:dic[@"user"][@"id"]];
            }
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                
                    [COMMON removeProgressLoading];
                    
                    if (!response) {
                        
                        [KSToastView ks_showToast:NSLocalizedString(@"INVALID_ERROR", @"")  duration:2.0f completion: ^{
                        }];

                        return ;
                    }
                    if([response isKindOfClass: [NSArray class] ]) {
                        return ;
                    }
                    if (response[@"success"]) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strVotre_demande_envoyee) delegate:nil cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
                        [alert show];
                        
                        NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
                        if (mulDic[@"inviter"]) {
                            [mulDic removeObjectForKey:@"inviter"];
                        }
                        [mulDic setObject:@"1" forKey:@"inviter"];
                        [_tourMemberArray replaceObjectAtIndex:index withObject:mulDic];
                        [_tblControl reloadData];
                    }else{
                        if ([response isKindOfClass:[NSDictionary class]]) {
                            if ([response[@"message"] isKindOfClass:[NSString class]]) {
                                [KSToastView ks_showToast: response[@"message"] duration:2.0f completion: ^{
                                }];
                            }
                        }

                    }

            };
        

    }
}

@end
