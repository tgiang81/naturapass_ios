//
//  AvatarViewController.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 5/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//


#import <UIKit/UIKit.h>
@protocol AVATARVIEWDELEGATE;
@interface AvatarViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout>
{
    NSArray          *avatarArray;

    IBOutlet  UICollectionView  *myCollection;
    
    UIButton                *avatarButton;
}
@property(nonatomic,retain)id<AVATARVIEWDELEGATE>avatarDelegate;

@end

@protocol AVATARVIEWDELEGATE <NSObject>

-(void)loadAvatarImage:(NSString *)_strImage;
-(void)gotoBack;

@end