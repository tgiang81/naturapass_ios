//
//  AppDelegate.m
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import "AppDelegate.h"

#import "AppCommon.h"
#import "ServiceHelper.h"
#import "FileHelper.h"
#import "UserSettingsHelper.h"
#import "ErrorViewController.h"
#import "NSDate+Extensions.h"
#import "Define.h"
#import "UIAlertView+Blocks.h"
#import "AZNotification.h"
#import "AZNotificationView.h"
#import "HomeVC.h"
#import "ChassesMurVC.h"
#import "MDLoginVC.h"
#import "CommentDetailVC.h"
#import "HNKGooglePlacesAutocompleteQuery.h"

#import "Group_Hunt_ListShare.h"
#import "iRate.h"
#import "iVersion.h"
#import "FMDB.h"
#import "FileHelper.h"
#import "DatabaseManager.h"
#import "MapDataDownloader.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <GoogleMaps/GoogleMaps.h>
#import "GroupEnterMurVC.h"
#import "Flurry.h"
#import "ChassesParticipeVC.h"
#import "SplashVC.h"
#import "ShortcutScreenVC.h"
#import "CommonHelper.h"
#import "PublicationVC.h"
#import "SplashGif.h"
#import "NSString+EMOEmoji.h"
#import "MapGlobalVC.h"
#import "SignalerTerminez.h"

//ggtt refresh list sending message
#import "MessageWaitToSend.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "CommonObj.h"
#import <UserNotifications/UserNotifications.h>

#import "RecordingDB.h"
#import "LiveHuntOBJ.h"

#import "NSDate+Extensions.h"
#import "MZFormSheetController.h"
#import "AlertLivehunt_StartdateChange.h"
#import "AlertStatisticVC.h"

@import Firebase;

@interface AppDelegate () <CLLocationManagerDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>
{
    dispatch_queue_t backgroundQueue;
    NSOperationQueue * operationQueue;
    UIApplication *app;
    NSDictionary *tmpDicEventSpecial;
    //    BOOL isGameFairWaitingApprove;
    //    NSArray *arrCheckGameFairTime;
    SocketManager* manager;
}

@end

@implementation AppDelegate


- (void)initialize
{
    //overriding the default iRate strings
    [iRate sharedInstance].appStoreID = APP_ID_BUNDLE;
    [iRate sharedInstance].messageTitle =  str(strTitle_app);
    [iRate sharedInstance].message = str(strRatingAppStore);
    [iRate sharedInstance].cancelButtonLabel = str(strJeneveuxpas);
    [iRate sharedInstance].remindButtonLabel = str(strPlustard);
    [iRate sharedInstance].rateButtonLabel = str(strOui);
    
    //set the bundle ID. normally you wouldn't need to do this
    //as it is picked up automatically from your Info.plist file
    //but we want to test with an app that's actually on the store
    [iVersion sharedInstance].appStoreID = APP_ID_BUNDLE;
    [[iVersion sharedInstance] setUpdateAvailableTitle:str(strNewVertionAvailale)];
    [[iVersion sharedInstance] setInThisVersionTitle:str(strNewInThisVersion)];
    [[iVersion sharedInstance] setRemindButtonLabel:str(strPlustard)];
    [[iVersion sharedInstance] setOkButtonLabel:str(strOui)];
    [[iVersion sharedInstance] setDownloadButtonLabel:str(strDownload)];
    [[iVersion sharedInstance] setIgnoreButtonLabel:str(strIgnore)];
    
    
    //configure iVersion. These paths are optional - if you don't set
    //them, iVersion will just get the release notes from iTunes directly (if your app is on the store)
    //    [iVersion sharedInstance].remoteVersionsPlistURL = @"http://charcoaldesign.co.uk/iVersion/versions.plist";
    //    [iVersion sharedInstance].localVersionsPlistPath = @"versions.plist";
}

-(void)setCallback:(locationCallback)callback
{
    _callback = callback;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    if (_callback) {
        _callback(newLocation);
    }
    //get altitude...medometer...
    
    NSString * strID = [NSString stringWithFormat:@"%@_recording_status", [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] ];
    
    int iVal = (int) [[NSUserDefaults standardUserDefaults] integerForKey: strID ];
    
    if (iVal == PEDOMETER_RECORDING ) {
     
        //for userid + huntid + positive + negative Altitude
//        NSString * strAltitude = [NSString stringWithFormat:@"%f",self.locationManager.location.altitude];
        NSString * strTmpAgendaID = [ NSString stringWithFormat:@"%@", [LiveHuntOBJ sharedInstance].dicLiveHunt[@"id"]];
        CLLocationDistance horizontalAccuracy = [newLocation distanceFromLocation:oldLocation];
//        NSLog(@"horizontalAccuracy -- %f",horizontalAccuracy);
        if (horizontalAccuracy <= acuracy_val) {
            //update to table tracking
            [RecordingDB addAltitudeWithAgenda:strTmpAgendaID andValue:self.locationManager.location.altitude ];
            //
        }
    }
    

    [self.locationManager stopUpdatingLocation];
}

//run background task
-(void)runLoopCheckLocation {
    //check if application is in background mode
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground && [COMMON isLoggedIn]) {
        [self.locationManager startUpdatingLocation];
    }
    
    [self performSelector: @selector(runLoopCheckLocation) withObject:nil afterDelay:5];
    
    //get noti
    [self fnGetAllNotifications];
}

-(void)fnGetAllNotifications
{
    
    NSURLSessionDataTask *task;
    NSURL *URL = [NSURL URLWithString:GET_ALL_NOTIFICATION];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: URL
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval: MEDIUM_TIMEOUT ];

    [request setValue:[NSString stringWithFormat:@"PHPSESSID=%@", [[NSUserDefaults standardUserDefaults] valueForKey:@"PHPSESSID"] ] forHTTPHeaderField:@"Cookie"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    task = [session dataTaskWithRequest:request
                      completionHandler:
            ^(NSData *data, NSURLResponse *response, NSError *error) {
                
                    id jsonData = nil;
                    int code = (int)error.code;
                    //check error
                
                    code = (int)((NSHTTPURLResponse *)response).statusCode;
                    if (data == nil) {
                        return;
                    }

                    id jsonDict =[NSJSONSerialization JSONObjectWithData: data options: 0 error: NULL];
                    if ([jsonDict isKindOfClass: [NSDictionary class]])
                    {
                            jsonData = (NSDictionary*)jsonDict;
                            [CommonObj sharedInstance].nbChasseInvitation = [jsonData[@"nbChasseInvitation"] intValue];
                            [CommonObj sharedInstance].nbGroupInvitation = [jsonData[@"nbGroupInvitation"] intValue] ;
                            [CommonObj sharedInstance].nbUnreadMessage = [jsonData[@"nbUnreadMessage"] intValue] ;
                            [CommonObj sharedInstance].nbUnreadNotification = [jsonData[@"nbUnreadNotification"] intValue] ;
                            [CommonObj sharedInstance].nbUserWaiting = [jsonData[@"nbUserWaiting"] intValue];

                            //notify
                            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING_BADGE object:nil];
//
                    }

                }];
    [task resume];

}


-(void)appDidBecomeActiveNotif:(NSNotification*)notif
{
}

-(void)appWillResignActiveNotif:(NSNotification*)notif
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [_db close];
    
    [socket disconnect];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    [localContext MR_saveToPersistentStoreAndWait];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application
{
}

- (BOOL) application: (UIApplication *) application willFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
    return YES;
}

//http://hayageek.com/ios-background-task/
-(void) fnCreateBackgroundTask
{
    UIApplication * application = [UIApplication sharedApplication];

    if([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)])
    {
       ASLog(@"Multitasking Supported");
       
        // But iOS background task timer value set as 10 minutes (600 secs). So if any background task runs more than 10 minutes,then the task will be suspended and code block specified with beginBackgroundTaskWithExpirationHandler is called to clean up the task.
        
       __block UIBackgroundTaskIdentifier background_task;
       background_task = [application beginBackgroundTaskWithExpirationHandler:^ {
         
         //Clean up code. Tell the system that we are done.
         [application endBackgroundTask: background_task];
         background_task = UIBackgroundTaskInvalid;
       }];
       
       //To make the code block asynchronous
       dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         
        //Continue checking to get location...save table points.
           
         //### background task starts
         ASLog(@"Running in the background\n");
           
                // When the app goes to background, you can see backgroundTimeRemaining value decreases from 599.XXX ( 1o minutes)
        ASLog(@"Background time Remaining: %f",[[UIApplication sharedApplication] backgroundTimeRemaining]);
//                    [NSThread sleepForTimeInterval:1]; //wait for 1 se
        //get Location...
//            [self performSelector: @selector(runLoopCheckLocation) withObject:nil afterDelay:5];
           
           
          //#### background task ends
          
          //Clean up code. Tell the system that we are done.
//          [application endBackgroundTask: background_task];
//          background_task = UIBackgroundTaskInvalid;
          });
    }
    else
    {
        ASLog(@"Multitasking Not Supported");
    }

}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    
    [Fabric with:@[[Crashlytics class]]];

    ASLog(@"%@",[FileHelper PDFLocal]);
    
    //GENERATE REFACTOR DB
//    [[DatabaseManager sharedManager] switchToNewDatabase];
    
    
//    for(NSString *fontfamilyname in [UIFont familyNames])
//    {
//        NSLog(@"Family:'%@'",fontfamilyname);
//        for(NSString *fontName in [UIFont fontNamesForFamilyName:fontfamilyname])
//        {
//            NSLog(@"\tfont:'%@'",fontName);
//        }
//        NSLog(@"---");
//    }
//    
    
    //init param
    operationQueue = [NSOperationQueue new];
    backgroundQueue = dispatch_queue_create("com.naturapass.locationuserupdate", 0);
    
    //    arrCheckGameFairTime = @[@1, @7, @14, @21, @28, @34];
    
    //init location manager
    self.locationManager =[[CLLocationManager alloc] init];
    self.locationManager.delegate=self;
    //    [self.locationManager setDesiredAccuracy: kCLLocationAccuracyNearestTenMeters];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
    
//    Call the requestWhenInUseAuthorization initially to enable your app's basic location support.
//    Call the requestAlwaysAuthorization method only when the user enables a feature of your app that requires that level of authorization.
//    if ([self.locationManager respondsToSelector: @selector(requestWhenInUseAuthorization)])
//        [self.locationManager requestWhenInUseAuthorization];
    if ([self.locationManager respondsToSelector: @selector(requestAlwaysAuthorization)])
        [self.locationManager requestAlwaysAuthorization];

    //create UIBackgroundTaskIdentifier and create tackground task, which starts after time
    app = [UIApplication sharedApplication];
    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
    
    //change locationManager status after time
    [self runLoopCheckLocation];
    
    //
    [GMSServices provideAPIKey:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
    
    [Flurry startSession: FLURRY_API_KEY];
    
    //FB
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    //    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [HNKGooglePlacesAutocompleteQuery setupSharedQueryWithAPIKey:    [[CommonHelper sharedInstance] getRandKeyGoogleSearch] ];
    
    if ([[self window] respondsToSelector: @selector(setTintColor:)])
    {
        [[self window] setTintColor: COLOR_SALON_TAB_SELECT];
        
        [[UISwitch appearance] setOnTintColor: COLOR_SALON_TAB_SELECT];
        
        [[UIRefreshControl appearance] setTintColor: COLOR_SALON_TAB_SELECT];
    }
    
    if ([UITableView instancesRespondToSelector: @selector(setSeparatorInset:)])
    {
        [[UITableView appearance] setSeparatorInset: UIEdgeInsetsZero];
    }
    
    //request authority
    UNAuthorizationOptions options = UNAuthorizationOptionAlert + UNAuthorizationOptionSound;
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center requestAuthorizationWithOptions:options
                          completionHandler:^(BOOL granted, NSError * _Nullable error) {
                              if (!granted) {
                                  NSLog(@"Something went wrong");
                              }
                          }];

    
    if ([UNUserNotificationCenter class] != nil) {
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter]
         requestAuthorizationWithOptions:authOptions
         completionHandler:^(BOOL granted, NSError * _Nullable error) {
             // ...
         }];
    } else
    {
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    
    [application registerForRemoteNotifications];
    [FIRMessaging messaging].delegate = self;

    
    //User must enable in Setting...
    if ([application respondsToSelector: @selector(registerForRemoteNotifications)])
    {
        //ASLog(@"ios 8 push");
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes: UIUserNotificationTypeAlert | UIUserNotificationTypeSound
                                                                                 categories: nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings: settings];
        
        [COMMON setPushAreEnabled: [[UIApplication sharedApplication] isRegisteredForRemoteNotifications]];
        //
        //		ASLog(@"can push: %@", [[UIApplication sharedApplication] isRegisteredForRemoteNotifications] ? @"YES" : @"NO");
        //
        //		ASLog(@"other push: %@", [COMMON pushAreEnabled] ? @"YES" : @"NO");
    }

    [[NSUserDefaults standardUserDefaults]setValue:@"GROUPS" forKey:@"GROUPSTYPES"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"DataNatura"];

    //Check to clone TREE/ANIMAL at the first time.

    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"LoadedTree"] == FALSE) {
        [[NSUserDefaults standardUserDefaults] setBool: TRUE forKey:@"LoadedTree"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        NSString *path = [[NSBundle mainBundle] resourcePath];
        path = [path stringByAppendingPathComponent:@"tree.json"];
        
        NSDictionary *dicTree = [NSDictionary dictionaryWithContentsOfFile:path];
        [[MapDataDownloader sharedInstance] doUpdateTree:dicTree];
        
    }
    
    //init Pedometer
    [[RecordingDB sharedInstance] initPedometer];
    
    if ([COMMON isLoggedIn]) {
        [self doLogin];
        
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
        {
            NSDictionary *userInfo = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
            [self handleRemoteNotification:application userInfo:userInfo];
        }
        else
        {
            [self showSplashView];
        }
    }else{
        //Display Login page
        [self gotoLogin];
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    // TODO: If necessary send token to application server.

    // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    //note: file: GoogleService-Info.plist specify for Dev/Prod
    //As long as App send Token Apple or FCM, BackEnd save them to DB and processing for both Apple + FCM. :)
    //PUT server
    
    //but user not yet login ?
    
    //save fcmToken...
    [[NSUserDefaults standardUserDefaults] setObject:fcmToken forKey:@"DEVICETOKEN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void)gotoLogin
{
    Class kclass =[MDLoginVC class];
    MDLoginVC *vc =[[MDLoginVC alloc] initWithNibName:NSStringFromClass(kclass) bundle:nil];
    self.loginVC =(MDLoginVC*)vc;
    self.window.rootViewController =vc;
}

-(void)loadLoginView {
    //Offline -> can not logout.
    
    if ([COMMON isReachable]) {
        
        //Confirm
        
        [UIAlertView showWithTitle:str(strDeconnexion) message:str(strEtesvousSurDevouloirvousdeconecter)
                 cancelButtonTitle:str(strAnuler)
                 otherButtonTitles:@[str(strValider)]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                              if (buttonIndex == 1) {
                                  [self deleteUserDeviceAction];
                                  [COMMON doLogout];
                                  MDLoginVC *loginVC = [[MDLoginVC alloc] initWithNibName:@"MDLoginVC" bundle:nil];
                                  self.window.rootViewController = loginVC;
                              }
                              
                          }];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: str(strTitle_app)
                                                            message: str(strNETWORK)
                                                           delegate: nil
                                                  cancelButtonTitle: str(strOK)
                                                  otherButtonTitles: nil];
        
        [alertView show];
    }
}

-(void) forceLogout
{
    [COMMON doLogout];
    MDLoginVC *loginVC = [[MDLoginVC alloc] initWithNibName:@"MDLoginVC" bundle:nil];
    self.window.rootViewController = loginVC;
    
}

-(void)deleteUserDeviceAction{
    
    NSString *strDeviceToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICETOKEN"];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI deleteUserDeviceAction:@"ios" identifier:strDeviceToken];
    serviceAPI.onComplete= ^(id response, int errCode)
    {};
}

- (void) doLogin{
    
    if (![COMMON isReachable]) {
        return;
    }
    NSString    *strUser      = [COMMON keyChainUserID];
    NSString    *strPassword  = [COMMON keyChainPasswordID];
    NSString *strDeviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICETOKEN"];
    
    ASLog(@"token: %@", strDeviceToken);
    
    if (!strDeviceToken || ![strDeviceToken length])
        ASLog(@"NIL TOKEN");
    
    if(strUser != NULL && strPassword !=NULL)
    {
        
        WebServiceAPI *serviceAPI =[WebServiceAPI new];
        if (strDeviceToken)
        {
            [serviceAPI getRequest: REAL_LOGIN(strUser, strPassword, strDeviceToken)];
        }
        else
        {
            [serviceAPI getRequest: REAL_LOGIN_NO_PUSH(strUser, strPassword)];
        }
        
        serviceAPI.onComplete =^(id response, int errCode)
        {
            NSMutableDictionary *userDictionary = [response valueForKey:@"user"];
            if ([userDictionary isKindOfClass:[NSDictionary class]] && userDictionary != NULL) {
                NSString *user_id   = [userDictionary valueForKey:@"id"];
                NSString *user_sid  = [userDictionary valueForKey:@"sid"];
                if (user_id != NULL && user_sid != NULL) {
                    [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:@"sender_id"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue: userDictionary[@"usertag"] forKey:@"user_tag"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:user_sid forKeyPath:@"PHPSESSID"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else
                {
                }
            }
            else
            {
                //Offline???
                
            }
        };
        
    } else {
    }
}

-(void)showSplashView {
    //check show GIF? or not yet
    BOOL bChecked = [[NSUserDefaults standardUserDefaults] boolForKey:@"showedGIF"];
    
    if (bChecked == NO) {
        
        [self showTourGuide];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showedGIF"];
    }else{
        
        int iCountAds = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"ads_counting_revert"];
        
        iCountAds += 1;
        
        switch (iCountAds) {
            case 1:
                [self showSecondSplashAds];
                break;
            case 3:
            {
                [self gotoApplication];
                //reset 0
                iCountAds = 0;
            }
                break;
                
            default:
                [self gotoApplication];
                break;
        }
        
        //Save current index
        [[NSUserDefaults standardUserDefaults] setInteger:iCountAds forKey:@"ads_counting_revert"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)hideSplashView
{
    if ([self shouldShowSecondSplash]) {
        [self showSecondSplashAds];
    } else {
        [self gotoApplication];
    }
}

- (void) gotoApplication
{
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];

    if (pushToken.length > 0) {
        NSDictionary *param = @{
                                @"identifier":pushToken,
                                @"device" : @"ios",
                                @"name": @"iphone"
                                };
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            ASLog(@"Finish update token push notification => %@", response);
        };
        [serviceObj fnPUT_USER_IDENTIFIER_WITHPARAM:param];
    }

    
    //IMPORTANT Init shareInstance Database:
    [[DatabaseManager sharedManager] swithDatabase];
    [self queueFMDatabase];
    
    
    // Update missing fields...table version
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT version FROM tb_db_version ORDER BY version DESC LIMIT 1"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        NSString *strMaxVersion = @"0";
        
        while ([set_querry next])
        {
            strMaxVersion = [set_querry stringForColumn:@"version"];
        }
        
        //setting parameters

        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        
        [param setObject:@{
                           @"version":strMaxVersion
                           }
                  forKey:@"version"];
        
        
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        
        [serviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];

        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            ASLog(@"%@ \n ", param);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                NSArray *arrResult = response[@"versions"];
                
                if (arrResult.count > 0 )
                {
                    //has change. sort arr from accending
                    
                    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
                    NSArray * arrVersions = [arrResult sortedArrayUsingDescriptors:@[brandDescriptor]];
                    
                    
                    //Each version
                    for (int i=0; i < arrVersions.count; i++)
                    {
                        
                        NSDictionary *dicT1 = arrVersions[i];
                        NSArray *arrSqls = dicT1[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            //                                ASLog(@"%@", tmpStr);
                            
                            [db  executeUpdate:tmpStr];
                            
                        }
                        //add version number to tble version.
                        NSString *strInsertTblVersion = [ NSString stringWithFormat: @"INSERT INTO `tb_db_version`(`version`) VALUES ('%@');", dicT1[@"version"] ] ;
                        [db  executeUpdate:strInsertTblVersion];
                        
                        
                    }
                    
                }
                
                //done update fields...import data...
                //User logged in...use default data...
                //Replace all special String B74D8A3F-87E9-4CDF-81E7-42B501F67CFC by new c_user_id...run

                
                // > version store...
                
                        //Decide to use NEW Default Database => will use new API to get publication data...Must tick...
                
                
                //update huge data from new public DB...thinking about version...
                NSString *path = [[NSBundle mainBundle] resourcePath];
                path = [path stringByAppendingPathComponent:@"db.json"];
                
                NSData *dData = [NSData dataWithContentsOfFile:path];
                
                NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData: dData options: 0 error: NULL];

               int hugeDBversion = (int) [[NSUserDefaults standardUserDefaults] integerForKey:@"hugeDBVersion"];
                
                //1 year update
                if ( [jsonData[@"version"] intValue] > hugeDBversion ) {
                    //must update with this new HugeDB...
                    [[NSUserDefaults standardUserDefaults] setInteger:[jsonData[@"version"] intValue] forKey:@"hugeDBVersion"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    //run command
                    NSArray *arrSqls = jsonData[@"sqlite"][@"publications"] ;
                    
                    for (NSString*strQuerry in arrSqls) {
                        [db  executeUpdate:strQuerry];
                    }
                }
                //else do not need updating.

                
                //update data from remote server
                [[MapDataDownloader sharedInstance] resetParam];
                
                [self fnDoThread1];
                
                [[MapDataDownloader sharedInstance] doUpdateFavorites];
            }
            
        };
        
    }];

    //OK natura db is ready for using...

    //refresh Waiting for sending
    
    [self refreshListWaitingSend];

    //change locationManager status after time
    
    //Set Default for this session LOGIN.
    
    [[NSUserDefaults standardUserDefaults] setInteger: 12 forKey:@"defaultMapType"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [CommonHelper sharedInstance].isActiveOverlay = YES;
    
    
    //Register Location Notify
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActiveNotif:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActiveNotif:) name:UIApplicationWillResignActiveNotification object:nil];
    //    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // Initialize Reachability
    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reachability.reachableBlock = ^(Reachability *reachability) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ServiceHelper sharedInstance] startSyncingPostingData];
        });
        
        NSLog(@"Network is reachable.");
    };
    
    reachability.unreachableBlock = ^(Reachability *reachability) {
        NSLog(@"Network is unreachable.");
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ServiceHelper sharedInstance] resetStatus];
        });
    };
    
    // Start Monitoring
    [reachability startNotifier];
    
    [[UIApplication sharedApplication] setStatusBarHidden: NO];
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    
    [self.window setRootViewController:self.navigationController ];
    
    //Init socket listening notification
    
    NSURL* url = [[NSURL alloc] initWithString:LINK_SOCKET];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    socket = manager.defaultSocket;

    
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        [socket emit:@"npevent-user:connected" with:@[      @{@"usertag":userTag }]];
    } ];
    
    [socket on:@"npevent-notification:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //lounge.join.invited
        //group.join.invited
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket on:@"npevent-invitation:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //    type = "user.friendship.asked";
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket on:@"npevent-chat-message:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //lounge.join.invited
        //group.join.invited
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket on:@"npevent-lounge:change-allow" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        ASLog(@"tetstestasetas");
        //update database...
        if (data.count>0) {
            //update database...
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                for (NSDictionary*dic in data) {
                    //add version number to tble version.
                    NSString *strSql =[NSString stringWithFormat:@"UPDATE tb_hunt SET c_allow_add = %@ , c_allow_chat_add = %@ , c_allow_show = %@ , c_allow_chat_show = %@  WHERE c_id = %@ AND c_user_id = %@",dic[@"allow_add"],dic[@"allow_add_chat"],dic[@"allow_show"],dic[@"allow_show_chat"],dic[@"id"],sender_id];
                    BOOL isOK =   [db  executeUpdate:strSql];

                }
                
                //update current view
                [[NSNotificationCenter defaultCenter] postNotificationName: UPDATE_RIGHT_CHAT object:nil];
                
            }];
        }
    } ];
    
    [socket connect];
    
    //RATING
    [self initialize];
}

//516
-(void)updateAllowShowAdd:(id)response
{
    
    if ([response isKindOfClass:[NSDictionary class]]) {
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
            NSMutableArray *arrSqls= [NSMutableArray new];
            if([response[@"sqlite"] isKindOfClass:[NSDictionary class]])
            {
                if (response[@"sqlite"][@"groups"]) {
                    [arrSqls addObjectsFromArray:response[@"sqlite"][@"groups"] ];
                }
                else if (response[@"sqlite"][@"hunts"])
                {
                    [arrSqls addObjectsFromArray:response[@"sqlite"][@"hunts"] ];
                }
                if (response[@"sqlite_agenda"][@"agendas"])
                {
                    [arrSqls addObjectsFromArray:response[@"sqlite_agenda"][@"agendas"] ];
                }
                
            }
            else
            {
                [arrSqls addObjectsFromArray:response[@"sqlite"] ];
                
                [arrSqls addObjectsFromArray:response[@"sqlite_agenda"] ];
            }
            
            for (NSString*strQuerry in arrSqls) {
                //correct API
                NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                BOOL isOK =   [db  executeUpdate:tmpStr];

            }
            
        }];
    }
    
}

-(void) gotoHomeOnly
{
    [self.window setRootViewController:self.navigationController ];
    
}

//response[@"lounge"]
-(void)enterChasesWall
{
    [self updateAllowShowAdd:tmpDicEventSpecial];
    
    ChassesParticipeVC  *vc=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
    vc.dicChassesItem = tmpDicEventSpecial;
    vc.isInvitionAttend =YES;
    vc.expectTarget = ISLOUNGE;
    [vc doCallback:^(NSString *strID) {
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
        [vc pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
    }];
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    
    HomeVC *rootViewController = (HomeVC *)[viewControllers objectAtIndex:0];
    [(MainNavigationController *)rootViewController.navigationController pushViewController:vc animated:NO completion:^ {
        ASLog(@"COMPLETED");
    }];
    
    [self.window setRootViewController:self.navigationController ];
    //Home ready...goto...chasseParticipeVC
}
-(void)enterChasesMur
{
    [self updateAllowShowAdd:tmpDicEventSpecial];
    
    GroupEnterMurVC *vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    vc.expectTarget = ISLOUNGE;
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
    
    
    NSArray *viewControllers = self.navigationController.viewControllers;
    
    HomeVC *rootViewController = (HomeVC *)[viewControllers objectAtIndex:0];
    [(MainNavigationController *)rootViewController.navigationController pushViewController:vc animated:NO completion:^ {
        ASLog(@"COMPLETED");
    }];
    
    [self.window setRootViewController:self.navigationController ];
    //Home ready...goto...chasseParticipeVC
}

-(void) fnDoThread1
{
    //get small
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_favorite, kSQL_address, kSQL_hunt, kSQL_group, kSQL_breed, kSQL_type, kSQL_brand, kSQL_calibre, kSQL_agenda]];// to get all agenda for Carter search.
    
    [MapDataDownloader sharedInstance].CallBackPublication = ^(){
        //get big
        [self fnDoThread2];
    };
}

-(void) fnDoThread2
{
    [MapDataDownloader sharedInstance].CallBackPublication = nil;
    
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_publication]];
    
    [MapDataDownloader sharedInstance].CallBackShape = ^(){
        //get big
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_shape]];
        [MapDataDownloader sharedInstance].CallBackShape = nil;
    };
    
}

/*
 check tag versions: to update structure.
 finish...
 run sqlite tag.
 
 
 the process is :
 you can get for e.g all elements, but we need to start executing "versions" and not "sqlite" to update structure before adding or updating elements.
 So check if "versions" exist. if true, for each version element exec all sqlite. if no error insert version in tb_db_version table, and exec other version if we have.
 When all version are updated, exec all "sqlite"
 
 {
 sqlite =     (
 );
 versions =     (
 {
 sqlite =             (
 "ALTER TABLE `tb_shape` ADD `c_lat_center` INTEGER",
 "ALTER TABLE `tb_shape` ADD `c_lng_center` INTEGER",
 ""
 );
 version = 20160222122811;
 }
 );
 }
 
 */

//

-(void)gotoScreenWithTypeNotification:(NSString*)type IdNoti:(NSString*)IdNoti dicNotif:(NSDictionary*)dic;
{
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group, kSQL_hunt, kSQL_agenda]];
    
    [[UIApplication sharedApplication] setStatusBarHidden: NO];
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    [viewController1  gotoScreenWithTypeNotification:type IdNoti:IdNoti dicNotif:dic];
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    
    self.window.rootViewController = self.navigationController;
    
}

- (void) showTourGuide
{
    SplashGif *viewController1 = [[SplashGif alloc] initWithNibName:@"SplashGif" bundle:nil];
    [viewController1 setCallBackClose:^(){
        [self hideSecondSplashAds];
    }];
    
    [self.window setRootViewController:viewController1 ];
}


- (void) showSecondSplashAds
{
    SplashVC *viewController1 = [[SplashVC alloc] initWithNibName:@"SplashVC" bundle:nil];
    
    
    [viewController1 setCallBackClose:^(){
        [self hideSecondSplashAds];
    }];
    [self.window setRootViewController:viewController1 ];
    
    // update display time
//    [UserSettingsHelper setIntersticeDisplayTime:[NSDate currentGMTZeroTime]];
    //reset counter
    
    [self performSelector: @selector(hideSecondSplashAds) withObject:nil afterDelay:3];
}


//MARK :- Shortcut vc

#pragma mark - ERROR
static BOOL isError503Showing;

-(void)showErrorViewController
{
    if (isError503Showing) {
        return;
    }
    isError503Showing = YES;
    
    ErrorViewController *errorVC=[[ErrorViewController alloc]initWithNibName:@"ErrorViewController" bundle:nil];
    errorVC.view.tag=10000;
    [errorVC.gotoHome addTarget:self action:@selector(hideErrorViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    errorVC.view.frame = self.window.frame;
    
    [[self window]addSubview:errorVC.view];
    
    UIView *v = errorVC.view;
    
    [self.window addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"V:|-0-[v]-0-|"
                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(v)]];
    
    [self.window addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|-0-[v]-0-|"
                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(v)]];
    
}
-(IBAction)hideErrorViewController:(id)sender
{
    isError503Showing = NO;
    
    UIView *view= [[self window] viewWithTag:10000];
    [view removeFromSuperview];
}

#pragma mark - SPLASH ADS
- (void) hideSecondSplashAds
{
    [self gotoApplication];
}
- (void) closeButtonPressed:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideSecondSplashAds) object:nil];
    [self performSelector: @selector(hideSecondSplashAds) withObject:nil afterDelay:0.2];
}

- (BOOL) shouldShowSecondSplash
{
    BOOL isShow = NO;

//    last schedule : display partners screen 1/3 times when open app

        NSString *lastLoad = [UserSettingsHelper getIntersticeDisplayTime];
        if (lastLoad) {
            NSDate *lastDate = [NSDate GMTZeroDateForString:lastLoad];
            int days = [NSDate numberOfDaysFromDate:lastDate ToDate:[NSDate date]];
            if (days >= 7) {
                isShow = YES;
            } else {
                isShow = NO;
            }
        } else {
            isShow = YES;
        }
    
    return isShow;
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    if ([COMMON isReachable]) {
        [[ServiceHelper sharedInstance] startSyncingPostingData];
    }
    
}

#pragma mark - REGISTER PUSH
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    //NSLog(@"token: %@", devToken);
    
//    NSString *deviceToken = [[[[devToken description]
//                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
//                              stringByReplacingOccurrencesOfString:@">" withString:@""]
//                             stringByReplacingOccurrencesOfString: @" " withString: @""];
//
//    ASLog(@"devicetoken_app = %@",deviceToken);
//
//    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"DEVICETOKEN"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [COMMON setPushAreEnabled: YES];
    /*
     * PUT /v1/user/identifier
     *
     * {
     *   "identifier": "e0366ffd02fdd942a960119e71fce654c2ccf5",
     *   "device": "ios",
     *   "name": "sony",
     * }
     
     */
    
    /*
     if (deviceToken.length >0) {
     NSDictionary *param = @{
     @"identifier":deviceToken,
     @"device" : @"ios",
     @"name": @"iphone"
     };
     WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
     serviceObj.onComplete = ^(NSDictionary*response, int errCode){
     //reset list
     };
     [serviceObj fnPUT_USER_IDENTIFIER_WITHPARAM:param];
     }
     */
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    ASLog(@"did fail to register: %@", error);
    
    [COMMON setPushAreEnabled: NO];
}

- (void)application: (UIApplication *) application didReceiveRemoteNotification: (NSDictionary *) userInfo
{
    //    ASLog(@"did receive push: %@", userInfo);
    [self ProcessingNotification_REMOTE:userInfo withApp:application ];
}


/******************************************************************************************************/
//new > iOS 10
//ggtt
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
    // Update the app interface directly.
    //Get remote notification
    
    // Play a Alert.
    completionHandler(UNNotificationPresentationOptionSound + UNNotificationPresentationOptionAlert);
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler {
    //auto record...
    
    if ([response.actionIdentifier isEqualToString:UNNotificationDismissActionIdentifier]) {
        // The user dismissed the notification without taking action.
    }
    else if ([response.actionIdentifier isEqualToString:UNNotificationDefaultActionIdentifier]) {
        // The user launched the app.
        [self processRemoteNotificationWithUserInfo: response.notification.request.content.userInfo];
    }
    
    // Else handle any custom actions. . .
}

-(void) ProcessingNotification_REMOTE :(NSDictionary*) userInfoTmp withApp:(UIApplication *)application
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:userInfoTmp];
    
    if (application.applicationState != UIApplicationStateActive)
    {
        
        //Wakeup app
        [self processRemoteNotificationWithUserInfo: userInfo];
        return;
    }
    else
    {
        NSString *strAlert = [userInfo[@"aps"][@"alert"] emo_emojiString];
        [AZNotification showNotificationWithTitle:strAlert controller:self.window.rootViewController notificationType:AZNotificationTypeMessage];
        
        //make crash!!!!
        [self cacheLocalNotify:userInfo];
    }
    
}

-(void) cacheLocalNotify :(NSDictionary*)userInfo
{
    NSMutableDictionary*mutDic = [userInfo mutableCopy];
    
    //continue
    //Local notification
    if ([userInfo[@"notification_id"] isKindOfClass: [NSNull class]]) {
        [mutDic setValue:@"" forKey:@"notification_id"];
    }
    
    if ([userInfo[@"object_id"] isKindOfClass: [NSNull class]]) {
        [mutDic setValue:@"" forKey:@"object_id"];
    }
    
    
    NSString *strAlert =mutDic[@"aps"][@"alert"];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.userInfo = mutDic;
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    localNotification.alertBody = strAlert;
    
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:2];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void) processRemoteNotificationWithUserInfo: (NSDictionary *) userInfo
{
    if ([COMMON isLoggedIn])
    {
        [self handleRemoteNotification: nil userInfo: userInfo];
    }
}

-(void) handleRemoteNotification:(UIApplication *)application userInfo:(NSDictionary *)userInfo {
    /*
     {
     aps =     {
     alert = "Dua Dua : 138";
     badge = 1;
     sound = default;
     };
     element = message;
     "notification_id" = "<null>";
     "object_id" = 93;
     projectID = 541284815128;
     type = "chat.new_message";
     }
     */
    //    ASLog(@"userinfo: %@", userInfo);

    /*
    { "type": "live.date.changed",
        "start_time": <the new start date time>,
        "end_time": <the new end date time>,
        "object_id": <id of the live>,
        
    }
*/
    
//    The start time has changed and the user opens the application after the time has passed or the problem / failure during recording.
    
    
    /*
    {
        aps =     {
            alert = "Votre naturalive a bien \U00e9t\U00e9 mis \U00e0 jour";
        };
        element = lounge;
        "end_time" = "2019-01-24T08:00:00+01:00";
        "gcm.message_id" = "0:1548113498328959%73c3708e73c3708e";
        "google.c.a.e" = 1;
        message = "Votre naturalive a bien \U00e9t\U00e9 mis \U00e0 jour";
        "notification_id" = 1143005;
        "object_id" = 2427;
        projectID = 891258442461;
        "start_time" = "2019-01-23T10:00:00+01:00";
        type = "live.date.changed";
    }
     
     set alarm to start the recording live when the live is launched
     -only 1 live is set alarm of recording, the first come will be set
     -if the time is changed by admin (before the live start), server will push to app and app will handle push to re-set the time of alarm

    */
//    handleRemoteNotification
    
    
    if ( [ userInfo[@"type"] isEqualToString:live_date_changed ]) {
        //show up Alert
        NSString *strTimeStart = userInfo[@"start_time"];
        AlertLivehunt_StartdateChange *vc = [[AlertLivehunt_StartdateChange alloc] initWithNibName:@"AlertLivehunt_StartdateChange" bundle:nil];
        UIViewController *vTopVC = (UIViewController *) self.window.rootViewController.presentedViewController;

        if ([NSDate compareLocalDateWithInput:strTimeStart] == NSOrderedDescending) {
            //live hunt is already inprogress, Admin trying to Edit the start date => Not allow, show Error
            vc.lbContent.text = @"UN PROBLÈME EST SURVENU, NOUS N'AVONS PAS PU ENREGISTRER LES DONNÉES DU NATURALIVE.";
        }else{
            //Live date doesn't happen, admin wants to change,..notify to user...update starttime for livehunt.
            vc.lbContent.text = @"VOTRE NATURALIVE A BIEN ÉTÉ MIS À JOUR";
            
            //start
            //object_id
            //if the time is changed by admin (before the live start), server will push to app and app will handle push to re-set the time of alarm
            
            [self fnScheduleLivehunt: [userInfo[@"object_id"] stringValue] withDate:userInfo[@"start_time"] ];
            
        }
        //run app -> auto record in background mode. when the time had come
        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
        
        formSheet.presentedFormSheetSize = CGSizeMake(300, 240);
        formSheet.shadowRadius = 8.0;
        formSheet.shadowOpacity = 0.8;
        formSheet.cornerRadius = 8.0;
        formSheet.shouldCenterVertically = YES;
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard;
        formSheet.transitionStyle = MZFormSheetTransitionStyleFade;
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        [vTopVC mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        }];

    } else if ( [ userInfo[@"type"] isEqualToString:local_push ]) {
        //ggtt
        
        AlertStatisticVC *vc = [[AlertStatisticVC alloc] initWithNibName:@"AlertStatisticVC" bundle:nil];
        vc.strLivehunt = userInfo[@"livehuntID"];
        
        UIViewController *vTopVC = (UIViewController *) self.window.rootViewController.presentedViewController;
        
        //run app -> auto record in background mode. when the time had come
        MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
        
        formSheet.presentedFormSheetSize = CGSizeMake(300, 240);
        formSheet.shadowRadius = 8.0;
        formSheet.shadowOpacity = 0.8;
        formSheet.cornerRadius = 8.0;
        formSheet.shouldCenterVertically = YES;
        formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard;
        formSheet.transitionStyle = MZFormSheetTransitionStyleFade;
        formSheet.shouldDismissOnBackgroundViewTap = YES;
        [vTopVC mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        }];

    } else{
        if ([COMMON isLoggedIn]) {
            [self putNotification:userInfo];
            
            //special cases:
            //        lounge_publication_new
            //        group_publication_new
            
            [self gotoScreenWithTypeNotification:userInfo[@"type"] IdNoti:userInfo[@"object_id"] dicNotif:userInfo];
        }

    }
    
}
- (void)putNotification:(NSDictionary*)userInfo{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //reset list
    };
    NSString *strNotificationID = @"";
    
    if ([userInfo[@"notification_id"] isKindOfClass: [NSString class] ]) {
        strNotificationID =userInfo[@"notification_id"];
        [serviceObj putUserReadNotificationAction:strNotificationID];
        
    }else if ([userInfo[@"notification_id"] isKindOfClass: [NSNumber class] ]) {
        
        strNotificationID = [userInfo[@"notification_id"]  stringValue];
        [serviceObj putUserReadNotificationAction:strNotificationID];
    }
}

#pragma mark -get count notification

-(void) showErrMsg :(NSString*)strMsg
{
    [AZNotification showNotificationWithTitle:strMsg controller:self.window.rootViewController notificationType:AZNotificationTypeError];
    
}
-(void) showPostMsgSuccess :(NSString*)strMsg
{
    [AZNotification showNotificationWithTitle:strMsg controller:self.window.rootViewController notificationType:AZNotificationTypeMessage];
    
}

-(void) refreshListWaitingSend{
    //refresh sending message.
    if ([COMMON isLoggedIn]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ServiceHelper sharedInstance] resetStatusPostingMessage];
            [[ServiceHelper sharedInstance] resetPostingChatMessage];
        });

    }

}

//application switchs back from background
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self refreshListWaitingSend];
    
    [socket connect];
    application.applicationIconBadgeNumber = 0;
    
    [[MapDataDownloader sharedInstance] doUpdateTableDBVersion];
}

//starts when application switchs into backghround
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [socket disconnect];
    [self fnCreateBackgroundTask];
}

#pragma mark - FB
//pdf PNG from EMAIL.
///Documents/Inbox/testPDF.pdf
///Documents/Inbox/logo.png
//Show publication with item...


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    NSLog(@"%@",[url scheme]);
    
    if ([[url scheme] isEqualToString:fbID]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    else if([[url scheme] isEqualToString:schemeName])
    {
        //goto hunt
        if ([COMMON isLoggedIn]) {
            [self doLogin];
            //goto hunt
            [self enterChasesWallFromWeb];
            
        }else{
            //Display Login page
            [self gotoLogin];
        }
        return YES;
    }
    else
    {
        [self handleDocumentOpenURL:url];
        return YES;
    }
}

-(void)handleDocumentOpenURL:(NSURL*)url
{
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray *imageExtensions = @[@"png", @"jpg",@"jpeg"];
        NSString *extension = [url pathExtension];
        if ([imageExtensions containsObject:extension]) {
            NSString *filename = [[url path] lastPathComponent];
            NSString *filePath = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:filename];
            [FileHelper duplicateFileFromPath:[url path] toPath:filePath];
            // A list of extensions to check against
            //delete inbox if finish uploading image.
            //User can save from email -> camera roll...then import to Profile documate later
            
            NSString *contentImage = [NSString stringWithFormat:@"%@",@"image"];
            [self showPublicationViewWithData:@{@"url": filename,@"type":contentImage}];
        }
        else // if ([extension isEqualToString:@"pdf"])
        {
            //only save PDF. LOCAL ...Hmm doc/xls/psd/pdf
            
            NSString *filename = [[url path] lastPathComponent];
            NSString *filePath = [[FileHelper PDFLocal] stringByAppendingPathComponent:filename];
            [FileHelper duplicateFileFromPath:[url path] toPath:filePath];
        }
    }
    //remove all sanbox
    [self performSelector:@selector(removeAllSanbox) withObject:nil afterDelay:60];
}

-(void)removeAllSanbox
{
    NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString  *documentsDirectory = [paths objectAtIndex:0];
    
    NSFileManager *fileMgr = [[NSFileManager alloc] init];
    NSError *error = nil;
    NSArray *directoryContents = [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error];
    if (error == nil) {
        for (NSString *path in directoryContents) {
            NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:path];
            NSArray *subdirectory = [fileMgr contentsOfDirectoryAtPath:fullPath error:&error];
            
            for (NSString *subPath in subdirectory) {
                NSString *subFullPath = [fullPath stringByAppendingPathComponent:subPath];
                BOOL removeSuccess = [fileMgr removeItemAtPath:subFullPath error:&error];
                if (!removeSuccess) {
                    // Error handling
                }
            }
        }
    } else {
        // Error handling
    }
}

-(void)showLiveHunt
{    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
            MapGlobalVC *subVC = [sb instantiateViewControllerWithIdentifier:@"MapGlobalVC"];
            subVC.mParent = viewController1;
            subVC.isSubVC = YES;
            subVC.isLiveHunt = YES;
            subVC.expectTarget = ISLOUNGE;

    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    [(MainNavigationController *)self.navigationController pushViewController:subVC animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

-(void)showAgenda
{
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    ChassesMurVC *vc = [[ChassesMurVC alloc] initWithNibName:@"ChassesMurVC" bundle:nil];
    vc.mParent = viewController1;
    vc.expectTarget = ISLOUNGE;

    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

-(void)showPublicationViewWithData:(NSDictionary*)data
{
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    SignalerTerminez *vc = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    vc.dicShareExtention = data;
    vc.mParent = viewController1;
    vc.expectTarget =ISMUR;
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

-(void)enterChasesWallFromWeb
{
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    GroupEnterMurVC *vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    vc.expectTarget =ISLOUNGE;
    vc.mParent = viewController1;
    
    //    [[GroupEnterOBJ sharedInstance] resetParams];
    //    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

-(void) fnScheduleWithListLivehunt: (NSArray*) arrData{
    for (NSDictionary*dic in arrData) {
        //sometime datebegin, sometime starttime
        NSString *strStartDate = nil;
        if (dic[@"start_time"] == nil) {
            if ([dic[@"meeting"]  isKindOfClass: [NSDictionary class]]) {
                
                strStartDate = dic[@"meeting"][@"date_begin"];
                
            }
        }
        [self fnScheduleLivehunt: [dic[@"id"] stringValue] withDate:strStartDate ];
    }
}


//5s request...check livehunt list...
//Add to local notification schedule...

//startdate - now = remaining time to trigger.

-(void) fnScheduleLivehunt: (NSString*)livehuntID withDate:(NSString*) strStartDate
{
    //ggtt
    
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = @"You have a livehunt comming!";
    content.body = @"";
    content.userInfo = @{@"type": local_push,
                         @"livehuntID": livehuntID
                         };
    content.sound = [UNNotificationSound defaultSound];

    //Note: Calling addNotificationRequest with the same identifier string will replace the existing notification. If you want to schedule multiple requests use a different identifier each time.
    //@manh convert startdate string to nsdate, component...
    //startDate
    
    //if startdate changed > current date (future) -> OK to schedule trigger.
    //but if start < current date, no need to trigger, reject...because Admin can not change date after
    //the date had in the past.
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    [inputFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [inputFormatter setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    
    NSDate *startDate = [inputFormatter dateFromString:strStartDate];

    NSTimeInterval mIntervalDate = [startDate timeIntervalSinceReferenceDate] - [[NSDate date] timeIntervalSinceReferenceDate];
    if (mIntervalDate > 0) {
        UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:mIntervalDate repeats:NO];
        
        //identify include Livehunt ID -> important !!!
        NSString *identifier = livehuntID;
        
        UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                              content:content trigger:trigger];
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        //add to schedule...
        [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
            if (error != nil) {
                NSLog(@"Something went wrong: %@",error);
            }
        }];

    }else{
        //startdate  < now -> invalid
    }
    
}
// //
#pragma mark
-(void) queueFMDatabase
{
    //if not exist table....
    //create
    //        INSERT INTO `tb_shape`(`c_id`,`c_owner_id`,`c_type`,`c_title`,`c_description`,`c_sharing`,`c_groups`,`c_hunts`,`c_friend`,`c_member`,`c_data`,`c_updated`,`c_nw_lat`,`c_nw_lng`,`c_se_lat`,`c_se_lng`,`c_user_id`)
    
    //TABLE SHAPE
    
    
    
    //c_allow_add using to tick on group Edit...show shareview in creating publication
    //c_allow_show using to tick on group Edit
    
    NSArray *arrSqlites = @[@"CREATE TABLE IF NOT EXISTS  `tb_shape` (`c_id`    INTEGER,\
                            `c_owner_id`    INTEGER,\
                            `c_type`    TEXT,\
                            `c_title`    TEXT,\
                            `c_description`    TEXT,\
                            `c_sharing`    INTEGER,\
                            `c_groups`    TEXT,\
                            `c_hunts`    TEXT,\
                            `c_friend`    INTEGER,\
                            `c_member`    INTEGER,\
                            `c_data`   TEXT,\
                            `c_updated`    INTEGER,\
                            `c_ne_lat`    REAL,\
                            `c_ne_lng`    REAL,\
                            `c_sw_lat`    REAL,\
                            `c_sw_lng`    REAL,\
                            `c_user_id`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id));",

                            
                             //Tracking table
                             @"CREATE TABLE IF NOT EXISTS   `tb_tracking` (\
                             `c_id`    INTEGER,\
                             `c_agenda_id`    TEXT,\
                             `c_user_id`    TEXT,\
                             `c_latest_altitude`    REAL,\
                             `c_negative_altitude`    REAL,\
                             `c_positive_altitude`    REAL,\
                             PRIMARY KEY(c_id,c_user_id)\
                             );",
                                         

                            //RecordingTable
                            @"CREATE TABLE IF NOT EXISTS   `tb_recording` (\
                            `c_id`    INTEGER,\
                            `c_agenda_id`    TEXT,\
                            `c_user_id`    TEXT,\
                            `c_start_time`    TEXT,\
                            `c_end_time`    TEXT,\
                            `c_distance`    REAL,\
                            `c_average`    REAL,\
                            `c_status`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            
                            //create table tempt Livehunt drawing point user location
                            @"CREATE TABLE IF NOT EXISTS  `tb_location_points` (`c_id`    INTEGER,\
                            `c_livehunt_id`    INTEGER,\
                            `c_lat`    REAL,\
                            `c_lon`    REAL,\
                            PRIMARY KEY(c_id)  );",
                            
                            //table group
                            @"CREATE TABLE IF NOT EXISTS   `tb_group` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_description`    TEXT,\
                            `c_access`    INTEGER,\
                            `c_admin`    INTEGER,\
                            `c_nb_member`    INTEGER,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            `c_allow_add`    INTEGER,\
                            `c_allow_show`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE  IF NOT EXISTS  `tb_hunt` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_description`    TEXT,\
                            `c_access`    INTEGER,\
                            `c_admin`    INTEGER,\
                            `c_nb_participant`    INTEGER,\
                            `c_start_date`    INTEGER,\
                            `c_end_date`    INTEGER,\
                            `c_meeting_address`    TEXT,\
                            `c_meeting_lat`    REAL,\
                            `c_meeting_lon`    REAL,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE  IF NOT EXISTS  `tb_agenda` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_description`    TEXT,\
                            `c_access`    INTEGER,\
                            `c_admin`    INTEGER,\
                            `c_nb_participant`    INTEGER,\
                            `c_start_date`    INTEGER,\
                            `c_end_date`    INTEGER,\
                            `c_meeting_address`    TEXT,\
                            `c_meeting_lat`    REAL,\
                            `c_meeting_lon`    REAL,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            `c_allow_show`    INTEGER,\
                            `c_allow_add`    INTEGER,\
                            `c_allow_chat_show`    INTEGER,\
                            `c_allow_chat_add`    INTEGER,\
                            `c_photo`    TEXT,\
                            `c_subscribers`    TEXT,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            
                            @"CREATE TABLE  IF NOT EXISTS `tb_favorite` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_legend`    TEXT,\
                            `c_color`    INTEGER,\
                            `c_category`    INTEGER,\
                            `c_tree`    TEXT,\
                            `c_card`    INTEGER,\
                            `c_animal`    INTEGER,\
                            `c_specific`    INTEGER,\
                            `c_sharing`    INTEGER,\
                            `c_groups`    TEXT,\
                            `c_hunts`    TEXT,\
                            `c_attchments`    TEXT,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_favorite_address` (\
                            `c_id`    INTEGER,\
                            `c_title`    TEXT,\
                            `c_favorite`    TEXT,\
                            `c_lat`    REAL,\
                            `c_lon`    REAL,\
                            `c_user_id`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            //table tb_db_version
                            @"CREATE TABLE IF NOT EXISTS  `tb_db_version` (`version`    INTEGER);",
                            //c_address
                            //c_cp + c_city
                            /*
                             
                             "INSERT INTO `tb_distributor`(`c_id`,`c_name`,`c_address`,`c_cp,`c_city`,`c_tel`,`c_email`,`c_lat`,`c_lon`,`c_brands`,`c_updated`,`c_logo`) VALUES ('641','ACL ARMURERIE','RUE RABELAIS LA MELIGRETTE','37220','CROUZILLES','02 47 58 30 60','acl-loches@orange.fr','47.1247564','0.4531836','[{\"name\":\"Browning\",\"partner\":1,\"logo\":\"\\/uploads\\/brands\\/images\\/resize\\/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg\"}]','1433238042','');",
                             
                             */
                            @"CREATE TABLE IF NOT EXISTS  `tb_distributor` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_address`    TEXT,\
                            `c_cp`    TEXT,\
                            `c_city`    TEXT,\
                            `c_tel`    TEXT,\
                            `c_email`    TEXT,\
                            `c_lat`    REAL,\
                            `c_lon`    REAL,\
                            `c_brands`    TEXT,\
                            `c_updated`    INTEGER,\
                            `c_logo`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_dog_breed` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_dog_type` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_weapon_brand` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_weapon_calibre` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );"
                            ];
    //table distributor
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        for (NSString*strQuerry in arrSqlites) {
            [db  executeUpdate:strQuerry];
        }
        
        
        /*ADD fields to group*/
        

        
        if (![db  columnExists:@"c_allow_chat_add" inTableWithName:@"tb_group"]) {
            NSArray*  arrQuery00 = @[@"ALTER TABLE tb_group ADD COLUMN c_allow_chat_add INTEGER",
                                    @"ALTER TABLE tb_group ADD COLUMN c_allow_chat_show INTEGER" ];
            
            for (NSString*strQuerry in arrQuery00) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        
        
        
        /*ADD 1 columns  c_default */
        
        if (![db  columnExists:@"c_default" inTableWithName:@"tb_favorite"]) {
            NSArray*  arrQuery2 = @[@"ALTER TABLE tb_favorite ADD COLUMN c_default INTEGER"];
            
            for (NSString*strQuerry in arrQuery2) {
                
                [db  executeUpdate:strQuerry];
            }
            
        }
        
        /*ADD 2 columns  c_lat_center + c_lng_center */
        if (![db  columnExists:@"c_lat_center" inTableWithName:@"tb_shape"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_shape ADD COLUMN c_lat_center REAL",
                                    @"ALTER TABLE tb_shape ADD COLUMN c_lng_center REAL" ];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        
        
        if (![db  columnExists:@"c_shareusers" inTableWithName:@"tb_shape"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_shape ADD COLUMN c_shareusers TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        
        if (![db  columnExists:@"c_shareusers" inTableWithName:@"tb_carte"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_carte ADD COLUMN c_shareusers TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        //check add
        
        if (![db  columnExists:@"c_owner_name" inTableWithName:@"tb_carte"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_carte ADD COLUMN c_owner_name TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        
        if (![db  columnExists:@"c_text" inTableWithName:@"tb_carte"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_carte ADD COLUMN c_text TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        if (![db  columnExists:@"c_created" inTableWithName:@"tb_carte"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_carte ADD COLUMN c_created TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        if (![db  columnExists:@"c_search" inTableWithName:@"tb_carte"])
        {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_carte ADD COLUMN c_search TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        

        if (![db  columnExists:@"c_shareusers" inTableWithName:@"tb_favorite"]) {
            NSArray*  arrQuery3 = @[@"ALTER TABLE tb_favorite ADD COLUMN c_shareusers TEXT"];
            
            for (NSString*strQuerry in arrQuery3) {
                
                [db  executeUpdate:strQuerry];
            }
        }
        
    }];
    
}


@end
