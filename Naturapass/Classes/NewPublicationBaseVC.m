//
//  NewPublicationBaseVC.m
//  Naturapass
//
//  Created by Minh Tran Trong Nhat on 9/27/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "NewPublicationBaseVC.h"

#import "NotificationVC.h"
#import "SubNavigationPRE_ANNULER.h"
#import "AlertVC.h"
#import "PublicationVC.h"
#import "ChassesCreate_Step1.h"
#import "MapGlobalVC.h"

@interface NewPublicationBaseVC ()
@end

@implementation NewPublicationBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    // Do any additional setup after loading the view.
    [self addMainNav:@"MainNavSignaler"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    //Change background color
    switch (self.expectTarget) {
        case ISLIVE:
        {
            [subview.myTitle setText:@"SIGNALER QUOI ?"];
        }
            break;
            
        default:
        {
            [subview.myTitle setText:@"POSTER QUOI ?"];
            //Change background color
            subview.imgBackground.image = [UIImage imageNamed:@"bg_nav_mur_v31"];
            subview.imgClose.image = [[UIImage imageNamed:@"icon_chasses_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            subview.imgClose.tintColor = [UIColor whiteColor];        }
            break;
    }
    self.vContainer.backgroundColor = self.colorBackGround;
    subview.backgroundColor = self.colorNavigation;
    [self addSubNav:nil];
    */
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: self.colorNavigation];

}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 1://HOME
        {//go to Live hunt screen
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[MapGlobalVC class]] ) {
                    [self.navigationController popToViewController:controller animated:TRUE];
                    
                    return;
                }
            }
            
            [self.navigationController popToRootViewControllerAnimated:YES];

        }
            break;
            
        case 0://Search
        {
            [self gotoback];
        }
            break;
            
        default:
            break;
    }
    
}
#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
        case 1://ANNULER
        {
            
            //ggtt check lai...
            
            
            NSString *str=@"";
            Class Cclass= nil;
            if (self.expectTarget == ISLOUNGE)
            {
                str = str(strAAgenda);
                Cclass= [ChassesCreate_Step1 class];
            }
            else
            {
                str =str(strPublication);
                Cclass= [PublicationVC class];
            }
            NSString *strmessage= @"";
            if ([PublicationOBJ sharedInstance].isEditer) {
                strmessage = [NSString stringWithFormat:str(strEtesVousSurDeVouloiranuilervotresaisie)];
            }
            else
            {
                strmessage = [NSString stringWithFormat:str(strMessage10),str];
            }
            AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@",str(strAAnnuler),str] message:strmessage cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
            
            [vc doBlock:^(NSInteger index, NSString *str) {
                if (index==0) {
                    // NON
                }
                else if(index==1)
                {
                    //OUI
                    NSArray * controllerArray = [[self navigationController] viewControllers];
                    
                    for (int i=0; i < controllerArray.count; i++) {
                        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                            
                            [self.navigationController popToViewController:self.mParent animated:YES];
                            
                            return;
                        }
                    }
                    [self doRemoveObservation];
                    
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            [vc showAlert];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)onNext:(id)sender {
}

@end
