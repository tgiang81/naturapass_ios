//
//  LegendAnnotation.m
//  Naturapass
//
//  Created by Giang on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "LegendAnnotation.h"

@implementation LegendAnnotation

- (id)initWithDic:(NSDictionary *)dic {
    self = [super init];
    if (self) {
        self.myDictionary = dic;
    }
    return self;
}

- (CLLocationCoordinate2D) coordinate
{
    //Mur locaation or Map location data???
    
    if ( [[self myDictionary][@"geolocation"] isKindOfClass: [NSDictionary class]]) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[self myDictionary][@"geolocation"][@"latitude"] doubleValue],
                                                                       [[self myDictionary][@"geolocation"][@"longitude"] doubleValue]);
        return coordinate;
        
    }
    
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[self myDictionary][@"c_lat"] doubleValue],
                                                                   [[self myDictionary][@"c_lon"] doubleValue]);
    return coordinate;
}

@end
