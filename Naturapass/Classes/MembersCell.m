//
//  CellKind2.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MembersCell.h"
#import "Config.h"
#import "CommonHelper.h"
#import "Define.h"
@implementation MembersCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [[CommonHelper sharedInstance] setRoundedView:self.imageIcon toDiameter:self.imageIcon.frame.size.width/2];

    
    [self.label1 setTextColor:[UIColor blackColor]];
    [self.label1 setFont:FONT_HELVETICANEUE(15)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
//    self.label1.preferredMaxLayoutWidth = CGRectGetWidth(self.label1.frame);
//    self.label2.preferredMaxLayoutWidth = CGRectGetWidth(self.label2.frame);

}
-(void) createMenuList:(NSArray*)arr
{
    
    
    
    if (self.menuPopover) {
        [self.menuPopover dismissMenuPopover];
    }
    
    UIView *tmpV = nil;
    
    if (arr.count==1) {
        tmpV = [self viewWithTag:POP_MENU_VIEW_TAG1];
    }else{
        tmpV = [self viewWithTag:POP_MENU_VIEW_TAG2];
    }
    
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:tmpV.frame menuItems:nil color:tmpV.backgroundColor];
    
    self.menuPopover.layer.masksToBounds = YES;
    self.menuPopover.layer.cornerRadius = 4;
    
    self.menuPopover.menuPopoverDelegate = self;
    self.menuPopover.menuItems = arr;
}

-(void) show:(UIView*)view
{
    [self.menuPopover showInView:view];// Cell
}
-(void) show:(UIView*)view offset:(CGRect)offset
{
    [self.menuPopover showInView:view offset:offset];// Cell

}
- (void)prepareForReuse {
    
    [self.menuPopover dismissMenuPopover];
    [super prepareForReuse];
}

#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    //    NSString *title = [NSString stringWithFormat:@"%@ selected.",[self.menuItems objectAtIndex:selectedIndex]];
    //
    //    NSLog(@"%@",title);
    if (_CallBackGroup) {
        _CallBackGroup(selectedIndex);
    }
}

@end
