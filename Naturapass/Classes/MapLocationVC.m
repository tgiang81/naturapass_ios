//
//  MapLocationVC.m
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MapLocationVC.h"

@implementation MapLocationVC

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_TINY_BAR_COLOR);

                //SUB
                [self addSubNav:nil];
            }else{
                
                //normal
                
                
                [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
                //add sub navigation
                [self addMainNav:@"MainNavMUR"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strMUR)];
                
                //Change background color
                subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
                
                [self addSubNav:@"SubNavigationMUR"];
                [self.searchAddress setTintColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                
                [vTmpMap.btnAdd setImage:  [UIImage imageNamed:@"mur_ic_add_publication"] forState:UIControlStateNormal];
                
            }
        }
            break;
        case ISGROUP:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(GROUP_TINY_BAR_COLOR);
                
                //SUB
                [self addSubNav:nil];
            }else{
                //No need filter...
                //            btnCarteFiltreAction.hidden = YES;
                
                [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
                //add sub navigation
                [self addMainNav:@"MainNavMUR"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strGROUPES)];
                
                //Change background color
                subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
                [self.searchAddress setTintColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
                
                [self addSubNav:@"SubNavigationGROUPENTER"];
                
                if (self.needRemoveSubItem) {
                    [self removeItem:4];
                }
                
                [vTmpMap.btnAdd setImage:  [UIImage imageNamed:@"ic_group_floating_btn"] forState:UIControlStateNormal];
                
            }
            
        }
            break;
            
        case ISLOUNGE:
        {
            
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
                
                //add sub navigation
                [self addMainNav:@"MainNavDetailMap"];
                MainNavigationBaseView *subview =  [self getSubMainView];
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
                
                //SUB
                [self addSubNav:nil];
            }else{
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
                //add sub navigation
                [self addMainNav:@"MainNavMUR"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strCHANTIERS)];
                
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.searchAddress setTintColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                
                if (self.isSpecial == YES)
                {
                    [self addSubNav:@"SubNavigationCHASSES"];
                    
                    
                }else{
                    [self addSubNav:@"SubNavigationGROUPENTER"];
                    [self checkAllow];
                    if (!allow_show_chat) {
                        [self removeItem:3];
                    }
                    if (self.needRemoveSubItem) {
                        [self removeItem:4];
                    }
                    
                }
                
                [vTmpMap.btnAdd setImage:  [UIImage imageNamed:@"ic_add_new_chasse"] forState:UIControlStateNormal];
            }
            
            
        }
            break;
        case ISCARTE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCARTE)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.searchAddress setTintColor:UIColorFromRGB(CARTE_MAIN_BAR_COLOR)];
            
            [self addSubNav:nil];
            [vTmpMap.btnAdd setImage:  [UIImage imageNamed:@"btn_add_on_map"] forState:UIControlStateNormal];
        }
            break;
            
        case ISPARAMTRES:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strPARAMETRES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationPARAMETER"];
            UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
            [btn1 addTarget:self action:@selector(gotoback) forControlEvents:UIControlEventTouchUpInside];
            
        }
            break;
        case ISMUR_FAV:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self addSubNav:@"SubNavigation_PRE_General"];
            
            UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
            [btn1 addTarget:self action:@selector(gotoback) forControlEvents:UIControlEventTouchUpInside];
            
        }
            break;
            //go from chasse invitation.
        case ISDEFAULT:
        {
        }
            break;
        default:
            
            break;
    }
    
    isUsingLocation = NO;
    //default maptype
//    vTmpMap.mapView_.mapType = kGMSTypeSatellite;
//    iMapType = 11;
    iMapType = (int) [[NSUserDefaults standardUserDefaults] integerForKey:@"defaultMapType" ];
    [self doMapType:iMapType]; //

    //show button map type, change constraint
    vTmpMap.btnTypeMap.hidden = NO;
    vTmpMap.constraintLeftButtonLocation.constant = [UIScreen mainScreen].bounds.size.width - 105;
    

    
    //Map to view publication full <picto...legend>
    
    if (self.simpleDic[@"id"] != nil && [self.simpleDic[@"geolocation"] isKindOfClass: [NSDictionary class]]) {
        
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [self.simpleDic[@"geolocation"][@"latitude"] doubleValue];
        simpleCoord.longitude= [self.simpleDic[@"geolocation"][@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        
        GMSMarker *marker = [GMSMarker markerWithPosition:simpleCoord];
        
        marker.userData = self.simpleDic;
        
        MDMarkersCustom *tmpImage= [[MDMarkersCustom alloc] initWithDictionary:self.simpleDic];
        
        [tmpImage setCallBackMarkers:^(UIImage *image,UIImage *image_nonLegend, float percent_arrow)
         {
             marker.groundAnchor = CGPointMake(percent_arrow, 1.0);
             marker.icon = [self resizeImage:image withLevelZoom:iZoomDefaultLevel withResize:NO withC_Marker:MARKER_PUBLICATION];
             marker.map = vTmpMap.mapView_;
         }];
        [tmpImage doSetCalloutView];
        
        return;
    }
    
    //From photo or has location data...
    if ( [[self.simpleDic objectForKey:@"latitude"] doubleValue] != 0 && [[self.simpleDic objectForKey:@"longitude"] doubleValue]!=0 )
    {
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [[self.simpleDic objectForKey:@"latitude"] doubleValue];
        simpleCoord.longitude= [[self.simpleDic objectForKey:@"longitude"] doubleValue];
        
        vTmpMap.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        
        if (self.expectTarget == ISGROUP || self.expectTarget == ISLOUNGE || self.expectTarget == ISMUR_FAV || self.expectTarget == ISPARAMTRES)
        {
            GMSMarker *marker = [GMSMarker markerWithPosition:simpleCoord];
            if (self.simpleDic[@"name"]) {
                marker.title = self.simpleDic[@"name"];
            }else{
                marker.title = @"";
            }
            
            marker.icon = [UIImage imageNamed:@"red_pin"];
            marker.map = vTmpMap.mapView_;
            [vTmpMap.mapView_ setSelectedMarker:marker];
        }
        
    }else{
        if([self locationCheckStatusDenied])
        {
            [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
            return;
        }
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [vTmpMap.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];

    }
    
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSDictionary *dic = nil;
    vTmpMap.btnPrint.hidden = YES;
    
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            
        }
            break;
        case ISGROUP:
        {
            
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            
        }
            break;
        case ISLOUNGE:
        {    UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }
        }
            break;
        case ISCARTE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR) ];
        }
            break;
        case ISPARAMTRES:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(PARAM_TINY_BAR_COLOR) ];
        }
            break;
        case ISMUR_FAV:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
        }
            break;
            
        default:
            break;
    }
    
    if (self.isSpecial) {
        [self setThemeNavSub:YES withDicOption:dic];
    }
    else
    {
        [self setThemeNavSub:NO withDicOption:dic];
        //set focus button status
        if (self.isSubVC == NO) {
            UIButton *btn = [self returnButton];
            
            [btn setSelected:YES];
            [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
        }
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(ISSCREEN) getTypeOfRequestScreen
{
    return ISMUR;
}

-(void) updateRightToChat:(NSNotification*)notif
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //refresh param...
        [self checkAllow];
        
        //hid/display subview discustion
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myTitle setText:str(strCHANTIERS)];
        //Change background color
        subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        [self addSubNav:@"SubNavigationGROUPENTER"];
        
        if (!allow_show_chat)
        {
            [self removeItem:3];
        }
        if (self.needRemoveSubItem)
        {
            [self removeItem:4];
        }
        
        [self setThemeNavSub:NO withDicOption:@{@"admin":@0}];
        UIButton *btn = [self returnButton];
        [btn setSelected:YES];
        [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
        
    });
}

#pragma mark - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    [super onSubNavClick:btn];
    
    
    if (self.isSpecial) {
        //treat like parent
        switch (self.expectTarget) {
            case ISLOUNGE:
            {
                [self onSubNavClick_Invite_Chasse: (int)btn.tag - START_SUB_NAV_TAG];
            }
                break;
            default:
                break;
        }
        
    }else{
        switch (self.expectTarget) {
            case ISMUR:
            {
                [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
            }
                break;
            case ISGROUP:
            {
                [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
            }
                break;
            case ISLOUNGE:
            {
                [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
            }
                break;
            default:
                break;
        }
        
    }
}

-(void)appDidBecomeActiveNotif:(NSNotification*)notif
{

}

-(void)appWillResignActiveNotif:(NSNotification*)notif
{

}
#pragma mark - CLICK MEDIA ITEM
//MARK:- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=self.searchAddress.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.searchAddress.text=strsearchValues;
    
    [self handleSearchForSearchString:self.searchAddress.text];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    //    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
    [self processLoungesSearchingURL:searchText];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.shouldBeginEditing = NO;
    self.tableControl.hidden=YES;
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSString *strsearchValues=self.searchAddress.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.searchAddress.text=strsearchValues;
    [self handleSearchForSearchString:self.searchAddress.text];
    [theSearchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    [self clearSearchResults];
    self.tableControl.hidden=NO;
    [self.tableControl reloadData];
}

//MARK: UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""]) {
        
        [self clearSearchResults];
        
    } else {
        [[HNKGooglePlacesAutocompleteQuery sharedQuery] resetAPIKEY:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
        
        [self.searchQuery fetchPlacesForSearchQuery:searchString
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                                 }];
                                                 
                                             } else {
                                                 self.searchResults = places;
                                                 [self.tableControl reloadData];
                                             }
                                         }];
    }
}
#pragma mark Search Helpers

- (void)clearSearchResults
{
    self.searchResults = @[];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}
@end
