//
//  Filter_Cell_Type4.h
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDPickerTextField.h"
@interface Filter_Cell_DatePersonnaliser : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UILabel *lbFin;
@property (weak, nonatomic) IBOutlet UILabel *lbDebut;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleFin;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleDebut;

@end
