//
//  ChassesCreateV2.m
//  Naturapass


#import "PublicationNew_Signalement.h"
#import "Define.h"
#import "AlertVC.h"
#import "NetworkCheckSignal.h"
#import "Signaler_ChoixSpecNiv1.h"
#import "Signaler_ChoixSpecNiv4_elargi.h"
#import "Signaler_ListFavorite.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "Signaler_ChoixSpecNiv2.h"
#import "Signaler_ChoixSpecNiv4.h"
#import "Signaler_ChoixSpecNiv5.h"
#import "SignalerTerminez.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface AnimalCollection : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *imgAnimal;

@end

@implementation AnimalCollection

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

@end

@interface PublicationNew_Signalement () <MFMailComposeViewControllerDelegate>
{
    __weak IBOutlet UICollectionView *collectionView;
    __weak IBOutlet NSLayoutConstraint *constraintBottom;
    __weak IBOutlet UIButton *btnFavorite;
    id resultLeaf;
    __weak IBOutlet UIButton *btnSentinal;
}

@end

@implementation PublicationNew_Signalement


-(NSDictionary*) recursiveGetObj:(NSString*) strPath fromTree:(NSArray*)arrTree
{
    NSArray*arrKeysID = [strPath componentsSeparatedByString:@"/"];
    
    if (arrKeysID.count >= 1) {
        //cut the first key
        NSString*strPrevKey = arrKeysID[0];
        
        NSArray*arrChildren = nil;
        NSDictionary*dicContainer = nil;
        
        for (NSDictionary*dic in arrTree)
        {
            if ([  [dic[@"id"] stringValue] isEqualToString:strPrevKey])
            {
                //get children arr...save name
                ASLog(@"%@", dic[@"name"]);
                arrChildren = dic[@"children"];
                //save
                dicContainer = dic;
                break;
            }
        }
        
        if (arrKeysID.count == 1) {
            //leaf stop
            //get the child
            ASLog(@"last dic: %@", arrTree);
            if (arrChildren.count == 0) {
                // i am a card
                resultLeaf = dicContainer;
            }else{
                //i am a parent
                resultLeaf = dicContainer;
                
            }
        }else{
            //continue recursive
            //recursive the remain keys
            NSUInteger index = [strPath rangeOfString:@"/"].location;
            if ((index != NSNotFound) && (strPath.length > index + 1))
            {
                NSString*subStrPath = [strPath substringFromIndex:index + 1];
                [self recursiveGetObj:subStrPath fromTree:arrChildren];
            }
            
        }
    }
    
    return nil;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.vContainer.backgroundColor = self.colorAttachBackGround;
    self.imgFavorisBG.backgroundColor = self.colorNavigation;
    self.imgSentinelleBG.backgroundColor = self.colorNavigation;
    MainNavigationBaseView *subview =  [self getSubMainView];
    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
    
    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
    
//    vSentinental
    btnSentinal.enabled = NO;
    btnSentinal.enabled == TRUE? (self.imgSentinelleBG.alpha = 1) : (self.imgSentinelleBG.alpha = 0.5);

    WebServiceAPI *ws = [[WebServiceAPI alloc]init];
    
    [ws fnGET_CHECK_LINKTO_RECEIVER];
    
    ws.onComplete =^(NSDictionary *respone, int code)
    {
        if ([respone isKindOfClass: [NSDictionary class]]) {
            if (respone[@"has_group_linkto_receiver"]) {
                BOOL b = TRUE;//[respone[@"has_group_linkto_receiver"] boolValue];
                if (b) {
                    btnSentinal.enabled = YES;
                    btnSentinal.enabled == TRUE? (self.imgSentinelleBG.alpha = 1) : (self.imgSentinelleBG.alpha = 0.5);

                }
            }
        }
    };
    
    //reuse for publication making from MAP
    if (self.expectTarget == ISCARTE) {
        [subview.myDesc setText:@"Précisez la publication..."];
        self.vBottom.hidden = TRUE;
        self.vBottom1.hidden = FALSE;
        
//        self.imgInActiveBG.hidden = !self.isHideSentinelle;
//        self.isHideSentinelle == TRUE? (self.imgSentinelleBG.alpha = 0.5) : (self.imgSentinelleBG.alpha = 1);
        
        _arrData = [NSMutableArray new];

        //special for Carte
        [_arrData addObjectsFromArray:
         @[
           @{@"pathtree": kPath_POSTE, @"OBJECT": @"POSTE", @"image": @"ic_cate_poste"},
           @{@"pathtree": kPath_MIRADOR_AFF, @"OBJECT": @"MIRADOR AFF.", @"image": @"ic_cate_miradoraff"},
           @{@"pathtree": kPath_MIRADOR_BAT, @"OBJECT": @"MIRADOR BAT.", @"image": @"ic_cate_miradorbat"},
           @{@"pathtree": kPath_CABANE, @"OBJECT": @"CABANE", @"image": @"ic_cate_cabane"},
           @{@"pathtree": kPath_PIEGES, @"OBJECT": @"PIEGES", @"image": @"ic_cate_pieges"},
           @{@"pathtree": kPath_EQUIPEMENTS, @"OBJECT": @"EQUIPEMENTS", @"image": @"ic_cate_equipements"},
           @{@"pathtree": kPath_HABITATS, @"OBJECT": @"HABITATS", @"image": @"ic_cate_habitats"},
           @{@"pathtree": kPath_ANIMAUX, @"OBJECT": @"ANIMAUX", @"image": @"ic_cate_animaux"},
           @{@"TYPE":@(OTHER), @"OBJECT": @"AUTRE", @"image": @"ic_cate_autre"},
           ]
         ];
        
    }
    else
    {
        switch (self.iType) {
            case PUB1:
            {
                switch (self.expectTarget) {
                    case ISCARTE:
                    {
                        [subview.myDesc setText:@"Précisez la publication..."];
                        self.vBottom.hidden = TRUE;
                        self.vBottom1.hidden = FALSE;
//                        self.isHideSentinelle == TRUE? (self.imgSentinelleBG.alpha = 0.5) : (self.imgSentinelleBG.alpha = 1);
                    }
                        break;
                        
                    default:
                    {
                        [subview.myDesc setText:@"Précisez le signalement..."];
                        self.vBottom.hidden = FALSE;
                        self.vBottom1.hidden = TRUE;
                    }
                        break;
                }
                
                _arrData = [NSMutableArray new];
                [_arrData addObjectsFromArray:

                 //104/106
                 @[ @{ @"OBJECT": @"ANIMAL TUÉ", @"image": @"ic_animal_tue",@"name" : @"Animaux/Tué chasse",
                       @"SECOND SCREEN":@[
                               //350/144
                               @{@"nametree":pathANIMAL_TUE_SANGLIER, @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                               //350/145
                               @{@"nametree":pathANIMAL_TUE_CHEVREUIL, @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                               //350/148
                               @{@"nametree":pathANIMAL_TUE_RENARD, @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                               //350/146
                               @{@"nametree":pathANIMAL_TUE_CERF, @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                               //352/149
                               @{@"nametree":pathANIMAL_TUE_FAISAN, @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                               //350/147
                               @{@"nametree":pathANIMAL_TUE_LIEVRE, @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                               //352/363
                               @{@"nametree":pathANIMAL_TUE_COLVERT, @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                               //352/151
                               @{@"nametree":pathANIMAL_TUE_PERDRIX, @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                               //352/617
                               @{@"nametree":pathANIMAL_TUE_BECASSE, @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                               //352/143
                               @{@"nametree":pathANIMAL_TUE_PIGEON, @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                               //350/150
                               @{@"nametree":pathANIMAL_TUE_LAPIN, @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                               //352/364
                               @{@"nametree":pathANIMAL_TUE_CORBEAU, @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                               //352/365
                               @{@"nametree":pathANIMAL_TUE_CORNEILLE, @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},
                               //350/608
                               @{@"nametree":pathANIMAL_TUE_CHAMOIS, @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                               @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre",
                                 @"nametree":pathANIMAL_TUE_OTHERS
                                 }
                               
                               ]
                       //104/111
                       }, @{ @"OBJECT":@"ANIMAL VU", @"image": @"ic_animal_vu",@"name" : @"Animaux/Vu",
                             @"SECOND SCREEN":@[
                                     //319/135
                                     @{@"nametree":pathANIMAL_VU_SANGLIER, @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                     //319/136
                                     @{@"nametree":pathANIMAL_VU_CHEVREUIL, @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                     //319/139
                                     @{@"nametree":pathANIMAL_VU_RENARD, @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                     //319/137
                                     @{@"nametree":pathANIMAL_VU_CERF, @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                     //320//377
                                     @{@"nametree":pathANIMAL_VU_FAISAN, @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                                     //319/138
                                     @{@"nametree":pathANIMAL_VU_LIEVRE, @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                                     //320/321
                                     @{@"nametree":pathANIMAL_VU_COLVERT, @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                                     //320/141
                                     @{@"nametree":pathANIMAL_VU_PERDRIX, @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                                     //320/381
                                     @{@"nametree":pathANIMAL_VU_BECASSE, @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                     //320/173
                                     @{@"nametree":pathANIMAL_VU_PIGEON, @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                                     //319/860
                                     @{@"nametree":pathANIMAL_VU_LAPIN, @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                                     //320/861
                                     @{@"nametree":pathANIMAL_VU_CORBEAU, @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                                     //320/862
                                     @{@"nametree":pathANIMAL_VU_CORNEILLE, @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},
                                     //319/376
                                     @{@"nametree":pathANIMAL_VU_CHAMOIS, @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                     @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre",
                                       @"nametree":pathANIMAL_VU_OTHERS
                                       }
                                     
                                     ]
                             //104/105
                             }, @{ @"OBJECT":@"ANIMAL LOUPÉ", @"image": @"ic_animal_loupe",@"name" : @"Animaux/Tiré (loupé)",
                                  
                                   @"SECOND SCREEN":@[
                                           //372/124
                                           @{@"nametree":pathANIMAL_LOUPE_SANGLIER, @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                           //372/125
                                           @{@"nametree":pathANIMAL_LOUPE_CHEVREUIL, @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                           //372/279
                                           @{@"nametree":pathANIMAL_LOUPE_RENARD, @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                           //372/126
                                           @{@"nametree":pathANIMAL_LOUPE_CERF, @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                           //380/371
                                           @{@"nametree":pathANIMAL_LOUPE_FAISAN, @"OBJECT":@"FAISAN", @"image": @"live_animal_ic_faisan"},
                                           //372/127
                                           @{@"nametree":pathANIMAL_LOUPE_LIEVRE, @"OBJECT":@"LIEVRE", @"image": @"live_animal_ic_lievre"},
                                           //380/864
                                           @{@"nametree":pathANIMAL_LOUPE_COLVERT, @"OBJECT":@"COLVERT", @"image": @"live_animal_ic_canard"},
                                           //380/130
                                           @{@"nametree":pathANIMAL_LOUPE_PERDRIX, @"OBJECT":@"PERDRIX", @"image": @"live_animal_ic_perdrix"},
                                           //380/132
                                           @{@"nametree":pathANIMAL_LOUPE_BECASSE, @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                           //380/133
                                           @{@"nametree":pathANIMAL_LOUPE_PIGEON, @"OBJECT":@"PIGEON", @"image": @"live_animal_ic_pigeon"},
                                           //372/128
                                           @{@"nametree":pathANIMAL_LOUPE_LAPIN, @"OBJECT":@"LAPIN", @"image": @"live_animal_ic_lapin"},
                                           //380/865
                                           @{@"nametree":pathANIMAL_LOUPE_CORBEAU, @"OBJECT":@"CORBEAU", @"image": @"live_animal_ic_corbeaux"},
                                           //380/866
                                           @{@"nametree":pathANIMAL_LOUPE_CORNEILLE, @"OBJECT":@"CORNEILLE", @"image": @"live_animal_ic_corneille"},
                                           //372/863
                                           @{@"nametree":pathANIMAL_LOUPE_CHAMOIS, @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                           @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre",
                                             @"nametree":pathANIMAL_LOUPE_OTHERS
                                             }
                                           
                                           ]
                                   //104/107
                                   }, @{ @"OBJECT":@"ANIMAL BLESSÉ", @"image": @"ic_animal_blesse",@"name" : @"Animaux/Blessé",
                                         @"SECOND SCREEN":@[
                                                 //153
                                                 @{@"nametree":pathANIMAL_BLESSE_SANGLIER, @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                                 //155
                                                 @{@"nametree":pathANIMAL_BLESSE_CERF, @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                                 //154
                                                 @{@"nametree":pathANIMAL_BLESSE_CHEVREUIL, @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                                 //858
                                                 @{@"nametree":pathANIMAL_BLESSE_RENARD, @"OBJECT":@"RENARD", @"image": @"live_animal_ic_renard"},
                                                 
                                                 //mising icon
                                                 //859
                                                 @{@"nametree":pathANIMAL_BLESSE_CHIEN, @"OBJECT":@"CHIEN", @"image": @"live_animal_ic_chien"},
                                                 @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre",
                                                   @"nametree":pathANIMAL_BLESSE_OTHERS
                                                   
                                                   }
                                                 
                                                 ]
                                         //104/623
                                         }, @{ @"OBJECT":@"TRACES", @"image": @"ic_animal_traces",@"name" : @"Traces/Indices",
                                               
                                               @"SECOND SCREEN":@[
                                                       //628
                                                       @{@"nametree":pathANIMAL_TRACES_SANGLIER, @"OBJECT": @"SANGLIER", @"image": @"live_animal_ic_sanglier"},
                                                       //626
                                                       @{@"nametree":pathANIMAL_TRACES_CERF, @"OBJECT":@"CERF", @"image": @"live_animal_ic_cerf"},
                                                       //627
                                                       @{@"nametree":pathANIMAL_TRACES_CHEVREUIL, @"OBJECT":@"CHEVREUIL", @"image": @"live_animal_ic_chevreuil"},
                                                       //625
                                                       @{@"nametree":pathANIMAL_TRACES_CHAMOIS, @"OBJECT":@"CHAMOIS", @"image": @"live_animal_ic_chamois"},
                                                       //624
                                                       @{@"nametree":pathANIMAL_TRACES_BECASSE, @"OBJECT":@"BECASSE", @"image": @"live_animal_ic_becasse"},
                                                       @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre",
                                                         @"nametree":pathANIMAL_TRACES_OTHERS
                                                         }
                                                       
                                                       ]
                                               } ,

                    //618
                    @{@"TYPE":@(CARD), @"nametree":pathJE_SUIS_ICI, @"OBJECT":@"JE SUIS ICI", @"image": @"ic_cate_jesuisici"} ,
                    //867
                    @{@"TYPE":@(CARD), @"nametree":pathSOS, @"OBJECT":@"SOS", @"image": @"ic_cate_sos"} ,
                    @{@"TYPE":@(OTHER), @"OBJECT":@"AUTRE", @"image": @"ic_animal_autre"}
                    
                    
                    ] ] ;
                
                
            }
                
                break;
            case PUB2:
            {
                [subview.myDesc setText:@"Animaux..."];
                
                [self fnHideFavorite];
                
            }
                
                break;
                
            case PUB3:
            {
                [subview.myDesc setText:@"Animaux..."];
                
                [self fnHideFavorite];
            }
                
                break;
                
            default:
                break;
        }
        
    }
    
}

-(void) fnHideFavorite
{
    //bottom
    constraintBottom.constant = 0;
    self.vBottom.hidden = TRUE;
    self.vBottom1.hidden = TRUE;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //RESET param publication here
    [[PublicationOBJ sharedInstance]  resetParams];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)favoriteAction:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //seem offline....or no fav...
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
            NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
            
            
            NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite  WHERE ( c_user_id=%@ AND c_default=%@ ) ORDER BY c_id", sender_id, @"1"];
            FMResultSet *set_querry = [db  executeQuery:strQuerry];
            
            NSMutableArray*mutArr = [NSMutableArray new];
            
            while ([set_querry next])
            {
                [mutArr addObject:@{ @"name": [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ? [[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities] : @"",
                                     @"id": [set_querry stringForColumn:@"c_id"] ? [set_querry stringForColumn:@"c_id"] : @""
                                     
                                     }];
            }
            
            
            [PublicationOBJ sharedInstance].arrCacheFavorites = mutArr;
            Signaler_ListFavorite *viewController1 = [[Signaler_ListFavorite alloc] initWithNibName:@"Signaler_ListFavorite" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
        }];
    });
    
}
-(IBAction)sentinelleAction:(id)sender
{
//    self.isHideSentinelle = !self.isHideSentinelle;
//    self.isHideSentinelle == TRUE? (self.imgSentinelleBG.alpha = 0.5) : (self.imgSentinelleBG.alpha = 1);
    
    AlertVC *vc = [[AlertVC alloc] initConfirmSentinelleWithExpectTarget:self.expectTarget color:self.colorNavigation];
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        //valider
        if (index == 1) {
            //send email...         String mailto = "mailto:team@naturapass.com";
            // From within your active view controller
            if([MFMailComposeViewController canSendMail]) {
                MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
                mailCont.mailComposeDelegate = self;
                
                [mailCont setSubject:@"[Naturapass]"];
                [mailCont setToRecipients:[NSArray arrayWithObject:@"team@naturapass.com"]];
                [mailCont setMessageBody:@"" isHTML:NO];
                [self presentViewController:mailCont animated:YES completion:nil];
                
            }

            
        }
    }];
    [vc showAlert];
}


// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrData.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = _arrData[indexPath.row];
    
    //    @{@"pathtree": kPath_POSTE, @"OBJECT": @"POSTE", @"image": @"live_animal_ic_sanglier"},
    
    if (self.expectTarget == ISCARTE)
    {
        if ([dic[@"TYPE"] intValue] == OTHER)
        {
            //show full list
            Signaler_ChoixSpecNiv1 *viewController1 = [[Signaler_ChoixSpecNiv1 alloc] initWithNibName:@"Signaler_ChoixSpecNiv1" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
        }
        else
        {
            //fetch the tree
            NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
            NSArray *arrTree = treeDic[@"default"];
            [self recursiveGetObj:dic[@"pathtree"] fromTree:arrTree];
            
            ASLog(@">>> %@", resultLeaf);
            
            if ( ( (NSArray*)resultLeaf[@"children"]).count > 0 )
            {
                
                //a sub tree
                //has search option
                if ([resultLeaf[@"search"] intValue] ==  1)
                {
                    Signaler_ChoixSpecNiv4 *viewController1 = [[Signaler_ChoixSpecNiv4 alloc] initWithNibName:@"Signaler_ChoixSpecNiv4" bundle:nil];
                    viewController1.myDic = resultLeaf;
                    viewController1.myName = resultLeaf[@"name"];

                    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                    
                    
                }else{
                    Signaler_ChoixSpecNiv2 *viewController1 = [[Signaler_ChoixSpecNiv2 alloc] initWithNibName:@"Signaler_ChoixSpecNiv2" bundle:nil];
                    viewController1.myDic = resultLeaf;
                    viewController1.myName = resultLeaf[@"name"];
                    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                    
                }
                
                
            }else{
                ASLog(@"bad format");
                //no children, i am a leaf or a card,
                /*
                 {
                 children =     {
                 };
                 groups =     {
                 };
                 id = 177;
                 name = Cabane;
                 receivers =     {
                 };
                 search = 0;
                 }
                 */
                if ( resultLeaf[@"card"] ) {
                    Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
                    viewController1.myDic = resultLeaf;
                    viewController1.iSpecific =  0;
                    viewController1.myName = resultLeaf[@"name"];

                    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                    
                }else{
                    //leaf
                    SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                    
                }
            }
        }
    }else{
        switch ([dic[@"TYPE"] intValue]) {
                
            case OTHER:
            {
                //1 or 3
                
                switch (self.iType) {
                        
                    case PUB1:
                    {
                        //show full list
                        Signaler_ChoixSpecNiv1 *viewController1 = [[Signaler_ChoixSpecNiv1 alloc] initWithNibName:@"Signaler_ChoixSpecNiv1" bundle:nil];
                        
                        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        
                    }
                        break;
                        
                    case PUB2:
                    {
                        //fetch the tree
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        NSArray *arrTree = treeDic[@"default"];
                        [self recursiveGetObj:dic[@"nametree"] fromTree:arrTree];

                        
                        if ( ( (NSArray*)resultLeaf[@"children"]).count > 0 ) {
                            
                            //has search option
                            if ([resultLeaf[@"search"] intValue] ==  1)
                            {
                                Signaler_ChoixSpecNiv4 *viewController1 = [[Signaler_ChoixSpecNiv4 alloc] initWithNibName:@"Signaler_ChoixSpecNiv4" bundle:nil];
                                viewController1.myDic = resultLeaf;
                                viewController1.myName = [NSString stringWithFormat:@"%@",self.myName];
                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                                
                            }else{
                                Signaler_ChoixSpecNiv2 *viewController1 = [[Signaler_ChoixSpecNiv2 alloc] initWithNibName:@"Signaler_ChoixSpecNiv2" bundle:nil];
                                
                                viewController1.myDic = resultLeaf;
                                viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];

                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                            }
                        }
                        
                        
                    }
                        break;
                    default:
                        break;
                }
                
                
            }
                break;
            case CARD:
            {
                //fetch the tree
                NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                NSArray *arrTree = treeDic[@"default"];
                [self recursiveGetObj:dic[@"nametree"] fromTree:arrTree];

                if ( ( (NSArray*)resultLeaf[@"children"]).count > 0 ) {
                    
                    //a sub tree
                    //has search option
                    if ([resultLeaf[@"search"] intValue] ==  1)
                    {
                        Signaler_ChoixSpecNiv4 *viewController1 = [[Signaler_ChoixSpecNiv4 alloc] initWithNibName:@"Signaler_ChoixSpecNiv4" bundle:nil];
                        viewController1.myDic = resultLeaf;
                        if (self.myName != nil) {
                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];
                        }else{
                            viewController1.myName = [NSString stringWithFormat:@"%@", resultLeaf[@"name"]];

                        }

                        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        
                        
                    }else{
                        Signaler_ChoixSpecNiv2 *viewController1 = [[Signaler_ChoixSpecNiv2 alloc] initWithNibName:@"Signaler_ChoixSpecNiv2" bundle:nil];
                        viewController1.myDic = resultLeaf;
                        
                        if (self.myName != nil) {
                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];
                        }else{
                            viewController1.myName = [NSString stringWithFormat:@"%@", resultLeaf[@"name"]];
                        }
                        
                        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        
                    }
                    
                }else{
                    //a card
                    
                    if ( resultLeaf[@"card"] ) {
                        Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
                        viewController1.myDic = resultLeaf;
                        if (self.myName != nil) {
                            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];
                        }else{
                            viewController1.myName = [NSString stringWithFormat:@"%@", resultLeaf[@"name"]];
                        }

                        viewController1.iSpecific =  0;
                        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        
                    }else{
                        //leaf
                        SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                        [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        
                    }
                }
            }
                break;
                
            case NORMAL:
            {
                
                switch (self.iType) {
                    case PUB1:
                    {
                        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"WHY" bundle:[NSBundle mainBundle]];
                        PublicationNew_Signalement *viewController1 = [sb instantiateViewControllerWithIdentifier:@"PublicationNew_Signalement"];
                        viewController1.iType = PUB2;
                        viewController1.arrData = [dic[@"SECOND SCREEN"] mutableCopy];
                        viewController1.myName = dic[@"name"];
                        
                        if (self.expectTarget == ISLIVE) {
                            [self pushVC:viewController1 animate:YES expectTarget:ISLIVE iAmParent:NO];
                        } else {
                            [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                        }
                    }
                        break;
                        
                    case PUB2:
                    {
                        //depend on sub tree branch...
                        //read content from file:
                        //fetch the tree
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        NSArray *arrTree = treeDic[@"default"];
                        [self recursiveGetObj:dic[@"nametree"] fromTree:arrTree];

                        //loupe Signaler_ChoixSpecNiv4
                        //                    NSString *strName = dic[@"nametree"] ;
                        
                        if ( ( (NSArray*)resultLeaf[@"children"]).count > 0 ) {
                            
                            //a sub tree
                            //has search option
                            if ([resultLeaf[@"search"] intValue] ==  1)
                            {
                                Signaler_ChoixSpecNiv4 *viewController1 = [[Signaler_ChoixSpecNiv4 alloc] initWithNibName:@"Signaler_ChoixSpecNiv4" bundle:nil];
                                viewController1.myDic = resultLeaf;
                                
                                viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];

                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                                
                                
                            }else{
                                Signaler_ChoixSpecNiv2 *viewController1 = [[Signaler_ChoixSpecNiv2 alloc] initWithNibName:@"Signaler_ChoixSpecNiv2" bundle:nil];
                                viewController1.myDic = resultLeaf;
                                viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];
                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                                
                            }
                            
                        }else{
                            //a card
                            
                            if ( resultLeaf[@"card"] ) {
                                Signaler_ChoixSpecNiv5 *viewController1 = [[Signaler_ChoixSpecNiv5 alloc] initWithNibName:@"Signaler_ChoixSpecNiv5" bundle:nil];
                                viewController1.myDic = resultLeaf;
                                viewController1.iSpecific =  0;
                                
                                viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,resultLeaf[@"name"]];
                                
                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                                
                            }else{
                                //leaf
                                SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                                [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:NO];
                                
                            }
                        }
                    }
                        break;
                        
                    default:
                        break;
                }
            }
                break;
                
            default:
                break;
        }
        
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *identifier = @"AnimalCollection";
    
    //    NSDictionary *dic = _arrData[indexPath.row];
    
    AnimalCollection *cell = (AnimalCollection *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dic = _arrData[indexPath.row];
    cell.imgAnimal.image = [UIImage imageNamed:dic[@"image"]];
    cell.name.text = dic[@"OBJECT"];
    
    return cell;
}

//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3.05 , collectionView.frame.size.width/3.05);
}

@end
