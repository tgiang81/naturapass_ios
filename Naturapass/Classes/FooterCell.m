//
//  Card_Cell_Type10.m
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "FooterCell.h"
#import "Define.h"
@implementation FooterCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.viewBoder.layer setBorderColor: [UIColorFromRGB(DISTRIBUTION_TEXT) colorWithAlphaComponent:0.3].CGColor];
    [self.viewBoder.layer setBorderWidth: 1.0];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
