//
//  MesSalonCustomCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 3/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "TypeCellToutes.h"
#import "CommonHelper.h"
#import "MDStatusButton.h"
@implementation TypeCellToutes
- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    [self.Img1.layer setMasksToBounds:YES];
    self.Img1.layer.cornerRadius= 30;
    self.Img1.layer.borderWidth =0;


    [self.mainView.layer setMasksToBounds:YES];
    self.mainView.layer.cornerRadius= 5.0;
    self.mainView.layer.borderWidth =0.5;
    self.mainView.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    
    UIView *tmpV = [self viewWithTag:POP_MENU_VIEW_TAG2];
    tmpV.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    [self.imgBackGroundSetting setImage:[UIImage imageNamed:@"chasse_bg_setting"]];
    [self.imgSettingSelected setImage:[UIImage imageNamed:@"ic_admin_setting"]];
    [self.imgSettingNormal setImage:[UIImage imageNamed:@"ic_chasse_setting_active"]];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}
-(void)fnSettingCell:(int)type
{
    [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
    [self.label3 setTextColor: color];
    [self.label9 setTextColor:color];
    
//    [self.button1 setBackgroundColor:color];
//    [self.button3 setBackgroundColor:color];
}
@end
