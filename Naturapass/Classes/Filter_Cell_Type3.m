//
//  Filter_Cell_Type3.m
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Filter_Cell_Type3.h"

@implementation Filter_Cell_Type3

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.txtInput.layer setMasksToBounds:YES];
    self.txtInput.layer.cornerRadius= 4.0;
    self.txtInput.layer.borderWidth =0.5;
    self.txtInput.layer.borderColor = [[UIColor lightGrayColor] CGColor];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
