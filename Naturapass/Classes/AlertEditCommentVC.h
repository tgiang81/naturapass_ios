//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
typedef void (^AlertEditCallback)(NSDictionary *dic);
@interface AlertEditCommentVC : UIViewController<UITextViewDelegate>
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strComment;
}
@property (nonatomic,copy) AlertEditCallback callback;
-(void)doBlock:(AlertEditCallback ) cb;
-(IBAction)closeAction:(id)sender;
-(void)showInVC:(UIViewController*)vc;
-(instancetype)initWithTitle:(NSString*)stitle strcomment:(NSString*)strcomment;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic)     IBOutlet UITextView *tvEditText;
@property (nonatomic, strong)  UIToolbar                   *keyboardToolbar;

@end
