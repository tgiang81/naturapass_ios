//
//  PublicationBaseVC.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PublicationBaseVC : BaseVC
@property (nonatomic, assign) BOOL isFromNewSignal;

- (IBAction)onNext:(id)sender;
@end
