//
//  GroupCreateOBJ.m
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateOBJ.h"
#import "WebServiceAPI.h"
#import "UIAlertView+Blocks.h"
#import "AppCommon.h"
@implementation GroupCreateOBJ


static GroupCreateOBJ *sharedInstance = nil;

+ (GroupCreateOBJ *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) resetParams
{
    self.imgData = nil;
    self.group_id =@"";
    self.strName=@"";
    self.strComment=@"";
    self.isModifi =NO;
    self.listHuntAdmin=nil;
    
    self.allow_add = NO;
    self.allow_show = NO;
    
    self.allow_chat_add = NO;
    self.allow_chat_show = NO;

}
-(void)modifiGroupWithVC:(UIViewController*)viewcontroller
{
    BaseVC *vc = (BaseVC*)viewcontroller;
    //    [UIAlertView showWithTitle:@"Modification du groupe"
    //                       message:NSLocalizedString(@"Etes-vous sur de vouloir modifier votre groupe ?", @"")
    //             cancelButtonTitle:@"Oui"
    //             otherButtonTitles:@[@"Non"]
    //                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
    //                          if (buttonIndex == [alertView cancelButtonIndex]) {
    GroupCreateOBJ *obj =[GroupCreateOBJ sharedInstance];
    
    NSDictionary *postDict =[NSDictionary new];
    NSDictionary * attachmentt= [NSDictionary new];
    postDict = @{
                 @"group":@{                 @"name":obj.strName,
                                             @"description":obj.strComment,
                                             @"access":[NSString stringWithFormat:@"%u",obj.accessKind],
                                             @"allow_add":  [NSNumber numberWithBool:obj.allow_add]  ,
                                             @"allow_show": [NSNumber numberWithBool:obj.allow_show],
                                             
                                             @"allow_add_chat":  [NSNumber numberWithBool:obj.allow_chat_add]  ,
                                             @"allow_show_chat": [NSNumber numberWithBool:obj.allow_chat_show]

                                             }
                 
                 };
    
    
    if (obj.imgData != nil) {
        UIImage *image = [UIImage imageWithData:obj.imgData];
        NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
        
        attachmentt = @{@"kFileName": @"image.png",
                        @"kFileData": fileData};
    }
    
    //update group
    if ([GroupCreateOBJ sharedInstance].isModifi) {
        [COMMON addLoading:vc];
        WebServiceAPI *serviceAPI =[WebServiceAPI new];
        [serviceAPI putUpdateGroupItem:postDict group_id:obj.group_id];
        serviceAPI.onComplete =^(NSDictionary *response, int errCode)
        {
            //image...if change...
            
            if (obj.imgData!=nil) {
                WebServiceAPI *serviceObj = [WebServiceAPI new];
                
                [serviceObj postGroupPhoto:obj.group_id withData:attachmentt?attachmentt:NULL];
                
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    
                    [COMMON removeProgressLoading];
                    [vc backEditListGroups];
                    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_GROUP object: nil userInfo: nil];
                };
            }else{
                
                //Step9...3
                
                [COMMON removeProgressLoading];
                [vc backEditListGroups];
                [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_GROUP object: nil userInfo: nil];
            }
        };
    }
    //                          }
    
    //                      }];
}
@end
