//
//  CellKind23.m
//  Naturapass
//
//  Created by Giang on 3/7/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "CellKind23.h"
#import "AppCommon.h"
@implementation CellKind23

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label1.preferredMaxLayoutWidth = CGRectGetWidth(self.label1.frame);
}


@end
