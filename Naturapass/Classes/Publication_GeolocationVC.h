//
//  Publication_GeolocationVC.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"

@interface Publication_GeolocationVC : PublicationBaseVC
@property (nonatomic, strong) NSDictionary *myDic;
@end
