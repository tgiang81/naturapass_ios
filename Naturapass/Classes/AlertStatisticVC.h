//
//  AlertStatisticVC.h
//  Naturapass
//
//  Created by giangtu on 1/27/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertStatisticVC : UIViewController
{
    //Statistic Pedometer
    
    __unsafe_unretained IBOutlet UILabel *lbDistance;
    __unsafe_unretained IBOutlet UILabel *lbDenivele;
    __unsafe_unretained IBOutlet UILabel *lbDeniveleNegatif;
    __unsafe_unretained IBOutlet UILabel *lbVitesseMoyenne;
    
    __unsafe_unretained IBOutlet UIButton *btnPedometer;
    __unsafe_unretained IBOutlet UIButton *btnTerminal;
    __unsafe_unretained IBOutlet UIButton *btnContinue;
    
    __unsafe_unretained IBOutlet UILabel *lbTitlePedometer;
    __unsafe_unretained IBOutlet UILabel *lbTitleTerminal;
    
    //Confirm end recording
    __unsafe_unretained IBOutlet UIButton *btnEndRecording;
    __unsafe_unretained IBOutlet UIButton *btnCancel;

}
@property (strong, nonatomic) NSString *strLivehunt;

@end
