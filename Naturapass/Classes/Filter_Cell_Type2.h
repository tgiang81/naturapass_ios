//
//  Filter_Cell_Type2.h
//  Naturapass
//
//  Created by Manh on 9/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_Type2 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeightArrow;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintPaddingLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;
@end
