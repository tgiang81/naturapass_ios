//
//  BaseNavigation.m
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import "SignUpNavigation.h"
#import "Define.h"
@interface SignUpNavigation ()
@end

@implementation SignUpNavigation

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.imgLogo setImage:[UIImage imageNamed:strLogo]];

}

@end
