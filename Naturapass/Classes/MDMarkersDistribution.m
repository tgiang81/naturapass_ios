//
//  MDMarkersDistribution.m
//  Naturapass
//
//  Created by Manh on 3/28/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDMarkersDistribution.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "CommonHelper.h"
#import "NSString+HTML.h"
#import "Config.h"
#import "ArrowView.h"
#import "UIImage+ProportionalFill.h"

#define  RADIUS 14
#define MOVE_DELTA 3
#define ICON_WIDTH  27
#define ICON_HEIGHT 27
static const CGFloat kNewMapAnnotationViewArrowWidth = 18;
static const CGFloat kNewMapAnnotationViewSquareSize = 45;
static const CGFloat kNewMapAnnotationViewArrowHeight = 12;

@interface MDMarkersDistribution ()
{
    NSMutableArray *arrMutPartners;
    
}
@property (nonatomic, strong)   NSMutableArray *arrPartners;
@property (nonatomic, strong)   UIImageView *imageView;
@property (nonatomic, assign) BOOL isObserving;
@property (nonatomic, strong) ArrowView *arrowView;

@end

@implementation MDMarkersDistribution


-(instancetype)initWithDictionary:(NSDictionary*)dicMarker
{
    self = [super init];
    
    if (self)
    {
        self.dicMarker = dicMarker;
        
        self.clipsToBounds = NO;
        //arrow
        CGRect arrowFrame = CGRectMake(((kNewMapAnnotationViewSquareSize - kNewMapAnnotationViewArrowWidth) / 2), kNewMapAnnotationViewSquareSize, kNewMapAnnotationViewArrowWidth, kNewMapAnnotationViewArrowHeight);
        self.arrowView = [[ArrowView alloc] initWithFrame: arrowFrame];
        [self addSubview: self.arrowView];
        [self setHidesArrow: NO];
        //end arrow
        
        arrMutPartners = [NSMutableArray new];
        self.arrPartners = [NSMutableArray new];
        
        pv.backgroundColor = [UIColor whiteColor];
        CGRect fr = pv.frame;
        fr.origin.x = kNewMapAnnotationViewArrowWidth/2;
        pv.userInteractionEnabled = YES;
        pv.frame = fr;
        
        //border
        [[CommonHelper sharedInstance] setRoundedView:pv toDiameter:RADIUS];
        [self addSubview: [self contentView]];
    }
    
    return self;
}
- (void) setHidesArrow:(BOOL)hidesArrow
{
    _hidesArrow = hidesArrow;
    
    [[self arrowView] setHidden: _hidesArrow];
    
    [[self backgroundView] setOpaque: !_hidesArrow];
    
    [[self backgroundView] setHidden: _hidesArrow];
}

- (UIView *) contentView
{
    if (!_contentView)
    {
        pv = [[UIView alloc]initWithFrame:CGRectMake(kNewMapAnnotationViewArrowWidth/2 - MOVE_DELTA , 20, ICON_WIDTH + 5, heightPopOut)];
        pv.userInteractionEnabled = YES;
        
        UIImageView *imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, ICON_WIDTH, ICON_HEIGHT)];
        [pv addSubview:imgIcon];
        [imgIcon setImage: [UIImage imageNamed: @"ic_distributor_map" ]];
        
        imgIcon.contentMode =  UIViewContentModeScaleAspectFit;
        
        CGPoint pTmp = imgIcon.center;
        pTmp.x = (ICON_WIDTH + 5) / 2;
        
        pTmp.y = heightPopOut/2;
        
        imgIcon.center = pTmp;
        
        //border
        [pv.layer setMasksToBounds:YES];
        pv.layer.cornerRadius= RADIUS /2;
        pv.layer.borderWidth =0;
        
        _contentView = pv;
        _contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return _contentView;
}

-(void) fnCalcPopview
{
    
    UIView* vTmp = [pv viewWithTag:199];
    if (vTmp) {
        [vTmp removeFromSuperview];
    }
    
    UIView* vT2 = [pv viewWithTag:200];
    if (vT2) {
        [vT2 removeFromSuperview];
    }
    
    //199
    UIView *v = [[UIView alloc] init];
    v.tag = 199;
    
    float offset = 0;
    
    
    for (int i = 0; i < arrMutPartners.count; i++) {
        
        NSDictionary *dic = arrMutPartners[i];
        
        UIImageView*imgv = [[UIImageView alloc] init];
        
        imgv.image = [self fnResize:dic[@"image"] :heightImageInPop ];
        
        imgv.frame = CGRectMake(offset, 0, imgv.image.size.width, imgv.image.size.height);
        offset += imgv.image.size.width;
        [v addSubview:imgv];
    }
    
    v.frame = CGRectMake(ICON_WIDTH + 5, (heightPopOut - heightImageInPop)/2, offset, heightImageInPop);
    [pv addSubview:v];
    
    //200 move move....
    pv.frame = CGRectMake(kNewMapAnnotationViewArrowWidth/2 - MOVE_DELTA, 20, ICON_WIDTH + 5 + offset, heightPopOut);
    
    [pv.layer setMasksToBounds:YES];
    pv.layer.cornerRadius= RADIUS /2;
    pv.layer.borderWidth =0;
    
    CGRect rect=  pv.frame;
    // height = pvY+ pvHeight+ heighArrow;
    //width = pxX+ pvWidth
    rect.size.height = rect.origin.y+heightPopOut+kNewMapAnnotationViewArrowHeight;
    rect.size.width = rect.size.width+ rect.origin.x;
    self.frame = rect;
    UIImage *image = [self imageFromWithView:self];
    [self returnImage:image];
    //    [self performSelector: @selector(returnImage:) withObject:image afterDelay:0.8];
    
}
-(void)returnImage:(UIImage*)image
{
    if (_CallBackMarkers) {
        _CallBackMarkers(image);
    }
}
- (UIView *) backgroundView
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [backgroundView setBackgroundColor: [UIColor redColor]];
    
    [backgroundView setOpaque: YES];
    return backgroundView;
}

-(UIImage*) fnResize :(UIImage*)img :(float)hHeight
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualWidth/actualHeight;
    
    float fixHeight = hHeight;
    
    imgRatio = fixHeight / actualHeight;
    actualWidth = imgRatio * actualWidth;
    actualHeight = fixHeight;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}
- (UIImage *)imageFromWithView:(UIView*)vView
{
    UIView *view = vView;
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
/*
 {
 "c_address" = "104 CHEMIN DU MAS DE CHEYLON";
 "c_brands" = "[{\"name\":\"Browning\",\"partner\":1,\"logo\":\"\\/uploads\\/brands\\/images\\/resize\\/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg\"}]";
 "c_city" = NIMES;
 "c_cp" = 30900;
 "c_email" = "michel.antoine@armurerie-francaise.com";
 "c_id" = 663;
 "c_lat" = "0.0";
 "c_logo" = "";
 "c_lon" = "0.0";
 "c_name" = APOLLO;
 "c_tel" = "04 48 06 01 33";
 "c_updated" = 1433238045;
 }
 */

-(void) loadImageData
{
    NSDictionary *dic = self.dicMarker;
    //CALLOUT PARTNER
    
    [self.arrPartners removeAllObjects];
    
    NSData *brandData = [dic[@"c_brands"] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSError *e;
    NSArray *brandArr = [NSJSONSerialization JSONObjectWithData:brandData options:NSJSONReadingMutableContainers error:&e];
    
    for (NSDictionary*obj in brandArr)
    {
        /*if ([obj[@"partner"] intValue] == 0)
         {
         //is brand
         //No partner -> only display icon
         }
         else */
        if ([obj[@"partner"] intValue] == 1)
        {
            //Is partner:
            [self.arrPartners addObject:obj];
        }
        
    }
    //        https://www.naturapass.com/uploads/brands/images/resize/0e599fee1de128bfe6fc418cef0befd5c754ab3a.jpeg
    //        https://www.naturapass.com/uploads/brands/images/resize/01ee7fe6d4fdcdeddc7b85744d498105ab8e12a0.jpeg
    //        https://www.naturapass.com/uploads/brands/images/resize/1d86ee2d9b6f5c95b79addb74e8638b378613a9f.jpeg
    //        {
    //            logo = "/uploads/brands/images/resize/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg";
    //            name = Browning;
    //            partner = 1;
    //        }
    //    NSDictionary *dic1 = @{
    //                           @"logo" : @"/uploads/brands/images/resize/0e599fee1de128bfe6fc418cef0befd5c754ab3a.jpeg",
    //                           @"name" : @"Verny-Carron",
    //                           @"partner" : @1
    //                           };
    //    NSDictionary *dic2 = @{
    //                           @"logo" : @"/uploads/brands/images/resize/01ee7fe6d4fdcdeddc7b85744d498105ab8e12a0.jpeg",
    //                           @"name" : @"Remington",
    //                           @"partner" : @1
    //                           };
    //    NSDictionary *dic3 = @{
    //                           @"logo" : @"/uploads/brands/images/resize/1d86ee2d9b6f5c95b79addb74e8638b378613a9f.jpeg",
    //                           @"name" : @"Mary Arrm",
    //                           @"partner" : @1
    //                           };
    //    [self.arrPartners removeAllObjects];
    //    self.arrPartners = [NSMutableArray arrayWithArray:@[dic1,dic2,dic3]];
    
    //Download partners
    if (self.arrPartners.count > 0)
    {
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        __block int iCount = (int) self.arrPartners.count;
        
        [arrMutPartners removeAllObjects];
        
        for (NSDictionary*dic in self.arrPartners) {
            NSURL *url  = nil;
            
            url = [NSURL URLWithString: [ NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"logo"] ] ];
            [manager loadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                iCount -= 1;
                
                if (image) {
                    UIImage *imgLogo = [self fnResize:image :heightImageLogo];
                    
                    UIImage *imgCorrected = nil;
                    
                    if (imgLogo.size.width > widthCell) {
                        imgCorrected = [self fnResizeFixWidth:imgLogo :widthCell];
                    }
                    UIImage *rsized = [self fnResize:image :heightImageInPop];
                    
                    [arrMutPartners addObject:@{ @"image":rsized,
                                                 @"size": [NSNumber numberWithFloat:rsized.size.width]
                                                 }];
                }
                if (iCount == 0) {
                    //NSLog(@"DONE");
                    [self fnCalcPopview];
                }

            }];
        }
    }
    else
    {
        CGRect rect=  pv.frame;
        // height = pvY+ pvHeight+ heighArrow;
        //width = pxX+ pvWidth
        rect.size.height = rect.origin.y+heightPopOut+kNewMapAnnotationViewArrowHeight;
        rect.size.width = rect.size.width+ rect.origin.x;
        self.frame = rect;
        
        dispatch_queue_t _queue = dispatch_queue_create("com.mycompany.myqueue.marker.distribution", DISPATCH_QUEUE_SERIAL);
        
        dispatch_async(_queue, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self fnCalcPopview];


            });
            
        });
    }
    
}

- (void)doSetCalloutView {
    
    [self loadImageData];
}
@end
