//
//  MurParameter_Notification_Smartphones.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurParameter_Notification_Smartphones.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface MurParameter_Notification_Smartphones ()
{
    NSMutableArray * arrData;
    
}

@end

@implementation MurParameter_Notification_Smartphones

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [NSMutableArray new];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnGET_NOTI_PARAMETERS];
    
    [COMMON addLoading:self];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        
        if([response[@"notifications"] isKindOfClass: [NSArray class]])
        {
            
            NSArray *arr = response[@"notifications"];
            
            for (NSDictionary*dic in arr) {
                [arrData addObject:@{@"name": dic[@"label"],
                                     @"image":@"mur_st_ic_message",
                                     @"status": dic[@"wanted"] ,
                                     @"id": dic[@"id"]
                                     }];
            }
            
            [self.tableControl reloadData];
            
        }
        //        "emails": [
        //                   {
        //                       "id": 1,
        //                       "label": "Commentaire sur votre publication",
        //                       "wanted": 1
        //                   },
    };
    
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind7" bundle:nil] forCellReuseIdentifier:identifierSection1];
    self.tableControl.estimatedRowHeight = 66;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CellKind7 *cell = nil;
    
    cell = (CellKind7 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];

    
    //Status
    cell.swControl.tag = indexPath.row + 100;
    cell.swControl.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [cell.swControl setOnTintColor:UIColorFromRGB(ON_SWITCH)];
    [cell.swControl setOn:[dic[@"status"] boolValue]];
    [cell.swControl addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)switchValueChanged:(id)sender
{
    UISwitch *sv = (UISwitch*)sender;
    int index = (int)sv.tag - 100;
    NSDictionary *dic = [arrData[index] mutableCopy];
    //1 10 3 4 11 6 7 5 8 9
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (response) {
            if ([response[@"success"] boolValue]) {
//                ASLog(@"%@",response);
                //change data
                [dic setValue: @(!([dic[@"status"] boolValue])) forKey: @"status" ];
                [arrData replaceObjectAtIndex:index withObject:dic];
                
            }
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:index inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.tableControl reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    };
    
    [serverAPI putSmartphoneNotificationSetting:@{@"option": dic[@"id"],
                                                    @"value": [NSNumber numberWithBool:sv.isOn]
                                                    }];
}
@end
