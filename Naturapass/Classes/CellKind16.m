//
//  CellKind16.m
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "CellKind16.h"

@implementation CellKind16

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.imgProfile.layer setMasksToBounds:YES];
    self.imgProfile.layer.cornerRadius= 25;
    self.imgProfile.layer.borderWidth =0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}
@end
