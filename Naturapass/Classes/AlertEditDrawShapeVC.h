//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
typedef void (^AlertEditDrawShapeVCCallback)(NSString *type);

@interface AlertEditDrawShapeVC : UIView<UITextFieldDelegate>
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
}
@property (nonatomic,copy) AlertEditDrawShapeVCCallback callback;
@property (nonatomic,strong) IBOutlet UIButton *deleteButton;
@property (nonatomic,strong) IBOutlet UIButton *modifyrButton;
@property (nonatomic,strong) IBOutlet UIButton *closeButton;

@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDesc;

@property(nonatomic,strong) IBOutlet UIView *borderColor;
@property(nonatomic,strong) IBOutlet UIView *viewEdit;
@property(nonatomic,strong) IBOutlet UIView *viewInfo;

@property (nonatomic,assign) BOOL isEdit;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;

-(void)doBlock:(AlertEditDrawShapeVCCallback ) cb;
-(void)showAlertWithDic:(NSDictionary*)dicInput withEdit:(BOOL)isEdit;
-(instancetype)init;
@end
