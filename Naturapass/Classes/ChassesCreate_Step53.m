//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step53.h"
#import "ChassesCreate_Step6.h"
#import "TypeCell31.h"
//
#import "CLTokenInputView.h"
#import "APContact.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SAMTextView.h"
#import "MDCheckBox.h"
#import "APAddressBook.h"
#import "TPKeyboardAvoidingScrollView.h"


#import "GroupCreate_Step4.h"
#import "GroupCreate_Step5.h"
#import "GroupCreate_Step6.h"
#import "GroupCreate_Step8.h"

#import "GroupCreateCell_Step4.h"
//tokenVS
#import "UserListViewController.h"
#import "VSTokenInputView.h"
#import "VSToken.h"
static CGFloat kMinInputViewHeight = 43;
static CGFloat kMaxInputViewHeight = 94;
static CGFloat kLeftPadding = 15;
static CGFloat kTopPadding = 10;
static CGFloat kRightPadding = 15;
static CGFloat kBottomPadding = 10;
static NSString *kEmailKey = @"email";
static NSString *kNameKey = @"name";
//


static NSString *GROUPCREATECELLSTEP4 = @"GroupCreateCell_Step4";
static NSString *CELLID4 = @"cellid4";

@interface ChassesCreate_Step53 ()<InputViewDelegate>
{
    IBOutlet UISearchBar    *toussearchBar;
    NSMutableArray *tourMemberArray;
    
    __weak IBOutlet UIView *viewEmail;
    __weak IBOutlet UIButton *btnEn;
    
    __weak IBOutlet UIButton *btnAucun;
    
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblDescription1;
    __weak IBOutlet UILabel *lblDescription2;
}

@property (strong, nonatomic) IBOutlet SAMTextView *emailContent;

@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSArray *filteredContacts;

@property (strong, nonatomic) NSMutableArray *selectedContacts;
@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
//tokenVS
@property (weak, nonatomic) IBOutlet UILabel        *placehoderInputView;
@property (weak, nonatomic) IBOutlet VSTokenInputView *addedMembersInputView;
@property (weak, nonatomic) IBOutlet UIView *userListContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addedMemberViewHeight;
@property (nonatomic) UserListViewController *userListController;
@property (nonatomic) NSMutableArray *addedMembers;
@property (nonatomic) NSMutableDictionary *tokensCache;
//

@end

@implementation ChassesCreate_Step53
#pragma mark - viewdid and init
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strINVITERDESMEMBRESNATURAPASS);
    lblDescription1.text = str(strPourInviterDesMembresNaturapass);
    lblDescription2.text = str(strLaPersonneQueVousRecherchez);
    // Do any additional setup after loading the view from its nib.
    [btnAucun setTitle: str(strAucunResultat) forState:UIControlStateNormal];
    self.needChangeMessageAlert = YES;

    [self initialization];
    [btnEn setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    
    [btnEn setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [btnEn setTitle:str(strValider) forState:UIControlStateNormal ];

    }
    //tokenVS
    self.addedMembersInputView.inPutViewDelegate = self;
    self.userListContainerView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.placehoderInputView.text = str(strTokenPlaceholder);
    //

}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //tockenVS
    [self configureUserListViewController];
    
}
-(void)initialization
{
    viewEmail.hidden = YES;
    self.emailContent.placeholder = str(strEmailContentPlaceholder);
    
    [self.tableControl registerNib:[UINib nibWithNibName:GROUPCREATECELLSTEP4 bundle:nil] forCellReuseIdentifier:CELLID4];
    
    [toussearchBar setPlaceholder:str(strRechercherUnMembre)];
    tourMemberArray = [NSMutableArray new];
    
    //for email view
    [self initialiseVariable];
    [self InitializeKeyboardToolBar];
    [self.emailContent setInputAccessoryView:self.keyboardToolbar];
    
}
#pragma mark -
- (void) initialiseVariable
{
    self.contacts = [NSMutableArray new];
    self.filteredContacts = nil;
    self.selectedContacts = [NSMutableArray new];
    
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    addressBook.fieldsMask = APContactFieldFirstName | APContactFieldLastName | APContactFieldEmails;
    addressBook.sortDescriptors = @[
                                    [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES],
                                    [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES]];
    [addressBook loadContacts:^(NSArray *contacts, NSError *error)
     {
         if (!error) {
             for (APContact *contact in contacts) {
                 NSArray *emails = contact.emails;
                 for (NSString *anEmail in emails) {
                     if (![self.contacts containsObject:anEmail]) {
                         [self.contacts addObject:anEmail];
                     }
                 }
             }
         }
     }];
    
}
- (void)resignKeyboard:(id)sender
{
    //tokenVS
    [self.view endEditing:YES];
    self.userListContainerView.hidden = YES;
    
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tourMemberArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:CELLID4 forIndexPath:indexPath];
    [cell.btnInvite setTypeCheckBox:UI_CHASSES_MUR_ADMIN];
    
    NSDictionary * dic = tourMemberArray[indexPath.row];
    
    [cell.lblTitle setText:dic[@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([dic[@"check"] boolValue]) {
        [cell.btnInvite setSelected:YES];
    }
    else
    {
        [cell.btnInvite setSelected:NO];
    }
    [cell.btnInviteOver addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row;
    
    return  cell;
    
    return  cell;
}


#pragma mark - Action
-(IBAction)addFriend:(UIButton *)sender{
    
    NSDictionary * dic = tourMemberArray[sender.tag];
    int i=(int)sender.tag;
    if ([dic[@"check"] boolValue]) {
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }else{
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:YES] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }
    
    [self.tableControl reloadData];
}





- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    //    CellKind3 *cell = (CellKind3 *)[tableView cellForRowAtIndexPath: indexPath];
    //    [self addFriend:cell.btnInvite];
}
- (BOOL)validateEmailWithString:(NSString*)emailText
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailText];
}

-(IBAction)fnSend:(id)sender
{
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < tourMemberArray.count; i++ )
    {
        NSDictionary*dic = tourMemberArray[i];
        if ([dic[@"check"] boolValue]) {
            [mutArr addObject:dic[@"id"]];
        }
    }
    
    
    //list friend is valid...has selected friend
    if (tourMemberArray.count > 0 && mutArr.count > 0 )
    {
        //IS SEND TO NON MEMBER
        
        NSString *strMsg = @"";
        if ((int)mutArr.count  == 1) {
            strMsg = [NSString stringWithFormat:str(strVous_avez_envoye_invitation),(int)mutArr.count ];
        }else{
            strMsg = [NSString stringWithFormat:str(strVous_avez_envoyer_invitations),(int)mutArr.count ];
        }
        
        [UIAlertView showWithTitle:str(strTitle_app) message: strMsg
                 cancelButtonTitle:str(strOK)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                              [COMMON addLoading:self];
                              //Request
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj fnPOST_JOIN_USER_LOUNGE:@{@"lounge": @{@"subscribers": mutArr}} loungeID:[ChassesCreateOBJ sharedInstance].strID];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  /*
                                   subscribers =     (
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 219;
                                   },
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 161;
                                   },
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 168;
                                   }
                                   );
                                   
                                   */
                                  [self gotoback];
                              };
                          }];
        
    }else{
        if (viewEmail.hidden == NO) {
            //tokenVS
            NSString *listEmail =self.addedMembersInputView.text;
            if (listEmail.length==0) {
                UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strAdresseEmailNonValide)
                                                                  message:@""
                                                                 delegate:self
                                                        cancelButtonTitle:str(strOui)
                                                        otherButtonTitles:nil];
                [alert show];
                return;
            }

            //IS SEND EMAIL
            [COMMON addLoading: self];
            WebServiceAPI *serviceAPI =[WebServiceAPI new];
                [serviceAPI postLoungeInvitationByEmailWithLounge: [ChassesCreateOBJ sharedInstance].strID subject:@"Invitation" toEmails:listEmail withContent:self.emailContent.text?self.emailContent.text:@"" ];
            serviceAPI.onComplete =^(NSDictionary *response, int errCode)
            {
                [COMMON removeProgressLoading];
                [self gotoback];
            };
        }
        
        }

    
}

#pragma mark - TextField Delegate
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [toussearchBar setText:@""];
    
    [searchBar resignFirstResponder];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    
    [theSearchBar resignFirstResponder];
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [self goLoungesSearchURL];
}

- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self goLoungesSearchURL];
    
}
#pragma mark - WebService
- (void) goLoungesSearchURL
{
    NSString *encodedString = [toussearchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    if (encodedString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];

    }
    else
    {
        [COMMON addLoadingForView:self.view];
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj getUsersSearchAction:encodedString];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            NSArray *arrFriends = response[@"users"] ;
            
            [COMMON removeProgressLoading];
            
            if (arrFriends.count == 0) {
                //display error..other while-> hidden it...
                [self.view bringSubviewToFront:viewEmail];
                viewEmail.hidden = NO;
                if ([ChassesCreateOBJ sharedInstance].isModifi) {
                    [btnEn setTitle:str(strValider) forState:UIControlStateNormal ];
                    
                }
                else
                {
                    [btnEn setTitle:str(strEnvoyer)  forState:UIControlStateNormal];
                }
                
                
            }else{
                viewEmail.hidden = YES;
                if ([ChassesCreateOBJ sharedInstance].isModifi) {
                    [btnEn setTitle:str(strValider) forState:UIControlStateNormal ];
                    
                }
                else{
                    [btnEn setTitle:str(strEnvoyer) forState:UIControlStateNormal];
                    [btnEn setTitle:str(strSUIVANT) forState:UIControlStateNormal];
                }
                
                //update list
                [tourMemberArray removeAllObjects];
                
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                             ascending:YES];
                //
                NSArray *results = [arrFriends sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                
                [tourMemberArray addObjectsFromArray:results ];
                
                for (int i=0;i<tourMemberArray.count; i++) {
                    NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
                    [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
                    [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
                }
                
                [self.tableControl reloadData];
                
                
            }
            
        };
    }

}
//MARK: -tokenVS
- (NSMutableArray *)addedMembers
{
    if (!_addedMembers)
    {
        _addedMembers = [NSMutableArray array];
    }
    return _addedMembers;
}

- (NSDictionary *)tokensCache
{
    if (!_tokensCache)
    {
        _tokensCache = [NSMutableDictionary dictionary];
    }
    return _tokensCache;
}

- (void)configureUserListViewController
{
    if (!self.userListController)
    {
        self.userListController = [UserListViewController userListController];
        //        [self addChildViewController:self.userListController];
        self.userListController.view.frame = self.userListContainerView.bounds;
        [self.userListContainerView addSubview:self.userListController.view];
        //        [self.userListController didMoveToParentViewController:self];
    }
    
    __weak ChassesCreate_Step53 *weakSelf = self;
    self.userListController.didScrollBlock = ^{
        [weakSelf.view endEditing:YES];
        weakSelf.userListContainerView.hidden = YES;
    };
}
- (void)layoutViewsBasedOnComposerHeight
{
    self.addedMemberViewHeight.constant = kMinInputViewHeight;
    self.addedMembersInputView.textContainerInset = UIEdgeInsetsMake(kTopPadding, kLeftPadding, kBottomPadding, kRightPadding);
}
- (void)searchUsersForText:(NSString *)searchText
{
    self.userListContainerView.hidden = NO;
    __weak ChassesCreate_Step53 *weakSelf = self;
    
    [self.userListController searchUserForUserName:searchText addedUsers:self.addedMembers withSelectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        weakSelf.userListContainerView.hidden = YES;
        if (succes)
        {
            [weakSelf addUser:dicUser updateTokenView:YES];
        }
    } deselectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        if (succes)
        {
            [weakSelf removeUser:dicUser];
        }
    }];
}
- (void)addUser:(NSDictionary *)dicUser updateTokenView:(BOOL)update
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (!token)
    {
        token = [[VSToken alloc] init];
        token.name      = dicUser[kNameKey];
        token.uniqueId  = dicUser[kEmailKey];
        token.sEmail    = dicUser[kEmailKey];
        
        [self.tokensCache setValue:token forKey:token.uniqueId];
    }
    token.timeStamp = [NSDate date];
    
    [self.addedMembers addObject:dicUser];
    [self.addedMembersInputView addToken:token needsLayout:update];
}

- (void)removeUser:(NSDictionary *)dicUser
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (token)
    {
        [self.addedMembers removeObject:dicUser];
        [self.tokensCache removeObjectForKey:token.uniqueId];
        [self.addedMembersInputView removeToken:token needsLayout:YES];
    }
}

- (NSDictionary *)userForToken:(VSToken *)token
{
    NSDictionary *userForToken = nil;
    for (NSDictionary *dicUser in self.addedMembers)
    {
        if ([dicUser[kEmailKey] isEqualToString:token.uniqueId])
        {
            userForToken = dicUser;
            break;
        }
    }
    return userForToken;
}
#pragma mark InputViewDelegate
- (void)keyboardWillHide:(NSNotification*)notification {
    self.userListContainerView.hidden = YES;
}
- (void)textDidChange:(NSString *)text
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if ([text length])
    {
        [self performSelector:@selector(searchUsersForText:) withObject:text afterDelay:0.3];
    }
    else
    {
        [self.userListController resetUserListScreen];
    }
    if (self.addedMembersInputView.text.length == 0) {
        self.placehoderInputView.hidden = NO;
        
    }
    else
    {
        self.placehoderInputView.hidden = YES;
    }
}

- (void)didRemoveToken:(VSToken *)token
{
    NSDictionary *dicUser = [self userForToken:token];
    if (dicUser)
    {
        [self removeUser:dicUser];
        [self.userListController resetUserListScreen];
    }
}


- (void)didUpdateSize:(CGSize)size
{
    CGFloat height = [self correctedHeight:size.height];
    [UIView animateWithDuration:0.2 animations:^{
        self.addedMemberViewHeight.constant = height;
        [self.view layoutIfNeeded];
    }];
}

- (CGFloat)correctedHeight:(CGFloat)height
{
    CGFloat correctedHeight = 0;
    if (height < kMinInputViewHeight)
        correctedHeight = kMinInputViewHeight;
    else if (height > kMaxInputViewHeight)
        correctedHeight = kMaxInputViewHeight;
    else
        correctedHeight = height;
    
    return correctedHeight;
}

@end
