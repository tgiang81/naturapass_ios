//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"

@interface TypeCell11: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;

//image
@property (nonatomic,retain) IBOutlet UIImageView       *imgParticipe;
@property (nonatomic,retain) IBOutlet UIImageView       *imgNeSaisPas;
@property (nonatomic,retain) IBOutlet UIImageView       *imgNeParticipePas;

//label
@property (nonatomic,retain) IBOutlet UILabel           *lbParticipe;
@property (nonatomic,retain) IBOutlet UILabel           *lbNeSaisPas;
@property (nonatomic,retain) IBOutlet UILabel           *lbNeParticipePas;

@property (nonatomic,retain) IBOutlet UIButton           *btnParticipe;
@property (nonatomic,retain) IBOutlet UIButton           *btnNeSaisPas;
@property (nonatomic,retain) IBOutlet UIButton           *btnNeParticipePas;

@end
