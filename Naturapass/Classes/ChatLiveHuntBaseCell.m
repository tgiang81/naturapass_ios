//
//  MediaCell.m
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatLiveHuntBaseCell.h"
#import "AppCommon.h"
#import "OHASBasicHTMLParser.h"
#import "ASSharedTimeFormatter.h"
#import "NSString+Extensions.h"
#import "Define.h"
#import "MediaCell+Color.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+EMOEmoji.h"
@implementation ChatLiveHuntBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [COMMON listSubviewsOfView:self];

    [self bringSubviewToFront:self.btnDetail];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    int iWidthChatView = screenRect.size.width;
    CGRect rect= self.frame;
    rect.size.width = iWidthChatView- 50;
    self.frame = rect;
}
//publier_option_bg
- (void)layoutSubviews
{
    [super layoutSubviews];
}

-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

-(void)assignValues:(NSDictionary *)dtInput
{
    NSDictionary*liveHuntDic = dtInput;
    NSString *strImage=@"";
    if (liveHuntDic[@"owner"][@"profilepicture"] != nil) {
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,liveHuntDic[@"owner"][@"profilepicture"]];
    }else{
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,liveHuntDic [@"owner"][@"photo"]];
    }
    strImage = [[strImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSURL * url = [[NSURL alloc] initWithString:strImage];
    [self.imageProfile sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    
    if ([dtInput[@"type"] isEqualToString:MessageType ])
    {
        self.img_chat_pub.image = [UIImage imageNamed:@"live_ic_discussion"];
    }else{
        self.img_chat_pub.image = [UIImage imageNamed:@"live_ic_publication"];
    }

    /*
    self.title.text = liveHuntDic[@"owner"][@"fullname"] ;
    [self.timeCreate setText:[self convertDate:liveHuntDic[@"created"]] ];

    if ([dtInput[@"type"] isEqualToString:MessageType ])
    {
        //TO EMOJI
        NSString *messageContent;
        if ([liveHuntDic[@"content"] isKindOfClass:[NSString class]]) {
            messageContent = [liveHuntDic[@"content"] emo_emojiString];
        }
        else{
            messageContent = @"";
        }
        
        self.myContent.htmlText = messageContent;
        
        self.obs_label_static.hidden = YES;
        
        [self.observation setText:@""];
        [self.obs_tree_content setText:@""];
        
    }
    else{

        int valueCheck =0;
        
        if   ( (liveHuntDic[@"media"] != nil) &&   [liveHuntDic[@"media"] isKindOfClass: [ NSDictionary class]] )
        {
            NSString *strImage=@"";
            NSString *strVideo= [liveHuntDic[@"media"][@"type"] stringValue];
            self.imgIcon.hidden = YES;
            //video
            if ([strVideo isEqualToString:@"101"] ){

                strImage = @"icone-video.png";
                
            } else {
                strImage = @"icone-photo.png";

            }
            self.imageContent.image = [UIImage imageNamed:strImage];
            self.contraintHeightImage.constant =83;
        }
        else
        {
            self.imageContent.image = nil;
            self.contraintHeightImage.constant =-13;
            valueCheck +=1;
        }
        
        //TO EMOJI
        NSString *messageContent;
        if ([liveHuntDic[@"content"] isKindOfClass:[NSString class]]) {
            messageContent = [liveHuntDic[@"content"] emo_emojiString];
        }
        else{
            messageContent = @"";
        }
        
        self.myContent.htmlText = messageContent;
        
        //observation
        NSArray *arrObservation = liveHuntDic[@"observations"];
        
        if (arrObservation.count > 0)
        {
            NSDictionary*dicObservation = arrObservation[0];
            
            NSArray*arrTree = dicObservation[@"tree"];
            
            NSMutableString *mutString = [NSMutableString new];
            
            for (int i = 0;  i< arrTree.count; i++) {
                
                NSString *strName = [arrTree[i] emo_emojiString];

                [mutString appendString:strName];
                
                if (i != arrTree.count - 1) {
                    [mutString appendString:@" / "];
                }
            }
            
            self.constraintHeightObservationIcon.constant = 15;
            [self.observation setText:mutString];
            self.obs_label_static.hidden = NO;
            
            NSArray*arrAttachment = dicObservation[@"attachments"];
            if (arrAttachment.count > 0)
            {
                NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
                
                for (int i = 0; i < arrAttachment.count; i++) {
                    
                    NSDictionary*attchDic =  arrAttachment[i];
                    
                    UIColor *color = [UIColor blackColor]; // select needed color
                    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
                    
                    NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:attchDic[@"label"] attributes:attrs];
                    [mLabel appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@": "]];
                    
                    //Add label
                    [mAttach appendAttributedString:mLabel];
                    NSDictionary *attrsValue = @{ NSForegroundColorAttributeName : UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) };
                    //value
                    
                    NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:@"" attributes:attrsValue];
                    
                    if (attchDic[@"value"])
                    {
                        NSString *strName = [attchDic[@"value"] emo_emojiString];

                        [mValue appendAttributedString: [NSAttributedString attributedStringWithString:  strName] ];
                    }
                    else if (attchDic[@"values"])
                    {
                        NSArray *mArr = attchDic[@"values"];
                        
                        for (int i = 0; i < mArr.count; i++) {
                            
                            NSString *str = [attchDic[@"values"][i] emo_emojiString];

                            [mValue appendAttributedString: [NSAttributedString attributedStringWithString: str ] ];
                            if (mArr.count > 1 && i < mArr.count-1) {
                                [mValue appendAttributedString: [NSAttributedString attributedStringWithString: @", " ] ];
                            }
                        }
                    }

                    [mAttach appendAttributedString:mValue];
                    
                    if (i != arrAttachment.count - 1) {
                        [mAttach appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@"\n"]];
                    }
                }
                [self.obs_tree_content setAttributedText:mAttach];
                self.lbLeftIndice.hidden = NO;
                
            }else{
                [self.obs_tree_content setText:@""];
                self.lbLeftIndice.hidden = YES;
            }
        }else{
            self.constraintHeightObservationIcon.constant = -13;
            self.obs_label_static.hidden = YES;
            self.lbLeftIndice.hidden = YES;

            [self.observation setText:@""];
            [self.obs_tree_content setText:@""];
            valueCheck+=1;
        }
        
    }
    */
}
-(void)setThemeWithFromScreen:(ISSCREEN)isScreen
{
    if (isScreen == ISLIVEMAP) {
        //LIVE_MAP_MAIN_BAR_COLOR
        self.title.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        self.observation.textColor =UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        self.lbHeader.backgroundColor =UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
    }
}
#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *outputFormatterBis = [[NSDateFormatter alloc] init];
    
    [outputFormatterBis setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [outputFormatterBis setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [outputFormatterBis setDateFormat: @"d MMM H:mm"];
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}
@end
