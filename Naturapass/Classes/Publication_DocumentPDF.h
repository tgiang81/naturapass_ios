//
//  Publication_DocumentPDF.h
//  Naturapass
//
//  Created by GiangTT on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "PublicationBaseVC.h"

#import <UIKit/UIKit.h>
#import "DirectoryWatcher.h"
#import "ParameterBaseVC.h"
typedef void (^documentCbk)(NSDictionary *objPDF);

@interface Publication_DocumentPDF : PublicationBaseVC<DirectoryWatcherDelegate,UIDocumentInteractionControllerDelegate>
{
}
@property (nonatomic,copy) documentCbk callback;
-(void)doBlock:(documentCbk ) cb;

@end
