//
//  TheProjectCell.m
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "FavorisSystemBaseCell.h"

@interface FavorisSystemBaseCell()
{
    __weak IBOutlet NSLayoutConstraint *contraintPaddingLeft;
}
@property (nonatomic) BOOL isExpanded;

@end

@implementation FavorisSystemBaseCell
- (void)awakeFromNib {
    [super awakeFromNib];
    [COMMON listSubviewsOfView:self];

    // Initialization code
    [self.imgColor.layer setMasksToBounds:YES];
    self.imgColor.layer.cornerRadius= 4.0;
    self.imgColor.layer.borderWidth =0.5;
    self.imgColor.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}
#pragma mark - Draw controls messages

- (void)drawRect:(CGRect)rect
{
    if (self.isEditType) {
        int indentation = (int)self.treeNode.nodeLevel * 35;
        indentation = indentation -20;
        contraintPaddingLeft.constant=indentation;

    }
    else
    {
        int indentation = (int)self.treeNode.nodeLevel * 35 + 20;
        contraintPaddingLeft.constant=indentation;
    }
}

- (void)setTheButtonBackgroundImage:(UIImage *)backgroundImage
{
    [self.imgArrow setImage:backgroundImage];
}

@end
