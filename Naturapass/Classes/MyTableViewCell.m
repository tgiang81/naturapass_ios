//
//  MyTableViewCell.m
//  Naturapass
//
//  Created by Giang on 6/5/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MyTableViewCell.h"

@implementation MyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView layoutIfNeeded];    
    self.name.preferredMaxLayoutWidth = CGRectGetWidth(self.name.frame);
    self.address.preferredMaxLayoutWidth = CGRectGetWidth(self.address.frame);
    self.city.preferredMaxLayoutWidth = CGRectGetWidth(self.city.frame);
    self.tel.preferredMaxLayoutWidth = CGRectGetWidth(self.tel.frame);
    self.email.preferredMaxLayoutWidth = CGRectGetWidth(self.email.frame);

}
@end
