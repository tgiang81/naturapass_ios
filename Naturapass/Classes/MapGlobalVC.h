//
//  MapGlobalVC.h
//  Naturapass
//
//  Created by GS-Soft on 10/4/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "BaseMapVC.h"
#import "ChatLiveHuntView.h"
#import "ARKit.h"
#import "CarterFilterView.h"
#import "ShortcutScreenVC.h"

@interface MapGlobalVC : BaseMapVC<ARLocationDelegate,UIPrintInteractionControllerDelegate>
{
    int iWidthChatView;
    BOOL bWarningNoFilterSelected;
    NSMutableArray *arrNodes;
    //Radar
    ARViewController    *_arViewController;
    NSArray             *_mapPoints;
    BOOL isPrinting, bFlagBusy;
//    BOOL isChooseAction;
    CGRect rectmapScaleView ;
    GMSVisibleRegion visibleRegion;
    BOOL isGoOutScreen;
    NSMutableArray *arrToRedrawAgenda ;

    NSMutableArray *arrToRedraw ;
    NSMutableArray *arrToRedrawDistributor ;
    dispatch_queue_t markerQueue;

    NSMutableArray *arrOwnerOfPublications ;

    __weak IBOutlet UIView *vPopup;
    
    __weak IBOutlet UIImageView *imgLeftMenu;
    __weak IBOutlet UISwitch *jtSwitch;
    __weak IBOutlet UILabel *lbFilter1;
    __weak IBOutlet UILabel *lbFilter2;
    
    __weak IBOutlet NSLayoutConstraint *constraint2TopCallOut;
    
    
    BOOL checkRedraw;
}

@property (nonatomic,retain) NSArray *arrCategoriesFilter;
@property (nonatomic,retain) NSDictionary *simpleDic;
@property (weak, nonatomic)  NSTimer *timerFollow;
@property (weak, nonatomic) IBOutlet UIView *vViewFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnLocationAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnHuntRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnFav;
@property (nonatomic,strong) CarterFilterView* viewFilter;
@property (nonatomic,strong) ShortcutScreenVC* viewShortCutSetting;

@property (weak, nonatomic)  NSTimer *timerChat;

@property (nonatomic,strong) IBOutlet UIView* blurBackground;
@property (nonatomic,strong) IBOutlet UIView* blurSearch;
@property (nonatomic,strong) IBOutlet UIView* viewSearch;
@property (nonatomic,assign)  BOOL showPopoutChat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnRefresh;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnFavorite;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintButtonLeftMenu;

//chat
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthViewChat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintR;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopViewChat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomViewChat;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCenterButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHoziButton;
@property (weak, nonatomic) IBOutlet UILabel *lbRDV;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnRDV_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnRDV_hori;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBtnPrint;

@property (nonatomic,strong) ShortcutScreenVC* viewShortCut;
@property (nonatomic,assign) PEDOMETER_STATUS pedometerStatus;

@end
