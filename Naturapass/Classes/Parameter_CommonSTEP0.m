//
//  Parameter_CommonSTEP0.m
//  Naturapass
//
//  Created by Giang on 3/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_CommonSTEP0.h"


#import "Parameter_CommonSTEP1.h"
#import "Parameter_CommonSTEP2.h"
#import "CellKind23.h"
#import "Config.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_CommonSTEP0 ()
{
    NSArray * arrData;
    __weak IBOutlet UILabel *label_title;
}

@end

@implementation Parameter_CommonSTEP0

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData =@[str(strQuel_type_de_chasse_pratiquez),
               str(strQuel_type_de_chasse_aimeriez)];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind23 *cell = nil;
    cell = (CellKind23 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];

    NSString* name = arrData[indexPath.row];
    
    cell.label1.text = name;
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.rightIcon setImage: [UIImage imageNamed:@"favo_arrow_right" ]];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Parameter_CommonSTEP1 *viewController1 = [[Parameter_CommonSTEP1 alloc] initWithNibName:@"Parameter_CommonSTEP1" bundle:nil];
    
    //    hunt practiced
    switch (indexPath.row) {
        case 0:
        {
            viewController1.myTypeVview = HUNT_PRACTICED;
        }
            break;
            
            //            hunt liked
        case 1:
        {
            viewController1.myTypeVview = HUNT_LIKED;
        }
            break;
            
        default:
            break;
    }
    [self pushVC:viewController1 animate:YES];
    
}


@end
