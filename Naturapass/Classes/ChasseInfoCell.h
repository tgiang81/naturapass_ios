//
//  MesSalonCustomCell.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 3/29/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellBase.h"
#import "Define.h"

@interface ChasseInfoCell : CellBase

//view
@property (nonatomic, retain) IBOutlet UIView *view1;
@property (nonatomic, retain) IBOutlet UIView *view3;

//image
@property (nonatomic, retain) IBOutlet UIImageView *Img1;
@property (nonatomic, retain) IBOutlet UIImageView *Img2;
@property (nonatomic, retain) IBOutlet UIImageView *Img3;
@property (nonatomic, retain) IBOutlet UIImageView *Img7;

//lable
@property (nonatomic, retain) IBOutlet UILabel *label1;
@property (nonatomic, retain) IBOutlet UILabel *label2;
@property (nonatomic, retain) IBOutlet UILabel *label3;
@property (nonatomic, retain) IBOutlet UILabel *label10;
@property (nonatomic, retain) IBOutlet UILabel *label11;

//button



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;

//funtion
-(void)fnSettingCell:(int)type;
@end
