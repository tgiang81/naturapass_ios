//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesSettingMembersParticipeVC.h"
#import "ChassesParameter_Notification_Email.h"
#import "ChassesParameter_Notification_Smartphones.h"
#import "CellKind2.h"
#import "ChassesMemberOBJ.h"
#import "ChasseSettingMembresComment.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChasseSettingMembres.h"
#import "FriendInfoVC.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChassesSettingMembersParticipeVC ()
{
    NSArray * arrData;
    NSString                    *numberOption;
    NSString *str_Owner;
    
    IBOutlet UIImageView *imgAvatar;
    
    IBOutlet UILabel      *lbTile;
    IBOutlet UIImageView *imgCrown;
    IBOutlet UIButton   *delButton;
    IBOutlet UIImageView *iconDelete;
    IBOutlet UIButton   *validerButton;
    IBOutlet UIImageView *iconValider;
    IBOutlet UIImageView *iconSuivant;

    __weak IBOutlet NSLayoutConstraint *contraintHeightTable;
    int owner_id;
    NSDictionary *dic;
    IBOutlet UILabel *DescriptionScreen1;
    IBOutlet UILabel *DescriptionScreen2;

}
@end

@implementation ChassesSettingMembersParticipeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    DescriptionScreen1.text = str(strModificationParticipant);
    DescriptionScreen1.text = str(strParticipation);
    [delButton setTitle: str(strSUPPRIMER) forState:UIControlStateNormal];
    [validerButton setTitle: str(strValider) forState:UIControlStateNormal];
    [self.btnSuivant setTitle: str(strSUIVANT) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
     dic= [NSDictionary new];

    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
    }
    else
    {
        dic =obj.dictionaryMyKind;
    }
    NSString *strFullName = dic[@"fullname"];
    strFullName = [strFullName stringByReplacingOccurrencesOfString:@"\r\n"
                                                         withString:@""];
    lbTile.text = strFullName;
    
    //image
    NSString *strImage;
    
    if (dic[@"profilepicture"] != nil) {
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [imgAvatar.layer setMasksToBounds:YES];
    imgAvatar.layer.cornerRadius= 22;
    imgAvatar.layer.borderWidth =0;
    
    
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    if (obj.isNonMember) {
        imgAvatar.image =[UIImage imageNamed:@"profile"];
    }
    else
    {
    [imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    }
    // neu la owner
    owner_id =(int)[[GroupEnterOBJ sharedInstance].dictionaryGroup[@"owner"][@"id"] intValue];

    if ([dic[@"id"] integerValue] ==owner_id ) {
        imgCrown.hidden=NO;
        delButton.hidden=YES;
        iconDelete.hidden=YES;
    }
    else
    {
        imgCrown.hidden=YES;
        delButton.hidden=NO;
        iconDelete.hidden=NO;

    }
    [self checkInvite:obj.isUserInvite];

    
    NSMutableDictionary *dic1 = [@{@"name":str(strJe_participe),
                                   @"image":@"ic_participate"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strJe_ne_sais_pas),
                                   @"image":@"ic_pre_participate"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strJe_ne_participe_pas),
                                   @"image":@"ic_not_participate"} copy];
    
    
    arrData =  [@[dic1,dic2,dic3] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    numberOption =[obj.dictionaryMyKind[@"participation"] stringValue];
    
    str_Owner =dic[@"id"];
    self.mykindID = obj.strMyKindId;
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    delButton.backgroundColor =UIColorFromRGB(CHASSES_CANCEL);

    
}
#pragma mark - callback
-(void)setCallback:(Callback)callback
{
    _callback=callback;
}
-(void)doCallback:(Callback)callback
{
    self.callback=callback;
}
#pragma mark - API
- (void) ParticipeAction
{
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    [COMMON addLoading:self];
    NSDictionary * postDict = [NSDictionary dictionaryWithObjectsAndKeys:numberOption,@"participation", nil];
    
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        [serviceAPI putLoungessuscriberParticipateAction:self.mykindID user_id:str_Owner withParametersDic:postDict];

    }
    else
    {
        [serviceAPI putLoungesuscriberNonmembersParticipion:self.mykindID strID:str_Owner withParametersDic:postDict];

    }

    serviceAPI.onComplete =^(id response, int errCode)
    {
        [self.tableControl reloadData];
        [COMMON removeProgressLoading];
        ChasseSettingMembresComment  *viewController1=[[ChasseSettingMembresComment alloc]initWithNibName:@"ChasseSettingMembresComment" bundle:nil];
        viewController1.mParent = self.mParent;

        [self pushVC:viewController1 animate:YES];
        
    };
    
}
#pragma mark - table delegate
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dicPar = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dicPar[@"image"]];
    
    //FONT
    cell.label1.text = dicPar[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    //
    int option =4;
    switch ([numberOption integerValue]) {
        case 0:
        {
            option=2;
            
        }
            break;
            
        case 1:
        {
            option=0;
            
        }
            break;
        case 2:
        {
            option=1;
        }
            break;
        default:
            break;
    }
    if(option == indexPath.row)
    {
        cell.imageCrown.hidden=NO;
        cell.imageCrown.image=[UIImage imageNamed:@"ic_chasse_sucess"];
    }
    else
    {
        cell.imageCrown.hidden=YES;
    }
    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.constraint_image_width.constant =40;
    cell.constraint_image_height.constant =40;
    cell.constraint_control_width.constant = 20;
    cell.constraintRight.constant = 5;
    btn.frame = CGRectMake(0, 0, btn.image.size.width , btn.image.size.height);
    [cell.viewControl addSubview:btn];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            numberOption=@"1";
            
        }
            break;
            
        case 1:
        {
            numberOption=@"2";
            
        }
            break;
        case 2:
        {
            numberOption=@"0";
            
        }
            break;
        default:
            break;
    }
    ChasseSettingMembresComment  *viewController1=[[ChasseSettingMembresComment alloc]initWithNibName:@"ChasseSettingMembresComment" bundle:nil];
    viewController1.mParent = self.mParent;
    viewController1.numberOption =numberOption;
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

//    [self ParticipeAction];
}
//https://www.naturapass.com/api/v1/lounges/3530/joins/313

//https://www.naturapass.com/api/v1/lounges/313/notmembers/119/participation
-(IBAction)deleteAction:(id)sender
{
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
        NSString *strID = dic[@"id"];
        [serviceAPI deleteLoungeSubcriberJoin:self.mykindID  members_id:strID];
    }
    else
    {
        dic =obj.dictionaryMyKind;
        NSString *strID = dic[@"id"];
        [serviceAPI deleteNonMembers:self.mykindID nonmembers_id:strID];
    }
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if(response[@"success"]){
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (int i=0; i < controllerArray.count; i++) {
                if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                    
                    [self.navigationController popToViewController:self.mParent animated:YES];
                    return;
                }
            }
            
            [self doRemoveObservation];

            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    };
}
-(IBAction)onNext:(id)sender
{
    ChasseSettingMembresComment  *viewController1=[[ChasseSettingMembresComment alloc]initWithNibName:@"ChasseSettingMembresComment" bundle:nil];
    viewController1.numberOption =numberOption;
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
}
-(IBAction)validerAction:(id)sender
{

    NSString *groupid= [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    ChassesMemberOBJ *obj = [ChassesMemberOBJ sharedInstance];
    NSString *UserId= @"";//;dic[@"user"][@"id"];
    if ([obj.dictionaryMyKind[@"user"] isKindOfClass:[NSDictionary class]]) {
        dic =obj.dictionaryMyKind[@"user"];
        UserId = dic[@"id"];
    }
    else
    {
        dic =obj.dictionaryMyKind;
        UserId = dic[@"id"];
    }
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE mykind_id:groupid strUserID:UserId ];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if ([response[@"subscriber"] isKindOfClass:[NSDictionary class]]) {
            [ChassesMemberOBJ sharedInstance].isUserInvite= NO;
            [self checkInvite:NO];

        }
    };
}
-(void)checkInvite:(BOOL)isInvite
{
    // neu la owner
    if (isInvite) {
        self.tableControl.hidden=YES;
        validerButton.hidden =NO;
        iconValider.hidden =NO;
        contraintHeightTable.constant =70;
        iconSuivant.hidden=YES;
        self.btnSuivant.hidden =YES;

    }
    else
    {
        self.tableControl.hidden=NO;
        validerButton.hidden =YES;
        iconValider.hidden =YES;
        contraintHeightTable.constant =170;
        iconSuivant.hidden=NO;
        self.btnSuivant.hidden =NO;
        
    }
}
-(IBAction)jumProfileInfoAction:(id)sender
{
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    if ([dic[@"id"] integerValue] ==[my_id integerValue]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic: dic];
    
    [self pushVC:friend animate:YES expectTarget:ISMUR];
}
@end
