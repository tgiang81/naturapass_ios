//
//  TagView31.m
//  Naturapass
//
//  Created by Giang on 3/2/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "TagView31.h"
#import "Define.h"
@interface TagView31 ()
{
    __weak IBOutlet UITableView *tblTag31;
    NSInteger _index;
}
@property (nonatomic) NSMutableArray *listVarTag31;

@property(nonatomic, strong) NSString *strSelected;
@property (nonatomic) NSDictionary *dicCheck;

@end

@implementation TagView31

-(void) showViewWithData:(NSArray*)arrContent withstrSelected:(NSString*)strSelected withIndex:(NSInteger)index
{
    _listVarTag31 = [NSMutableArray new];//arrContent;
    [_listVarTag31 addObject:@{@"name":str(strChoisir_une_fiche)}];
    [_listVarTag31 addObjectsFromArray:arrContent];
    self.strSelected = strSelected;
    _index = index;
    [tblTag31 reloadData];
    int iCount =0;
    int min = 2;
    int max = 5;
    if (_listVarTag31.count<min) {
        iCount= min;
    }
    else if (_listVarTag31.count>max)
    {
        iCount =max;
    }
    else
    {
        iCount = (int)_listVarTag31.count;
    }
    self.constraintHeightViewBorder.constant = iCount*40+10;
}


-(void)awakeFromNib{
    [super awakeFromNib];

    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    [tblTag31 registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
}


#pragma callback

#pragma callback

-(void)setCallback:(callBackAlert)callback
{
    _CallBack=callback;
}
#pragma callback
-(void)doBlock:(callBackAlert ) cb
{
    self.CallBack = cb;
    
}

#pragma mark -action

-(void)showInVC:(UIViewController*)vc
{
}

-(IBAction)fnDismiss:(id)sender
{
    [self removeFromSuperview];
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _listVarTag31.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.CallBack) {
        if (indexPath.row==0) {
            self.CallBack(_index,@{@"isRemove":@1});
        }
        else
        {
            _dicCheck = _listVarTag31[indexPath.row];
            self.CallBack(_index,@{@"value":_dicCheck[@"id"],@"text":_dicCheck[@"name"]});
        }
    }
    [self removeFromSuperview];
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary*mDic = _listVarTag31[indexPath.row];
    
    //        NSString *name = self.filteredNames[indexPath.row];
    cell.textLabel.text = mDic[@"name"];
    
    //??
    if ([mDic[@"id"] intValue] == [self.strSelected intValue])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
    
}



@end
