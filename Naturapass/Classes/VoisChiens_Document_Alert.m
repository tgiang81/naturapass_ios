//
//  MyAlertView.m
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "VoisChiens_Document_Alert.h"

#import "Config.h"
#import "CellSimple.h"
#import "AppCommon.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface VoisChiens_Document_Alert ()
{
    NSArray *arrTitle;
}
@end

@implementation VoisChiens_Document_Alert

-(void)awakeFromNib{
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];

    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [popTable registerNib:[UINib nibWithNibName:@"CellSimple" bundle:nil] forCellReuseIdentifier:identifierSection1];
    arrTitle =@[str(strPRENDRE_UNE_PHOTO),str(strPRENDRE_UNE_VIDEO),str(strCHOISIR_DANS_MA_BIBLIOTHEQUE),str(strAnuler)];
    self.constraintHeightViewBorder.constant = arrTitle.count*54;
    
    popTable.estimatedRowHeight = 50;
    popTable.rowHeight = UITableViewAutomaticDimension;

}

-(void) doShow :(UIViewController*)parent
{
    [parent.view addSubview:self];
    
    CGRect r = parent.view.frame;
    
    self.frame = CGRectMake(0, 0, r.size.width, r.size.height);

    [popTable reloadData];
}

#pragma callback
-(void)setCallback:(MyAlertViewCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(MyAlertViewCbk ) cb
{
    self.callback = cb;
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrTitle count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellSimple *cell = nil;
    
    cell = (CellSimple *)[popTable dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSString *strTitle = arrTitle[indexPath.row];
    
    //FONT
    cell.label1.text = strTitle;
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(13)];
    [cell layoutIfNeeded];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self removeFromSuperview];
    if (_callback) {
        _callback(indexPath.row);
    }
}
@end
