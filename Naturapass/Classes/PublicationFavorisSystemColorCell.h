//
//  TheProjectCell.h
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TreeViewNode.h"
#import "FavorisSystemBaseCell.h"
@interface PublicationFavorisSystemColorCell : FavorisSystemBaseCell

@end
