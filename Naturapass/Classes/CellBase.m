//
//  CellBase.m
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CellBase.h"
#import "Define.h"
#import "AppCommon.h"
@implementation CellBase

-(void) awakeFromNib
{
    [super awakeFromNib];
    
    self.imgBackGroundSetting.hidden=YES;
    self.imgBgMore.hidden=YES;
    [COMMON listSubviewsOfView:self];
    
}

-(void) createMenuList:(NSArray*)arr
{
    
    if (self.viewMenu) {
        [self hide];
    }
    
    self.viewMenu = nil;
    
    if (arr.count==1) {
        self.viewMenu = [self viewWithTag:POP_MENU_VIEW_TAG1];
        UIButton *item1 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG1];
        [item1  setTitle:arr[0] forState:UIControlStateNormal];
        [item1 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
    }else{
        self.viewMenu = [self viewWithTag:POP_MENU_VIEW_TAG2];
        UIButton *item1 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG1];
        UIButton *item2 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG2];
        
        [item1  setTitle:arr[0] forState:UIControlStateNormal];
        [item2  setTitle:arr[1] forState:UIControlStateNormal];
        
        [item1 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [item2 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        
    }

    [self.btnHideMenu addTarget:self action:@selector(hideMenu) forControlEvents:UIControlEventTouchUpInside];
}
-(void) menuListwithPublication:(NSDictionary *)publicationDic
{

        if (self.viewMenu) {
            [self hide];
        }
        self.viewMenu = nil;
        self.viewMenu = [self viewWithTag:POP_MENU_VIEW_TAG2];
        UIButton *item1 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG1];
        UIButton *item2 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG2];
        UIButton *item3 = (UIButton*)[self.viewMenu viewWithTag:MENU_ITEM_TAG3];
        [item3  setTitle:@"S'y rendre..." forState:UIControlStateNormal];
        [item2 setAttributedTitle:@"" forState:UIControlStateNormal];

        if ([publicationDic[@"geolocation"] isKindOfClass: [NSDictionary class] ]) {
            self.viewGeoMore.hidden = NO;
            self.constraintHeightViewGeoMore.constant = 41;
        }else{
            self.viewGeoMore.hidden = YES;
            self.constraintHeightViewGeoMore.constant = 0;
        }
        if ( [publicationDic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
            [item1  setTitle:str(strModifier) forState:UIControlStateNormal];
            [item2  setTitle:str(strSupprimer) forState:UIControlStateNormal];
            NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:str(strSupprimer)];
            [attString addAttribute:NSForegroundColorAttributeName
                              value:[UIColor whiteColor]
                              range:NSMakeRange(0, str(strSupprimer).length)];
            [item2 setAttributedTitle:attString forState:UIControlStateNormal];
    
        }else{
            [item1  setTitle:str(strSignaler) forState:UIControlStateNormal];
            
            NSString *strUserName=[NSString stringWithFormat:@" %@ %@", CHECKSTRING(publicationDic[@"owner"][@"firstname"]), CHECKSTRING(publicationDic[@"owner"][@"lastname"]) ];

            NSMutableAttributedString *attString = [NSMutableAttributedString new];
            
            NSMutableAttributedString *mMenu = [[NSMutableAttributedString alloc] initWithString:str(strMenu_item_Blacklister)];
            NSMutableAttributedString *mName = [[NSMutableAttributedString alloc] initWithString:strUserName];
            [attString appendAttributedString:mMenu];
            [attString appendAttributedString:mName];
            [attString addAttribute:NSFontAttributeName
                           value:[UIFont boldSystemFontOfSize:14.0f]
                           range:NSMakeRange(str(strMenu_item_Blacklister).length, strUserName.length)];
            [attString addAttribute:NSForegroundColorAttributeName
                           value:[UIColor whiteColor]
                           range:NSMakeRange(0, str(strMenu_item_Blacklister).length+strUserName.length)];
            [item2 setAttributedTitle:attString forState:UIControlStateNormal];
        }

    
        [item1 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [item2 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [item3 addTarget:self action:@selector(dismissMenuItem:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnHideMenu addTarget:self action:@selector(hideMenu) forControlEvents:UIControlEventTouchUpInside];
        item1.backgroundColor = [UIColor clearColor];
        item2.backgroundColor = [UIColor clearColor];
        item3.backgroundColor = [UIColor clearColor];
        self.viewMenu.layer.masksToBounds = YES;
        self.viewMenu.layer.cornerRadius = 7;
}
-(void) show
{
    self.viewMenu.hidden=NO;
    self.imgBackGroundSetting.hidden=NO;
    self.imgSettingNormal.hidden=YES;
    self.imgSettingSelected.hidden=NO;
    self.btnHideMenu.hidden=NO;
    [self.btnHideMenu bringSubviewToFront:self];
    self.imgBgMore.hidden=NO;
}
-(void) hide
{
    self.imgBackGroundSetting.hidden=YES;
    self.imgSettingNormal.hidden=NO;
    self.imgSettingSelected.hidden=YES;
    self.viewMenu.hidden=YES;
    self.btnHideMenu.hidden=YES;
    self.imgBgMore.hidden=YES;

}
-(IBAction)dismissMenuItem:(id)sender
{
    int index = (int)[sender tag];
    if (_CallBackGroup) {
        if (index==11) {
            _CallBackGroup(1);
            
        }
        else if(index ==10)
        {
            _CallBackGroup(0);
        }
        else if(index ==12)
        {
            _CallBackGroup(2);
        }
    }
    [self hide];
}
-(void)hideMenu
{
    [self hide];
    
}
- (void)prepareForReuse {
    
    //    [self.menuPopover dismissMenuPopover];
    [self hide];
    [super prepareForReuse];
}

#pragma mark -
#pragma mark MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    //    NSString *title = [NSString stringWithFormat:@"%@ selected.",[self.menuItems objectAtIndex:selectedIndex]];
    //
    //    NSLog(@"%@",title);
    if (_CallBackGroup) {
        _CallBackGroup(selectedIndex);
    }
}
@end
