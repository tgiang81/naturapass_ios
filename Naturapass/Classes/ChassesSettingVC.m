//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesSettingVC.h"
#import "ChassesParameter_Notification_Email.h"
#import "ChassesParameter_Notification_Smartphones.h"
#import "CellKind2.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChassesSettingVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation ChassesSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text =  str(strParametresUtilisateurs);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name":str(strNotifications_email),
                                   @"image":@"ic_chasse_setting_email"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strNotifications_smartphones),
                                   @"image":@"ic_chasse_settingsmartphone"} copy];

    arrData =  [@[dic1,dic2] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];

    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];

    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_cell" ]];

    cell.constraint_control_width.constant = 20;
    cell.constraintRight.constant = 5;
    btn.frame = CGRectMake(0, 0, btn.image.size.width , btn.image.size.height);
    [cell.viewControl addSubview:btn];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            ChassesParameter_Notification_Email *viewController1 = [[ChassesParameter_Notification_Email alloc] initWithNibName:@"ChassesParameter_Notification_Email" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 1:
        {
            ChassesParameter_Notification_Smartphones *viewController1 = [[ChassesParameter_Notification_Smartphones alloc] initWithNibName:@"ChassesParameter_Notification_Smartphones" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
        default:
            break;
    }
}
@end
