//
//  Parameter_MesFavoris.m
//  Naturapass
//
//  Created by Giang on 2/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_MesFavoris.h"
#import "Parameter_Favorites.h"
#import "Parameter_MesPublicationFav.h"
#import "CellKind18.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_MesFavoris ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation Parameter_MesFavoris

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMES_FAVORIS);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name": str(strMes_adresse_favorites),
                                   @"image":@"param_ic_add_fav"} copy];
    NSMutableDictionary *dic2 = [@{@"name":str(strMes_publications_favorites),
                                   @"image":@"param_ic_pub_fav"} copy];
    
    arrData =  [@[dic1,dic2] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind18" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind18 *cell = nil;
    
    cell = (CellKind18 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            Parameter_Favorites *viewController1 = [[Parameter_Favorites alloc] initWithNibName:@"Parameter_Favorites" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
        case 1:
        {
            Parameter_MesPublicationFav *viewController1 = [[Parameter_MesPublicationFav alloc] initWithNibName:@"Parameter_MesPublicationFav" bundle:nil];
            [self pushVC:viewController1 animate:YES];
            
        }
            break;
            
        default:
            break;
    }
}

@end
