//
//  ChassesCreateBaseVC.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreateBaseVC.h"
#import "SubNavigationPRE_ANNULER.h"
#import "AlertVC.h"
#import "ChassesMurVC.h"
#import "chassesEditListVC.h"
#import "ChassesCreate_Step11.h"

@interface ChassesCreateBaseVC ()

@end

@implementation ChassesCreateBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strCHANTIERS)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);
    
    
    [self addSubNav:@"SubNavigationPRE_ANNULER"];
    
    
    
    //
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(CHASSES_MAIN_BAR_COLOR) ];
    SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
    
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
    
    btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    
    btn2.backgroundColor = UIColorFromRGB(CHASSES_CANCEL);

}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];

    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
        case 1://ANNULER
        {
            NSString *str=str(strAAgenda);
            NSString *strmessage =@"";
            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                strmessage = [NSString stringWithFormat:str(strEtesVousSurDeVouloiranuilervotresaisie)];
            }
            else
            {
                if (self.needChangeMessageAlert) {
                    strmessage = [NSString stringWithFormat:str(strMessage32),str];
                    
                }else{
                    
                    strmessage = [NSString stringWithFormat:str(strMessage10),str];
                }

                
            }
            AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"Annuler %@",str] message:strmessage cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
            
            [vc doBlock:^(NSInteger index, NSString *str) {
                if (index==0) {
                    // NON
                }
                else if(index==1)
                {
                    //OUI
                    if (self.needChangeMessageAlert) {
                        ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
                        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];

                    }else{
                        NSArray * controllerArray = [[self navigationController] viewControllers];
                        
                        for (int i=0; i < controllerArray.count; i++) {
                            if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                                
                                [self.navigationController popToViewController:self.mParent animated:YES];
                                
                                return;
                            }
                        }
                        
                        [self doRemoveObservation];

                        [self.navigationController popToRootViewControllerAnimated:YES];

                    }

                }
            }];
            [vc showAlert];
        }
            break;
            
        default:
            break;
    }
}

@end
