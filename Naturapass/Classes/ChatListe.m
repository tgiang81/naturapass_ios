//
//  ChatListe.m
//  Naturapass
//
//  Created by Giang on 9/21/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatListe.h"
#import "CellKind11.h"
#import "ChatVC.h"
#import "ChatNouveauVC.h"
#import "TypeCellFooterDiscussion.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChatListeViewFull.h"

static NSString *identifierSection0 = @"MyTableViewCell0";
static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";

static NSString *identifierFooter = @"footer";


@interface ChatListe ()
{
    NSMutableArray *arrDataDiscussions;
    NSMutableArray *arrDataDiscussions_groupe;
    NSMutableArray *arrDataDiscussions_chasses;
    
    NSArray * arrCategories;
    NSArray * arrTitleFooter;
    IBOutlet UILabel *lbTitle;
    
}
@end

@implementation ChatListe

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strDISCUSSIONS);
    arrDataDiscussions = [NSMutableArray new];
    arrDataDiscussions_groupe = [NSMutableArray new];
    arrDataDiscussions_chasses = [NSMutableArray new];
    
    /*
     1. Khi current user chat với A (2 người) thì luôn hiển thị avatar và tên của A.
     2. Khi current user chat với 2 người trở lên (>=3 người)
     + Khi current user là latest message => Tên sẽ hiển thị tất cả participants và hiển thị ảnh đầu tiên của list participants.
     + trường hợp khác thì hiển thị tên của người chat cuối + tổng số participants.
     C et un autre membre
     C et 3 membres
     
     //Onwer is latest user who post the message...

     */
    
    
    //Refresh/loadmore
    [self initRefreshControl];
    
    arrCategories = @[str(strEMPTY), str(strDiscussionsGroupes), str(strDiscussionsChasses)];
    arrTitleFooter = @[str(strToutesLesDiscussions), str(strToutesLesDiscussionsDeGroupes),str(strToutesLesDiscussionsDeChasses)];

    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind11" bundle:nil] forCellReuseIdentifier:identifierSection0];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind11" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind11" bundle:nil] forCellReuseIdentifier:identifierSection2];
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellFooterDiscussion" bundle:nil] forCellReuseIdentifier:identifierFooter];

    [self loadCache];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //call when back from chat
    if ([COMMON isReachable]) {
        [self startRefreshControl];
    }
}
-(void)loadCache
{
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHATLISTE_SAVE)];
    NSDictionary *dicTmp = [NSDictionary dictionaryWithContentsOfFile:strPath];
    if (dicTmp.count > 0) {
        //ref
            [arrDataDiscussions removeAllObjects];
            [arrDataDiscussions_chasses removeAllObjects];
            [arrDataDiscussions_groupe removeAllObjects];
        //List message
        if (dicTmp && [dicTmp[@"messages"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = dicTmp[@"messages"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions addObject:dic];
            }
        }
        if (dicTmp && [dicTmp[@"messagesHunt"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = dicTmp[@"messagesHunt"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions_chasses addObject:dic];
            }
        }
        if (dicTmp && [dicTmp[@"messagesGroup"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = dicTmp[@"messagesGroup"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions_groupe addObject:dic];
            }
        }
    }

}
#pragma mark - REFRESH/LOAD MORE
//overwrite
- (void)insertRowAtTop {
    [self getAllConversationAction:YES];
}
-(void)insertRowAtBottom
{
    [self getAllConversationAction:YES];
}

-(void)getAllConversationAction:(BOOL) isfresh
{

    NSString *strOffset = @"0";
    NSString *strOffset_Hunt = @"0";
    NSString *strOffset_Group = @"0";
    if (arrDataDiscussions.count>0 && isfresh ==NO) {
        strOffset =[NSString stringWithFormat:@"%d",(int)arrDataDiscussions.count];
    }
    if (arrDataDiscussions_chasses.count>0 && isfresh ==NO) {
        strOffset_Hunt = [NSString stringWithFormat:@"%d",(int)arrDataDiscussions_chasses.count];
    }
    if (arrDataDiscussions_groupe.count && isfresh ==NO) {
        strOffset_Group = [NSString stringWithFormat:@"%d",(int)arrDataDiscussions_groupe.count];
    }
    
//    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnGET_ALL_CONVERSATIONS:@"10" offset:strOffset limit_hunt:@"10" offset_hunt:strOffset_Hunt limit_group:@"10" offset_group:strOffset_Group];
    __weak typeof(self) wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//        [COMMON removeProgressLoading];
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        if (!response) {
            return ;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        //ref
        if (isfresh) {
            [arrDataDiscussions removeAllObjects];
            [arrDataDiscussions_chasses removeAllObjects];
            [arrDataDiscussions_groupe removeAllObjects];
        }
        //List message
        if (response && [response[@"messages"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messages"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions addObject:dic];
            }
        }
        if (response && [response[@"messagesHunt"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messagesHunt"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions_chasses addObject:dic];
            }
        }
        if (response && [response[@"messagesGroup"] isKindOfClass: [NSArray class]]) {
            NSArray *arr = response[@"messagesGroup"];
            
            for (NSDictionary*dic in arr) {
                [arrDataDiscussions_groupe addObject:dic];
            }
        }
        if ([response isKindOfClass:[NSDictionary class]]) {
            // Path to save array data
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHATLISTE_SAVE)   ];
            
//            NSLog(@">>> %@", strPath);
            // Write array
            NSDictionary *dicCache = response;
            [dicCache writeToFile:strPath atomically:YES];
        }

        [self.tableControl reloadData];


    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            if(arrDataDiscussions.count == 0) return 0;
            return arrDataDiscussions.count +1;
        }
            break;
        case 1:
        {
            if(arrDataDiscussions_groupe.count == 0) return 0;
            return arrDataDiscussions_groupe.count+1;
        }
            break;
        case 2:
        {
            if(arrDataDiscussions_chasses.count == 0) return 0;
            return arrDataDiscussions_chasses.count+1;
        }
            break;
            
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //footer
    if ((indexPath.section ==0 && indexPath.row >=arrDataDiscussions.count) ||
       (indexPath.section ==1 && indexPath.row >=arrDataDiscussions_groupe.count)||
       (indexPath.section ==2 && indexPath.row >=arrDataDiscussions_chasses.count)) {
        
        NSString *strTitle =arrTitleFooter[indexPath.section];
        TypeCellFooterDiscussion *cell = (TypeCellFooterDiscussion *)[self.tableControl dequeueReusableCellWithIdentifier:identifierFooter];
        cell.label1.text=strTitle;
        cell.view1.backgroundColor =[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return  cell;
    }
    CellKind11 *cell = nil;
    NSDictionary *dic = nil;
    
    switch (indexPath.section) {
        case 0:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
            dic = arrDataDiscussions[indexPath.row] ;
            if (dic)
            {
                
                
                //RULE:
                /*
                 1. Khi current user chat với A (2 người) thì luôn hiển thị avatar và tên của A.
                 2. Khi current user chat với 2 người trở lên (>=3 người)
                 
                 + Khi current user là latest message => Tên sẽ hiển thị tất cả participants và hiển thị ảnh đầu tiên của list participants.
                 
                 + trường hợp khác thì hiển thị tên của người chat cuối + tổng số participants.
                 C et un autre membre
                 C et 3 membres
                 */
                
                NSString* imgUrl;
                NSString * compountNames;
                NSMutableArray*mutArray = [NSMutableArray new];
                
                
                NSArray *arrPartics =dic[@"conversation"][@"participants"];
                
                //>2
                //I am owner of the message
                //        C et un autre membre
                //        C et 3 membres
                
                
                //        if ((currentUser = latestUser) || listparticipants = 1) {
                //            // Display all name in participants.
                //        } else {
                //            totalParticipants = participants - 1; // except latestUser;
                //            if (totalParticipants>1) {
                //                // Display "latestuser and totalParticipants members.
                //            } else {
                //                // Display "latestuser and 1 member.
                //            }
                //        }
                
                
                if (   ([dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) ||
                    (int)arrPartics.count == 1)
                {
                    // Display all name in participants.
                    NSArray *arrParts =  dic[@"conversation"][@"participants"];
                    
                    if (arrParts.count > 0) {
                        NSDictionary*firstDic = dic[@"conversation"][@"participants"][0];
                        
                        if (firstDic[@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,firstDic[@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,firstDic [@"photo"]];
                        }
                        
                        
                        for (NSDictionary*Dic2 in dic[@"conversation"][@"participants"]) {
                            [mutArray addObject:Dic2[@"firstname"]];
                        }
                        
                        compountNames = [mutArray componentsJoinedByString:@", "];
                        
                    }
                }else{
                    int totalParticipants = (int) arrPartics.count - 1; // except latestUser;
                    
                    if (totalParticipants > 1) {
                        // Display "latestuser and totalParticipants members.
                        if (dic[@"owner"][@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
                        }
                        
                        compountNames = [NSString stringWithFormat:@"%@ et %d membres", dic[@"owner"][@"firstname"], (int)arrPartics.count-1];
                    } else {
                        // Display "latestuser and 1 member.
                        if (dic[@"owner"][@"profilepicture"] != nil) {
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                        }else{
                            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
                        }
                        
                        compountNames = [NSString stringWithFormat:@"%@ %@", dic[@"owner"][@"firstname"],str(strEt_un_autre_membre)];
                    }
                    
                }
                
                
                [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
                //FONT
                cell.label1.text = compountNames;
                
                NSString *valueEmoj = [dic[@"content"] emo_emojiString];

                cell.label2.text = valueEmoj;
                //timer
                NSDateFormatter *inputFormatter;
                
                NSDateFormatter *outputFormatterBis;
                
                //dd mmm .. this year...
                //hh mm.. today
                
                inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
                NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"updated"]];
                NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
                
                outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
                
                
                //"date": "2015-09-21T00:00:00+0200"
                cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
            }
            
            //set color
            if ([dic[@"unreadCount"] intValue] >0) {
                [cell.contentView setBackgroundColor:UIColorFromRGB(DISCUSSION_CELL_ACTIVE_COLOR)];
            }else{
                [cell.contentView setBackgroundColor:UIColorFromRGB(DISCUSSION_CELL_WHITE_COLOR)];
            }

        }
            break;
            
            
            /*
             {
             content = czv;
             created = "2015-10-22T10:06:29+02:00";
             group =     {
             }
             
             */
        case 1:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
            dic = arrDataDiscussions_groupe[indexPath.row] ;
            
            NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];

            [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
            //FONT
            cell.label1.text = dic[@"group"][@"name"];
            
            NSString *valueEmoj = [dic[@"content"] emo_emojiString];

            cell.label2.text = valueEmoj;
            //timer
            NSDateFormatter *inputFormatter;
            
            NSDateFormatter *outputFormatterBis;
            
            //dd mmm .. this year...
            //hh mm.. today
            
            inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"created"]];
            NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
            
            outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
            
            
            //"date": "2015-09-21T00:00:00+0200"
            cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
        }
            break;
            
        case 2:
        {
            cell = (CellKind11 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
            dic = arrDataDiscussions_chasses[indexPath.row] ;
            NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic [@"owner"][@"photo"]];
            
            [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
            //FONT
            cell.label1.text = dic[@"hunt"][@"name"];
            
            NSString *valueEmoj = [dic[@"content"] emo_emojiString];

            cell.label2.text = valueEmoj;
            
            //timer
            NSDateFormatter *inputFormatter;
            
            NSDateFormatter *outputFormatterBis;
            
            //dd mmm .. this year...
            //hh mm.. today
            
            inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
            NSString *inputDateStr=[NSString stringWithFormat:@"%@",dic[@"created"]];
            NSDate * inputDates = [ inputFormatter dateFromString:inputDateStr ];
            
            outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
            
            
            //"date": "2015-09-21T00:00:00+0200"
            cell.label3.text   = [ outputFormatterBis stringFromDate:inputDates ];
        }
            break;
            
        default:
            break;
    }
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

//append a cell -> act like button

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //footer
    if ((indexPath.section ==0 && indexPath.row ==arrDataDiscussions.count)||
        (indexPath.section ==1 && indexPath.row ==arrDataDiscussions_groupe.count)||
        (indexPath.section ==2 && indexPath.row ==arrDataDiscussions_chasses.count)) {
        
        ChatListeViewFull *vc = [[ChatListeViewFull alloc] initWithNibName:@"ChatListeViewFull" bundle:nil];
        vc.conversation_stype =(int)indexPath.section;
        [self pushVC:vc animate:NO expectTarget:ISDISCUSS];
        return  ;
    }
    ChatVC *viewController1 = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
    viewController1.mParent = self;

    //
    //cell
    //Tick read!!!
    /*
     318:      * JSON lié:
    319:      * {
        320:      *      "conversation": {
            321:      *          "id": null,
            322:      *          "messageId": 1
            323:      *      }
        324:      *
        325:      * }
     */
    [[GroupEnterOBJ sharedInstance] resetParams];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];


    switch (indexPath.section) {
        case 0:
        {
            NSDictionary*dic =    arrDataDiscussions[indexPath.row];
            [serviceObj fnPUT_READ_MESSAGE:@{@"conversation":@{@"id":dic[@"conversation"][@"id"]}}];
            viewController1.myConversationID = dic[@"conversation"][@"id"];
            viewController1.expectTarget =ISDISCUSS;
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                //request update counter...
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];                
            };
            [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:YES completion:^ {
                NSLog(@"COMPLETED");
            }];
        }
            break;
        case 1:
        {
            viewController1.expectTarget =ISGROUP;

            NSDictionary*dic = arrDataDiscussions_groupe[indexPath.row];
            [COMMON addLoading:self];
            [serviceObj getItemWithKind:MYGROUP myid: dic[@"group"][@"id"]];
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [COMMON removeProgressLoading];
                if (response[@"group"]) {
                    NSDictionary *dic = response[@"group"];
                    [[GroupEnterOBJ sharedInstance] resetParams];
                    [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
                    
                    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:YES completion:^ {
                        NSLog(@"COMPLETED");
                    }];
                }
            };
        }
            break;
        case 2:
        {
            viewController1.expectTarget =ISLOUNGE;
            NSDictionary*dic =    arrDataDiscussions_chasses[indexPath.row];
            [serviceObj getItemWithKind:MYCHASSE myid: dic[@"hunt"][@"id"]];
            [COMMON addLoading:self];
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [COMMON removeProgressLoading];
                if (response[@"lounge"]) {
                    NSDictionary *dic = response[@"lounge"];
                    [[GroupEnterOBJ sharedInstance] resetParams];
                    [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
                    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:YES completion:^ {
                        NSLog(@"COMPLETED");
                    }];
                }
            };
        }
            break;
        default:
            break;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 1:
        {
            if(arrDataDiscussions_groupe.count == 0) return 0;
        }
            break;
        case 2:
        {
            if(arrDataDiscussions_chasses.count == 0) return 0;
        }
            break;
            
        default:
            return 0;
            break;
    }
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return nil;
    }
    
    UIView* sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    sectionView.backgroundColor= UIColorFromRGB(MAIN_COLOR);
    UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 320, 29)];
    [headerLabel setTextColor:[UIColor blackColor]];
    headerLabel.numberOfLines=2;
    [headerLabel setBackgroundColor:[UIColor clearColor]];
    [headerLabel setFont:[UIFont boldSystemFontOfSize:17]];
    
    headerLabel.text= arrCategories[section];
    
    [sectionView addSubview:headerLabel];
    
    return  sectionView;
    
}
-(IBAction)fnCreateNewChasses:(id)sender
{
    ChatNouveauVC *viewController1 = [[ChatNouveauVC alloc] initWithNibName:@"ChatNouveauVC" bundle:nil];
    [self pushVC:viewController1 animate:NO expectTarget:ISDISCUSS];
}

@end
