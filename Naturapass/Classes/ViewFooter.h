//
//  ViewFooter.h
//  DoTest
//
//  Created by GS-Soft on 6/6/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ returnHight)(int);

@interface ViewFooter : UIView
{
    NSMutableArray *arrFetchData;
}
@property (nonatomic,strong) IBOutlet UITableView *tableControl;
@property (nonatomic, strong)  returnHight block;
@property (nonatomic, assign) BOOL isObserving;

-(void) reloadWithData:(NSArray*)arr withBlock:(returnHight)bl;
-(void) releaseObserver;

@end
