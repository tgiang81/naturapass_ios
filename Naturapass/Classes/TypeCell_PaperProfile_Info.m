//
//  TypeCell_PaperProfile_Info.m
//  Naturapass
//
//  Created by Giang on 4/5/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "TypeCell_PaperProfile_Info.h"

@implementation TypeCell_PaperProfile_Info

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [COMMON listSubviewsOfView:self];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.title.preferredMaxLayoutWidth = CGRectGetWidth(self.title.frame);
    self.name.preferredMaxLayoutWidth = CGRectGetWidth(self.name.frame);
    self.text.preferredMaxLayoutWidth = CGRectGetWidth(self.text.frame);

}


@end
