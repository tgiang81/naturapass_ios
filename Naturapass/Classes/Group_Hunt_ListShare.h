//
//  Group_Hunt_ListShare.h
//  Naturapass
//
//  Created by Giang on 10/8/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Group_Hunt_ListShare : NSObject
{
    
}
+ (Group_Hunt_ListShare *) sharedInstance;
-(void) fnGetListGroup_Hunts;

@end
