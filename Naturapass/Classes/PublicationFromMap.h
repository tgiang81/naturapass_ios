//
//  PublicationFromMap.h
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "SettingActionView.h"
typedef void (^callBackWithDeleteItem) (NSDictionary *dicPublication);
@interface PublicationFromMap : BaseVC
{
    NSMutableArray                  *publicationArray;
    NSMutableArray                  *arrAgenda;
    NSMutableArray                  *arrGroup;
    NSMutableArray                  *arrSection3;

    int widthImage;
    SettingActionView *shareDetail;

}
@property (nonatomic, copy) callBackWithDeleteItem callbackWithDelItem;
-(void)setDataWithPublication:(NSArray*)arr;
@end
