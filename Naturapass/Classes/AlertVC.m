//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"

//Meteo
#import <AFNetworking/AFNetworking.h>

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "TypeCell10.h"
#import "MDCheckBox.h"
#import "AppCommon.h"
#import "MeoteoView.h"
#import "NSDate+Extensions.h"
#import "CommonHelper.h"
#import "PublicationOBJ.h"
//Meteo
#import <WebKit/WebKit.h>
#import "NSTViewWarmuper.h"

static NSString *cell10 =@"TypeCell10";
static NSString *cellID10 =@"cellID10";

#import "ColorCollectionCell.h"
#import "FileHelper.h"

@implementation AlertVC 
{
    UIToolbar                   *keyboardToolbar;
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;

    
    //Carte Type
    __weak IBOutlet UISwitch *btnSwitch;
    __weak IBOutlet UILabel *lblDesctiption;
    __weak IBOutlet UILabel *label1;
    __weak IBOutlet UITextField *tfLegend;

    int localTag;

    //Vent
    IBOutlet UILabel *desc;
    //Meteo
    IBOutlet UIButton *btnClose;
    IBOutlet UIButton *btnCurrentLocation;
    IBOutlet UIButton *btnCloseMap;
    IBOutlet UIImageView *imgCatCenter;

        NSMutableDictionary *mutDic;
        NSArray *meteoKeyArray;
    IBOutlet UITableView *tableControl;
    //color publication
        IBOutlet  UICollectionView  *myCollection;
        NSMutableArray * arrData;
        int indexSelect;
    __weak IBOutlet UIButton *btnOui;
    __weak IBOutlet UIButton *btnNon;
}
/*********************************************************/
- (instancetype)initStartTimeChangedStatisticAlert {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStartTimeChangedStatistic" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        [btnCancel addTarget:self action:@selector(fnCancel) forControlEvents:UIControlEventTouchUpInside ];        
    }
    return self;
}

- (instancetype)initConfirmEndRecordingPedometerAlert {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertConfirmEndStatistic" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        
        
        //        lbDistance;
        //        lbDenivele;
        //        lbDeniveleNegatif;
        //        lbVitesseMoyenne;
        
        //remember to reset it.
        NSDictionary*dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"statistic pedometer" ];
        
        if (dic) {
          lbDistance.text = dic[@"1"];
          lbDenivele.text = dic[@"2"];
          lbDeniveleNegatif.text = dic[@"3"];
          lbVitesseMoyenne.text = dic[@"4"];
        }

        [btnEndRecording addTarget:self action:@selector(fnEndRecording) forControlEvents:UIControlEventTouchUpInside ];
        
        [btnCancel addTarget:self action:@selector(fnCancel) forControlEvents:UIControlEventTouchUpInside ];
        
        ;
        
    }
    return self;
}

- (instancetype)initAlertConfirmRecordNewOrContinueLastTime {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertConfirmRecordNewOrContinueLastTime" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        
        //2 continue record
        [btnContinue addTarget:self action:@selector(fnContinueRecord) forControlEvents:UIControlEventTouchUpInside ];
        
        //3 record new
        [btnCancel addTarget:self action:@selector(fnCancel) forControlEvents:UIControlEventTouchUpInside ];
        
    }
    return self;
}

-(void) fnEndRecording
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0, @"");
    }
    
}

-(void) fnCancel
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1, @"");
    }
    
}
/*********************************************************/

/*********************************************************/

- (instancetype)initPedometerSTOP :(NSDictionary*) inDic {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStatisticStop" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        NSNumber *numDistance = inDic[@"distance"];
        NSNumber *numDenivele = inDic[@"positive"];
        NSNumber *numDeniveleNegatif = inDic[@"negative"];
        NSNumber *numVitesseMoyenne = inDic[@"average"];
        
        lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
        lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
        lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
        lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
        //Play + Annuler  ...start recording
        [btnContinue addTarget:self action:@selector(fnStartRecording) forControlEvents:UIControlEventTouchUpInside ];
    }
    return self;
}

- (instancetype)initPedometerSTOP2 :(NSDictionary*) inDic {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStatisticStop2" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        NSNumber *numDistance = inDic[@"distance"];
        NSNumber *numDenivele = inDic[@"positive"];
        NSNumber *numDeniveleNegatif = inDic[@"negative"];
        NSNumber *numVitesseMoyenne = inDic[@"average"];
        
        lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
        lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
        lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
        lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
        //Show new alert with:   Continue last record + Record new
        [btnContinue addTarget:self action:@selector(fnShowNextAlert) forControlEvents:UIControlEventTouchUpInside ];
    }
    return self;
}


- (instancetype)initPedometerRECORDING :(NSDictionary*) inDic {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStatisticRecording" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        NSNumber *numDistance = inDic[@"distance"];
        NSNumber *numDenivele = inDic[@"positive"];
        NSNumber *numDeniveleNegatif = inDic[@"negative"];
        NSNumber *numVitesseMoyenne = inDic[@"average"];
        
        lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
        lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
        lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
        lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
        
        [btnPedometer addTarget:self action:@selector(fnMeterAndPause) forControlEvents:UIControlEventTouchUpInside ];
        [btnTerminal addTarget:self action:@selector(fnTerminalRecord) forControlEvents:UIControlEventTouchUpInside ];
        
        [btnContinue addTarget:self action:@selector(fnContinueRecord) forControlEvents:UIControlEventTouchUpInside ];
        
        
    }
    return self;
}

- (instancetype)initPedometerPAUSE :(NSDictionary*) inDic {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStatisticPause" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        NSNumber *numDistance = inDic[@"distance"];
        NSNumber *numDenivele = inDic[@"positive"];
        NSNumber *numDeniveleNegatif = inDic[@"negative"];
        NSNumber *numVitesseMoyenne = inDic[@"average"];
        
        lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
        lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
        lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
        lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
        
        [btnPedometer addTarget:self action:@selector(fnMeterAndPause) forControlEvents:UIControlEventTouchUpInside ];
        [btnTerminal addTarget:self action:@selector(fnTerminalRecord) forControlEvents:UIControlEventTouchUpInside ];
        
        [btnContinue addTarget:self action:@selector(fnContinueRecord) forControlEvents:UIControlEventTouchUpInside ];
        
        
    }
    return self;
}


- (instancetype)initPedometerAlert :(NSDictionary*) inDic {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertStatistic" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        NSNumber *numDistance = inDic[@"distance"];
        NSNumber *numDenivele = inDic[@"positive"];
        NSNumber *numDeniveleNegatif = inDic[@"negative"];
        NSNumber *numVitesseMoyenne = inDic[@"average"];

        lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
        lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
        lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
        lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
        
        [btnPedometer addTarget:self action:@selector(fnMeterAndPause) forControlEvents:UIControlEventTouchUpInside ];
        [btnTerminal addTarget:self action:@selector(fnTerminalRecord) forControlEvents:UIControlEventTouchUpInside ];

        [btnContinue addTarget:self action:@selector(fnContinueRecord) forControlEvents:UIControlEventTouchUpInside ];

        
    }
    return self;
}
-(void)fnSetNameButtonPedometer:(NSString*)namePedometer wittNameButtonTerminal:(NSString*)nameTerminal withDesc:(NSString*)desc
{
    lbTitlePedometer.text = namePedometer;
    lbTitleTerminal.text = nameTerminal;
    _lbDescriptions.text = desc;
}
-(void) fnMeterAndPause
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0, @"");
    }

}

-(void) fnTerminalRecord
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1, @"");
    }

}

-(void) fnContinueRecord
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(2, @"");
    }
    
}

-(void) fnStartRecording
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(3, @"");
    }
    
}

-(void) fnShowNextAlert
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1, @"");
    }
    
}


/*********************************************************/


- (void)resignKeyboard:(id)sender
{
    [tvInputMessage resignFirstResponder];
}

- (instancetype)initAlertSignalez {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSignalez" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {

        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 20.0;
        
        tvInputMessage.layer.masksToBounds = YES;
        tvInputMessage.layer.cornerRadius = 20.0;
        tvInputMessage.placeholder = @"Entrez un message...";
        [tvInputMessage becomeFirstResponder];

        
        if (keyboardToolbar == nil)
        {
            CGRect screenBounds = [[UIScreen mainScreen] bounds];

            keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, screenBounds.size.width, 38.0f)];
            keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
            
            spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                             target:self
                                                                             action:nil];
            
            doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strOK)
                                                                style:UIBarButtonItemStyleDone
                                                               target:self
                                                               action:@selector(resignKeyboard:)];
            
            [keyboardToolbar setItems:[NSArray arrayWithObjects:
                                            spaceBarItem,
                                            doneBarItem, nil]];
        }
        
        [tvInputMessage setInputAccessoryView: keyboardToolbar];

    }
    return self;
}

- (instancetype)initConfirmLiveHuntAlert {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertConfirmLiveHunt" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        _lbDescriptions.text = @"VOUS N'AVEZ ACTUELLEMENT AUCUN NATURALIVE EN COURS.\nVOUS POUVEZ CRÉER UN NATURALIVE OU EN REJOINDRE UN VIA LES ÉVÉNEMEMNTS PUBLICS.";
        _lbCreateLiveHunt.text = @"CRÉER UN NATURALIVE";
        _lbRejoinLiveHunt.text = @"REJOINDRE UN NATURALIVE";
        
    }
    return self;
}
-(IBAction)createLiveHuntAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1, @"");
    }
}
-(IBAction)rejoinLiveHuntAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(2, @"");
    }
}

- (instancetype)initAlert48h {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertViewLiveHuntAfter48h" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 20.0;

//        buttonAgenda.layer.masksToBounds = YES;
//        buttonAgenda.layer.cornerRadius =25.0;
//        buttonAccueil.layer.masksToBounds = YES;
//        buttonAccueil.layer.cornerRadius =25.0;

        self.lbTitle.text = str(strAlertViewLiveHuntAfter48h_Message);

    }
    return self;
}
- (instancetype)initConfirmSentinelleWithExpectTarget:(ISSCREEN)exp color:(UIColor*)colorNav {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertConfirmSentinelle" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        _lbDescriptions.text = @"NOUS PROPOSONS UN OUTIL\nDE REMONTÉS D'INFORMATIONS\nTERRAIN POUR LES FÉDÉRATIONS.\n\nSI VOUS SOUHAITEZ EN SAVOIR PLUS CONTACTEZ-NOUS";
        self.colorNavigation = colorNav;
        _lbDescriptions.textColor = self.colorNavigation;
        imgValider.backgroundColor = self.colorNavigation;
        imgIconBg.backgroundColor = self.colorNavigation;
    }
    return self;
}
- (instancetype)initConfirmFavorisWithExpectTarget:(ISSCREEN)exp {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertConfirmFavoris" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        _lbDescriptions.text = @"ÉTES-VOUS SÛR(E) DE VOULOIR\nCRÉER UN FAVORI À PARTIR\nDE CETTE PUBLICATION.";
        
        switch (exp) {
            case ISLIVE:
            {
                _lbDescriptions.textColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
                imgValider.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
            }
                break;
            case ISCARTE:
            {
                _lbDescriptions.textColor = UIColorFromRGB(CARTE_NAV_COLOR);
                imgValider.backgroundColor = UIColorFromRGB(CARTE_NAV_COLOR);
            }
                break;
            default:
            {
                _lbDescriptions.textColor = UIColorFromRGB(POSTER_NAV_COLOR);
                imgValider.backgroundColor = UIColorFromRGB(POSTER_NAV_COLOR);

            }
                break;
        }
        
    }
    return self;
}
-(instancetype)initAlertMap
{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"Alert_LargeMap" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        self.mapView_.settings.scrollGestures = YES;
        self.mapView_.settings.zoomGestures = YES;
        self.mapView_.myLocationEnabled = YES;
        [self.mapView_ setIndoorEnabled:NO];
        
        [self.mapView_.layer setMasksToBounds:YES];
        self.mapView_.layer.cornerRadius= 4;
        self.mapView_.layer.borderWidth =0;
        
        [btnCloseMap.layer setMasksToBounds:YES];
        btnCloseMap.layer.cornerRadius= 15;
        
        [btnCurrentLocation.layer setMasksToBounds:YES];
        btnCurrentLocation.layer.cornerRadius= 15;
        
        //    [vContour.layer setMasksToBounds:YES];
        //    vContour.layer.cornerRadius= 4;
        //    vContour.layer.borderWidth =0;
        
        
        //IGN test
        self.mapView_.settings.compassButton = YES;
        self.mapView_.settings.allowScrollGesturesDuringRotateOrZoom = NO;
        self.mapView_.mapType = kGMSTypeHybrid;
        self.mapView_.delegate = self;
        iZoomDefaultLevel = ZOOM_DEFAULT;
        [self fnSetLocation:[PublicationOBJ sharedInstance].latitude withLongitude:[PublicationOBJ sharedInstance].longtitude];
    }
    return self;
}

-(instancetype)initTermAndCondition
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertTermAndCondition" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 10;
        subAlertView.layer.borderWidth = 0;
        
        btnOui.layer.cornerRadius = 20.0;
        btnNon.layer.cornerRadius = 20.0;
        
        WKWebView *aWebView = [[NSTWKWebViewWarmuper sharedViewWarmuper] dequeueWarmupedWKWebView];
        
        NSString *pathA = [[NSBundle mainBundle] pathForResource:@"condition" ofType:@"html" inDirectory:@""];
        
        NSURL *url = [NSURL fileURLWithPath:pathA];
        
        NSURL *readAccessToURL = [[url URLByDeletingLastPathComponent] URLByDeletingLastPathComponent];
        
        [aWebView loadFileURL:url allowingReadAccessToURL:readAccessToURL];

        [containSubView addSubview:aWebView];
        
        aWebView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [containSubView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[aWebView]-(0)-|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(aWebView)]];
        
        [containSubView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[aWebView]|"
                                                                               options:0
                                                                               metrics:nil
                                                                                 views:NSDictionaryOfVariableBindings(aWebView)]];
        
        
        
        //[containSubView layoutIfNeeded];
        
    }
    return self;
    
    
}

-(instancetype)initChooseColor
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertChooseColor" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        tvInputMessage.placeholder = @"Entrez un message...";
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 4;
        subAlertView.layer.borderWidth =0;
        [btnClose.layer setMasksToBounds:YES];
        btnClose.layer.cornerRadius= 15;

        arrData = [NSMutableArray new];

        NSString *strPath = [FileHelper pathForApplicationDataFile: FILE_PUBLICATION_COLOR_SAVE ];
        
        NSDictionary*dic = [NSDictionary dictionaryWithContentsOfFile:strPath];
        
        if (dic) {
            [arrData addObjectsFromArray: dic[@"colors"] ];
        }
        indexSelect = -1;


        [myCollection registerNib:[UINib nibWithNibName:@"ColorCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"ColorCollectionCell"];
        myCollection.delegate = self;
        myCollection.dataSource = self;
            for (int i =0; i<arrData.count; i++) {
                NSDictionary *dicData = arrData[i];
                if ( [dicData[@"id"] intValue] == [[PublicationOBJ sharedInstance].publicationcolor intValue]) {
                    indexSelect = i;
                    break;
                }
            }
        tfLegend.text = [PublicationOBJ sharedInstance].legend;
        tfLegend.delegate = self;
        
        [tableControl reloadData];
        
    }
    return self;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(instancetype)initWithTitle:(NSString*)stitle message:(NSString*)smessage cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertVC" owner:self options:nil] objectAtIndex:0] ;

    if (self) {
        if (cancelButtonTitle.length==0) {
            _cancelButton.hidden = YES;
        }
        else
        {
            _cancelButton.hidden = NO;
            [_cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];

        }
        if (otherButtonTitles.length==0) {
            _otherButton.hidden = YES;
        }
        else
        {
            _otherButton.hidden = NO;
            [_otherButton setTitle:otherButtonTitles forState:UIControlStateNormal];
            
        }
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;

        strTitle =stitle;
        strDescriptions =smessage;
        
        [COMMON listSubviewsOfView:self];
        _lbTitle.text =strTitle;
        _lbDescriptions.text=strDescriptions;
    }
    return self;
}

-(instancetype)initInputMessage
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"InputMessageLiveHunt" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        tvInputMessage.placeholder = @"Entrez un message...";
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 4;
        subAlertView.layer.borderWidth =0;
        
        
        [subAlertView2.layer setMasksToBounds:YES];
        subAlertView2.layer.cornerRadius= 3;
        subAlertView2.layer.borderWidth =0;
        [tvInputMessage becomeFirstResponder];
        UIFont *f = [UIFont fontWithName:@"Lato-Italic" size:15];
        [tvInputMessage setFont: f];

    }
    return self;
}

-(instancetype)initMapType :(int)iType withColor:(UIColor*)colorNav
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertMapType" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        _colorNavigation = colorNav;

        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 4;
        subAlertView.layer.borderWidth =0;
        
        [btnTerminal.layer setMasksToBounds:YES];
        btnTerminal.layer.cornerRadius= 16;
        self.lbTitle.textColor = _colorNavigation;
        btnTerminal.backgroundColor = _colorNavigation;
        [btnSwitch setOnTintColor: _colorNavigation ];
        lineView.backgroundColor = _colorNavigation;
        localTag = iType;
        [self changeStatus];
        //ggtt
        label1.text = @"Voulez-vous afficher les parcelles cadastrales par dessus le type de carte de votre choix?";
        
        [self.lbTitle setText:str(strTypeAffichagedelacarte)];
        lblDesctiption.text = str(strSelectionLeTypeDaffichagedeCarte);

        btnSwitch.on = [CommonHelper sharedInstance].isActiveOverlay;

    }
    return self;
}

-(instancetype)initVent :(NSDictionary*) location withColor:(UIColor*)colorNav
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertMapVent" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        _colorNavigation = colorNav;
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 4;
        subAlertView.layer.borderWidth =0;
        
        
        [self.lbTitle setText:str(strVent)];
        [desc setTextColor:_colorNavigation];
        [self.lbTitle setTextColor:_colorNavigation];

        //Meteo API
        [COMMON addLoadingForView:self];
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj getInfoWindMap:location[@"latitude" ] withLng:location[@"longitude"] withKey:[[CommonHelper sharedInstance] getRandKey]];
        serviceObj.bNoNeedResponse = YES;
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                
                if ([response[@"wind"] isKindOfClass: [NSDictionary class]])
                {
                    NSString *result = nil;
                    
                    if (response[@"wind"] [@"deg"]) {
                        result = [NSString stringWithFormat:@"%@ - %0.2fkm/h",[self getDirection: [response[@"wind"] [@"speed"] doubleValue]], round( [response[@"wind"] [@"speed"] doubleValue])];
                        
                    }else{
                        result = [NSString stringWithFormat:@"%0.2fkm/h", round( [response[@"wind"] [@"speed"] doubleValue])];
                        
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [desc setText: result];
                    });
                    
                    
                }
            });
            
        };  
    }
    return self;
}


-(NSString*) getDirection :(double) deg{
    
    if (deg >= 33.75f && deg < 78.75) {
        return @"Nord/Est";
    } else if (deg >= 78.75f && deg < 123.75f) {
        return @"Est";
    } else if (deg >= 123.75f && deg < 168.75f) {
        return @"Sud/Est";
    } else if (deg >= 168.75f && deg < 213.75f) {
        return @"Sud";
    } else if (deg >= 213.75f && deg < 258.75f) {
        return @"Sub/Ouest";
    } else if (deg >= 258.75f && deg < 303.75f) {
        return @"Ouest";
    } else if (deg >= 303.75 && deg < 348.75) {
        return @"Nord/Ouest";
    } else {
        return @"Nord";
    }
    
    return @"";
}



-(instancetype)initMapMeteo :(NSDictionary*) location withColor:(UIColor*)colorNav
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertMapMeteo" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        _colorNavigation = colorNav;
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 4;
        subAlertView.layer.borderWidth =0;
        
        mutDic = [NSMutableDictionary new];
        [btnClose setTitle:str(strFERMER) forState:UIControlStateNormal];
        
        [self.lbTitle setText:str(strMeteo)];
        [self.lbTitle setTextColor:_colorNavigation];
        tableControl.delegate = self;
        tableControl.dataSource = self;
        
        [tableControl registerNib:[UINib nibWithNibName:cell10 bundle:nil] forCellReuseIdentifier:cellID10];
        
        
        [COMMON addLoadingForView:self];
        
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj getInfoWeatherMap:location[@"latitude" ] withLng:location[@"longitude"] withKey:[[CommonHelper sharedInstance] getRandKey]];
        serviceObj.bNoNeedResponse = YES;
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return;
            }
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                if ([response[@"city"] isKindOfClass: [NSDictionary class]])
                {
                    
                    NSArray *arrMeteo = response[@"list"];
                    
                    [mutDic removeAllObjects];
                    
                    for (NSDictionary*dicMeteo in arrMeteo)
                    {
                        
                        //date to string
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                        
                        NSDate *capturedStartDate = [dateFormatter dateFromString: dicMeteo[@"dt_txt"] ];
                        //filter via date...
                        
                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                        
                        NSString *strDate = [NSString stringWithFormat:@"%@",
                                             [dateFormatter stringFromDate:capturedStartDate]];
                        
                        if (mutDic.allKeys.count > 0) {
                            if (mutDic[strDate] != nil) {
                                //date exist...
                                NSMutableArray *tmpArr = [mutDic[strDate] mutableCopy];
                                [tmpArr addObject:dicMeteo];
                                
                                //set again
                                [mutDic setObject:tmpArr forKey:strDate];
                                
                            }else{
                                //new date
                                [mutDic setObject:@[dicMeteo] forKey:strDate];
                            }
                            
                        }else{
                            //first object
                            [mutDic setObject:@[dicMeteo] forKey:strDate];
                        }
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if ([response[@"city"][@"name"] isKindOfClass: [NSString class]]) {
                            NSString *strTitle = [NSString stringWithFormat:@"Météo - %@", response[@"city"][@"name"]];
                            
                            [self.lbTitle setText: strTitle ];
                            
                        }
                        
                        meteoKeyArray = [mutDic.allKeys sortedArrayUsingComparator :^(id first, id second) {
                            
                            NSDateFormatter *df = [[NSDateFormatter alloc] init];
                            [df setDateFormat:@"yyyy-MM-dd"];
                            NSDate *d1 = [df dateFromString:(NSString*) first];
                            NSDate *d2 = [df dateFromString:(NSString*) second];
                            return [d1 compare: d2];
                        }];
                        
                        
                        [tableControl reloadData];
                    });
                    
                    
                }
            });
            
        };

       
        
    }
    return self;
}
-(void)fnSetexpectTarget:(ISSCREEN)exp withColor:(UIColor*)colorNav
{
    _expectTarget = exp;
    _colorNavigation = colorNav;
    
    [btnCurrentLocation setImage:nil forState:UIControlStateNormal];
    btnCurrentLocation.backgroundColor = self.colorNavigation;
    imgCatCenter.image = [UIImage imageNamed:@"ic_cat_center"];
    
    [btnCloseMap setImage:[[UIImage imageNamed:@"ic_cat_zoom"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    btnCloseMap.tintColor = self.colorNavigation;
    btnCloseMap.backgroundColor = [UIColor whiteColor];
    btnClose.backgroundColor = self.colorNavigation;

    lbTitleColor.textColor = _colorNavigation;
    lbTitleLegend.textColor = _colorNavigation;
    _otherButton.backgroundColor = _colorNavigation;
}
-(void) fnSetChooseColor
{
    NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
    if([dicFavo[@"color"] isKindOfClass:[NSDictionary class]])
    {
        for (int i =0; i<arrData.count; i++) {
            NSDictionary *dicData = arrData[i];
            if ( [dicData[@"id"] intValue] == [dicFavo[@"color"][@"id"] intValue]) {
                indexSelect = i;
                break;
            }
        }
    }
    tfLegend.text = dicFavo[@"legend"];
    
    [tableControl reloadData];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return meteoKeyArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TypeCell10 *cell = nil;
    
    cell = (TypeCell10 *)[tableControl dequeueReusableCellWithIdentifier:cellID10 forIndexPath:indexPath];
    
    NSString*strDateKey = meteoKeyArray[indexPath.row];
    
    NSArray *arrData = mutDic[strDateKey];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *d1 = [df dateFromString:strDateKey];
    
    NSDateComponents *componentsNow = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDateComponents *componentsD1 = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:d1];
    
    
    if ([componentsNow year] < [componentsD1 year]) {
        cell.label1.text =  [NSDate currentRFC822TimeYear:strDateKey];
        
    }else{
        cell.label1.text =  [NSDate currentRFC822TimeNoYear:strDateKey];
    }
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.label1 setTextColor:_colorNavigation];
    cell.colorNavigation = _colorNavigation;
    [cell fnSetWeatherData:arrData];
    cell.scrollContent.contentSize =CGSizeMake(88*(arrData.count+0.6), cell.scrollContent.frame.size.height);
    
    return cell;
}

-(void)awakeFromNib{
    [super awakeFromNib];

}


// MARK:- Carte Type
//plan/satelite/hybride/relief

-(IBAction)fnCurrentLocation:(UIButton*)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:self.mapView_.camera.zoom]];

}

-(IBAction)fnBtnClick:(UIButton*)sender
{
    localTag = sender.tag;
    [self changeStatus];
    
    //apply change:
    [self fnValider:nil];
}

-(void)changeStatus{
    
    for (int i = 0; i < 6; i++) {
        UIButton*btn = [subAlertView viewWithTag: START_SUB_NAV_TAG+i];
        [btn setBackgroundImage: nil forState:UIControlStateNormal];
        btn.backgroundColor = _colorNavigation;
        [btn.layer setMasksToBounds:YES];
        btn.layer.cornerRadius= 4;

        UIImageView *img = [subAlertView viewWithTag: 2*(START_SUB_NAV_TAG+i)];
        img.hidden = YES;
    }
    UIImageView *imgSelected = [subAlertView viewWithTag: 2*localTag];
    imgSelected.hidden = NO;
}

-(IBAction)fnValider:(UIButton*)sender
{
    //Validate
    [CommonHelper sharedInstance].isActiveOverlay = btnSwitch.isOn;
    
    //plan/satelite/hybride/relief
    //10 - 11 - 12- 13
    _callback(localTag, @"");

    [self closeAction:nil];
    
}
//MARK: - MAPVIEW DELEGATE

-(void)mapView:(GMSMapView*)mapView idleAtCameraPosition:(GMSCameraPosition *)cameraPosition
{
    //CENTER
    float lat = mapView.camera.target.latitude;
    float lng = mapView.camera.target.longitude;
    
    //continue
   NSString *strLatitude=[NSString stringWithFormat:@"%f",lat];
   NSString *strLongitude=[NSString stringWithFormat:@"%f",lng];
    [PublicationOBJ sharedInstance].latitude =strLatitude;
    [PublicationOBJ sharedInstance].longtitude = strLongitude;
}
-(void)fnSetLocation:(NSString*)latitude withLongitude:(NSString*)longitude
{
    //From photo or has location data...
    if ( [latitude doubleValue] != 0 && [longitude doubleValue]!=0 )
    {
        CLLocationCoordinate2D simpleCoord;
        simpleCoord.latitude = [latitude doubleValue];
        simpleCoord.longitude= [longitude doubleValue];
        
        self.mapView_.camera = [GMSCameraPosition cameraWithTarget:simpleCoord zoom: iZoomDefaultLevel];
        
    }else{
        if([self locationCheckStatusDenied])
        {
            [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:LAT_DEFAULT longitude:LON_DEFAULT zoom:iZoomDefaultLevel]];
            return;
        }
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self.mapView_ animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:appDelegate.locationManager.location.coordinate.latitude longitude:appDelegate.locationManager.location.coordinate.longitude zoom:iZoomDefaultLevel]];
        
    }
}
-(BOOL)locationCheckStatusDenied
{
    if(![CLLocationManager locationServicesEnabled] ||
       [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strTitle_app) message:@"La localisation est inactive. Souhaitez-vous l'activer ?"  cancelButtonTitle:str(strAnuler) otherButtonTitles:str(strOK)];
        [vc doBlock:^(NSInteger index, NSString *str) {
            if (index==0) {
                // NON
            }
            else if(index==1)
            {
                //OUI
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:
                                                            UIApplicationOpenSettingsURLString]];
            }
        }];
        [vc showAlert];
        return YES;
    }
    return NO;
}
#pragma mark - UICollectionView COLOR
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrData.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    indexSelect = (int)indexPath.row;
    [myCollection reloadData];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"ColorCollectionCell";
    
    ColorCollectionCell *cell = (ColorCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    unsigned result = 0;
    NSScanner *scanner = [NSScanner scannerWithString:dic[@"color"]];
    
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&result];
    
    [cell.vColor setBackgroundColor:UIColorFromRGB(result)];
    
    if (indexPath.row == indexSelect) {
        cell.vColor.layer.borderWidth = 2.0;
        cell.vColor.layer.borderColor=[[UIColor lightGrayColor]CGColor];
        if ([dic[@"color"] isEqualToString:@"FFFFFF"]) {
            
            cell.imgCheck.image = [UIImage imageNamed:@"CheckBlack"];
            
        }
        else
        {
            cell.imgCheck.image = [UIImage imageNamed:@"CheckColor"];
            
        }
    }else{
        cell.vColor.layer.borderWidth = 0.0;
        cell.imgCheck.image = nil;
        
    }
    
    return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(subAlertView.frame.size.width/6-4, subAlertView.frame.size.width/6-4);
}

#pragma callback
-(void)setCallback:(AlertVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertVCCallback ) cb
{
    self.callback = cb;
    
}
-(void)setCallbackColor:(AlertVCColorCallback)callbackColor
{
    _callbackColor=callbackColor;
}

-(void)doBlockColor:(AlertVCColorCallback ) cb
{
    self.callbackColor = cb;
    
}

#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
}

-(IBAction)fnJustCloseAction:(id)sender
{
    [self removeFromSuperview];
}

-(IBAction)closeAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0, @"");
    }
}
-(IBAction)OUIAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1, tvInputMessage.text);
    }
}

- (IBAction)fnEnvoyer:(id)sender {
    [self removeFromSuperview];
    NSString *strColorID = nil;
    NSString *trimmedString = [tfLegend.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
    if (indexSelect > -1) {
        NSDictionary *dic = arrData[indexSelect];
        strColorID = dic[@"id"];
        [dicFavo setValue:dic forKey:@"color"];
    }
    if(trimmedString.length > 0)
    {
        [dicFavo setValue:trimmedString forKey:@"legend"];
    }
    
    [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];

    if (_callbackColor) {
        _callbackColor(1, trimmedString,strColorID);
    }
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
