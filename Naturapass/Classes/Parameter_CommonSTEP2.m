//
//  Parameter_CommonSTEP2.m
//  Naturapass
//
//  Created by Giang on 3/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_CommonSTEP2.h"
#import "CellKind23.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_CommonSTEP2 ()
{
    NSMutableArray * arrData;
    NSMutableArray *arrDefault;
//    BOOL isBusy;
    
    IBOutlet UISearchBar                *toussearchBar;
    __weak IBOutlet NSLayoutConstraint *constraintHeightSearch;
}

@end

@implementation Parameter_CommonSTEP2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [NSMutableArray new];
    arrDefault = [NSMutableArray new];
//    isBusy = NO;
    
    //Refresh/loadmore
    [self initRefreshControl];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    switch (self.myTypeVview) {
            
        case CITY:
        {
            toussearchBar.placeholder = str(strMoteur_de_recherche_des_villes);

            self.strSearch = [[NSUserDefaults standardUserDefaults] objectForKey:@"save_search_city"];
            
        }
            break;
        case COUNTRY:
        {
            
            toussearchBar.placeholder = str(strMoteur_de_recheche_pays);
            self.strSearch =   [[NSUserDefaults standardUserDefaults] objectForKey:@"save_search_country"];
            
        }
            break;
        case HUNT_PRACTICED:
        case HUNT_LIKED:
        {
            constraintHeightSearch.constant = 0;
        }
            break;
        default:
            break;
    }
    
    [self doSearchDefault];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - REFRESH/LOAD MORE

//overwrite
- (void)insertRowAtTop {
    [self stopRefreshControl];
}

- (void)insertRowAtBottom {
    
    [self doSearch];
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind23 *cell = nil;
    
    cell = (CellKind23 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    
    //Check if id is in my list => not show +
    
    switch (self.myTypeVview) {
        case CITY:
        case COUNTRY:
        {
            if ([dic[@"added"] intValue] == 0) {
                [cell.rightIcon setImage: [UIImage imageNamed:@"ic_plus_label" ]];
                [cell.btnDel addTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
                [cell.rightIcon setImage: nil];
                [cell.btnDel removeTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
            }
            
        }
            break;
        case HUNT_PRACTICED:
        {
            if ([dic[@"practiced"] intValue] == 0) {
                [cell.rightIcon setImage: [UIImage imageNamed:@"ic_plus_label" ]];
                [cell.btnDel addTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
                [cell.rightIcon setImage: nil];
                [cell.btnDel removeTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
            }

        }
            break;
        case HUNT_LIKED:
        {
            if ([dic[@"liked"] intValue] == 0) {
                [cell.rightIcon setImage: [UIImage imageNamed:@"ic_plus_label" ]];
                [cell.btnDel addTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
                
            }else{
                [cell.rightIcon setImage: nil];
                [cell.btnDel removeTarget:self action:@selector(fnAdd:) forControlEvents:UIControlEventTouchUpInside];
            }

        }
            break;
        default:
            break;
    }
    
    cell.btnDel.tag =indexPath.row;
    
    
    [cell layoutIfNeeded];
    
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(void) fnAdd:(UIButton*)btn
{
    
    NSMutableDictionary*dic = [arrData[btn.tag] mutableCopy];
    
    if ([COMMON isReachable]) {
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [COMMON addLoading:self];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            [COMMON removeProgressLoading];
            
            if (!response) {
                return ;
            }
            
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            switch (self.myTypeVview) {
                case CITY:
                {
                    //go back if success... report fail message if error
                    if ([response[@"city"] isKindOfClass: [NSDictionary class]])
                    {
                        [dic setValue:@1 forKey:@"added"];

                        [arrData replaceObjectAtIndex:btn.tag withObject:dic];
                        
                        NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                        [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                    }
                    else{
                        
                    }
                }
                    break;
                case COUNTRY:
                {
                    //go back if success... report fail message if error
                    if ([response[@"country"] isKindOfClass: [NSDictionary class]])
                    {
                        [dic setValue:@1 forKey:@"added"];
                        
                        [arrData replaceObjectAtIndex:btn.tag withObject:dic];

                        NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                        [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                        
                    }
                    else{
                        
                    }
                    
                }
                    break;
                case HUNT_PRACTICED:
                {
                    if ([response[@"practiced"] isKindOfClass: [NSArray class]])
                    {
                        [dic setValue:@1 forKey:@"practiced"];
                        
                        [arrData replaceObjectAtIndex:btn.tag withObject:dic];

                        NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                        [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                        
                    }
                    else{
                        
                    }

                    
                    /*
                     practiced =     (
                     {
                     id = 1;
                     name = "A la hutte / Gabion";
                     },
                     */
                }
                    break;
                case HUNT_LIKED:
                {
                    /*
                     liked =     (
                     {
                     id = 1;
                     name = "A la hutte / Gabion";
                     },

                     */
                    if ([response[@"liked"] isKindOfClass: [NSArray class]])
                    {
                        [dic setValue:@1 forKey:@"liked"];
                        
                        [arrData replaceObjectAtIndex:btn.tag withObject:dic];

                        NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                        [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                        
                    }
                    else{
                        
                    }
                }
                    break;
                default:
                    break;
            }
        };
        
        switch (self.myTypeVview) {
            case CITY:
            {
                [serviceObj fnADD_HUNT_CITY_LOCATION:dic[@"id"]];
            }
                break;
            case COUNTRY:
            {
                [serviceObj fnADD_HUNT_COUNTRY_LOCATION:dic[@"id"]];
                
            }
                break;
            case HUNT_PRACTICED:
            {
                [serviceObj fnADD_HUNT_PRACTICE:dic[@"id"]];
                
            }
                break;
            case HUNT_LIKED:
            {
                [serviceObj fnADD_HUNT_LIKED:dic[@"id"]];
                
            }
                break;
            default:
                break;
        }
    }
    
}

#pragma mark - Search

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    _strSearch = searchText;
    if (_strSearch.length ==0) {
        arrData= [arrDefault mutableCopy];
        [self.tableControl reloadData];
    }else{
        
            [arrData removeAllObjects];
            [self.tableControl reloadData];
            [self doSearch];
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
}
//
//- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
//
//    [theSearchBar resignFirstResponder];
//
//    _strSearch = theSearchBar.text;
//    [arrData removeAllObjects];
//    [self.tableControl reloadData];
//
//    [self doSearch];
//}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void) doSearchDefault
{
    if ([COMMON isReachable])
    {
//        if ( self.myTypeVview!= HUNT_PRACTICED && self.myTypeVview!= HUNT_LIKED)
//        {
//            return;
//        }
        
//        if (isBusy == YES) {
//            return;
//        }
//
//        isBusy = YES;
//
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [COMMON addLoading:self];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            [COMMON removeProgressLoading];
            
            [self stopRefreshControl];
//            isBusy = NO;
            
            if (!response) {
                return ;
            }
            
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            [arrData removeAllObjects];
            
            switch (self.myTypeVview) {
                case CITY:
                {
                    if([response[@"cities"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"cities"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case COUNTRY:
                {
                    //            practiced = 1;
                    
                    if([response[@"countries"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"countries"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case HUNT_PRACTICED:
                case HUNT_LIKED:
                {
                    //            liked = 0;
                    
                    if([response[@"hunttypes"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"hunttypes"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                    
                }
                    break;
                default:
                    break;
            }
            arrDefault = [arrData mutableCopy];
        };
        
        switch (self.myTypeVview) {
            case CITY:
            {
                [serviceObj fnGET_HUNT_LIST_CITIES:@{@"limit":@"1000",
                                                     @"offset": @"0",
                                                     @"filter":@""
                                                     }];
            }
                break;
            case COUNTRY:
            {
                [serviceObj fnGET_HUNT_LIST_COUNTRIES:@{@"limit":@"1000",
                                                        @"offset": @"0",
                                                        @"filter":@""
                                                        }];
            }
                break;
                
                //                Get Hunt type
                //
                //                GET /v2/user/profile/hunttype?limit=20&offset=0
            case HUNT_PRACTICED:
            case HUNT_LIKED:
            {
                
                [serviceObj fnGET_HUNT_TYPES:@{@"limit":@"1000",
                                               @"offset": @"0"
                                               }];
                
            }
                break;
            default:
                break;
        }
        
        
    }
    
}

-(void) doSearch
{
    if ([COMMON isReachable])
    {
        
        
        if (_strSearch == nil || [_strSearch isEqualToString: @""])
        {
            //get default
            [self stopRefreshControl];

            [self doSearchDefault];
            
            return;
        }
        
            
            
//        if (self.myTypeVview!= HUNT_PRACTICED && self.myTypeVview!= HUNT_LIKED )
//            )
//        {
//            [self stopRefreshControl];
//
//            return;
//        }
        
//        if (isBusy == YES) {
//            return;
//        }
//
//        isBusy = YES;

        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
//        [COMMON addLoading:self];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
//            [COMMON removeProgressLoading];
            
            [self stopRefreshControl];
//            isBusy = NO;
            
            if (!response) {
                return ;
            }
            
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            switch (self.myTypeVview) {
                case CITY:
                {
                    if([response[@"cities"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"cities"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case COUNTRY:
                {
                    //            practiced = 1;

                    if([response[@"countries"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"countries"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case HUNT_PRACTICED:
                case HUNT_LIKED:
                {
                    //            liked = 0;

                    if([response[@"hunttypes"] isKindOfClass: [NSArray class]]){
                        [arrData addObjectsFromArray:response[@"hunttypes"]];
                        
                        [self.tableControl reloadData];
                        
                    }

                }
                    break;
                default:
                    break;
            }
            
        };
        
        switch (self.myTypeVview) {
            case CITY:
            {
                [serviceObj fnGET_HUNT_LIST_CITIES:@{@"limit":@"20",
                                                     @"offset": [NSString stringWithFormat:@"%lu",(unsigned long)arrData.count],
                                                     @"filter":_strSearch
                                                     }];                }
                break;
            case COUNTRY:
            {
                [serviceObj fnGET_HUNT_LIST_COUNTRIES:@{@"limit":@"20",
                                                        @"offset": [NSString stringWithFormat:@"%lu",(unsigned long)arrData.count],
                                                        @"filter":_strSearch
                                                        }];
            }
                break;
                
//                Get Hunt type
//                
//                GET /v2/user/profile/hunttype?limit=20&offset=0
            case HUNT_PRACTICED:
            case HUNT_LIKED:
            {
                
                [serviceObj fnGET_HUNT_TYPES:@{@"limit":@"20",
                                                        @"offset": [NSString stringWithFormat:@"%lu",(unsigned long)arrData.count]
                                                        }];

            }
                break;
            default:
                break;
        }
        
        
    }
    
}

@end
