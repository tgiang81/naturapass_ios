//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertSuggestGroupView.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "AlertSuggestCell.h"
#import "CommonHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *identifierSection0 = @"MyTableViewCell0";

@implementation AlertSuggestGroupView
{
    WebServiceAPI *serviceAPI;
    NSMutableArray                  *searchedArray;
    NSMutableArray                  *arrFullData;
    NSDictionary                    *dicSelected;

}
-(instancetype)initSuggestView
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSuggestGroupView" owner:self options:nil] objectAtIndex:0] ;

    if (self) {
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertSuggestCell" bundle:nil] forCellReuseIdentifier:identifierSection0];
        self.tableControl.estimatedRowHeight = 60;
        [self.subView.layer setMasksToBounds:YES];
        self.subView.layer.cornerRadius= 5;
        self.subView.layer.borderWidth = 0.5;
        self.subView.layer.borderColor = [UIColor lightGrayColor].CGColor;

    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];

}
-(void)fnDataInput:(NSString*)strSearch withSelected:(NSDictionary*)dicSec withArrFullData:(NSArray*)fullData
{
    searchedArray = [NSMutableArray new];
    arrFullData = [NSMutableArray new];
    [arrFullData addObjectsFromArray:fullData];
    dicSelected = [dicSec copy];
    
    self.strSearch = strSearch;
    [searchedArray addObjectsFromArray:[self resultArray:arrFullData]];
    [self.tableControl reloadData];
}
-(NSArray*)resultArray:(NSArray*)fullData
{
    if(self.strSearch.length > 0)
    {
        NSMutableArray *arrResult = [NSMutableArray new];
        NSString *predicateString = [NSString stringWithFormat:@"name contains[c] '%@'", self.strSearch];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
        arrResult = [[fullData filteredArrayUsingPredicate:predicate] mutableCopy];
        return arrResult;
    }
    else
    {
        return fullData;
    }
}
#pragma callback
-(void)setCallback:(AlertSuggestGroupViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSuggestGroupViewCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect
{
    UIView *view = self;
    [viewSuper addSubview:view];
    view.frame = rect;
}

-(IBAction)closeAction:(id)sender
{
    [_btnDissmiss removeFromSuperview];
    [self removeFromSuperview];
    if(_callback)
    {
        _callback(dicSelected);
    }
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    self.strSearch = searchText;
    searchedArray = [[self resultArray:arrFullData] mutableCopy];
    [self.tableControl reloadData];
}
//MARK: UISearchDisplayDelegate

#pragma mark Search Helpers

- (void)clearSearchResults
{
    [searchedArray removeAllObjects];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}
#pragma mark - Helpers

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchedArray.count;
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlertSuggestCell *cell = (AlertSuggestCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
    NSDictionary *dic = searchedArray[indexPath.row];
    cell.lbTitle.text = dic[@"name"];
    cell.contraintWidthAvatar.constant = 40;
    cell.viewAvatar.hidden = NO;
    NSString* imgUrl  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    
    [cell.imgAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
    if([dic[@"id"] intValue] == [dicSelected[@"id"] intValue])
    {
        cell.imgCheck.hidden = FALSE;
    }
    else
    {
        cell.imgCheck.hidden = YES;
    }
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self removeFromSuperview];
    NSMutableDictionary *dic = [searchedArray[indexPath.row] mutableCopy];
    if([dic[@"id"] intValue] == [dicSelected[@"id"] intValue])
    {
        dicSelected = nil;
    }
    else
    {
        dicSelected = [dic copy];
    }
    if(_callback) {
        _callback(dicSelected);
    }

}

@end
