//
//  MainNavigationBaseView.h
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UIView+MGBadgeView.h"

typedef void (^callBackMainNav) (UIButton*);

@interface MainNavigationBaseView : UIView
@property (nonatomic, strong) IBOutlet UILabel *myTitle;
@property (nonatomic, strong) IBOutlet UILabel *myDesc;

@property (nonatomic, copy) callBackMainNav myMainNavCallback;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *uploadConstraintWidth;
@property (strong, nonatomic) IBOutlet UIImageView *imgUpload;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UIImageView *imgClose;
@property (strong, nonatomic) IBOutlet UIImageView *imgComment;

@property (strong, nonatomic) IBOutlet UIButton *btnClose;


-(IBAction)fnMainNavClick:(id)sender;
-(void) badgingBtn:(UIImageView*)btn count:(int) iCount;

@end
