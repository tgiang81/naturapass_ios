//
//  BaseView.h
//  Naturapass
//
//  Created by Manh on 6/16/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
-(void) initRefreshControl;
-(void)startRefreshControl;
-(void)stopRefreshControl;
-(void)isRemoveScrollingViewHeight:(BOOL)enable;
-(void)moreRefreshControl;
@property(nonatomic,assign) BOOL isMoiMur;
@end
