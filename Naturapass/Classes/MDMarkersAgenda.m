//
//  MDMarkersAgenda.m
//  Naturapass
//
//  Created by Giang on 10/31/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDMarkersAgenda.h"
#import "Config.h"
#import "UIImage+ProportionalFill.h"
#import "Define.h"

@implementation MDMarkersAgenda

-(instancetype)initWithDictionary:(NSDictionary*)dicMarker
{
    self = [super init];
    if (self) {
        self.dicMarker = dicMarker;
    }
    return self;
}

- (void)doSetCalloutView {
    
    [self loadImageData];
}

-(void)loadImageData
{
    UIView *view = [[UIView alloc] init];
    
    //set size image makers
    imageViewMarker = [[UIImageView alloc] init];
    CGRect imageViewFrame = [imageViewMarker frame];
    imageViewFrame.size = CGSizeMake(76, 98);

    [imageViewMarker setFrame:imageViewFrame];
    
    [view addSubview:imageViewMarker];
    
    //set frame view
    [view setFrame:CGRectMake(0, 0, imageViewFrame.size.width, imageViewFrame.size.height)];
    //get og
    percent_arrow = (imageViewFrame.origin.x + imageViewFrame.size.width/2)/view.frame.size.width;
    
    //set image marker
    NSString *photoDefault = @"";
    
    photoDefault = @"ic_agenda_point";
    
    UIImage *imageTmp = nil;
    
    UIImage * image = [UIImage imageNamed:photoDefault];
    
    [imageViewMarker setImage:  image];
    
    imageTmp = [self imageFromWithImage:view];
    [self returnImage:imageTmp withImage:image];
    
}

-(void)returnImage:(UIImage*)image withImage:(UIImage*)nonLegend
{
    if (_CallBackMarkers) {
        _CallBackMarkers(image,nonLegend,percent_arrow);
    }
    
}
- (UIImage *)imageFromWithImage:(UIView*)view
{
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
