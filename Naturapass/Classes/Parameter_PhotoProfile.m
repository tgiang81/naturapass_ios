//
//  Parameter_Profile.m
//  Naturapass
//
//  Created by Giang on 10/12/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Parameter_PhotoProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PhotoSheetSignUpVC.h"
#import "AZNotification.h"
#import "CommonHelper.h"
@interface Parameter_PhotoProfile ()<UITextFieldDelegate,UIActionSheetDelegate,WebServiceAPIDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSDictionary         *dicProfile;
    NSDictionary         *dicImageInfo;
    NSArray              *civiliteArray;
    NSNumber             *pickerValue;
    UIPickerView         *formPicker;
    UIActionSheet        *formActionSheet;
    
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIImageView *imgIconProfile;

    IBOutlet UIButton    *btnSuivant;
    IBOutlet UILabel    *lbmodifi;
    IBOutlet UILabel *lbTitle;
    
}

@end

@implementation Parameter_PhotoProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMA_PHOTO_DE_PROFIL);
    lbmodifi.text = str(strMODIFIER);
    [btnSuivant setTitle:  str(strTERMINER) forState:UIControlStateNormal];

    lbmodifi.backgroundColor = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
    [imgProfile.layer setMasksToBounds:YES];
    imgProfile.layer.cornerRadius= 75;
    imgProfile.layer.borderWidth =0;
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnGETUSERPROFILE_API];
    
    
    [COMMON addLoading:self];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        dicProfile =[NSDictionary dictionaryWithDictionary:response[@"user"]];
        [self loadDataOnView];
    };
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

-(void)loadDataOnView
{
    if(dicProfile)
    {
        //profilepicture
        NSString *imgUrl=@"";
        if (dicProfile[@"profilepicture"] != nil) {
            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicProfile[@"profilepicture"]];
        }else{
            imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicProfile[@"photo"]];
        }
        [imgProfile sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
        imgIconProfile.hidden=YES;
    }
}

#pragma mark - Profile Image Picker

- (IBAction)profileImageOptions:(id)sender{
    PhotoSheetSignUpVC *vc = [[PhotoSheetSignUpVC alloc] initWithNibName:@"PhotoSheetSignUpVC" bundle:nil];
    [vc setMyCallback:^(NSDictionary*data){
        dicImageInfo = data;
        imgProfile.image = dicImageInfo[@"image"];
        imgIconProfile.hidden=YES;
        
    }];
    
    [self presentViewController:vc animated:NO completion:^{}];
    [self.view endEditing:YES];
}


- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}

#pragma mark
-(IBAction)suivantAction:(id)sender
{
    [self uploadImage];
}

-(void) uploadImage
{
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        [COMMON removeProgressLoading];
        
//        NSLog(@"%@", response);
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if ([response valueForKey:@"success"]){
            [UIAlertView showWithTitle:str(strTitle_app)
                               message:str(strMessage20)
                     cancelButtonTitle:str(strOui)
                     otherButtonTitles:@[str(strNon)]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == [alertView cancelButtonIndex]) {
                                      [self gotoback];
                                  }
                              }];
        }
        else{
            NSString *errorMsg=response[@"message"];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
        }
    };

    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];

    NSDictionary *attachment =[NSDictionary new];
    if(UIImageJPEGRepresentation(dicImageInfo[@"image"], 1.0)){        //neu su dun avarta thi su dung api, con su dung anh thi dung api khac
        
        attachment = @{@"kFileName": @"image.png",
                       @"kFileData": UIImageJPEGRepresentation(dicImageInfo[@"image"], 0.5) };
        [serviceAPI postUserProfilePictureActionwithAttachmentImage:attachment];
    }
}
@end
