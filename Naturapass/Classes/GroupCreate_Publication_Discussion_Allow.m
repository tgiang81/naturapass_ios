//
//  GroupCreate_Publication_Discussion_Allow.m
//  Naturapass
//
//  Created by GiangTT on 12/20/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "GroupCreate_Publication_Discussion_Allow.h"

#import "CellKind2.h"
#import "ChassesCreate_Step2.h"
#import "DatabaseManager.h"
#import "TypeCellAllowHeader.h"
#import "GroupCreate_Step2.h"

static NSString *typecell31ID =@"TypeCell31ID";

static NSString *typecell31ID2 =@"TypeCell31ID2";

@interface GroupCreate_Publication_Discussion_Allow ()
{
    NSMutableArray *arrDataADD, *arrDataSHOW;
    int iIndexAddSelect,  iIndexShowSelect;;
    
}

@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;

@end

@implementation GroupCreate_Publication_Discussion_Allow

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.btnSuivant setBackgroundColor:UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
    
    
    arrDataADD = [NSMutableArray new];
    arrDataSHOW = [NSMutableArray new];
    
    iIndexAddSelect = -1;
    
    iIndexShowSelect = -1;
    
    [self initialization];
    // Do any additional setup after loading the view from its nib.
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}

-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:typecell31ID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellAllowHeader" bundle:nil] forCellReuseIdentifier:typecell31ID2];
    
    if (self.myTypeView == CHAT_ALLOW_GROUP) {
        self.label1.text = str(strTitle_Add_Show_Chat_1);
        self.label2.text = str(strTitle_Add_Show_Chat_Group);
        
    }else{ //     PUBLICATION_ALLOW_GROUP=0,
        self.label1.text = str(strTitle_Add_Show_Publication_1);
        self.label2.text = str(strTitle_Add_Show_Publication_Group);
    }    

    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        BOOL isExist = NO;
        
        //allow_add = [0 => ADMIN, 1 => ALL_MEMBERS]
        
        BOOL theValue = NO;
        
        //            _label1.text =str(strQUI_PEUT_PUBLIER);//admin
        //            _label2.text =str(strVous_pouvez_limiter_la_publication_agenda);
        
        //can add
        //Discussion
        
        
        if (self.myTypeView == CHAT_ALLOW_GROUP) {
            NSString *strQuerry = [NSString stringWithFormat:@" SELECT c_allow_chat_add FROM tb_group WHERE (c_admin=1 AND c_user_id=%@ AND c_id=%@ ) ",sender_id, [GroupCreateOBJ sharedInstance].group_id];
            
            FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
            
            
            while ([set_querry1 next])
            {
                isExist = YES;
                
                theValue = [set_querry1 boolForColumn:@"c_allow_chat_add"];
            }
            
            [arrDataADD addObjectsFromArray: @[
                                               @{@"name":str(strTOUS_LES_MEMBRES),
                                                 @"image":strIC_group_invite_mesgroup,
                                                 @"isSelect": isExist == YES ? (   theValue==1 ? @1 : @0): [NSNumber numberWithBool:NO] },
                                               
                                               @{@"name":str(strADMINISTRATEURS),
                                                 @"image":strIC_group_setting_admin,
                                                 @"isSelect": isExist == YES ? (   theValue==0 ? @1 : @0): [NSNumber numberWithBool:NO] }
                                               
                                               ]];
            if (isExist) {
                iIndexAddSelect = 1 - theValue;
                
            }
            //discussion can show
            BOOL isExist2 = NO;
            
            //allow_add = [0 => ADMIN, 1 => ALL_MEMBERS]
            
            BOOL theValue2 = NO;
            
            //can add
            //Discussion
            
            
            NSString *strQuerry2 = [NSString stringWithFormat:@" SELECT c_allow_chat_show FROM tb_group WHERE (c_admin=1 AND c_user_id=%@ AND c_id=%@ ) ",sender_id, [GroupCreateOBJ sharedInstance].group_id];
            
            FMResultSet *set_querry2 = [db  executeQuery:strQuerry2];
            
            
            while ([set_querry2 next])
            {
                isExist2 = YES;
                
                theValue2 = [set_querry2 boolForColumn:@"c_allow_chat_show"];
            }
            
            if (isExist2) {
                iIndexShowSelect = 1 - theValue2;
            }
            
            [arrDataSHOW addObjectsFromArray: @[
                                                @{@"name":str(strTOUS_LES_MEMBRES),
                                                  @"image":strIC_group_invite_mesgroup,
                                                  @"isSelect": isExist2 == YES ? (   theValue2==1 ? @1 : @0): [NSNumber numberWithBool:NO] },
                                                
                                                @{@"name":str(strADMINISTRATEURS),
                                                  @"image":strIC_group_setting_admin,
                                                  @"isSelect": isExist2 == YES ? (   theValue2==0 ? @1 : @0): [NSNumber numberWithBool:NO] }
                                                
                                                ]];
            
        }else{ //PUBLICATION_ALLOW_GROUP
            NSString *strQuerry = [NSString stringWithFormat:@" SELECT c_allow_add FROM tb_group WHERE (c_admin=1 AND c_user_id=%@ AND c_id=%@ ) ",sender_id, [GroupCreateOBJ sharedInstance].group_id];
            
            FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
            
            
            while ([set_querry1 next])
            {
                isExist = YES;
                
                theValue = [set_querry1 boolForColumn:@"c_allow_add"];
            }
            
            if (isExist) {
                iIndexAddSelect = 1 - theValue;
                
            }
            
            [arrDataADD addObjectsFromArray: @[
                                               @{@"name":str(strTOUS_LES_MEMBRES),
                                                 @"image":strIC_group_invite_mesgroup,
                                                 @"isSelect": isExist == YES ? (   theValue==1 ? @1 : @0): [NSNumber numberWithBool:NO] },
                                               
                                               @{@"name":str(strADMINISTRATEURS),
                                                 @"image":strIC_group_setting_admin,
                                                 @"isSelect": isExist == YES ? (   theValue==0 ? @1 : @0): [NSNumber numberWithBool:NO] }
                                               
                                               ]];
            
            //discussion can show
            BOOL isExist2 = NO;
            
            //allow_add = [0 => ADMIN, 1 => ALL_MEMBERS]
            
            BOOL theValue2 = NO;
            
            //can add
            //Discussion
            
            
            NSString *strQuerry2 = [NSString stringWithFormat:@" SELECT c_allow_show FROM tb_group WHERE (c_admin=1 AND c_user_id=%@ AND c_id=%@ ) ",sender_id, [GroupCreateOBJ sharedInstance].group_id];
            
            FMResultSet *set_querry2 = [db  executeQuery:strQuerry2];
            
            
            while ([set_querry2 next])
            {
                isExist2 = YES;
                
                theValue2 = [set_querry2 boolForColumn:@"c_allow_show"];
            }
            
            if (isExist2) {
                iIndexShowSelect = 1 - theValue2;
            }
            
            [arrDataSHOW addObjectsFromArray: @[
                                                @{@"name":str(strTOUS_LES_MEMBRES),
                                                  @"image":strIC_group_invite_mesgroup,
                                                  @"isSelect": isExist2 == YES ? (   theValue2==1 ? @1 : @0): [NSNumber numberWithBool:NO] },
                                                
                                                @{@"name":str(strADMINISTRATEURS),
                                                  @"image":strIC_group_setting_admin,
                                                  @"isSelect": isExist2 == YES ? (   theValue2==0 ? @1 : @0): [NSNumber numberWithBool:NO] }
                                                
                                                ]];
        }
        
        [self.tableControl reloadData];
        
    }];
}

#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrDataSHOW.count + 1; //2
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        TypeCellAllowHeader *cell = (TypeCellAllowHeader *)[tableView dequeueReusableCellWithIdentifier:typecell31ID2];
        
        if (self.myTypeView == CHAT_ALLOW_AGENDA) {
            if (indexPath.section==0) {
                cell.lbTitle.text= str(strAgenda_Add_Chat);
                
            }else if (indexPath.section==1){
                cell.lbTitle.text= str(strAgenda_Show_Chat);
                
            }
        }else{ //PUBLICATION_ALLOW_AGENDA
            if (indexPath.section==0) {
                cell.lbTitle.text= str(strAgenda_Add_Publication);
                
            }else if (indexPath.section==1){
                cell.lbTitle.text= str(strAgenda_Show_Publication);
                
            }
        }
        
        [cell setBackgroundColor: [UIColor whiteColor] ];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else
    {
        NSDictionary *dic = nil;
        if (indexPath.section == 0) {//add
            dic = arrDataADD[indexPath.row-1];
        }else{
            dic = arrDataSHOW[indexPath.row-1];
        }
        
        CellKind2 *cell = (CellKind2 *)[tableView dequeueReusableCellWithIdentifier:typecell31ID];
        
        cell.imageIcon.image=  [UIImage imageNamed:dic[@"image"]];
        
        cell.label1.text = dic[@"name"];
        
        if ([dic[@"isSelect"] boolValue] == TRUE) {
            cell.imageCrown.hidden=NO;
            cell.imageCrown.image=[UIImage imageNamed:@"ic_created_group"];
            
        }else{
            cell.imageCrown.hidden=YES;
            
        }
        return cell;
    }
    
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 50;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    UIView *sectionView = nil;
//    sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame) - 10, 50)];
//
//    UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, CGRectGetWidth(self.view.frame) - 20, 40)];
//    [headerLabel setTextColor:[UIColor blackColor]];
//    headerLabel.numberOfLines=2;
//    [headerLabel setBackgroundColor:[UIColor clearColor]];
//    [headerLabel setFont:[UIFont boldSystemFontOfSize:15]];
//    [sectionView addSubview:headerLabel];
//
//    if (self.myTypeView == CHAT_ALLOW_AGENDA) {
//        if (section==0) {
//            headerLabel.text= str(strAgenda_Add_Chat);
//
//        }else if (section==1){
//            headerLabel.text= str(strAgenda_Show_Chat);
//
//        }
//    }else{ //PUBLICATION_ALLOW_AGENDA
//        if (section==0) {
//            headerLabel.text= str(strAgenda_Add_Publication);
//
//        }else if (section==1){
//            headerLabel.text= str(strAgenda_Show_Publication);
//
//        }
//    }
//
//    [sectionView setBackgroundColor: [UIColor whiteColor] ];
//
//    return sectionView;
//}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 50;
    }
    else
    {
        return 60;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        return;
    }
    //set tick
    
    if (indexPath.section == 0) {//add
        
        iIndexAddSelect =  (int) indexPath.row-1;
        
        
        [arrDataADD removeAllObjects];
        
        [arrDataADD addObjectsFromArray: @[
                                           @{@"name":str(strTOUS_LES_MEMBRES),
                                             @"image":strIC_group_invite_mesgroup,
                                             @"isSelect":   iIndexAddSelect == 0 ? [NSNumber numberWithBool:YES] :  [NSNumber numberWithBool:NO] },
                                           
                                           @{@"name":str(strADMINISTRATEURS),
                                             @"image":strIC_group_setting_admin,
                                             @"isSelect":   iIndexAddSelect == 0 ? [NSNumber numberWithBool:NO] :  [NSNumber numberWithBool:YES] }
                                           
                                           ]];
        
        NSRange range = NSMakeRange(0,1);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        
    }else{
        
        iIndexShowSelect = (int) indexPath.row-1;
        
        [arrDataSHOW removeAllObjects];
        
        [arrDataSHOW addObjectsFromArray: @[
                                            @{@"name":str(strTOUS_LES_MEMBRES),
                                              @"image":strIC_group_invite_mesgroup,
                                              @"isSelect":   iIndexShowSelect == 0 ? [NSNumber numberWithBool:YES] :  [NSNumber numberWithBool:NO] },
                                            
                                            @{@"name":str(strADMINISTRATEURS),
                                              @"image":strIC_group_setting_admin,
                                              @"isSelect":   iIndexShowSelect == 0 ? [NSNumber numberWithBool:NO] :  [NSNumber numberWithBool:YES] }
                                            
                                            ]];
        NSRange range = NSMakeRange(1,1);
        NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
        
    }
}

-(IBAction) goNext
{
    
    if (self.myTypeView == PUBLICATION_ALLOW_GROUP) {
        
        
        if ([GroupCreateOBJ sharedInstance].isModifi == YES) {
            
            [GroupCreateOBJ sharedInstance].allow_add = (iIndexAddSelect == 0)? YES : NO;
            [GroupCreateOBJ sharedInstance].allow_show = (iIndexShowSelect == 0)? YES : NO;
            
            
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                
                if ([response[@"group"] isKindOfClass: [NSDictionary class]]) {
                    
                    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                        NSArray *arrSqls = response[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            ASLog(@"%@", tmpStr);
                            
                            BOOL isOK =   [db  executeUpdate:tmpStr];

                        }
                        
                    }];
                    
                    
                    
                    [COMMON removeProgressLoading];
                    [self backEditListGroups];
                    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_GROUP object: nil userInfo: nil];
                    
                }
                
            };
            
            //? server support edit???
            
            [serviceObj fnPUT_GROUP_allowaddshow_Publication_EDIT:[GroupCreateOBJ sharedInstance].group_id withParam:@{ @"allow_add": [NSNumber numberWithBool:[GroupCreateOBJ sharedInstance].allow_add],
                                                                                                                      @"allow_show": [NSNumber numberWithBool:[GroupCreateOBJ sharedInstance].allow_show]
                                                                                                                      } ];
            
            return;
        }
        
        if (iIndexAddSelect == -1 || iIndexShowSelect== -1) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strWarningInputDialogAllowAddShow) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
            
            //Alert message
            return;
        }
        
        
        
        [GroupCreateOBJ sharedInstance].allow_add = (iIndexAddSelect == 0)? YES : NO;
        [GroupCreateOBJ sharedInstance].allow_show = (iIndexShowSelect == 0)? YES : NO;
        
        GroupCreate_Publication_Discussion_Allow *viewController1 = [[GroupCreate_Publication_Discussion_Allow alloc] initWithNibName:@"GroupCreate_Publication_Discussion_Allow" bundle:nil];
        viewController1.myTypeView = CHAT_ALLOW_AGENDA;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        
        
    }else{ // CHAT
        
        if ([GroupCreateOBJ sharedInstance].isModifi == YES) {
            
            [GroupCreateOBJ sharedInstance].allow_chat_add = (iIndexAddSelect == 0)? YES : NO;
            [GroupCreateOBJ sharedInstance].allow_chat_show = (iIndexShowSelect == 0)? YES : NO;
            
            [COMMON addLoading:self];
            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
            
            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                if ([response[@"group"] isKindOfClass: [NSDictionary class]]) {
                    
                    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                        
                        NSArray *arrSqls = response[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            ASLog(@"%@", tmpStr);
                            
                            BOOL isOK =   [db  executeUpdate:tmpStr];

                        }
                    }];
                    
                    
                    [COMMON removeProgressLoading];
                    [self backEditListGroups];
                    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_GROUP object: nil userInfo: nil];
                    
                }
            };
            
            [serviceObj fnPUT_GROUP_allowaddshow_Chat_EDIT:[GroupCreateOBJ sharedInstance].group_id withParam:@{ @"allow_add_chat": [NSNumber numberWithBool:[GroupCreateOBJ sharedInstance].allow_chat_add],
                                                                                                               @"allow_show_chat": [NSNumber numberWithBool:[GroupCreateOBJ sharedInstance].allow_chat_show]
                                                                                                               } ];


            
            return;
        }
        
        if (iIndexAddSelect == -1 || iIndexShowSelect== -1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strWarningInputDialogAllowAddShow) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
            
            //Alert message
            return;
        }
        
        [GroupCreateOBJ sharedInstance].allow_chat_add = (iIndexAddSelect == 0)? YES : NO;
        [GroupCreateOBJ sharedInstance].allow_chat_show = (iIndexShowSelect == 0)? YES : NO;
        
        GroupCreate_Step2 *viewController1 = [[GroupCreate_Step2 alloc] initWithNibName:@"GroupCreate_Step2" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        
    }
}

@end
