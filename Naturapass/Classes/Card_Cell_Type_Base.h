//
//  Card_Cell_Type10.h
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"
#import "UIImage+CheckBox.h"
typedef void (^callBackEndEditing) (NSInteger index);
typedef void (^callBackValue) (NSInteger index, NSDictionary *value);
typedef void (^AlertSeclectCallback)(NSDictionary *dicResult);
@interface Card_Cell_Type_Base : UITableViewCell<UITextViewDelegate>
{
    NSArray *arrData;
    NSArray *listCheck;
    TYPE_CONTROL typeControl;
    NSMutableArray *arrMultiple;
    NSDictionary *dicImage;
}
@property (nonatomic,strong) IBOutlet UITableView *tableControl;

@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDescription;
@property (nonatomic,strong) IBOutlet UITextField *txtInput;
@property (nonatomic,strong) IBOutlet UITextField *txtInputNumber;
@property (nonatomic,strong) IBOutlet UITextView *tvDescription;
@property (nonatomic,strong) IBOutlet UITextField *txtAnimal;
@property (nonatomic,strong) IBOutlet UIImageView *imgIconText;
@property (nonatomic,strong) IBOutlet UIImageView *imgDecrease;
@property (nonatomic,strong) IBOutlet UIImageView *imgIncrease;
@property (nonatomic,strong) IBOutlet UIImageView *imgIconDecrease;
@property (nonatomic,strong) IBOutlet UIImageView *imgIconIncrease;

@property (nonatomic,strong) IBOutlet UIImageView *imgRightArrow;

@property (nonatomic,strong) IBOutlet UIButton *btnDecrease;
@property (nonatomic,strong) IBOutlet UIButton *btnIncrease;
@property (nonatomic,strong) IBOutlet UIButton *btnShowMultipSelect;
@property (nonatomic,strong) IBOutlet UIView *viewBoder;
@property (assign, nonatomic)  TYPE_NUMBER typenumber;
@property (assign, nonatomic) int iValue;
@property (assign, nonatomic) NSInteger index;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property(nonatomic,strong)  UIImage *imgCheckBoxActive;
@property(nonatomic,strong)  UIImage *imgCheckBoxInActive;
@property(nonatomic,strong)  UIColor *colorNavigation;

-(void)fnSetData:(NSArray*)arrContent  arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC;

@property (nonatomic, copy) callBackValue CallBackValue;
@property (nonatomic, copy) callBackEndEditing CallBackEditing;

-(void)doBlock:(AlertSeclectCallback ) cb;
-(void)showWithData:(NSArray*)arrContent withTypeControl:(TYPE_CONTROL)typeC;
-(void)fnTextSelect:(NSArray*)arrSelect;
//
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTable;

@end
