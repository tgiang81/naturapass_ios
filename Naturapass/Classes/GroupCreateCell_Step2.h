//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface GroupCreateCell_Step2 : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;
@property (nonatomic, strong) IBOutlet UILabel *lblDescription;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar;

@end
