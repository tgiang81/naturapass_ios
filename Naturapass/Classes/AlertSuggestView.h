//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "HNKGooglePlacesAutocomplete.h"
#import "KSToastView.h"
#import "CLPlacemark+HNKAdditions.h"

typedef void (^AlertSuggestViewCallback)(CLLocation *m_Placemark, NSString *addressString);

@interface AlertSuggestView : UIView<UITableViewDelegate,UITableViewDataSource>
{
}
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic)  UIButton *btnDissmiss;

@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property(nonatomic,strong)  NSString *strSearch;

@property (nonatomic,copy) AlertSuggestViewCallback callback;
-(void)doBlock:(AlertSuggestViewCallback ) cb;
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect;
-(instancetype)initSuggestView;
-(IBAction)closeAction:(id)sender;
- (void) processLoungesSearchingURL:(NSString *)searchText;
-(void)fnDataInput:(NSString*)strSearch;
@end
