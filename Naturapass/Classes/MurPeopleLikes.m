//
//  MurPeopleLikes.m
//  Naturapass
//
//  Created by Giang on 12/10/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MurPeopleLikes.h"

#import "CellKind12.h"
#import "AppCommon.h"
#import "LikeListCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "CCMPopupTransitioning.h"
#import "SubNavigation_PRE_General.h"
#import "MurVC.h"
static NSString *identifier = @"CellKind12ID";


@interface MurPeopleLikes ()
{
    UIColor *color;
    UIImage *imgAdd;
    NSString *sender_id;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation MurPeopleLikes

- (void)setData:(NSMutableArray*)arrMembers
{
    _arrMembers=arrMembers;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = @"Retrouvez ici toutes les personnes qui aiment cette publication";    
    NSString *strImg =@"";
    switch (self.expectTarget) {
        case ISMUR://MUR
        {
            // Do any additional setup after loading the view.
            [self.subview removeFromSuperview];
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview removeFromSuperview];
            
            [self addMainNav:@"MainNavSignaler"];
            
            //Set title
            subview =  [self getSubMainView];
            [subview.myTitle setText:@"LISTE DES J'AIME"];
            [subview.myDesc setText:@"Ajouter..."];
            subview.imgClose.hidden = TRUE;
            subview.btnClose.hidden = TRUE;
            subview.imgBackground.image = [UIImage imageNamed:@"mur_top_bar_bg"];
            //Change status bar color
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            [self addSubNav:nil];

            color = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            strImg=@"mur_new_addfriend";
            self.vContainer.backgroundColor = UIColorFromRGB(0x1B1F26);
            self.view.backgroundColor = UIColorFromRGB(0x1B1F26);
        }
            break;
        case ISCARTE://CARTE
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            
        }
            break;
        case ISDISCUSS://DISCUSSION
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP://GROUP
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            strImg=@"ic_add_member_group";
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(GROUP_BACK);

        }
            break;
        case ISLOUNGE://CHASSES
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            strImg=@"chasse_ic_add_friend";
            UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);

        }
            break;
        case ISAMIS://AMIS
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
            
        }
            break;
        case ISPARAMTRES://PARAMETERS
        {
            [self addSubNav:@"SubNavigation_PRE_General"];
            SubNavigation_PRE_General *okSubView = (SubNavigation_PRE_General*)self.subview;
            color = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
            
        }
            break;
        default:
            break;
    }
    imgAdd =[UIImage imageNamed:strImg];
    [self.tableControl registerNib:[UINib nibWithNibName:@"LikeListCell" bundle:nil] forCellReuseIdentifier:identifier];
    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://back
        {
            if ([self.mParent isKindOfClass: [MurVC class]]) {
                [self gotoback];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
            break;
    }
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrMembers count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /*
     {
     likes =     (
     {
     courtesy = 1;
     firstname = Valot;
     fullname = "Valot Vincent";
     id = 3608;
     lastname = Vincent;
     parameters =             {
     friend = 1;
     };
     photo = "/uploads/users/images/thumb/c4134f0387cfd0ccdbdbe3ed7bbdc101e35f4446.jpeg";
     usertag = "manh-manh";
     }
     );
     }
     */
    LikeListCell *cell = nil;
    
    cell = (LikeListCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = _arrMembers[indexPath.row];
    cell.lbTitle.text = dic[@"fullname"];
    //image
    NSString *imgUrl;
    //profilepicture
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:imgUrl]];
    [cell.imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    [cell layoutIfNeeded];
    
    //Arrow
    // neu la normal thi an nut add friend con neu la admin thic check da co moi quan he thi an con khong thi show ra cho nguoi dung click
    NSInteger mutualFriend = 0;
    BOOL isAddFriendHide =YES;
    if ([dic[@"id"] integerValue] ==[sender_id integerValue]) {
        isAddFriendHide= YES;
    }
    else
    {
        /*
         relation =     {
         friendship = 0;
         mutualFriends = 0;
         };
         
         
         */
        if (dic[@"relation"]) {
            if ([dic[@"relation"][@"friendship"] isKindOfClass:[NSDictionary class]])
            {
                isAddFriendHide =YES;
            }else{
                isAddFriendHide=NO;
            }
            mutualFriend = [dic[@"relation"][@"mutualFriends"] integerValue];
        }
        
    }
    
//    3 amis en commun
//    If you have common friends, it displays “n amis en commun” (without ‘s’ at the end of “commun” it’s design error).
//    if there is only 1 common friends then text is “1 ami en commun.
    
    if (mutualFriend >0) {
        if (mutualFriend > 1) {
            cell.lbDesc.text = [NSString stringWithFormat:@"%ld amis en commun",mutualFriend];
        }else{
            cell.lbDesc.text = [NSString stringWithFormat:@"1 ami en commun"];
        }
    }
    else
    {
        cell.lbDesc.text = @"";
    }
    
    if (isAddFriendHide) {
        cell.btnAddFriend.hidden =YES;
        cell.imgAddFriend.hidden=YES;
        
        [cell.btnAddFriend removeTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        cell.btnAddFriend.hidden=NO;
        cell.imgAddFriend.hidden=NO;
        
        [cell.btnAddFriend addTarget:self action:@selector(fnAddFriend:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    cell.btnAddFriend.tag= indexPath.row;
    cell.imgAddFriend.image =imgAdd;
    
    cell.backgroundColor = UIColorFromRGB(0x353B41);
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = _arrMembers[indexPath.row];
    
    if ([dic[@"id"] integerValue] !=[sender_id integerValue]) {
        
        NSDictionary *dic =_arrMembers[indexPath.row];
        if (_CallBack) {
            [self dismissViewControllerAnimated:YES completion:^{
            }];
            _CallBack(indexPath.row, dic);
        }
    }
}

#pragma mark -Add friend
-(IBAction)fnAddFriend:(UIButton*)sender
{
    int index =(int)[sender tag];
    [COMMON addLoading:self];
    NSDictionary *dic = self.arrMembers[index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postUserFriendShipAction:  dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 50;
        [alert show];
        [COMMON removeProgressLoading];
        if ([response[@"relation"] isKindOfClass: [NSDictionary class] ]) {
            //co qh
            
            NSMutableDictionary *userDic =[NSMutableDictionary dictionaryWithDictionary:dic];
            if (userDic[@"relation"]) {
                [userDic removeObjectForKey:@"relation"];
                
            }
            [userDic addEntriesFromDictionary:response];
            for (int i=0; i<self.arrMembers.count; i++) {
                NSDictionary *temp =self.arrMembers[i];
                if ([temp[@"user"][@"id"] integerValue] == [dic[@"user"][@"id"] integerValue]) {
                    [self.arrMembers replaceObjectAtIndex:i withObject:userDic];
                    break;
                }
            }
            //reload item
            NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
            [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
        }
    };
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
