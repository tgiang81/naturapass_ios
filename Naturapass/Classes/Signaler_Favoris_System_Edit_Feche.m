//
//  Publication_ChoixSpecNiv5.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Signaler_Favoris_System_Edit_Feche.h"
#import "Publication_Partage.h"
#import "CellKind5.h"
#import "CellKind5_1.h"
#import "Card_Cell_Type0.h"
#import "Card_Cell_Type1.h"
#import "Card_Cell_Type10.h"
#import "Card_Cell_Type20.h"
#import "Card_Cell_Type30.h"
#import "Card_Cell_Type40.h"
#import "Card_Cell_Type32.h"
#import "Card_Cell_Type31.h"
#import "Card_Cell_Type_Base.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AlertPublicationMultipleSelect.h"
#import "MDPopover.h"
#import "MMNumberKeyboard.h"
#import "MyAlertView.h"

#import "TagView31.h"
#import "TagView32.h"


static NSString *identifierSection0 = @"MyTableViewCell0";
static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection10 = @"MyTableViewCell10";
static NSString *identifierSection20 = @"MyTableViewCell20";
static NSString *identifierSection30 = @"MyTableViewCell30";
static NSString *identifierSection40 = @"MyTableViewCell40";
static NSString *identifierSection32 = @"MyTableViewCell32";
static NSString *identifierSection31 = @"MyTableViewCell31";

static NSString *identifier1 = @"MyTableViewCell1";
static NSString *identifier2 = @"MyTableViewCell2";
static NSString *Card_Cell = @"Card_Cell_Type_Base";

@interface Signaler_Favoris_System_Edit_Feche () <UITextFieldDelegate,MMNumberKeyboardDelegate>
{
    NSMutableArray * arrData;
    
    UITextField *txtAnimal;
    IBOutlet UIButton *btnSuivatnt;
    int indexEdit;
    MDPopover *popup;
    NSDateFormatter *formatter;
    NSIndexPath *indexPathTextCell;
    IBOutlet UILabel *lbTitle;

}
//Tag32


- (IBAction)resignFirstResponder:(id)sender;
- (IBAction)takeFirstResponder:(id)sender;
- (IBAction)enabledSwitched:(id)sender;
- (IBAction)completeDuplicatesSwitched:(id)sender;



@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
@property (nonatomic, strong) Card_Cell_Type_Base *prototypeCell;
@property (strong, nonatomic) IBOutlet UIView *viewBottom;

@end

@implementation Signaler_Favoris_System_Edit_Feche
/*
 type = 20 => date input (11/02/2016)
 type = 21 => hour input (09:00)
 type = 22 => date + hour input (11/02/2016 09:00)
 */

#pragma mark - COMMON functions

- (void) dateUpdated:(UIDatePicker *)datePicker {
    
    [self updateDataDate];
    
}

-(void) updateDataDate
{
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:    [formatter setDateFormat:@"dd/MM/YYYY"];
            break;
        case 21:    [formatter setDateFormat:@"HH:mm"];
            break;
        case 22:    [formatter setDateFormat:@"dd/MM/YYYY HH:mm"];
            break;
    }
    
    NSString *strTest = [formatter stringFromDate:self.datePicker.date];
    ASLog(@"%@",strTest);
    //update text value for text in cell.
    Card_Cell_Type_Base *cell = [self.tableControl cellForRowAtIndexPath:indexPathTextCell];
    
    cell.txtInput.text=strTest;
    
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[indexPathTextCell.row]];
    [dicMul removeObjectForKey:@"value"];
    [dicMul setObject:strTest forKey:@"value"];
    
    [dicMul removeObjectForKey:@"text"];
    [dicMul setObject:strTest forKey:@"text"];
    [arrData replaceObjectAtIndex:indexPathTextCell.row withObject:dicMul];
    
}

/*
 - (IBAction)buttonDone:(id)sender {
 [self.view endEditing:YES];
 
 }
 - (void)resignKeyboard:(id)sender
 {
 [self.view endEditing:YES];
 [self updateDataDate];
 }
 */
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    //tick the selected index...
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[Card_Cell_Type_Base class]]) {
        parent = parent.superview;
    }
    
    Card_Cell_Type_Base *cell = (Card_Cell_Type_Base *)parent;
    
    indexPathTextCell = [self.tableControl indexPathForCell:cell];
    
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeDate];
            [self updateDataDate];
        }
            break;
        case 21:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeTime];
            [self updateDataDate];
        }
            break;
        case 22:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [self updateDataDate];
        }
            break;
    }
    
    return true;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myDesc setText:@"Utiliser un favori..."];
    self.viewBottom.backgroundColor = self.colorBackGround;
    self.vContainer.backgroundColor = self.colorAttachBackGround;
    [btnSuivatnt.layer setMasksToBounds:YES];
    btnSuivatnt.layer.cornerRadius= 25.0;
    [btnSuivatnt setTitle:@"FINALISER" forState:UIControlStateNormal ];
    btnSuivatnt.backgroundColor = self.colorNavigation;

    lbTitle.text = @"Détails du signalement";
    lbTitle.textColor = [UIColor whiteColor];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.viewPickerBackground removeFromSuperview];
    [self InitializeKeyboardToolBar];

    [self.datePicker addTarget:self action:@selector(dateUpdated:) forControlEvents:UIControlEventValueChanged];
    formatter = [[NSDateFormatter alloc] init];

    if ([self.nodeItemAttachment[@"isFull"] boolValue]==YES) {
        arrData = [self.nodeItemAttachment[@"attachment"] mutableCopy];
    }
    else
    {
        arrData =[NSMutableArray arrayWithArray:@[self.nodeItemAttachment]];
    }
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind5" bundle:nil] forCellReuseIdentifier:identifier1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind5_1" bundle:nil] forCellReuseIdentifier:identifier2];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type0" bundle:nil] forCellReuseIdentifier:identifierSection0];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type1" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type10" bundle:nil] forCellReuseIdentifier:identifierSection10];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type20" bundle:nil] forCellReuseIdentifier:identifierSection20];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type30" bundle:nil] forCellReuseIdentifier:identifierSection30];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type40" bundle:nil] forCellReuseIdentifier:identifierSection40];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type32" bundle:nil] forCellReuseIdentifier:identifierSection32];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type31" bundle:nil] forCellReuseIdentifier:identifierSection31];
    self.tableControl.estimatedRowHeight = 80;
    [self InitializeKeyboardToolBar];
    
    if ([PublicationOBJ sharedInstance].isEditer) {
        [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textViewDidEndEditing:(UITextView *)m_textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:
        case 21:
        case 22:
        {
            [self updateDataDate];
        }
            break;
    }
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void) showDateView:(UIButton *)sender
{
    
    //Card_Cell_Type20
    
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[Card_Cell_Type20 class]]) {
        parent = parent.superview;
    }
    
    Card_Cell_Type20 *cell = (Card_Cell_Type20 *)parent;
    
    [cell.txtInput becomeFirstResponder];
}
-(Card_Cell_Type_Base*)prototypeCellWithIndexPath:(NSIndexPath*)indexPath
{
    NSInteger index =indexPath.row;
    NSDictionary *dic = arrData[index];
    Card_Cell_Type_Base *cell = self.prototypeCell;
    
    switch ([dic[@"type"] intValue]) {
            
            //type =0
        case 0:
        {
            cell = (Card_Cell_Type0 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0];
        }
            break;
            //type 1
        case 1:
        {
            cell = (Card_Cell_Type1 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
            
        }
            break;
            //type =10
        case 10:
        {
            
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10];
        }
            break;
            //type =11
        case 11:
        {
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10];
        }
            break;
            //type =20
        case 20:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            //type =21
        case 21:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            //not design
            //type =22
        case 22:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            
            //type =30
        case 30:
        {
            cell = (Card_Cell_Type30 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection30];
        }
            break;
            //type =30
        case 31:
        {
            cell = (Card_Cell_Type31 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection31];
        }
            break;
            //type =32
        case 32:
        {
            cell = (Card_Cell_Type32 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection32];
        }
            break;
            //type =40
            
        case 40:
        case 50:
            
        {
            cell = (Card_Cell_Type40 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection40];
        }
            break;
        default:
        {
        }
            break;
    }
    return cell;
    
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[Card_Cell_Type_Base class]])
    {
        NSInteger index =indexPath.row;
        NSDictionary *dic = arrData[index];
        
        Card_Cell_Type_Base *cell = (Card_Cell_Type_Base *)cellTmp;
        cell.expectTarget = self.expectTarget;
        cell.colorNavigation = self.colorNavigation;
        switch ([dic[@"type"] intValue]) {
                
                //type =0
            case 0:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"icon_sign_pen"];
                cell.txtInput.delegate = self;
                [cell.txtInput setInputAccessoryView:self.keyboardToolbar];
                cell.txtInput.text=dic[@"text"];
            }
                break;
                //type 1
            case 1:
            {
                [cell.tvDescription setInputAccessoryView:self.keyboardToolbar];
                cell.tvDescription.text=dic[@"text"];
                [cell setCallBackEditing:^(NSInteger index)
                 {
                     [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
                 }];
            }
                break;
                //type =10
            case 10:
            {
                cell.typenumber = NUMBER_INT;
                [cell.txtInputNumber setInputAccessoryView:self.keyboardToolbar];
                cell.txtInputNumber.keyboardType = UIKeyboardTypeNumberPad;
                cell.txtInputNumber.text=dic[@"text"];
                cell.txtInputNumber.delegate = self;
                
            }
                break;
                //type =11
            case 11:
            {
                
                //val
                cell.typenumber = NUMBER_FLOAT;
                [cell.txtInputNumber setInputAccessoryView:self.keyboardToolbar];
                //                cell.txtInputNumber.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                cell.txtInputNumber.text=dic[@"text"];
                cell.txtInputNumber.delegate = self;
                
                ///
                // Create and configure the keyboard.
                MMNumberKeyboard *keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
                keyboard.allowsDecimalPoint = YES;
                keyboard.delegate = self;
                cell.txtInputNumber.inputView = keyboard;
                
            }
                break;
                //type =20
            case 20:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"icon_sign_agenda"];
                cell.txtInput.delegate =self;
                
                cell.txtInput.inputView = self.viewPickerBackground;
                
                cell.txtInput.text=dic[@"text"];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //not design
                //type =21
            case 21:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"icon_sign_agenda"];
                cell.txtInput.delegate =self;
                cell.txtInput.text=dic[@"text"];
                
                cell.txtInput.inputView = self.viewPickerBackground;
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //not design
                //type =22
            case 22:
            {
                cell.imgIconText.image = [UIImage imageNamed:@"icon_sign_agenda"];
                cell.txtInput.delegate =self;
                
                cell.txtInput.inputView = self.viewPickerBackground;
                
                cell.txtInput.text=dic[@"text"];
                
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                
                //type =30
            case 30:
            {
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=str(strChoisir_une_fiche);
                }
                cell.btnShowMultipSelect.tag=indexPath.row+10;
                //                [cell.btnShowMultipSelect addTarget:self action:@selector(showMultipSelect:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showSelectOne:) forControlEvents:UIControlEventTouchUpInside];
                cell.imgRightArrow.image = [UIImage imageNamed:@"icon_sign_dropdown-1"];
            }
                break;
                //type =30
            case 31:
            {
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=@"";
                }
                cell.btnShowMultipSelect.tag=indexPath.row+200;
                //                [cell.btnShowMultipSelect addTarget:self action:@selector(showSelectOne:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showMultipSelect:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //type =32
            case 32:
            {
                cell.btnShowMultipSelect.tag=indexPath.row+100;
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=@"";
                }
                [cell.btnShowMultipSelect addTarget:self action:@selector(showPoupMultipleSelect:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
                //type =40
                
            case 40:
            case 50:
                
            {
                NSArray *arrContent =arrData[index][@"contents"];
                NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
                NSMutableArray *arrCheck = [NSMutableArray new];
                if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
                    arrCheck = [NSMutableArray arrayWithArray:dicMul[@"value"]];
                }
                [cell fnSetData:arrContent arrCheck:arrCheck withTypeControl:([dic[@"type"] intValue] == 40)?CONTROL_CHECKBOX:CONTROL_RADIO];
                
                [cell doBlock:^(NSDictionary *dicResult) {
                    
                    NSMutableArray *listSelect = [NSMutableArray new];
                    NSMutableArray *listSelectText = [NSMutableArray new];
                    
                    listSelect = [NSMutableArray arrayWithArray:dicResult[@"value"]];
                    [dicMul removeObjectForKey:@"value"];
                    [dicMul setObject:listSelect forKey:@"value"];
                    
                    listSelectText = [NSMutableArray arrayWithArray:dicResult[@"text"]];
                    NSString *strText= [listSelectText componentsJoinedByString:@", "];
                    [dicMul removeObjectForKey:@"text"];
                    [dicMul setObject:strText forKey:@"text"];
                    
                    [arrData replaceObjectAtIndex:index withObject:dicMul];
                    [self.tableControl reloadData];
                    
                }];
            }
                break;
            default:
            {
            }
                break;
        }
        cell.index= indexPath.row;
        [cell setCallBackValue:^(NSInteger index, NSDictionary *value)
         {
             NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
             
             [dicMul removeObjectForKey:@"value"];
             [dicMul setObject:value[@"value"] forKey:@"value"];
             
             [dicMul removeObjectForKey:@"text"];
             [dicMul setObject:value[@"value"] forKey:@"text"];
             
             [arrData replaceObjectAtIndex:index withObject:dicMul];
         }];
        
        
        cell.lbTitle.text = dic[@"name"];
        
        [cell layoutIfNeeded];
        
//        if (self.isEditFavo && indexEdit ==indexPath.row) {
//            cell.lbTitle.textColor = [UIColor redColor];
//        }
//        else{
            cell.lbTitle.textColor = [UIColor whiteColor];
//        }
        cell.imgDecrease.image =[[UIImage imageNamed:@"pub_ic_minus_pink"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.imgDecrease.tintColor = self.colorNavigation;
        cell.imgIncrease.image =[[UIImage imageNamed:@"pub_ic_plus_pink"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.imgIncrease.tintColor = self.colorNavigation;
        cell.imgIconDecrease.image = [UIImage imageNamed:@"ic_minus_white"];
        cell.imgIconIncrease.image = [UIImage imageNamed:@"ic_plus_white"];

        cell.lbDescription.textColor = self.colorNavigation;
        cell.txtInput.textColor = self.colorNavigation;
        cell.txtInputNumber.backgroundColor = [UIColor whiteColor];
        cell.txtInputNumber.textColor =  self.colorNavigation;
        cell.viewBoder.backgroundColor = [UIColor whiteColor];
        cell.backgroundColor = self.colorCellBackGround;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Card_Cell_Type_Base *cell = (Card_Cell_Type_Base*)[self fnGetUITableViewCellWithIndexPath:indexPath];
    if (cell) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    return cell;
}

-(UITableViewCell*)fnGetUITableViewCellWithIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index =indexPath.row;
    NSDictionary *dic = arrData[index];
    Card_Cell_Type_Base *cell = nil;
    
    switch ([dic[@"type"] intValue]) {
            
            //type =0
        case 0:
        {
            cell = (Card_Cell_Type0 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
        }
            break;
            //type 1
        case 1:
        {
            cell = (Card_Cell_Type1 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        }
            break;
            //type =10
        case 10:
        {
            
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10 forIndexPath:indexPath];
        }
            break;
            //type =11
        case 11:
        {
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10 forIndexPath:indexPath];
        }
            break;
            //type =20
        case 20:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            //type =21
        case 21:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            //not design
            //type =22
        case 22:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            
            //type =30
        case 30:
        {
            cell = (Card_Cell_Type30 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection30 forIndexPath:indexPath];
        }
            break;
            //type =30
        case 31:
        {
            cell = (Card_Cell_Type31 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection31 forIndexPath:indexPath];
        }
            break;
            //type =32
        case 32:
        {
            cell = (Card_Cell_Type32 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection32 forIndexPath:indexPath];
        }
            break;
            //type =40
            
        case 40:
        case 50:
            
        {
            cell = (Card_Cell_Type40 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection40 forIndexPath:indexPath];
        }
            break;
        default:
        {
        }
            break;
    }
    return cell;
}

//"receivers":[] Is federation...will responsed inside category list.

/*
 {"observation":{"specific":0,"attachments":[{"label":1,"value":2},{"label":2,"value":1},{"label":3,"value":1},{"label":4,"value":1},{"label":5,"value":3}],"receivers":[],"category":128}}
 */

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma callback
-(void)setCallback:(EditFecheCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(EditFecheCallback ) cb
{
    self.callback = cb;
    
}

- (IBAction)onNext:(id)sender {
    //Upload server
    if (_callback) {
        _callback(self.indexItem,@{@"attachment":arrData});
    }
    [self gotoback];

}
- (BOOL)isValidate:(NSDictionary *)dic{
    NSString *str=nil;
    if ([dic[@"required"] boolValue]) {
        if ([dic[@"type"] intValue]==10) {
            if ([dic[@"value"] intValue]<=0) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
        else
        {
            if ([dic[@"value"] isEqualToString:@""] || dic[@"value"]== nil) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
    }
    else
    {
        if ([dic[@"type"] intValue]==10) {
            if ([dic[@"value"] isEqualToString:@""] || dic[@"value"]== nil) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
    }
    if (str) {
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:@""
                                                          message:[NSString stringWithFormat:str(strVeuillez_remplir_le_champs),str]
                                                         delegate:self
                                                cancelButtonTitle:str(strOK)
                                                otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    else
    {
        return YES;
    }
}
-(IBAction)showMultipSelect:(id)sender
{
    NSInteger index =[sender tag]-200;
    NSArray *arrContent =arrData[index][@"contents"];
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    NSMutableArray *arrCheck = [NSMutableArray new];
    if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
        arrCheck = [NSMutableArray arrayWithArray:dicMul[@"value"]];
    }
    
    
    
    MyAlertView *ttest = [[[NSBundle mainBundle] loadNibNamed:@"MyAlertView" owner:self options:nil] objectAtIndex:0] ;
    
    
    [ttest  initParams:@"abc" arrContent:arrContent arrCheck:arrCheck withTypeControl:CONTROL_CHECKBOX parent:self];
    //
    [ttest doBlock:^(NSDictionary *dicResult) {
        
        NSMutableArray *listSelect = [NSMutableArray new];
        listSelect = [NSMutableArray arrayWithArray:dicResult[@"value"]];
        [dicMul removeObjectForKey:@"value"];
        [dicMul setObject:listSelect forKey:@"value"];
        
        NSMutableArray *listSelectText = [NSMutableArray new];
        listSelectText = [NSMutableArray arrayWithArray:dicResult[@"text"]];
        NSString *strText= [listSelectText componentsJoinedByString:@", "];
        [dicMul removeObjectForKey:@"text"];
        [dicMul setObject:strText forKey:@"text"];
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
        
    }];
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
}

#pragma mark - TYPE32

-(IBAction)showPoupMultipleSelect:(id)sender
{
    NSInteger index =[sender tag]-100;
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    
    NSArray *arrContent =dicMul[@"contents"];
    
    NSArray*arrCheck = nil;
    
    if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
        arrCheck =  dicMul[@"value"];
    }
    
    TagView32 *ttest = [[[NSBundle mainBundle] loadNibNamed:@"TagView32" owner:self options:nil] objectAtIndex:0] ;
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    [ttest doBlock:^(NSInteger index,NSDictionary *dicResult) {
        NSMutableArray *listSelect = [NSMutableArray new];
        NSMutableArray *listSelectText = [NSMutableArray new];
        for (NSDictionary *dicData in dicResult[@"arrData"]) {
            [listSelect addObject:dicData[@"value"]];
            [listSelectText addObject:dicData[@"text"]];
            
        }
        [dicMul removeObjectForKey:@"value"];
        [dicMul setObject:listSelect forKey:@"value"];
        
        NSString *strText= [listSelectText componentsJoinedByString:@", "];
        [dicMul removeObjectForKey:@"text"];
        [dicMul setObject:strText forKey:@"text"];
        
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
    }];
    [ttest showViewWithData:arrContent withArrSelected:arrCheck withIndex:index];
}

/****************/
//Tag31

//[{"label":76,"value":2}]

-(IBAction)showSelectOne:(id)sender
{
    TagView31 *ttest = [[[NSBundle mainBundle] loadNibNamed:@"TagView31" owner:self options:nil] objectAtIndex:0] ;
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    NSInteger index =[sender tag]-10;
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    
    [ttest showViewWithData:dicMul[@"contents"] withstrSelected:dicMul[@"value"] withIndex:index];
    [ttest doBlock:^(NSInteger index,NSDictionary *dicResult) {
        [dicMul removeObjectForKey:@"value"];
        [dicMul removeObjectForKey:@"text"];
        
        if (!([dicResult[@"isRemove"] boolValue] ==YES)) {
            [dicMul setObject:dicResult[@"value"] forKey:@"value"];
            [dicMul setObject:dicResult[@"text"] forKey:@"text"];
        }
        
        
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
        
    }];
}
@end
