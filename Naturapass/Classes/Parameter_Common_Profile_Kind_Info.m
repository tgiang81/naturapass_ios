//
//  Parameter_VoisChiens_Info.m
//  Naturapass
//
//  Created by Manh on 3/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_Common_Profile_Kind_Info.h"
#import "PdfCollectionCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CommonHelper.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "VoisChiens_Document_WebView_Alert.h"
#import <MediaPlayer/MediaPlayer.h>


#define             kRatio                      0.7f
#define             kTrackTintColor             [UIColor lightGrayColor]
#define             kProgressColor              [UIColor blackColor]


@interface Parameter_Common_Profile_Kind_Info ()<IDMPhotoBrowserDelegate>
{
    IBOutlet UILabel *lbName;
    IBOutlet UILabel *lbBreed;
    IBOutlet UILabel *lbSexe;
    IBOutlet UILabel *lbBirthday;
    IBOutlet UILabel *lbType;
    
    IBOutlet UILabel *lbTitleBreed;
    IBOutlet UILabel *lbTitleSexe;
    IBOutlet UILabel *lbTitleBirthday;
    IBOutlet UILabel *lbTitleType;
    IBOutlet UIImageView *imgAvatar;
    IBOutlet UIButton    *btnValider;
    IBOutlet  UICollectionView  *myCollection;
    IBOutlet UIView *viewBirthday;
    NSMutableDictionary *myMutDic;
    
}
@property (nonatomic, strong) NSMutableArray *arrDocument;

@end

@implementation Parameter_Common_Profile_Kind_Info

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fnSetValueUI];
    [myCollection registerNib:[UINib nibWithNibName:@"PdfCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"PdfCollectionCellID"];
    self.constraintHeightImage.constant = 0;
    self.arrDocument = [NSMutableArray new];
    myMutDic = [NSMutableDictionary new];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self fnSetDataInfo];
    
}
-(void)fnSetValueUI
{
    
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            lbTitleBreed.text =[ NSString stringWithFormat:@"%@ : ",str(strRACE)];
            lbTitleSexe.text  =[ NSString stringWithFormat:@"%@ : ",str(strSEXE)];
            lbTitleBirthday.text =[ NSString stringWithFormat:@"%@ : ",str(strDATE_DE_NAISANCE)];
            lbTitleType.text  =[ NSString stringWithFormat:@"%@ : ",str(strTYPE)];
            self.constraintHeightViewBirthDay.constant = 37;
            viewBirthday.hidden = NO;
        }
            break;
        case TYPE_WEAPONS:
        {
            lbTitleBreed.text =[ NSString stringWithFormat:@"%@ : ",str(strBRAND)];
            lbTitleSexe.text  =[ NSString stringWithFormat:@"%@ : ",str(strCALIBRE)];
            lbTitleType.text  =[ NSString stringWithFormat:@"%@ : ",str(strTYPE)];
            self.constraintHeightViewBirthDay.constant = 0;
            viewBirthday.hidden =YES;
        }
            break;
        default:
            break;
    }
}


- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction) clickPhoto
{
    IDMPhoto *aphoto = nil;
    
    NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"photo"][@"path"]]];
    
    strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
    
    NSURL * urlImage = [NSURL URLWithString:strImage];
    
    
    aphoto = [IDMPhoto photoWithURL:urlImage];
    // Create and setup browser
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[aphoto]];
    browser.delegate = self;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
    
}

-(void)fnSetDataInfo
{
    if (self.dicInfo) {
        switch (self.myTypeVview) {
            case TYPE_DOG:
            {
                //name
                lbName.text = self.dicInfo[@"name"];
                //breed
                if ([self.dicInfo[@"breed"] isKindOfClass:[NSDictionary class]]) {
                    lbBreed.text = self.dicInfo[@"breed"][@"name"];
                    
                }
                //type
                if ([self.dicInfo[@"type"] isKindOfClass:[NSDictionary class]]) {
                    lbType.text = self.dicInfo[@"type"][@"name"];
                }
                //sex
                lbSexe.text =[self.dicInfo[@"sex"] intValue] == 1?str(strFEMALE):str(strMALE);
                //birthday
                if ([self.dicInfo[@"birthday"] isKindOfClass:[NSString class]]) {
                    NSString *strDay = [self convertDatetoSever:self.dicInfo[@"birthday"]];
                    lbBirthday.text = strDay;
                }
                //photo
                if ([self.dicInfo[@"photo"] isKindOfClass:[NSDictionary class]]) {
                    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"photo"][@"path"]]]];
                    //Default
                    imgAvatar.image = [UIImage imageNamed:@"placeholder_photo"];
                    self.constraintHeightImage.constant =imgAvatar.image.size.height;
                    [self.view setNeedsDisplay];
                    
                    [imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        if (image) {
                            if (image.size.width > CGRectGetWidth(imgAvatar.bounds)) {
                                
                                UIImage *tempImg = [self fnResizeFixWidth:image :imgAvatar.bounds.size.width];
                                imgAvatar.image = tempImg;
                                self.constraintHeightImage.constant =tempImg.size.height;
                                [self.view setNeedsDisplay];
                                
                            }else{
                                imgAvatar.image = image;
                                self.constraintHeightImage.constant =image.size.height;
                                [self.view setNeedsDisplay];
                                
                            }
                        }else{
                            //image nil...or load when offline
                            
                        }
                        
                    }];
                }
                else
                {
                    
                    self.constraintHeightImage.constant = 0;
                }
                //medias
                if ([self.dicInfo[@"medias"] isKindOfClass:[NSArray class]]) {
                    self.arrDocument = [self.dicInfo[@"medias"] mutableCopy];
                    [myCollection reloadData];
                }
                
            }
                break;
            case TYPE_WEAPONS:
            {
                //name
                lbName.text = self.dicInfo[@"name"];
                //brand
                if ([self.dicInfo[@"brand"] isKindOfClass:[NSDictionary class]]) {
                    lbBreed.text = self.dicInfo[@"brand"][@"name"];
                }
                //type
                if (self.dicInfo[@"type"]) {
                    lbType.text =[self.dicInfo[@"type"] intValue] == 1?str(strCarabine):str(strFusil);
                }
                //brand
                if ([self.dicInfo[@"calibre"] isKindOfClass:[NSDictionary class]]) {
                    lbSexe.text = self.dicInfo[@"calibre"][@"name"];
                }
                //photo
                if ([self.dicInfo[@"photo"] isKindOfClass:[NSDictionary class]]) {
                    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"photo"][@"path"]]]];
                    [imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        
                        if (image) {
                            if (image.size.width > 200/*CGRectGetWidth(imgAvatar.bounds)*/) {
                                
                                UIImage *tempImg = [self fnResizeFixWidth:image :self.view.frame.size.width];
                                imgAvatar.image = tempImg;
                                self.constraintHeightImage.constant =tempImg.size.height;
                                [self.view setNeedsDisplay];
                                
                            }else{
                                imgAvatar.image = image;
                                self.constraintHeightImage.constant =image.size.height;
                                [self.view setNeedsDisplay];
                                
                            }
                        }else{
                            //image nil...or load when offline
                            imgAvatar.image = [UIImage imageNamed:@"placeholder_photo"];
                            self.constraintHeightImage.constant =imgAvatar.image.size.height;
                            [self.view setNeedsDisplay];
                            
                        }
                        
                    }];
                }
                else
                {
                    
                    self.constraintHeightImage.constant = 0;
                }
                //medias
                if ([self.dicInfo[@"medias"] isKindOfClass:[NSArray class]]) {
                    self.arrDocument = [self.dicInfo[@"medias"] mutableCopy];
                    [myCollection reloadData];
                }
                /*
                 {
                 brand = 0;
                 calibre = 0;
                 created = "2016-03-09T10:56:59+01:00";
                 id = 4;
                 medias =     (
                 );
                 name = down;
                 photo = 0;
                 type = 1;
                 updated = "2016-03-15T10:36:45+01:00";
                 }
                 */
            }
                break;
            default:
                break;
        }
        
    }
}

-(NSString*)convertDatetoSever:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] dateFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)renameFileFrom:(NSString*)oldPath to:(NSString *)newPath
{
    NSFileManager *fileMan = [NSFileManager defaultManager];
    NSError *error = nil;
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        return NO;
    }
    return YES;
}
-(void)replaceFilePDFwithDic:(NSDictionary*)dic withTypeView:(TYPE_VIEW)typeview
{
    NSDictionary *dicMedias;
    switch (typeview) {
        case TYPE_WEAPONS:
        {
            dicMedias = dic[@"weapon"];
        }
            break;
        case TYPE_DOG:
        {
            
        }
            break;
        default:
            break;
    }
    NSArray *arrMedias = dicMedias[@"medias"];
    for (NSDictionary *dicItem in arrMedias) {
        if ([dicItem[@"type"] intValue] == 102) {
            
            NSString *pathCache = [[SDImageCache sharedImageCache] cachePathForKey:dicItem[@"path"] inPath:[FileHelper SelectedFiles]];
        }
    }
}
- (void)moviePlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter] removeObserver: self name: MPMoviePlayerPlaybackDidFinishNotification object: nil];
    [self dismissMoviePlayerViewControllerAnimated];
}

#pragma mark - UICollectionView
//----------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.arrDocument.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic =self.arrDocument[indexPath.row];
    
    
    NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:dic[@"path"]]];
    
    
    //PDF
    if ([dic[@"type"] intValue]==102)
    {
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"jpeg" withString:@"pdf"];
        
        //url => Name file...
        NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
        
        NSString *filePath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
        
        if ( [FileHelper isfileExisting:filePath]) {
            //read...
            //check if exist read cache
            //else download from remote and read...
            
            VoisChiens_Document_WebView_Alert *ttest = [[VoisChiens_Document_WebView_Alert alloc] initWithURL:filePath] ;
            self.view.backgroundColor = [UIColor clearColor];
            self.modalPresentationStyle = UIModalPresentationCurrentContext;
            [self presentViewController:ttest animated:YES completion:nil];
            
        }else{
        }
        
    }//photo
    else if ([dic[@"type"] intValue]==100)
    {
        //url => Name file...
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        
        NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
        NSString *filePath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
        
        IDMPhoto *aphoto = nil;
        
        if ( [FileHelper isfileExisting:filePath]) {
            NSURL * urlImage = [NSURL fileURLWithPath:filePath];
            
            aphoto = [IDMPhoto photoWithURL:urlImage];
            // Create and setup browser
            IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[aphoto]];
            browser.delegate = self;
            // Show
            [self presentViewController:browser animated:YES completion:nil];
            
        }else{}
    }//video
    else if ([dic[@"type"] intValue]==101)
    {
        MPMoviePlayerViewController *  moviePlayer = nil;
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"videos/resize" withString:@"videos/mp4"];
        strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".mp4"];
        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg" withString:@".mp4"];
        
        
        NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
        
        NSString *filePath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
        
        NSURL *videoURL = nil;
        
        if ([FileHelper isfileExisting:filePath]) {
            videoURL =[NSURL fileURLWithPath:filePath];
        }else{
            videoURL =[NSURL URLWithString:strImage];
        }
        
        moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
        
        
        [[moviePlayer moviePlayer] prepareToPlay];
        
        //        ASLog(@"play movie");
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:moviePlayer];
        
        [self presentMoviePlayerViewControllerAnimated:moviePlayer];
        return;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"PdfCollectionCellID";
    
    PdfCollectionCell *cell = (PdfCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic =self.arrDocument[indexPath.row];
    cell.vColor.layer.borderWidth = 0.0;
    cell.vColor.layer.borderColor=[UIColor clearColor].CGColor;
    cell.lbCount.backgroundColor =[UIColor clearColor];
    cell.lbCount.text= @"";
    
    NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:dic[@"path"]]];
    
    cell.progressView.thicknessRatio = 0.1f;
    cell.progressView.trackTintColor = kTrackTintColor;
    cell.progressView.progressTintColor = kProgressColor;
    
    //PDF
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSString *placehoder =@"";
    
    if ([dic[@"type"] intValue]== 102) {
        placehoder =@"param_ic_pdf";
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"jpeg" withString:@"pdf"];
        
        
        //url => Name file...
        NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
        
        NSString *filePath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
        
        //File not exist => download
        BOOL isReady = NO;
        
        
        
        if (myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]]) {
            isReady = myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]];
        }
        
        if ( ![FileHelper isfileExisting:filePath] && (isReady == NO)) {
            
            myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]] = @1;
            
            //Invocation
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strImage]];
            
            NSURLSessionDownloadTask *downloadTask = [ manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
                
                float progress = downloadProgress.fractionCompleted;
                
                [cell.progressView setProgress:progress animated:YES];
                
            } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                return [NSURL fileURLWithPath:filePath];
                
            } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]] = @0;
                
                NSLog(@"downloadComplete!");
                [cell.progressView removeFromSuperview];
                
            }];
            [downloadTask resume];
            
        }else{
            if ([FileHelper isfileExisting:filePath]) {
                [cell.progressView removeFromSuperview];
            }
        }
    } else if ([dic[@"type"] intValue]== 100) {
        placehoder =@"placeholder_photo";
        //Photo...Video...downloading...
        myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]] = @1;
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        //url => Name file...
        NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
        
        NSString *filePath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
        
        //File not exist => download
        
        //File not exist => download
        BOOL isReady = NO;
        
        if (myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]]) {
            isReady = myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]];
        }
        
        
        if ( ![FileHelper isfileExisting:filePath]) {
            //Invocation
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strImage]];
            
            NSURLSessionDownloadTask *downloadTask = [ manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
                
                float progress = downloadProgress.fractionCompleted;
                
                [cell.progressView setProgress:progress animated:YES];
                
            } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
                return [NSURL fileURLWithPath:filePath];
                
            } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
                myMutDic[ [NSString stringWithFormat:@"%ld",(long)indexPath.row ]] = @0;
                
                NSLog(@"downloadComplete!");
                [cell.progressView removeFromSuperview];
                
            }];
            [downloadTask resume];
            
            
        }else{
            if ([FileHelper isfileExisting:filePath]) {
                [cell.progressView removeFromSuperview];
            }
        }
        
    }
    
    
    
    NSURL* url =  [NSURL URLWithString:strImage];
    [cell.imgThumb sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:placehoder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 2;
}
//-------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//-------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(collectionView.frame.size.width/3.05 , collectionView.frame.size.width/3.05);
}

-(IBAction)fnBack:(id)sender
{
    [self gotoback];
}

@end
