//
//  SettingNotificationEmailMobileVC.m
//  Naturapass
//
//  Created by manh on 1/9/17.
//  Copyright © 2017 Appsolute. All rights reserved.
//

#import "SettingNotificationEmailMobileVC.h"
#import "SettingNotificationCell.h"
#import "AlertListFrequence.h"
static NSString *settingNotificationID = @"SettingNotificationID";

@interface SettingNotificationEmailMobileVC ()
{
    NSMutableDictionary *dicGlobal;
    NSMutableArray *arrCategory;
}
@end

@implementation SettingNotificationEmailMobileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableControl registerNib:[UINib nibWithNibName:@"SettingNotificationCell" bundle:nil] forCellReuseIdentifier:settingNotificationID];
    self.tableControl.estimatedRowHeight = 100;
    dicGlobal = [NSMutableDictionary new];

    NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
    
    NSString *catStr =  [NSString stringWithFormat:@"%@_%@_",[COMMON getUserId], strGroup_id];
    
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(catStr ,(self.expectTarget== ISGROUP)?FILE_SETTING_GROUP_SAVE:FILE_SETTING_AGENDA_SAVE)];

    
    NSDictionary *dicTmp = [NSDictionary dictionaryWithContentsOfFile:strPath];
    [self loadDataWithResponse:dicTmp];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnGET_GROUPS_PARAMETERS_MOBILE:strGroup_id mykind:(self.expectTarget == ISGROUP)?MYGROUP:MYCHASSE];
    
    
    if (dicTmp == nil) {
        [COMMON addLoading:self];
    }
    
    serviceObj.onComplete = ^(id response, int errCode){
        [COMMON removeProgressLoading];
        if ([response isKindOfClass:[NSDictionary class]]) {
            if([response[@"mobile"] isKindOfClass: [NSArray class]] || [response[@"email"] isKindOfClass: [NSArray class]])
            {
                [response writeToFile:strPath atomically:YES];
                [self loadDataWithResponse:response];
            }
        }
    };

}

-(void)loadDataWithResponse:(NSDictionary*)response
{
    arrCategory = [NSMutableArray new];
    NSString *descEmail;
    NSString *descMobile;
    if (self.expectTarget == ISGROUP) {
        descEmail = @"Lorsqu'il y a une nouvelle publication dans ce groupe, vous voulez être averti par email ?";
        descMobile = @"Lorsqu'il y a une nouvelle publication dans ce groupe, voulez-vous recevoir une notification sur votre smartphone ?";
    }
    else
    {
        descEmail = @"Lorsqu'il y a une nouvelle publication dans ce agenda, vous voulez être averti par email ?";
        descMobile = @"Lorsqu'il y a une nouvelle publication dans ce agenda, voulez-vous recevoir une notification sur votre smartphone ?";
    }
    NSDictionary *dicCategory = @{@"label":@"NOTIFICATIONS EMAIL",
                                  @"description":descEmail};
    
    NSDictionary *dicCategory2 = @{@"label":@"NOTIFICATIONS MOBILE",
                                   @"description":descMobile};
    //
    dicGlobal = [response mutableCopy];
    if([response[@"email"] isKindOfClass: [NSArray class]])
    {
        [arrCategory addObject:dicCategory];
        NSArray *arrEmail = response[@"email"];
        NSMutableDictionary *dicActive = [NSMutableDictionary new];
        for (NSDictionary *dic in arrEmail) {
            if ([dic[@"active"] boolValue]) {
                dicActive = [dic mutableCopy];
                break;
            }
        }
        [dicGlobal setObject:dicActive forKey:@"value_email"];
    }
    if([response[@"mobile"] isKindOfClass: [NSArray class]])
    {
        [arrCategory addObject:dicCategory2];
        NSDictionary *dicMobile = response[@"mobile"][0];
        BOOL wanted = [dicMobile[@"wanted"] boolValue];
        
        NSMutableArray *arrFrequenceMobile = [NSMutableArray new];
        NSDictionary *dicM = @{@"val":@(0),@"label":str(strJamais),@"active":@(!wanted)};
        NSDictionary *dicM1 = @{@"val":@(1),@"label":str(strInstanee),@"active":@(wanted)};
        [arrFrequenceMobile addObject:dicM];
        [arrFrequenceMobile addObject:dicM1];
        [dicGlobal setObject:arrFrequenceMobile forKey:@"temp_mobile"];
        [dicGlobal setObject:wanted?dicM1:dicM forKey:@"value_mobile"];
        
    }

    [self.tableControl reloadData];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrCategory.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SettingNotificationCell *cell = nil;
    
    cell = (SettingNotificationCell *)[self.tableControl dequeueReusableCellWithIdentifier:settingNotificationID forIndexPath:indexPath];
    NSDictionary *dic = arrCategory[indexPath.row];
    cell.lbTitle.text = dic[@"label"];
    cell.lbDescription.text = dic[@"description"];
    if (self.expectTarget == ISLOUNGE) {
        cell.lbValueFrequence.textColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    }
    else
    {
        cell.lbValueFrequence.textColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    }
    //email
    if (indexPath.row == 0) {
        if ([dicGlobal[@"value_email"] isKindOfClass:[NSDictionary class]]) {
            cell.lbValueFrequence.text  = dicGlobal[@"value_email"][@"label"];
        }
        else
        {
            cell.lbValueFrequence.text  = @"";
        }

    }
    //smartphone
    else
    {
        if ([dicGlobal[@"value_mobile"] isKindOfClass:[NSDictionary class]]) {
            cell.lbValueFrequence.text  = dicGlobal[@"value_mobile"][@"label"];
        }
        else
        {
            cell.lbValueFrequence.text  = @"";
        }
    }
    cell.btnChoose.tag=4000+indexPath.row;
    [cell.btnChoose addTarget:self action:@selector(chooseFrequence:) forControlEvents:UIControlEventTouchUpInside];

    cell.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(void)chooseFrequence:(id)sender
{
    NSInteger tag =((UIButton*)sender).tag;
    NSInteger index = tag-4000;
    NSArray *arrFrequence;
    //email
    if (index == 0) {
        arrFrequence = dicGlobal[@"email"];
    }
    //smartphone
    else
    {
        arrFrequence = dicGlobal[@"temp_mobile"];
    }
    //show list
    AlertListFrequence *vcListMarker = [[AlertListFrequence alloc] init];
    [vcListMarker doBlock:^(NSDictionary *userData) {
        [self changeValue:userData withIndex:index];
    }];
    vcListMarker.arrFrequence = [arrFrequence mutableCopy];
    vcListMarker.expectTarget = self.expectTarget;
    [vcListMarker showAlert];
}

-(void)changeValue:(NSDictionary*)userData withIndex:(NSInteger)index
{
    NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];

    NSString *catStr =  [NSString stringWithFormat:@"%@_%@_",[COMMON getUserId], strGroup_id];

    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(catStr ,(self.expectTarget== ISGROUP)?FILE_SETTING_GROUP_SAVE:FILE_SETTING_AGENDA_SAVE)];

    //email
    if (index == 0) {
        [COMMON addLoading:self];

        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj putMailable: strGroup_id mailable:[userData[@"val"] intValue] mykind:(self.expectTarget == ISGROUP)?MYGROUP:MYCHASSE];
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];

            if ([response[@"success"] boolValue]) {
                NSArray *arrFrequence = dicGlobal[@"email"];
                NSMutableDictionary *dicValue = [NSMutableDictionary new];
                NSMutableArray *arrValue = [NSMutableArray new];
                for (int i = 0; i < arrFrequence.count; i++) {
                    dicValue = [arrFrequence[i] mutableCopy];
                    if ([dicValue[@"val"] intValue] == [userData[@"val"] intValue]) {
                        [dicValue setObject:@(1) forKey:@"active"];
                        [dicGlobal setValue:dicValue forKey:@"value_email"];
                    }
                    else
                    {
                        [dicValue removeObjectForKey:@"active"];
                    }
                    [arrValue addObject:dicValue];
                }
                [dicGlobal setValue:arrValue forKey:@"email"];
                [self.tableControl reloadData];
                NSMutableDictionary *dicResponse = [NSMutableDictionary new];
                if (dicGlobal[@"email"]) {
                    [dicResponse setObject:dicGlobal[@"email"] forKey:@"email"];
                }
                if (dicGlobal[@"mobile"]) {
                    [dicResponse setObject:dicGlobal[@"mobile"] forKey:@"mobile"];
                }
                
                [dicResponse writeToFile:strPath atomically:YES];
            }
        };

    }
    //smartphone
    else
    {
        
        [COMMON addLoading:self];
        
        WebServiceAPI *serverAPI =[WebServiceAPI new];
        serverAPI.onComplete = ^(id response, int errCode)
        {
            
            [COMMON removeProgressLoading];
            if ([response[@"success"] boolValue]) {
                NSArray * arrFrequence = dicGlobal[@"temp_mobile"];
                NSMutableDictionary *dicValue = [NSMutableDictionary new];
                NSMutableArray *arrValue = [NSMutableArray new];
                for (int  i = 0; i < arrFrequence.count; i++) {
                    dicValue = [arrFrequence[i] mutableCopy];
                    if ([dicValue[@"val"] intValue] == [userData[@"val"] intValue]) {
                        [dicValue setObject:@(1) forKey:@"active"];
                        [dicGlobal setValue:dicValue forKey:@"value_mobile"];
                    }
                    else
                    {
                        [dicValue removeObjectForKey:@"active"];
                    }
                    [arrValue addObject:dicValue];
                    
                }
                [dicGlobal setValue:arrValue forKey:@"temp_mobile"];
                
                NSMutableArray *arrMobile = [dicGlobal[@"mobile"] mutableCopy];
                NSMutableDictionary *dicMobile = [arrMobile[0] mutableCopy];
                [dicMobile setObject:userData[@"val"] forKey:@"wanted"];
                [arrMobile replaceObjectAtIndex:0 withObject:dicMobile];
                [dicGlobal setValue:arrMobile forKey:@"mobile"];
                [self.tableControl reloadData];
                NSMutableDictionary *dicResponse = [NSMutableDictionary new];
                if (dicGlobal[@"email"]) {
                    [dicResponse setObject:dicGlobal[@"email"] forKey:@"email"];
                }
                if (dicGlobal[@"mobile"]) {
                    [dicResponse setObject:dicGlobal[@"mobile"] forKey:@"mobile"];
                }
                [dicResponse writeToFile:strPath atomically:YES];

            }
        };
        NSMutableArray *arrMobile = [dicGlobal[@"mobile"] mutableCopy];
        NSMutableDictionary *dicMobile = [arrMobile[0] mutableCopy];
        [serverAPI putGroupSmartphoneNotificationSetting:@{@"option": dicMobile[@"id"],
                                                           @"value": userData[@"val"],
                                                           @"group_id": strGroup_id
                                                           }];
    }
}

@end
