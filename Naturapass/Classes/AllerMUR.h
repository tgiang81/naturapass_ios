//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "Define.h"
typedef void (^AllerMURCallback)(NSInteger index);

@interface AllerMUR : UIView
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
    NSDictionary *myDic;
}
@property (nonatomic,copy) AllerMURCallback callback;

-(void)doBlock:(AllerMURCallback ) cb;
-(IBAction)closeAction:(id)sender;
-(void)showAlert;
-(instancetype)initWithData:(NSDictionary*) data;

@property (nonatomic,strong) IBOutlet UIButton *cancelButton;
@property (nonatomic,strong) IBOutlet UIButton *otherButton;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,strong) IBOutlet UIButton *voirNatura;
@property (nonatomic,strong) IBOutlet UIButton *voirGPS;
-(void)fnColorWithExpectTarget:(ISSCREEN)expectTarget;
@end
