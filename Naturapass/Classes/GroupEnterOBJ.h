//
//  GroupCreateOBJ.h
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
@interface GroupEnterOBJ : NSObject
+ (GroupEnterOBJ *) sharedInstance;

@property(nonatomic,strong) NSDictionary *dictionaryGroup;
-(void) resetParams;
@end
