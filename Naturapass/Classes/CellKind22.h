//
//  CellKind22.h
//  Naturapass
//
//  Created by Giang on 2/22/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface CellKind22 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ico_status;
@property (weak, nonatomic) IBOutlet UILabel *textLbl;
@property (weak, nonatomic) IBOutlet UIImageView *imgTick;

@end
