//
//  Publication_Favoris_System.h
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignalerBaseVC.h"

@interface Signaler_Favoris_System_Edit : SignalerBaseVC
@property (nonatomic, retain) NSMutableArray *displayArray;
-(void)refreshCard;
@end
