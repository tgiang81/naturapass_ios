//
//  LikeListCell.m
//  Naturapass
//
//  Created by JoJo on 11/28/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "LikeListCell.h"

@implementation LikeListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.imgAvatar.layer setMasksToBounds:YES];
    self.imgAvatar.layer.cornerRadius=  15;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
