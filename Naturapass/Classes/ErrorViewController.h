//
//  ErrorViewController.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 6/13/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorViewController : UIViewController {
    IBOutlet UILabel            *errorTitleLabel;
    IBOutlet UILabel            *errorDescLabel;
    IBOutlet UILabel            *inconvienenceLabel;
    
    IBOutlet UIView             *errorView;

    IBOutlet UIImageView        *errorImage;
    IBOutlet UIImageView        *imgLogo;

}
@property (nonatomic,strong) IBOutlet UIButton *gotoHome;

@end
