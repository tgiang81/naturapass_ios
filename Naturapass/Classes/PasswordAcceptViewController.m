//
//  PasswordAcceptViewController.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 5/28/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "PasswordAcceptViewController.h"

@interface PasswordAcceptViewController ()
{
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnValider;
}
@end

@implementation PasswordAcceptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    lbTitle.text = str(strUn_email_envoye);
    [btnValider setTitle:  str(strValider) forState:UIControlStateNormal];

    self.navigationItem.hidesBackButton=YES;
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
}
-(IBAction)validerButtonAction:(id)sender{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
