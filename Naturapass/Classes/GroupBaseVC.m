//
//  GroupBaseVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupBaseVC.h"
#import "GroupMesVC.h"
#import "GroupSettingVC.h"
#import "GroupSearchVC.h"
#import "NotificationVC.h"
#import "ChassesMurVC.h"
#import "GroupEnterOBJ.h"
#import "GroupInvitationAttend.h"

#import "CommonObj.h"
@interface GroupBaseVC ()

@end

@implementation GroupBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addMainNav:@"MainNavMUR"];
    
    //show tabBottom
    if ([self isKindOfClass:[GroupMesVC class]] ||
        [self isKindOfClass:[GroupSettingVC class]] ||
        [self isKindOfClass:[GroupSearchVC class]] ||
        [self isKindOfClass:[GroupInvitationAttend class]] ) {
        self.showTabBottom = YES;
    }
    

    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strGROUPES)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    
    [self addSubNav:@"SubNavigationGROUP"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];

    [self setThemeNavSub:NO withDicOption:nil];

    //Set select status buttons.
    UIButton *btn = [self returnButton];
    
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
    //Confort
    UIButton *btnCheck = (UIButton*)[self.subview viewWithTag: 2*( START_SUB_NAV_TAG + 3)];
    [self.subview badgingBtn:btnCheck count: (int)[CommonObj sharedInstance].nbGroupInvitation];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void) fnGetAllNotifications
{
    UIImageView *btnAmis =(UIImageView*)[subviewCount viewWithTag:24];
    UIImageView *btnDiscussion =(UIImageView*)[subviewCount viewWithTag:26];
    UIImageView *btnNotification =(UIImageView*)[subviewCount viewWithTag:28];
    
    [subviewCount badgingBtn:btnAmis count:[CommonObj sharedInstance].nbUserWaiting];
    [subviewCount badgingBtn:btnDiscussion count:[CommonObj sharedInstance].nbUnreadMessage];
    [subviewCount badgingBtn:btnNotification count:[CommonObj sharedInstance].nbUnreadNotification];
    [self fnGetAllNotifications:3 creen:ISGROUP];
}
#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];

    [super onSubNavClick:btn];

    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://MES GROUP
        {
            if ([self isKindOfClass: [GroupMesVC class]])
                return;
            [self doPushVC:[GroupMesVC  class] iAmParent:NO];
        }
            break;
            
        case 2://SEARCH..it's me
        {
            if ([self isKindOfClass: [GroupSearchVC class]])
                return;
            
            [self doPushVC:[GroupSearchVC  class] iAmParent:NO];
        }
            break;
            
        case 3://Invitation attend
        {
            if ([self isKindOfClass: [GroupInvitationAttend class]])
                return;

            [self doPushVC:[GroupInvitationAttend  class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
}

@end
