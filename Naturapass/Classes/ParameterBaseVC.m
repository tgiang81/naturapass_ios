//
//  MurBaseVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ParameterBaseVC.h"

#import "Parameter_Publication.h"
#import "Parameter_Notification_GlobalVC.h"
#import "Parameter_Favorites.h"
#import "Parameter_ProfilVC.h"
#import "ParameterVC.h"
#import "Parameter_MesFavoris.h"
#import "Define.h"

#import "SubNavigationMUR.h"

@interface ParameterBaseVC ()

@end


@implementation ParameterBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    self.showTabBottom = YES;

    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strPARAMETRES)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
    
    [self addSubNav:@"SubNavigationPARAMETER"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //Change status bar color
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(PARAM_TINY_BAR_COLOR) ];

    
    [self setThemeNavSub:NO withDicOption:nil];

    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];

}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    
    [super onSubNavClick:btn];

    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }
                    BaseVC *viewController1 = (BaseVC*)controller;
                    if ([controller isKindOfClass: [Parameter_Notification_GlobalVC class]] && viewController1.nofiticationType != self.nofiticationType) {
                        continue;
                    }
                    [self cleanAnyTmpData];
                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://Profile
        {
            if ([self isKindOfClass: [Parameter_ProfilVC class]])
                return;
            
            [self doPushVC:[Parameter_ProfilVC class] iAmParent:NO];
        }
            break;
        case 2:// publication
        {
            if ([self isKindOfClass: [Parameter_Publication class]])
                return;
            
            [self doPushVC:[Parameter_Publication class] iAmParent:NO];
        }
            break;
            
            
        case 3:// mail
        {
            if ([self isKindOfClass: [Parameter_Notification_GlobalVC class]] && self.nofiticationType == ISEMAIL)
                return;
            
            Class class = [Parameter_Notification_GlobalVC class];
            BaseVC *viewController1 = [[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
            viewController1.nofiticationType = ISEMAIL;
            [self pushVC:viewController1 animate:NO expectTarget:self.expectTarget iAmParent:NO];
        }
            break;
            
        case 4:// phone
        {
            if ([self isKindOfClass: [Parameter_Notification_GlobalVC class]] && self.nofiticationType == ISSMARTPHONE)
                return;
            
            Class class = [Parameter_Notification_GlobalVC class];
            BaseVC *viewController1 = [[class alloc] initWithNibName:NSStringFromClass(class) bundle:nil];
            viewController1.nofiticationType = ISSMARTPHONE;
            [self pushVC:viewController1 animate:NO expectTarget:self.expectTarget iAmParent:NO];
        }
            break;

        case 5:// address
        {
            if ([self isKindOfClass: [Parameter_MesFavoris class]])
                return;
            
            [self doPushVC:[Parameter_MesFavoris class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
}

@end
