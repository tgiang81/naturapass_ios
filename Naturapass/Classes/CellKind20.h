//
//  CellKind20.h
//  Naturapass
//
//  Created by Giang on 11/6/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface CellKind20 : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;

@end
