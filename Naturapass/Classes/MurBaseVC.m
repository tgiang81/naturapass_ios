//
//  MurBaseVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurBaseVC.h"

#import "MurVC.h"
#import "MurFilterVC.h"
#import "MurSettingVC.h"

#import "Define.h"
#import "NotificationVC.h"

#import "SubNavigationMUR.h"
#import "ResearchVC.h"
#import "ChatListe.h"
#import "CommonObj.h"
#import "NotificationVC.h"
#import "FriendInfoVC.h"

@interface MurBaseVC ()

@end


@implementation MurBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavMUR_V3"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strMUR)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    //show tabBottom
    if ([self isKindOfClass:[MurVC class]] ||
        [self isKindOfClass:[NotificationVC class]] ||
        [self isKindOfClass:[MurSettingVC class]] ||
        
        [self isKindOfClass:[FriendInfoVC class]] ||
        
        [self isKindOfClass:[MurFilterVC class]]
        )
    {
        self.showTabBottom = YES;
        self.murV3 = TRUE;
    }
    if ([self isKindOfClass:[MurVC class]]) {
        
        [subview.myTitle setText:@"COMMUNAUTÉ"];
        [self addSubNav:@"SubNavigationMUR_V3"];
    }
    else if ([self isKindOfClass:[NotificationVC class]]) {
        [self addSubNav:nil];
    }
    else
    {
        [self addSubNav:@"SubNavigationMUR"];
    }
    
    [self updateStatusNavControls];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) fnDoRefreshNotification
{
    UIImageView *imgNotification =(UIImageView*)[self.subview viewWithTag:96];
    [self.subview badgingBtn:imgNotification count:[CommonObj sharedInstance].nbUnreadNotification];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //Change status bar color
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
    
    UIImageView *imgNotification =(UIImageView*)[self.subview viewWithTag:96];
    [self.subview badgingBtn:imgNotification count:[CommonObj sharedInstance].nbUnreadNotification];
    
    if ([self isKindOfClass:[MurVC class]]) {
        if (_isMoiMur) {
            [self.subview insertSubview:self.subview.btnLeMur atIndex:0];
            [self.subview.btnMonMur setBackgroundImage:[UIImage imageNamed:@"icon_le_mur_active"] forState:UIControlStateNormal];
            [self.subview.btnLeMur setBackgroundImage:[UIImage imageNamed:@"icon_le_mur_inactive"] forState:UIControlStateNormal];

        }
        else
        {
            [self.subview insertSubview:self.subview.btnMonMur atIndex:0];
            [self.subview.btnLeMur setBackgroundImage:[UIImage imageNamed:@"icon_le_mur_active"] forState:UIControlStateNormal];
            [self.subview.btnMonMur setBackgroundImage:[UIImage imageNamed:@"icon_le_mur_inactive"] forState:UIControlStateNormal];
        }

    }
    else
    {
        [self setThemeNavSub:NO withDicOption:nil];
        
        UIButton *btn = [self returnButton];
        [btn setSelected:YES];
        [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
    }
    
}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    
    [super onSubNavClick:btn];
    if ([self isKindOfClass:[MurVC class]]) {
        switch (btn.tag -START_SUB_NAV_TAG) {
            case 0://HOME
            {
                //[self doRemoveObservation];
                
                //[self.navigationController popToRootViewControllerAnimated:YES];
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate gotoApplication];
            }
                break;
                
            case 1://Search
            {
                if ([self isKindOfClass: [ResearchVC class]])
                    return;
                
                [self doPushVC:[ResearchVC  class] iAmParent:TRUE];
            }
                break;
                
                
            case 2://FILTER
            {
                [self fnShowFilterView];
            }
                break;
                
            case 3://DISCUSS
            {
                if ([self isKindOfClass: [ChatListe class]])
                    return;
                
                [self doPushVC:[ChatListe  class] iAmParent:TRUE expectTarget:ISDISCUSS];
            }
                break;
            case 4://LE MUR
            {
                if ([self isKindOfClass: [MurVC class]] && !_isMoiMur)
                    return;
                
                MurVC *viewController1 = [[MurVC alloc] initWithNibName:@"MurVC" bundle:nil];
                viewController1.expectTarget = self.expectTarget;
                viewController1.isMoiMur = NO;
                [self pushVC:viewController1 animate:NO];
            }
                break;
            case 5://MOI MUR
            {
                if ([self isKindOfClass: [MurVC class]] && _isMoiMur)
                    return;
                
                MurVC *viewController1 = [[MurVC alloc] initWithNibName:@"MurVC" bundle:nil];
                viewController1.expectTarget = self.expectTarget;
                viewController1.isMoiMur = YES;
                [self pushVC:viewController1 animate:NO];
                
            }
                break;
            case 6://NOTIFICATIONS
            {
                if ([self isKindOfClass: [NotificationVC class]])
                    return;
                
                [self doPushVC:[NotificationVC  class] iAmParent:TRUE expectTarget:ISMUR];
            }
                break;
            default:
                break;
        }
    }
    else
    {
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            
//            if (![self isKindOfClass: [MurVC class]])
//                [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES object: nil userInfo: nil];
//            
//
//            [self gotoback];
            
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }
                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self doRemoveObservation];

            [self gotoback];
        }
            break;
            
        case 1://MUR
        {
            if ([self isKindOfClass: [MurVC class]])
                return;
            
            [self doPushVC:[MurVC class] iAmParent:NO];
        }
            break;
            
            
        case 2://Filter
        {
            if ([self isKindOfClass: [MurFilterVC class]])
                return;
            
            [self doPushVC:[MurFilterVC class] iAmParent:NO];
        }
            break;
            
        case 3://SETTING
        {
            if ([self isKindOfClass: [MurSettingVC class]])
                return;
            
            [self doPushVC:[MurSettingVC class] iAmParent:NO];
        }
            
            break;
            
        default:
            break;
    }
    }
}

@end
