//
//  MeoteoView.h
//  Naturapass
//
//  Created by Manh on 12/2/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MeoteoView : UIView
@property (weak, nonatomic) IBOutlet UILabel           *lbTime;
@property (weak, nonatomic) IBOutlet UILabel           *lbMain;
@property (weak, nonatomic) IBOutlet UILabel           *lbRain;
@property (weak, nonatomic) IBOutlet UILabel           *lbTemperature;
@property (weak, nonatomic) IBOutlet UIImageView           *imgMain;

-(void)fnDataView:(NSDictionary*)dicWeather;
@end
