//
//  GroupEnterMurVC.m
//  Naturapass
//
//  Created by Manh on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupEnterMurVC.h"
#import "GroupesViewBaseCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MLKMenuPopover.h"
#import "CommentDetailVC.h"
#import "SignalerTerminez.h"
#import "FriendInfoVC.h"
#import "Define.h"
#import "MediaCell+Color.h"
#import "CommonHelper.h"
#import "MapLocationVC.h"
#import "MurEditPublicationVC.h"
#import "ChassesCreateOBJ.h"
#import "GroupCreateOBJ.h"
#import "MurPeopleLikes.h"
#import "DatabaseManager.h"
#import "MurView.h"
#import "AmisAddScreen1.h"
#import "GroupCreateOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateV2.h"
#import "Publication_FavoriteAddress.h"
#import "ChatListe.h"
#import "Publication_Carte.h"

static NSString *MDMediaCell = @"MediaCell";
static NSString *MDMediaCellID = @"MediaCellID";

@interface GroupEnterMurVC ()<WebServiceAPIDelegate>
{
    //array
    NSMutableArray                  *publicGroupArray;
    //
    int                             clickedbtntag;
    NSInteger                       selectedSection;
//    IBOutlet UIButton               *btnCreateEnterGroup;
    MurView                         *murView;

}
@property (nonatomic, strong) MediaCell *prototypeCell;
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;
@property (nonatomic,strong)   IBOutlet UIView      *viewContent;

@end

@implementation GroupEnterMurVC

#pragma mark - viewdid and init

- (void)viewDidLoad { 
    [super viewDidLoad];
    __weak typeof(self) wself = self;
    //add view mur
    /*
     [murView fnSetDataPublication:publicGroupArray];
     [murView stopRefreshControl];
     [murView startRefreshControl];
     [murView fnSetDataPublication:publicGroupArray];

     */
    murView = [[MurView alloc] initWithEVC:self expectTarget:self.expectTarget];
    murView.isLiveHunt = self.isLiveHunt;
    [murView setCallback:^(VIEW_ACTION_TYPE type, NSArray *arrData)
     {
         switch (type) {
             case VIEW_ACTION_REFRESH_TOP:
             {
                 [wself insertRowAtTop];
             }
                 break;
             case VIEW_ACTION_REFRESH_BOTTOM:
             {
                 [wself insertRowAtBottom];
             }
                 break;
             case VIEW_ACTION_UPDATE_DATA:
             {
                 [wself updateDataPublication:arrData];
             }
                 break;
             default:
                 break;
         }
     }];
//    _btnShortCut.hidden = !allow_add;
    if (self.expectTarget == ISGROUP) {
        [_btnShortCut setBackgroundImage:[UIImage imageNamed:@"ic_group_floating_btn"] forState:UIControlStateNormal];

//        if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"] intValue]< USER_ADMIN) {
//            
//            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
//                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
//                BOOL isShow = NO;
//                NSString *strQuerry = [NSString stringWithFormat:@" SELECT c_allow_add FROM tb_group WHERE ( c_user_id=%@ AND c_id=%@ ) ",sender_id,[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
//                
//                FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
//                
//                
//                while ([set_querry1 next])
//                {
//                    isShow = [set_querry1 boolForColumn:@"c_allow_add"];
//                }
//                //allow_add = [0 => ADMIN, 1 => ALL_MEMBERS]
//                btnCreateEnterGroup.hidden = !isShow;
//
//            }];
//        }
        
    }else{

        if (self.isLiveHunt) {
            
            [_btnShortCut setBackgroundImage:[UIImage imageNamed:@"livehunt_ic_add"] forState:UIControlStateNormal];
        }
        else
        {
            [_btnShortCut setBackgroundImage:[UIImage imageNamed:@"ic_add_new_chasse"] forState:UIControlStateNormal];
        }
        
//            btnCreateEnterGroup.hidden = !allow_add;
    }

    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(insertRowAtTop) name: NOTIFY_REFRESH_MES object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPublicationEdit:) name: NOTIFY_REFRESH_MES_EDIT object:nil];

    [[CommonHelper sharedInstance] setRoundedView:_btnShortCut toDiameter:_btnShortCut.frame.size.height /2];

    publicGroupArray =[NSMutableArray new];

    //offline...get from cache...userid/group/chasse    
    NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"%@_%d_%@",[COMMON getUserId],self.expectTarget, [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]  ] ];
    
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        [publicGroupArray  addObjectsFromArray:arrTmp];
        [murView fnSetDataPublication:publicGroupArray];
    }
    
    
//    if (self.isLiveHunt == NO)
//    {
//        if (self.isNotifi) {
//            self.isNotifi =!self.isNotifi;
//        }
////        [self insertRowAtTop];
//
//    }

    _lbMessage.text=EMPTY_GROUP_PUBLICATION;
    
    //reset
    [[ChassesCreateOBJ sharedInstance] resetParams];
    [[GroupCreateOBJ sharedInstance] resetParams];
    [self addShortCut];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPublicationNew:) name: NOTIFY_REFRESH_MES_NEW object:nil];
    
    [murView addContraintSupview:self.viewContent];

//    if (self.isLiveHunt) {
        if (self.isNotifi)
        {
            self.isNotifi =!self.isNotifi;
        }
        [self insertRowAtTop];

//    }
}
-(void)updateSubview
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //refresh UI
        if (self.expectTarget == ISLOUNGE)
        {
//            _btnShortCut.hidden = !allow_add;
            [self.viewShortCut fnAllowAdd:allow_add];
            [self insertRowAtTop];
        }
    });
}
-(BOOL)checkAddpublication:(NSDictionary*)publicationNew
{
    BOOL isAdd = NO;
    
    NSDictionary*dic = publicationNew;
    
    NSArray *arrGroups = dic[@"groups"];
    for (NSDictionary *dic in arrGroups) {
        if ([dic[@"id"] intValue] == [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] intValue]) {
            isAdd = YES;
            return isAdd;
        }
    }
    
    NSArray *arrHunts = dic[@"hunts"];
    for (NSDictionary *dic in arrHunts) {
        if ([dic[@"id"] intValue] == [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] intValue]) {
            isAdd = YES;
            return isAdd;
        }
    }
    return isAdd;
}
-(void)refreshPublicationNew:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    if (dic && [self checkAddpublication:dic]) {
        
        if (publicGroupArray.count>0) {
            [publicGroupArray insertObject:dic atIndex:0];
        }
        else
        {
            [publicGroupArray addObject:dic];
        }
        [self checkEmpty:publicGroupArray];
        [murView fnSetDataPublication:publicGroupArray];
        
    }
}
-(void)refreshPublicationEdit:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    if (dic[@"publication"]) {
        for (int i = 0; i < publicGroupArray.count; i++) {
            NSDictionary *dicItem = publicGroupArray[i];
            if ([dicItem[@"id"] intValue] == [dic[@"publication"][@"id"] intValue]) {
                [publicGroupArray replaceObjectAtIndex:i withObject:dic[@"publication"]];
                break;
            }
        }
        [murView fnSetDataPublication:publicGroupArray];
    }
}
#pragma refresh action
- (void)insertRowAtTop {
    [self getGroupPublications:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
}

- (void)insertRowAtBottom {
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];

    //mld
    [serviceObj getPublicationsAction:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] forSharing:group_limit withOffset:[NSString stringWithFormat:@"%lu",(unsigned long)[publicGroupArray count] ] kind:  ( (self.expectTarget == ISGROUP) ? MYGROUP : @"hunts") ];
    __weak typeof(self) wself = self;

    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [murView stopRefreshControl];
        if([wself fnCheckResponse:response]) return;

        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSArray * arr = response[@"publications"];
        
        if (arr.count > 0) {
            [publicGroupArray addObjectsFromArray:response[@"publications"]];
            
            NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"%@_%d_%@",[COMMON getUserId],self.expectTarget, [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]  ] ];

            [publicGroupArray writeToFile:strPath atomically:YES];
            
            
            [murView fnSetDataPublication:publicGroupArray];
        }
    };
}

-(void)reloadTable {
    [COMMON removeProgressLoading];
    [self getGroupPublications:[[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue]];
}

#pragma mark - service
-(void)getGroupPublications:(NSString *)strGroup_id{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];

    __weak typeof(self) wself = self;

    [serviceObj getPublicationsAction:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]  forSharing:group_limit withOffset:@"0" kind:  (self.expectTarget == ISGROUP ) ? MYGROUP: @"hunts" ];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [murView stopRefreshControl];
        if ([wself fnCheckResponse:response]) {
            [self checkEmpty:publicGroupArray];
            return ;
        }
        if (!response) {
            [self checkEmpty:publicGroupArray];
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            [self checkEmpty:publicGroupArray];
            return ;
        }
        [publicGroupArray removeAllObjects];
        
        [publicGroupArray addObjectsFromArray:response[@"publications"]];
        //cache Mur group/chasse. via ID user
        
        NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"%@_%d_%@",[COMMON getUserId],self.expectTarget, [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]  ] ];
        
        [publicGroupArray writeToFile:strPath atomically:YES];

        
        [murView fnSetDataPublication:publicGroupArray];
        [self checkEmpty:publicGroupArray];
    };
}
-(void)checkEmpty:(NSMutableArray*)arr
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
    }
    else
    {
        self.lbMessage.hidden=NO;
    }
}
-(void)updateDataPublication:(NSArray*)arrData
{
    publicGroupArray = [arrData mutableCopy];
    [self checkEmpty:publicGroupArray];
    
}

- (IBAction)createPublication:(id)sender {
    NSString * strTmp = [ NSString stringWithFormat:@"%@", [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] ];
    
    [[PublicationOBJ sharedInstance]  resetParams];

    SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    
    if (self.expectTarget == ISGROUP) {
        [PublicationOBJ sharedInstance].groupID = strTmp;

    }else{
        [PublicationOBJ sharedInstance].huntID = strTmp;

    }
    

    [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
}
//MARK: short cut
-(void) addShortCut
{
    __weak typeof(self) wself = self;
    self.btnShortCut.hidden = NO;
    self.viewShortCut = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:ISMUR];
    [self.viewShortCut setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         wself.btnShortCut.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_ADD_FIREND:
             {
                 AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_AGENDA:
             {
                 [[ChassesCreateOBJ sharedInstance] resetParams];
                 ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_GROUP:
             {
                 [[GroupCreateOBJ sharedInstance] resetParams];
                 GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:YES];
                 
             }
                 break;
             case SHORTCUT_ACTION_DISCUSTION:
             {
                 ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISDISCUSS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
                 viewController1.isFromSetting = YES;
                 [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_PUBLICATION:
             {
                 [wself createPublication: nil];
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCut.constraintBottomCloseButton.constant = 74;
    self.viewShortCut.constraintTraillingCloseButton.constant = 17;
    [self.viewShortCut addContraintSupview:self.view];
    self.viewShortCut.hidden = YES;
    [self.viewShortCut fnAllowAdd:allow_add];
}
-(IBAction)fnShortCut:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.btnShortCut.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        self.btnShortCut.hidden = YES;
        [self.viewShortCut hide:NO];
        [self returnRotation];
    }];
}

-(void) returnRotation{
    [UIView animateWithDuration:0.1 animations:^{
        self.btnShortCut.transform = CGAffineTransformIdentity;
    }];
}

@end
