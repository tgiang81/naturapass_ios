//
//  UploadProgress.m
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AlertViewMembersInvite.h"
#import "CellKind12.h"
#import "AppCommon.h"
#import "CellKind3.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "MDCheckBox.h"
#import "GroupCreateCell_Step4.h"
static NSString *cellkind3 =@"GroupCreateCell_Step4";
static NSString *cellkin3ID =@"CellKind3ID";
@interface AlertViewMembersInvite ()
{
    IBOutlet UITableView *tableControl;
    IBOutlet UIView  *viewHearder;

    NSString *sender_id;
    UIColor *color;
    int intType;
}
@end

static NSString *identifier = @"CellKind12ID";

@implementation AlertViewMembersInvite
- (id)initWithArrayMembers:(NSMutableArray*)arrMembers  wihtID:(NSString*)strID
{
    self = [super initWithNibName:@"AlertViewMembersInvite" bundle:nil];
    if (self) {
        // Custom initialization
        _arrMembers=arrMembers;
        _strID = strID;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    viewHearder.backgroundColor = UIColorFromRGB(0xE77321);
    [tableControl registerNib:[UINib nibWithNibName:cellkind3 bundle:nil] forCellReuseIdentifier:cellkin3ID];
    sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    switch (self.expectTarget) {
        case ISMUR://MUR
        {
            color = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        case ISCARTE://CARTE
        {
            color = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            
        }
            break;
        case ISDISCUSS://DISCUSSION
        {
            color = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
            
        }
            break;
        case ISGROUP://GROUP
        {
            color = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            intType = UI_GROUP_MUR_ADMIN;

            
        }
            break;
        case ISLOUNGE://CHASSES
        {
            color = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            intType = UI_CHASSES_MUR_ADMIN;
            
        }
            break;
        case ISAMIS://AMIS
        {
            color = UIColorFromRGB(AMIS_MAIN_BAR_COLOR);
            
        }
            break;
        case ISPARAMTRES://PARAMETERS
        {
            color = UIColorFromRGB(PARAM_MAIN_BAR_COLOR);
            
        }
            break;
        default:
            break;
    }
    viewHearder.backgroundColor = color;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrMembers count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        NSDictionary*dic = _arrMembers[indexPath.row];
    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[tableControl dequeueReusableCellWithIdentifier:cellkin3ID forIndexPath:indexPath];
    [cell.btnInvite setTypeCheckBox:intType];

    [cell.lblTitle setText:dic[@"user"][@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"user"][@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([dic[@"inviter"] boolValue]) {
        [cell.btnInvite setSelected:YES];
    }
    else
    {
        [cell.btnInvite setSelected:NO];
    }
    [cell.btnInviteOver addTarget:self action:@selector(fnInviteMember:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return  cell;
}

-(IBAction)fnInviteMember:(MDCheckBox*)sender
{
    int index = (int) sender.tag;
    NSDictionary * dic = _arrMembers[index];
    
    if ([sender isSelected]) {
        NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
        if (mulDic[@"inviter"]) {
            [mulDic removeObjectForKey:@"inviter"];
        }
        [_arrMembers replaceObjectAtIndex:index withObject:mulDic];
        [tableControl reloadData];
    }
    else
    {
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        if (self.expectTarget == ISLOUNGE) {
            [serviceObj postUserFriendsAction: _strID andFriend:dic[@"user"][@"id"]];
        }
        else
        {
          [serviceObj postGroupInviteFriendNaturapass: _strID andFriend:dic[@"user"][@"id"]];
        }
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            if (response[@"success"]) {
                NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:dic];
                if (mulDic[@"inviter"]) {
                    [mulDic removeObjectForKey:@"inviter"];
                }
                [mulDic setObject:@"1" forKey:@"inviter"];
                [_arrMembers replaceObjectAtIndex:index withObject:mulDic];
                [tableControl reloadData];
            }
        };
    }
}
-(IBAction)fnDismiss:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
