//
//  AlertSuggestCell.h
//  Naturapass
//
//  Created by JoJo on 4/12/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertSuggestCell : UITableViewCell
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
    @property (nonatomic,strong) IBOutlet UIImageView *imgCheck;
@property (nonatomic,strong) IBOutlet UIView *viewAvatar;
@property (nonatomic,strong) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintWidthAvatar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintWidthFriend;
@property (nonatomic,strong) IBOutlet UIImageView *imgFriend;

@end
