//
//  Publication_Ajout_Favoris.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Ajout_Favoris.h"
#import "PublicationVC.h"
#import "AZNotification.h"
#import "ServiceHelper.h"
#import "MapDataDownloader.h"
#import "DatabaseManager.h"

@interface Publication_Ajout_Favoris ()
{
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnRegister;

}
@end

@implementation Publication_Ajout_Favoris

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strAjout_de_la_specification_aux_favorisr);
    [btnRegister setTitle:  str(strENREGISTRER) forState:UIControlStateNormal];

    self.tfNom.placeholder = str(strNom_du_favori);
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(void)saveCache:(NSDictionary*)dic
{
//    [[ServiceHelper sharedInstance] fnGetFavories];
}
-(IBAction)onNext:(id)sender
{
    if ([self.tfNom.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return ;
    }
    [self addFavoriteWithName:self.tfNom.text withDic:self.dicFavo];
//        [self saveCache:dicFavo];
}

-(void)addFavoriteWithName:(NSString*)strName withDic:(NSDictionary*)dicFavo
{
    ASLog(@"1");
    /*
     77:      * POST /v2/publications/favorites
     78:      *
     79:      * {
     80:      *     "favorite": {
     81:      *       "name": "premier fav",
     82:      *       "legend": "ma legend",
     83:      *       "sharing":{"share":1,"withouts":[]},
     84:      *       "groups": [206],
     85:      *       "hunts": [],
     86:      *       "publicationcolor": "4",
     87:      *       "category": 137,
     88:      *       "card": 14,
     89:      *       "specific": 0,
     90:      *       "animal": null,
     91:      *       "attachments":[{"label":32,"value":1},{"label":34,"value":1}]
     92:      *     }
     93:      * }
     */
    /*
    {
        attachments =     (
                           {
                               label = 58;
                               name = "Ind\U00e9termin\U00e9";
                               value = 0;
                           },
                           {
                               label = 59;
                               name = "M\U00e2le";
                               value = 0;
                           },
                           {
                               label = 60;
                               name = Femelle;
                               value = 0;
                           },
                           {
                               label = 61;
                               name = Jeune;
                               value = 0;
                           }
                           );
        color =     {
            color = 000000;
            id = 8;
            name = noir;
        };
        iSharing = 0;
        legend = b;
        observation =     {
            attachments =         (
                                   {
                                       label = 58;
                                       value = 0;
                                   },
                                   {
                                       label = 59;
                                       value = 0;
                                   },
                                   {
                                       label = 60;
                                       value = 0;
                                   },
                                   {
                                       label = 61;
                                       value = 0;
                                   }
                                   );
            category = 149;
            specific = 0;
        };
        precision = "Animaux/Tu\U00e9 chasse/Oiseaux/Faisan v\U00e9n\U00e9r\U00e9, Faisans de Chasse";
        sharingGroupsArray =     (
                                  {
                                      categoryName = Dodd;
                                      groupID = 404;
                                      isSelected = 1;
                                  }
                                  );
        sharingHuntsArray =     (
        );
    }
    */
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    NSArray *arrHunt =nil;
    NSArray *arrGroup =nil;

    NSMutableDictionary *dicPost = [NSMutableDictionary new];
    if (strName) {
        [dicPost setValue:strName forKey:@"name"];
    }
    
    if (dicFavo[@"card_id"]) {
        [dicPost setValue:dicFavo[@"card_id"] forKey:@"card"];
    }
    
    if (dicFavo[@"legend"]) {
        [dicPost setValue:dicFavo[@"legend"] forKey:@"legend"];
    }
    
    if (dicFavo[@"iSharing"]) {
        [dicPost setValue:@{@"share":dicFavo[@"iSharing"]} forKey:@"sharing"];
    }
    
    if (dicFavo[@"sharingHuntsArray"]) {
        arrHunt =[self convertArray2ArrayID:dicFavo[@"sharingHuntsArray"] forGroup:NO];
        [dicPost setValue:arrHunt forKey:@"hunts"];
    }
    
    if (dicFavo[@"sharingGroupsArray"]) {
        arrGroup= [self convertArray2ArrayID:dicFavo[@"sharingGroupsArray"] forGroup:YES];
        [dicPost setValue:arrGroup forKey:@"groups"];
    }
    if (dicFavo[@"sharingUsers"]) {
        arrGroup= [self convertPersonneArray2ArrayID:dicFavo[@"sharingUsers"]];
        [dicPost setValue:arrGroup forKey:@"users"];
    }
    if (dicFavo[@"color"]) {
        [dicPost setValue:dicFavo[@"color"][@"id"] forKey:@"publicationcolor"];
    }

    //Default
    [dicPost setValue:@"0" forKey:@"specific"];

    if (dicFavo[@"observation"]) {
        if (dicFavo[@"observation"][@"attachments"]) {
            NSMutableDictionary *dicObservation = [dicFavo[@"observation"]  mutableCopy];
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{@"label": dic[@"label"],
                                       @"value": dic[@"value"]}];
            }
            [dicPost setValue:newAttach forKey:@"attachments"];
        }

        if (dicFavo[@"observation"][@"category"]) {
            [dicPost setValue:dicFavo[@"observation"][@"category"] forKey:@"category"];
        }
        if (dicFavo[@"observation"][@"card"]) {
            [dicPost setValue:dicFavo[@"observation"][@"card"] forKey:@"card"];
        }
        if (dicFavo[@"observation"][@"animal"]) {
            [dicPost setValue:dicFavo[@"observation"][@"animal"] forKey:@"animal"];
        }
        if (dicFavo[@"observation"][@"specific"]) {
            [dicPost setValue:dicFavo[@"observation"][@"specific"] forKey:@"specific"];
        }
    }
    
    [COMMON addLoading:self];

        [serviceObj fnPOST_PUBLICATION_FAVORITES:@{@"favorite":dicPost}];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){


                //reset
                [COMMON removeProgressLoading];
                
                if ([response[@"favorite"] isKindOfClass:[NSDictionary class]]) {
                  
                    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                        NSArray *arrSqls = response[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            ASLog(@"%@", tmpStr);
                            
                            BOOL isOK =   [db  executeUpdate:tmpStr];

                        }
                    }];
                    
                    //go back
                    NSArray * controllerArray = [[self navigationController] viewControllers];
                    
                    for (int i=0; i < controllerArray.count; i++) {
                        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                            
                            [self.navigationController popToViewController:self.mParent animated:YES];
                            
                            return;
                        }
                    }
                    
                    [self doRemoveObservation];

                    [self.navigationController popToRootViewControllerAnimated:YES];

                }else{
                    


                }
            

        };
    
}

- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}
-(NSArray*) convertArray2ArrayID:(NSArray*)arr forGroup:(BOOL) isGroup
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            if (isGroup) {
                [array addObject:dic[@"groupID"]];
                
            }else{
                [array addObject:dic[@"huntID"]];
            }
        }
    }
    return array;
}
-(NSArray*) convertPersonneArray2ArrayID:(NSArray*)arr
{
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"status"];
        if ( [num boolValue]) {
                [array addObject:dic[@"id"]];
        }
    }
    return array;
}
@end
