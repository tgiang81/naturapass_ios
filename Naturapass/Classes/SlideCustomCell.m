//
//  SlideCustomCell.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 1/28/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "SlideCustomCell.h"

@implementation SlideCustomCell

@synthesize tickButton;
@synthesize slideTitleLabel;
@synthesize slideImage;
@synthesize arrowImage;
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];
    
}
-(void)setListLabel:(NSString *)_text
{
    slideTitleLabel.text=_text;
}

-(void)setListImages:(NSString *)_text
{
    slideImage.image=[UIImage imageNamed:_text];
}

@end
