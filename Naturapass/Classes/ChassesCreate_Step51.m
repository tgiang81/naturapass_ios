//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step51.h"
#import "ChassesCreate_Step6.h"
#import "TypeCell61.h"
//
#import "GroupCreate_Step4.h"
#import "GroupCreate_Step5.h"
#import "GroupCreate_Step6.h"
#import "GroupCreate_Step8.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChassesCreateOBJ.h"

#import "GroupCreateCell_Step4.h"
#import <SDWebImage/UIImageView+WebCache.h>
static NSString *GROUPCREATECELLSTEP4 = @"GroupCreateCell_Step4";
static NSString *CELLID4 = @"cellid4";


@interface ChassesCreate_Step51 ()
{

    NSMutableArray *tourMemberArray;
}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;

@end

@implementation ChassesCreate_Step51

- (IBAction)onNext:(id)sender {
    ChassesCreate_Step6 *viewController1 = [[ChassesCreate_Step6 alloc] initWithNibName:@"ChassesCreate_Step6" bundle:nil];
    [self pushVC:viewController1 animate:YES];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initialization];
    self.needChangeMessageAlert = YES;

    [self.btnSuivant setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}

-(void)initialization
{
    tourMemberArray = [NSMutableArray new];

    [self.tableControl registerNib:[UINib nibWithNibName:GROUPCREATECELLSTEP4 bundle:nil] forCellReuseIdentifier:CELLID4];
    _label1.text =str(strInviterMesAmisNaturapass);
    _label2.text =str(strPourInviterDesAmis);
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    [serviceObj getTheFriends:YES withID:nil];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        [COMMON removeProgressLoading];
        
        [tourMemberArray removeAllObjects];
        
        NSArray *arrTmp = response[@"friends"] ;
        
        
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                     ascending:YES];
        //
        NSArray *results = [arrTmp sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        
        [tourMemberArray addObjectsFromArray:results ];

        // commented out old starting point :)
        //[results addObjectsFromArray:[all filteredArrayUsingPredicate:predicate]];
        
        // create a descriptor
        // this assumes that the results are Key-Value-accessible
        
        for (int i=0;i<tourMemberArray.count; i++) {
            NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
            [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
            [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
        }
        [self checkInvited];
        [self.tableControl reloadData];
    };
}


#pragma mark - TableView datasource & delegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tourMemberArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:CELLID4 forIndexPath:indexPath];
    [cell.btnInvite setTypeCheckBox:UI_CHASSES_MUR_ADMIN];
    
    NSDictionary * dic = tourMemberArray[indexPath.row];
    
    [cell.lblTitle setText:dic[@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([dic[@"check"] boolValue]) {
        [cell.btnInvite setSelected:YES];
    }
    else
    {
        [cell.btnInvite setSelected:NO];
    }
    [cell.btnInviteOver addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row;
    
    return  cell;
}
- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    
}

-(void)checkInvited
{
    for (int i=0; i<tourMemberArray.count; i++) {
        NSDictionary *dic=tourMemberArray[i];
        for (int j =0; j<self.arrSubcriber.count; j++) {
            NSDictionary *dic1 =self.arrSubcriber[j];
            if ((dic[@"id"] ==dic1[@"user"][@"id"]) && [dic1[@"access"] integerValue]>=USER_NORMAL) {
                [tourMemberArray removeObjectAtIndex:i];
            }
        }
    }
}
#pragma mark - Action
-(IBAction)addFriend:(UIButton *)sender{
    
    NSDictionary * dic = tourMemberArray[sender.tag];
    int i= (int)sender.tag;
    if ([dic[@"check"] boolValue]) {
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:NO] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }else{
        
        NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:tourMemberArray[i]];
        [dicMul setObject:[NSNumber numberWithBool:YES] forKey:@"check"];
        [tourMemberArray replaceObjectAtIndex:i withObject:dicMul];
    }
    
    [self.tableControl reloadData];
}


//{"groupe": {"subscribers": [1,3,10]}}
- (IBAction)inviteAllMyFriends:(id)sender {
    
    NSMutableArray *mutArr = [NSMutableArray new];
    
    for (int i=0; i < tourMemberArray.count; i++ )
    {
        NSDictionary*dic = tourMemberArray[i];
        if ([dic[@"check"] boolValue]) {
            [mutArr addObject:dic[@"id"]];
        }
    }
    
    if (mutArr.count > 0)
    {
        NSString *strMsg = @"";
        if ((int)mutArr.count  == 1) {
            strMsg = [NSString stringWithFormat:str(strVous_avez_envoye_invitation),(int)mutArr.count ];
        }else{
            strMsg = [NSString stringWithFormat:str(strVous_avez_envoyer_invitations),(int)mutArr.count ];
        }
        
        [UIAlertView showWithTitle:str(strTitle_app) message: strMsg
                 cancelButtonTitle:str(strOK)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                              [COMMON addLoading:self];
                              //Request
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj fnPOST_JOIN_USER_LOUNGE:@{@"lounge": @{@"subscribers": mutArr}} loungeID:[ChassesCreateOBJ sharedInstance].strID];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  /*
                                   subscribers =     (
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 219;
                                   },
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 161;
                                   },
                                   {
                                   access = 0;
                                   added = 1;
                                   "user_id" = 168;
                                   }
                                   );
                                   
                                   */
                                  [self gotoback];
                              };
                          }];
        
    }
}

@end
