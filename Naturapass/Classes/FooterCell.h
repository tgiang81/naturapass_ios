//
//  Card_Cell_Type10.h
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface FooterCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBrand;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintWidthImage;
@property (weak, nonatomic) IBOutlet UIView *viewBoder;

@end
