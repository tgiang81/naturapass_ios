//
//  SplashGif.m
//  Naturapass
//
//  Created by GiangTT on 11/18/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "SplashGif.h"
//#import "UIImage+animatedGIF.h"
#import "FLAnimatedImage.h"


@interface SplashGif ()
{
    __weak IBOutlet UIButton *LeftBtn;
    __weak IBOutlet UIButton *rightBtn;
    __weak IBOutlet UIButton *oneButton;
    
    int indexScreen;
}

@property (weak, nonatomic) IBOutlet FLAnimatedImageView *imgSplash;

@end

@implementation SplashGif

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    indexScreen = 0;
    
    [self showGif1];
    //Image
    
}

- (IBAction)fnLeft:(id)sender {
    switch (indexScreen) {
        case 1:
        {
            [self showGif1];
        }
            break;
        case 2:
        {
            [self showGif2];
            
        }
            break;
        case 3:
        {
            [self showGif3];
        }
            break;
        case 4:
        {
            [self showGif4];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)fnRight:(id)sender {
    switch (indexScreen) {
            
        case 1:
        {
            [self showGif3];
            
        }
            break;
        case 2:
        {
            [self showGif4];
            
        }
            break;
        case 3:
        {
            [self showGif5];
            
        }
            break;
        case 4:
        {
            self.imgSplash.image = nil;
            self.imgSplash.animationImages = nil;

            [self fnClose:nil];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)fnOne:(id)sender {
    switch (indexScreen) {
        case 0:
        {
            [self showGif2];
        }
            break;
        default:
            break;
    }
}

-(void) showGif1
{
    indexScreen = 0;

    oneButton.hidden = NO;
    LeftBtn.hidden = rightBtn.hidden = YES;

    
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"mur" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.imgSplash.animatedImage = animatedImage1;
    

    [oneButton setTitle:@"SUIVANT" forState:UIControlStateNormal];
    //Mur > Just « SUIVANT »
}

-(void) showGif2
{
    indexScreen = 1;

    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"carte" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.imgSplash.animatedImage = animatedImage1;
    
    oneButton.hidden = YES;
    LeftBtn.hidden = rightBtn.hidden = NO;
    [LeftBtn setTitle:@"PRECEDENT" forState:UIControlStateNormal];
    [rightBtn setTitle:@"SUIVANT" forState:UIControlStateNormal];
    
    //Carte > PRECEDENT and SUIVANT

}

-(void) showGif3
{
    indexScreen = 2;
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"groupe" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.imgSplash.animatedImage = animatedImage1;

    //Groupe > PRECEDENT and SUIVANT
    oneButton.hidden = YES;
    LeftBtn.hidden = rightBtn.hidden = NO;
    [LeftBtn setTitle:@"PRECEDENT" forState:UIControlStateNormal];
    [rightBtn setTitle:@"SUIVANT" forState:UIControlStateNormal];

}

-(void) showGif4
{
    indexScreen = 3;
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"agenda" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.imgSplash.animatedImage = animatedImage1;

    //Agenda > PRECEDENT and SUIVANT
    oneButton.hidden = YES;
    LeftBtn.hidden = rightBtn.hidden = NO;
    [LeftBtn setTitle:@"PRECEDENT" forState:UIControlStateNormal];
    [rightBtn setTitle:@"SUIVANT" forState:UIControlStateNormal];

}

-(void) showGif5
{

    indexScreen = 4;
    NSURL *url1 = [[NSBundle mainBundle] URLForResource:@"chat" withExtension:@"gif"];
    NSData *data1 = [NSData dataWithContentsOfURL:url1];
    FLAnimatedImage *animatedImage1 = [FLAnimatedImage animatedImageWithGIFData:data1];
    self.imgSplash.animatedImage = animatedImage1;

    //Discussions > PRECEDENT and TERMINER
    
    oneButton.hidden = YES;
    LeftBtn.hidden = rightBtn.hidden = NO;
    [LeftBtn setTitle:@"PRECEDENT" forState:UIControlStateNormal];
    [rightBtn setTitle:@"TERMINER" forState:UIControlStateNormal];
}

- (IBAction)fnClose:(id)sender {
    if (self.CallBackClose) {
        self.CallBackClose();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
