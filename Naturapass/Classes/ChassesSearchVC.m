//
//  ChassesToutesView.m
//  Naturapass
//
//  Created by Giang on 7/16/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesSearchVC.h"
#import "Config.h"
#import "AppCommon.h"

#import "WebServiceAPI.h"
#import "ASSharedTimeFormatter.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Define.h"
#import "UIAlertView+Blocks.h"


#import <SDWebImage/UIImageView+WebCache.h>
#import "TypeCellToutes.h" // neu muon back up tro lai doi TypeCellToutes ->TypeCell1
#import "MLKMenuPopover.h"
#import "GroupEnterMurVC.h"
#import "GroupEnterOBJ.h"
#import "GroupSearchAlertView.h"
#import "AlertViewMembers.h"
#import "FriendInfoVC.h"
#import "CCMPopupTransitioning.h"
#import "MapLocationVC.h"
#import "ChassesParticipeVC.h"

static NSString *typecell1 = @"TypeCellToutes";
static NSString *typecell1ID = @"TypeCell1ID";


#define FIELDS_COUNT                    3

@interface ChassesSearchVC () <UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,WebServiceAPIDelegate>{
    
    IBOutlet UIButton *btnMySalon;
    IBOutlet UIButton *btnAllSalons;
    
    NSMutableArray *arraySalonsList;
    
    IBOutlet UIView *viewAlertView;
    IBOutlet UIView *viewMessageView;
    IBOutlet UILabel *labelRejoinSalon;
    IBOutlet UILabel *labelRejoinDescp;
    IBOutlet UIButton *btnClose;
    
    IBOutlet UIView *viewMesSalonCollection;
    
    UINavigationController *mesSalonNavigation;
    UIView                  *refreshHeaderView;
    UILabel                 *refreshLabel;
    UIImageView             *refreshArrow;
    UIImageView             *refreshbg;
    UIActivityIndicatorView *refreshSpinner;
    UILabel                         *timeLabel;
    
    NSString                        *accessValue;
    NSString                        *textPull;
    NSString                        *textRelease;
    NSString                        *strName;
    int             buttontag;
    NSMutableArray *buttonClickedArray;
    NSMutableArray *validerBtnClickedArray;
    UIButton *tempRejoindreButton;
    IBOutlet UISearchBar                *toussearchBar;
    NSMutableArray      *loungesArray;
    WebServiceAPI *loungeAction;
    __weak IBOutlet UILabel *lbTitle;
}
@property (nonatomic, strong) TypeCellToutes *prototypeCell;

@end
@implementation ChassesSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strTousLesEvenements);
    [self intialization];
}
-(void)intialization{
    //init tableview cell
    [self.tableControl registerNib:[UINib nibWithNibName:typecell1 bundle:nil] forCellReuseIdentifier:typecell1ID];
    
    loungesArray=[[NSMutableArray alloc]init];
    buttonClickedArray=[[NSMutableArray alloc]init];
    validerBtnClickedArray=[[NSMutableArray alloc]init];
    
    [suscriberView setHidden:YES];
    
    [self initRefreshControl];
    
    self.tableControl.estimatedRowHeight = 150;
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_CHASS_SEARCH" ] ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        if (arrTmp.count > 0) {
            [loungesArray  addObjectsFromArray:arrTmp];
            
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.tableControl reloadData];
        });
    });
    
    if ([COMMON isReachable]) {
        [self performSelector:@selector(startRefreshControl) withObject:nil afterDelay:0.5];
    }
    
    [toussearchBar setTintColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
    
    loungeAction = [[WebServiceAPI alloc]init];
    
}
//overwrite
- (void)insertRowAtTop {
    [self getLounchesOffset:NO];
    
}

//overwrite
- (void)insertRowAtBottom {
    [self getLounchesOffset:YES];
    
}

#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self getLounchesOffset:NO];
}

- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    //    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    //    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    [self getLounchesOffset:NO];
    [toussearchBar setShowsCancelButton:NO animated:YES];
    
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [theSearchBar resignFirstResponder];
    
    [COMMON addLoading:self];
    [self getLounchesOffset:NO];
    [toussearchBar setShowsCancelButton:NO animated:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [toussearchBar setShowsCancelButton:YES animated:YES];
}
#pragma mark -API
-(void)getLounchesOffset:(BOOL)isMore{
    NSString *strSearchBar = toussearchBar.text?toussearchBar.text:@"";
    NSString *strQuery =@"";
    if (![strSearchBar isEqualToString:@""]) {
        strQuery =[NSString stringWithFormat:@"&filter=%@",strSearchBar];
    }
    
    NSInteger offset=0;
    if (isMore) {
        offset =[loungesArray count];
    }
    [loungeAction getLoungesAction:lounge_limit withOffset:[NSString stringWithFormat:@"%ld",(long)offset] andQuery:strQuery];
    
    __weak ChassesSearchVC *myWeek = self;
    
    loungeAction.onComplete = ^(NSDictionary *response, int code)
    {
        [myWeek stopRefreshControl];
        [COMMON removeProgressLoading];
        if([myWeek fnCheckResponse:response]) return;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        NSArray *array = [response objectForKey:@"lounges"];
        [myWeek buildData:array :isMore ];
    };
}

-(void) buildData:(NSArray*)arr :(BOOL)isMore
{
    if (isMore == NO) {
        [loungesArray removeAllObjects];
    }
    
    [loungesArray addObjectsFromArray:arr];
    
    //Save
    NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"CACHE_CHASS_SEARCH" ] ];
    [loungesArray writeToFile:strPath atomically:YES];
    
    [self.tableControl reloadData];
    
}
#pragma mark -  TableView Delegates
- (TypeCellToutes *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:typecell1ID];
    }
    return _prototypeCell;
}

- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[TypeCellToutes class]])
    {
        TypeCellToutes *cell = (TypeCellToutes *)cellTmp;
        cell.Img4.hidden=YES;
        NSMutableArray *arrData =[NSMutableArray new];
        
        arrData =[loungesArray copy];
        
        if([arrData count]>0){
            //
            NSDictionary *dic = arrData[indexPath.row];
            
            //location
            [cell.btnLocation addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnLocation.tag = indexPath.row + 200;
            
            NSString *loungDateString=@"";
            loungDateString=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"end_date"]];
            if([loungDateString isEqualToString:@"(null)"])
                loungDateString=[NSString stringWithFormat:@"%@",dic[@"endDate"]];
            NSString * outputString = [self convertDate:loungDateString] ;
            
            strName = [dic[@"name"] emo_emojiString];

            if([strName isEqualToString:@"(null)"]||strName==nil)
            {
                strName = [dic[@"lounge"][@"name"] emo_emojiString];
            }
            cell.label1.text=strName;
            
            if(![loungDateString isEqualToString:@"(null)"])
                cell.label9.text=[NSString stringWithFormat:@"%@", outputString];
            
            //ADDRESS
            NSString*strLocation;
            if(dic[@"meetingAddress"][@"address"]!=nil){
                strLocation=[NSString stringWithFormat:@"%@",dic[@"meetingAddress"][@"address"]];
                [cell.label9 setText:strLocation];
            }
            else {
                [cell.label9 setText: [ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meetingAddress"][@"latitude"] floatValue],
                                       [dic[@"meetingAddress"][@"longitude"] floatValue]
                                       ] ];
            }
            NSString *strText = [dic[@"description"] emo_emojiString];
            if([strText isEqualToString:@"(null)"]||strText==nil)
            {
                strText = [dic[@"lounge"][@"description"] emo_emojiString];
            }
            if ([strText isEqualToString:@"(null)"]) {
                strText=@"";
            }
            //DATE
            NSString * dateStrings = [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meetingDate"]]];
            if(dateStrings !=nil)
                [cell.label5 setText:[NSString stringWithFormat:@"%@",dateStrings]];
            else
                [cell.label5 setText:@""];
            
            //END DATE
            NSString *outputStrings= [self convertDate:[NSString stringWithFormat:@"%@",dic[@"endDate"]]];
            if(outputStrings !=nil)
                cell.label7.text = [NSString stringWithFormat:@"%@", outputStrings];
            else
                cell.label7.text = @"";
            
            [cell.label10 setText:strText];
            [cell.label10 setBackgroundColor:[UIColor clearColor]];
            [cell layoutIfNeeded];
            cell.button2.tag=indexPath.row+2;
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                strAccess= [NSString stringWithFormat:@"%@",dic[@"connected"][@"access"]];
            }
            else
            {
                strAccess=@"4";
            }
            NSInteger iAccess = 10;
            if (strAccess && ![strAccess isEqualToString:@""]) {
                iAccess=[strAccess intValue];
            }
            [cell.button2 setColorEnable:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            [cell.button2 setColorDisEnable:[UIColor lightGrayColor]];
            
            //refresh
            [cell.button2 removeTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.button2 removeTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            [cell.button2 removeTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
            
            switch (iAccess)
            {
                case USER_INVITED:
                    
                {
                    [cell.button2 setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
                    [cell.button2 setEnabled:YES];
                    [cell.button2 addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
                }
                    break;
                case USER_WAITING_APPROVE:
                {
                    [cell.button2 setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
                    [cell.button2 setEnabled:NO];
                }
                    break;
                    
                case USER_NORMAL:
                case USER_ADMIN:
                {
                    [cell.button2 setTitle:str(strACCEDER) forState:UIControlStateNormal];
                    [cell.button2 setEnabled:YES];
                    [cell.button2 addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
                }
                    break;
                case USER_REJOINDRE:
                {
                    [cell.button2 setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
                    [cell.button2 setEnabled:YES];
                    [cell.button2 addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
                }
                    break;
                default:
                    break;
            }
            
            NSString *strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"access"]];
            if([strAccessGroup isEqualToString:@"(null)"]||strAccessGroup==nil)
                strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"access"]];
            
            if(strAccessGroup.intValue == ACCESS_PRIVATE)
                cell.label2.text=str(strAccessPrivate);
            else if(strAccessGroup.intValue ==ACCESS_SEMIPRIVATE)
                cell.label2.text=str(strAccessSemiPrivate);
            else if(strAccessGroup.intValue==ACCESS_PUBLIC)
                cell.label2.text=str(strAccessPublic);
            cell.label3.text =[NSString stringWithFormat:@"%@ %@",dic[@"nbSubscribers"],strParticipants];
            
            NSString *strImage=dic[@"photo"];
            NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
            
            [cell.Img1 sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
            
            [cell fnSettingCell:UI_CHASSES_TOUTE];
            cell.Img3.hidden =YES;
            cell.label3.hidden =YES;
            cell.imgBackGroundSetting.hidden=YES;
            cell.imgSettingSelected.hidden=YES;
            cell.imgSettingNormal.hidden=YES;
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
        }
        
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [loungesArray count];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TypeCellToutes *cell = [self.tableControl dequeueReusableCellWithIdentifier:typecell1ID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark - MemberAction
-(IBAction)MemberButtonAction:(UIButton *)sender {
    NSInteger index =  sender.tag -1;
    [toussearchBar resignFirstResponder];
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[loungesArray copy];
    
    strLoungeID=arrData[index][@"id"];
    [COMMON addLoading:self];
    __weak typeof(self) wself = self;
    WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
    [loungeSubscribeAction getLoungeSubscribeFriendAction:strLoungeID];
    loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"subscribers"]];
        
        for (int i=0; i<arrMembers.count; i++) {
            NSDictionary *dic =[arrMembers objectAtIndex:i];
            if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                [arrMembers removeObjectAtIndex:i];
            }
        }
        [self showPopupMembers:arrMembers];
    };
    
}

-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    AlertViewMembers *viewController1 = [[AlertViewMembers alloc]initWithArrayMembers:arrMembers];
    viewController1.expectTarget =ISLOUNGE;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         
         [self pushVC:friend animate:YES expectTarget:ISMUR];
     }];
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = viewController1;
    popup.presentingController = self;
    [self presentViewController:viewController1 animated:YES completion:nil];
}

// FIXME: "Rejoindre" button for Portal 2
-(void) gotoGroup:(UIButton*)sender
{
    
    NSDictionary *dic = loungesArray [sender.tag-2];
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

- (void)cellRejoindre:(UIButton *)sender
{
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[loungesArray copy];
    
    [toussearchBar resignFirstResponder];
    if ([sender isSelected]) {
        return;
    }
    else{
        NSInteger btn_index=sender.tag-2;
        strLoungeID=arrData[btn_index][@"id"];
        NSInteger accessValueNumber = [arrData[btn_index][@"access"] integerValue];
        accessValue = [NSString stringWithFormat: @"%ld", (long) accessValueNumber];
        NSString *desc=@"";
        NSString *strNameGroup=arrData[btn_index][@"name"];
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            
            desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
            
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            //Join_Salon_Text
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            //Join_Salon_Text
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
        }
        
        else
        {
            NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
        }
        [COMMON addLoading:self];
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        [LoungeJoinAction postLoungeJoinAction:strLoungeID];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
            
            [COMMON removeProgressLoading];
            if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
                [self.tableControl reloadData];
                return ;
            }
            NSInteger access= [respone[@"access"] integerValue];
            if (access==NSNotFound) {
                [self.tableControl reloadData];
                return ;
            }
            [UIView animateWithDuration:0.3 animations:^{
            } completion:^(BOOL finished){}];
            [[[[UIApplication sharedApplication] delegate] window] addSubview:viewAlertView];
            [viewAlertView bringSubviewToFront:[UIApplication sharedApplication].keyWindow ];
            //set value with key =@"access"
            NSDictionary*dic = arrData[btn_index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(access) forKey:@"access"];
            
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            
            if (loungesArray.count > btn_index) {
                [loungesArray replaceObjectAtIndex:btn_index withObject:newDictConnect];
                [self.tableControl reloadData];
                
            }
            //            GroupSearchAlertView *vc = [[GroupSearchAlertView alloc] initWithTitle:strTile Desc:desc ];
            //            [self presentViewController:vc animated:NO completion:^{
            //
            //            }];
            
            //            vc.exp= ISLOUNGE;
            
            [UIAlertView showWithTitle:strTile message:desc
                     cancelButtonTitle:str(strYES)
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              }];
        };
    }
}
- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    NSInteger index = sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[loungesArray copy];
    
    strLoungeID=arrData[index][@"id"];
    
    NSInteger accessValueNumber = [arrData[index][@"access"] integerValue];
    
    accessValue = [NSString stringWithFormat: @"%ld", (long) accessValueNumber];
    NSString *desc=@"";
    NSString *strNameGroup=[arrData objectAtIndex:index][@"name"];
    NSString *strTile=@"";
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
        
        strTile = str(strRejoinAcceptPublic);
        
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
        
        
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
    }
    else
    {
        NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
    }
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];    
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
        
        [COMMON removeProgressLoading];
        if([respone isKindOfClass: [NSArray class] ]) {
            return ;
        }
        BOOL access= [respone[@"success"] boolValue];
        if (access) {
            NSDictionary*dic = arrData[index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(2) forKey:@"access"];
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [loungesArray replaceObjectAtIndex:index withObject:newDictConnect];
            
        }
        ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
        viewController1.dicChassesItem =arrData[index];
        viewController1.isInvitionAttend =YES;
        [viewController1 doCallback:^(NSString *strID) {
            GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
            
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup = arrData[index];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        }];
        [self pushVC:viewController1 animate:YES];
        //        vc.exp =ISLOUNGE;
        [self.tableControl reloadData];
    };
}
-(void)rejoindreCommitAction:(UIButton *)sender{
    NSInteger index = sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[loungesArray copy];
    
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),arrData[index][@"name"]];
    [toussearchBar resignFirstResponder];
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}

- (IBAction)locationButtonAction:(UIButton*) sender
{
    int index = (int) sender.tag - 200;
    NSDictionary *myDic = loungesArray[index];
    
    id dic = myDic[@"meetingAddress"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = NO;
        viewController1.isSpecial = YES;
        
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"]};
        
        viewController1.needRemoveSubItem = NO;
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = myDic;
        
        [self pushVC:viewController1 animate:YES expectTarget: ISLOUNGE];
    }
    
    /*
     meetingAddress =     {
     address = "San Francisco";
     latitude = "37.785834";
     longitude = "-122.406417";
     };
     */
}

@end
