//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesParticipeVC.h"
#import "ChassesParameter_Notification_Email.h"
#import "ChassesParameter_Notification_Smartphones.h"
#import "CellKind2.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ChassesParticipeVC ()
{
    NSArray * arrData;
    NSString                    *numberOption;

    __weak IBOutlet UILabel *lbTitle;
}
@property(nonatomic,strong) NSString *mykindID;

@end

@implementation ChassesParticipeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strAllezVouzParticiper);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name":str(strJe_participe),
                                   @"image":@"ic_participate"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strJe_ne_sais_pas),
                                   @"image":@"ic_pre_participate"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strJe_ne_participe_pas),
                                   @"image":@"ic_not_participate"} copy];

    arrData =  [@[dic1,dic2,dic3] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    if ([self.dicChassesItem[@"connected"] isKindOfClass:[NSDictionary class]]) {
        numberOption =[self.dicChassesItem[@"connected"][@"participation"] stringValue];

    }
    else
    {
        numberOption = @"-1";

    }
    self.mykindID =self.dicChassesItem[@"id"];


}
#pragma mark - callback
-(void)setCallback:(Callback)callback
{
    _callback=callback;
}
-(void)doCallback:(Callback)callback
{
    self.callback=callback;
}
#pragma mark - API
- (void) ParticipeAction
{
    NSString *strID =self.dicChassesItem[@"id"];
    [COMMON addLoading:self];
    NSDictionary * postDict = [NSDictionary dictionaryWithObjectsAndKeys:numberOption,@"participation", nil];
    
   NSString *myID = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI putLoungessuscriberParticipateAction:self.mykindID user_id:myID withParametersDic:postDict];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (_callback) {
            self.callback(strID);
        }
        if (!_isInvitionAttend) {
            [self gotoback];

        }
    };
    
}
#pragma mark - table delegate
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];

    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    //
    int option =4;
    switch ([numberOption integerValue]) {
        case 0:
        {
            option=2;
            
        }
            break;
            
        case 1:
        {
            option=0;
            
        }
            break;
        case 2:
        {
            option=1;
        }
            break;
        default:
            break;
    }
    
    if (_isInvitionAttend) {
        cell.imageCrown.hidden=YES;
    }
    else
    {
        
        if(option == indexPath.row)
        {
            cell.imageCrown.hidden=NO;
            cell.imageCrown.image=[UIImage imageNamed:@"ic_chasse_sucess"];
        }
        else
        {
            cell.imageCrown.hidden=YES;
        }
    }

    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.constraint_image_width.constant =40;
    cell.constraint_image_height.constant =40;
    cell.constraint_control_width.constant = 20;
    cell.constraintRight.constant = 5;
    btn.frame = CGRectMake(0, 0, btn.image.size.width , btn.image.size.height);
    [cell.viewControl addSubview:btn];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case 0:
        {
            numberOption=@"1";

        }
            break;
            
        case 1:
        {
            numberOption=@"2";

        }
            break;
        case 2:
        {
            numberOption=@"0";
            
        }
            break;
        default:
            break;
    }
    [self ParticipeAction];
}
@end
