//
//  ChassesCreateBaseCell.h
//  Naturapass
//
//  Created by JoJo on 8/2/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFTokenField.h"
#import <GoogleMaps/GoogleMaps.h>
#import "MDRadioButton.h"
#import "SAMTextView.h"
typedef void (^tokenDeleteViewCallback)(NSInteger index);
@interface ChassesCreateBaseCell : UITableViewCell<ZFTokenFieldDataSource, ZFTokenFieldDelegate,UITextViewDelegate>
{
    int iZoomDefaultLevel;

}
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbDesc;

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imgArrow;
@property (weak, nonatomic) IBOutlet UIButton *btnSelected;
@property (weak, nonatomic) IBOutlet UIImageView *imgCheckbox;
@property (weak, nonatomic) IBOutlet UIImageView *imgDropdown;

//DATE
@property (weak, nonatomic) IBOutlet UIView *viewDebut;
@property (weak, nonatomic) IBOutlet UILabel *lbDebutDate;
@property (weak, nonatomic) IBOutlet UILabel *lbDebutTime;

@property (weak, nonatomic) IBOutlet UIView *viewFin;
@property (weak, nonatomic) IBOutlet UILabel *lbFinDate;
@property (weak, nonatomic) IBOutlet UILabel *lbFinTime;
//TEXT FIELD
@property (weak, nonatomic) IBOutlet UITextField *tfInput;
@property (weak, nonatomic) IBOutlet UIView *viewInput;
@property (weak, nonatomic) IBOutlet UIImageView *imageLeftTF;

@property (weak, nonatomic) IBOutlet UIButton *btnDebutDate;
@property (weak, nonatomic) IBOutlet UIButton *btnDebutHour;

@property (weak, nonatomic) IBOutlet UIButton *btnFinDate;
@property (weak, nonatomic) IBOutlet UIButton *btnFinHour;

@property (weak, nonatomic) IBOutlet UIButton *btnAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenMap;

    @property (weak, nonatomic) IBOutlet UILabel *lbTitleAmis;
    @property (weak, nonatomic) IBOutlet UISwitch *swAmis;

@property (weak, nonatomic) IBOutlet UILabel *lbTout;
@property (weak, nonatomic) IBOutlet UILabel *lbAdmin;
@property (weak, nonatomic) IBOutlet UILabel *lbDiscuter;
@property (weak, nonatomic) IBOutlet UILabel *lbConsulter;

@property (weak, nonatomic) IBOutlet UIImageView *imgToutDiscuter;
@property (weak, nonatomic) IBOutlet UIImageView *imgAdminDiscuter;
@property (weak, nonatomic) IBOutlet UIImageView *imgToutConsulter;
@property (weak, nonatomic) IBOutlet UIImageView *imgAdminConsulter;

@property (weak, nonatomic) IBOutlet UIButton *btnToutDiscuter;
@property (weak, nonatomic) IBOutlet UIButton *btnAdminDiscuter;
@property (weak, nonatomic) IBOutlet UIButton *btnToutConsulter;
@property (weak, nonatomic) IBOutlet UIButton *btnAdminConsulter;
@property (weak, nonatomic) IBOutlet UIView *viewMap;
@property (weak, nonatomic) IBOutlet UIButton *btnCloseMap;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ratioMap;

@property (weak, nonatomic) IBOutlet  GMSMapView *mapView_;

@property (weak, nonatomic) IBOutlet UILabel *lbMediaLeft;
@property (weak, nonatomic) IBOutlet UILabel *lbMediaRight;
@property (weak, nonatomic) IBOutlet UIImageView *imgMediaLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgMediaRight;
@property (weak, nonatomic) IBOutlet UIButton *btnMediaLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnMediaRight;

@property (weak, nonatomic) IBOutlet UILabel *lbMediaThree;
@property (weak, nonatomic) IBOutlet UIImageView *imgMediaThree;
@property (weak, nonatomic) IBOutlet UIButton *btnMediaThree;

@property (weak, nonatomic) IBOutlet UIButton *btnChooseColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgChooseColor;
@property (weak, nonatomic) IBOutlet UIImageView *imgChooseColorBg;

@property (weak, nonatomic) IBOutlet UIView *viewPartager;
@property (weak, nonatomic) IBOutlet UILabel *lbPartager;
@property (weak, nonatomic) IBOutlet UIButton *btnPartager;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentLocation;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintWidthImageLeft;
@property (nonatomic,retain)IBOutlet MDRadioButton   *btnCheckBox;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintPaddingLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnExpandNode;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectedNode;
@property (weak, nonatomic) IBOutlet SAMTextView *textView;
@property (nonatomic,strong) IBOutlet UISwitch *swFilter;
@property (nonatomic,strong) IBOutlet UILabel *numberRemain;
@property (weak, nonatomic) IBOutlet UIImageView *imgCatCenter;

@property (nonatomic,assign) int indexTracking;

@property (weak, nonatomic) IBOutlet ZFTokenField *tokenField;
@property (strong, nonatomic) NSArray *tokens;
@property (nonatomic,copy) tokenDeleteViewCallback callbackTokenDelete;
-(void)addPersone:(NSArray*)arr;
-(void)doBlockTokenDelete:(tokenDeleteViewCallback ) cb;
-(void)fnSetLocation:(NSString*)latitude withLongitude:(NSString*)longitude;
@end
