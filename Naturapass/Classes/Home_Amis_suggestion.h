//
//  Home_Amis_suggestion.h
//  Naturapass
//
//  Created by Giang on 9/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Home_Amis_suggestion : UIView
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *amis_num;
@property (weak, nonatomic) IBOutlet UIButton *btnAjouter;
@property (weak, nonatomic) IBOutlet UIButton *btnFriendInfo;

@end
