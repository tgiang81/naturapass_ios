//
//  Parameter_Profile.m
//  Naturapass
//
//  Created by Giang on 10/12/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Parameter_PassProfile.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "PhotoSheetSignUpVC.h"
#import "AZNotification.h"
#import "CommonHelper.h"
@interface Parameter_PassProfile ()<UITextFieldDelegate,UIActionSheetDelegate,WebServiceAPIDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{

    IBOutlet UITextField *tfNewPW;
    IBOutlet UITextField *tfOldPW;
    IBOutlet UITextField *tfValidatePW;
    IBOutlet UIButton    *btnSuivant;
    IBOutlet UIScrollView       *myPersonalScrollView;
    NSDictionary         *dicProfile;
    IBOutlet UILabel *lbTitle;

}

@end

@implementation Parameter_PassProfile

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strCHANGER_DE_MOT_DE_PASSE);
    tfOldPW.placeholder = str(strMot_de_passe_actuel);
    tfNewPW.placeholder = str(strNouveau_mot_de_passe);
    tfValidatePW.placeholder = str(strVerification);

    [self InitializeKeyboardToolBar];
    dicProfile =[NSDictionary new];


    [tfNewPW setInputAccessoryView:self.keyboardToolbar];
    [tfOldPW setInputAccessoryView:self.keyboardToolbar];
    [tfValidatePW setInputAccessoryView:self.keyboardToolbar];
}

#pragma mark - Textfield Delegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}

- (IBAction)buttonDone:(id)sender {
    [self.view endEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
}

#pragma textfield delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}
   /**
          260:      * change le mdp de l'utilisateur connecté
          261:      *
          262:      * POST /v2/users/changepasswords
          263:      *
          264:      * user[current_password]
          265:      * user[new][first]
          266:      * user[new][second]
          267:      *
          268:      * @param Request $request
          269:      * @return \FOS\RestBundle\View\View
          270:      */

#pragma mark
-(IBAction)suivantAction:(id)sender
{

        NSString  *strUserDetails=[[NSUserDefaults standardUserDefaults]valueForKey:@"PASS"];
        NSString  *  strOldPassword = [COMMON encrypt:[NSString stringWithFormat:@"%@%@",SHA1_KEY,tfOldPW.text]];
        NSString  *  strNewPassword = [COMMON encrypt:[NSString stringWithFormat:@"%@%@",SHA1_KEY,tfNewPW.text]];
        NSString  * strPassword = [COMMON encrypt:[NSString stringWithFormat:@"%@%@",SHA1_KEY,tfValidatePW.text]];
        if (![tfOldPW.text length] || ![tfOldPW.text length] || ![tfValidatePW.text length]) {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strUpdate_Alert) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
            [COMMON removeProgressLoading];
            return;
        }
        else if(![strNewPassword isEqualToString:strPassword]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strPassword_Alert) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
            [COMMON removeProgressLoading];
            return;
        }
        else if(![strOldPassword isEqualToString:strUserDetails]){
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strOldPassword_Alert) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            [alert show];
            [COMMON removeProgressLoading];
            return;
        }
    NSDictionary *postDict =@{@"user":@{
                                 @"current_password":strOldPassword,
                                 @"new":@{
                                         @"first":strNewPassword,
                                         @"second":strPassword
                                         }
                                 }};
        WebServiceAPI *serviceAPI =[WebServiceAPI new];

        serviceAPI.onComplete =^(id response, int errCode)
        {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];

            [COMMON removeProgressLoading];
            
//            NSLog(@"%@", response);
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            if ([response valueForKey:@"success"]){
                [UIAlertView showWithTitle:str(strTitle_app)
                                   message:str(strMessage21)
                         cancelButtonTitle:str(strOK)
                         otherButtonTitles:nil
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                          [self gotoback];
                                  }];
            }
            else{
                NSString *errorMsg=response[@"message"];
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:errorMsg delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
                [alert show];
            }
        };
        
        [COMMON addLoading:self];
        
        [serviceAPI postChangePasswordAction:postDict];

}
@end
