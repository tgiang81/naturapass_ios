//
//  SignalerColorTextFieldCell.m
//  Naturapass
//
//  Created by JoJo on 8/3/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SignalerColorTextFieldCell.h"

@implementation SignalerColorTextFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.imgChooseColorBg.layer setMasksToBounds:YES];
    self.imgChooseColorBg.layer.cornerRadius= 4;
    
    [self.imgChooseColor.layer setMasksToBounds:YES];
    self.imgChooseColor.layer.cornerRadius= 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
