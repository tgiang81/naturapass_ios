//
//  MediaCell.m
//  Naturapass
//
//  Created by Giang on 8/11/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatLiveHuntCell.h"
@implementation ChatLiveHuntCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self bringSubviewToFront:self.btnDetail];
    self.lbHeader.text = str(strDISCUSSION);
    
    [self.imageProfile.layer setMasksToBounds:YES];
    self.imageProfile.layer.cornerRadius= 20;
    self.imageProfile.layer.borderWidth =0;
    self.imageProfile.layer.masksToBounds = YES;

}
@end
