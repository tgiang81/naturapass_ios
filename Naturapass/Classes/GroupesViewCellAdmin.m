//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "GroupesViewCellAdmin.h"
#import "Define.h"

@implementation GroupesViewCellAdmin

-(void) awakeFromNib
{
    [super awakeFromNib];
    UIView *tmpV = [self viewWithTag:POP_MENU_VIEW_TAG2];
    tmpV.backgroundColor =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    
    //
    [self.imgBackGroundSetting setImage:[UIImage imageNamed:@"group_ic_bg_setting"]];
    [self.imgSettingSelected setImage:[UIImage imageNamed:@"ic_admin_setting"]];
    [self.imgSettingNormal setImage:[UIImage imageNamed:@"group_ic_setting"]];
}

@end
