//
//  MyAlertView.m
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "VoisChiens_Document_Valider_Alert.h"

#import "Config.h"
#import "AppCommon.h"
@interface VoisChiens_Document_Valider_Alert ()
{
    NSArray *arrTitle;
}
@end

@implementation VoisChiens_Document_Valider_Alert

-(void)awakeFromNib{
    [COMMON listSubviewsOfView:self];

    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}
-(void)setTitle:(NSString*)title
{
    lbTitle.text =title;
}
-(IBAction)ouiAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(0);
    }
}
-(IBAction)nonAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(1);
    }
}
#pragma callback
-(void)setCallback:(MyAlertViewCbk)callback
{
    _callback=callback;
}

-(void)doBlock:(MyAlertViewCbk ) cb
{
    self.callback = cb;
    
}

@end
