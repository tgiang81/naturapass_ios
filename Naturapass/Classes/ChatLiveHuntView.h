//
//  ChatLiveHuntView.h
//  Naturapass
//
//  Created by Manh on 12/15/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatLiveHuntCell.h"
#import "ChatLiveHuntObsCell.h"
#import "ChatLiveHuntObsTextCell.h"

#import "ChatLiveHuntBaseCell.h"
#import "SAMTextView.h"
typedef void (^callLiveChatView) (NSDictionary*dic);
@interface ChatLiveHuntView : UIViewController <UIGestureRecognizerDelegate,UITextViewDelegate>
@property(nonatomic,strong) IBOutlet UITableView *tableControl;

@property (nonatomic,strong)   IBOutlet SAMTextView  *textView;
@property (nonatomic,strong)   IBOutlet UIView  *viewFooter;


@property(nonatomic,strong) NSDictionary *huntDic;

@property (nonatomic,assign)  BOOL allow_add_chat;
@property (nonatomic,assign)  BOOL allow_show_chat;


@property (nonatomic,assign)  BOOL allow_add;
@property (nonatomic,assign)  BOOL allow_show;

@property (nonatomic,assign)  BOOL showPopoutChat;
@property (nonatomic, copy) callLiveChatView callbackLiveChatview;
@property (nonatomic, strong)  UIToolbar                   *keyboardToolbar;
@property (nonatomic, assign)  int                   *widthViewChat;

-(void) connectMapLive;
-(void) closeSock;
-(void)moreRefreshControl;
-(void) refreshTextInput;
-(void)getLiveChatWithMore:(BOOL)isMore;
-(void)removeItemWithPublication:(NSDictionary*)dicPublication;
-(void)unSelectedItem;
@end
