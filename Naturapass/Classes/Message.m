//
//  Message.m
//  Whatsapp
//
//  Created by Rafael Castro on 6/16/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "Message.h"
#import "ASSharedTimeFormatter.h"
@implementation Message

-(id)init
{
    self = [super init];
    if (self)
    {
        self.sender = MessageSenderMyself;
        self.status = MessageStatusSending;
        self.text = @"";
        self.heigh = 44;
        self.identifier = @"";
        self.ownerID = nil;
    }
    return self;
}

+(Message *)messageFromDictionary:(NSDictionary *)dictionary
{
    Message *message = [[Message alloc] init];
    message.text = dictionary[@"text"];
    message.identifier = dictionary[@"message_id"];
    message.status = [dictionary[@"status"] integerValue] + 1;
    
//    NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSSSS";
    
    //Date in UTC
    NSDateFormatter *outputFormatterBis = [[NSDateFormatter alloc] init];
    
    [outputFormatterBis setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    
    [outputFormatterBis setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    
    [outputFormatterBis setDateFormat: @"d MMM H:mm"];
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",dictionary[@"sent"]];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    
    message.date = outputStrings;
    
    return message;
}

@end
