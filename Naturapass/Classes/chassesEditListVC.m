//
//  chassesEditListVC.m
//  Naturapass
//
//  Created by Manh on 11/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "chassesEditListVC.h"
#import "CellKind20.h"

#import "ChassesCreate_Step1.h"
#import "ChassesCreate_Step2.h"
#import "ChassesCreate_Step3.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreate_Step5.h"
#import "Etape9_Agenda_Groupe_selection.h"
#import "ChassesCreate_Step7.h"
#import "GroupAdminListVC.h"
#import "ChassesCreate_Publication_Discussion_Allow.h"


static NSString *identifierSection1 = @"MyTableViewCell1";

@interface chassesEditListVC ()
{
    NSArray * arrData;
    __weak IBOutlet UILabel *lblTitle;
}
@end

@implementation chassesEditListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strAdministrationDunEvenement);
    
    self.btnTerminer.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    // Do any additional setup after loading the view from its nib.
    
    
    NSMutableDictionary *dic1 = [@{@"name":kItem_1} copy];
    NSMutableDictionary *dic2 = [@{@"name":kItem_2} copy];
    NSMutableDictionary *dic3 = [@{@"name":kItem_3} copy];
    NSMutableDictionary *dic4 = [@{@"name":kItem_4} copy];
    NSMutableDictionary *dic5 = [@{@"name":kItem_5} copy];
    NSMutableDictionary *dic6 = [@{@"name":kItem_6} copy];
    NSMutableDictionary *dic7 = [@{@"name":kItem_7} copy];
    NSMutableDictionary *dic8 = [@{@"name":kItem_8} copy];
    NSMutableDictionary *dic9 = [@{@"name":kItem_9} copy];
    
    arrData =  [@[dic1,dic2,dic3,dic4,dic5,dic6,dic7,dic8,dic9] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind20" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind20 *cell = nil;
    
    cell = (CellKind20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    //FONT
    cell.name.text = dic[@"name"];
    
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrData[indexPath.row];
    
    NSString *strCheck = dic[@"name"];
    
    if ([strCheck isEqualToString:kItem_1]) {
        ChassesCreate_Step1 *viewController1 = [[ChassesCreate_Step1 alloc] initWithNibName:@"ChassesCreate_Step1" bundle:nil];
        [self pushVC:viewController1 animate:YES];
        
    } else if ([strCheck isEqualToString:kItem_2]) {
        ChassesCreate_Publication_Discussion_Allow *viewController1 = [[ChassesCreate_Publication_Discussion_Allow alloc] initWithNibName:@"ChassesCreate_Publication_Discussion_Allow" bundle:nil];
        viewController1.myTypeView = PUBLICATION_ALLOW_AGENDA;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        
    } else if ([strCheck isEqualToString:kItem_3]) {
        ChassesCreate_Publication_Discussion_Allow *viewController1 = [[ChassesCreate_Publication_Discussion_Allow alloc] initWithNibName:@"ChassesCreate_Publication_Discussion_Allow" bundle:nil];
        viewController1.myTypeView = CHAT_ALLOW_AGENDA;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
        
    } else if ([strCheck isEqualToString:kItem_4]) {
        ChassesCreate_Step2 *viewController1 = [[ChassesCreate_Step2 alloc] initWithNibName:@"ChassesCreate_Step2" bundle:nil];
        [self pushVC:viewController1 animate:YES];
    } else if ([strCheck isEqualToString:kItem_5]) {
        ChassesCreate_Step3 *viewController1 = [[ChassesCreate_Step3 alloc] initWithNibName:@"ChassesCreate_Step3" bundle:nil];
        [self pushVC:viewController1 animate:YES];
    } else if ([strCheck isEqualToString:kItem_6]) {
        ChassesCreate_Step5 *viewController1 = [[ChassesCreate_Step5 alloc] initWithNibName:@"ChassesCreate_Step5" bundle:nil];
        [self pushVC:viewController1 animate:YES];
    } else if ([strCheck isEqualToString:kItem_7]) {
        [self getTree];
        //        ChassesCreate_Step6 *viewController1 = [[ChassesCreate_Step6 alloc] initWithNibName:@"ChassesCreate_Step6" bundle:nil];
        //        [self pushVC:viewController1 animate:YES];
        ChassesCreate_Step7 *viewController1 = [[ChassesCreate_Step7 alloc] initWithNibName:@"ChassesCreate_Step7" bundle:nil];
        [self pushVC:viewController1 animate:YES];
    } else if ([strCheck isEqualToString:kItem_8]) {
        
        Etape9_Agenda_Groupe_selection *viewController1 = [[Etape9_Agenda_Groupe_selection alloc] initWithNibName:@"Etape9_Agenda_Groupe_selection" bundle:nil];
        [self pushVC:viewController1 animate:YES];

    } else if ([strCheck isEqualToString:kItem_9]) {
        GroupAdminListVC *vc = [[GroupAdminListVC alloc] initWithNibName:@"GroupAdminListVC" bundle:nil];
        //mld
        [vc fnGroupId:[ChassesCreateOBJ sharedInstance].strID
              isAdmin:YES];
        [self pushVC:vc animate:YES expectTarget:ISLOUNGE];
    }else{
        
    }
    
}
-(IBAction)modifiAction:(id)sender
{
    [self gotoback];
}
-(void)getTree
{
    NSString *strLatitude = [ChassesCreateOBJ sharedInstance].latitude;
    NSString *strLongitude = [ChassesCreateOBJ sharedInstance].longitude;
    
    if (strLatitude.length>0 && strLongitude.length>0 && [COMMON isReachable])
    {
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj getCategoriesGeolocated:strLatitude strLng:strLongitude];
        
        serviceObj.onComplete = ^(NSDictionary*response1, int errCode){
            //Has Data
            if (![response1[@"model"] isKindOfClass: [NSNull class]])
            {
                
                if ([response1[@"model"] isKindOfClass: [NSString class]])
                {
                    if ([response1[@"model"] isEqualToString:@"default"])
                    {
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                        // get tree/card from tree cache default
                    }
                    else
                    {
                        // Receiver_1....                                     "model": "receiver_7"
                        NSString *strReceiver = response1[@"model"];
                        
                        //check in the cache
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        if (treeDic[strReceiver] != [NSNull class] && treeDic[strReceiver])
                        {
                            [PublicationOBJ sharedInstance].treeCategory = treeDic[strReceiver];
                        }
                        else //doesn't exist...
                        {
                            strReceiver = [strReceiver stringByReplacingOccurrencesOfString:@"_" withString:@"="];
                            
                            //receiver_7
                            
                            WebServiceAPI *serviceGetTree =[WebServiceAPI new];
                            serviceGetTree.onComplete =^(id response2, int errCode){
                                if ([response2 isKindOfClass: [NSDictionary class]]) {
                                    
                                    //if not contained -> request get new tree... then merge result to the cache
                                    [[PublicationOBJ sharedInstance] doMergeTreeWithNew:response2];
                                    //Get needed tree
                                    
                                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                                    
                                    [PublicationOBJ sharedInstance].treeCategory = treeDic[response1[@"model"]];
                                }
                                
                            };
                            
                            [serviceGetTree fnGET_CATEGORIES_BY_RECEIVER:strReceiver];
                            
                        }
                    }
                }
                //A TREE
                else if ([response1[@"tree"] isKindOfClass: [NSArray  class]])
                {
                    [PublicationOBJ sharedInstance].treeCategory = response1[@"tree"];
                    
                    return;
                }else{
                    //Offline
                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                    
                    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                    
                }
            }else{
                //Offline
                NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                
                [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                
            }
        };
        
    }
    else
    {
        //Offline
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
    }
}
@end
