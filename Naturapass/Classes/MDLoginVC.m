//
//  MDLoginVC.m
//  Naturapass
//
//  Created by Manh on 7/23/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDLoginVC.h"
#import "MPGTextField.h"
#import "AppDelegate.h"
#import "Define.h"
#import "AppCommon.h"
#import "KeychainItemWrapper.h"

#import "SignUpViewController.h"
#import "ForgetPasswordViewController.h"

#import "AvatarViewController.h"

#import "ServiceHelper.h"
#import "AZNotification.h"
#import "KSToastView.h"
#import "UserSettingsHelper.h"
//FB
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AlertVC.h"
#import "Naturapass-Swift.h"

@interface MDLoginVC ()<UITextFieldDelegate,MPGTextFieldDelegate,REGISTERDELEGATE>
{
    IBOutlet UILabel *labelVersion;

    IBOutlet UILabel *labelTitle;
    IBOutlet UILabel *labelNatura;
    IBOutlet UILabel *labelPass;
    IBOutlet UILabel *labelForgotPassword;
    IBOutlet UILabel *labelOR;
    IBOutlet UILabel *resetLbl;
    IBOutlet MPGTextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    
    IBOutlet UIButton *buttonLogin;
    IBOutlet UIButton *buttonSignup;
    IBOutlet UIButton *resetConnectionBtn;
    IBOutlet UIButton *btnLoginFB;
    IBOutlet UIImageView *imgLogo;
    IBOutlet UILabel *lbTerm;

    NSMutableArray              *userArray;
    
    AppDelegate                 *appDelegate;
    
    
    NSMutableArray              *suggestList;
    NSArray                     *permissions;
    
    //native
    NSString                    *strPassword;
    KeychainItemWrapper         *keychain;
    //
    
}
@end

@implementation MDLoginVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    [imgLogo setImage:[UIImage imageNamed:strLogo]];
    
//    AnimationBuider ;
    
    //set 2 colors
    /*
     UIColor *color1 = [UIColor blackColor];
     NSDictionary *attrsVal1 = @{ NSForegroundColorAttributeName : color1 };
     
     UIColor *color2 = UIColorFromRGB(COLOR_LOGO_CCPA);
     NSDictionary *attrsVal2 = @{ NSForegroundColorAttributeName : color2 };
     
     NSMutableAttributedString *mTitleApp = [[NSMutableAttributedString alloc] initWithString:@"Inter" attributes:attrsVal1];
     NSMutableAttributedString *valueText1 = [[NSMutableAttributedString alloc] initWithString:@"actions" attributes:attrsVal2];
     NSMutableAttributedString *valueText2 = [[NSMutableAttributedString alloc] initWithString:@" Citoyennes" attributes:attrsVal1];
     
     [mTitleApp appendAttributedString:valueText1];
     [mTitleApp appendAttributedString:valueText2];
     
     [labelNatura setAttributedText:mTitleApp];
     */
    UIColor *color2 = UIColorFromRGB(COLOR_LOGO_CCPA);
    NSDictionary *attrsVal2 = @{ NSForegroundColorAttributeName : color2 };
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Conditions générales d’utilisation"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    
    [attributeString addAttribute:NSForegroundColorAttributeName
                            value:color2
                            range:(NSRange){0,[attributeString length]}];
    
    [lbTerm setAttributedText:attributeString];

    
    UIColor *color1 = [UIColor blackColor];
    NSDictionary *attrsVal1 = @{ NSForegroundColorAttributeName : color1 };
    
    
    NSMutableAttributedString *text0 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ",str(strLogin_Title)]];
    
    NSMutableAttributedString *text1 = [[NSMutableAttributedString alloc] initWithString:@"Natura" attributes:attrsVal2];
    NSMutableAttributedString *text2 = [[NSMutableAttributedString alloc] initWithString:@"pass" attributes:attrsVal1];
    
    [text0 appendAttributedString:text1];
    [text0 appendAttributedString:text2];
    
    [labelTitle setAttributedText:text0];
    
    self.navigationItem.hidesBackButton = YES;
    userArray=[[NSMutableArray alloc]init];
    [self configureUIElement];
    [self generateData];
    [textFieldEmail setDelegate:self];
    keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"ID" accessGroup:nil];
    [COMMON storeKeychain];
    
    
    
}

- (void)configureUIElement{
    
    [textFieldEmail    setText:@""];
    [textFieldPassword setText:@""];
    
    //Get Version Build
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];

    labelVersion.text = [NSString stringWithFormat:@"Ver 2.4.5 (%@)",version];
    
    [textFieldEmail     setPlaceholder:str(strEmail_Address)];
    [textFieldPassword  setPlaceholder:str(strPPassword)];
    
    [labelForgotPassword setText:str(strForgot_Password)];
    [labelOR             setText:str(strOr)];
    [resetLbl            setText:str(strReset_Connection)];
    
    [buttonLogin  setTitle:str(strLogin) forState:UIControlStateNormal];
    [buttonSignup setTitle:str(strRegister) forState:UIControlStateNormal];
    
    [resetConnectionBtn setSelected:YES];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RegisterConnect];
    [resetConnectionBtn setImage:[UIImage imageNamed:@"reset_active.png"] forState:UIControlStateNormal];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    permissions              = [[NSArray alloc] initWithObjects:@"offline_access",@"user_about_me",@"read_stream",@"user_about_me",@"user_birthday",@"email", @"user_photos", @"publish_stream", nil];
    
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([COMMON isLoggedIn]) {
        [self gotoSalonView];
    }else{
    }
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    //first time login
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"firstTimeLogin"] == FALSE)
    {
        
        [self fnTermNCondition: nil];
        
        [[NSUserDefaults standardUserDefaults] setBool: TRUE forKey:@"firstTimeLogin"];
        [[NSUserDefaults standardUserDefaults]synchronize];
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [COMMON removeProgressLoading];
    
    [super viewWillDisappear: animated];
}


#pragma mark - Store keychain


#pragma mark - Button Actions

- (void)gotoSalonView{
    [Flurry logEvent:@"do_finish_login" timed:YES];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate showSplashView];
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)login:(id)sender{
    [self.view endEditing:YES];
    [self doLogin];
}

-(IBAction)checkResetButtonAction:(id)sender{
    
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:RegisterConnect];
        [resetConnectionBtn setImage:[UIImage imageNamed:@"cb_inactive.png"] forState:UIControlStateNormal];
        
    }
    else{
        [sender setSelected:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:RegisterConnect];
        [resetConnectionBtn setImage:[UIImage imageNamed:@"reset_active.png"] forState:UIControlStateSelected];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//loadurl
- (void)webServiceDidSucceedSync:(WebServiceAPI *)webService responseDict:(id)response requestUrl:(NSString *)Url
{
    [COMMON removeProgressLoading];
    
    //ASLog(@"response: %@", response);
    
    if([COMMON isReachable]){
        
        ASLog(@"response=%@",response);
        
    }
    
}
- (void)webServiceDidFailSync:(WebServiceAPI *)webService error:(NSError *)error
{
    if([COMMON isReachable])
        [COMMON removeProgressLoading];
}


- (void) doLogin{
    if (![COMMON isReachable]) {
        
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:@""
                                                          message:str(strNETWORK)
                                                         delegate:self
                                                cancelButtonTitle:str(strOui)
                                                otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    
    [COMMON addLoading:self];
    
    strPassword = [NSString stringWithFormat:@"%@%@",SHA1_KEY,textFieldPassword.text];
    strPassword = [COMMON encrypt:strPassword];
    
    [[NSUserDefaults standardUserDefaults]setValue:strPassword forKey:@"PASS"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    // NSString *strDeviceToken=[[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICETOKEN"];
    //strDeviceToken=[OpenUDID value];
    
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
    
    if (!pushToken || ![pushToken length])
        ASLog(@"nil stoken");
    
    if ([self isValidData]){
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        ASLog(@"token: %@", pushToken);
        
        
        //hardcode

        //Romain

        
//        [serviceObj getRequest:@"https://naturapass.e-conception.fr/api/user/login?email=r.gimaret@e-conception.fr&password=c00895b84c3fde8f3a9cc5115bf79e02a0abd690&device=ios&identifier=(null)&name=iOS&authorized=0"];

        
        //Patrick
//        [serviceObj getRequest:@"https://www.naturapass.com/api/user/login?email=p.burdeyron@e-conception.fr&password=9d5d5462a0e99c1cc972fa45a9bc116ea90404cd&device=ios&identifier=(null)&name=iOS&authorized=0"];

        if (pushToken)
        {
            [serviceObj getRequest: REAL_LOGIN([textFieldEmail text], strPassword,  pushToken)];
        }
        else
        {
            [serviceObj getRequest: REAL_LOGIN_NO_PUSH([textFieldEmail text], strPassword)];
        }
//
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            
            if (errCode ==  404) {
                
                UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                                  message:response[@"message"]
                                                                 delegate:self
                                                        cancelButtonTitle:str(strOK)
                                                        otherButtonTitles:nil];
                [alert show];
                
                return ;
            }
            NSMutableDictionary *userDictionary = [response valueForKey:@"user"];
            if ([userDictionary isKindOfClass:[NSDictionary class]] && userDictionary != NULL) {
                NSString *user_id   = [userDictionary valueForKey:@"id"];
                NSString *user_sid  = [userDictionary valueForKey:@"sid"];
                if (user_id != NULL && user_sid != NULL) {
                    
                    [keychain setObject:textFieldEmail.text forKey:(__bridge id)kSecAttrAccount];
                    [keychain setObject:strPassword forKey:(__bridge id)kSecValueData];
                    BOOL exist = NO;
                    for (NSDictionary *dic in suggestList) {
                        if ([[dic objectForKey:@"email"] isEqualToString:[userDictionary objectForKey:@"email"]]) {
                            if (![dic[@"pwd"] isEqualToString:textFieldPassword.text]) {
                                [suggestList removeObject:dic];
                                exist = NO;
                                
                            }
                            else
                            {
                                exist = YES;
                            }
                            break;
                        }
                    }
                    
                    if (exist == NO) {
                        NSMutableDictionary *userPwdDictionary = [NSMutableDictionary dictionaryWithDictionary:userDictionary];
                        [userPwdDictionary setObject:textFieldPassword.text forKey:@"pwd"];
                        [suggestList addObject:userPwdDictionary];
                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:suggestList forKey:SUGGEST_LIST];
                    [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:@"sender_id"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:userDictionary[@"email"] forKey:@"my_email"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue: userDictionary[@"usertag"] forKey:@"user_tag"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:user_sid forKeyPath:@"PHPSESSID"];
                    [[NSUserDefaults standardUserDefaults] setValue:userDictionary forKeyPath:@"CURRENTUSERDETAILS"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    //set time
                    NSNumber * iTimes = [UserSettingsHelper getGameFairDisplayTime];
                    
                    if ([iTimes intValue] < 35) {
                        NSNumber *iCheck = [NSNumber numberWithInteger: [iTimes intValue]+1];
                        [UserSettingsHelper setGameFairDisplayTime: iCheck];
                    }
                    [self gotoSalonView];
                }
                else
                {
                    UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                                      message:str(strMessage27)
                                                                     delegate:self
                                                            cancelButtonTitle:str(strOK)
                                                            otherButtonTitles:nil];
                    [alert show];
                }
            }else{
                //??? timeout...
                [KSToastView ks_showToast:str(strRequestTimeout)  duration:4.0f completion: ^{
                }];
            }
            
            
        };
    }
}
- (BOOL)isValidData{
    
    if ([textFieldEmail.text isEqualToString:@""]) {
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                          message:str(strMessage28)
                                                         delegate:self
                                                cancelButtonTitle:str(strOK)
                                                otherButtonTitles:nil];
        [alert show];
        [COMMON removeProgressLoading];
        
        return NO;
    }
    if ([textFieldPassword.text isEqualToString:@""]) {
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                          message:str(strMessage29)
                                                         delegate:self
                                                cancelButtonTitle:str(strOK)
                                                otherButtonTitles:nil];
        [alert show];
        [COMMON removeProgressLoading];
        return NO;
    }
    
    return YES;
}

-(IBAction)forgetPasswordAction:(id)sender{
    ForgetPasswordViewController *forgetPasswordVC=[[ForgetPasswordViewController alloc]initWithNibName:@"ForgetPasswordViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:forgetPasswordVC];
    
    
    [self presentViewController:nav animated:YES completion:^{
    }];
    
}


- (IBAction)signUp:(id)sender{
    
    [textFieldEmail    resignFirstResponder];
    [textFieldPassword resignFirstResponder];
    SignUpViewController *signupView = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    signupView.registerDelegate=self;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:signupView];
    
    [self presentViewController:nav animated:YES completion:^{
    }];
    
}

- (void)generateData
{
    textFieldEmail.backgroundColor = [UIColor colorWithWhite:0.3 alpha:0.8];
    textFieldEmail.seperatorColor = [UIColor clearColor];
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Add code here to do background processing
        //
        //
        NSArray *contents = [[NSUserDefaults standardUserDefaults] objectForKey:SUGGEST_LIST];
        if (suggestList == nil) {
            suggestList = [[NSMutableArray alloc] init];
        } else {
            [suggestList removeAllObjects];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
            [suggestList addObjectsFromArray:contents];
        });
    });
}

#pragma mark MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField
{
    if ([textField isEqual:textFieldEmail]) {
        return suggestList;
    } else {
        return nil;
    }
}

- (BOOL)textFieldShouldSelect:(MPGTextField *)textField
{
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(NSDictionary *)result
{
    NSString *pwd = [result objectForKey:@"pwd"];
    if (pwd.length > 0) {
        textFieldPassword.text = [result objectForKey:@"pwd"];
    }
}

#pragma mark - TextField Delgate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == textFieldPassword) {
        [textFieldPassword resignFirstResponder];
        [self doLogin];
        
    }else{
        [textFieldEmail resignFirstResponder];
        
    }
    
    return YES;
}
-(void)loadUserValue:(NSString *)userName pass:(NSString *)passwords {
    
    textFieldEmail.text=userName;
    textFieldPassword.text=passwords;
    [self doLogin];
}

//TEST gtt
-(IBAction)fnReset:(id)sender
{
    //    NSUserDefaults *myDefault = [NSUserDefaults standardUserDefaults];
    //
    //
    //    [myDefault removeObjectForKey:strQueuePostingMessage];
    //    [myDefault removeObjectForKey:strQueuePostingPhoto];
    //    [myDefault removeObjectForKey:strQueuePostingVideo];
    //
    //    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
#pragma mark - FB
//id,name,gender,last_name,first_name,email,birthday",@"fields
-(IBAction)loginFBAction:(id)sender
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];   //ESSENTIAL LINE OF CODE
//    login.loginBehavior = FBSDKLoginBehaviorWeb;
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             
         } else if (result.isCancelled) {
         } else {
             if ([FBSDKAccessToken currentAccessToken]) {
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id,name,gender,last_name,first_name,email,birthday"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error) {
                          [COMMON saveFBDetails:result];
                          [self getFacebookLogin];
                          
                      }
                  }];
             }
         }
     }];
}
-(void)putFacebookEmailAction
{
    NSDictionary *userFBDict=   [[NSUserDefaults standardUserDefaults] valueForKey:@"FB_UserDetails"];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI_X =[WebServiceAPI new];
    [serviceAPI_X putRequestWithoutCookie:@{@"user":@{ @"fid":userFBDict[@"id"],
                                                       @"email": userFBDict[@"email"]}}];
    serviceAPI_X.onComplete =^(id response, int errCode)
    {
        ASLog(@"%@",response);
        
        //if update successful -> continue login with fb
        [COMMON removeProgressLoading];
        if (response[@"success"]) {
            [self getFacebookLogin];
        }
        else
        {
            NSString *errorMsg=@"";
            
            if (response[@"message"]) {
                errorMsg=[response valueForKey:@"message"];
            }
            else
            {
                errorMsg=@"Oups, une erreur s’est produite";
            }
            [self showErrorView:errorMsg];
            
        }
        
    };
}
-(void)postFacebookUserAction{
    NSDictionary *userFBDict=   [[NSUserDefaults standardUserDefaults] valueForKey:@"FB_UserDetails"];
    if (!userFBDict[@"email"]) {
        NSString *strErr = str(strMessage30);
        [KSToastView ks_showToast: strErr duration:5.0f completion: ^{
        }];
        return;
    }
    NSString *titleString;
    
    if ([userFBDict[@"gender"] isEqualToString: @"male"])
        titleString = @"1";
    else if ([userFBDict[@"gender"] isEqualToString: @"female"])
        titleString = @"2";
    else
        titleString = @"0";
    
    NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"add_camera_icon"],0.5);
    
    NSDictionary *postDictionary = @{@"user[courtesy]" :    titleString,
                                     @"user[firstname]" :   userFBDict[@"first_name"],
                                     @"user[lastname]" :    userFBDict[@"last_name"],
                                     @"user[email]" :       userFBDict[@"email"],
                                     @"user[facebook_id]" : userFBDict[@"id"]};
    
    NSDictionary *attachment = @{@"kFileName": @"image.png",
                                 @"kFileData": imageData};
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI postFacebookUserAction:postDictionary withAttachmentImage:attachment];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if([COMMON isReachable])
        {
            ASLog(@"response = %@",response);
            
            //Signup FB -> user exist with email… => put update that user with FB…
            
            NSString * userid=@"";
            userid= [response valueForKey:@"user_id"];
            if([userid isKindOfClass:[NSNumber class]])
            {
                [self getFacebookLogin];
                return ;
            }
            else
            {
                [self putFacebookEmailAction];
            }
            
        }
    };
    
}
- (void)getFacebookLogin{
    [COMMON addLoading:self];
    NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
    
    if (!pushToken || ![pushToken length])
        ASLog(@"nil stoken");
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    ASLog(@"token: %@", pushToken);
    [serviceObj getRequest: FB_LOGIN_API([COMMON getFBUserId],  pushToken)];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response || response[@"message"]) {
            [self postFacebookUserAction];
            return ;
        }
        NSMutableDictionary *userDictionary = [response valueForKey:@"user"];
        if ([userDictionary isKindOfClass:[NSDictionary class]] && userDictionary != NULL) {
            NSString *user_id   = [userDictionary valueForKey:@"id"];
            NSString *user_sid  = [userDictionary valueForKey:@"sid"];
            if (user_id != NULL && user_sid != NULL) {
                
                [keychain setObject:textFieldEmail.text forKey:(__bridge id)kSecAttrAccount];
                [keychain setObject:strPassword forKey:(__bridge id)kSecValueData];
                BOOL exist = NO;
                for (NSDictionary *dic in suggestList) {
                    if ([[dic objectForKey:@"email"] isEqualToString:[userDictionary objectForKey:@"email"]]) {
                        if (![dic[@"pwd"] isEqualToString:textFieldPassword.text]) {
                            [suggestList removeObject:dic];
                            exist = NO;
                            
                        }
                        else
                        {
                            exist = YES;
                        }
                        break;
                    }
                }
                
                if (exist == NO) {
                    NSMutableDictionary *userPwdDictionary = [NSMutableDictionary dictionaryWithDictionary:userDictionary];
                    [userPwdDictionary setObject:textFieldPassword.text forKey:@"pwd"];
                    [suggestList addObject:userPwdDictionary];
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:suggestList forKey:SUGGEST_LIST];
                [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:@"sender_id"];
                
                [[NSUserDefaults standardUserDefaults] setValue:userDictionary[@"email"] forKey:@"my_email"];
                
                [[NSUserDefaults standardUserDefaults] setValue: userDictionary[@"usertag"] forKey:@"user_tag"];
                
                [[NSUserDefaults standardUserDefaults] setValue:user_sid forKeyPath:@"PHPSESSID"];
                [[NSUserDefaults standardUserDefaults] setValue:userDictionary forKeyPath:@"CURRENTUSERDETAILS"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //set time
                NSNumber * iTimes = [UserSettingsHelper getGameFairDisplayTime];
                
                if ([iTimes intValue] < 35) {
                    NSNumber *iCheck = [NSNumber numberWithInteger: [iTimes intValue]+1];
                    [UserSettingsHelper setGameFairDisplayTime: iCheck];
                }
                
                [self gotoSalonView];
            }
            else
            {
                [appDelegate showErrMsg :str(strMessage26)];
            }
        }else{
            
            UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strTitle_app)
                                                              message:str(strMessage27)
                                                             delegate:self
                                                    cancelButtonTitle:str(strOK)
                                                    otherButtonTitles:nil];
            [alert show];
            
        }
        
        
    };
    
}
- (void)showErrorView:(NSString *)strMessage{
    //    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}

- (IBAction)fnTermNCondition:(id)sender {
    AlertVC *vc = [[AlertVC alloc] initTermAndCondition];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        if (index==0) {
            // NON
            exit(1);
        }
        else if(index==1)
        {
            //OUI
        }
    }];
    [vc showAlert];
}


@end
