//
//  ChassesCreateOBJ.h
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
#import "BaseVC.h"
#import "UIAlertView+Blocks.h"
#import "AppCommon.h"
@interface ChassesCreateOBJ : NSObject
+ (ChassesCreateOBJ *) sharedInstance;
@property (nonatomic, strong) NSString *strName;
@property (nonatomic, strong) NSString *strComment;
@property (nonatomic, strong) NSString *strFin;
@property (nonatomic, strong) NSString *strDebut;
@property (nonatomic, strong) NSData *imgData;
@property (nonatomic, strong) NSString *strID;
@property (nonatomic, assign) ACCESS_KIND accessKind;
@property (nonatomic, assign) BOOL isModifi;
@property (nonatomic, assign) BOOL isMurPass;


@property (nonatomic, strong) NSString *photo;

@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSArray *listGroupAdmin;
@property (nonatomic,strong) NSDictionary *attachment;
//id of group to involve join
@property (nonatomic, strong) NSString *group_id_involve;

@property (nonatomic, assign) BOOL     allow_add;
@property (nonatomic, assign) BOOL     allow_show;

@property (nonatomic, assign) BOOL     allow_chat_add;
@property (nonatomic, assign) BOOL     allow_chat_show;

-(void) resetParams;
-(void)modifiChassesWithVC:(UIViewController*)vc;
-(void)fnSetData:(NSDictionary*)dic;
@end
