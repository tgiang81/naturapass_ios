//
//  GroupCreate_Step3.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step3.h"
#import "GroupCreate_Step4.h"
#import "GroupCreate_Step5.h"
#import "GroupCreate_Step6.h"
#import "GroupCreate_Step7.h"
#import "GroupCreate_Step8.h"
#import "GroupCreate_Step9.h"
#import "GroupCreate_Step1.h"
#import "TypeCell31.h"
#import "GroupCreate_Step3.h"
#import "GroupCreateOBJ.h"
#import "GroupSettingMembres.h"
static NSString *typecell31 =@"TypeCell31";
static NSString *typecell31ID =@"TypeCell31ID";

@interface GroupCreate_Step3 ()
{
    NSMutableArray *arrData;
    NSMutableArray *arrAmis;
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDescription;

}


@end

@implementation GroupCreate_Step3
- (IBAction)onNext:(id)sender {
    if (self.fromSetting) {
        [self gotoback];
    }
    else
    {
        if ([GroupCreateOBJ sharedInstance].isModifi) {
            [self backEditListGroups];
        }
        else
        {
            GroupCreate_Step9 *viewController1 = [[GroupCreate_Step9 alloc] initWithNibName:@"GroupCreate_Step9" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }

    }
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.subview removeFromSuperview];
    [self addSubNav:nil];

    lbTitle.text = str(strInviterDesMembres);
    lbDescription.text = str(strPersonnesArejoindreVotreGroupe);

    [self initialization];
    // Do any additional setup after loading the view from its nib.
    if ([GroupCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
}

-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:typecell31 bundle:nil] forCellReuseIdentifier:typecell31ID];
    
    arrData = [NSMutableArray arrayWithArray:@[ @{@"name":str(strMes_amis),
                                                  @"image":strIC_group_amis},
                                                @{@"name":str(strMesGroupes),
                                                  @"image":strIC_group_invite_mesgroup},
                                                @{@"name":str(strAutres_Natiz),
                                                  @"image":strIC_group_member_natura},
                                                @{@"name":str(strNon_Natiz),
                                                  @"image":strIC_group_non_member},
                                                
                                                ]];
    
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI getTheFriends:YES withID:nil];
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        if (response!=nil) {
            if ([response isKindOfClass: [NSDictionary class]]) {
                
                arrAmis = [NSMutableArray arrayWithArray:response[@"friends"]];
                [self checkInvited];
               NSString*str = [NSString stringWithFormat: @"%@ (%d)",str(strMes_amis), (int) arrAmis.count];
                [arrData replaceObjectAtIndex:0 withObject:@{@"name":str,
                                                             @"image":strIC_group_amis}];
                [self.tableControl reloadData];
            }
        }
        
    };
    
    //Load mes group
    WebServiceAPI *serviceObjNumGroup = [[WebServiceAPI alloc]init];
    [serviceObjNumGroup getUserGroupsAction];
    //get groups that user joined
    serviceObjNumGroup.onComplete = ^(NSDictionary*response, int errCode){
        
        if (response !=nil) {
            
            NSArray *arrGroupes = response[@"groups"];
            if (arrGroupes.count > 0) {
                
            }
            
            //-1 not include this group
            NSString*str = [NSString stringWithFormat: @"%@ (%u)",str(strMesGroupes),   (int) arrGroupes.count >= 1 ? arrGroupes.count-1 : 0];
            
            [arrData replaceObjectAtIndex:1 withObject:@{@"name":str,
                                                         @"image":strIC_group_invite_mesgroup}];
            [self.tableControl reloadData];
        }
    };
}
-(void)checkInvited
{
    for (int i=0; i<arrAmis.count; i++) {
        NSDictionary *dic=arrAmis[i];
        for (int j =0; j<self.arrSubcriber.count; j++) {
            NSDictionary *dic1 =self.arrSubcriber[j];
            if ((dic[@"id"] ==dic1[@"user"][@"id"]) && [dic1[@"access"] integerValue]>=USER_NORMAL) {
                [arrAmis removeObjectAtIndex:i];
            }
        }
    }
}
#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeCell31 *cell = (TypeCell31 *)[tableView dequeueReusableCellWithIdentifier:typecell31ID];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.img1.image=  [UIImage imageNamed:dic[@"image"]];
    
    cell.label1.text = dic[@"name"];
    cell.button1.tag =indexPath.row;
    [cell.button1 addTarget:self action:@selector(choosesInvite:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(IBAction)choosesInvite:(id)sender
{
    NSInteger index =[sender tag];
    switch (index) {
        case 0:
        {
            //Mes amis
            GroupCreate_Step4 *viewController1 = [[GroupCreate_Step4 alloc] initWithNibName:@"GroupCreate_Step4" bundle:nil];
            viewController1.arrSubcriber =self.arrSubcriber;

            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }
            break;
        case 1:
        {
            //Mes groupes
            GroupCreate_Step5 *viewController1 = [[GroupCreate_Step5 alloc] initWithNibName:@"GroupCreate_Step5" bundle:nil];

            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }
            break;
        case 2:
        {
            //Autres Natiz
            GroupCreate_Step6 *viewController1 = [[GroupCreate_Step6 alloc] initWithNibName:@"GroupCreate_Step6" bundle:nil];

            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }
            break;
        case 3:
        {
            //Non-Natiz
            GroupCreate_Step8 *viewController1 = [[GroupCreate_Step8 alloc] initWithNibName:@"GroupCreate_Step8" bundle:nil];

            [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
        }
            break;
        default:
            break;
    }
}
@end
