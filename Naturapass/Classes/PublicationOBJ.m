//
//  PublicationOBJ.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PublicationOBJ.h"
#import "ServiceHelper.h"

#import "Reachability.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreLocation/CoreLocation.h>
#import "WebServiceAPI.h"
#import "NSDate+Extensions.h"
#import "FileHelper.h"
#import "CommonHelper.h"

@implementation PublicationOBJ

static PublicationOBJ *sharedInstance = nil;

+ (PublicationOBJ *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}


-(void)fnSetValueForKeyWithPublication:(NSDictionary*)dic
{
    
    [self resetParams];
    self.isEditer =YES;
    if ([dic[@"sharing"] isKindOfClass:[NSDictionary class]]) {
        self.iShare = [dic[@"sharing"][@"share"] intValue];
        
    }
    
    //TO EMOJI
    NSString *valueEmoj = [dic[@"content"] emo_emojiString];

    self.publicationTxt = valueEmoj;
    if ([dic[@"geolocation"] isKindOfClass:[NSDictionary class]]) {
        self.latitude =dic[@"geolocation"][@"latitude"];
        self.longtitude =dic[@"geolocation"][@"longitude"];
        self.address =dic[@"geolocation"][@"address"];
    }
    self.arrGroup =dic[@"groups"];
    self.arrHunt =dic[@"hunts"];
    self.publicationID = dic[@"id"];
    //    self.mParentVC =self;
    self.dicEditer =[NSMutableDictionary new];
    self.dicEditer = [dic mutableCopy];
    self.legend =dic[@"legend"];
    
    if ([dic[@"media"] isKindOfClass:[NSDictionary class]]) {
        self.dicMedia = dic[@"media"];
    }
    NSArray *arrOb = dic[@"observations"];
    if (arrOb.count>0) {
        self.observationID =dic[@"observations"][0][@"id"];
    }
    //color
    if ([dic[@"color"] isKindOfClass:[NSDictionary class]]) {
        self.publicationcolor =dic[@"color"][@"id"];
    }
    NSArray *arrShareUser = dic[@"shareusers"];
    if (arrShareUser.count> 0) {
        NSMutableArray *arrUser = [NSMutableArray new];
        for (NSDictionary *dic in arrShareUser) {
            NSDictionary *dicCopy = @{
                                      @"id":dic[@"id"],
                                      @"name":dic[@"fullname"],
                                      @"status":@(1),
                                      @"type": @(6)
                                      };
            [arrUser addObject:dicCopy];
        }
        self.arrsharingUsers = arrUser;
    }
}
-(void) resetParams
{
    self.removeGeo = NO;
    self.publicationTxt = nil;
    self.mediaType = TYPE_MESSAGE;
    
    self.enableGeolocation = NO;
    self.enableLandmark = NO;

    self.urlVideo = nil;
    self.urlPhoto = nil;
    
    self.latitude = nil;
    self.longtitude = nil;
    self.location = nil;
    self.legend = nil;
    
    self.groupID = nil;
    self.huntID = nil;
    self.observation = nil;
    self.publicationID =nil;
    self.isEditer=NO;
    self.address =nil;
    self.iShare =0;
    self.dicGeolocation =nil;
    self.arrHunt =nil;
    self.arrGroup =nil;
    self.mParentVC =nil;
    self.dicEditer =nil;
    self.observationID =nil;
    self.dicMedia =nil;
    self.sharingGroupsArray =nil;
    self.sharingHuntsArray =nil;
    self.sharingGroupsArray = nil;
    self.publicationcolor = nil;
    self.arrReceivers = nil;
    self.sharingReceiversArray =nil;
    self.dicFavoris = nil;
    self.arrsharingUsers = nil;
    self.arrMultiplePhoto = nil;
    self.dicPatarge = nil;
    self.isPostFavo = NO;
    self.isEditFavo = NO;
}

-(NSString*) convertArray2String:(NSArray*)arr forGroup:(BOOL) isGroup
{
    NSMutableString * strRet = [NSMutableString new];
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            if (isGroup) {
                [array addObject:dic[@"groupID"]];
                
            }else{
                [array addObject:dic[@"huntID"]];
            }
        }
    }
    
    if (array.count > 0) {
        NSString *tmp = [array componentsJoinedByString:@","];
        
        [strRet appendString:tmp];
    }
    return strRet;
}
-(void) uploadData :(NSDictionary *) dicSharing
{
    
    
    //Register...
    NSMutableString * strGroups = [NSMutableString new];
    NSMutableString * strHunts = [NSMutableString new];
    NSMutableString * strUser = dicSharing[@"sharingUser"];

    [strGroups setString:@""];
    [strHunts setString:@""];
    
    NSMutableDictionary *postDict = [NSMutableDictionary new];
    
    NSMutableDictionary *TmpData = [NSMutableDictionary new];
    
    /*
     
     nicolasmendez [1:32 PM]
     to share a publication to a chasse, you have to put : publication[hunts] = 1,2,3...
     
     nicolasmendez [1:34 PM]
     you have to get all my group/all chasse before adding a publication, and not when you add it no?
     
     */
    
    if (dicSharing[@"sharingHuntsArray"]) {
        [strHunts appendString:[self convertArray2String:dicSharing[@"sharingHuntsArray"] forGroup:NO]];
        
    }
    if (dicSharing[@"sharingGroupsArray"]) {
        [strGroups appendString:[self convertArray2String:dicSharing[@"sharingGroupsArray"] forGroup:YES]];
        
    }
    //    strGroups = [strGroups stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    //append my_id group/chasse
    if ([PublicationOBJ sharedInstance].groupID != nil)
    {
        if (((NSArray*)dicSharing[@"sharingGroupsArray"]).count > 0) {
            [ strGroups appendString: [ NSString stringWithFormat:@",%@", [PublicationOBJ sharedInstance].groupID ]];
            
        }else{
            [ strGroups appendString: [ NSString stringWithFormat:@"%@", [PublicationOBJ sharedInstance].groupID ]];
            
        }
    }else if ([PublicationOBJ sharedInstance].huntID != nil)
    {
        if (((NSArray*)dicSharing[@"sharingHuntsArray"]).count > 0) {
            [ strHunts appendString: [ NSString stringWithFormat:@",%@", [PublicationOBJ sharedInstance].huntID ]];
            
        }else{
            [ strHunts appendString: [ NSString stringWithFormat:@"%@", [PublicationOBJ sharedInstance].huntID ]];
            
        }
    }
    
    NSDictionary *sharingDictionary = @{@"share" : dicSharing[@"iSharing"]};
    
    
    NSDictionary *geolocationDictionary = nil;
    
    //? Enable Geo
    if ([PublicationOBJ sharedInstance].enableGeolocation)
    {
        
        if ([PublicationOBJ sharedInstance].altitude != nil) {
            geolocationDictionary = @{@"latitude" : [PublicationOBJ sharedInstance].latitude,
                                      @"longitude" : [PublicationOBJ sharedInstance].longtitude,
                                      @"altitude" : [PublicationOBJ sharedInstance].altitude,
                                      @"address" : [PublicationOBJ sharedInstance].location != nil? [PublicationOBJ sharedInstance].location : @""};
            
        }else{
            geolocationDictionary = @{@"latitude" : [PublicationOBJ sharedInstance].latitude,
                                      @"longitude" : [PublicationOBJ sharedInstance].longtitude,
                                      @"address" : [PublicationOBJ sharedInstance].location != nil? [PublicationOBJ sharedInstance].location : @""};
            
        }
    }
    
    NSDictionary *aDictionary = nil;
    NSString *legendStr = nil;
    //Legend
    
    //is only for attached file...
    if ([PublicationOBJ sharedInstance].legend) {
        legendStr =  [PublicationOBJ sharedInstance].legend;
    }else{
        legendStr =  @"";
    }
    //text
    NSString *txtStr = [PublicationOBJ sharedInstance].publicationTxt?[PublicationOBJ sharedInstance].publicationTxt:@"";
    NSString * timeStr = [NSDate currentStandardUTCTimeGMTPLUS:@"GMT+2"];
    
    //color
    NSString *srtColor = [PublicationOBJ sharedInstance].publicationcolor?[PublicationOBJ sharedInstance].publicationcolor:@"";
    //? Enable Geo
    //to UNICODE
    NSString *valueUnicode = [txtStr emo_UnicodeEmojiString];
    BOOL strLandmark = [PublicationOBJ sharedInstance].enableLandmark?YES:NO;
    if (geolocationDictionary) {
        aDictionary = @{@"content" : valueUnicode,
                        @"sharing" : sharingDictionary,
                        @"groups" : strGroups,
                        @"hunts":strHunts,
                        @"landmark":[NSNumber numberWithBool:strLandmark],
                        @"geolocation" : geolocationDictionary,
                        @"legend":legendStr,
                        @"created":timeStr,
                        @"publicationcolor":srtColor,
                        @"users":strUser?strUser:@"",
                        @"guid": [CommonHelper generatorString]
                        };
        
        
    }else{
        aDictionary = @{@"content" : valueUnicode,
                        @"sharing" : sharingDictionary,
                        @"groups" : strGroups,
                        @"hunts":strHunts,
                        @"landmark":[NSNumber numberWithBool:strLandmark],
                        @"legend":legendStr,
                        @"created":timeStr,
                        @"users":strUser?strUser:@"",
                        @"guid": [CommonHelper generatorString]
                        };
        
    }
    [postDict setObject:aDictionary forKey:@"publication"];
    
    if ([PublicationOBJ sharedInstance].observation) {
        NSMutableDictionary *dicObservation = [[PublicationOBJ sharedInstance].observation  mutableCopy];
        
        if (((NSArray*)dicSharing[@"receivers"]).count > 0) {
            NSArray *arrReceivers =[self convertReceiversArray:dicSharing[@"receivers"]];
            NSMutableArray *newReceivers = [NSMutableArray new];

            if (arrReceivers.count>0) {
                int i = 0;
                for (NSDictionary *dic in arrReceivers) {
                    
                    [newReceivers addObject:@{[NSString stringWithFormat:@"%d",i]:dic}];
                    i++;
                }
                [dicObservation setValue:newReceivers forKey:@"receivers"];
            }
            
        }
        if (dicObservation[@"attachments"]) {
            
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            int i = 0;
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{[NSString stringWithFormat:@"%d",i]:
                                           @{@"label": dic[@"label"],
                                             @"value": dic[@"value"]}}];
                i++;
            }
            
            [dicObservation setObject:newAttach forKey:@"attachments"];

        }
        //count index recursive
        [postDict setObject:dicObservation forKey:@"observation"];

    }
    
    //    NSData *dataLogWrite=[str dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *strPath = [FileHelper pathForApplicationDataFile:FILE_LOG];
    [postDict writeToFile:strPath atomically:YES];
    
    //receivers [] get from Federation...
    
    [TmpData setObject:postDict forKey:strPostDataKey];
    
    //
    NSDictionary *attachment = nil;
    
    switch ([PublicationOBJ sharedInstance].mediaType) {
        case TYPE_MESSAGE:
        {
            [TmpData setObject:strMessageKey forKey:strPostTypeKey];
        }
            break;
        case TYPE_PHOTO:
        {
            attachment = @{@"kFileName": @"image.png",
                           @"kFileData":  [PublicationOBJ sharedInstance].urlPhoto };
            
            [TmpData setObject:strPhotoKey forKey:strPostTypeKey];
            [TmpData setObject:attachment forKey:strPostAttachmentKey];
            
        }
            break;
        case TYPE_VIDEO:
        {
            
            attachment = @{@"kFileName": @"video.mp4",
                           @"kFileData": [PublicationOBJ sharedInstance].urlVideo};
            [TmpData setObject:strVideoKey forKey:strPostTypeKey];
            [TmpData setObject:attachment forKey:strPostAttachmentKey];
            
        }
            break;
            
        default:
            break;
    }
    
    NSDictionary* currentData = [NSDictionary dictionaryWithDictionary:TmpData];
    
    [[ServiceHelper sharedInstance] addNewPostingTask:currentData];
}

-(NSArray*) convertArray:(NSArray*)arr forGroup:(BOOL) isGroup
{
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arr) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            if (isGroup) {
                [array addObject:dic[@"groupID"]];
                
            }else{
                [array addObject:dic[@"huntID"]];
            }
        }
    }
    
    return array;
}
-(NSArray*)convertReceiversArray:(NSArray*)arrReceivers
{
    NSMutableArray *array = [NSMutableArray new];
    
    for (NSDictionary*dic in arrReceivers) {
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            NSDictionary *dicRe= @{ @"receiver": dic[@"id"]};
            [array addObject:dicRe];
        }
    }
    
    return array;
}
-(void)modifiPosterAll:(UIViewController*)viewcontroller
{
    PublicationOBJ *obj =[PublicationOBJ sharedInstance];

    NSMutableDictionary *dicUpdate = [NSMutableDictionary new];
    
    //add text
    NSString *strContent =obj.publicationTxt?obj.publicationTxt:@"";
    
    NSString *valueUnicode = [strContent emo_UnicodeEmojiString];
    
    [dicUpdate setValue:valueUnicode forKey:@"content"];
    
//    "removeGeo" : bool,

    if (obj.removeGeo) {
        [dicUpdate setValue:@"1" forKey:@"removeGeo"];
    }
    else
    {
        //add geolocation
        if(obj.latitude)
        {
            [dicUpdate setObject:obj.latitude forKey:@"latitude"];
        }
        if(obj.longtitude)
        {
            [dicUpdate setObject:obj.longtitude forKey:@"longitude"];
        }
        if(obj.address)
        {
            [dicUpdate setObject:obj.address forKey:@"address"];
        }
        if(obj.altitude)
        {
            [dicUpdate setObject:obj.altitude forKey:@"altitude"];
        }
    }
    
    NSMutableDictionary *dicShare = [NSMutableDictionary new];
    //            //add group
    if (obj.sharingGroupsArray) {
        //grID1-grID2-grID3
        
        [dicShare setValue:obj.sharingGroupsArray forKey:@"groups"];
        
    }
    //add hunt
    if (obj.sharingHuntsArray) {
        [dicShare setValue:obj.sharingHuntsArray forKey:@"hunts"];
        
        
    }
    //add share
    if (obj.arrsharingUsers) {
        [dicShare setValue:obj.arrsharingUsers forKey:@"users"];
    }
    
    [dicShare setValue:[NSNumber numberWithInt:obj.iShare] forKey:@"share"];
    
    [dicUpdate setValue:dicShare forKey:@"sharing"];
    
    //add legend
    if (obj.legend) {
        [dicUpdate setValue:obj.legend forKey:@"legend"];
    }
    
    //add legend
    if (obj.publicationcolor) {
        [dicUpdate setValue:obj.publicationcolor forKey:@"publicationcolor"];
    }
    
    //add observation
    if (obj.observation) {
        NSMutableDictionary *dicObservation = [obj.observation  mutableCopy];
        if (dicObservation[@"attachments"]) {
            
            NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
            NSMutableArray *newAttach = [NSMutableArray new];
            for (NSDictionary *dic in arrAttach) {
                
                [newAttach addObject:@{@"label": dic[@"label"],
                                       @"value": dic[@"value"]}];
            }
            
            [dicObservation setObject:newAttach forKey:@"attachments"];
            
        }
        
        [dicUpdate setValue:dicObservation forKey:@"observation"];
    }
    
    BaseVC *vc = (BaseVC*)viewcontroller;
    

        NSMutableDictionary *postDict = [NSMutableDictionary new];
        
        [postDict setObject:dicUpdate forKey:@"value"];
        [postDict setObject:[PublicationOBJ sharedInstance].publicationID forKey:@"publication_id"];
        
        [COMMON addLoading:vc];
        WebServiceAPI *serverAPI = [WebServiceAPI new];
        
        [serverAPI fnPUT_UPDATE_PUBLICATION_ALL:postDict];
        
        serverAPI.onComplete =^(id response, int errCode)
        {
            [COMMON removeProgressLoading];
            if ([response[@"publication"] isKindOfClass:[NSDictionary class]]) {
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app updateAllowShowAdd:response];
                //                [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_OBJ_NOTIFICATION object:nil];
                
                UIViewController *viewcontroller = obj.mParentVC;
                [[PublicationOBJ sharedInstance] fnSetValueForKeyWithPublication:response[@"publication"]];
                [PublicationOBJ sharedInstance].mParentVC= viewcontroller;
                [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_EDIT object: response userInfo: nil];
                
                [UIAlertView showWithTitle:str(strVotre_publication_modifiee)
                                   message:str(strVoulez_vous_faire_modifications)
                         cancelButtonTitle:str(strOui)
                         otherButtonTitles:@[str(strNon)]
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                          //ok tro ve list
//                                          [vc backEditListMur];
                                          
                                      }
                                      else
                                      {
                                          //non tro ve wall
                                          [vc.navigationController popToViewController:[PublicationOBJ sharedInstance].mParentVC animated:YES];
                                          
                                      }
                                  }];
            }
            
        };
}
-(void)modifiPublication:(UIViewController*)viewcontroller withType:(int) mType
{
    BaseVC *vc = (BaseVC*)viewcontroller;
    
    PublicationOBJ *obj =[PublicationOBJ sharedInstance];
    
    NSMutableDictionary *dicUpdate = [NSMutableDictionary new];
    NSString *strType =@"";
    switch (mType) {
        case EDIT_CARTE:
        {
            strType = @"geolocation";
            //add geolocation
            if(obj.latitude)
            {
                [dicUpdate setObject:obj.latitude forKey:@"latitude"];
            }
            if(obj.longtitude)
            {
                [dicUpdate setObject:obj.longtitude forKey:@"longitude"];
            }
            if(obj.address)
            {
                [dicUpdate setObject:obj.address forKey:@"address"];
            }
            if(obj.altitude)
            {
                [dicUpdate setObject:obj.altitude forKey:@"altitude"];
            }
            
        }
            break;
        case EDIT_TEXT:
        {
            
            strType = @"content";
            //add text
            NSString *strContent =obj.publicationTxt?obj.publicationTxt:@"";
            
            NSString *valueUnicode = [strContent emo_UnicodeEmojiString];

            [dicUpdate setValue:valueUnicode forKey:@"content"];

        }
            break;
        case EDIT_PARTAGE:
        {
            
           /* input json: {"sharing":{
                "share":1,
                "withouts":[
                ]
            },
            */
            strType = @"sharing";
            NSMutableDictionary *dicShare = [NSMutableDictionary new];
//            //add group
            if (obj.arrGroup) {
                //grID1-grID2-grID3
                
                [dicShare setValue:obj.sharingGroupsArray forKey:@"groups"];

            }
            //add hunt
            if (obj.arrHunt) {
                [dicShare setValue:obj.sharingHuntsArray forKey:@"hunts"];

                
            }
            //add share
            if (obj.arrsharingUsers) {
                [dicShare setValue:obj.arrsharingUsers forKey:@"users"];
            }

            [dicShare setValue:[NSNumber numberWithInt:obj.iShare] forKey:@"share"];

            [dicUpdate setValue:dicShare forKey:@"sharing"];

            //add share personer
        }
            break;
        case EDIT_LEGEND:
        {
            
            strType = @"legend";
            //add legend
            if (obj.legend) {
                [dicUpdate setValue:obj.legend forKey:@"legend"];
            }
            

        }
            break;
        case EDIT_PRECISIONS:
        {
            
            strType = @"observations";
            //add legend
            if (obj.observation) {
                NSMutableDictionary *dicObservation = [obj.observation  mutableCopy];
                if (dicObservation[@"attachments"]) {
                    
                    NSMutableArray *arrAttach = [dicObservation[@"attachments"] mutableCopy];
                    NSMutableArray *newAttach = [NSMutableArray new];
                    for (NSDictionary *dic in arrAttach) {
                        
                        [newAttach addObject:@{@"label": dic[@"label"],
                                               @"value": dic[@"value"]}];
                    }
                    
                    [dicObservation setObject:newAttach forKey:@"attachments"];
                    
                }

                [dicUpdate setValue:dicObservation forKey:@"observation"];
            }
            
            
        }
            break;
        case EDIT_COLOR:
        {
            
            strType = @"color";
            //add legend
            if (obj.publicationcolor) {
                [dicUpdate setValue:obj.publicationcolor forKey:@"publicationcolor"];
            }
            
            
        }
            break;
        default:
            break;
    }
    
    if (strType.length>0) {
        NSMutableDictionary *postDict = [NSMutableDictionary new];
        
        [postDict setObject:dicUpdate forKey:@"value"];
        [postDict setObject:[PublicationOBJ sharedInstance].publicationID forKey:@"publication_id"];
        
        [COMMON addLoading:vc];
        WebServiceAPI *serverAPI = [WebServiceAPI new];
        
        [serverAPI fnPUT_UPDATE_PUBLICATION:postDict withType:strType];
        
        serverAPI.onComplete =^(id response, int errCode)
        {
            [COMMON removeProgressLoading];
            if ([response[@"publication"] isKindOfClass:[NSDictionary class]]) {
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app updateAllowShowAdd:response];
//                [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_OBJ_NOTIFICATION object:nil];

                UIViewController *viewcontroller = obj.mParentVC;
                [[PublicationOBJ sharedInstance] fnSetValueForKeyWithPublication:response[@"publication"]];
                [PublicationOBJ sharedInstance].mParentVC= viewcontroller;
                [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_EDIT object: response userInfo: nil];
                
                [UIAlertView showWithTitle:str(strVotre_publication_modifiee)
                                   message:str(strVoulez_vous_faire_modifications)
                         cancelButtonTitle:str(strOui)
                         otherButtonTitles:@[str(strNon)]
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                          //ok tro ve list
                                          [vc backEditListMur];
                                          
                                      }
                                      else
                                      {
                                          //non tro ve wall
                                          [vc.navigationController popToViewController:[PublicationOBJ sharedInstance].mParentVC animated:YES];
                                          
                                      }
                                  }];
            }
            
        };
    }

    
}

-(BOOL) checkShowCategory:(id)arrGroupOfCategory
{
    BOOL isExist = NO;
    
    if (!arrGroupOfCategory) {
        return YES;
    }
    if ([arrGroupOfCategory isKindOfClass:[NSDictionary class]]) {
        return YES;
    }
    NSArray *arrMes = (NSArray*)arrGroupOfCategory;
    if (arrMes.count==0) {
        return YES;
    }
    
    // If my groups contain one of these group -> ok to display
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)   ];
    NSArray *arrMesGroup = [NSArray arrayWithContentsOfFile:strPath];

    if (arrMesGroup.count > 0)
    {
        for (NSDictionary *dic in arrMes)
        {
            for (NSDictionary *dicMes in arrMesGroup)
            {
                
                if ([dic[@"id"] intValue]== [dicMes[@"groupID"]  intValue])
                {
                    isExist = YES;
                    break;
                }
            }

        }
        

        return isExist;
    }
    
        return NO;
}

//Merge tree...
/*
 Structure:
 specific_card
 cards
 tree =
 */

-(void) doMergeTreeWithNew:(NSDictionary*) response
{
//
    NSMutableArray *arrTemp = [NSMutableArray new];
    NSString*  strPath = nil;

    //Specific cards
    if ([response[@"specific_card"] isKindOfClass: [NSArray class]]) {
        //
        [arrTemp addObjectsFromArray: response[@"specific_card"]];
        [arrTemp addObjectsFromArray: [self getCacheSpecific_Cards]];
        
        strPath = [FileHelper pathForApplicationDataFile: CACHE_SPECIFIC_CARDS  ];
        [arrTemp writeToFile:strPath atomically:YES];
    }
    
    //Cards
    if ([response[@"cards"] isKindOfClass: [NSArray class]]) {
        //
        [arrTemp removeAllObjects];
        
        
        [arrTemp addObjectsFromArray: [self getCache_Cards]];

        NSArray *mArr = response[@"cards"];
        
        for (NSDictionary*jDic in mArr)
        {
            BOOL isExit = NO;

            for (int i=0; i< arrTemp.count ; i++)
            {
                NSDictionary*mDic =  arrTemp[i];
                
                if ([mDic[@"id"] intValue] == [jDic[@"id"] intValue]) {
                    [arrTemp replaceObjectAtIndex:i withObject:jDic];
                    isExit = YES;
                    break;
                }
            }
            
            if (!isExit) {
                [arrTemp addObject:jDic];
            }
        }
        
        [arrTemp addObjectsFromArray: response[@"cards"]];
        
        strPath = [FileHelper pathForApplicationDataFile: CACHE_CARDS  ];
        [arrTemp writeToFile:strPath atomically:YES];
    }
    
    //Tree
    if ([response[@"tree"] isKindOfClass: [NSDictionary class]]) {
        //

        NSDictionary * tmpDic = response[@"tree"];
//        response[@"fav"]
        
        NSMutableDictionary *mutDic = [NSMutableDictionary new];
        [mutDic addEntriesFromDictionary: [self getCache_Tree] ];

        [mutDic addEntriesFromDictionary:tmpDic];
        [mutDic setObject:@[] forKey:@"favorite"];
        
        strPath = [FileHelper pathForApplicationDataFile: CACHE_TREE  ];
        [mutDic writeToFile:strPath atomically:YES];
    }
}

-(void) doRefreshTree:(NSDictionary*) response
{
    NSMutableArray *arrTemp = [NSMutableArray new];

    NSString*  strPath = nil;

    if ([response[@"tree"] isKindOfClass: [NSDictionary class]]) {
        //
        
        NSDictionary * tmpDic = response[@"tree"];
        //        response[@"fav"]
        
        NSMutableDictionary *mutDic = [NSMutableDictionary new];
        [mutDic addEntriesFromDictionary: tmpDic ];
        
        [mutDic addEntriesFromDictionary:tmpDic];
        [mutDic setObject:@[] forKey:@"favorite"];
        
        strPath = [FileHelper pathForApplicationDataFile: CACHE_TREE  ];
        [mutDic writeToFile:strPath atomically:YES];
    }

    //Specific cards
    if ([response[@"specific_card"] isKindOfClass: [NSArray class]]) {
        //
        strPath = [FileHelper pathForApplicationDataFile: CACHE_SPECIFIC_CARDS  ];
        [FileHelper removeFileAtPath:strPath];
        
        [arrTemp removeAllObjects];
        [arrTemp addObjectsFromArray: response[@"specific_card"]];
        
        [arrTemp writeToFile:strPath atomically:YES];
    }
    
    //Cards
    if ([response[@"cards"] isKindOfClass: [NSArray class]]) {
        //
        strPath = [FileHelper pathForApplicationDataFile: CACHE_CARDS  ];
        [FileHelper removeFileAtPath:strPath];

        [arrTemp removeAllObjects];
        [arrTemp addObjectsFromArray: response[@"cards"]];
        
        [arrTemp writeToFile:strPath atomically:YES];
    }

}
//a dic
-(NSDictionary*) getCache_Tree
{
    NSString*  strPath = [FileHelper pathForApplicationDataFile: CACHE_TREE  ];
    
    NSDictionary*rep = [NSDictionary dictionaryWithContentsOfFile:strPath];
    return rep;
}

-(NSArray*) getCache_Cards
{
    NSString*  strPath = [FileHelper pathForApplicationDataFile: CACHE_CARDS  ];
    
    NSArray*rep = [NSArray arrayWithContentsOfFile:strPath];
    return rep;
}

-(NSArray*) getCacheSpecific_Cards
{
    NSString*  strPath = [FileHelper pathForApplicationDataFile: CACHE_SPECIFIC_CARDS  ];
    
    NSArray*rep = [NSArray arrayWithContentsOfFile:strPath];
    return rep;
}

@end
