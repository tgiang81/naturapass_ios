//
//  GroupCreate_Step1.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Parameter_Common_Profile_Kind_Change.h"

#import "PhotoSheetVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ImageWithCloseButton.h"
#import "AlertVC.h"
#import "CellKind23.h"
#import "CellKind18.h"
#import "VoisChiens_Document_Alert.h"
#import "ChooseDocumentVC.h"
#import "CommonHelper.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "FileHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ServiceHelper.h"
#import "VoisChiens_Document_Valider_Alert.h"
#import "DatabaseManager.h"
#import <AVFoundation/AVFoundation.h>
#import "NSString+Extensions.h"
#import "NSString+HTML.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";

static int val_min = 1;
static int val_max = 4;
static int val_height_cell = 44;
@interface Parameter_Common_Profile_Kind_Change ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    IBOutlet UILabel *lbNameScreen;
    NSMutableDictionary *postDict;
    NSMutableDictionary *attachment;
    
    NSMutableArray *arrDocumentsNeedUploading;
    
    NSMutableArray *arrChoose;
    
    NSMutableArray *arrBreed;
    NSMutableArray *arrSex;
    
    NSMutableArray *arrType;
    NSMutableArray *arrDocument;
    NSData         *dataPhoto;
    BOOL isShow;
    int type_choose;
    int iWeaponCountCheck;
    int iDogCountCheck;
    NSDateFormatter *formatter;
    
}
@property (strong, nonatomic)  IBOutlet DPTextField    *dateTextField;

@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPhotoSelected;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintViewChooseTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintViewChooseHeight;
@property (strong, nonatomic)   IBOutlet UITableView *tableDocument;
@end

@implementation Parameter_Common_Profile_Kind_Change
/*
 *      weapon[name] = "my gun"
 202:      *      weapon[calibre] = 1
 203:      *      weapon[brand] = 3
 204:      *      weapon[type] = [1 => CARABINE, 0 => SHOTGUN]
 205:      *      weapon[photo][file] = Données de photo
 206:      *      weapon[medias][1000][file] = Document
 207:      *      weapon[medias][1001][file] = Document
 */

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialization];
    
    arrDocumentsNeedUploading = [NSMutableArray new];
    
    [viewChoose.layer setMasksToBounds:YES];
    viewChoose.layer.cornerRadius= 4.0;
    viewChoose.layer.borderWidth =0.5;
    viewChoose.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableDocument registerNib:[UINib nibWithNibName:@"CellKind18" bundle:nil] forCellReuseIdentifier:identifierSection2];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-YYYY"];
    
    [self.dateTextField setCallBack:^(){
    }];
    
    self.dateTextField.datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    
    [self.dateTextField.datePicker addTarget:self action:@selector(dateUpdated:) forControlEvents:UIControlEventValueChanged];
    
    arrChoose = [NSMutableArray new];
    arrBreed = [NSMutableArray new];
    arrSex = [NSMutableArray new];
    arrType = [NSMutableArray new];
    arrDocument = [NSMutableArray new];
    
    [self fnSetValueUI];
    
    
    iWeaponCountCheck = 0;
    iDogCountCheck = 0;
    
    //Delete Profile image.
    [FileHelper removeFileAtPath: [FileHelper documentTempDirectory]];
    
}

//if go back...del tmp files.

-(void) cleanAnyTmpData {
    
    for (NSDictionary*dic in arrDocumentsNeedUploading) {
        [FileHelper removeFileAtPath:dic[@"kLocalPath"] ];
    }

}

- (void) dateUpdated:(UIDatePicker *)datePicker {
    
    self.dateTextField.text = [formatter stringFromDate:self.dateTextField.datePicker.date];
}

-(void) doSetDefaultDataIfModifyDog
{
    if (iDogCountCheck == 2) {
        if (self.isModifi) {
            [self fnSetDataModifi];
        }
    }
}
-(void) doSetDefaultDataIfModify
{
    if (iWeaponCountCheck == 2) {
        if (self.isModifi) {
            [self fnSetDataModifi];
        }
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self generateData];
}

-(void) generateData
{
    NSString *strNameMedias = @"";

    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            strNameMedias = PROFILE_ONE_DOG;
        }
            break;
        case TYPE_WEAPONS:
        {
            strNameMedias = PROFILE_ONE_WEAPON;
        }
            break;
        default:
            break;
    }
    
    [arrDocumentsNeedUploading removeAllObjects];
    //list all cases that imported photo/video.
    
    for (NSDictionary *dic in arrDocument) {
        
        if(!dic[@"id"])
        {
            NSString*fileType ;
            if ([[dic[@"fileName"] pathExtension] isEqualToString:@"pdf"])
            {
                fileType = @"PDF";
                NSString *fileURL = dic[@"filePath"];
                
                if(fileURL)
                {
                    //need PDF Folder.???
                    
                    
                    NSData *dataMedia = [NSData dataWithContentsOfFile:fileURL];
                    
                    //check file name...pdf tag
                    
                    [arrDocumentsNeedUploading addObject:@{@"name":[NSString stringWithFormat:@"%@[medias][1000][file]",strNameMedias],
                                                           @"kFileName": dic[@"fileName"],
                                                           @"kFileData": dataMedia,
                                                           @"kType": fileType,
                                                           @"kLocalPath": fileURL}];
                    
                }
                
            }else if ( [dic[@"type"] intValue] == 1)//take photo...lib
            {
                fileType = @"IMAGE";
                
                //photo
                if (dic[@"keyPathFile"])
                {
                    
                    NSString *fullImageName = (NSString*)dic[@"keyPathFile"];
                    if(fullImageName)
                    {
                        
                        NSString *strPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent: fullImageName ];
                        
                        NSData *dataMedia = [NSData dataWithContentsOfFile:strPath];
                        
                        //check file name...pdf tag
                        
                        [arrDocumentsNeedUploading addObject:@{@"name":[NSString stringWithFormat:@"%@[medias][1000][file]",strNameMedias],
                                                               @"kFileName": dic[@"fileName"],
                                                               @"kFileData": dataMedia,
                                                               @"kType": fileType,
                                                               @"kLocalPath": strPath}];
                        
                    }
                }
            }
            else if ( [dic[@"type"] intValue] == 0)//take photo...lib
            {
                fileType = @"IMAGE";
                
                NSString *strPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent: dic[@"keyPathFile"] ];
                
                NSData *dataMedia = [NSData dataWithContentsOfFile:strPath];
                
                [arrDocumentsNeedUploading addObject:@{@"name":[NSString stringWithFormat:@"%@[medias][1000][file]",strNameMedias],
                                                       @"kFileName": dic[@"fileName"],
                                                       @"kFileData": dataMedia,
                                                       @"kType": fileType,
                                                       @"kLocalPath": strPath}];
                
            }else{//take from folder...Document.
                
                //video ?
                fileType = @"VIDEO";
                
                NSString *strPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent: dic[@"keyPathFile"] ];
                
                NSData *dataMedia = [NSData dataWithContentsOfFile:strPath];
                
                [arrDocumentsNeedUploading addObject:@{@"name":[NSString stringWithFormat:@"%@[medias][1000][file]",strNameMedias],
                                                       @"kFileName": dic[@"fileName"],
                                                       @"kFileData": dataMedia,
                                                       @"kType": fileType,
                                                       @"kLocalPath": strPath}];
            }
        }
    }

}


-(void)initialization
{
    [nameTextField setAutocapitalizationType: UITextAutocapitalizationTypeWords];
    [self InitializeKeyboardToolBar];
    [nameTextField setInputAccessoryView:self.keyboardToolbar];
    //Photo
    self.viewPhotoSelected.hidden=YES;
    [self.viewPhotoSelected setMyCallback: ^(){
        //Close
        self.viewPhotoSelected.hidden=YES;
        self.viewPhotoSelected.imageContent.image = nil;
    }];
}

-(void)fnSetValueUI
{
    
    NSString *strKind = @"";
    
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            self.constraintHeightViewBirthDay.constant = 48;
            viewBirthDay.hidden =NO;
            
            strKind =str(strAjout_ou_modification_chien); //
            
            [self getListBreeds];
            [self getListTypes];
            
            [arrSex addObjectsFromArray:@[
                                          @{@"isSelect":@0,@"name":[NSString stringWithFormat:@"-- %@ --",str(strSexe)],@"id":@1000000},
                                          @{@"isSelect":@0,@"name":str(strFemale),@"id":@"1"},
                                          @{@"isSelect":@0,@"name":str(strMale),@"id":@"0"}]];
            nameTextField.placeholder = str(strNom_du_chien);
            raceTextField.placeholder = str(strRace);
            sexTextField.placeholder  = str(strSexe);
            typeTextField.placeholder = str(strTType);
        }
            break;
            
        case TYPE_WEAPONS:
        {
            self.constraintHeightViewBirthDay.constant = 0;
            viewBirthDay.hidden =YES;
            
            //sex breed type
            if (self.isModifi) {
                strKind =str(strModification_arme);
            }
            else
            {
                strKind =str(strAjout_arme);
                
            }
            [self getListBrands];
            [self getListCalibres];
            [arrType addObjectsFromArray:@[
                                           @{@"isSelect":@0,@"name":[NSString stringWithFormat:@"-- %@ --",str(strTType)],@"id":@1000000},
                                           @{@"isSelect":@0,@"name":str(strCarabine),@"id":@"1"},
                                           @{@"isSelect":@0,@"name":str(strFusil),@"id":@"0"}]];
            nameTextField.placeholder = str(strNom_de_larme);
            raceTextField.placeholder = str(strMarque);
            sexTextField.placeholder  = str(strCalibre);
            typeTextField.placeholder = str(strTType);
        }
            break;
        default:
            break;
    }
    
    lbNameScreen.text = strKind;
}

-(void)fnSetDataModifi
{
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            //name
            nameTextField.text = self.dicModifi[@"name"];
            //sex
            for (int i = 0 ; i < arrSex.count; i++) {
                NSMutableDictionary *MutDic = [arrSex[i] mutableCopy];
                if ([MutDic[@"id"]intValue] == [self.dicModifi[@"sex"] intValue]) {
                    [MutDic setValue:@1 forKey:@"isSelect"];
                    sexTextField.text = MutDic[@"name"];
                    [arrSex replaceObjectAtIndex:i withObject:MutDic];
                    break;
                }
            }
            //Breed
            for (int i = 0 ; i < arrBreed.count; i++) {
                NSMutableDictionary *MutDic = [arrBreed[i] mutableCopy];
                if ([MutDic[@"id"]intValue] == [self.dicModifi[@"breed"][@"id"] intValue]) {
                    [MutDic setValue:@1 forKey:@"isSelect"];
                    raceTextField.text = MutDic[@"name"];
                    [arrBreed replaceObjectAtIndex:i withObject:MutDic];
                    break;
                }
            }
            //type
            for (int i = 0 ; i < arrType.count; i++) {
                NSMutableDictionary *MutDic = [arrType[i] mutableCopy];
                if ([MutDic[@"id"]intValue] == [self.dicModifi[@"type"][@"id"] intValue]) {
                    [MutDic setValue:@1 forKey:@"isSelect"];
                    typeTextField.text = MutDic[@"name"];
                    [arrType replaceObjectAtIndex:i withObject:MutDic];
                    break;
                }
            }
            //birthday birthday = "2016-03-17T04:56:00+01:00";
            
            if ([self.dicModifi[@"birthday"] isKindOfClass:[NSString class]]) {
                NSString *strDay = [self convertDatetoSever:self.dicModifi[@"birthday"]];
                
                formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd-MM-YYYY"];
                
                
                NSDateFormatter *inputFormatter =[[NSDateFormatter alloc] init];
                [inputFormatter setDateFormat:@"dd-MM-YYYY"];
                
                NSDate * inputDates = [ inputFormatter dateFromString:strDay ];
                [self.dateTextField.datePicker setDate:inputDates];
                self.dateTextField.text = strDay;
            }
            //photo
            if ([self.dicModifi[@"photo"] isKindOfClass:[NSDictionary class]]) {
                self.viewPhotoSelected.hidden = NO;
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicModifi[@"photo"][@"path"]]]];
                [self.viewPhotoSelected.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                }];
            }
            //media
            if ([self.dicModifi[@"medias"] isKindOfClass:[NSArray class]]) {
                arrDocument = [self.dicModifi[@"medias"] mutableCopy];
                [self.tableDocument reloadData];
            }
            /*
             {
             "dog": {
             "id": 34,
             "name": "my gun 1234",
             "type": false,
             "breed": false,
             "photo": {
             "id": 18,
             "type": 100,
             "path": "/uploads/dogs/images/resize/c2ffcbe95e1fe7fffed2d462fcfd0a0670122904.jpeg"
             },
             "medias": [
             {
             "id": 21,
             "type": 100,
             "path": "/uploads/dogs/images/resize/70e1f9b6a4be233758b55e117e622b017b40d1b2.jpeg"
             },
             {
             "id": 22,
             "type": 100,
             "path": "/uploads/dogs/images/resize/3b7a0dc67458f744a3697fac25ba87599366d2ca.jpeg"
             }
             ],
             "sex": 1,
             "birthday": "2016-03-14T05:53:00+01:00",
             "created": "2016-03-14T04:48:44+01:00",
             "updated": "2016-03-14T05:06:07+01:00"
             }
             }
             */
        }
            break;
        case TYPE_WEAPONS:
        {
            //name
            nameTextField.text = self.dicModifi[@"name"];
            //sex
            for (int i = 0 ; i < arrSex.count; i++) {
                NSMutableDictionary *MutDic = [arrSex[i] mutableCopy];
                if ([MutDic[@"id"]intValue] == [self.dicModifi[@"calibre"][@"id"] intValue]) {
                    [MutDic setValue:@1 forKey:@"isSelect"];
                    sexTextField.text = MutDic[@"name"];
                    [arrSex replaceObjectAtIndex:i withObject:MutDic];
                    break;
                }
            }
            //Breed
            for (int i = 0 ; i < arrBreed.count; i++) {
                NSMutableDictionary *MutDic = [arrBreed[i] mutableCopy];
                if ([MutDic[@"id"]intValue] == [self.dicModifi[@"brand"][@"id"] intValue]) {
                    [MutDic setValue:@1 forKey:@"isSelect"];
                    raceTextField.text = MutDic[@"name"];
                    [arrBreed replaceObjectAtIndex:i withObject:MutDic];
                    break;
                }
            }
            //type
            for (int i = 0 ; i < arrType.count; i++) {
                NSMutableDictionary *MutDic = [arrType[i] mutableCopy];
                if (self.dicModifi[@"type"]) {
                    if ([MutDic[@"id"]intValue] == [self.dicModifi[@"type"] intValue]) {
                        [MutDic setValue:@1 forKey:@"isSelect"];
                        typeTextField.text = MutDic[@"name"];
                        [arrType replaceObjectAtIndex:i withObject:MutDic];
                        break;
                    }
                    
                }
            }
            
            //photo
            if ([self.dicModifi[@"photo"] isKindOfClass:[NSDictionary class]]) {
                self.viewPhotoSelected.hidden = NO;
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicModifi[@"photo"][@"path"]]]];
                [self.viewPhotoSelected.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                }];
            }
            //media
            if ([self.dicModifi[@"medias"] isKindOfClass:[NSArray class]]) {
                arrDocument = [self.dicModifi[@"medias"] mutableCopy];
                [self.tableDocument reloadData];
            }
            
        }
            break;
        default:
            break;
    }
    
}

#pragma mark - API

-(void) getListTypes
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_dog_type ORDER BY c_id"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrType removeAllObjects];
        [arrType addObject:@{@"id":@1000000, @"name": [NSString stringWithFormat:@"-- %@ --",str(strTType)]}];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrType addObject:objDic];
        }
        [self.tableControl reloadData];
        iDogCountCheck += 1;
        [self doSetDefaultDataIfModifyDog];
        
    }];
    
}

-(void) getListBreeds
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_dog_breed ORDER BY c_id"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrBreed removeAllObjects];
        [arrBreed addObject:@{@"id":@1000000, @"name": [NSString stringWithFormat:@"-- %@ --",str(strRace)]}];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrBreed addObject:objDic];
        }
        [self.tableControl reloadData];
        iDogCountCheck += 1;
        [self doSetDefaultDataIfModifyDog];
        
    }];
    
}
-(void) getListBrands
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_weapon_brand ORDER BY c_id"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrBreed removeAllObjects];
        [arrBreed addObject:@{@"id":@1000000, @"name": [NSString stringWithFormat:@"-- %@ --",str(strMarque)]}];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrBreed addObject:objDic];
        }
        [self.tableControl reloadData];
        //get enought data list...set default selected....
        iWeaponCountCheck += 1;
        [self doSetDefaultDataIfModify];
        
    }];
    
}
-(void) getListCalibres
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_weapon_calibre ORDER BY c_id"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrSex removeAllObjects];
        [arrSex addObject:@{@"id":@1000000, @"name": [NSString stringWithFormat:@"-- %@ --",str(strCalibre)]}];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"name":[[set_querry stringForColumn:@"c_name"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrSex addObject:objDic];
        }
        [self.tableControl reloadData];
        //get enought data list...set default selected....
        iWeaponCountCheck += 1;
        [self doSetDefaultDataIfModify];
        
    }];
    
}

#pragma mark - action

-(IBAction)createDogAction:(id)sender
{
    [self fnChangeDog];
}

//    "dog[birthday]" = "2016-04-07T05:55:00UTC+02:00";

-(void)replaceFilePDFwithDic:(NSDictionary*)dic withTypeView:(TYPE_VIEW)typeview
{
    NSDictionary *dicMedias;
    switch (typeview) {
        case TYPE_WEAPONS:
        {
            dicMedias = dic[@"weapon"];
        }
            break;
        case TYPE_DOG:
        {
            
        }
            break;
        default:
            break;
    }
    NSArray *arrMedias = dicMedias[@"medias"];
    for (NSDictionary *dicItem in arrMedias) {
        if ([dicItem[@"type"] intValue] == 2) {
            NSString *pathCache = [[SDImageCache sharedImageCache] cachePathForKey:dicItem[@"path"] inPath:[FileHelper SelectedFiles]];
            //check if exist read cache
            NSFileManager *fileMgr = [[NSFileManager alloc] init];
            NSError *error = nil;
            NSArray *subdirectory = [fileMgr contentsOfDirectoryAtPath:pathCache error:&error];
            if (error == nil) {
                //exist cache
            }
            else
            {
                [self renameFileFrom:@"" to:pathCache];
            }
        }
    }
}
- (BOOL)renameFileFrom:(NSString*)oldPath to:(NSString *)newPath
{
    NSFileManager *fileMan = [NSFileManager defaultManager];
    NSError *error = nil;
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        return NO;
    }
    return YES;
}
-(void)postMultiMediasWithArrData:(NSArray*)arrData offset:(int)offset
{
    ASLog(@"syn ~~~~~~~~~~ %d",offset+1);
    
    
    //    if (arrData.count< offset || arrData.count==0) {
    //        //stop
    //        [COMMON removeProgressLoading];
    //        return;
    //    }
    //    if ([COMMON isReachable]&& ([COMMON isLoggedIn]))
    //    {
    //        NSDictionary *dicData = arrData[offset];
    //        NSDictionary *param = dicData[@"param"];
    //        NSDictionary *attach = dicData[@"attachment"];
    //        NSString *url = @"https://naturapass.e-conception.fr/app_dev.php/api/v2/users/profiles/dogs";
    //        AFHTTPRequestOperation *op = [[ServiceHelper sharedInstance] postProfileRequestWithURL:url params:param attachment:attach!=nil?attach:NULL];
    //        op.responseSerializer = [AFJSONResponseSerializer serializer];
    //
    //        [op setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
    //            if (totalBytesExpectedToWrite > 0)
    //            {
    //                //file da tren server...remove local cache
    //                float totalUpload = (float) (totalBytesWritten*100/totalBytesExpectedToWrite);
    //                if (totalUpload>98) {
    //                    //                    ASLog(@"totalUpload ~~~~ waitting....");
    //
    //                }
    //                else
    //                {
    //                    //                    ASLog(@"totalUpload ~~~~ %f",totalUpload);
    //                    //                    [self.progressView setProgress:totalUpload/100];
    //                    [self fnSetProgressWithIndex:0 value:totalUpload/100];
    //                }
    //
    //            }
    //
    //        }];
    //
    //        [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
    //            //            ASLog(@"responseObject~~~~~ %@ ",responseObject);
    //            ASLog(@"success ~~~~~ %d ",offset+1);
    //
    //            //            [self.progressView setProgress:1];
    //            [self fnSetProgressWithIndex:0 value:1];
    //            NSMutableArray *arrResult = [NSMutableArray arrayWithArray:arrData];
    //            [arrResult  removeObjectAtIndex:offset];
    //            [self postMultiMediasWithArrData:arrResult offset:offset];
    //
    //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //            ASLog(@"error ~~~~~ %d ",offset+1);
    //
    //            //            ASLog(@"error~~~%@ ",error);
    //            //            [self.progressView setProgress:0];
    //            [self fnSetProgressWithIndex:0 value:0];
    //            [self postMultiMediasWithArrData:arrData offset:offset+1];
    //
    //
    //        }];
    //    }
    
}
-(void)fnSetProgressWithIndex:(int)index value:(float)value
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    CellKind18 *cell = (CellKind18*)[self.tableDocument cellForRowAtIndexPath:indexPath];
    [cell.progressView setProgress:value];
}

#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter* aformatter = [[NSDateFormatter alloc] init];
    [aformatter setDateFormat:@"dd-MM-YYYY"];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    
    NSDate * inputDates = [ aformatter dateFromString:loungDateStrings ];
    
    NSString * outputStrings = [ formatter stringFromDate:inputDates ];
    return outputStrings;
}

-(NSString*)convertDatetoSever:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ formatter stringFromDate:inputDates ];
    return outputStrings;
}
-(IBAction)chooseAction:(id)sender
{
    CGRect rect = CGRectZero;
    int index = (int)[sender tag];
    [arrChoose removeAllObjects];
    
    int count =0;
    type_choose =index;
    switch (index) {
            //Race
        case 21:
        {
            rect = raceTextField.frame;
            arrChoose = [arrBreed mutableCopy];
            count = (int)arrChoose.count;
            
        }
            break;
            //Sex
        case 22:
        {
            rect = sexTextField.frame;
            arrChoose = [arrSex mutableCopy];
            count = (int)arrChoose.count;
            
        }
            break;
            //Type
        case 23:
        {
            rect = typeTextField.frame;
            arrChoose = [arrType mutableCopy];
            count = (int)arrChoose.count;
            
        }
            break;
        default:
            break;
    }
    if (count< val_min) {
        count = val_min;
    }
    if (count>val_max) {
        count = val_max;
    }
    isShow =!isShow;
    if (isShow) {
        self.constraintViewChooseTop.constant = rect.origin.y + rect.size.height;
        self.constraintViewChooseHeight.constant = count*val_height_cell+10;
        viewChoose.hidden = NO;
        [self.tableControl reloadData];
    }
    else
    {
        viewChoose.hidden = YES;
    }
    
}
-(IBAction)chooseDocumentAction:(id)sender
{
    VoisChiens_Document_Alert *ttest = [[[NSBundle mainBundle] loadNibNamed:@"VoisChiens_Document_Alert" owner:self options:nil] objectAtIndex:0] ;
    [ttest doShow:self];
    [ttest doBlock:^(NSInteger index) {
        switch (index) {
                //@"PRENDRE UNE PHOTO",@"CHOISIR DANS MA BIBLIOTHÈQUE",@"ANNULER"
            case 0:
            {
                
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorry_No_Camera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
                    [altView show];
                    return;
                }
                [self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];
            }
                break;
                //@"PRENDRE UNE VIDEO"
            case 1:
            {
                
                if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorry_No_Camera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
                    [altView show];
                    return;
                }
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
                
                [self presentViewController:picker animated:YES completion:NULL];
            }
                break;
                //@"CHOISIR DANS MA BIBLIOTHÈQUE"
            case 2:
            {
                ChooseDocumentVC *viewController1 = [[ChooseDocumentVC alloc] initWithNibName:@"ChooseDocumentVC" bundle:nil];
                [self pushVC:viewController1 animate:YES];
                [viewController1 doBlock:^(NSArray *arrListChoose) {
                    //                        [arrDocument removeAllObjects];
                    //                        arrDocument = [NSMutableArray arrayWithArray:arrListChoose];
                    [arrDocument addObjectsFromArray:arrListChoose];
                    [self.tableDocument reloadData];
                }];
                
                
            }
                break;
                //@"ANNULER"
            case 3:
            {
                
            }
                break;
            default:
                break;
        }
    }];
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
}
- (IBAction)profileImageOptions:(id)sender{
    [nameTextField resignFirstResponder];
    
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.viewPhotoSelected.imageContent.image = imgData;
        self.viewPhotoSelected.hidden = NO;
        dataPhoto = UIImageJPEGRepresentation(imgData, 1.0);
        
    }];
    
    [self presentViewController:vc animated:NO completion:^{
        
    }];
}
//-(IBAction)fnRemoveDocumentItem:(id)sender
//{
//    int index = (int)[sender tag]-100;
//    if (index< arrDocument.count) {
//        [arrDocument removeObjectAtIndex:index];
//    }
//}
#pragma mark - delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 100) animated:YES];
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    if (textField ==self.dateTextField ) {
        self.dateTextField.text = [formatter stringFromDate:self.dateTextField.datePicker.date];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [nameTextField resignFirstResponder];
    return YES;
}
#pragma mark -keyboar tool bar
- (void)resignKeyboard:(id)sender
{
    [nameTextField resignFirstResponder];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 150) animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
#pragma mark - table
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableDocument) {
        return arrDocument.count;
    }
    else
    {
        return arrChoose.count;
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableDocument) {
        return 60;
    }
    else
    {
        return val_height_cell;
        
    }
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableDocument) {
        CellKind18 *cell = nil;
        
        cell = (CellKind18 *)[self.tableDocument dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
        
        NSDictionary *dic = arrDocument[indexPath.row];
        if (dic[@"path"]) {
            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:dic[@"path"]]]];
            NSString *placehoder =@"";
            if ([dic[@"type"] intValue]== 2) {
                placehoder =@"param_ic_pdf";
            }
            else
            {
                placehoder =@"placeholder_photo";
            }
            [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:placehoder] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
            NSString *filename = [[url path] lastPathComponent];
            cell.label1.text = filename;
            
        }
        else
        {
            //1. photo
            if ([dic[@"type"] intValue]==0 || [dic[@"type"] intValue]==1) {
                [cell.imageIcon setImage:dic[@"thumbnail"]];
            }
            else if ([[dic[@"fileName"] pathExtension] isEqualToString:@"pdf"])
            {
                NSString *fileURL = dic[@"filePath"];
                NSURL* pdfFileUrl = [NSURL fileURLWithPath:fileURL];
                CGPDFDocumentRef pdf = CGPDFDocumentCreateWithURL((CFURLRef)pdfFileUrl);
                cell.imageIcon.image =[[CommonHelper sharedInstance] buildThumbnailImage:pdf];
                
            }
            cell.label1.text = dic[@"fileName"];
        }
        
        //FONT
        [cell.label1 setTextColor:[UIColor blackColor]];
        [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
        cell.rightIcon.image =[ UIImage imageNamed:@"param_ic_close"];
        cell.constraintRightIconWidth.constant = 11;
        cell.constraintRightIconHeight.constant = 11;
        [cell layoutIfNeeded];
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
    else
    {
        CellKind23 *cell = nil;
        
        cell = (CellKind23 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        
        NSDictionary *dic = arrChoose[indexPath.row];
        
        //FONT
        cell.label1.text = dic[@"name"];
        [cell.label1 setTextColor:[UIColor blackColor]];
        [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
        if ([dic[@"isSelect"] boolValue]) {
            cell.backgroundColor=[UIColor lightGrayColor];
        }
        else
        {
            cell.backgroundColor=[UIColor whiteColor];
        }
        cell.rightIcon.hidden=YES;
        [cell layoutIfNeeded];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableDocument) {
        if (indexPath.row< arrDocument.count) {
            NSDictionary *dic = arrDocument[indexPath.row];
            if (dic[@"id"]) {
                NSString *strKind = @"";
                NSString *strTitleAlert = str(strEtes_vous_sur_de_vouloir_supprimer);
                switch (self.myTypeVview) {
                    case TYPE_DOG:
                    {
                        strKind =PROFILE_ONE_DOG;
                    }
                        break;
                    case TYPE_WEAPONS:
                    {
                        strKind =PROFILE_ONE_WEAPON;
                    }
                        break;
                    case TYPE_PAPER:
                    {
                        strKind =PROFILE_ONE_PAPER;
                    }
                        break;
                    default:
                        break;
                }
                VoisChiens_Document_Valider_Alert *ttest = [[[NSBundle mainBundle] loadNibNamed:@"VoisChiens_Document_Valider_Alert" owner:self options:nil] objectAtIndex:0] ;
                
                [ttest setTitle:strTitleAlert];
                [ttest doBlock:^(NSInteger index) {
                    switch (index) {
                            //@"oui"
                        case 0:
                        {
                            [COMMON addLoading:self];
                            WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                            [serviceObj deleteProfileMediaWithKind:strKind WithmediaID:dic[@"id"]];
                            serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                [COMMON removeProgressLoading];
                                if (response[@"success"]) {
                                    [arrDocument removeObjectAtIndex:indexPath.row];
                                    [self.tableDocument reloadData];
                                }
                            };
                        }
                            break;
                            //@"non"
                        case 1:
                        {
                            
                        }
                            break;
                        default:
                            break;
                    }
                }];
                [self.vContainer addSubview:ttest];
                
                [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                                        options:0
                                                                                        metrics:nil
                                                                                          views:NSDictionaryOfVariableBindings(ttest)]];
                
                [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                                        options:0
                                                                                        metrics:nil
                                                 
                                                                                          views:NSDictionaryOfVariableBindings(ttest)]];
            }
            else
            {
                [arrDocument removeObjectAtIndex:indexPath.row];
                [self.tableDocument reloadData];
            }
        }
    }
    else
    {
        for (int i = 0 ; i < arrChoose.count; i++) {
            NSMutableDictionary *MutDic = [arrChoose[i] mutableCopy];
            
            if (i == indexPath.row) {
                [MutDic setValue:[MutDic[@"isSelect"] boolValue]?@0:@1 forKey:@"isSelect"];
            }
            else
            {
                [MutDic setValue:@0 forKey:@"isSelect"];
            }
            [arrChoose replaceObjectAtIndex:i withObject:MutDic];
        }
        isShow = NO;
        viewChoose.hidden = YES;
        [self.tableControl reloadData];
        switch (type_choose) {
                //Race
            case 21:
            {
                arrBreed = [arrChoose mutableCopy];
                raceTextField.text = arrBreed[indexPath.row][@"name"];
            }
                break;
                //Sex
            case 22:
            {
                arrSex = [arrChoose mutableCopy];
                sexTextField.text = arrSex[indexPath.row][@"name"];
            }
                break;
                //Type
            case 23:
            {
                arrType = [arrChoose mutableCopy];
                typeTextField.text = arrType[indexPath.row][@"name"];
            }
                break;
            default:
                break;
        }
    }
}
//
#pragma mark  - media
- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType
{
    //iPhone
    UIImagePickerController *imagePicker;
    
    NSArray *mediaTypes = [UIImagePickerController
                           availableMediaTypesForSourceType:sourceType];
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.mediaTypes = mediaTypes;
    imagePicker.delegate = (id)self;
    imagePicker.sourceType = sourceType;
    
    [self presentViewController:imagePicker animated:NO completion:Nil];
    
}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}


- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
    //    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data
    
    
    CLLocation * loc = self.app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        
        
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

#pragma mark  -  PICKER DELEGATE

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // FIXME: USE NSSTRING CONSTANT NOT STRING VALUE: UIImagePickerControllerOriginalImage
    
    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString*)kUTTypeMovie])
    {
        if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
            NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            NSString *moviePath = [videoUrl path];
            
            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                
                //                    self.myCallback(@{@"videourl":moviePath});
                AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL: videoUrl options:nil];
                AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
                generate1.appliesPreferredTrackTransform = YES;
                NSError *err = NULL;
                CMTime time = CMTimeMake(1, 2);
                CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
                UIImage *returnThumbnail = [[UIImage alloc] initWithCGImage:oneRef];
                NSString *originalFileName = [moviePath lastPathComponent];
                //don't save Movie.
                
                NSString *fileName = [NSString GetUUID];
                
                //TEMP Profile Image/video selected.
                
                NSData *dt = [NSData dataWithContentsOfFile:moviePath ]; //

                NSString *strPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent: fileName ];
                
                [dt writeToFile:strPath atomically:YES];
                
                [arrDocument addObject:@{@"type":@3,
                                         @"thumbnail":returnThumbnail,
                                         @"filePath":moviePath,
                                         @"fileName":originalFileName,
                                         @"keyPathFile": fileName  }];
                [self.tableDocument reloadData];
            }
        }
        
        [self onCancel:nil];
    }
    else if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        if([info objectForKey:@"UIImagePickerControllerOriginalImage"])
        {
            UIImage *tmpimage = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            NSString *strName = [CommonHelper  generatorString];
            
            //photo selected document file
            
            NSString *photoFile = [[FileHelper SelectedFiles] stringByAppendingPathComponent:strName];
            NSData *imageData = UIImageJPEGRepresentation(tmpimage,1);
            [imageData writeToFile:photoFile atomically:YES];
            
            NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
            
            
            //            assetURL.path;
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                
                CLLocation * loc = self.app.locationManager.location;
                
                if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
                    NSDictionary *retDic = @{@"image":strName,
                                             @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                                             @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
                    [self resultPhoto:@{@"index":@(0),@"dataPhoto":retDic}];
                }else{
                    [self resultPhoto:@{@"index":@(0),@"dataPhoto":@{@"image":strName}}];
                    
                }
                [self onCancel:nil];
                
            }else{
                void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *) = ^(ALAsset *asset)
                {
                    NSDictionary *metadata = asset.defaultRepresentation.metadata;
                    //                    ASLog(@"Image Meta Data: %@",metadata);
                    NSNumber *latNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Latitude"];
                    NSNumber *lonNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Longitude"];
                    
                    if (latNum && lonNum) {
                        NSDictionary *retDic = @{@"image":strName,
                                                 @"Latitude":latNum,
                                                 @"Longitude":lonNum};
                        [self resultPhoto:@{@"index":@(0),@"dataPhoto":@{@"image":retDic}}];
                        
                    }else{
                        [self resultPhoto:@{@"index":@(0),@"dataPhoto":@{@"image":strName}}];
                        
                    }
                    
                    [self onCancel:nil];
                };
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                [library assetForURL:assetURL
                         resultBlock:ALAssetsLibraryAssetForURLResultBlock
                        failureBlock:^(NSError *error) {
                        }];
            }
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [picker dismissViewControllerAnimated: YES completion: NULL];
    
    [self onCancel:nil];
    [self onCancel:nil];
}

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//photo document.
-(void)resultPhoto:(NSDictionary*)dicResult
{
    if (dicResult[@"dataPhoto"]) {
        NSString *photoFile = [[FileHelper SelectedFiles] stringByAppendingPathComponent:dicResult[@"dataPhoto"][@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        [arrDocument addObject:@{@"type":@1,
                                 @"thumbnail":imgData,
                                 @"filePath":photoFile,
                                 @"fileName":dicResult[@"dataPhoto"][@"image"],
                                 @"keyPathFile":dicResult[@"dataPhoto"][@"image"] }];
        [self.tableDocument reloadData];
    }
    
    
}

- (BOOL)isValidData{
    
    if ([nameTextField.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    return YES;
}
- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}
-(void)fnChangeDog
{
    if (![self isValidData]) {
        return;
    }
    postDict =[NSMutableDictionary new];
    attachment= [NSMutableDictionary new];
    
    
    NSString *strKind = @"";
    NSString *strNameMedias = @"";
    switch (self.myTypeVview) {
        case TYPE_DOG:
        {
            strKind = PROFILE_MULTI_DOGS;
            strNameMedias = PROFILE_ONE_DOG;
            if (nameTextField.text.length>0) {
                [postDict setValue:nameTextField.text forKey:@"dog[name]"];
            }
            for (NSDictionary *dicbreed in arrBreed) {
                if ([dicbreed[@"isSelect"] boolValue] == YES) {
                    //marque = no select...id...temp naturapass key
                    if (!([dicbreed[@"id"] intValue] == 1000000)) {
                        [postDict setValue:dicbreed[@"id"] forKey:@"dog[breed]"];
                    }
                    
                }
            }
            for (NSDictionary *dicSex in arrSex) {
                if ([dicSex[@"isSelect"] boolValue] == YES) {
                    if (!([dicSex[@"id"] intValue] == 1000000)) {
                        [postDict setValue:dicSex[@"id"] forKey:@"dog[sex]"];
                    }
                }
            }
            for (NSDictionary *dicType in arrType) {
                if ([dicType[@"isSelect"] boolValue] == YES) {
                    if (!([dicType[@"id"] intValue] == 1000000)) {
                        
                        [postDict setValue:dicType[@"id"] forKey:@"dog[type]"];
                    }
                }
            }
            if (self.dateTextField.text.length>0) {
                NSString *date = self.dateTextField.text;//[self convertDate:self.dateTextField.text]; dd-MM-YYY
                [postDict setValue:date forKey:@"dog[birthday]"];
                
            }
            
        }
            break;
        case TYPE_WEAPONS:
        {
            strKind = PROFILE_MULTI_WEAPONS;
            strNameMedias = PROFILE_ONE_WEAPON;
            
            if (nameTextField.text.length>0) {
                [postDict setValue:nameTextField.text forKey:@"weapon[name]"];
            }
            for (NSDictionary *dicbreed in arrBreed) {
                if ([dicbreed[@"isSelect"] boolValue] == YES) {
                    if (!([dicbreed[@"id"] intValue] == 1000000)) {
                        
                        [postDict setValue:dicbreed[@"id"] forKey:@"weapon[brand]"];
                    }
                }
            }
            for (NSDictionary *dicSex in arrSex) {
                if ([dicSex[@"isSelect"] boolValue] == YES) {
                    if (!([dicSex[@"id"] intValue] == 1000000)) {
                        
                        [postDict setValue:dicSex[@"id"] forKey:@"weapon[calibre]"];
                    }
                }
            }
            for (NSDictionary *dicType in arrType) {
                if ([dicType[@"isSelect"] boolValue] == YES) {
                    if (!([dicType[@"id"] intValue] == 1000000)) {
                        
                        [postDict setValue:dicType[@"id"] forKey:@"weapon[type]"];
                    }
                }
            }
            
        }
            break;
        default:
            break;
    }
    
    if (dataPhoto != nil) {
        UIImage *image = [UIImage imageWithData:dataPhoto];
        NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //
        
        [attachment setObject:@{@"name":[NSString stringWithFormat:@"%@[photo][file]",strNameMedias],@"kFileName": @"image.png",@"kFileData": fileData} forKey:@"photo"];
    }
    

    //generate data.
    
    [COMMON addLoading:self];
    
    //upload text => got ID => continue uploading files...
    /**
     * add pic a dog
     *
     * POST /v2/users/{ID_DOG]/profiles/addpicdogs/media
     *
     * Content-Type: form-data
     *      dog[medias][1000][file] = Document
     *      dog[medias][1001][file] = Document
     
     putProfileFilesWithKindID
     
     **/
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    //add field name.
    
    if (self.isModifi) {
        [serviceObj putProfileWithKindID:self.dicModifi[@"id"] withKind:self.myTypeVview withParams:postDict attachment:attachment!=nil?attachment:NULL];
    }
    else
    {
        [serviceObj postProfileWithParam:postDict attachment:attachment withKind:self.myTypeVview];
    }
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        if (arrDocumentsNeedUploading.count > 0) {
            //continue uploading
            //update fail.....Modification fail
            
            if ([response isKindOfClass: [NSDictionary class]]) {
                switch (self.myTypeVview) {
                    case TYPE_DOG:
                    {
                        if ( [response[@"dog"] isKindOfClass: [NSDictionary class]]) {
                            [self continueUploadWithProfileID:response[@"dog"][@"id"] withArray: arrDocumentsNeedUploading];
                        }
                    }
                        break;
                    case TYPE_WEAPONS:
                    {
                        if ( [response[@"weapon"] isKindOfClass: [NSDictionary class]]) {
                            [self continueUploadWithProfileID:response[@"weapon"][@"id"] withArray: arrDocumentsNeedUploading];
                        }
                    }
                        break;
                    case TYPE_PAPER:
                    {
                        if ( [response[@"paper"] isKindOfClass: [NSDictionary class]]) {
                            [self continueUploadWithProfileID:response[@"paper"][@"id"] withArray: arrDocumentsNeedUploading];
                        }
                    }
                        break;
                    default:
                        break;
                }
            }else{
                [COMMON removeProgressLoading];
                
                [self gotoback];
                
            }
        }else{
            [COMMON removeProgressLoading];
            
            [self gotoback];
        }
    };
}


-(void) continueUploadWithProfileID:(NSString*)strID withArray:(NSArray*)arrFiles
{
    if (arrFiles.count >0) {
        NSMutableArray *mutArr = [arrFiles mutableCopy];
        
        NSDictionary *dic =  arrFiles[0];
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        //if attachment => only post text...set null
        
        [serviceObj putProfileFilesWithKindID:strID attachment:dic withParam:postDict withKind:self.myTypeVview];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode)
        {
            NSDictionary *firstObj = nil;
            NSArray *checkArr = nil;
            switch (self.myTypeVview) {
                case TYPE_DOG:
                {
                    checkArr = response[@"dog"][@"medias"];
                }
                    break;
                case TYPE_WEAPONS:
                {
                    checkArr = response[@"weapon"][@"medias"];
                }
                    break;
                case TYPE_PAPER:
                {
                    checkArr = response[@"paper"][@"medias"];
                }
                    break;
                default:
                    break;
            }

            if (checkArr.count > 0)
            {
                switch (self.myTypeVview) {
                    case TYPE_DOG:
                    {
                        firstObj = response[@"dog"][@"medias"][@"lastpic"];
                    }
                        break;
                    case TYPE_WEAPONS:
                    {
                        firstObj = response[@"weapon"][@"medias"][@"lastpic"];
                    }
                        break;
                    case TYPE_PAPER:
                    {
                        firstObj = response[@"paper"][@"medias"][@"lastpic"];
                    }
                        break;
                    default:
                        break;
                }

                NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:firstObj[@"path"]]];
                
                
                if ([dic[@"kType"] isEqualToString:@"PDF"]) {
                    //uploaded file PDF...rename it
                    
                    strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
                    
                    strImage=[strImage stringByReplacingOccurrencesOfString:@"jpeg" withString:@"pdf"];
                    
                    NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
                    
                    //SelectedFiles
                    //write kFileData to file. PDF
                    
                    
                    NSString *destPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
                    
                    ASLog(@"%@",destPath);
                    
                    [ dic[@"kFileData"]  writeToFile:destPath atomically:YES];
                    
                    
                }else if ([dic[@"kType"] isEqualToString:@"IMAGE"]){
                    //uploaded photo...save it.
                    //kFileData ? Image save only https://www.naturapass.com/uploads/dogs/images/resize/c738cd0d7b7b092fbc17265d1f7023c2b0a6af0b.jpeg
                    strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
                    
                    NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
                    //upload successful...rename local file.
                    NSString *destPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
                    
                    [FileHelper renameFile:dic[@"kLocalPath"] toName: destPath];
                    
                    ASLog(@"%@",destPath);
                    
                    [ dic[@"kFileData"]  writeToFile:destPath atomically:YES];
                    
                }else if ([dic[@"kType"] isEqualToString:@"VIDEO"]){
                    
                    //Don't save video. ??? Del file video.
                    strImage=[strImage stringByReplacingOccurrencesOfString:@"videos/resize" withString:@"videos/mp4"];
                    strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".mp4"];
                    strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg" withString:@".mp4"];
                    
                    NSString *nameFile =  [CommonHelper stringToMD5: strImage ];
                    //upload successful...rename local file.
                    NSString *destPath = [[FileHelper SelectedFiles] stringByAppendingPathComponent:nameFile];
                    
                    [FileHelper renameFile:dic[@"kLocalPath"] toName: destPath];
                    
                    ASLog(@"%@",destPath);
                    
                    [ dic[@"kFileData"]  writeToFile:destPath atomically:YES];
                    
                }
            
            }

        
            //continue upload
            [mutArr removeObjectAtIndex:0];

            [self continueUploadWithProfileID:strID withArray: mutArr ];
        };
    
    }else{
        [COMMON removeProgressLoading];
        [self gotoback];
    }
}

@end
