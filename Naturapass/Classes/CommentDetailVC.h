//
//  CommentDetailVC.h
//  Naturapass
//
//  Created by Manh on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "Define.h"
#import "SettingActionView.h"
typedef void (^CommentDetailVCCallback)(NSString *pubicationID);
typedef void (^callBackWithDeleteItem) (NSDictionary *dicPublication);
@interface CommentDetailVC :BaseVC
{
    SettingActionView *shareDetail;

}

@property (nonatomic,copy) CommentDetailVCCallback callback;

-(void)doCallback:(CommentDetailVCCallback)callback;

@property (nonatomic,retain) NSDictionary *commentDic;
@property (nonatomic, copy) callBackWithDeleteItem callbackWithDelItem;
@property (nonatomic,assign) BOOL isHiddenComment;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@end
