//
//  Card_Cell_Type10.m
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Card_Cell_Type40.h"
#import "PublicationControlCheckboxCell.h"
static NSString *identifierCard = @"CardCheckboxCellID";
static NSString *tableCell = @"PublicationControlCheckboxCell";

@implementation Card_Cell_Type40

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [self.tableControl registerNib:[UINib nibWithNibName:tableCell bundle:nil] forCellReuseIdentifier:identifierCard];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//
-(void)fnSetData:(NSArray*)arrContent  arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC
{
    typeControl =typeC;
    //mld
    arrData =arrContent;
    listCheck = arrCheck;
    arrMultiple= [NSMutableArray arrayWithArray:arrData];
    for (int i =0; i <listCheck.count; i++) {
        for (int j=0; j< arrMultiple.count; j++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[j]];
            if ([listCheck[i] intValue]== [dic[@"id"] intValue]) {
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:j withObject:dic];
                break;
            }
            
        }
    }
    
    int count =0;
    if (arrContent.count>4) {
        count= 4;
    }
    else
    {
        count= (int)arrContent.count;
    }
    self.constraintHeightTable.constant = count*40;
    [self.tableControl reloadData];
}

#pragma callback
-(void)setCallback:(AlertSeclectCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSeclectCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrMultiple count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicationControlCheckboxCell *cell = nil;
    
    cell = (PublicationControlCheckboxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierCard forIndexPath:indexPath];
    NSDictionary *dic = arrMultiple[indexPath.row];
    cell.lbTittle.textColor = [UIColor whiteColor];
    cell.lbTittle.text = dic[@"name"];
    if (typeControl == CONTROL_RADIO) {
        self.imgCheckBoxActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:self.colorNavigation colorBorder:[UIColor darkGrayColor] isRadio:YES];
        self.imgCheckBoxInActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:[UIColor whiteColor] colorBorder:[UIColor darkGrayColor] isRadio:YES];
    }
    else
    {
        self.imgCheckBoxActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:self.colorNavigation colorBorder:[UIColor whiteColor] isRadio:NO];
        self.imgCheckBoxInActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:[UIColor whiteColor] colorBorder:[UIColor whiteColor] isRadio:NO];

    }
    if ([dic[@"isSelect"] boolValue]== YES) {
        
        [cell.btnCheckBox setBackgroundImage:self.imgCheckBoxActive forState:UIControlStateNormal];
    }
    else
    {
        [cell.btnCheckBox setBackgroundImage:self.imgCheckBoxInActive forState:UIControlStateNormal];
    }
    cell.btnCheckBox.tag = indexPath.row +200;
    [cell.btnCheckBox addTarget:self action:@selector(selectAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self fnSelectItem:indexPath.row];
}
- (IBAction)selectAction:(id)sender
{
    int index = (int)([sender tag] -200);
    [self fnSelectItem:index];
}
-(void)fnSelectItem:(int)index
{
    switch (typeControl) {
        case CONTROL_CHECKBOX:
        {
            if (index<arrMultiple.count) {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [self.tableControl reloadData];
            }
        }
            break;
        case CONTROL_RADIO:
        {
            if (index<arrMultiple.count) {
                arrMultiple= [NSMutableArray arrayWithArray:arrData];
                
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMultiple[index]];
                if ([dic[@"isSelect"] boolValue]==YES) {
                    [dic setValue:@0 forKey:@"isSelect"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"isSelect"];
                }
                [arrMultiple replaceObjectAtIndex:index withObject:dic];
                [self.tableControl reloadData];
            }
        }
            break;
        default:
            break;
    }
    
    NSMutableArray *arrValue = [NSMutableArray new];
    NSMutableArray *arrText = [NSMutableArray new];
    
    for (NSDictionary *dic in arrMultiple) {
        if ([dic[@"isSelect"] boolValue]== YES) {
            [arrValue addObject:dic[@"id"]];
            [arrText addObject:dic[@"name"]];
        }
    }
    if (_callback) {
        _callback(@{@"value":arrValue,@"text":arrText});
    }
}
@end
