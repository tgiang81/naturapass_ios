//
//  ChassesMessView.m
//  Naturapass
//
//  Created by Giang on 7/16/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesMurVC.h"


#import "Config.h"
#import "AppCommon.h"
#import "WebServiceAPI.h"
#import <CoreLocation/CoreLocation.h>
#import "ASSharedTimeFormatter.h"
#import "Define.h"
#import "ChassesCreateV2.h"
#import "ChassesCreate_Step5.h"
#import "UIAlertView+Blocks.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TypeCell1.h"
#import "MLKMenuPopover.h"
#import "GroupEnterMurVC.h"
#import "ChassesParticipeVC.h"
#import "ChassesCreateOBJ.h"
#import "GroupEnterOBJ.h"
#import "CommonHelper.h"
#import "ChasseSettingMembres.h"
#import "MapLocationVC.h"
#import "ChassesCreateV2.h"
#import "AmisAddScreen1.h"
#import "GroupCreateOBJ.h"
#import "GroupCreate_Step1.h"
#import "ChassesCreateOBJ.h"
#import "ChassesCreateV2.h"
#import "Publication_FavoriteAddress.h"
#import "ChatListe.h"
#import "Publication_Carte.h"
#import "SignalerTerminez.h"
static NSString *typecell1 = @"TypeCell1";
static NSString *typecell1ID = @"TypeCell1ID";

@interface ChassesMurVC ()<WebServiceAPIDelegate,CLLocationManagerDelegate, UITableViewDelegate>
{
    CLLocationManager   *locationManager;
    CLPlacemark         *placemark;
    NSTimer             *timer;
    __weak IBOutlet UILabel *lbTitle;
}
@property (nonatomic, strong) TypeCell1 *prototypeCell;
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;
@end

@implementation ChassesMurVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMONAGENDA);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doRefresh) name: NOTIFY_REFRESH_MES_CHASSES object:nil];
    
    //init table view cell
    [self.tableControl registerNib:[UINib nibWithNibName:typecell1 bundle:nil] forCellReuseIdentifier:typecell1ID];
    self.tableControl.estimatedRowHeight = 100;
    messalonArray=[[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view from its nib.
    self.isGoin = YES;
    [self initRefreshControl];
    _lbMessage.text= str(EMPTY_CHASSE_MES) ;
    [self addShortCut];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [Flurry logEvent:@"MesChasseFragment" timed:YES];

    if (self.isGoin) {
        self.isGoin = NO;
        [self intialization];

    }
    

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];

    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_hunt,kSQL_agenda]];
}

-(void) refreshVisibleCell
{

}

-(void)intialization{
    [self loadCache];
    [self insertRowAtTop];
}
- (TypeCell1 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:typecell1ID];
    }
    return _prototypeCell;
}

-(void)loadCache
{
    
    //load from cache
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHASSE_MUR_SAVE)   ];
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    
    [messalonArray removeAllObjects];
    [messalonArray  addObjectsFromArray:arrTmp];
    [self.tableControl reloadData];
    [self stopRefreshControl];
    
}

-(void)checkEmpty:(NSMutableArray*)arr msg:(NSString*)strMsg
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
    }
    else
    {
        [self.lbMessage setText:strMsg];
        
        [self.lbMessage setNeedsDisplay];
        
        self.lbMessage.hidden=NO;
        [self.view bringSubviewToFront:self.lbMessage];
    }
}
//overwrite
- (void)insertRowAtTop {
    [self getMesSalon];
}

//overwrite
- (void)insertRowAtBottom {
    
    [self getOffsetMesSalons];
}

-(void) doRefresh
{
    [self startRefreshControl];
}

#pragma notification subcrice admin

-(void)checkNotificationRefreshMesChass:(NSNotification*)notifi

{
    [self getMesSalon];
}

#pragma mark - Webservice

-(void)getMesSalon{
    
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        //offline -> don't show the mesage "you have no groups"
        self.lbMessage.hidden=YES;
        
        [KSToastView ks_showToast: str(strYouHaveNoChasse) duration:2.0f completion: ^{
        }];
        
        return;
    }

    
    __weak typeof(self) wself = self;    
        WebServiceAPI *getSalon = [[WebServiceAPI alloc]init];
        [getSalon getUserLoungesAction:loungeList_Limit withOffset:@"0"];
        getSalon.onComplete = ^(NSDictionary *response, int errCode)
        {
            dispatch_async( dispatch_get_main_queue(), ^{
                [COMMON removeProgressLoading];
                [self stopRefreshControl];
                
                if (errCode == -1001)
                {
                    //timeout
                    [self checkEmpty:messalonArray msg:str(strTryAgainWithYourGoodNetwork)];
                    
                    return;
                }
                if ([wself fnCheckResponse:response])
                {
                    [self checkEmpty:messalonArray msg:EMPTY_CHASSE_MES];
                    return;
                }                
                if (!response) {
                    
                    [self checkEmpty:messalonArray msg:str(EMPTY_CHASSE_MES)];
                    return ;
                }
                if([response isKindOfClass: [NSArray class] ]) {
                    [self checkEmpty:messalonArray msg:str(EMPTY_CHASSE_MES)];
                    return ;
                }
                
                if([response isKindOfClass:[NSDictionary class]]){
                    [messalonArray removeAllObjects];
                    [messalonArray addObjectsFromArray: [response objectForKey:@"lounges"]];
                    [self.tableControl reloadData];
                    // Path to save array data
                    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHASSE_MUR_SAVE)   ];
                    // Write array
                    [messalonArray writeToFile:strPath atomically:YES];
                }
                [self checkEmpty:messalonArray msg:str(EMPTY_CHASSE_MES)];

            });
            
        };

    //    [COMMON addLoading:self];
    
}

-(void)getOffsetMesSalons{
    
    int strPageCount =  (int)[messalonArray count];
    
    //
    if (strPageCount == 0) {
        [self stopRefreshControl];
        return;
    }
    
    __weak typeof(self) wself = self;
    WebServiceAPI *getSalonPageCount = [[WebServiceAPI alloc]init];
    [getSalonPageCount getUserLoungesAction:loungeList_Limit withOffset:[NSString stringWithFormat:@"%ld",(long)strPageCount]];
    getSalonPageCount.onComplete = ^(NSDictionary *response, int code)
    {

        [self stopRefreshControl];
        
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        if([response isKindOfClass:[NSDictionary class]]){
            
            if([[response objectForKey:@"lounges"] count]>0)
            {
                
                //check if exist... abort.
                
                for (NSDictionary*mDic in [response objectForKey:@"lounges"]) {
                    if ([messalonArray containsObject:mDic]) {
                        continue;
                    }
                    //else
                    [messalonArray addObject:mDic];
                }
                
                [self.tableControl reloadData];
                // Path to save array data
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHASSE_MUR_SAVE)   ];
                // Write array
                [messalonArray writeToFile:strPath atomically:YES];
            }
        }
        [self checkEmpty:messalonArray msg:str(EMPTY_CHASSE_MES)];
    };
}
-(void)getItemWithKind:(NSString*)mykind myid:(NSString*)myid
{
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getItemWithKind:mykind myid:myid];
    serviceAPI.onComplete=^(NSDictionary *response, int errCode)
    {
        NSMutableDictionary *nsDic =[NSMutableDictionary dictionaryWithDictionary: response[@"lounge"]];
        for (int i=0; i<messalonArray.count; i++) {
            NSDictionary *dic = [messalonArray objectAtIndex:i];
            if ([nsDic[@"id"] isEqual:dic[@"id"]]) {
                [messalonArray replaceObjectAtIndex:i withObject:nsDic];
                //reload item
                NSArray *indexArray =[self.tableControl indexPathsForVisibleRows];
                [self.tableControl reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
                break;
            }
        }
    };
}
#pragma mark - TableView
-(void)getParticipationName:(int)participationId imgPartic:(UIImageView*)imgPartic
{
    NSString *url =@"";
    switch (participationId) {
        case 0:// Ne partticipe pas
        {
            url =@"ic_not_participate";
        }
            break;
        case 1://Participe
        {
            url =@"ic_participate";
        }
            break;
        case 2://sais pas
        {
            url =@"ic_pre_participate";
        }
            break;
        default:
            break;
    }
    [imgPartic setImage:[UIImage imageNamed:url]];
}

-(void) setDataForMessCell:(TypeCell1*) cell withIndex:(NSIndexPath *)indexPath
{
    if([messalonArray count]>0){
        
        NSDictionary*dic = messalonArray[indexPath.row];
        
        NSString *strAdminLabelBlack = [NSString stringWithFormat:@"%@ %@",str(strAdministre_par),dic[@"owner"][@"fullname"]];
        [cell.label11 setText:strAdminLabelBlack];
        
        //PARTIC
        [self getParticipationName: (int)[dic[@"connected"][@"participation"] integerValue] imgPartic:cell.Img4];
        //IMAGE
        
        NSString *strImage;
        
        if (messalonArray[indexPath.row][@"profilepicture"] != nil) {
            strImage=messalonArray[indexPath.row][@"profilepicture"];
        }else{
            strImage=messalonArray[indexPath.row][@"photo"];
        }
        
        
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
        
        [cell.Img1 sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_photo"] ];
        NSString *strDescription = [dic[@"description"] emo_emojiString];
        NSString *strName = [dic[@"name"] emo_emojiString];

        // NAME
        [cell.label1 setText:strName];
        // DESC
        cell.label10.text=strDescription;
        //ACCESS
        strAccess=[NSString stringWithFormat:@"%@",dic[@"access"]];
        if([strAccess isEqualToString:@"0"])
            cell.label2.text=str(strAccessPrivate);
        else if([strAccess isEqualToString:@"1"])
            cell.label2.text=str(strAccessSemiPrivate);
        
        else if([strAccess isEqualToString:@"2"])
            cell.label2.text=str(strAccessPublic);
        //PARTICIPATION
        
        
        if ( [messalonArray[indexPath.row][@"nbParticipants"] intValue] > 1) {
            cell.label3.text =[NSString stringWithFormat:@"%d participants",[dic[@"nbParticipants"] intValue]];
        }else{
            cell.label3.text =[NSString stringWithFormat:@"%d participant",[dic[@"nbParticipants"] intValue]];
        }
        
        //
        
        //DATE
        NSString * dateStrings = [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meetingDate"]]];
        if(dateStrings !=nil)
            [cell.label5 setText:[NSString stringWithFormat:@"%@",dateStrings]];
        else
            [cell.label5 setText:@""];
        //PENDING
        if ( [dic[@"nbPending"] intValue] == 0) {
            cell.label12.hidden = YES;
        }else if ( [dic[@"nbPending"] intValue] > 1) {
            [cell.label12 setText:[NSString stringWithFormat:@"%@ %@",dic[@"nbPending"],str(strPersonnes_en_attente_de_validation)]];
        }else{
            [cell.label12 setText:[NSString stringWithFormat:@"%@ %@",dic[@"nbPending"],str(strPersonne_en_attente_de_validation)]];
        }
        
        //ADDRESS
        NSString*strLocation;
        if(messalonArray[indexPath.row][@"meetingAddress"][@"address"]!=nil){
            strLocation=[NSString stringWithFormat:@"%@",dic[@"meetingAddress"][@"address"]];
            [cell.label9 setText:strLocation];
        }
        else {
            [cell.label9 setText: [ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meetingAddress"][@"latitude"] floatValue],
                                   [dic[@"meetingAddress"][@"longitude"] floatValue]
                                   ] ];
        }
        
        //END DATE
        NSString *outputStrings= [self convertDate:[NSString stringWithFormat:@"%@",dic[@"endDate"]]];
        if(outputStrings !=nil)
            cell.label7.text = [NSString stringWithFormat:@"%@", outputStrings];
        else
            cell.label7.text = strEMPTY;
        //BUTTON
        //Button 1...Enter
        [cell.button1 addTarget:self action:@selector(enterChasesWall:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnLabel addTarget:self action:@selector(enterChasesWall:) forControlEvents:UIControlEventTouchUpInside];
        
        
        //Check address... enable click...
        
        [cell.btnLocation addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnLocation.tag = indexPath.row + 100;
        
        /*
         meetingAddress =     {
         address = "San Francisco";
         latitude = "37.785834";
         longitude = "-122.406417";
         };
         */
        
        //second button 2 admin/ 0 normal
        //admin... go in
        if([dic[@"connected"][@"access"]isEqual:@3])
        {
            [cell.button2 removeTarget:nil
                                action:NULL
                      forControlEvents:UIControlEventAllEvents];
            [cell.button2 addTarget:self action:@selector(enterChassesInfoAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.Img6.image = [UIImage imageNamed:@"ic_admin_setting"];
            [cell.button2 setTitle:@"ADMINISTRER" forState:UIControlStateNormal];
            
            [cell.button2 setBackgroundImage: [UIImage imageNamed:@"btn_chasse_bg"] forState:UIControlStateNormal];
            [cell createMenuList:@[@"Administrer", @"Fermer l'événement"] ];
            cell.btnSetting.tag = 5000+indexPath.row;
            [cell.btnSetting addTarget:self action:@selector(showMenuPopOver:) forControlEvents:UIControlEventTouchUpInside];
            cell.Img3.hidden =NO;
            cell.label3.hidden =NO;
        }
        //normal...un-join
        else //if([messalonArray[indexPath.row][@"connected"][@"access"]isEqual:@2])
        {
            [cell.button2 removeTarget:nil
                                action:NULL
                      forControlEvents:UIControlEventAllEvents];
            [cell.button2 addTarget:self action:@selector(deleteJoinAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.Img6.image = [UIImage imageNamed:@"ic_close"];
            [cell.button2 setTitle:@"SE DESINSCRIRE" forState:UIControlStateNormal];
            [cell.button2 setBackgroundImage: [UIImage imageNamed:@"btn_chasse_orange_bg"] forState:UIControlStateNormal];
            
            
            [cell.btnSetting removeTarget:nil
                                   action:NULL
                         forControlEvents:UIControlEventAllEvents];
            cell.imgBackGroundSetting.hidden=YES;
            cell.imgSettingSelected.hidden=YES;
            cell.imgSettingNormal.hidden=YES;
            cell.Img3.hidden =YES;
            cell.label3.hidden =YES;
        }
        [cell.button5 addTarget:self action:@selector(participateEventAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.button5.tag=indexPath.row+5;
        //tag
        cell.button1.tag=indexPath.row+1;
        cell.btnLabel.tag=indexPath.row+1;

        
        cell.button2.tag=indexPath.row+2;
        cell.button3.tag=indexPath.row+3;
        cell.button4.tag=indexPath.row+4;
        cell.label12.tag=indexPath.row+12;
        // setting theme
        if([messalonArray[indexPath.row][@"connected"][@"access"]isEqual:@3])
        {
            [cell fnSettingCell:UI_CHASSES_MUR_ADMIN];
            if ([messalonArray[indexPath.row][@"nbPending"] integerValue]>0) {
                [cell.button4 addTarget:self action:@selector(friendRequestAction:) forControlEvents:UIControlEventTouchUpInside];
                cell.view4.hidden=NO;
                cell.constraintHeight2.constant = 20;
            }
            else
            {
                cell.view4.hidden=YES;
                cell.constraintHeight2.constant = 0;
                
            }
        }else if([dic[@"connected"][@"access"]isEqual:@2])
        {
            [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
        }
        
        [cell layoutIfNeeded];
        
    }
}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TypeCell1 *cell=(TypeCell1 *) cellTmp;
    [self setDataForMessCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [messalonArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([messalonArray count]>0){
        TypeCell1 *cell = [tableView dequeueReusableCellWithIdentifier:typecell1ID forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    }
    return nil;
}

-(IBAction)friendRequestAction:(UIButton *)sender {
    
    int iIndex = (int) (sender.tag  - 4);
    NSDictionary *dic = messalonArray [iIndex];
    ChasseSettingMembres *viewController1 = [[ChasseSettingMembres alloc] initWithNibName:@"ChasseSettingMembres" bundle:nil];
    
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    [self pushVC:viewController1 animate:NO];
    
}
#pragma notification subcrice admin
-(void)checkNotificationLoungeSubcriceAdmin:(NSNotification*)notifi
{
    NSString *strID =(NSString*)[notifi object];
    [self getItemWithKind:MYCHASSE myid:strID];
    
}

#pragma mark - doAction

- (IBAction)locationButtonAction:(UIButton*) sender
{
    int index = (int) sender.tag - 100;
    NSDictionary *myDic = messalonArray[index];
    
    id dic = myDic[@"meetingAddress"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = YES;
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"],
                                      @"name": myDic[@"name"]};

        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = myDic;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        
    }
}


-(IBAction)createAction:(id)sender
{
    [[ChassesCreateOBJ sharedInstance] resetParams];
    ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
    [self pushVC:viewController1 animate:YES];
    
}
-(IBAction)participateEventAction:(UIButton *)sender
{
    int index = sender.tag-5;
    __weak ChassesMurVC *wself =self;
    ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
    viewController1.dicChassesItem =messalonArray[index];
    [viewController1 doCallback:^(NSString *strID) {
        [wself getItemWithKind:@"lounges" myid:strID];
    }];
    [self pushVC:viewController1 animate:YES];
}
-(IBAction)deleteJoinAction:(UIButton *)sender {
    indexLounge =sender.tag-2;
    NSString *strContent = [ NSString stringWithFormat: str(strDeleteJoinMessageChasse)];
    [UIAlertView showWithTitle:str(strDeleteJoinTitleChasse)
                       message:strContent
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {//ANNULER
                              
                          }
                          else //Oui
                          {
                              [COMMON addLoading:self];
                              WebServiceAPI *deleteLoungAction = [[WebServiceAPI alloc]init];
                              [deleteLoungAction deleteLoungeJoinAction:[[messalonArray objectAtIndex:indexLounge]valueForKey:@"id"]];
                              deleteLoungAction.onComplete =^(NSDictionary *response, int errCode)
                              {
                                  [COMMON removeProgressLoading];
                                  [self getMesSalon];
                              };
                          }
                      }];

    
}
-(void)modifyChasse:(NSInteger)index
{
    NSDictionary *dic = [messalonArray objectAtIndex:index];
    
    [[ChassesCreateOBJ sharedInstance] fnSetData:dic];

    ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
    [self pushVC:viewController1 animate:YES];


}
-(void)closeGroupAction:(NSInteger)index
{
    NSDictionary *dic = [messalonArray objectAtIndex:index];
    NSString *strContent = [ NSString stringWithFormat: str(strCloseChasse)];
    [UIAlertView showWithTitle:str(strTitle_app)
                       message:strContent
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                          }
                          else
                          {
                              
                              [COMMON addLoading:self];
                              
                              WebServiceAPI *serviceObj = [WebServiceAPI new];
                              [serviceObj deleteLoungeAction: dic[@"id"]];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  
                                  if (!response) {
                                      return ;
                                  }
                                  
                                  if([response isKindOfClass: [NSArray class] ]) {
                                      return ;
                                  }
                                  
                                  if ([response[@"success"] integerValue]==1) {
                                      
                                      //delete file...cache.
                                      NSString *strGroup_id = [GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"];
                                      
                                      NSString *catStr =  [NSString stringWithFormat:@"%@_%@_",[COMMON getUserId], strGroup_id];
                                      
                                      NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(catStr ,FILE_SETTING_AGENDA_SAVE)];
                                      [FileHelper removeFileAtPath:strPath];
                                      

                                      
                                      
                                      [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                                      
                                      AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                                      [app updateAllowShowAdd:response];

                                      [messalonArray removeObjectAtIndex:index];
                                      [self.tableControl reloadData];
                                      
                                      [self checkEmpty:messalonArray msg:EMPTY_CHASSE_MES];
                                      
                                      [self moreRefreshControl];
                                  }
                              };
                          }
                      }];
}

-(void)enterChassesInfoAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    index = tag - 2;
    [self modifyChasse:index];
}

#pragma mark - CONVERTDATE
-(NSString*)convertDateTime:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *dateFormatterTer = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd/MM/yyyy HH:mm" forFormatter: dateFormatterTer];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ dateFormatterTer stringFromDate:inputDates ];
    return outputStrings;
}
#pragma mark - ENTER a Chass

-(IBAction)enterChasesWall:(id)sender
{
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[TypeCell1 class]]) {
        parent = parent.superview;
    }
    
    TypeCell1 *cell = (TypeCell1 *)parent;
    int iIndex = (int) (cell.button1.tag  - 1);
    NSDictionary *dic = messalonArray [iIndex];
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

- (void)loadSelected:(NSString *)_strParticipate
{
    [self getMesSalon];
    strParticipate=_strParticipate;
    [self.tableControl reloadData];
}
#pragma mark MENU POPUP
- (IBAction)showMenuPopOver:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = 0;
    index = tag - 5000;
    //
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[TypeCell1 class]]) {
        parent = parent.superview;
    }
    
    TypeCell1 *cell = (TypeCell1 *)parent;
    // Hide already showing popover
    [cell show];
    //
    __weak ChassesMurVC *wself =self;
    [cell setCallBackGroup:^(NSInteger ind)
     {
         if (ind==1) {
             [wself closeGroupAction:index];
         }
         else if(ind==0)
         {
             [self modifyChasse:index];
         }
     }];
    
    
}
//MARK: short cut
-(void) addShortCut
{
    __weak typeof(self) wself = self;
    self.btnShortCut.hidden = NO;
    self.viewShortCut = [[ShortcutScreenVC alloc] initWithEVC:self expectTarget:ISMUR];
    [self.viewShortCut setCallback:^(SHORTCUT_ACTION_TYPE type)
     {
         wself.btnShortCut.hidden = NO;
         switch (type) {
             case SHORTCUT_ACTION_ADD_FIREND:
             {
                 AmisAddScreen1 *viewController1 = [[AmisAddScreen1 alloc] initWithNibName:@"AmisAddScreen1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_AGENDA:
             {
                 [[ChassesCreateOBJ sharedInstance] resetParams];
                 ChassesCreateV2 *viewController1 = [[ChassesCreateV2 alloc] initWithNibName:@"ChassesCreateV2" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_GROUP:
             {
                 [[GroupCreateOBJ sharedInstance] resetParams];
                 GroupCreate_Step1 *viewController1 = [[GroupCreate_Step1 alloc] initWithNibName:@"GroupCreate_Step1" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:YES];
                 
             }
                 break;
             case SHORTCUT_ACTION_DISCUSTION:
             {
                 ChatListe *viewController1 = [[ChatListe alloc] initWithNibName:@"ChatListe" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISDISCUSS iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_FAVORIS:
             {
                 Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
                 viewController1.isFromSetting = YES;
                 [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             case SHORTCUT_ACTION_CREATE_PUBLICATION:
             {
                 [[PublicationOBJ sharedInstance]  resetParams];
                 
                 SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
                 [wself pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:YES];
             }
                 break;
             default:
                 break;
         }
     }];
    self.viewShortCut.constraintBottomCloseButton.constant = 74;
    self.viewShortCut.constraintTraillingCloseButton.constant = 17;
    [self.viewShortCut addContraintSupview:self.view];
    self.viewShortCut.hidden = YES;
    [self.viewShortCut fnAllowAdd:YES];
}
-(IBAction)fnShortCut:(id)sender
{
    
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        self.btnShortCut.layer.transform = CATransform3DMakeRotation((180) * 45, 0, 0, 1);
        
        
    } completion:^(BOOL finished) {
        
        //finish rotate:
        self.btnShortCut.hidden = YES;
        [self.viewShortCut hide:NO];
        [self returnRotation];
    }];
}

-(void) returnRotation{
    [UIView animateWithDuration:0.1 animations:^{
        self.btnShortCut.transform = CGAffineTransformIdentity;
    }];
}

#pragma mark - dealloc
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
