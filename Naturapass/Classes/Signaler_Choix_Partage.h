//
//  Publication_Choix_Partage.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "SignalerBaseVC.h"
#import "UIImage+CheckBox.h"

@interface Signaler_Choix_Partage : SignalerBaseVC

@property(nonatomic,assign) BOOL isEditFavo;
@property (strong, nonatomic) IBOutlet UIView *viewBottom;
@property(nonatomic,strong)  UIImage *imgCheckBoxActive;
@property(nonatomic,strong)  UIImage *imgCheckBoxInActive;

@end
