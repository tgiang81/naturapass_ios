//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDCheckBox.h"
#import "CellBase.h"

@interface GroupesViewBaseCell : CellBase

@property (nonatomic, strong) IBOutlet UILabel *lblAdminGroup;

@property (nonatomic, strong) IBOutlet UIButton *btnEnterGroup;
@property (nonatomic, strong) IBOutlet UIButton *btnCloseGroup;
@property (nonatomic, strong) IBOutlet UIButton *btnUnsubscribe;
@property (nonatomic, strong) IBOutlet UIButton *btnAdminstrator;

@property (nonatomic, strong) IBOutlet UIButton *btnEmailNotify;
@property (nonatomic, strong) IBOutlet MDCheckBox *checkbox;


//
@property (nonatomic, strong) IBOutlet UIButton *btnInvite;


@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *txtDescription;
@property (nonatomic, strong) IBOutlet UILabel *lblAccess;
@property (nonatomic, strong) IBOutlet UILabel *lblNbSubcribers;
@property (nonatomic, strong) IBOutlet UILabel *lblNbPending;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar;
@property (nonatomic, strong) IBOutlet UIImageView *imgMember;

@property (nonatomic,strong) IBOutlet UIButton *btnValidation;
@property (nonatomic,strong) IBOutlet UIButton *btnBanniList;
@property (nonatomic,strong) IBOutlet UIButton *btnAdminList;
@property (nonatomic, strong) IBOutlet UILabel *lbBanniList;
@property (nonatomic, strong) IBOutlet UILabel *lbAdminList;

@property (weak, nonatomic) IBOutlet UIButton *btnLabel;

//@property (weak, nonatomic) IBOutlet UIView *viewBorder;
-(void)fnSettingCell:(int)type;
@end
