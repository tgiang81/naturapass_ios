//
//  DistributionView.m
//  Naturapass
//
//  Created by Giang on 6/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "DistributionView.h"
#import "DistributionAnnotation.h"
#import "ASImageView.h"
#import "MyCollectionCell.h"

#import "MyTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonHelper.h"
#import "NSString+HTML.h"

//#import "Define.h"
#import "Config.h"
#define  RADIUS 14
#define MOVE_DELTA 3
#define ICON_WIDTH  27
#define ICON_HEIGHT 27
static NSString *identifier = @"MyTableViewCell";

@interface DistributionView ()
{
    NSMutableArray *arrMutPartners;

}
@property (nonatomic, strong)   NSMutableArray *arrPartners;
@property (nonatomic, strong)   NSMutableArray *arrFetchData;
@property (weak, nonatomic)     IBOutlet UITableView *myTableView;
@property (nonatomic, strong)   IBOutlet  UICollectionView  *myCollectionView;
@property (nonatomic, strong)   UIImageView *imageView;
@property (nonatomic, assign) BOOL isObserving;
@property (nonatomic, strong) MyTableViewCell *prototypeCell;

@end

@implementation DistributionView

@synthesize contentView = _contentView;


- (instancetype) initForDisMapViewAnnotation: (DistributionAnnotation *) imageMapAnnotation {
    
    self = [super initForMapAnnotation: imageMapAnnotation withReuseIdentifier: NSStringFromClass([self class])];
    
    if (self)
    {
        [self setMapViewAnnotation: imageMapAnnotation];
        
        self.clipsToBounds = NO;
        self.canShowCallout = NO;
        arrMutPartners = [NSMutableArray new];
        self.arrFetchData = [NSMutableArray new];
        self.arrPartners = [NSMutableArray new];
        pv.backgroundColor = [UIColor whiteColor];

        [self setHidesArrow: NO];

        CGRect fr = pv.frame;
        fr.origin.x = kNewMapAnnotationViewArrowWidth/2;
        pv.userInteractionEnabled = YES;
        pv.frame = fr;
        
        //border 
        [[CommonHelper sharedInstance] setRoundedView:pv toDiameter:RADIUS];
        
        [self addSubview: [self contentView]];
    }
    
    return self;
}

- (UIView *) contentView
{
    if (!_contentView)
    {
        pv = [[UIView alloc]initWithFrame:CGRectMake(kNewMapAnnotationViewArrowWidth/2 - MOVE_DELTA , 20, ICON_WIDTH + 5, heightPopOut)];
        pv.userInteractionEnabled = YES;
        
        UIImageView *imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, ICON_WIDTH, ICON_HEIGHT)];
        [pv addSubview:imgIcon];
        [imgIcon setImage: [UIImage imageNamed: @"ic_distributor_map" ]];
        
        imgIcon.contentMode =  UIViewContentModeScaleAspectFit;
        
        CGPoint pTmp = imgIcon.center;
        pTmp.x = (ICON_WIDTH + 5) / 2;
        
        pTmp.y = heightPopOut/2;
        
        imgIcon.center = pTmp;
        
        //border
        [pv.layer setMasksToBounds:YES];
        pv.layer.cornerRadius= RADIUS /2;
        pv.layer.borderWidth =0;
        
        _contentView = pv;
        _contentView.backgroundColor = [UIColor whiteColor];
        
    }
    
    return _contentView;
}
                        
-(void) fnCalcPopview
{
    
    UIView* vTmp = [pv viewWithTag:199];
    if (vTmp) {
        [vTmp removeFromSuperview];
    }
    
    UIView* vT2 = [pv viewWithTag:200];
    if (vT2) {
        [vT2 removeFromSuperview];
    }
    
    //199
    UIView *v = [[UIView alloc] init];
    v.tag = 199;
    
    float offset = 0;
    
    
    for (int i = 0; i < arrMutPartners.count; i++) {
        
        NSDictionary *dic = arrMutPartners[i];
        
        UIImageView*imgv = [[UIImageView alloc] init];
        
        imgv.image = [self fnResize:dic[@"image"] :heightImageInPop ];
        
        imgv.frame = CGRectMake(offset, 0, imgv.image.size.width, imgv.image.size.height);
        offset += imgv.image.size.width;
        [v addSubview:imgv];
    }
    
    v.frame = CGRectMake(ICON_WIDTH + 5, (heightPopOut - heightImageInPop)/2, offset, heightImageInPop);
    [pv addSubview:v];
    
    //200 move move....
    pv.frame = CGRectMake(kNewMapAnnotationViewArrowWidth/2 - MOVE_DELTA, 20, ICON_WIDTH + 5 + offset, heightPopOut);
    [pv.layer setMasksToBounds:YES];
    pv.layer.cornerRadius= RADIUS /2;
    pv.layer.borderWidth =0;

}

- (UIView *) backgroundView
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [backgroundView setBackgroundColor: [UIColor clearColor]];
    
    [backgroundView setOpaque: YES];
    return backgroundView;
}

-(UIImage*) fnResize :(UIImage*)img :(float)hHeight
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualWidth/actualHeight;
    
    float fixHeight = hHeight;
    
    imgRatio = fixHeight / actualHeight;
    actualWidth = imgRatio * actualWidth;
    actualHeight = fixHeight;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
     actualHeight = imgRatio * actualHeight;
     actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

/*
 {
 "c_address" = "104 CHEMIN DU MAS DE CHEYLON";
 "c_brands" = "[{\"name\":\"Browning\",\"partner\":1,\"logo\":\"\\/uploads\\/brands\\/images\\/resize\\/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg\"}]";
 "c_city" = NIMES;
 "c_cp" = 30900;
 "c_email" = "michel.antoine@armurerie-francaise.com";
 "c_id" = 663;
 "c_lat" = "0.0";
 "c_logo" = "";
 "c_lon" = "0.0";
 "c_name" = APOLLO;
 "c_tel" = "04 48 06 01 33";
 "c_updated" = 1433238045;
 }
 */

-(void) loadImageData
{
    [self.arrFetchData removeAllObjects];
    
    NSDictionary *dic = self.mapViewAnnotation.publicationDictionary;
    //CALLOUT PARTNER
    
    [self.arrPartners removeAllObjects];
    
    NSData *brandData = [dic[@"c_brands"] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSError *e;
    NSArray *brandArr = [NSJSONSerialization JSONObjectWithData:brandData options:NSJSONReadingMutableContainers error:&e];
    
    for (NSDictionary*obj in brandArr)
    {
        if ([obj[@"partner"] intValue] == 0)
        {
            //is brand
            [self.arrFetchData addObject:obj];
            //No partner -> only display icon
        }
        else if ([obj[@"partner"] intValue] == 1)
        {
            //Is partner:
            [self.arrPartners addObject:obj];
        }
        
    }
    //Download partners
    if (self.arrPartners.count > 0)
    {
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        __block int iCount = (int) self.arrPartners.count;
        
        
        [arrMutPartners removeAllObjects];
        for (NSDictionary*dic in self.arrPartners) {
            NSURL *url  = nil;
            
            url = [NSURL URLWithString: [ NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"logo"] ] ];

            NSLog(@"%@",url.absoluteString);
            [manager loadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                iCount -= 1;
                
                if (image) {
                    UIImage *imgLogo = [self fnResize:image :heightImageLogo];
                    
                    UIImage *imgCorrected = nil;
                    
                    if (imgLogo.size.width > widthCell) {
                        imgCorrected = [self fnResizeFixWidth:imgLogo :widthCell];
                    }

                    UIImage *rsized = [self fnResize:image :heightImageInPop];
                    
                    
                    [arrMutPartners addObject:@{ @"image":rsized,
                                                 @"size": [NSNumber numberWithFloat:rsized.size.width]
                                                 }];
                }
                
                if (iCount == 0) {
                    [self fnCalcPopview];
                }

            }];
        }
    }
    
}

- (void)doSetCalloutView {
    
    [self loadImageData];
}

- (void) setMapViewAnnotation: (DistributionAnnotation *) ano
{
    [super setMapViewAnnotation: ano];
    
    [self setAnnotation: ano];
}
-(void) btnClick
{
}

@end
