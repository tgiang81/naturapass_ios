//
//  MurAgendaCell.h
//  Naturapass
//
//  Created by manh on 12/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
@interface MurAgendaCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UIImageView *imgAvatar;
@property(nonatomic,strong) IBOutlet UIImageView *imgApply;
@property(nonatomic,strong) IBOutlet UILabel *lbTitle;
@property(nonatomic,strong) IBOutlet UILabel *lbAccess;
@property(nonatomic,strong) IBOutlet UILabel *lbTextDebut;
@property(nonatomic,strong) IBOutlet UILabel *lbTextFin;
@property(nonatomic,strong) IBOutlet UILabel *lbTextLocation;
@property(nonatomic,strong) IBOutlet UILabel *lbDebutValue;
@property(nonatomic,strong) IBOutlet UILabel *lbFinValue;
@property(nonatomic,strong) IBOutlet UILabel *lbLocationValue;
@property(nonatomic,strong) IBOutlet UILabel *lbDesctiption;
@property(nonatomic,strong) IBOutlet UIButton *btnAccess;
@property(nonatomic,strong) IBOutlet UIButton *btnLocation;
@property(nonatomic,strong) IBOutlet UIView *vContraint;
@property(nonatomic,strong) IBOutlet UIView *vHeader;
@property(nonatomic,strong) IBOutlet UIView *vContent;
@property(nonatomic,strong) IBOutlet UIView *vAvatar;
@property(nonatomic,strong) IBOutlet UIView *vTitle;
@property(nonatomic,strong) IBOutlet UIView *vLocation;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightLocation;

@property (nonatomic,assign) ISSCREEN expectTarget;
-(void)fnSetColorWithExpectTarget:(ISSCREEN)target withIsLiveHunt:(BOOL)liveHunt;
@end
