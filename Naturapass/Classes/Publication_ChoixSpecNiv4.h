//
//  Publication_ChoixSpecNiv4.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"

@interface Publication_ChoixSpecNiv4 : PublicationBaseVC
@property (nonatomic, strong) NSDictionary *myDic;

@property(nonatomic,strong) NSString * myName;

@end
