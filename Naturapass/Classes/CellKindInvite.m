//
//  CellKind2.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CellKindInvite.h"
#import "Define.h"
#import "CommonHelper.h"

@implementation CellKindInvite

- (void)awakeFromNib {
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];

    [[CommonHelper sharedInstance] setRoundedView:self.imageIcon toDiameter:self.imageIcon.frame.size.width/2];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label1 setTextColor:color];
}
@end
