//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step11.h"
#import "SubNavigation_PRE_General.h"
#import "ChassesMurVC.h"
#import "ChassesMurPassVC.h"
@interface ChassesCreate_Step11 ()
{}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@property(nonatomic,strong) IBOutlet UIImageView *img1;
@property(nonatomic,strong) IBOutlet UIImageView *img2;

@end

@implementation ChassesCreate_Step11
#pragma mark -setColor
-(void)fnColorTheme
{
    _label1.text =str(strMessage14);
    _label2.text =str(strMessage15);
    
    _label1.textColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);

    
    [self.btnSuivant setTitle:str(strTERMINER) forState:UIControlStateNormal];
    
    self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_TINY_BAR_COLOR);

}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.needChangeMessageAlert = YES;

    [self.subview removeFromSuperview];
    [self addSubNav:nil];
    [self fnColorTheme];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(IBAction)onNext:(id)sender
{
    if ([ChassesCreateOBJ sharedInstance].isMurPass && [ChassesCreateOBJ sharedInstance].isModifi) {
        NSArray * controllerArray = [[self navigationController] viewControllers];
        
        for (int i=0; i < controllerArray.count; i++) {
            if ([controllerArray[i] isKindOfClass: [ChassesMurPassVC class]]) {
                ChassesMurPassVC *vc =(ChassesMurPassVC*)controllerArray[i];
                    [vc getItemWithKind:MYCHASSE myid:[ChassesCreateOBJ sharedInstance].strID];
                    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES_PASS object: nil userInfo: nil];
                [self.navigationController popToViewController:vc animated:YES];
                
                return;
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES_PASS object: nil userInfo: nil];
        
        [self doRemoveObservation];

        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        NSArray * controllerArray = [[self navigationController] viewControllers];
        
        for (int i=0; i < controllerArray.count; i++) {
            if ([controllerArray[i] isKindOfClass: [ChassesMurVC class]]) {
                
                ChassesMurVC *vc =(ChassesMurVC*)controllerArray[i];
                if ([ChassesCreateOBJ sharedInstance].isModifi) {
                    [vc getItemWithKind:MYCHASSE myid:[ChassesCreateOBJ sharedInstance].strID];
                    
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES object: nil userInfo: nil];
                }
                [self.navigationController popToViewController:vc animated:YES];
                return;
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_MES_CHASSES object: nil userInfo: nil];
        [self doRemoveObservation];

        [self.navigationController popToRootViewControllerAnimated:YES];
    }

}

@end
