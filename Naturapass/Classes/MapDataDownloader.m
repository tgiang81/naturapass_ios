//
//  MapDataDownloader.m
//  TestMap
//
//  Created by Giang on 10/2/15.
//  Copyright © 2015 PHS. All rights reserved.
//

#import "MapDataDownloader.h"
#import "Define.h"
#import "Config.h"

#import "CommonHelper.h"

#import "FileHelper.h"
#import "DatabaseManager.h"
#import "PublicationOBJ.h"
#import "AnimalsEntity.h"

static MapDataDownloader *sharedInstance = nil;

//#define LINK @"https://naturapass.e-conception.fr/app_dev.php/api/v2/publication/map?swLat=21.01646936066789&swLng=105.8186555607444&neLat=21.03489605220065&neLng=105.8323884703893&reset=1&sharing=1"

static bool isFinish;

@implementation MapDataDownloader

+ (MapDataDownloader *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(id)init
{
    if (self = [super init]) {
        self.operationQueue =  [NSOperationQueue new];
        
        isFinish = YES;
        
        return self;
    }
    
    return nil;
}

-(void) createAnimationList:(NSDictionary*) dic
{
    AnimalsEntity *obj = [AnimalsEntity MR_createEntity];
    obj.name = dic[@"name"];
    obj.myid =  [NSNumber numberWithInt:  [dic[@"id"] intValue] ];
    
    //    NSArray *mama = [AnimalsEntity MR_findAll];
}

//cache TREE. For Naturapass.

-(void) resetParamTree
{
    isGettingTree = NO;
}

//update tree
-(void) fnGetObservationTree
{
    if (!isGettingTree) {
        
        [self performSelector:@selector(resetParamTree) withObject:nil afterDelay:60];
        
        isGettingTree = YES;
        WebServiceAPI *serviceObjTree = [WebServiceAPI new];
        [serviceObjTree getToCacheSystem];
        
        serviceObjTree.onComplete = ^(NSDictionary*response, int errCode){
            isGettingTree = NO;
//            NSString *strPath = [FileHelper pathForApplicationDataFile: @"tree.json"];
//            [response writeToFile:strPath atomically:YES];

            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            if (response) {
                [self doUpdateTree: response];
            }
            
        };
        
    }
    
}

-(void) doUpdateTree:(NSDictionary*) response{
    
    [[PublicationOBJ sharedInstance]  doRefreshTree:response];
    
    //Animals -> vao day:
    NSManagedObjectContext *privateContext = [NSManagedObjectContext MR_defaultContext];
    
    [privateContext performBlock:^{
        
        //Delete all old animals
        NSArray*arrAnimal = [AnimalsEntity MR_findAll];
        
        for (AnimalsEntity*obj in arrAnimal) {
            [obj MR_deleteEntity];
        }
        
        //update all new animals.
        
        for (NSDictionary*dic in response[@"animals"])
        {
            [self performSelector:@selector(createAnimationList:) withObject:dic afterDelay:0.01];
            
        }
    }];
}
-(void) doUpdateTableDBAgenda
{
    
    //publication shape group hunt distributor address favorite
    NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT version FROM tb_db_version ORDER BY version DESC LIMIT 1"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        NSString *strMaxVersion = @"0";
        
        while ([set_querry next])
        {
            strMaxVersion = [set_querry stringForColumn:@"version"];
        }
        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        
        [param setObject:@{
                           @"version":strMaxVersion
                           }
                  forKey:@"version"];
        
        [param setObject:@"true" forKey:@"agenda"];
        
        serviceObj = [WebServiceAPI new];
        
        [serviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];
        
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            isFinish = YES;
            
            ASLog(@"%@ \n ", param);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                NSArray *arrResult = response[@"versions"];
                
                if (arrResult.count > 0 )
                {
                    //has change. sort arr from accending
                    
                    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
                    NSArray * arrVersions = [arrResult sortedArrayUsingDescriptors:@[brandDescriptor]];
                    
                    
                    //Each version
                    for (int i=0; i < arrVersions.count; i++)
                    {
                        
                        NSDictionary *dicT1 = arrVersions[i];
                        NSArray *arrSqls = dicT1[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            //                                ASLog(@"%@", tmpStr);
                            
                            BOOL isOK =   [db  executeUpdate:tmpStr];

                        }
                        //add version number to tble version.
                        NSString *strInsertTblVersion = [ NSString stringWithFormat: @"INSERT INTO `tb_db_version`(`version`) VALUES ('%@');", dicT1[@"version"] ] ;
                        [db  executeUpdate:strInsertTblVersion];
                        
                    }
                    
                }
                
                //finish table version...move to table carte.shape
                if ([response[@"sqlite"] isKindOfClass: [NSDictionary class]])
                {
                    
                    NSMutableArray *mutArr =  [NSMutableArray new];
                    
                    NSArray *arrSqls66 = response[@"sqlite"][@"agendas"] ;
                    if ([arrSqls66 isKindOfClass: [NSArray class]]) {
                        //Clear Agendas data
                        NSString *strDeleteAgenda = [ NSString stringWithFormat: @"DELETE FROM `tb_agenda` WHERE `c_user_id` = '%@';", sender_id ] ;
                        //add version number to tble version.
                        
                        BOOL isOK =   [db  executeUpdate:strDeleteAgenda];

                    }
                    if (arrSqls66.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls66];
                        
                    }
                    
                    if (mutArr.count > 0)
                    {
                        
                        for (NSString*strQuerry in mutArr) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            BOOL isOK =   [db  executeUpdate:tmpStr];
                        }
                        
                        
                    }
                    
                }
                /**/
                
            }
            
        };
        
    }];
}

-(void) doUpdateTableDBVersion
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT version FROM tb_db_version ORDER BY version DESC LIMIT 1"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        NSString *strMaxVersion = @"0";
        
        while ([set_querry next])
        {
            strMaxVersion = [set_querry stringForColumn:@"version"];
        }
        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        
        [param setObject:@{
                           @"version":strMaxVersion
                           }
                  forKey:@"version"];
        
        WebServiceAPI *serviceObjX = [WebServiceAPI new];
        
        [serviceObjX PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];
        
        
        serviceObjX.onComplete = ^(NSDictionary*response, int errCode){
            
            ASLog(@"[GIANG_1] %@ \n ", response);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                NSArray *arrResult = response[@"versions"];
                
                if (arrResult.count > 0 )
                {
                    //has change. sort arr from accending
                    
                    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
                    NSArray * arrVersions = [arrResult sortedArrayUsingDescriptors:@[brandDescriptor]];
                    
                    
                    //Each version
                    for (int i=0; i < arrVersions.count; i++)
                    {
                        
                        NSDictionary *dicT1 = arrVersions[i];
                        NSArray *arrSqls = dicT1[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            //                                ASLog(@"%@", tmpStr);
                            
                            [db  executeUpdate:tmpStr];
                            
                        }
                        //add version number to tble version.
                        NSString *strInsertTblVersion = [ NSString stringWithFormat: @"INSERT INTO `tb_db_version`(`version`) VALUES ('%@');", dicT1[@"version"] ] ;
                        [db  executeUpdate:strInsertTblVersion];
                        
                    }
                    
                }
                
            }
            
        };
        
    }];
}

-(void) doUpdateDogsWeapon //Dog weapon
{
    WebServiceAPI *localWebserviceObj = [WebServiceAPI new];
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        
        [param setObject:@{
                           @"reload":@"1"
                           }
                  forKey:@"breed"];
        
        [param setObject:@{
                           @"reload":@"1"
                           }
                  forKey:@"type"];
        
        [param setObject:@{
                           @"reload":@"1"
                           }
                  forKey:@"brand"];
        
        
        [param setObject:@{
                           @"reload":@"1"
                           }
                  forKey:@"calibre"];
        
        
        [localWebserviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];
        
        
        localWebserviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            
            ASLog(@"%@ \n ", param);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                //finish table version...move to table carte.shape
                if ([response[@"sqlite"] isKindOfClass: [NSDictionary class]])
                {
                    
                    NSMutableArray *mutArr =  [NSMutableArray new];
                    
                    
                    NSArray *arrSqls8 = response[@"sqlite"][@"breeds"] ;
                    if (arrSqls8.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls8];
                        
                    }
                    
                    NSArray *arrSqls9 = response[@"sqlite"][@"types"] ;
                    if (arrSqls9.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls9];
                        
                    }
                    
                    NSArray *arrSqls10 = response[@"sqlite"][@"brands"] ;
                    if (arrSqls10.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls10];
                        
                    }
                    NSArray *arrSqls11 = response[@"sqlite"][@"calibres"] ;
                    if (arrSqls11.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls11];
                        
                    }
                    
                    if (mutArr.count > 0)
                    {
                        
                        for (NSString*strQuerry in mutArr) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            BOOL isOK =   [db  executeUpdate:tmpStr];

                        }
                        
                        
                    }
                    
                    //? update Favorite View.
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_OBJ_NOTIFICATION object:nil];
                    
                }
                /**/
                
            }
            
        };
        
    }];
}


-(void) doUpdateFavorites //Fav pub+address
{
    WebServiceAPI *localWebserviceObj = [WebServiceAPI new];
//    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase* db) {
        
        //PUBLICATION
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strCUPDATEDFavPublication =@"0";
        
        //favorite publication
        FMResultSet *set_querry6 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_favorite WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1", sender_id ] ];
        
        while ([set_querry6 next])
        {
            strCUPDATEDFavPublication = [set_querry6 stringForColumn:@"c_updated"] ? [set_querry6 stringForColumn:@"c_updated"] : @"0";
            
        }
        
        //4
        NSString *strCIDFavPublication = @"";
        FMResultSet *set_querry10 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_id FROM tb_favorite  WHERE c_user_id=%@ AND c_updated=0", sender_id ] ];
        
        while ([set_querry10 next])
        {
            strCIDFavPublication = [set_querry10 stringForColumn:@"c_id"] ? [set_querry10 stringForColumn:@"c_id"] : @"0";
            
        }
        
        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        [param setObject:@{
                           @"reload":@"1"
                           }
                  forKey:@"address"];
        
        
        [param setObject:@{
                           @"ids":strCIDFavPublication,
                           @"updated":strCUPDATEDFavPublication
                           }
                  forKey:@"favorite"];
        
        [localWebserviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];
        
        
        localWebserviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            
            ASLog(@"%@ \n ", param);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                //finish table version...move to table carte.shape
                if ([response[@"sqlite"] isKindOfClass: [NSDictionary class]])
                {
                    
                    NSMutableArray *mutArr =  [NSMutableArray new];
                    
                    NSArray *arrSqls4 = response[@"sqlite"][@"favorites"] ;
                    if (arrSqls4.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls4];
                        
                    }
                    
                    NSArray *arrSqls5 = response[@"sqlite"][@"addresss"] ;
                    if (arrSqls5.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls5];
                        
                    }
                    
                    if (mutArr.count > 0)
                    {
                        
                        for (NSString*strQuerry in mutArr) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                              [db  executeUpdate:tmpStr];
                        }
                        
                    }
                    
                    //? update Favorite View.
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_OBJ_NOTIFICATION object:nil];
                    
                }
                /**/
                
            }
            
        };
        
    }];
}


-(void) doOperator:(NSArray*)myArrType
{
    __block NSArray * localArr = myArrType;
    __weak MapDataDownloader *myWeak = self;
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        //
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT version FROM tb_db_version ORDER BY version DESC LIMIT 1"];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        NSString *strMaxVersion = @"0";
        
        while ([set_querry next])
        {
            strMaxVersion = [set_querry stringForColumn:@"version"];
        }
        
        //setting parameters
        
        //PUBLICATION
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strCUPDATEDPublication = @"0";
        
        NSString *strCUPDATEDShape =@"0";
        
        NSString *strCUPDATEDDistribution = @"0";
        
        NSString *strCUPDATEDGroup =@"0";
        
        NSString *strCUPDATEDHunt =@"0";
        
        NSString *strCUPDATEDAgenda =@"0";
        
        NSString *strCUPDATEDFavPublication =@"0";
        
        //differ to me -> to avoid lack other publications, because we can make it by our self.
        
        FMResultSet *set_querry1 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_carte WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1",sender_id ] ];
        
        while ([set_querry1 next])
        {
            strCUPDATEDPublication = [set_querry1 stringForColumn:@"c_updated"] ? [set_querry1 stringForColumn:@"c_updated"] : @"0";
            //has c_updated
        }
        
        //DISTRIBUTION
        FMResultSet *set_querry2 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_distributor ORDER BY c_updated DESC LIMIT 1" ] ];
        while ([set_querry2 next])
        {
            strCUPDATEDDistribution = [set_querry2 stringForColumn:@"c_updated"] ? [set_querry2 stringForColumn:@"c_updated"] : @"0";
        }
        
        FMResultSet *set_querry3 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_shape WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1",sender_id ] ];
        
        while ([set_querry3 next])
        {
            strCUPDATEDShape = [set_querry3 stringForColumn:@"c_updated"] ? [set_querry3 stringForColumn:@"c_updated"] : @"0";
        }
        
        //GROUP
        FMResultSet *set_querry4 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_group WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1", sender_id ] ];
        
        while ([set_querry4 next])
        {
            strCUPDATEDGroup = [set_querry4 stringForColumn:@"c_updated"] ? [set_querry4 stringForColumn:@"c_updated"] : @"0";
            
        }
        
        //HUNT
        FMResultSet *set_querry5 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_hunt WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1", sender_id ] ];
        
        while ([set_querry5 next])
        {
            strCUPDATEDHunt = [set_querry5 stringForColumn:@"c_updated"] ? [set_querry5 stringForColumn:@"c_updated"] : @"0";
            
        }
        
        //AGENDA
        FMResultSet *set_querry55 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_agenda WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1", sender_id ] ];
        
        while ([set_querry55 next])
        {
            strCUPDATEDAgenda = [set_querry55 stringForColumn:@"c_updated"] ? [set_querry55 stringForColumn:@"c_updated"] : @"0";
            
        }
        
        
        //favorite publication
        FMResultSet *set_querry6 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_updated from tb_favorite WHERE c_user_id=%@ ORDER BY c_updated DESC LIMIT 1", sender_id ] ];
        
        while ([set_querry6 next])
        {
            strCUPDATEDFavPublication = [set_querry6 stringForColumn:@"c_updated"] ? [set_querry6 stringForColumn:@"c_updated"] : @"0";
            
        }
        
        //get IDS with update=0
        
        NSString *strCID = @"";
        FMResultSet *set_querry7 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_id FROM tb_carte  WHERE  c_user_id=%@ AND c_updated=0", sender_id ] ];
        
        while ([set_querry7 next])
        {
            strCID = [set_querry7 stringForColumn:@"c_id"] ? [set_querry7 stringForColumn:@"c_id"] : @"0";
            
        }
        //2
        
        NSString *strCIDGroup = @"";
        FMResultSet *set_querry8 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_id FROM tb_group  WHERE c_user_id=%@ AND c_updated=0", sender_id ] ];
        
        while ([set_querry8 next])
        {
            strCIDGroup = [set_querry8 stringForColumn:@"c_id"] ? [set_querry8 stringForColumn:@"c_id"] : @"0";
            
        }
        
        //3
        NSString *strCIDHunt = @"";
        FMResultSet *set_querry9 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_id FROM tb_hunt  WHERE c_user_id=%@ AND c_updated=0", sender_id ] ];
        
        while ([set_querry9 next])
        {
            strCIDHunt = [set_querry9 stringForColumn:@"c_id"] ? [set_querry9 stringForColumn:@"c_id"] : @"0";
            
        }
        
        //4
        NSString *strCIDFavPublication = @"";
        FMResultSet *set_querry10 = [db  executeQuery:[NSString stringWithFormat:@"SELECT c_id FROM tb_favorite  WHERE c_user_id=%@ AND c_updated=0", sender_id ] ];
        
        while ([set_querry10 next])
        {
            strCIDFavPublication = [set_querry10 stringForColumn:@"c_id"] ? [set_querry10 stringForColumn:@"c_id"] : @"0";
            
        }
        
        //publication shape group hunt distributor address favorite
        NSMutableDictionary *param = [NSMutableDictionary new];
        
        NSString *pushToken = [[NSUserDefaults standardUserDefaults] objectForKey: @"DEVICETOKEN"];
        
        if (!pushToken || ![pushToken length])
        {
            ASLog(@"nil stoken");
            
            pushToken = @"1e7da48ebacb6f1e8b8e73937ffc91520029efc6d1982f499907faf743c7381d";
        }
        
        [param setObject: pushToken
                  forKey:@"identifier"];
        
        
        [param setObject:@{
                           @"version":strMaxVersion
                           }
                  forKey:@"version"];
        
        //7
        if (myArrType == nil) {
            //ALL
            localArr = @[kSQL_publication, kSQL_shape, kSQL_group, kSQL_hunt, kSQL_agenda, kSQL_distributor, kSQL_address, kSQL_favorite,kSQL_breed,kSQL_type,kSQL_brand,kSQL_calibre];
        }
        
        for (NSString*strIn in myArrType) {
            
            if ([strIn isEqualToString:kSQL_publication]) {
                
                [param setObject:@{
                                   @"ids":strCID,
                                   @"updated":strCUPDATEDPublication,
                                   @"limit": [NSString stringWithFormat:@"%d",MAX_COUNT_RECORD]
                                   }
                          forKey:@"publication"];
                
            }else if ([strIn isEqualToString:kSQL_shape]) {
                
                [param setObject:@{
                                   @"updated":strCUPDATEDShape
                                   }
                          forKey:@"shape"];
                
            }else if ([strIn isEqualToString:kSQL_group]) {
                
                [param setObject:@{
                                   @"ids":strCIDGroup,
                                   @"updated":strCUPDATEDGroup
                                   }
                          forKey:@"group"];
                
            }else if ([strIn isEqualToString:kSQL_hunt]) {
                
                [param setObject:@{
                                   @"ids":strCIDHunt,
                                   @"updated":strCUPDATEDHunt
                                   }
                          forKey:@"hunt"];
                
            }
            else if ([strIn isEqualToString:kSQL_agenda]) {
                
                [param setObject:@"true"
                          forKey:@"agenda"];
            }
            else if ([strIn isEqualToString:kSQL_distributor]) {
                
                [param setObject:@{
                                   @"updated":strCUPDATEDDistribution,
                                   @"limit": [NSString stringWithFormat:@"%d",MAX_COUNT_RECORD]
                                   
                                   }
                          forKey:@"distributor"];
                
            }else if ([strIn isEqualToString:kSQL_address]) {
                
                [param setObject:@{
                                   @"reload":@"1"
                                   }
                          forKey:@"address"];
                
            }else if ([strIn isEqualToString:kSQL_favorite]) {
                
                [param setObject:@{
                                   @"ids":strCIDFavPublication,
                                   @"updated":strCUPDATEDFavPublication
                                   }
                          forKey:@"favorite"];
                
            }else if ([strIn isEqualToString:kSQL_breed]) {
                
                [param setObject:@{
                                   @"reload":@"1"
                                   }
                          forKey:@"breed"];
                
            }else if ([strIn isEqualToString:kSQL_type]) {
                
                [param setObject:@{
                                   @"reload":@"1"
                                   }
                          forKey:@"type"];
                
            }else if ([strIn isEqualToString:kSQL_brand]) {
                
                [param setObject:@{
                                   @"reload":@"1"
                                   }
                          forKey:@"brand"];
                
            }else if ([strIn isEqualToString:kSQL_calibre]) {
                
                [param setObject:@{
                                   @"reload":@"1"
                                   }
                          forKey:@"calibre"];
            }
        }
        
        serviceObj = [WebServiceAPI new];
        
        //
       BOOL bIsUsingDefaultDB = [[NSUserDefaults standardUserDefaults] boolForKey:@"isUsedDefaultDB" ];
         

        if (bIsUsingDefaultDB == NO) {
            //using new default db...server marked all these publications -> will not response it anymore.
            [serviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_USING_DEFAULT_DB:param];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isUsedDefaultDB"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
        }else{
            //hmm old user...old db method using
            [serviceObj PUT_SQLITE_ALL_POINTS_IN_MAP_WITHPARAM:param];
        }
        
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            isFinish = YES;
            
            ASLog(@"%@ \n ", param);
            
            if ([response isKindOfClass: [NSDictionary class]] )
            {
                /**/
                
                NSArray *arrResult = response[@"versions"];
                
                if (arrResult.count > 0 )
                {
                    //has change. sort arr from accending
                    
                    NSSortDescriptor * brandDescriptor = [[NSSortDescriptor alloc] initWithKey:@"version" ascending:YES];
                    NSArray * arrVersions = [arrResult sortedArrayUsingDescriptors:@[brandDescriptor]];
                    
                    
                    //Each version
                    for (int i=0; i < arrVersions.count; i++)
                    {
                        
                        NSDictionary *dicT1 = arrVersions[i];
                        NSArray *arrSqls = dicT1[@"sqlite"] ;
                        
                        for (NSString*strQuerry in arrSqls) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            //                                ASLog(@"%@", tmpStr);
                            
                             [db  executeUpdate:tmpStr];

                        }
                        //add version number to tble version.
                        NSString *strInsertTblVersion = [ NSString stringWithFormat: @"INSERT INTO `tb_db_version`(`version`) VALUES ('%@');", dicT1[@"version"] ] ;
                        [db  executeUpdate:strInsertTblVersion];
                        
                        
                    }
                    
                }
                
                //finish table version...move to table carte.shape
                if ([response[@"sqlite"] isKindOfClass: [NSDictionary class]])
                {
                    
                    NSMutableArray *mutArr =  [NSMutableArray new];
                    
                    NSArray *arrSqls = response[@"sqlite"][@"distributors"] ;
                    
                    
                    if (arrSqls.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls];
                    }
                    
                    NSArray *arrSqls2 = response[@"sqlite"][@"shapes"] ;
                    if (arrSqls2.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls2];
                    }
                    
                    NSArray *arrSqls3 = response[@"sqlite"][@"publications"] ;
                    if (arrSqls3.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls3];
                        
                    }
                    
                    NSArray *arrSqls4 = response[@"sqlite"][@"favorites"] ;
                    if (arrSqls4.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls4];
                        
                    }
                    
                    NSArray *arrSqls5 = response[@"sqlite"][@"addresss"] ;
                    if (arrSqls5.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls5];
                        
                    }
                    
                    NSArray *arrSqls6 = response[@"sqlite"][@"hunts"] ;
                    if (arrSqls6.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls6];
                        
                    }
                    
                    NSArray *arrSqls66 = response[@"sqlite"][@"agendas"] ;
                    if ([arrSqls66 isKindOfClass: [NSArray class]]) {
                        //Clear Agendas data
                        NSString *strDeleteAgenda = [ NSString stringWithFormat: @"DELETE FROM `tb_agenda` WHERE `c_user_id` = '%@';", sender_id ] ;
                        //add version number to tble version.
                        
                        [db  executeUpdate:strDeleteAgenda];
                        
                    }
                    if (arrSqls66.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls66];
                        
                    }
                    
                    NSArray *arrSqls7 = response[@"sqlite"][@"groups"] ;
                    if (arrSqls7.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls7];
                        
                    }
                    
                    NSArray *arrSqls8 = response[@"sqlite"][@"breeds"] ;
                    if (arrSqls8.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls8];
                        
                    }
                    
                    NSArray *arrSqls9 = response[@"sqlite"][@"types"] ;
                    if (arrSqls9.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls9];
                        
                    }
                    
                    NSArray *arrSqls10 = response[@"sqlite"][@"brands"] ;
                    if (arrSqls10.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls10];
                        
                    }
                    NSArray *arrSqls11 = response[@"sqlite"][@"calibres"] ;
                    if (arrSqls11.count > 0) {
                        [mutArr addObjectsFromArray:arrSqls11];
                        
                    }
                    //invoke
                    //Count resunt record...if < batch MAX_COUNT_RECORD
                    
                    
                    
                    if (mutArr.count > 0)
                    {
                        
                        for (NSString*strQuerry in mutArr) {
                            //correct API
                            NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                            
                            
                            //                                ASLog(@"%@", tmpStr);
                            
                            [db  executeUpdate:tmpStr];

                        }
                    }
                    
                    //continue or stop here
                    
                    //pass publication first
                    if ([response[@"sqlite"][@"publications"] isKindOfClass: [NSArray class]]) {
                        
                        if ([response[@"sqlite"][@"count_publications"] intValue] >= MAX_COUNT_RECORD)
                        {
                            isFinish = NO;
                            ASLog(@"=====>>> COUNT....");
                            //continue request...recursive.
                            [myWeak doOperator:myArrType];
                            
                        }else{
                            //last batch...stop request next shape if any.
                            isFinish = YES;
                            if (myWeak.CallBackShape) {
                                
                                myWeak.CallBackShape();
                            }
                            
                        }
                    }
                    
                    if ([response[@"sqlite"][@"distributors"] isKindOfClass: [NSArray class]])
                    {
                        if ([response[@"sqlite"][@"count_distributors"] intValue] >= MAX_COUNT_RECORD)
                        {
                            isFinish = NO;
                            ASLog(@"=====>>> COUNT_SHAPE....");
                            //continue request...recursive.
                            [myWeak doOperator:myArrType];
                            
                        }else{
                            isFinish = YES;
                            
                        }
                    }
                    
                    
                    if ([response[@"sqlite"][@"shapes"] isKindOfClass: [NSArray class]]) {
                        isFinish = YES;
                        if (myWeak.CallBackEditShape) {
                            
                            myWeak.CallBackEditShape();
                        }
                    }
                    
                    //general
                    if (myWeak.CallBackPublication) {
                        
                        myWeak.CallBackPublication();
                    }
                    
                    
                    //                    if ([response[@"sqlite"][@"publications"] isKindOfClass: [NSArray class]]) {
                    //                        //will del
                    //                        if (myWeak.CallBackShape) {
                    //
                    //                            myWeak.CallBackShape();
                    //                        }
                    //                    }
                    
                    //My post...
                    
                    if (isFinish) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:HIDE_INDICATOR_OBJ_NOTIFICATION object:nil];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_OBJ_NOTIFICATION object:nil];
                    
                    
                    
                }
                /**/
                
            }
            
        };
        
    }];
}

-(void) resetParam
{
    isFinish = YES;
    
    [self.operationQueue cancelAllOperations];
    
    [serviceObj cancelTask];
}

-(void)fnGetSqliteForAllPointsInMap :(NSArray*) myArrType
{
    if (isFinish) {
        
        isFinish = NO;
        [self performSelector:@selector(resetParam) withObject:nil afterDelay:180];
        
        NSInvocationOperation *operationOne = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(doOperator:) object:myArrType];
        [self.operationQueue addOperation:operationOne];
    }
}
@end
