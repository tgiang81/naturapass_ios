//
//  MurView.m
//  Naturapass
//
//  Created by Manh on 6/16/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MurView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CommentDetailVC.h"
#import "DatabaseManager.h"
#import "FriendInfoVC.h"
#import "MapLocationVC.h"
#import "MurEditPublicationVC.h"
#import "MurPeopleLikes.h"
#import "AllerMUR.h"
#import "AllerMURGPS.h"
#import "MapGlobalVC.h"
#import "MurAgendaCell.h"
#import "GroupEnterMurVC.h"
#import "ChassesParticipeVC.h"
#import "AlertVC.h"
#import "SignalerTerminez.h"

static NSString *MediaCellIDMur = @"MediaCell";
static NSString *MurAgendaID = @"MurAgendaID";
static NSString *MurGroupID = @"MurGroupID";

@implementation MurView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0] ;
    if (self) {
        _expectTarget = expectTarget;
        _parentVC = vc;
        [self instance];
    }
    return self;
}
-(void)fnSetDataPublication:(NSArray*)arrPublication
{
    publicationArray = [arrPublication mutableCopy];
    [self.tableControl reloadData];
}
-(void)fnPublicationNew:(NSArray*)arrPublication
{
    publicationArray = [arrPublication mutableCopy];
    [self.tableControl reloadData];
}
-(void)fnAddMorePublication:(NSArray*)arrPublication
{
    // build the index paths for insertion
    // since you're adding to the end of datasource, the new rows will start at count
    NSMutableArray *indexPaths = [NSMutableArray array];
    NSInteger currentCount = publicationArray.count;
    for (int i = 0; i < arrPublication.count; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:currentCount+i inSection:0]];
    }
    
    // do the insertion
    [publicationArray addObjectsFromArray:arrPublication];;
    // tell the table view to update (at all of the inserted index paths)
    [self.tableControl beginUpdates];
    [self.tableControl insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationNone];
    [self.tableControl endUpdates];
}
-(void)instance
{
    [Flurry logEvent:@"MurFragment" timed:YES];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"MediaCell" bundle:nil] forCellReuseIdentifier:MediaCellIDMur];
    [self.tableControl registerNib:[UINib nibWithNibName:@"MurAgendaCell" bundle:nil] forCellReuseIdentifier:MurAgendaID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"MurAgendaCell" bundle:nil] forCellReuseIdentifier:MurGroupID];

    self.tableControl.estimatedRowHeight = 160;
    
//    self.tableControl.rowHeight = UITableViewAutomaticDimension;

    self.backgroundColor = UIColor.clearColor;
    
    self.tableControl.backgroundColor =   UIColor.clearColor;//UIColorFromRGB(0x525964);
    shareDetail = [[SettingActionView alloc] initSettingActionView];
    widthImage = [UIScreen mainScreen].bounds.size.width * 286/304;
    publicationArray = [NSMutableArray new];
    //Refresh/loadmore
    [self initRefreshControl];
    
}

-(void)addContraintSupview:(UIView*)viewSuper
{
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    view.frame = viewSuper.frame;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
}
-(void)setCallback:(MurViewCallback)callback
{
    _callback = callback;
}
//overwrite
- (void)insertRowAtTop {
    //request first page
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        return;
    }
    if (_callback) {
        _callback(VIEW_ACTION_REFRESH_TOP, publicationArray);
    }
}
- (void)insertRowAtBottom {
    
    if ( ![COMMON isReachable] ) {
        
        [self stopRefreshControl];
        return;
    }
    if (_callback) {
        _callback(VIEW_ACTION_REFRESH_BOTTOM,publicationArray);
    }
}

#pragma mark - PHOTO BROWSER

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //NSLog(@"Did finish modal presentation");
    [_parentVC dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark - TABLEVIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView==self.tableControl)
    {
        if ([publicationArray count]>0)
        {
            return [publicationArray count];
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

-(void) showPhotoList:(NSDictionary*)dic{
    
    //Play Video...
    
    
    NSString *strVideo= [dic[@"media"][@"type"] stringValue];
    
//    if ([strVideo isEqualToString:@"101"] )
    {
        NSMutableArray *photos = [NSMutableArray new];
        
        // url
        NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[dic valueForKey:@"media"] valueForKey:@"path"]];
        strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".jpeg"];
        strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4" withString:@".jpeg"];
        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".jpeg"];
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        NSURL *urlImage = [NSURL URLWithString:strImage];
        
        // timer
        NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
        NSDateFormatter *outputFormatter = [[ASSharedTimeFormatter sharedFormatter] outputFormatter];
        [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
        [ASSharedTimeFormatter checkFormatString: @"dd-MM-yyyy HH:mm" forFormatter: outputFormatter];
        NSString *relativeTime = [dic valueForKey:@"created"];
        NSDate * inputDate = [ inputFormatter dateFromString:relativeTime ];
        NSString * outputString = [ outputFormatter stringFromDate:inputDate ];
        NSString *strDateLocation=[NSString stringWithFormat:@"%@",outputString];
        
        IDMPhoto *aphoto = nil;
        if ([COMMON isReachable]) {
            aphoto = [IDMPhoto photoWithURL:urlImage];
        }
        else
        {
            if (dic[@"kFileData"]) {
                NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:dic[@"kFileData"]];
                aphoto = [IDMPhoto photoWithURL:[NSURL fileURLWithPath:photoFile ]];
            }else{
                aphoto = [IDMPhoto photoWithURL:urlImage];
            }
        }
        
        
        
        
        aphoto.caption = strDateLocation;
        [photos addObject:aphoto];
        // Create and setup browser
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
        browser.delegate = self;
        
        // Show
        [_parentVC presentViewController:browser animated:YES completion:nil];

    }

}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[MediaCell class]])
    {
        MediaCell *cell = (MediaCell *)cellTmp;
        
        NSDictionary *dic = [publicationArray objectAtIndex:indexPath.row] ;
        cell.isMurView = TRUE;
        [cell setThemeWithFromScreen:self.isLiveHunt?ISLIVEMAP:self.expectTarget];
        
        if ([publicationArray objectAtIndex:0] != [NSNull null])
        {
            cell.btnTitle.tag=indexPath.row;
            [cell.btnTitle addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnProfile.tag=indexPath.row;
            [cell.btnProfile addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *strImage=@"";
            if (dic[@"owner"][@"profilepicture"] != nil) {
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
            }else{
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            }
            NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
            
            [cell.imageProfile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
            cell.contraintHeightImageContent.constant = 0;
            if   ( (dic[@"media"] != nil) &&   [dic[@"media"] isKindOfClass: [ NSDictionary class]] )
            {
                if ([dic[@"media"] isKindOfClass:[NSDictionary class]]) {
                    NSString *strVideo= [dic[@"media"][@"type"] stringValue];
                    
                    //video
                    if ([strVideo isEqualToString:@"101"] ){
                        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[[publicationArray objectAtIndex:indexPath.row]
                                                                                     valueForKey:@"media"]
                                                                                    valueForKey:@"path"]];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4.mp4" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".mp4.jpeg"];
                        
                        cell.imgIcon.hidden = NO;
                        
                    } else {
                        //image
                        cell.imgIcon.hidden = YES;
                        
                        strImage=[NSString stringWithFormat:@"%@%@.jpeg",IMAGE_ROOT_API,dic[@"media"][@"path"]];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".jpeg"];
                    }
                    
                    url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
                    
//                    __weak MediaCell *weakCell = cell;
//                    __weak typeof(self) weakSelf = self;
                    cell.imageContent.tag = indexPath.row;
//                    cell.imageContent.image = nil;
                    cell.imageContent.url= [COMMON characterTrimming:strImage];
                    float heightImg = [self fnResize:CGSizeMake([dic[@"media"][@"size"][@"width"] floatValue], [dic[@"media"][@"size"][@"height"] floatValue]) width:widthImage].height;
                    if (heightImg > widthImage*9/16) {
                        heightImg = widthImage*9/16;
                    }
                    cell.contraintHeightImageContent.constant = heightImg;
                    [cell.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        /*
                        //check if in visible cell => update...
                        MediaCell *checkCell = (MediaCell*)[self.tableControl cellForRowAtIndexPath:indexPath];
                        //ok reload
                        
                        if (image) {
                            UIImage *tempImg = [self fnResizeFixWidth:image :widthImage];
                            
                            weakCell.imageContent.image = tempImg;
                        }
                        if (checkCell && image && [weakCell.imageContent.url isEqualToString:imageURL.absoluteString]) {
                            [weakSelf.tableControl reloadData];
                        }
                         */

                    }];
                    
                    
                    
                    [cell.btnTitle addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    
                    [cell.imageContent setOncallback:^(NSInteger index)
                     {
                         int dx = (int)index - COMMENTBUTTONTAG;
                         
                         NSDictionary *dic = [publicationArray objectAtIndex:dx];
                         [self displayCommentVC:dic];
                         
                     }];
                    
                }
            }
            else
            {
                //text
                cell.imgIcon.hidden = YES;
                cell.imageContent.image = nil;
                [cell.imageContent fnRemoveClick];
            }
            
            [cell.location addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.btnAller addTarget:self action:@selector(fnAller:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnAller.tag = indexPath.row + ALLERBUTTONTAG;
            cell.location.tag = indexPath.row + LOCATIONBUTTONTAG;
            //Assign Values in Cell
            [cell assignValues:dic];
            
            cell.btnObs.tag = indexPath.row + 2001;
            [cell setCbClickObs:^(UIButton*theBtn)
             {
                 NSInteger index = (int)theBtn.tag - 2001;
                 NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
                 [self displayCommentVC:dicTmp];
                 /*
                 BOOL readmore = ![dicTmp[@"readobs"] boolValue];
                 [dicTmp setObject:@(readmore) forKey:@"readobs"];
                 [publicationArray replaceObjectAtIndex:index withObject:dicTmp];
                 
                 [self.tableControl beginUpdates];
                 NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                 [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                 [self.tableControl endUpdates];
                  */
             }];
            NSDictionary *attrs = @{ NSForegroundColorAttributeName : UIColorFromRGB(MUR_MAIN_BAR_COLOR),NSFontAttributeName: cell.myContent.font};
            cell.myContent.truncationTokenStringAttributes = attrs;
            cell.myContent.truncationTokenString = @"...Voir plus";
            UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture:)];
            
            readMoreGesture.numberOfTapsRequired = 1;
            [cell.myContent addGestureRecognizer:readMoreGesture];
            cell.myContent.tag = indexPath.row + 1001;
            cell.myContent.lineBreakMode =  NSLineBreakByTruncatingTail;
            cell.myContent.numberOfLines = 3;
//            if ([dic[@"readmore"] boolValue]) {
//                cell.myContent.lineBreakMode =  NSLineBreakByWordWrapping;
//                cell.myContent.numberOfLines = 0;
//            }
//            else
//            {
//                cell.myContent.lineBreakMode =  NSLineBreakByTruncatingTail;
//                cell.myContent.numberOfLines = 3;
//
//            }
            [cell setCbClickText:^(UIButton*theBtn)
             {
                 int index = (int)theBtn.tag - COMMENTBUTTONTAG;
                 NSDictionary *dic = [publicationArray objectAtIndex:index];
                 [self displayCommentVC:dic];
                 
             }];
        }
        
        //OpenIN file
        
        [cell.btnOpenFile addTarget:self action:@selector(openFileViewAction:) forControlEvents:UIControlEventTouchUpInside];
        
        
        [cell.btnTextComment addTarget:self action:@selector(commentViewAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnComment addTarget:self action:@selector(commentViewAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnLike addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //Is me -> edit + Del
        //friend -> Signal
        
//        if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
//            [cell createMenuList:@[str(strModifier),str(strSupprimer)] ];
//            
//            
//        }else{
//            [cell createMenuList:@[str(strSignaler),str(strMenu_item_Blacklister)] ];
//            
//        }
        
        [cell.btnSetting addTarget:self action:@selector(settingAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSetting.tag =indexPath.row+100;
        
        [cell.btnLikesList addTarget:self action:@selector(displayLikesList:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnLikesList.tag =indexPath.row+110;
        
        [cell.btnShareDetail addTarget:self action:@selector(shareDetailAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnShareDetail.tag =indexPath.row+210;
        
        cell.btnLike.tag = indexPath.row + LIKETAG;
        cell.btnComment.tag = indexPath.row + COMMENTBUTTONTAG;
        cell.btnTextComment.tag = indexPath.row + COMMENTBUTTONTAG;
        cell.imageContent.tag =indexPath.row + COMMENTBUTTONTAG;
        [cell layoutIfNeeded];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MediaCell *cell = [tableView dequeueReusableCellWithIdentifier:MediaCellIDMur forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    /*
    MurAgendaCell *cell = [tableView dequeueReusableCellWithIdentifier:MurAgendaID forIndexPath:indexPath];
    [self configureAgendaCell:cell forRowAtIndexPath:indexPath];
    MurAgendaCell *cell = [tableView dequeueReusableCellWithIdentifier:MurGroupID forIndexPath:indexPath];
    [self configureGroupCell:cell forRowAtIndexPath:indexPath];
    */

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
//MARK: - [START GROUP]
- (void)configureGroupCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[MurAgendaCell class]])
    {
        MurAgendaCell *cell = (MurAgendaCell *)cellTmp;
        [cell fnSetColorWithExpectTarget:ISLOUNGE withIsLiveHunt:NO];
        NSDictionary *dic = [publicationArray objectAtIndex:indexPath.row] ;
        //TITLE
       NSString *strName=dic[@"name"];
        if([strName isEqualToString:@"(null)"]||strName==nil)
            strName=dic[@"lounge"][@"name"];
        cell.lbTitle.text=strName;
        //DESCRIPTION
        NSString *strText=[NSString stringWithFormat:@"%@",dic[@"description"]];
        if([strText isEqualToString:@"(null)"]||strText==nil)
            strText=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"description"]];
        cell.lbDesctiption.text = strText;
        //ACCESS
        NSString *strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"access"]];
        if([strAccessGroup isEqualToString:@"(null)"]||strAccessGroup==nil)
            strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"access"]];
        
        if(strAccessGroup.intValue == ACCESS_PRIVATE)
            cell.lbAccess.text=str(strPRIVATE);
        else if(strAccessGroup.intValue ==ACCESS_SEMIPRIVATE)
            cell.lbAccess.text=str(strSEMIPRIVATE);
        else if(strAccessGroup.intValue==ACCESS_PUBLIC)
            cell.lbAccess.text=str(strPUBLIC);
        //ACTION ACCESS
        cell.btnAccess.tag=indexPath.row+2;
       NSString *strAccess= [dic[@"connected"][@"access"] stringValue];
        NSInteger iAccess = 1000;
        if (strAccess && ![strAccess isEqualToString:@""]) {
            iAccess=[strAccess intValue];
        }
        [cell.btnAccess removeTarget:self action:@selector(groupRejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAccess removeTarget:self action:@selector(groupGotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAccess removeTarget:self action:@selector(groupCellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
        
        
        switch (iAccess)
        {
            case USER_INVITED:
                
            {
                [cell.btnAccess setTitle:str(strTo_Commit) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(groupRejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.btnAccess setTitle:str(strOn_hold) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:NO];
                cell.btnAccess.backgroundColor = [UIColor lightGrayColor];
            }
                break;
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.btnAccess setTitle:str(strAAccess) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(groupGotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                
                [cell.btnAccess setTitle:str(strSinscrire) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(groupCellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
                break;
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];


    }
}
-(void)groupRejoindreCommitAction:(UIButton *)sender{
    [UIAlertView showWithTitle:str(strTitle_app) message:str(strVous_avez_ete_invite_dans_ce_group)
             cancelButtonTitle:str(strAnuler)
             otherButtonTitles:@[str(strValider)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self groupCellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}
- (void)groupCellRejoindreFromInviter:(UIButton *)sender
{
    NSMutableArray *arrData = [NSMutableArray new];
    arrData =[publicationArray mutableCopy];
    NSInteger index = sender.tag-2;
    NSString *strLoungeID=arrData[index][@"id"];
    NSDictionary *dic =arrData[index];
    NSInteger accessValueNumber = [dic[@"access"] integerValue];
    NSString *desc=@"";
    NSString *strNameGroup=dic[@"name"];
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        desc =[NSString stringWithFormat:str(strRejoindre_un_group_public),strNameGroup];
        
    }
    else if (accessValueNumber == ACCESS_PRIVATE || accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoindre_un_group),strNameGroup];
        
    }
    else
    {
        NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
    }
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [LoungeJoinAction putJoinWithKind:MYGROUP mykind_id:strLoungeID strUserID:UserId ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [COMMON removeProgressLoading];
        if([respone isKindOfClass: [NSArray class] ]) {
            return ;
        }
        BOOL access= [respone[@"success"] boolValue];
        if (access) {
            
            NSDictionary *oldDict = dic[@"connected"];
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(2) forKey:@"access"];
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [publicationArray replaceObjectAtIndex:index withObject:newDictConnect];
            //nav
            [self fngotoGroup:index];
        }

        [UIAlertView showWithTitle:str(strTitle_app) message:desc
                 cancelButtonTitle:str(strOui)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
        [self.tableControl reloadData];
    };
    
}
-(void) groupGotoGroup:(UIButton*)sender
{
    NSInteger tag = ((UIButton *)sender).tag-2;
    [self fngotoGroup:tag];
}
- (void)groupCellRejoindre:(UIButton *)sender
{
    NSMutableArray *arrData = [NSMutableArray new];
    arrData =[publicationArray copy];
    
    if ([sender isSelected]) {
        return;
    }
    else{
        NSInteger btn_index=sender.tag-2;
       NSString *strLoungeID=[[arrData objectAtIndex:btn_index]valueForKey:@"id"];
        
        
        NSInteger accessValueNumber = [arrData[btn_index][@"access"] integerValue];
        NSString *desc=@"";
        NSString *strNameGroup=arrData[btn_index][@"name"];
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[NSString stringWithFormat:str(strRejoindre_un_group_public),strNameGroup];
            
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE || accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoindre_un_group),strNameGroup];
            
        }
        else
        {
            NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
        }
        [COMMON addLoading:_parentVC];
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        [LoungeJoinAction postGroupJoinAction:strLoungeID];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            
            [COMMON removeProgressLoading];
            if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
                [self.tableControl reloadData];
                return ;
            }
            NSInteger access= [respone[@"access"] integerValue];
            if (access==NSNotFound) {
                [self.tableControl reloadData];
                return ;
            }

            [UIAlertView showWithTitle:str(strTitle_app) message:desc
                     cancelButtonTitle:@"Oui"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              }];
            //set value with key =@"access"
            NSDictionary*dic = arrData[btn_index];
            NSDictionary *oldDict = dic[@"connected"];
            
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(access) forKey:@"access"];
            
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [publicationArray replaceObjectAtIndex:btn_index withObject:newDictConnect];
            [self.tableControl reloadData];
        };
    }
}
-(void)fngotoGroup:(NSInteger)index
{
    NSMutableArray *arrData = [NSMutableArray new];
        arrData =[publicationArray copy];
    NSDictionary *dic= [arrData objectAtIndex:index];
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    
    [_parentVC pushVC:viewController1 animate:YES];
    //
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
}
//MARK: -[END GROUP]
//MARK: - [START AGENDA]
- (void)configureAgendaCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[MurAgendaCell class]])
    {
        MurAgendaCell *cell = (MurAgendaCell *)cellTmp;
        [cell fnSetColorWithExpectTarget:ISLOUNGE withIsLiveHunt:NO];
        NSDictionary *dic = [publicationArray objectAtIndex:indexPath.row] ;
        //title
        NSString *strName=dic[@"name"];
        if([strName isEqualToString:@"(null)"]||strName==nil)
            strName=dic[@"lounge"][@"name"];
        cell.lbTitle.text=strName;
        //access
        NSString *strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"access"]];
        if([strAccessGroup isEqualToString:@"(null)"]||strAccessGroup==nil)
            strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"access"]];
        
        if(strAccessGroup.intValue == ACCESS_PRIVATE)
            cell.lbAccess.text=str(strAccessPrivate);
        else if(strAccessGroup.intValue ==ACCESS_SEMIPRIVATE)
            cell.lbAccess.text=str(strAccessSemiPrivate);
        else if(strAccessGroup.intValue==ACCESS_PUBLIC)
            cell.lbAccess.text=str(strAccessPublic);
        //date
        //DATE
        NSString * dateStrings = [_parentVC convertDate:[NSString stringWithFormat:@"%@",dic[@"meetingDate"]]];
        if(dateStrings !=nil)
            [cell.lbFinValue setText:[NSString stringWithFormat:@"%@",dateStrings]];
        else
            [cell.lbFinValue setText:@""];
        
        //END DATE
        NSString *outputStrings= [_parentVC convertDate:[NSString stringWithFormat:@"%@",dic[@"endDate"]]];
        if(outputStrings !=nil)
            cell.lbDebutValue.text = [NSString stringWithFormat:@"%@", outputStrings];
        else
            cell.lbDebutValue.text = @"";
        //
        NSInteger iAccess = 10;
        
        if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
            iAccess = [dic[@"connected"][@"access"]intValue];
        }
        else
        {
            iAccess = 4;
        }
        //ADDRESS
        NSString*strLocation;
        if(dic[@"meetingAddress"][@"address"]!=nil){
            strLocation=[NSString stringWithFormat:@"%@",dic[@"meetingAddress"][@"address"]];
            [cell.lbLocationValue setText:strLocation];
        }
        else {
            [cell.lbLocationValue setText: [ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meetingAddress"][@"latitude"] floatValue],
                                            [dic[@"meetingAddress"][@"longitude"] floatValue]
                                            ] ];
        }
        //LOCATION
        [cell.btnLocation addTarget:self action:@selector(agendaLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnLocation.tag = indexPath.row + 200;
        
        //DESCRIPTION
        NSString *strText;
        strText=[NSString stringWithFormat:@"%@",dic[@"description"]];
        if([strText isEqualToString:@"(null)"]||strText==nil)
            strText=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"description"]];
        if ([strText isEqualToString:@"(null)"]) {
            strText=@"";
        }
        cell.lbDesctiption.text = strText;
        //refresh
        cell.btnAccess.tag=indexPath.row+2;
        [cell.btnAccess removeTarget:self action:@selector(agendaRejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAccess removeTarget:self action:@selector(agendaGotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnAccess removeTarget:self action:@selector(agendaCellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
        
        switch (iAccess)
        {
            case USER_INVITED:
                
            {
                [cell.btnAccess setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(agendaRejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.btnAccess setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:NO];
                cell.btnAccess.backgroundColor = [UIColor lightGrayColor];
            }
                break;
                
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.btnAccess setTitle:str(strACCEDER) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(agendaGotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_REJOINDRE:
            {
                [cell.btnAccess setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
                [cell.btnAccess setEnabled:YES];
                [cell.btnAccess addTarget:self action:@selector(agendaCellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                break;
        }
        
        
    }
}
-(void) agendaGotoGroup:(UIButton*)sender
{
    
    NSDictionary *dic = publicationArray [sender.tag-2];
    
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    
    [_parentVC pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

- (void)agendaCellRejoindre:(UIButton *)sender
{
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[publicationArray copy];
    
    if ([sender isSelected]) {
        return;
    }
    else{
        NSInteger btn_index=sender.tag-2;
        NSString *strLoungeID=arrData[btn_index][@"id"];
        NSInteger accessValueNumber = [arrData[btn_index][@"access"] integerValue];
        NSString *desc=@"";
        NSString *strNameGroup=arrData[btn_index][@"name"];
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            
            desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
            
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            //Join_Salon_Text
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            //Join_Salon_Text
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
        }
        
        else
        {
            NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
        }
        [COMMON addLoading:_parentVC];
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        [LoungeJoinAction postLoungeJoinAction:strLoungeID];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            
            [COMMON removeProgressLoading];
            if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
                [self.tableControl reloadData];
                return ;
            }
            NSInteger access= [respone[@"access"] integerValue];
            if (access==NSNotFound) {
                [self.tableControl reloadData];
                return ;
            }
            NSDictionary*dic = arrData[btn_index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(access) forKey:@"access"];
            
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            
            if (publicationArray.count > btn_index) {
                [publicationArray replaceObjectAtIndex:btn_index withObject:newDictConnect];
                [self.tableControl reloadData];
                
            }
            [UIAlertView showWithTitle:strTile message:desc
                     cancelButtonTitle:str(strYES)
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              }];
        };
    }
}

- (void)agendaCellRejoindreFromInviter:(UIButton *)sender
{
    NSInteger index = sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[publicationArray copy];
    
    NSString *strLoungeID=arrData[index][@"id"];
    
    NSInteger accessValueNumber = [arrData[index][@"access"] integerValue];
    
    NSString *desc=@"";
    NSString *strNameGroup=[arrData objectAtIndex:index][@"name"];
    NSString *strTile=@"";
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
        
        strTile = str(strRejoinAcceptPublic);
        
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
        
        
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
    }
    else
    {
        NSAssert(NO, @"Unknown access value: %ld", (long) accessValueNumber);
    }
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        [COMMON removeProgressLoading];
        if([respone isKindOfClass: [NSArray class] ]) {
            return ;
        }
        BOOL access= [respone[@"success"] boolValue];
        if (access) {
            NSDictionary*dic = arrData[index];
            NSMutableDictionary *oldDict = [[NSMutableDictionary alloc] init];
            if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                oldDict = dic[@"connected"];
            }
            NSMutableDictionary *newDictAccess = [[NSMutableDictionary alloc] init];
            [newDictAccess addEntriesFromDictionary:oldDict];
            [newDictAccess setObject:@(2) forKey:@"access"];
            NSMutableDictionary *newDictConnect = [[NSMutableDictionary alloc] init];
            [newDictConnect addEntriesFromDictionary:dic];
            [newDictConnect setObject:newDictAccess forKey:@"connected"];
            [publicationArray replaceObjectAtIndex:index withObject:newDictConnect];
            
        }
        
        ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
        viewController1.dicChassesItem =arrData[index];
        viewController1.isInvitionAttend =YES;
        [viewController1 doCallback:^(NSString *strID) {
            GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
            
            [[GroupEnterOBJ sharedInstance] resetParams];
            [GroupEnterOBJ sharedInstance].dictionaryGroup = arrData[index];
            [_parentVC pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        }];
        [_parentVC pushVC:viewController1 animate:YES];
        //        vc.exp =ISLOUNGE;
        [self.tableControl reloadData];
    };
}
-(void)agendaRejoindreCommitAction:(UIButton *)sender{
    NSInteger index = sender.tag-2;
    NSMutableArray *arrData =[NSMutableArray new];
    arrData =[publicationArray copy];
    
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),arrData[index][@"name"]];
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self agendaCellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}

- (IBAction)agendaLocationAction:(UIButton*) sender
{
    int index = (int) sender.tag - 200;
    NSDictionary *myDic = publicationArray[index];
    
    id dic = myDic[@"meetingAddress"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = NO;
        viewController1.isSpecial = YES;
        
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"]};
        
        viewController1.needRemoveSubItem = NO;
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = myDic;
        
        [_parentVC pushVC:viewController1 animate:YES expectTarget: ISLOUNGE];
    }
}
//MARK: - [END AGENDA]

-(void) fnAller:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSInteger tag = [btn tag] - ALLERBUTTONTAG;
    
    NSDictionary *dic = [publicationArray objectAtIndex:tag] ;
    
    AllerMUR *vc = [[AllerMUR alloc] initWithData:dic];
    
    [vc doBlock:^(NSInteger index) {
        switch (index) {
            case 0:
            {
                //cancel
            }
                break;
            case 1:
            {
                MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
                mapVC.isSubVC = YES;
                mapVC.simpleDic = dic;
                mapVC.isLiveHuntDetail = YES;
                [_parentVC pushVC:mapVC animate:YES expectTarget:self.expectTarget];

                
                
                //to naturapass
                /*
                MapGlobalVC *mapVC = [[MapGlobalVC alloc]initWithNibName:@"MapGlobalVC" bundle:nil];
                mapVC.simpleDic = @{@"latitude":dic[@"geolocation"][@"latitude"],
                                    @"longitude":dic[@"geolocation"][@"longitude"]};
                
                [_parentVC pushVC:mapVC animate:YES expectTarget:ISCARTE];
                */
                
            }
                break;
            case 2:
            {
                //self add new VIEW.
                NSString *strLat =@"";
                NSString *strLng =@"";
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                double lat = appDelegate.locationManager.location.coordinate.latitude;
                double lng = appDelegate.locationManager.location.coordinate.longitude;
                
                strLat = dic[@"geolocation"][@"latitude"];
                strLng = dic[@"geolocation"][@"longitude"];
                
                AllerMURGPS *vc = [[AllerMURGPS alloc] initWithData:nil];
                
                [vc doBlock:^(NSInteger index) {
                    
                    switch (index) {
                            
                        case 0:
                        {}break;
                            
                        case 1:
                        {
                            //cancel
                            // Plans
                            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
                            NSURL *url =[NSURL URLWithString:str];
                            
                            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                                [[UIApplication sharedApplication] openURL:url];
                            } else {
                                NSLog(@"Can't use applemap://");
                            }
                            
                        }
                            break;
                        case 2:
                        {
                            //Maps
                            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
                            
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
                            }
                            
                        }
                            break;
                        case 3:
                        {
                            //Waze
                            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"waze://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                // Waze is not installed. Launch AppStore to install Waze app
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
                            }
                            
                        }
                            break;
                            
                    }
                    
                }];
                [vc showAlert];
                
            }
                break;
            default:
                break;
        }
    }];
    [vc fnColorWithExpectTarget:self.expectTarget];
    [vc showAlert];
    
}

#pragma mark - Resize Image
-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}
-(CGSize) fnResize:(CGSize)size width:(float)wWidth
{
    if (0 == size.height) {
        return CGSizeMake(0,0);
    }
    
    float actualHeight = size.height;
    float actualWidth = size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    return CGSizeMake(actualWidth, actualHeight);
}
#pragma mark - Action
-(void)reloadDataItemWithIndex:(NSString*)publicationId
{
    [self getPublictionItem:publicationId];
}

-(void) displayCommentVC: (NSDictionary *)dic
{
    
    CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
    commentVC.commentDic= dic;
    
    [commentVC doCallback:^(NSString *pubicationID) {
        [self reloadDataItemWithIndex:pubicationID];
    }];
    
    [commentVC setCallbackWithDelItem:^(NSDictionary *dicPublication)
     {
         //delete
         for (NSDictionary *tmp in publicationArray) {
             if ([[tmp[@"id"] stringValue] isEqualToString:[dicPublication[@"id"] stringValue]]) {
                 [publicationArray removeObject:tmp];
                 
                 if (_callback) {
                     _callback(VIEW_ACTION_UPDATE_DATA, publicationArray);
                 }
                 break;
             }
         }
         [self.tableControl reloadData];
   
     }];
    // push
    [_parentVC pushVC:commentVC animate:YES expectTarget:self.expectTarget];
    
}
#pragma mark - user profile action

- (void) userProfileAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[[publicationArray objectAtIndex:tag][@"owner"][@"id"] integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    if ([_parentVC isKindOfClass:[FriendInfoVC class]]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    
    [friend setFriendDic:[[publicationArray objectAtIndex:tag] objectForKey:@"owner"]];
    [_parentVC pushVC:friend animate:YES expectTarget:self.expectTarget];
}

- (void) userProfileFromTheirNameAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[[publicationArray objectAtIndex:tag][@"owner"][@"id"] integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    if ([_parentVC isKindOfClass:[FriendInfoVC class]]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic:[[publicationArray objectAtIndex:tag] objectForKey:@"owner"]];
    [_parentVC pushVC:friend animate:YES expectTarget:self.expectTarget];
    
}
- (IBAction)locationButtonAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSInteger locationtag = [btn tag] - LOCATIONBUTTONTAG;
    
    NSDictionary*dic = [publicationArray objectAtIndex:locationtag] ;
    
    if ([dic[@"geolocation"] isKindOfClass: [NSDictionary class ] ])
    {
        NSDictionary *dLocation = [[publicationArray objectAtIndex:locationtag] valueForKey:@"geolocation"];
        
        if (dLocation != nil)
        {
            MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
            mapVC.isSubVC = YES;
            
            mapVC.simpleDic = dic;
            
            [_parentVC pushVC:mapVC animate:YES expectTarget:self.expectTarget];
            
        }
    }
    
}

#pragma mark - menu
-(IBAction)settingAction:(UIButton *)sender
{
    
    NSInteger index =[sender tag]-100;
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    [cell show];
    [cell setCallBackGroup:^(NSInteger ind)
     {
         NSDictionary *dic = [publicationArray objectAtIndex:index] ;
         
         //Editer/supprimer/signaler
         
         if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
             switch (ind) {
                     
                 case 0:
                 {
                     [self editerAction:index];
                     
                 }
                     break;
                 case 1:
                 {
                     NSString *strContent = str(strMessage23);
                     
                     [UIAlertView showWithTitle: str(strMessage24)
                                        message:strContent
                              cancelButtonTitle:str(strOui)
                              otherButtonTitles:@[str(strNon)]
                                       tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                           if (buttonIndex == [alertView cancelButtonIndex]) {
                                               [self deletePublicationAction:index];
                                           }
                                           else
                                           {
                                               
                                           }
                                       }];
                     
                     
                 }
                     break;
                 case 2:
                 {
                     //fake location button
                     UIButton *btnLocationFake = [UIButton new];
                     btnLocationFake.tag = index + LOCATIONBUTTONTAG;
                     [self locationButtonAction:btnLocationFake];

                 }
                     break;
                 default:
                     break;
             }
             
         }else{
             switch (ind) {
                     
                 case 0:
                 {
                     [self signalerAction:index];
                     
                 }
                     break;
                 case 1:
                 {
                     [self blackListerAction: index];
                 }
                     break;
                 case 2:
                 {
                     //fake location button
                     UIButton *btnLocationFake = [UIButton new];
                     btnLocationFake.tag = index + LOCATIONBUTTONTAG;
                     [self locationButtonAction:btnLocationFake];

                 }
                     break;
                 default:
                     break;
             }
         }
         
         
     }];
}
#pragma mark - LikeAction

-(IBAction)likeButtonAction:(UIButton *)sender
{
    
    [COMMON addLoadingForView:_parentVC.view];
    [sender setSelected:YES];
    NSInteger liketag = [sender tag] - LIKETAG;
    
    if([[[publicationArray objectAtIndex:liketag] valueForKey:@"isUserLike"] integerValue]==1)
    {
        //Current like -> unlike
        
        NSString *pubicationID=[[[publicationArray objectAtIndex:liketag]valueForKey:@"id"] stringValue];
        
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj deletePublicationActionAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            
//            [self getPublictionItem:pubicationID];
            [self updateLike:liketag withDicLike:response];

        };
    }
    else
    {
        //Current unlike
        
        NSString *pubicationID=[[[publicationArray objectAtIndex:liketag]valueForKey:@"id"] stringValue];
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj postPublicationLikeAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
//            [self getPublictionItem:pubicationID];
            [self updateLike:liketag withDicLike:response];
        };
    }
}
-(void)updateLike:(NSInteger)index withDicLike:(NSDictionary*)dicLike
{
    if (dicLike[@"likes"]) {
        NSMutableDictionary *dic = [publicationArray[index] mutableCopy];
        [dic setObject:dicLike[@"likes"] forKey:@"likes"];
        [dic setObject:dicLike[@"likedusers"] forKey:@"likedusers"];
        BOOL isUserLike = ![dic[@"isUserLike"] boolValue];
        [dic setObject:@(isUserLike) forKey:@"isUserLike"];
        [publicationArray replaceObjectAtIndex:index withObject:dic];
        [self.tableControl reloadData];
        if (_callback) {
            _callback(VIEW_ACTION_UPDATE_DATA, publicationArray);
        }
    }
}
#pragma mark - DeleteAction

-(void)deletePublicationAction:(NSInteger )index
{
    [COMMON addLoadingForView:_parentVC.view];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI deletePublicationAction:publicationArray[index][@"id"]];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        NSDictionary *dic = (NSDictionary*)response;
        if ([dic[@"success"] boolValue]) {
            
            //update sqldb
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                
                NSString *strQuerry = [NSString stringWithFormat:@"DELETE FROM `tb_carte` WHERE `c_id` IN (%@) AND ( c_user_id=%@ OR c_user_id= %d);", publicationArray[index][@"id"], sender_id, SPECIAL_USER_ID];
                
                NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                
                ASLog(@"%@", tmpStr);
                
                BOOL isOK =   [db  executeUpdate:tmpStr];
                
            }];
            
            [publicationArray removeObjectAtIndex:index];
            
            // Path to save array data
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_MUR_SAVE)   ];
            [publicationArray writeToFile:strPath atomically:YES];
            
            [self.tableControl reloadData];
            if (_callback) {
                _callback(VIEW_ACTION_UPDATE_DATA, publicationArray);
            }
            
        }
    };
}

- (IBAction)signalerAction:(NSInteger)iTag {
    
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    
    
    AlertVC *vc = [[AlertVC alloc] initAlertSignalez];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        
        switch ((int)index) {
                //ACCUEIL -> Homepage
            case 0:
            {
            }
                break;
                
            case 1:
            {
                NSDictionary * postDict = @{@"publication": @{@"signal":@1, @"explanation":str(strContent)} };
                
                WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                
                [serviceObj postPublicationSignalAction: dic[@"id"] withParametersDic:postDict];
                
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    NSLog(@"%@",response);
                    
                };

            }
                break;
                
            default:
                break;
        }
    }];
    [vc showAlert];
}

-(void)blackListerAction:(NSInteger)iTag
{
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    
    NSString *strContent =[NSString stringWithFormat: str(strVoulez_vous_blacklister),dic[@"owner"][@"fullname"]];
    
    [UIAlertView showWithTitle:str(strBlacklister)
                       message:strContent
             cancelButtonTitle:str(strOUI)
             otherButtonTitles:@[str(strNON)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              [COMMON addLoading:_parentVC];
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj postUserLock:@{@"id": dic[@"owner"][@"id"]}];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  if (_callback) {
                                      _callback(VIEW_ACTION_REFRESH_TOP, publicationArray);
                                  }
                              };
                          }
                          else
                          {
                              
                          }
                      }];
}
/*
 
 */
-(void)editerAction:(NSInteger)iTag
{
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    [[PublicationOBJ sharedInstance] fnSetValueForKeyWithPublication:dic];
    [PublicationOBJ sharedInstance].mParentVC =_parentVC;
    SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    [_parentVC pushVC:viewController1 animate:YES expectTarget:ISMUR];
}

-(IBAction)openFileViewAction:(UIButton *)sender
{
    //test with PNG ggtt

    //if difference with type psd => open it
    NSURL *URL = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"xlsx"];
    
    
    if (URL) {
        // Initialize Document Interaction Controller
        self.documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:URL];
        
        // Configure Document Interaction Controller
        [self.documentInteractionController setDelegate:self];
        
        // Present Open In Menu
//        [self.documentInteractionController presentOpenInMenuFromRect:CGRectZero inView:self animated:YES];
        [self.documentInteractionController presentPreviewAnimated:YES];

    }

}

- (UIViewController *) documentInteractionControllerViewControllerForPreview: (UIDocumentInteractionController *) controller {
    return _parentVC;
}

-(IBAction)commentViewAction:(UIButton *)sender
{
    int index = (int)sender.tag - COMMENTBUTTONTAG;
    NSDictionary *dic = [publicationArray objectAtIndex:index];
    [self displayCommentVC:dic];
}
-(IBAction)displayLikesList:(id)sender
{
    NSInteger index =[sender tag]-110;
    NSDictionary *dic = [publicationArray objectAtIndex:index];
    [COMMON addLoading:_parentVC];
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getPublicationLikesAction:dic[@"id"]];
    serviceAPI.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        if ([_parentVC fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"likes"]];
        
        if (arrMembers.count>0) {
            [self showPopupMembers:arrMembers];
        }
    };
    
}
-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    MurPeopleLikes *viewController1 = [[MurPeopleLikes alloc] initWithNibName:@"MurPeopleLikes" bundle:nil];
    [viewController1 setData:arrMembers];
    
    viewController1.expectTarget =self.expectTarget;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         [_parentVC pushVC:friend animate:YES expectTarget:self.expectTarget];
     }];
    [_parentVC pushVC:viewController1 animate:YES expectTarget:ISMUR];
    
}
#pragma mark - GET DATA

- (void)getPublictionItem:(NSString*)public_id {
    //request first page
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getPublicationsActionItem:public_id];
    __weak BaseVC *wself = _parentVC;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        dispatch_async(dispatch_get_main_queue(), ^{
            [COMMON removeProgressLoading];
            if([wself fnCheckResponse:response]) return;
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            NSMutableArray *publicationArrayItem =[NSMutableArray new];
            
            [publicationArrayItem addObjectsFromArray:@[response[@"publication"]]];
            
            if (publicationArrayItem.count > 0) {
                NSString *idItem =response[@"publication"][@"id"];
                for (int i=0; i<publicationArray.count; i++) {
                    NSString *idPub =[publicationArray objectAtIndex:i][@"id"];
                    
                    if (idItem.intValue== idPub.intValue) {
                        // update
                        [publicationArray replaceObjectAtIndex:i withObject:publicationArrayItem[0]];
//                        [self.tableControl reloadData];
                        [self reloadWithIndex:i];
                                                if (_callback) {
                                                    _callback(VIEW_ACTION_UPDATE_DATA, publicationArray);
                                                }
                        break;
                    }
                }
                
            }
        });
    };
}
-(void)reloadWithIndex:(int)index
{
    for (UITableViewCell *cell in [self.tableControl visibleCells]) {
        NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
        if (indexPath.row < publicationArray.count && index == indexPath.row) {
            [self configureCell:cell forRowAtIndexPath:indexPath];
        }
    }

}
-(void)readMoreDidClickedGesture:(UITapGestureRecognizer*)sender
{
    UIView *myContent = sender.view;
    NSInteger index = myContent.tag - 1001;
    NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
    [self displayCommentVC:dicTmp];
    /*
    NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
    BOOL readmore = ![dicTmp[@"readmore"] boolValue];
    [dicTmp setObject:@(readmore) forKey:@"readmore"];
    [publicationArray replaceObjectAtIndex:index withObject:dicTmp];
    
    [self.tableControl beginUpdates];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableControl endUpdates];
     */
}
//MARK: - Share detail view
-(IBAction)shareDetailAction:(UIButton *)sender
{
    NSInteger index =[sender tag]-210;
    NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];

    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    [self showShareDetail:self.tableControl withPointView:cell.imgIconShare withData:dicTmp];
}
-(void)showShareDetail:(UIView*)viewSuper withPointView:(UIView*)pointView withData:(NSDictionary*)dic
{
    [shareDetail fnInputData:dic];
    [shareDetail showAlertWithSuperView:viewSuper withPointView:pointView];
    [shareDetail doBlock:^(NSDictionary * _Nonnull dicResult) {
        
    }];
}
-(void)hideShareDetail
{
    if([shareDetail isDescendantOfView:self.tableControl]) {
        [shareDetail hideAlert];
    }
}
@end
