//
//  Card_Cell_Type10.h
//  Naturapass
//
//  Created by Manh on 9/28/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card_Cell_Type_Base.h"
typedef void (^AlertSeclectCallback)(NSDictionary *dicResult);

@interface Card_Cell_Type40 : Card_Cell_Type_Base
@property (nonatomic,copy) AlertSeclectCallback callback;
-(void)doBlock:(AlertSeclectCallback ) cb;
@end
