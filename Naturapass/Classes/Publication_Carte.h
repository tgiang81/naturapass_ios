//
//  Publication_Carte.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"
#import "BaseMapVC.h"

@interface Publication_Carte : BaseMapVC
{
}

//Show if setting view
@property (assign) BOOL isFromSetting;
@property (nonatomic,strong) NSDictionary *dicCenterMap;

@property (nonatomic,retain)IBOutlet UITextField  *addressTitleSetting;
@property (nonatomic,retain)IBOutlet UIButton  *btnFav;

//Publication
@property (nonatomic,retain)NSString  *photoLatitude;
@property (nonatomic,retain)NSString  *photoLongitude;
@property (nonatomic,retain)NSString  *photoAltitude;
@property (nonatomic,retain) NSDictionary *simpleDic;

@end
