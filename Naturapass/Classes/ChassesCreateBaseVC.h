//
//  ChassesCreateBaseVC.h
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "ChassesCreateOBJ.h"
@interface ChassesCreateBaseVC : BaseVC

@property (assign) BOOL needChangeMessageAlert;

@property (nonatomic,assign) AGENDA_CREATE_ADD_VIEW myTypeView;

@property (strong, nonatomic)   IBOutlet UIButton *btnSuivant;
@end
