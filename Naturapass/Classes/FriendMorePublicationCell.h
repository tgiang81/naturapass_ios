//
//  FriendMorePublicationCell.h
//  Naturapass
//
//  Created by JoJo on 12/17/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FriendMorePublicationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;

@end

NS_ASSUME_NONNULL_END
