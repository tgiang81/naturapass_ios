//
//  LiveRecording.m
//  Naturapass
//
//  Created by giangtu on 12/15/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "RecordingDB.h"
#import "DatabaseManager.h"

//    latestAltitudeValue = -1000000;
//    sumPositiveAltitude = 0;
//    sumNegativeAltitude = 0;
//
//    //30s request get altitude...input is current lat/lon
//    self.continuousPressTimer =  [NSTimer scheduledTimerWithTimeInterval:30 target:self selector:@selector(fnGetAltitude_elevation) userInfo:nil repeats:YES];
//
//? save
//  distance = [pedometerData.distance floatValue];
//  average = [pedometerData.averageActivePace floatValue];
// distance parcourue -- distance traveled
// vitesse moyenne --- average speed : pace estimation ...


#pragma MARK - RECORING

static RecordingDB *sharedInstance = nil;

static CMPedometer *pedometer;

@interface RecordingDB ()
{
}

@end

@implementation RecordingDB

+ (RecordingDB *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) initPedometer
{
    if ([CMPedometer isStepCountingAvailable]) {
        pedometer = [CMPedometer new];
    }
}

//Maintain your own separate count as an instance property.
+(void) queryPedometerFromDate: (NSDate*) stDate
{
    //    This handler block is only called once.
    
    // retrieve data between dates
    [pedometer queryPedometerDataFromDate: stDate toDate:[NSDate date] withHandler:^(CMPedometerData * pedometerData, NSError * error) {
        
        // historic pedometer data is provided here
        sharedInstance.myPedoMeterData = pedometerData;
    }];
}

+(void) startPedometerUpdates
{
    if ([CMPedometer isStepCountingAvailable])
    {
        // start live tracking
        [pedometer startPedometerUpdatesFromDate:[NSDate date]
                                     withHandler:^(CMPedometerData *pedometerData, NSError *error) {
                                         // this block is called for each live update
                                         
                                     }];
    }
}

+(void) stopPedometerUpdates
{
    [pedometer stopPedometerUpdates];
}

+(void) addWithAgenda:(NSString*) agendaId
            startTime:(NSString*) startTime
              endTime:(NSString*) endTime
             distance:(float) distance
              average: (float) average
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = @"";
        strQuerry = [NSString stringWithFormat: @"INSERT OR REPLACE INTO `tb_recording` (`c_user_id`,`c_agenda_id`,`c_start_time`,`c_end_time`,`c_distance`,`c_average`) VALUES ('%@','%@','%@','%@','%f','%f')",
                     sender_id,
                     agendaId,
                     startTime,
                     endTime,
                     distance,
                     average ] ;
        
        [db  executeUpdate:strQuerry];
    }];
}

+(BOOL) ifDataFoAgendaExist:(NSString*) agendaId
{
    __block BOOL bIsExist = NO;
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        //recording table
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_recording WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        while ([set_querry_hunt next])
        {
            bIsExist = YES;
        }
    }];
    
    return bIsExist;
}

+(NSDictionary*) getSumaryWithAgenda:(NSString*) agendaId
{
    NSMutableArray *mutArr = [NSMutableArray new];
    
   __block double sumPostiveAltitude = 0;
   __block double sumNegativeAltitude = 0;

    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        
        //recording table
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_recording WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            
            [mutArr addObject:@{@"c_start_time":  [set_querry_hunt stringForColumn:@"c_start_time"],
                                @"c_end_time":  [set_querry_hunt stringForColumn:@"c_end_time"],
                                
                                @"c_distance": @( [set_querry_hunt doubleForColumn:@"c_distance"]),
                                @"c_average":  @([set_querry_hunt doubleForColumn:@"c_average"])
                                }];
            
        }
        
        
        //tracking table
        strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_tracking WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        
        set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            sumPostiveAltitude = [set_querry_hunt doubleForColumn:@"c_positive_altitude"];
            sumNegativeAltitude = [set_querry_hunt doubleForColumn:@"c_negative_altitude"];
        }
        
    }];
    
    //calculate
    double sumDistance = 0;
    double sumAverage = 0;
    
    for (NSDictionary*dic in mutArr) {
        sumDistance += [dic[@"c_distance"] doubleValue];
        sumAverage += [dic[@"c_average"] doubleValue];
    }
    
    
    return @{@"distance" : @(sumDistance),
             @"average" : @(sumAverage),
             @"positive" : @(sumPostiveAltitude),
             @"negative" : @(sumNegativeAltitude)
             };
}

+(void) removeWithAgenda:(NSString*) agendaId{
    NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *strQuerry = [ NSString stringWithFormat: @"DELETE FROM `tb_recording` WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        [db  executeUpdate:strQuerry];
    }];
    
}

#pragma mark - Positive - Negative Altitude

+(void) addAltitudeWithAgenda:(NSString*) agendaId andValue:(float) mValueAltitude
{
    //cache last value also
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        
        NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = @"";
        
        strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_tracking WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry];
        
        double positive_altitude = 0;
        double negative_altitude = 0;
        double latest_altitude = 0;

        while ([set_querry_hunt next])
        {
            positive_altitude = [set_querry_hunt doubleForColumn:@"c_positive_altitude"];
            negative_altitude = [set_querry_hunt doubleForColumn:@"c_negative_altitude"];
            latest_altitude   = [set_querry_hunt doubleForColumn:@"c_latest_altitude"];
        }
        
        //special number
        if (  mValueAltitude > latest_altitude) {
            //positive
            positive_altitude += positive_altitude - latest_altitude ;
            
            strQuerry = [NSString stringWithFormat: @"INSERT OR REPLACE INTO `tb_tracking` (`c_user_id`,`c_agenda_id`,`c_positive_altitude`,`c_latest_altitude`) VALUES ('%@','%@','%f','%f')",
                         sender_id,
                         agendaId,
                         positive_altitude,
                         mValueAltitude] ;

        }else if (  mValueAltitude < latest_altitude) {
            negative_altitude += latest_altitude - mValueAltitude;
            
            strQuerry = [NSString stringWithFormat: @"INSERT OR REPLACE INTO `tb_tracking` (`c_user_id`,`c_agenda_id`,`c_negative_altitude`,`c_latest_altitude`) VALUES ('%@','%@','%f','%f')",
                         sender_id,
                         agendaId,
                         negative_altitude,
                         mValueAltitude ] ;

        }
        [db  executeUpdate:strQuerry];
    }];
}

+(void) removeTrackingWithAgenda:(NSString*) agendaId{
    NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *strQuerry = [ NSString stringWithFormat: @"DELETE FROM `tb_tracking` WHERE `c_user_id` = '%@' AND `c_agenda_id` = '%@';", sender_id, agendaId ] ;
        [db  executeUpdate:strQuerry];
    }];
    
}

@end\
