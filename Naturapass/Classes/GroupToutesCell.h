//
//  SalonViewCustomcell.h
//  Naturapass
//
//  Created by ocsdev7 on 03/12/13.
//  Copyright (c) 2013 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDStatusButton.h"
#import "AppCommon.h"

@interface GroupToutesCell : UITableViewCell

@property (nonatomic,retain) IBOutlet MDStatusButton      *btnRejoindre;
@property (nonatomic,retain) IBOutlet MDStatusButton      *btnMember;
@property (nonatomic,retain) IBOutlet UIImageView   *usericonImages;
@property (nonatomic,retain) IBOutlet UILabel       *titleNameLabel;
@property (nonatomic,retain) IBOutlet UILabel       *endDatelabel;
@property (nonatomic,retain) IBOutlet UILabel       *publicAccessLabel;
@property (nonatomic,retain) IBOutlet UILabel       *descLabel;
@property (weak, nonatomic) IBOutlet UIView *viewBorder;
-(void)fnSettingCell:(int)type;
@end
