//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertSuggestView.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "AlertSuggestCell.h"
#import "CommonHelper.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "NetworkCheckSignal.h"
static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";

static NSString *identifierSection0 = @"MyTableViewCell0";

@implementation AlertSuggestView
{
    CLLocation *my_Placemark;
    NSString   *my_addressString;
    NSMutableArray *arrFavorite;
    NSMutableArray *arrPlace;

}
-(instancetype)initSuggestView
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertSuggestView" owner:self options:nil] objectAtIndex:0] ;

    if (self) {
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertSuggestCell" bundle:nil] forCellReuseIdentifier:identifierSection0];
        self.tableControl.estimatedRowHeight = 60;
        [self.subView.layer setMasksToBounds:YES];
        self.subView.layer.cornerRadius= 5;
        self.subView.layer.borderWidth = 0.5;
        self.subView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
        my_Placemark= [CLLocation new];
        arrFavorite = [NSMutableArray new];
        arrPlace = [NSMutableArray new];

    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];

}
-(void)fnDataInput:(NSString*)strSearch
{
    self.strSearch = strSearch;
    [self processLoungesSearchingURL:self.strSearch];
}
#pragma callback
-(void)setCallback:(AlertSuggestViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertSuggestViewCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithSuperView:(UIView*)viewSuper withRect:(CGRect)rect
{
//    _btnDissmiss = [[UIButton alloc] initWithFrame:viewSuper.frame];
//    [viewSuper addSubview:_btnDissmiss];
//    [_btnDissmiss addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
//    _btnDissmiss.tag = 11111;

    
    UIView *view = self;
    [viewSuper addSubview:view];
    view.frame = rect;
    [self reloadData];
}
-(void) reloadData
{
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite_address  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrFavorite removeAllObjects];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"title":[[set_querry stringForColumn:@"c_title"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"],
                                      @"latitude":[set_querry stringForColumn:@"c_lat"],
                                      @"longitude":[set_querry stringForColumn:@"c_lon"],
                                      @"fav": @(TRUE)
                                      };
            [arrFavorite addObject:objDic];
        }
        [self clearSearchResults];
        
    }];
}
-(NSArray*)resultArray:(NSArray*)arrDataPlace withFav:(NSArray*)arrFav
{
    if(self.strSearch.length > 0)
    {
        NSMutableArray *arrResult = [NSMutableArray new];
        NSString *predicateString = [NSString stringWithFormat:@"title contains[c] '%@'", self.strSearch];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
        arrResult = [[arrFav filteredArrayUsingPredicate:predicate] mutableCopy];
        [arrResult addObjectsFromArray:arrDataPlace];
        return arrResult;
    }
    else
    {
        return arrFav;
    }
}
-(IBAction)closeAction:(id)sender
{
    [_btnDissmiss removeFromSuperview];
    [self removeFromSuperview];
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
     NSString *strsearchValues= [searchText stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    self.strSearch = searchText;
    
    [self handleSearchForSearchString:strsearchValues];
}
//MARK: UISearchDisplayDelegate

- (void)handleSearchForSearchString:(NSString *)searchString
{
    if ([searchString isEqualToString:@""] || !searchString) {
        [self clearSearchResults];
        
    } else {
        [[HNKGooglePlacesAutocompleteQuery sharedQuery] resetAPIKEY:[[CommonHelper sharedInstance] getRandKeyGoogleSearch]];
        
        [self.searchQuery fetchPlacesForSearchQuery:searchString
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                                 }];
                                                 
                                             } else {
                                                 arrPlace = [places mutableCopy];
                                                self.searchResults = [self resultArray:arrPlace withFav:arrFavorite];
                                                 [self.tableControl reloadData];
                                             }
                                         }];
    }
}
#pragma mark Search Helpers

- (void)clearSearchResults
{
    self.searchResults = [self resultArray:arrPlace withFav:arrFavorite];
    [self.tableControl reloadData];
}

- (void)handleSearchError:(NSError *)error
{
    NSLog(@"ERROR = %@", error);
}
#pragma mark - Helpers

- (HNKGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.searchResults.count <indexPath.row)
        return nil;
    HNKGooglePlacesAutocompletePlace *place = self.searchResults[indexPath.row];
    return place;
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.searchResults count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AlertSuggestCell *cell = (AlertSuggestCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
    cell.imgCheck.image = [UIImage imageNamed:@"agenda_icon_address_favorite"];
    if ([self.searchResults[indexPath.row] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = self.searchResults[indexPath.row];
        cell.lbTitle.text = dic[@"title"];
        cell.imgCheck.hidden = NO;
    }
    else
    {
        cell.lbTitle.text = [self placeAtIndexPath:indexPath].name;
        cell.imgCheck.hidden = YES;
    }
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_btnDissmiss removeFromSuperview];
    [self removeFromSuperview];
    if ([self.searchResults[indexPath.row] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = self.searchResults[indexPath.row];
        my_addressString = dic[@"title"];
        my_Placemark = [[CLLocation alloc]initWithLatitude: [dic[@"latitude"] floatValue] longitude: [dic[@"longitude"] floatValue]];
        if ([COMMON isReachable])
        {
            [[NetworkCheckSignal sharedInstance] getAddressFromCoordinate:^(NSString* mAddress) {
                
                //i got the address
                my_addressString = mAddress;
                if (_callback) {
                    _callback(my_Placemark,my_addressString);
                }

            } withData:@{@"latitude" : dic[@"latitude"],
                         @"longitude" : dic[@"longitude"]}];
        }
        else
            {
                if (_callback) {
                    _callback(my_Placemark,my_addressString);
                }
            }

    }
    else
    {
        my_addressString = nil;
        HNKGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
        [CLPlacemark hnk_placemarkFromGooglePlace:place
                                           apiKey:self.searchQuery.apiKey
                                       completion:^(CLLocation *m_Placemark, NSString *addressString, NSError *error) {
                                           if (error) {
                                               [KSToastView ks_showToast:str(strAdresseIntrouvable) duration:2.0f completion: ^{
                                               }];
                                               
                                           } else if (m_Placemark) {
                                               if (self.searchResults.count > indexPath.row) {
                                                   my_Placemark = m_Placemark;
                                                   my_addressString= addressString;
                                                   
                                                   if (my_addressString.length>0) {
                                                       if (_callback) {
                                                           _callback(my_Placemark,my_addressString);
                                                       }
                                                   }
                                               }
                                           }
                                       }];    }

}
@end
