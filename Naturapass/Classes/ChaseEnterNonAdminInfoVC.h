//
//  ChaseEnterNonAdminInfoVC.h
//  Naturapass
//
//  Created by Giang on 12/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "GroupEnterBaseVC.h"
typedef void (^Callback)(NSString *strID);

@interface ChaseEnterNonAdminInfoVC : GroupEnterBaseVC
@property(nonatomic,strong) NSDictionary *dicChassesItem;
-(void)doCallback:(Callback)callback;
@property (nonatomic,copy) Callback callback;
@end
