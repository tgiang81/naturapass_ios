//
//  MyAlertView.h
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^MyAlertViewCbk)(NSInteger index);

@interface VoisChiens_Document_Alert : UIView <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *popTable;
}
@property (nonatomic,copy) MyAlertViewCbk callback;
@property (nonatomic,strong) IBOutlet UIView *viewBoder;

-(void)doBlock:(MyAlertViewCbk ) cb;

-(void) doShow :(UIViewController*)parent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBorder;

@end


