//
//  SettingActionView.h
//  Naturapass
//
//  Created by JoJo on 11/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^SettingActionViewViewCallback)(NSDictionary *dicResult);

@interface SettingActionView : UIView
{
    NSDictionary *publicationDic;
}
@property (strong, nonatomic) IBOutlet UIView *subView;
@property (strong, nonatomic) IBOutlet UIView *viewMore;
@property (strong, nonatomic) IBOutlet UIImageView *imgSettingMore;
@property (strong, nonatomic) IBOutlet UIView *viewGroups;
@property (strong, nonatomic) IBOutlet UIView *viewAgenda;
@property (strong, nonatomic) IBOutlet UIView *viewPersonne;

@property (strong, nonatomic) IBOutlet UILabel *lbAmisTitle;

@property (strong, nonatomic) IBOutlet UILabel *lbGroupTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbGroupDesc;

@property (strong, nonatomic) IBOutlet UILabel *lbAgendaTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbAgendaDesc;

@property (strong, nonatomic) IBOutlet UILabel *lbPersonneTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbPersonneDesc;
@property (strong, nonatomic) UIButton *btnDismiss;

@property (strong, nonatomic) IBOutlet UIView *paddingLine1;
@property (strong, nonatomic) IBOutlet UIView *paddingLine2;
@property (strong, nonatomic) IBOutlet UIView *paddingLine3;

@property (strong, nonatomic) IBOutlet UIView *bgImageShare;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeight1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeight2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeight3;

@property (nonatomic,copy) SettingActionViewViewCallback callback;
-(void)doBlock:(SettingActionViewViewCallback ) cb;
-(void)showAlertWithSuperView:(UIView*)viewSuper withPointView:(UIView*)pointView;
-(instancetype)initSettingActionView;
-(void)hideAlert;
-(void)fnInputData:(NSDictionary*)dic;
@end

NS_ASSUME_NONNULL_END
