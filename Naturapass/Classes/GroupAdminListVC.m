//
//  ConfigurationMessageViewController.m
//  Naturapass
//
//  Created by ocsdeveloper9 on 2/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import "GroupAdminListVC.h"
#import "UIAlertView+Blocks.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MDCheckBox.h"
#import "MDCheckDel.h"
#import "AlertVC.h"
#import "GroupEnterSettingVC.h"
#import "GroupCreateCell_Step4.h"
#import "SubNavigationPRE_ANNULER.h"
#import "GroupCreateOBJ.h"
#import "ChassesCreateOBJ.h"
static NSString *cellIdentifier = @"GroupCreateCell_Step4";
static NSString *identifierSection1 = @"MyTableViewCell1";
@interface GroupAdminListVC ()<WebServiceAPIDelegate>{
    NSInteger preViousTag;
    __weak IBOutlet UILabel *lbAdmin_Bannir;
    __weak IBOutlet UIButton *btnValider;

}

@end

@implementation GroupAdminListVC
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self intialization];
    sharingListArray =[NSMutableArray new];
    arrDataRoot =[NSMutableArray new];
    if (self.expectTarget ==ISLOUNGE) {
        btnValider.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        if ([ChassesCreateOBJ sharedInstance].isModifi) {
            [btnValider setTitle:str(strValider) forState:UIControlStateNormal ];
        }
    }
    else if(self.expectTarget == ISGROUP)
    {
        btnValider.backgroundColor =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        if ([GroupCreateOBJ sharedInstance].isModifi) {
            [btnValider setTitle:str(strValider) forState:UIControlStateNormal ];
        }

    }
    [self getLoungeSuscriber];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //neu la edit
    //Set title

    if ([ChassesCreateOBJ sharedInstance].isModifi || [GroupCreateOBJ sharedInstance].isModifi) {
        
        MainNavigationBaseView *subview =  [self getSubMainView];
        [self addSubNav:@"SubNavigationPRE_ANNULER"];
        //coloring btn
        SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
        
        UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
        UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
        if (self.expectTarget ==ISLOUNGE ) {
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            [subview.myTitle setText:str(strCHANTIERS)];

            btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
            
            btn2.backgroundColor = UIColorFromRGB(CHASSES_CANCEL);
        }
        else if(self.expectTarget == ISGROUP)
        {
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [subview.myTitle setText:str(strGROUPES)];
            
            btn1.backgroundColor = UIColorFromRGB(GROUP_BACK);
            
            btn2.backgroundColor = UIColorFromRGB(GROUP_CANCEL);
        }
    }



}
#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    if ([ChassesCreateOBJ sharedInstance].isModifi || [GroupCreateOBJ sharedInstance].isModifi) {
        switch (btn.tag -START_SUB_NAV_TAG) {
            case 0://BACK
            {
                [self gotoback];
            }
                break;
            case 1://ANNULER
            {
                NSString *str=@"";
                if (self.expectTarget ==ISLOUNGE) {
                    str=str(strAAgenda);
                }
                else if(self.expectTarget == ISGROUP)
                {
                    str=@"Group";
                    
                }
                AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"Annuler %@",str] message:@"Etes-vous sûr de vouloir annuler votre saisie ?"  cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];                
                [vc doBlock:^(NSInteger index , NSString *str) {
                    if (index==0) {
                        // NON
                    }
                    else if(index==1)
                    {
                        //OUI
                        NSArray * controllerArray = [[self navigationController] viewControllers];
                        
                        for (int i=0; i < controllerArray.count; i++) {
                            if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                                
                                [self.navigationController popToViewController:self.mParent animated:YES];
                                return;
                            }
                        }
                        [self doRemoveObservation];

                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                }];
                [vc showAlert];
            }
                break;
                
            default:
                break;
        }
    }
    else
    {
//        [self viewWillDisappear:YES];
        [super onSubNavClick:btn];
    }
    
}
-(void)intialization{
    [self.tableControl registerNib:[UINib nibWithNibName:cellIdentifier bundle:nil] forCellReuseIdentifier:identifierSection1];
    if (is_Admin) {
        [lbAdmin_Bannir setText:str(strADMINISTRATEUR)];
    }else{
        [lbAdmin_Bannir setText:str(strEXCLUREDESMEMBRES)];
    }
}
-(void)fnGroupId:(NSString*)group_id isAdmin:(BOOL)isAdmin
{
    Group_Id=group_id;
    is_Admin=isAdmin;
}

//{
//    isAdmin = 1;
//}

-(void)fnGroupSubcribers:(NSString*)groupid subcribers:(NSNumber*)subcriberID
{
    NSString *mykind =(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE;
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj setDelegate:self];
    [serviceObj putAdminSubscribersWithKind:mykind group_id:groupid subcribers_id:[subcriberID stringValue]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self.tableControl reloadData];
        if (!response) {
            return ;
        }
        if ([self fnCheckResponse:response]) {
            [self reloadDataWithSubscriberID:[subcriberID stringValue] isAdmin: YES];
        }
        if (response[@"isAdmin"] != nil) {
            [self reloadDataWithSubscriberID:[subcriberID stringValue] isAdmin: [response[@"isAdmin"]  boolValue]];
            [self gotoback];
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
    };
}
-(void) reloadDataWithSubscriberID:(NSString*) strID isAdmin:(BOOL) bAdmin
{
    //update root array
    for (int i=0; i< sharingListArray.count; i++) {
        NSDictionary*dic = sharingListArray[i];
        if ([[dic[@"user"][@"id"] stringValue] isEqualToString: strID ]) {
            NSMutableDictionary *mutDic = [dic mutableCopy];
            [mutDic removeObjectForKey:@"status"];
            [mutDic setValue: [NSNumber numberWithBool:bAdmin] forKey:@"status"];
            [sharingListArray replaceObjectAtIndex:i withObject:mutDic];
            [arrDataRoot removeAllObjects];
            arrDataRoot = [sharingListArray  mutableCopy];
            [self.tableControl reloadData];
        }
    }
}
//{
//    success = 1;
//}
-(void)fnUnsubscribeGroupAction:(NSString*)groupid subcribers:(NSNumber*)subcriberID{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj setDelegate:self];
    [serviceObj unsubscribeGroupAction:[subcriberID stringValue] :groupid];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self.tableControl reloadData];
        if (!response) {
            return ;
        }
        if ([self fnCheckResponse:response]) {
            [self reloadDataWithSubscriberID:[subcriberID stringValue] isAdmin: NO];
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        if (response[@"success"]) {
            [self reloadDataUnsubcriber: [subcriberID stringValue]];
            [self gotoback];
        }
    };
}
-(void) reloadDataUnsubcriber:(NSString*) strID
{
    //update root array
    for (int i=0; i< sharingListArray.count; i++) {
        NSDictionary*dic = sharingListArray[i];
        if ([[dic[@"user"][@"id"] stringValue] isEqualToString: strID ]) {
            [sharingListArray removeObjectAtIndex:i];
            [arrDataRoot removeAllObjects];
            arrDataRoot = [sharingListArray  mutableCopy];
            [self.tableControl reloadData];
        }
    }
}
-(void)getLoungeSuscriber {
    [COMMON addLoading:self];
    __weak typeof(self) wself = self;

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    if(self.expectTarget ==ISLOUNGE)
    {


        //mld
        [serviceObj getLoungeSubscribeAction:Group_Id];

    }
    else
    {
        [serviceObj getGroupToutesSubscribeAction:Group_Id];

    }
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }

        [sharingListArray removeAllObjects];
        [arrDataRoot removeAllObjects];

        
        NSArray *arrTmp = [response objectForKey:@"subscribers"];
        

        for (NSDictionary*dic in arrTmp) {
            NSMutableDictionary *mutDic = [dic mutableCopy];
            //admin
            if (is_Admin) {
                if ([dic[@"access"] intValue] == 3) {
                    [mutDic setValue:[NSNumber numberWithBool:YES] forKey:@"status"];
                    
                }
                else if ([dic[@"access"] intValue] == 2)
                {
                    [mutDic setValue:[NSNumber numberWithBool:NO] forKey:@"status"];

                }
                else{
                    continue;
                }
            }
            //banier
            else{
                [mutDic setValue:[NSNumber numberWithBool:NO] forKey:@"status"];

            }


            [sharingListArray addObject:mutDic];
            [arrDataRoot addObject:mutDic];

        }

        [self.tableControl reloadData];
        [COMMON removeProgressLoading];
    };
    
}

#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return [sharingListArray count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
        return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic= sharingListArray[indexPath.row];
    
    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    if (self.expectTarget == ISGROUP) {
        [cell.btnInvite setTypeCheckBox:UI_GROUP_MUR_ADMIN];
        
    }
    else if(self.expectTarget == ISLOUNGE)
    {
        [cell.btnInvite setTypeCheckBox:UI_CHASSES_MUR_ADMIN];
    }
    [cell.lblTitle setText:dic[@"user"][@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"user"][@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"user"][@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([dic[@"status"] boolValue] == YES) {
        [cell.btnInvite setSelected:YES];
        
    }else{
        [cell.btnInvite setSelected:NO];
        
    }
    [cell.btnInviteOver addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row +100;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
-(IBAction)selectTickAction:(id)sender
{
    UIButton *btn= (UIButton*)sender;
    int index =(int)btn.tag -100;
    //
    NSDictionary *dic= sharingListArray[index];
    NSMutableDictionary *mutDic = [NSMutableDictionary dictionaryWithDictionary:dic];
    if ([mutDic[@"status"] boolValue]) {
        [mutDic removeObjectForKey:@"status"];
        [mutDic setValue:[NSNumber numberWithBool:NO] forKey:@"status"];
        [sharingListArray replaceObjectAtIndex:index withObject:mutDic];
        [self.tableControl reloadData];
    }
    else
    {
        [mutDic removeObjectForKey:@"status"];
        [mutDic setValue:[NSNumber numberWithBool:YES] forKey:@"status"];
        [sharingListArray replaceObjectAtIndex:index withObject:mutDic];
        [self.tableControl reloadData];

    }
}
-(IBAction)validerAction:(id)sender
{
    BOOL isRun = NO;
    if (is_Admin) {
        for (int i= 0; i<sharingListArray.count; i++)
        {
            
            NSDictionary *dic = sharingListArray[i];
            if ([arrDataRoot[i][@"status"] boolValue] != [sharingListArray[i][@"status"] boolValue]) {
                if (isRun== NO) {
                    isRun = YES;
                    [COMMON addLoading:self];
                }
                [self fnGroupSubcribers:Group_Id subcribers:dic[@"user"][@"id"]];

            }
        }
    }
    else
    {
        for (int i= 0; i<sharingListArray.count; i++) {
            NSDictionary *dic = sharingListArray[i];
            if ([sharingListArray[i][@"status"] boolValue] ==YES) {
                if (isRun== NO) {
                    isRun = YES;
                    [COMMON addLoading:self];
                }
                
                [self fnUnsubscribeGroupAction:Group_Id subcribers:dic[@"user"][@"id"]];
            }

        }
    }
    

}
#pragma callback

-(void)setCallback:(GroupAdminListVCCallback)callback

{
    
    _callback=callback;
    
}

-(void)doCallback:(GroupAdminListVCCallback)callback

{
    
    self.callback=callback;
    
}
@end
