//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "SAMTextView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Define.h"
#import "BaseView.h"
typedef void (^AlertVosGroupesVCCallback)(NSArray *arrResult);
@interface AlertVosGroupesVC : BaseView <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger indexExpand;
}
@property (nonatomic, strong) NSMutableArray *arrMesGroupes;
@property (nonatomic, strong) NSMutableArray *arrSelected;
@property (nonatomic,copy) AlertVosGroupesVCCallback callback;
-(void)doBlock:(AlertVosGroupesVCCallback ) cb;
@property(nonatomic,strong)  UIColor *colorNavigation;
-(void)showAlertWithArrSelected:(NSArray*)arrSelected;
-(instancetype)initWithVosGroupes;
@property (nonatomic,strong) IBOutlet UIView *subAlertView;
@property (nonatomic,strong) IBOutlet UIView *headerView;
@property (nonatomic,strong) IBOutlet UIView *bottomView;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UIButton *btnTerminal;
@property (nonatomic,strong) IBOutlet UIButton *btnClose;
@property (nonatomic,strong) IBOutlet UITableView *tableControl;
@property (nonatomic,strong) IBOutlet UIImageView *imgLineHeader;
@property (nonatomic,strong) IBOutlet UIImageView *imgLineBottom;



-(void)fnSetexpectTarget:(ISSCREEN)exp;
@end
