//
//  FriendProfilCell.h
//  Naturapass
//
//  Created by JoJo on 12/17/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "Config.h"
NS_ASSUME_NONNULL_BEGIN

@interface FriendProfilCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCircleAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imgSettingBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgSettingNormal;
@property (weak, nonatomic) IBOutlet UIView *viewSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnBlock;
@property (weak, nonatomic) IBOutlet UIButton *btnDeBlock;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbAmis;
@property (weak, nonatomic) IBOutlet UILabel *lbAmisCommon;
@property (weak, nonatomic) IBOutlet UIButton *btnLeft;
@property (weak, nonatomic) IBOutlet UIButton *btnRight;

@property (weak, nonatomic) IBOutlet UIButton *btnListAmis;
@property (weak, nonatomic) IBOutlet UIButton *btnListAmisCommon;
@property (weak, nonatomic) IBOutlet UIButton *btnAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;

@property (weak, nonatomic) IBOutlet UIImageView *imgLine;
@property (weak, nonatomic) IBOutlet UIView *viewMain;

@property (weak, nonatomic) IBOutlet UIImageView *iconFriend;
@property (weak, nonatomic) IBOutlet UIImageView *iconFriendCommon;

@property (nonatomic,assign) ISSCREEN expectTarget;
@property(nonatomic,strong)  UIColor *colorCommon;
@property(nonatomic,strong)  UIColor *colorAnnuler;
-(void)fnSetColorWithExpectTarget:(ISSCREEN)expTarget;
-(void)fnSetFriendInfo:(NSDictionary*)dic;
@end

NS_ASSUME_NONNULL_END
