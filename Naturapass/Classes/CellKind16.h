//
//  CellKind16.h
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOLabel.h"
#import "AppCommon.h"

@interface CellKind16 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *nameHunt;
@property (weak, nonatomic) IBOutlet UILabel *accessType;
@property (weak, nonatomic) IBOutlet UILabel *contentHunt;
@property (weak, nonatomic) IBOutlet UIButton *btnValid;
@property (weak, nonatomic) IBOutlet UIButton *btnRefuse;

@property (weak, nonatomic) IBOutlet UIButton *btnLocation;


@property (weak, nonatomic) IBOutlet UILabel *startDate;
@property (weak, nonatomic) IBOutlet UILabel *endDate;
@property (weak, nonatomic) IBOutlet SOLabel *location;

@end
