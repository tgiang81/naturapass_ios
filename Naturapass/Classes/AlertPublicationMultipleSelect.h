//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "Define.h"
typedef void (^AlertSeclectCallback)(NSDictionary *dicResult);
@interface AlertPublicationMultipleSelect : UIViewController
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSArray *arrData;
    NSArray *listCheck;

    IBOutlet UITableView *tableControl;
    TYPE_CONTROL typeControl;
}
@property (nonatomic,copy) AlertSeclectCallback callback;
-(void)doBlock:(AlertSeclectCallback ) cb;
-(void)showInVC:(UIViewController*)vc;
-(instancetype)initWithTitle:(NSString*)stitle arrContent:(NSArray*)arrContent arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;

@end
