//
//  SignalerMediaCell.m
//  Naturapass
//
//  Created by JoJo on 9/11/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SignalerPartagerCell.h"

@implementation SignalerPartagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.viewPartager.layer setMasksToBounds:YES];
    self.viewPartager.layer.cornerRadius= 15;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
