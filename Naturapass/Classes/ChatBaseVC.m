//
//  ChatBaseVC.m
//  Naturapass
//
//  Created by Giang on 9/21/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatBaseVC.h"
#import "ChatListe.h"
#import "ChatVC.h"

@interface ChatBaseVC ()

@end

@implementation ChatBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addMainNav:@"MainNavMUR"];
    
    if ([self isKindOfClass:[ChatListe class]] ||
        [self isKindOfClass:[ChatVC class]]
        ) {
        self.showTabBottom = YES;
    }

    
    [self addSubNav:@"SubNavigation_DISCUSSION"];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //add sub navigation
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:@"DISCUSSIONS"];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(DISCUSSION_TINY_BAR_COLOR) ];
}
#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    [super onSubNavClick:btn];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
            
        case 1:
        {
            [self gotoback];

        }
            break;
        default:
            break;
    }
}




@end
