//
//  SalonViewCustomcell.m
//  Naturapass
//
//  Created by ocsdev7 on 03/12/13.
//  Copyright (c) 2013 OCSMobi - 11. All rights reserved.
//

#import "GroupToutesCell.h"
#import "Define.h"
#import "CommonHelper.h"
#import "MDStatusButton.h"
@interface GroupToutesCell ()

@end

@implementation GroupToutesCell
@synthesize btnRejoindre,btnMember;
@synthesize usericonImages;
@synthesize titleNameLabel;
@synthesize endDatelabel;
@synthesize publicAccessLabel;
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.viewBorder.layer setMasksToBounds:YES];
    self.viewBorder.layer.cornerRadius= 5.0;
    self.viewBorder.layer.borderWidth =0.5;
    self.viewBorder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
  
    [self.usericonImages.layer setMasksToBounds:YES];
    self.usericonImages.layer.cornerRadius= 30;
    self.usericonImages.layer.borderWidth =0;
    

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    
    self.titleNameLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.titleNameLabel.frame);

    self.descLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.descLabel.frame);
}

-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_ADMIN_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MUR_NORMAL_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
//            [self fnSetColor: UIColorFromRGB(GROUP_TOUS_COLOR)];
            [self.titleNameLabel setTextColor:UIColorFromRGB(GROUP_TOUS_COLOR)];

        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.titleNameLabel setTextColor:color];
    [self.btnMember setBackgroundColor:color];
    [self.btnRejoindre setBackgroundColor:color];
}
@end
