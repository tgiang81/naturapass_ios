//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "AppCommon.h"

@interface TypeCell10: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;
//
////image
//@property (nonatomic,retain) IBOutlet UIImageView       *image1;
//@property (nonatomic,retain) IBOutlet UIImageView       *image2;
//@property (nonatomic,retain) IBOutlet UIImageView       *image3;
//
//
//
//
////label
@property (nonatomic,retain) IBOutlet UILabel           *label1;
//
//@property (nonatomic,retain) IBOutlet UILabel           *lbTime1;
//@property (nonatomic,retain) IBOutlet UILabel           *lbTime2;
//@property (nonatomic,retain) IBOutlet UILabel           *lbTime3;
//
//@property (nonatomic,retain) IBOutlet UILabel           *lbDesc1;
//@property (nonatomic,retain) IBOutlet UILabel           *lbDesc2;
//@property (nonatomic,retain) IBOutlet UILabel           *lbDesc3;
//
//@property (nonatomic,retain) IBOutlet UILabel           *lbHumidity1;
//@property (nonatomic,retain) IBOutlet UILabel           *lbHumidity2;
//@property (nonatomic,retain) IBOutlet UILabel           *lbHumidity3;
//
//@property (nonatomic,retain) IBOutlet UILabel           *lbTemperature1;
//@property (nonatomic,retain) IBOutlet UILabel           *lbTemperature2;
//@property (nonatomic,retain) IBOutlet UILabel           *lbTemperature3;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollContent;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ContraintWithContentView;
@property(nonatomic,strong)  UIColor *colorNavigation;

//funtion
-(void)fnSettingCell:(int)type;
-(void)fnSetWeatherData:(NSArray*)arrWearther;
@end
