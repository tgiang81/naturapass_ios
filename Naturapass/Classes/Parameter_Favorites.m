//
//  MurParameter_Favorites.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Parameter_Favorites.h"
#import "Publication_Carte.h"
#import "MapLocationVC.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_Favorites ()
{
    NSMutableArray * arrData;
    __weak IBOutlet UILabel *warning;
    __weak IBOutlet UILabel *lbTitle;

}

@end

@implementation Parameter_Favorites

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMES_ADRESSES_FAVORITES);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:UPDATE_OBJ_NOTIFICATION object:nil];

    arrData = [NSMutableArray new];
    warning.hidden = YES;
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind8" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    /*
     {"addresses":[{"id":11,"address":"Ngõ 4 Trần Huy Liệu, Khu tập thể Giảng Võ, Giảng Võ, Ba Đình, Hà N
     ội, Vietnam","latitude":"21.0272775","longitude":"105.8234869","favorite":false,"title":"de"},
     
     {"id":12
     ,"address":"1 Hào Nam, Chợ Dừa, Đống Đa, Hà Nội, Vietnam","latitude":"21.025067","longitude":"105.827916"
     ,"favorite":false,"title":"def"},{"id":13,"address":"1 Hào Nam, Chợ Dừa, Đống Đa, Hà Nội, Vietnam","latitude"
     :"21.025067","longitude":"105.827916","favorite":false,"title":"da"}]}
     */
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind8 *cell = nil;
    
    cell = (CellKind8 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: @"param_ic_st_address"];
    
    //FONT
    cell.label1.text = dic[@"title"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.label1.numberOfLines=2;
    
    [cell.btnDel addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDel.tag =indexPath.row;
    //Arrow
    [cell.rightIcon setImage: [UIImage imageNamed:@"mur_st_ic_delete" ]];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //https://naturapass.e-conception.fr/api/v1/users/10/address
    [self locationButtonAction:indexPath.row];
}

-(void) refreshData
{
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        [COMMON removeProgressLoading];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite_address  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
        
        FMResultSet *set_querry = [db  executeQuery:strQuerry];
        
        [arrData removeAllObjects];
        
        while ([set_querry next])
        {
            //title/id
            NSDictionary *objDic = @{ @"title":[[set_querry stringForColumn:@"c_title"] stringByDecodingHTMLEntities],
                                      @"id":[set_querry stringForColumn:@"c_id"]
                                      };
            [arrData addObject:objDic];
        }
        
        [self.tableControl reloadData];
        
        if ([COMMON isReachable]) {
            
            if (arrData.count == 0) {
                warning.hidden = NO;
                
                warning.text = str(strMessage18);
                
            }else{
                
                warning.hidden = YES;
            }
            
        }
    }];
    
}

-(IBAction)deleteAction:(id)sender
{
    NSInteger index = [sender tag];
    [UIAlertView showWithTitle:str(strTitle_app)
                       message:str(strMessage19)
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[strNon]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              NSDictionary *dic = arrData[index];
                              
                              [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
                              [COMMON addLoadingForView:self.view];
                              
                              WebServiceAPI *serverAPI =[WebServiceAPI new];
                              serverAPI.onComplete = ^(id response, int errCode)
                              {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      [COMMON removeProgressLoading];
                                      [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
                                      
                                      if (errCode == 204 ) {
                                          //delete in sql...
                                          
                                          [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                                              NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
                                              NSString *strInsertTblVersion = [ NSString stringWithFormat: @"DELETE FROM `tb_favorite_address` WHERE `c_id` IN (%@) AND `c_user_id` = '%@';", dic[@"id"], sender_id ] ;
                                              //add version number to tble version.
                                              
                                              [db  executeUpdate:strInsertTblVersion];
                                              
                                              NSString *strQuerry = [NSString stringWithFormat:@"SELECT * FROM tb_favorite_address  WHERE c_user_id=%@  ORDER BY c_id", sender_id];
                                              
                                              FMResultSet *set_querry = [db  executeQuery:strQuerry];
                                              
                                              [arrData removeAllObjects];
                                              
                                              while ([set_querry next])
                                              {
                                                  //title/id
                                                  NSDictionary *objDic = @{ @"title":[[set_querry stringForColumn:@"c_title"] stringByDecodingHTMLEntities],
                                                                            @"id":[set_querry stringForColumn:@"c_id"]
                                                                            };
                                                  [arrData addObject:objDic];
                                              }
                                              
                                              [self.tableControl reloadData];
                                              
                                              if ([COMMON isReachable]) {
                                                  
                                                  if (arrData.count == 0) {
                                                      warning.hidden = NO;
                                                      
                                                      warning.text = str(strMessage18);
                                                      
                                                  }else{
                                                      
                                                      warning.hidden = YES;
                                                  }
                                                  
                                              }
                                              
                                          }];
                                          
                                      }
                                      
                                      
                                  });
                                  
                              };
                              
                              [serverAPI fnDELETE_USER_ADDRESS: @{@"id":dic[@"id"]}];
                              
                          }
                          else
                          {
                              
                          }
                      }];
}

-(IBAction)fnAddAddress:(id)sender
{
    Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
    viewController1.isFromSetting = YES;
    [self pushVC:viewController1 animate:YES];
}

- (void)locationButtonAction:(NSInteger)index
{
}
/*
 //https://naturapass.e-conception.fr/api/v1/user/addresses
 //https://naturapass.e-conception.fr/api/v1/users/addresses
 
 {"address":{"address":"Ngõ 4 Trần Huy Liệu, Khu tập thể Giảng Võ, Giảng Võ, Ba Đình, Hà Nội, Vietnam"
 ,"latitude":21.0272775,"longitude":105.82348690000003,"title":"de"}}
 
 */



@end
