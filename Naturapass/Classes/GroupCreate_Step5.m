//
//  GroupCreate_Step5.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step5.h"
#import "GroupCreate_Step6.h"
#import "TypeCell71.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MDCheckBox.h"
#import "AlertViewMembers.h"
#import "AlertViewMembersInvite.h"
#import "CCMPopupTransitioning.h"
static NSString *Cell_Step5 =@"TypeCell71";
static NSString *Cell_Step5_ID =@"TypeCell71ID";

static NSString *cellkind3 =@"CellKind3";
static NSString *cellkin3ID =@"CellKind3ID";


@interface GroupCreate_Step5 ()
{
    NSMutableArray *arrMesGroupes;
    NSMutableArray *tourMemberArray;
    int secIndex;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UILabel *lbDescription3;
    IBOutlet UILabel *lbDescription4;

}
@property (nonatomic, strong) TypeCell71 *prototypeCell;


@end

@implementation GroupCreate_Step5
- (TypeCell71 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:Cell_Step5_ID];
    }
    
    return _prototypeCell;
}
#pragma mark - viewdid and init
- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strInviterPersonnesGroup);
    lbDescription2.text = str(strIciVousPouvezInviter);
    lbDescription3.text = str(strIensembleDesMembresGroup);
    lbDescription4.text = str(strSeulementCertainsMembresGroup);
    self.needChangeMessageAlert = YES;

    // Do any additional setup after loading the view from its nib.
    [self initialization];
}
-(void)initialization
{
    arrMesGroupes = [NSMutableArray new];
    tourMemberArray = [NSMutableArray new];
    secIndex = -1;
    //
    [self.tableControl registerNib:[UINib nibWithNibName:Cell_Step5 bundle:nil] forCellReuseIdentifier:Cell_Step5_ID];
    
    [self initRefreshControl];
    [self insertRowAtTop];
}
#pragma mark - Actions

- (void)insertRowAtTop {
    //request first page
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    __weak typeof(self) wself = self;
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:@"0"];
    
    __weak GroupCreate_Step5 *weakSelf = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            
            return ;
        }
        
        if (response[@"groups"]) {
            [arrMesGroupes removeAllObjects];
            
            for (NSDictionary*dic in response[@"groups"] )
            {
                if ( [dic[@"id"] intValue] == [[GroupCreateOBJ sharedInstance].group_id intValue]) {
                    continue;
                }
                
                [arrMesGroupes addObject:dic];
            }
            
            
            [weakSelf.tableControl reloadData];
        }
        
    };
}

- (void)insertRowAtBottom {
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getMyGroupsAction:groups_limit forFilter:@"" withOffset:[NSString stringWithFormat:@"%ld",(long)arrMesGroupes.count]];
    
    __weak typeof(self) wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            
            return ;
        }
        NSArray*arrGroups = response[@"groups"];
        
        if (arrGroups.count > 0) {

            for (NSDictionary*dic in response[@"groups"] ) {
                if ( [dic[@"id"] intValue] == [[GroupCreateOBJ sharedInstance].group_id intValue]) {
                    continue;
                }
                
                [arrMesGroupes addObject:dic];
            }
            
            [wself.tableControl reloadData];
        }
        
    };
}
#pragma mark -  TableView Delegates
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary*dic = arrMesGroupes[indexPath.row];
    
    TypeCell71 *cell = (TypeCell71*)cellTmp;
    cell.parent = self;

    [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:dic[@"photo"] ] ];
    
    [cell.label1 setText:dic[@"name"]];
    
    int         nbSubscribers = [dic[@"nbSubscribers"] intValue];
    if (nbSubscribers ==1) {
        cell.label3.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        
    }
    else
    {
        cell.label3.text = [NSString stringWithFormat:@"%d %@s", nbSubscribers,str(strMMembre)];
        
    }
    [cell.label4 setText:dic[@"description"] ];
    
    cell.button1.tag=indexPath.row+1;
    [cell.button1 addTarget:self action:@selector(MemberButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.button1 setTitle:str(strMEMBERS) forState:UIControlStateNormal];
    
    cell.button2.tag=indexPath.row+2;
    [cell.button2 addTarget:self action:@selector(fnInviteGroupes:) forControlEvents:UIControlEventTouchUpInside];
    //    [cell.btnInviteGroup setTitle:NSLocalizedString(@"Members", @"") forState:UIControlStateNormal];
    if (indexPath.row ==  secIndex) {
        [cell fnShowMemberWithArray:tourMemberArray];
        cell.imgArrow.hidden = NO;
    }
    else
    {
        [cell fnShowMemberWithArray:nil];
        cell.imgArrow.hidden = YES;
    }
    [cell fnSettingCell:UI_GROUP_MUR_NORMAL expectTarget:ISGROUP];
    if (dic[@"sendAll"]) {
        [cell.button2 setSelected:YES];
        cell.button2.backgroundColor = [UIColor lightGrayColor];
        [cell.button2 setTitle:str(strEnvoyer) forState:UIControlStateNormal];
    }
    else
    {
        [cell.button2 setTitle:str(strENVOYERATOUS) forState:UIControlStateNormal];
        [cell.button2 setSelected:NO];
        cell.button2.backgroundColor =UIColorFromRGB(GROUP_TINY_BAR_COLOR);
        
    }
    cell.strID =[GroupCreateOBJ sharedInstance].group_id;
    [cell layoutIfNeeded];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

}
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return [arrMesGroupes count];
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    int noOfRows = 0;
//    
//    if(section == secIndex)
//        noOfRows = (int)[tourMemberArray count];
    
    return [arrMesGroupes count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    
    [self configureCell:self.prototypeCell cellForRowAtIndexPath:indexPath];
    
    
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TypeCell71 *cell = (TypeCell71 *)[self.tableControl dequeueReusableCellWithIdentifier:Cell_Step5_ID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
}

#pragma mark - MemberAction
-(IBAction)MemberButtonAction:(UIButton *)sender {
    __weak typeof(self) wself = self;
    NSInteger index =  [sender tag] -1;
    if (secIndex == index) {
        
        secIndex = -1;
        [tourMemberArray removeAllObjects];
        [self.tableControl reloadData];
        
    }else{
        secIndex = (int)index;
        NSMutableArray *arrData =[NSMutableArray new];
        arrData =[arrMesGroupes copy];
        NSString *strLoungeID=arrData[index][@"id"];
        [COMMON addLoading:self];
        WebServiceAPI *loungeSubscribeAction = [[WebServiceAPI alloc]init];
        [loungeSubscribeAction getGroupToutesSubscribeAction:strLoungeID];
        loungeSubscribeAction.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if ([wself fnCheckResponse:response]) return ;
            if (!response) {
                return ;
            }
            NSMutableArray *arrMembers =[NSMutableArray new];
            [arrMembers addObjectsFromArray:[response objectForKey:@"subscribers"]];
            
            for (int i=0; i<arrMembers.count; i++) {
                NSDictionary *dic =[arrMembers objectAtIndex:i];
                if ([[dic[@"user"][@"id"] stringValue] isEqualToString:[COMMON getUserId]]) {
                    [arrMembers removeObjectAtIndex:i];
                }
            }
            if (arrMembers.count>0) {
                [tourMemberArray removeAllObjects];
                [tourMemberArray addObjectsFromArray:arrMembers];
                [self.tableControl  reloadData];
                //            [self showPopupMembers:arrMembers];
            }
        };
    }
    
}

-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    AlertViewMembersInvite *viewController1 = [[AlertViewMembersInvite alloc]initWithArrayMembers:arrMembers wihtID:[GroupCreateOBJ sharedInstance].group_id];
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         // khi click thi invite members
     }];
    viewController1.expectTarget = ISGROUP;
    CCMPopupTransitioning *popup = [CCMPopupTransitioning sharedInstance];
    popup.destinationBounds = CGRectMake(0, 0, 300, 460);
    popup.presentedController = viewController1;
    popup.presentingController = self;
    [self presentViewController:viewController1 animated:YES completion:nil];
}

-(IBAction)fnInviteGroupes:(UIButton*)sender
{
    int index = (int) sender.tag-2;
    NSDictionary*dic = arrMesGroupes[index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj postGroupInviteOtherGroup: [GroupCreateOBJ sharedInstance].group_id otherGroup:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            [KSToastView ks_showToast:NSLocalizedString(@"INVALID_ERROR", @"")  duration:2.0f completion: ^{
            }];

            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            
            return ;
        }
        if (index<arrMesGroupes.count) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrMesGroupes[index]];
            [dic setValue:@1 forKey:@"sendAll"];
            [arrMesGroupes replaceObjectAtIndex:index withObject:dic];
            [self.tableControl reloadData];
        }
    };
}

@end
