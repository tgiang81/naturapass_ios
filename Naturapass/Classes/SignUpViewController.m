//
//  SignUpViewController.m
//  Naturapass
//
//  Created by ocsdev7 on 29/11/13.
//  Copyright (c) 2013 OCSMobi - 11. All rights reserved.
//

#import "SignUpViewController.h"
#import "AppCommon.h"
#import "Config.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "WebServiceAPI.h"
#import "Base64.h"
#import "AvatarViewController.h"
#import "PhotoSheetSignUpVC.h"
#import "SignUpNavigation.h"
#import "AZNotification.h"
@class AbstractActionSheetPicker;

@interface SignUpViewController ()<UITextFieldDelegate,UIActionSheetDelegate,WebServiceAPIDelegate,UIActionSheetDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    
    
    IBOutlet UILabel *labelTitle;
    IBOutlet UILabel *labelBottomText;
    IBOutlet UILabel *labelErrorText;
    
    IBOutlet UIImageView *imgviewProfile;
    
    IBOutlet UITextField *textFieldCivility;
    IBOutlet UITextField *textFieldName;
    IBOutlet UITextField *textFieldFirstname;
    IBOutlet UITextField *textFieldEmail;
    IBOutlet UITextField *textFieldPassword;
    IBOutlet UIButton *btnModify;
    IBOutlet UIButton *btnRegister;
    
//    IBOutlet UIView *viewProfileOption;
    IBOutlet UIView *viewActionsheetView;
    IBOutlet UILabel *labelChoosePhoto;
    
    NSArray        *civiliteArray;
    NSInteger selectedGenderIndex;
    
    UIImagePickerController *imagePicker;
    
    
    UITextField *currentTfd;
    SignUpNavigation *navigation;
}

@end

@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.hidesBackButton = YES;
    [self configureUIElement];
    [self InitializeKeyboardToolBar];
}

- (void)configureUIElement{
    
    textFieldCivility.inputView = self.viewPickerBackground;
    
    [self.viewPickerBackground removeFromSuperview];
    self.pickerView.delegate=self;
    
    civiliteArray = [[NSArray alloc] initWithObjects:str(strCivilite0),str(strCivilite1),str(strCivilite2), nil];
    
    [textFieldEmail     setText:@""];
    [textFieldName      setText:@""];
    [textFieldFirstname setText:@""];
    [textFieldPassword  setText:@""];
    [textFieldCivility  setText:@""];
    
    [textFieldEmail     setPlaceholder:str(strEmail_Address)];
    [textFieldName      setPlaceholder:str(strNName)];
    [textFieldFirstname setPlaceholder:str(strFirstName)];
    [textFieldPassword  setPlaceholder:str(strPPassword)];
    [textFieldCivility  setPlaceholder:str(strChoose_Civility)];
    
    [labelTitle       setText:str(strRegister_Title)];
    [labelBottomText  setText:str(strRegister_Condition)];
    [labelChoosePhoto setText:str(strChoose_Photo)];
    
    [btnModify   setTitle:str(strChange) forState:UIControlStateNormal];
    [btnRegister setTitle:str(strNEXT) forState:UIControlStateNormal];
    
}
#pragma mark - Custom Navigation
- (void)loadNavigation{
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;   // iOS 7 specific
    navigation =[[SignUpNavigation alloc] initWithNibName:@"SignUpNavigation" bundle:nil];
    [navigation.imgLogo setImage:[UIImage imageNamed:strLogo]];

    [self.navigationController.navigationBar addSubview:navigation.view];
    
    //update frame
    CGRect recTmp =navigation.view.frame;
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    recTmp.size.width = screenWidth;
    navigation.view.frame = recTmp;
    [navigation.btnBack addTarget:self action:@selector(gotoBack) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadNavigation];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];

}
-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear: animated];
}
- (BOOL)validateEmailWithString:(NSString*)emailText
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailText];
}

- (BOOL)isValidData{
    
    if ([textFieldName.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strNName) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    if ([textFieldFirstname.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strFirstName) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    
    if ([textFieldEmail.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strEEmail) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    if(![self validateEmailWithString:textFieldEmail.text]){
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strINVALIDEMAIL),[str(strEMPTY) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
        
    }
    if(imageData == nil)
    {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strImage_Missing),[str(strEMPTY) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
        
    }
    if ([textFieldPassword.text isEqualToString:@""]) {
        [self showErrorView :[NSString stringWithFormat:@"%@ %@",str(strAlert_Message),[str(strPPassword) lowercaseString]] ];
        [COMMON removeProgressLoading];
        return NO;
    }
    if (pickerValue ==nil) {
        //oblige
       
            [self showErrorView :  [NSString stringWithFormat:str(strVeuillez_remplir_le_champs),@"Civilité"]  ];
            [COMMON removeProgressLoading];
            return NO;
        
//        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
//        [f setNumberStyle:NSNumberFormatterDecimalStyle];
//        NSNumber * myNumber = [f numberFromString:@"0"];
//
//        pickerValue=myNumber;
//        [COMMON removeProgressLoading];
        
//        return NO;
    }
    
    return YES;
}

#pragma mark-Picker View Delegates

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [civiliteArray objectAtIndex:row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [civiliteArray count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    textFieldCivility.text=[civiliteArray objectAtIndex:row];
    pickerValue=[NSNumber numberWithInteger:row];
}



#pragma mark-PickerDelegate

-(void)selectedElement:(NSString *)_selectedElement index:(int)_index{
    [textFieldCivility setText:_selectedElement];
}

  #pragma mark - Profile Image Picker

- (IBAction)profileImageOptions:(id)sender{
        PhotoSheetSignUpVC *vc = [[PhotoSheetSignUpVC alloc] initWithNibName:@"PhotoSheetSignUpVC" bundle:nil];
        [vc setMyCallback:^(NSDictionary*data){
            UIImage *image;
            image = data[@"image"];
            userImage = image;
            imgviewProfile.image = userImage;
            imageData = UIImageJPEGRepresentation(userImage, 0.5);
            strUserImage = [Base64 encode:imageData];
            strAvatarUserImage=data[@"name"];
            strAvatarCount =data[@"nameAvatar"];
            [btnModify   setTitle:str(strEMPTY) forState:UIControlStateNormal];
        }];
    
    [self presentViewController:vc animated:NO completion:^{}];
    [textFieldCivility resignFirstResponder];
    [textFieldName resignFirstResponder];
    [textFieldFirstname resignFirstResponder];
    [textFieldEmail resignFirstResponder];
    [textFieldPassword resignFirstResponder];

    
}
#pragma mark - Validate (Signup)
- (IBAction)signUp:(id)sender{
    
//    if(switchAcceptCGU.isOn) {
    
        [self loadURL];
        [self resignKeyboard];
//    }
//    else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:str(strERROR)
//                                                        message:str(strCGU_unchecked)
//                                                       delegate:self
//                                              cancelButtonTitle:str(strOK)
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    
}


- (void)showErrorView:(NSString *)strMessage{
    [AZNotification showNotificationWithTitle:strMessage controller:self notificationType:AZNotificationTypeError];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 10)
    {
        [self.registerDelegate loadUserValue:textFieldEmail.text pass:textFieldPassword.text];
    }
    [COMMON removeProgressLoading];
}
- (void)gotoBackView{
    [self gotoBack];
}


#pragma mark - Back

- (void)gotoBack{
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

-(void) InitializeKeyboardToolBar{
    
    //    // Date Picker
    if (keyboardToolbar == nil)
    {
        keyboardToolbar            = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 38.0f)];
        keyboardToolbar.barStyle   = UIBarStyleBlackTranslucent;
        
        previousBarItem = [[UIBarButtonItem alloc]  initWithTitle:str(strPREVIOUS)
                                                            style:UIBarButtonItemStyleBordered
                                                           target:self
                                                           action:@selector(previousField:)];
        
        nextBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strNEXT)
                                                            style:UIBarButtonItemStyleBordered
                                                           target:self
                                                           action:@selector(nextField:)];
        
        spaceBarItem    = [[UIBarButtonItem alloc]  initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                         target:nil
                                                                         action:nil];
        
        doneBarItem     = [[UIBarButtonItem alloc]  initWithTitle:str(strDONE)
                                                            style:UIBarButtonItemStyleDone
                                                           target:self
                                                           action:@selector(resignKeyboard)];
        
        [keyboardToolbar setItems:[NSArray     arrayWithObjects:
                                   previousBarItem,
                                   nextBarItem,
                                   spaceBarItem,
                                   doneBarItem, nil]];
        
        
    }

        [textFieldCivility setInputAccessoryView:keyboardToolbar];
    
    [textFieldName setInputAccessoryView:keyboardToolbar];
    [textFieldFirstname setInputAccessoryView:keyboardToolbar];
    [textFieldEmail setInputAccessoryView:keyboardToolbar];
    [textFieldPassword setInputAccessoryView:keyboardToolbar];
    
}
- (void)animateView:(NSUInteger)tag
{
    [self checkBarButton:tag];
    CGRect rect = self.view.frame;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    if (tag == 1) {
        rect.origin.y = -20.0f ;
    } else if (tag == 2){
        rect.origin.y =-55.0f ;
    }
    else if (tag ==3)
    {
        rect.origin.y =-130.0f ;
        
    }
    else if (tag ==4)
    {
        rect.origin.y=-280.0f;
    }
    [UIView commitAnimations];
}
- (void)resignKeyboard
{
    [textFieldCivility resignFirstResponder];
    [textFieldName resignFirstResponder];
    [textFieldFirstname resignFirstResponder];
    [textFieldEmail resignFirstResponder];
    [textFieldPassword resignFirstResponder];

}

- (void)previousField:(id)sender
{
    id firstResponder = [self getFirstResponder];
    NSUInteger tag = [firstResponder tag];
    
    NSUInteger previousTag = tag == 1 ? 1 : tag - 1;
    [self checkBarButton:previousTag];
    [self animateView:previousTag];
    UITextField *previousField = (UITextField *)[self.view viewWithTag:previousTag];
    [previousField becomeFirstResponder];
    [self checkSpecialFields:previousTag];
}

- (void)nextField:(id)sender
{
    id firstResponder = [self getFirstResponder];
    NSUInteger tag = [firstResponder tag];
    NSUInteger nextTag = tag == 4? 5 : tag + 1;
    [self checkBarButton:nextTag];
    [self animateView:nextTag];
    UITextField *nextField = (UITextField *)[self.view viewWithTag:nextTag];
    [nextField becomeFirstResponder];
    [self checkSpecialFields:nextTag];
}
- (void)checkSpecialFields:(NSUInteger)tag
{
    
}
- (void)checkBarButton:(NSUInteger)tag
{
    UIBarButtonItem *previousBarItem1 = (UIBarButtonItem *)[[keyboardToolbar items] objectAtIndex:0];
    UIBarButtonItem *nextBarItem1     = (UIBarButtonItem *)[[keyboardToolbar items] objectAtIndex:1];
    
    [previousBarItem1 setEnabled:tag == 1 ? NO : YES];
    [nextBarItem1     setEnabled:tag == 4 ? NO : YES];
}

- (id)getFirstResponder
{
    NSUInteger index = 0;
    while (index <= 5) {
        UITextField *textField = (UITextField *)[self.view viewWithTag:index];
        if ([textField isFirstResponder]) {
            return textField;
        }
        index++;
    }
    return 0;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == textFieldCivility) {
        self.pickerView.hidden = false;
    }
    return true;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textFieldCivility resignFirstResponder];
    [textFieldName resignFirstResponder];
    [textFieldFirstname resignFirstResponder];
    [textFieldEmail resignFirstResponder];
    [textFieldPassword resignFirstResponder];
    return YES;
}
- (IBAction)goToCGU:(id)sender {
    
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.naturapass.com/cgu"]];
}


//loadurl
- (void)loadURL{
    strPassword = [NSString stringWithFormat:@"%@%@",SHA1_KEY,textFieldPassword.text];
    
    NSDictionary *postDict;
    NSMutableArray *keys;
    NSMutableArray *objects;
    
    strAvatarCount = [[strAvatarCount componentsSeparatedByCharactersInSet:
                       [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                      componentsJoinedByString:@""];
    
    if([self isValidData])
    {
        if([strAvatarUserImage isEqualToString:@"AVATARIMAGE"]){
            
            
            keys = [NSMutableArray arrayWithObjects:@"user[courtesy]",
                    @"user[lastname]",
                    @"user[firstname]",
                    @"user[avatar]",
                    @"user[email]",
                    @"user[password]",
                    nil];
            
            objects = [NSMutableArray arrayWithObjects:pickerValue,
                       textFieldName.text,
                       textFieldFirstname.text,
                       strAvatarCount,
                       textFieldEmail.text,
                       [COMMON encrypt:strPassword], nil];
            
            
            postDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
            [COMMON addLoading:self];
            WebServiceAPI *serviceAPI =[WebServiceAPI new];
            [serviceAPI postAvatarAction:postDict];
            serviceAPI.onComplete =^(id response, int errCode)
            {
                [COMMON removeProgressLoading];
                if([COMMON isReachable])
                {
                    ASLog(@"response = %@",response);
                    NSString * userid=@"";
                    userid= [response valueForKey:@"user_id"];
                    if([userid isKindOfClass:[NSNumber class]]){
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRegistered_Successfully) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
                        alert.tag=10;
                        [alert show];
                    }
                    else
                    {
                        NSString *errorMsg=[response valueForKey:@"message"];
                        [self showErrorView:errorMsg];
                    }
                }
            };
            
        }
        else {
            
            keys = [NSMutableArray arrayWithObjects:@"user[courtesy]",
                    @"user[lastname]",
                    @"user[firstname]",
                    @"user[email]",
                    @"user[password]",
                    nil];
            
            objects = [NSMutableArray arrayWithObjects:pickerValue,
                       textFieldName.text,
                       textFieldFirstname.text,
                       textFieldEmail.text,
                       [COMMON encrypt:strPassword], nil];
            
            postDict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
            NSDictionary *attachment = @{@"kFileName": @"image.png",
                                         @"kFileData": imageData};
            [COMMON addLoading:self];
            WebServiceAPI *serviceAPI =[WebServiceAPI new];
            [serviceAPI postUserAction:postDict withAttachmentImage:attachment];
            serviceAPI.onComplete =^(id response, int errCode)
            {
                [COMMON removeProgressLoading];
                if([COMMON isReachable])
                {
                    ASLog(@"response = %@",response);
                    NSString * userid=@"";
                    userid= [response valueForKey:@"user_id"];
                    if([userid isKindOfClass:[NSNumber class]]){
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strRegistered_Successfully) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
                        alert.tag=10;
                        [alert show];
                    }
                    else
                    {
                        NSString *errorMsg=[response valueForKey:@"message"];
                        [self showErrorView:errorMsg];
                    }
                }
            };
        }
        
    }
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
