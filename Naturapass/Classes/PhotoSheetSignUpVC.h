//

//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^onDataBack)(NSDictionary*);

@interface PhotoSheetSignUpVC : UIViewController
@property (nonatomic, copy) onDataBack myCallback;
@property (nonatomic,assign) ISSCREEN expectTarget;

@end
