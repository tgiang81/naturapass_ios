//
//  SlideCustomCell.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 1/28/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface SlideCustomCell : UITableViewCell
{
    //label
    IBOutlet UILabel                        *slideTitleLabel;
    
    //button
    IBOutlet UIButton                       *tickButton;
    
    //image
    IBOutlet UIImageView                    *slideImage;
    IBOutlet UIImageView                    *arrowImage;
}

@property (nonatomic,retain)IBOutlet UIButton   *tickButton;
@property (nonatomic,retain)IBOutlet UILabel    *slideTitleLabel;
@property (nonatomic,retain)IBOutlet UIImageView    *slideImage;
@property (nonatomic,retain)IBOutlet UIImageView    *arrowImage;

-(void)setListLabel:(NSString *)_text;
-(void)setListImages:(NSString *)_text;

@end
