//
//  MediaCell+Color.h
//  Naturapass
//
//  Created by Manh on 8/12/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MediaCell.h"

@interface MediaCell (Color)
-(void)fnSetTheme:(UIColor*)color;
@end
