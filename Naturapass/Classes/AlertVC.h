//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "SAMTextView.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Define.h"
typedef void (^AlertVCCallback)(NSInteger index, NSString *strContent);
typedef void (^AlertVCColorCallback)(NSInteger index, NSString *strContent,NSString *strColor);

@interface AlertVC : UIView <UITableViewDelegate, UITableViewDataSource ,UICollectionViewDelegate, UICollectionViewDataSource,GMSMapViewDelegate, UITextFieldDelegate >
{
    IBOutlet UIView *subAlertView;
    
    IBOutlet UIView *subAlertView2;
    IBOutlet UIView *containSubView;

    NSString *strTitle;
    NSString *strDescriptions;
    __weak IBOutlet SAMTextView *tvInputMessage;
    __weak IBOutlet UILabel *lbTitleLegend;
    __weak IBOutlet UILabel *lbTitleColor;
    __weak IBOutlet UIImageView *imgValider;
    __weak IBOutlet UIImageView *imgIconBg;

    int iZoomDefaultLevel;
    
    //Statistic Pedometer
    
    __unsafe_unretained IBOutlet UILabel *lbDistance;
    __unsafe_unretained IBOutlet UILabel *lbDenivele;
    __unsafe_unretained IBOutlet UILabel *lbDeniveleNegatif;
    __unsafe_unretained IBOutlet UILabel *lbVitesseMoyenne;
    
    __unsafe_unretained IBOutlet UIButton *btnPedometer;
    __unsafe_unretained IBOutlet UIButton *btnTerminal;
    __unsafe_unretained IBOutlet UIButton *btnContinue;

    __unsafe_unretained IBOutlet UILabel *lbTitlePedometer;
    __unsafe_unretained IBOutlet UILabel *lbTitleTerminal;

    //Confirm end recording
    __unsafe_unretained IBOutlet UIButton *btnEndRecording;
    __unsafe_unretained IBOutlet UIButton *btnCancel;

    __unsafe_unretained IBOutlet UIView *lineView;

}
@property (nonatomic,copy) AlertVCCallback callback;
@property (nonatomic,copy) AlertVCColorCallback callbackColor;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property(nonatomic,strong)  UIColor *colorNavigation;

@property (assign) int iTag;

-(instancetype)initAlertMap;
- (instancetype)initConfirmFavorisWithExpectTarget:(ISSCREEN)exp;
- (instancetype)initConfirmSentinelleWithExpectTarget:(ISSCREEN)exp color:(UIColor*)colorNav;
-(instancetype)initTermAndCondition;

-(instancetype)initMapType :(int)iType withColor:(UIColor*)colorNav;
-(instancetype)initVent :(NSDictionary*) location withColor:(UIColor*)colorNav;
-(instancetype)initMapMeteo :(NSDictionary*) location withColor:(UIColor*)colorNav;
-(void) fnSetChooseColor;
-(void)doBlock:(AlertVCCallback ) cb;
-(void)doBlockColor:(AlertVCColorCallback ) cb;

-(IBAction)closeAction:(id)sender;
-(void)showAlert;

-(instancetype)initChooseColor;

-(instancetype)initInputMessage;

- (instancetype)initAlert48h;
- (instancetype)initPedometerAlert:(NSDictionary*) inDic;
- (instancetype)initConfirmEndRecordingPedometerAlert;
- (instancetype)initStartTimeChangedStatisticAlert;
-(void)fnSetNameButtonPedometer:(NSString*)namePedometer wittNameButtonTerminal:(NSString*)nameTerminal withDesc:(NSString*)desc;


- (instancetype)initAlertSignalez;



-(instancetype)initWithTitle:(NSString*)stitle message:(NSString*)smessage cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDescriptions;
@property (nonatomic,strong) IBOutlet UIButton *cancelButton;
@property (nonatomic,strong) IBOutlet UIButton *otherButton;
@property (nonatomic,strong) IBOutlet UIButton *btnCreateLiveHunt;
@property (nonatomic,strong) IBOutlet UIButton *btnRejoinLiveHunt;

@property (nonatomic,strong) IBOutlet UILabel *lbCreateLiveHunt;
@property (nonatomic,strong) IBOutlet UILabel *lbRejoinLiveHunt;
@property (nonatomic,strong) IBOutlet UIButton *currentLocationButton;

@property (strong, nonatomic) IBOutlet  GMSMapView *mapView_;

-(void)fnSetexpectTarget:(ISSCREEN)exp withColor:(UIColor*)colorNav;
- (instancetype)initConfirmLiveHuntAlert;

- (instancetype)initAlertConfirmRecordNewOrContinueLastTime;

- (instancetype)initPedometerSTOP2 :(NSDictionary*) inDic;
- (instancetype)initPedometerSTOP :(NSDictionary*) inDic;
- (instancetype)initPedometerRECORDING :(NSDictionary*) inDic;
- (instancetype)initPedometerPAUSE :(NSDictionary*) inDic;
@end
