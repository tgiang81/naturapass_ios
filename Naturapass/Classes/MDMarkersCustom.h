//
//  MDMakersCustom.h
//  Naturapass
//
//  Created by Manh on 3/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"


typedef void (^callBackMarkersCustom) (UIImage *image,UIImage *image_nonLegend, float percent_arrow);
@interface MDMarkersCustom : UIView
{
    int sizeLegend;
    UILabel *lbMarker;
    UIImageView *imageViewMarker;
    float percent_arrow;
}
//@property (nonatomic,assign) BOOL isResize;
//@property (nonatomic,assign) NSInteger levelZoom;
//@property (nonatomic,assign) NSInteger heightMarker;

@property (nonatomic,strong) NSDictionary *dicMarker;
@property (nonatomic, copy) callBackMarkersCustom CallBackMarkers;
-(instancetype)initWithDictionary:(NSDictionary*)dicMarker;
- (void)doSetCalloutView;
@end
