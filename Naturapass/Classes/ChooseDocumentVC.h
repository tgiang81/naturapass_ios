//
//  ChooseDocumentVC.h
//  MDReadPDF
//
//  Created by Manh on 3/8/16.
//  Copyright © 2016 Manh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectoryWatcher.h"
#import "ParameterBaseVC.h"
typedef void (^documentCbk)(NSArray *arrListChoose);
@interface ChooseDocumentVC : ParameterBaseVC<DirectoryWatcherDelegate,UIDocumentInteractionControllerDelegate>
{
    IBOutlet  UICollectionView  *myCollection;
}
@property (nonatomic,copy) documentCbk callback;
-(void)doBlock:(documentCbk ) cb;

@end
