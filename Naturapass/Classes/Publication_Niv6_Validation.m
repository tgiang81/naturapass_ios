//
//  Publication_Niv6_Validation.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Niv6_Validation.h"
#import "Publication_Favoris_System.h"
#import "PublicationVC.h"

@interface Publication_Niv6_Validation ()
{
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UILabel *lbDescription3;
    IBOutlet UIButton *btnAJOUTERAUX;
    IBOutlet UIButton *btnTERMINER;



}
@end

@implementation Publication_Niv6_Validation

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.subview removeFromSuperview];
    [self addSubNav:nil];
    lbDescription1.text = str(strMessage14);
    lbDescription2.text = str(strVotre_publication_succes);
    lbDescription3.text = str(strVous_pouvez_suivre_son_avancement);
    [btnAJOUTERAUX setTitle:str(strAJOUTER_AUX_FAVORIS)  forState:UIControlStateNormal];
    [btnTERMINER setTitle:str(strTERMINER)  forState:UIControlStateNormal];


}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    Publication_Favoris_System *viewController1 = [[Publication_Favoris_System alloc] initWithNibName:@"Publication_Favoris_System" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    
}


-(IBAction)fnTerminal:(id)sender
{
    //push update
    
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
            
            [self.navigationController popToViewController:self.mParent animated:YES];
            
            return;
        }
    }
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
