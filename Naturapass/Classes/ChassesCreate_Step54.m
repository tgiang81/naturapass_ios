//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step54.h"
#import "ChassesCreate_Step6.h"

#import "CLTokenInputView.h"
#import "SAMTextView.h"
#import "APContact.h"
#import "APAddressBook.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ChassesCreate_Step5.h"
//tokenVS
#import "UserListViewController.h"
#import "VSTokenInputView.h"
#import "VSToken.h"
static CGFloat kMinInputViewHeight = 43;
static CGFloat kMaxInputViewHeight = 94;
static CGFloat kLeftPadding = 15;
static CGFloat kTopPadding = 10;
static CGFloat kRightPadding = 15;
static CGFloat kBottomPadding = 10;
static NSString *kEmailKey = @"email";
static NSString *kNameKey = @"name";
//

@interface ChassesCreate_Step54 ()<InputViewDelegate>
{
    IBOutlet UIButton *envoyer;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblDescription1;
}
@property (strong, nonatomic) IBOutlet SAMTextView *emailContent;

@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
//tokenVS
@property (weak, nonatomic) IBOutlet UILabel        *placehoderInputView;
@property (weak, nonatomic) IBOutlet VSTokenInputView *addedMembersInputView;
@property (weak, nonatomic) IBOutlet UIView *userListContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addedMemberViewHeight;
@property (nonatomic) UserListViewController *userListController;
@property (nonatomic) NSMutableArray *addedMembers;
@property (nonatomic) NSMutableDictionary *tokensCache;
//

@end

@implementation ChassesCreate_Step54
#pragma mark -  viewdid an init
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strInviterDesNonMembrer);
    lblDescription1.text = str(strPourLesPersonnesQui);
    [envoyer setTitle: str(strEnvoyer) forState:UIControlStateNormal];
    self.needChangeMessageAlert = YES;

    // Do any additional setup after loading the view from its nib.
    [self initialization];
    [envoyer setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [envoyer setTitle:str(strValider) forState:UIControlStateNormal ];
        
    }
    //tokenVS
    self.addedMembersInputView.inPutViewDelegate = self;
    self.userListContainerView.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    self.placehoderInputView.text = str(strTokenPlaceholder);
    //
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //tockenVS
    [self configureUserListViewController];
    
}
-(void)initialization
{
    self.emailContent.placeholder = str(strEmailContentPlaceholder);
    [self initialiseVariable];
    [self InitializeKeyboardToolBar];
    [self.emailContent setInputAccessoryView:self.keyboardToolbar];
    
}
- (void) initialiseVariable
{
}

#pragma mark - keyboard tool bar

- (void)resignKeyboard:(id)sender
{
    //tokenVS
    [self.view endEditing:YES];
    self.userListContainerView.hidden = YES;
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
#pragma mark - CLTokenInputViewDelegate


-(IBAction)fnSend:(id)sender
{
    //tokenVS
    NSString *listEmail =self.addedMembersInputView.text;
    if (listEmail.length==0) {
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:str(strAdresseEmailNonValide)
                                                          message:@""
                                                         delegate:self
                                                cancelButtonTitle:str(strOui)
                                                otherButtonTitles:nil];
        [alert show];
        return;
    }
    //IS SEND EMAIL
    [COMMON addLoading: self];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI postLoungeInvitationByEmailWithLounge: [ChassesCreateOBJ sharedInstance].strID subject:@"Invitation" toEmails:listEmail withContent:self.emailContent.text?self.emailContent.text:@"" ];
    serviceAPI.onComplete =^(NSDictionary *response, int errCode)
    {
        [COMMON removeProgressLoading];
        [self gotoback];
    };
    
}
//MARK: -tokenVS
- (NSMutableArray *)addedMembers
{
    if (!_addedMembers)
    {
        _addedMembers = [NSMutableArray array];
    }
    return _addedMembers;
}

- (NSDictionary *)tokensCache
{
    if (!_tokensCache)
    {
        _tokensCache = [NSMutableDictionary dictionary];
    }
    return _tokensCache;
}

- (void)configureUserListViewController
{
    if (!self.userListController)
    {
        self.userListController = [UserListViewController userListController];
        //        [self addChildViewController:self.userListController];
        self.userListController.view.frame = self.userListContainerView.bounds;
        [self.userListContainerView addSubview:self.userListController.view];
        //        [self.userListController didMoveToParentViewController:self];
    }
    
    __weak ChassesCreate_Step54 *weakSelf = self;
    self.userListController.didScrollBlock = ^{
        [weakSelf.view endEditing:YES];
        weakSelf.userListContainerView.hidden = YES;
    };
}
- (void)layoutViewsBasedOnComposerHeight
{
    self.addedMemberViewHeight.constant = kMinInputViewHeight;
    self.addedMembersInputView.textContainerInset = UIEdgeInsetsMake(kTopPadding, kLeftPadding, kBottomPadding, kRightPadding);
}
- (void)searchUsersForText:(NSString *)searchText
{
    self.userListContainerView.hidden = NO;
    __weak ChassesCreate_Step54 *weakSelf = self;
    
    [self.userListController searchUserForUserName:searchText addedUsers:self.addedMembers withSelectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        weakSelf.userListContainerView.hidden = YES;
        if (succes)
        {
            [weakSelf addUser:dicUser updateTokenView:YES];
        }
    } deselectedUserBlock:^(BOOL succes, NSDictionary *dicUser, NSError *error) {
        if (succes)
        {
            [weakSelf removeUser:dicUser];
        }
    }];
}
- (void)addUser:(NSDictionary *)dicUser updateTokenView:(BOOL)update
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (!token)
    {
        token = [[VSToken alloc] init];
        token.name      = dicUser[kNameKey];
        token.uniqueId  = dicUser[kEmailKey];
        token.sEmail    = dicUser[kEmailKey];
        
        [self.tokensCache setValue:token forKey:token.uniqueId];
    }
    token.timeStamp = [NSDate date];
    
    [self.addedMembers addObject:dicUser];
    [self.addedMembersInputView addToken:token needsLayout:update];
}

- (void)removeUser:(NSDictionary *)dicUser
{
    VSToken *token = self.tokensCache[dicUser[kEmailKey]];
    if (token)
    {
        [self.addedMembers removeObject:dicUser];
        [self.tokensCache removeObjectForKey:token.uniqueId];
        [self.addedMembersInputView removeToken:token needsLayout:YES];
    }
}

- (NSDictionary *)userForToken:(VSToken *)token
{
    NSDictionary *userForToken = nil;
    for (NSDictionary *dicUser in self.addedMembers)
    {
        if ([dicUser[kEmailKey] isEqualToString:token.uniqueId])
        {
            userForToken = dicUser;
            break;
        }
    }
    return userForToken;
}
#pragma mark InputViewDelegate
- (void)keyboardWillHide:(NSNotification*)notification {
    self.userListContainerView.hidden = YES;
}
- (void)textDidChange:(NSString *)text
{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    if ([text length])
    {
        [self performSelector:@selector(searchUsersForText:) withObject:text afterDelay:0.3];
    }
    else
    {
        [self.userListController resetUserListScreen];
    }
    if (self.addedMembersInputView.text.length == 0) {
        self.placehoderInputView.hidden = NO;
        
    }
    else
    {
        self.placehoderInputView.hidden = YES;
    }
}

- (void)didRemoveToken:(VSToken *)token
{
    NSDictionary *dicUser = [self userForToken:token];
    if (dicUser)
    {
        [self removeUser:dicUser];
        [self.userListController resetUserListScreen];
    }
}


- (void)didUpdateSize:(CGSize)size
{
    CGFloat height = [self correctedHeight:size.height];
    [UIView animateWithDuration:0.2 animations:^{
        self.addedMemberViewHeight.constant = height;
        [self.view layoutIfNeeded];
    }];
}

- (CGFloat)correctedHeight:(CGFloat)height
{
    CGFloat correctedHeight = 0;
    if (height < kMinInputViewHeight)
        correctedHeight = kMinInputViewHeight;
    else if (height > kMaxInputViewHeight)
        correctedHeight = kMaxInputViewHeight;
    else
        correctedHeight = height;
    
    return correctedHeight;
}

@end
