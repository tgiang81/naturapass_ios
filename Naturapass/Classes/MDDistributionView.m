//
//  DistributionView.m
//  Naturapass
//
//  Created by Giang on 6/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MDDistributionView.h"
#import "ASImageView.h"
#import "MyCollectionCell.h"

#import "MyTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <QuartzCore/QuartzCore.h>
#import "CommonHelper.h"
#import "NSString+HTML.h"
#import "Config.h"

#define  RADIUS 14
#define MOVE_DELTA 3
#define ICON_WIDTH  27
#define ICON_HEIGHT 27

static NSString *identifier = @"MyTableViewCell";

@interface MDDistributionView ()
{
    NSMutableArray *arrMutPartners, *arrLogoPartners,*arrFetchRight;
    float headerHeight;
    float heightFOOT;
    float widthRight;
    float headerCONTENT;

}
@property (nonatomic, strong)   NSMutableArray *arrPartners;
@property (nonatomic, strong)   NSMutableArray *arrFetchData;
@property (weak, nonatomic)     IBOutlet UITableView *myTableView;
@property (nonatomic, strong)   IBOutlet  UICollectionView  *myCollectionView;
@property (nonatomic, strong)   UIImageView *imageView;
@property (nonatomic, assign) BOOL isObserving;
@property (nonatomic, strong) MyTableViewCell *prototypeCell;

@end

@implementation MDDistributionView


- (instancetype) initForData: (NSDictionary *) dicDistrubtion {
    
    self = [super initWithFrame:CGRectMake(0, 0, 100, 100)];
    
    if (self)
    {
        self.publicationDictionary = dicDistrubtion;
        
        self.clipsToBounds = NO;
        headerHeight = 0.11f;
        widthRight = 0;
        
        arrMutPartners = [NSMutableArray new];
        arrLogoPartners = [NSMutableArray new];
        arrFetchRight =[NSMutableArray new];
        self.arrFetchData = [NSMutableArray new];
        self.arrPartners = [NSMutableArray new];
        [self doSetCalloutView];
        [self showCalloutView];
    }
    
    return self;
}

- (UIView *) backgroundView
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [backgroundView setBackgroundColor: [UIColor clearColor]];
    
    [backgroundView setOpaque: YES];
    return backgroundView;
}

-(UIImage*) fnResize :(UIImage*)img :(float)hHeight
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualWidth/actualHeight;
    
    float fixHeight = hHeight;
    
    imgRatio = fixHeight / actualHeight;
    actualWidth = imgRatio * actualWidth;
    actualHeight = fixHeight;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
     actualHeight = imgRatio * actualHeight;
     actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}

/*
 {
 "c_address" = "104 CHEMIN DU MAS DE CHEYLON";
 "c_brands" = "[{\"name\":\"Browning\",\"partner\":1,\"logo\":\"\\/uploads\\/brands\\/images\\/resize\\/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg\"}]";
 "c_city" = NIMES;
 "c_cp" = 30900;
 "c_email" = "michel.antoine@armurerie-francaise.com";
 "c_id" = 663;
 "c_lat" = "0.0";
 "c_logo" = "";
 "c_lon" = "0.0";
 "c_name" = APOLLO;
 "c_tel" = "04 48 06 01 33";
 "c_updated" = 1433238045;
 }
 */

-(void) loadImageData
{
    [self.arrFetchData removeAllObjects];
    
    NSDictionary *dic = self.publicationDictionary;
    //CALLOUT PARTNER
    
    [self.arrPartners removeAllObjects];
    
    NSData *brandData = [dic[@"c_brands"] dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSError *e;
    NSArray *brandArr = [NSJSONSerialization JSONObjectWithData:brandData options:NSJSONReadingMutableContainers error:&e];
    
    for (NSDictionary*obj in brandArr)
    {
        if ([obj[@"partner"] intValue] == 0)
        {
            //is brand
            [self.arrFetchData addObject:obj];
            //No partner -> only display icon
        }
        else if ([obj[@"partner"] intValue] == 1)
        {
            //Is partner:
            [self.arrPartners addObject:obj];
        }
        
    }
    //Download FetchData
    if (self.arrFetchData.count > 0)
    {
        SDWebImageManager *managerData = [SDWebImageManager sharedManager];
        __block int iCountData = (int) self.arrFetchData.count;
        [arrFetchRight removeAllObjects];
        for (NSDictionary*dicData in self.arrFetchData) {
            NSURL *urlData  = nil;
            urlData = [NSURL URLWithString: [ NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"c_logo"] ] ];
            [managerData loadImageWithURL:urlData options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                iCountData -= 1;
                if (image) {
                    UIImage *imgLogo = [self fnResize:image :widthBrand];
                    [arrFetchRight addObject:@{ @"image":imgLogo,
                                                @"name":dicData[@"name"]}];
                }
                if (iCountData == 0) {
                }

            }];

        }
    }
    //Download partners
    if (self.arrPartners.count > 0)
    {
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        __block int iCount = (int) self.arrPartners.count;
        
        
        [arrMutPartners removeAllObjects];
        [arrLogoPartners removeAllObjects];

        for (NSDictionary*dic in self.arrPartners) {
            NSURL *url  = nil;
            
            url = [NSURL URLWithString: [ NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"logo"] ] ];
            [manager loadImageWithURL:url options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
                
            } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, SDImageCacheType cacheType, BOOL finished, NSURL * _Nullable imageURL) {
                iCount -= 1;
                
                if (image) {
                    UIImage *imgLogo = [self fnResize:image :heightImageLogo];
                    
                    UIImage *imgCorrected = nil;
                    
                    if (imgLogo.size.width > widthCell) {
                        imgCorrected = [self fnResizeFixWidth:imgLogo :widthCell];
                    }
                    
                    [arrLogoPartners addObject:@{ @"image": (imgCorrected == nil ? imgLogo : imgCorrected) ,
                                                  @"name":dic[@"name"]}];
                    
                    UIImage *rsized = [self fnResize:image :heightImageInPop];
                    
                    
                    [arrMutPartners addObject:@{ @"image":rsized,
                                                 @"size": [NSNumber numberWithFloat:rsized.size.width]
                                                 }];
                }
                
                if (iCount == 0) {
                    //                                        NSLog(@"DONE");
                    //                                        [self fnCalcPopview];
                    
                    //FOOTER
                    
                    
                    {
                        self.vFoot = [[[NSBundle mainBundle] loadNibNamed:@"ViewFooter" owner:self options:nil] objectAtIndex:0] ;
                        
                        CGRect tmpFootRect = self.vFoot.frame;
                        
                        tmpFootRect.size.width = widthCell;
                        
                        self.vFoot.frame = tmpFootRect;
                        
                        [self.vFoot reloadWithData:arrLogoPartners withBlock:^(int iHeight) {
                            heightFOOT = iHeight;
                            
                            //refresh foot/ recal
                            [self calcLayout:widthCell+widthRight];
                            
                        }];
                    }
                    
                }

            }];
        }
    }

    //HEADER
    
    [[NSBundle mainBundle] loadNibNamed:@"ViewHead" owner:self options:nil];

    NSURL *url = nil;
    

    url = [NSURL URLWithString: [ NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API, dic[@"c_logo"] ] ];

    [vHead.imgV sd_setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //reload
        
        if (image.size.width > widthCell) {
            float actualHeight = image.size.height;
            float actualWidth = image.size.width;
            
            float imgRatio = actualWidth/actualHeight;
            
            float fixWidth = widthCell;
            
            imgRatio = fixWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = fixWidth;
            
            CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
            UIGraphicsBeginImageContext(rect.size);
            [image drawInRect:rect];
            UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            //reset image data
            vHead.imgV.image = returnImg;
        }
        
        
        //reload all...Header...Have header height
        headerHeight = vHead.imgV.image.size.height;
        CGRect tmp = CGRectMake(0, 0, widthCell, heightHeader);
        
        vHead.frame = tmp;
    }];
    
    
    //RIGHT
    
}

- (void)doSetCalloutView {
    
    [self loadImageData];

    //load view from nib
    self.calloutView = [[[NSBundle mainBundle] loadNibNamed:@"DistributionView" owner:self options:nil] objectAtIndex:0];
    
    [self.myCollectionView registerNib:[UINib nibWithNibName:@"MyCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"MyCollectionCell"];
    [self.myTableView registerNib:[UINib nibWithNibName:@"MyTableViewCell" bundle:nil] forCellReuseIdentifier:@"MyTableViewCell"];
    
    [self addSubview: self.calloutView];
    
    [self calcLayout:widthCell+widthRight];
}

-(void) calcLayout:(float)widthcell
{
    if (self.isObserving == NO) {
        [self.myCollectionView addObserver:self forKeyPath:@"contentSize" options:NSKeyValueObservingOptionOld context:NULL];
        
        self.isObserving = YES;
    }
    
    CGFloat totalHeight = headerHeight + headerCONTENT + heightFOOT ;//text blur
    
    //int numPartners = (int) self.arrPartners.count;
    
    self.calloutView.frame = CGRectMake(0, 0, widthcell , totalHeight);
    [self fnSetFrame];
    //reset pos
    [self positionSubviews];
    
    [self.myTableView reloadData];
    [self.myCollectionView reloadData];
}

-(void)fnSetFrame
{
    self.frame = self.calloutView.frame;
}
- (void)positionSubviews {
//    CGRect frame = self.calloutView.frame;
    
//    frame.origin.y =  ( frame.size.height - 15);// self.settings.calloutOffset; delta
//    frame.origin.x = (self.frame.size.width - widthCell/* table width<-frame.size.width*/) / 2.0;
    
//    self.calloutView.frame = frame;
//    [self fnSetFrame];
}

//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
//    
//    UITouch *touch = [touches anyObject];
//    
//    UIView * btnTmp = (UIView*)[pv viewWithTag:200];
//
//    
//    // toggle visibility
//    if (touch.view == pv || touch.view == btnTmp) {
//        if (self.calloutView.isHidden) {
//            //
//            //Push to hide all
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"hideAll" object:nil userInfo:nil];
//            //then show
//            [self showCalloutView];
//        } else {
//            [self hideCalloutView];
//        }
//    } else
//    if (touch.view == self.calloutView) {
////        [self showCalloutView];
//    } else {
//        [self hideCalloutView];
//    }
//}

//-(void) btnClick
//{
//    if (self.calloutView.isHidden) {
//        //
//        //Push to hide all
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"hideAll" object:nil userInfo:nil];
//        //then show
//        [self showCalloutView];
//    } else {
//        [self hideCalloutView];
//    }
//
//}

- (void)hideCalloutView {
    if (self.calloutView.isHidden == NO)
    {
        self.calloutView.hidden = YES;
        self.calloutView.alpha = 0;
    }
}

- (void)showCalloutView
{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ohmydistribution" object:self userInfo:nil];
        self.calloutView.alpha = 1;
        self.calloutView.hidden = NO;
        [self calcLayout:widthCell+ widthRight];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView == self)
        return nil;
    
    return hitView;
}

#pragma mark - UITableView

//------------------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  1;
}

//------------------------------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


//FOOTER
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (heightFOOT == 0) {
        return 1;
    }
    
    return heightFOOT;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{

    [self updateAllViews];
    
    return self.vFoot;
}

-(void) updateAllViews
{
    //reframe tableview
    CGRect rect = self.myTableView.frame;
    CGFloat totalHeight = headerHeight + headerCONTENT + heightFOOT ;//text blur
    rect.size.height=totalHeight;
    self.myTableView.frame=rect;
    
    
    CGRect rectCollection = self.myCollectionView.frame;
    rectCollection.size.height = rect.size.height;
    self.myCollectionView.frame = rectCollection;
    
    //callout
    CGRect tmp = self.calloutView.frame;
    tmp.size.width = rect.size.width + rectCollection.size.width;
    tmp.size.height = totalHeight;
    
    self.calloutView.frame = tmp;
    [self fnSetFrame];
    //reset pos
    [self positionSubviews];
    
}
//HEADER
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return headerHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return vHead;
}

//CELL CONTENT
//------------------------------------------------------------------------------------------------------------------------
- (MyTableViewCell *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.myTableView dequeueReusableCellWithIdentifier:identifier];
    }
    return _prototypeCell;
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[MyTableViewCell class]])
    {
        MyTableViewCell *cell = (MyTableViewCell *)cellTmp;
        
        cell.backgroundColor=[UIColor whiteColor];
        NSDictionary *dic = self.publicationDictionary;
        
        cell.name.text = [dic[@"c_name"] stringByDecodingHTMLEntities];
        cell.address.text = [dic[@"c_address"] stringByDecodingHTMLEntities];
        
        cell.city.text=  [NSString stringWithFormat:@"%@ %@", [dic[@"c_cp"] stringByDecodingHTMLEntities] , [dic[@"c_city"] stringByDecodingHTMLEntities]];
        
        cell.tel.text = dic[@"c_tel"];
        cell.email.text = dic[@"c_email"];
        [cell layoutIfNeeded];

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    headerCONTENT = size.height+1;
    return size.height+1;
}

/*
 [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
 @"c_name":[set_querry stringForColumn:@"c_name"],
 @"c_address":[set_querry stringForColumn:@"c_address"],
 @"c_cp":[set_querry stringForColumn:@"c_cp"],
 @"c_city":[set_querry stringForColumn:@"c_city"],
 @"c_tel":[set_querry stringForColumn:@"c_tel"],
 @"c_email":[set_querry stringForColumn:@"c_email"],
 @"c_lat":[set_querry stringForColumn:@"c_lat"],
 @"c_lon":[set_querry stringForColumn:@"c_lon"],
 @"c_brands":[set_querry stringForColumn:@"c_brands"],
 @"c_updated":[set_querry stringForColumn:@"c_updated"],
 @"c_logo":[set_querry stringForColumn:@"c_logo"],
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyTableViewCell *cell = (MyTableViewCell *)[self.myTableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

//RIGHT PANEL
#pragma mark - UICollectionView
//-------------------------------------------------------------------------------------------------------------------------------
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrFetchRight.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = @"MyCollectionCell";
    
    MyCollectionCell *cell = (MyCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor whiteColor];
    NSDictionary *dic = arrFetchRight[indexPath.row];
    cell.imgBrand.image = dic[@"image"];
       return cell;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}
//------------------------------------------------------------------------------------------------------------------------

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0,0,0,0);
}
//------------------------------------------------------------------------------------------------------------------------

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = arrFetchRight[indexPath.row];
    UIImage *data = dic[@"image"];
    return data.size;
}

#pragma mark-
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary  *)change context:(void *)context
{
    if([keyPath isEqualToString:@"contentSize"]) {
        float newWidth = self.myCollectionView.collectionViewLayout.collectionViewContentSize.width;

        if (newWidth > 0) {
            self.isObserving = NO;
            widthRight = newWidth;
            
            [self.myCollectionView removeObserver:self forKeyPath:@"contentSize" context:NULL];
            //Whatever you do here when the reloadData finished
            CGRect rect= self.myCollectionView.frame;
            rect.size.width=newWidth;
            self.myCollectionView.frame=rect;
            //
            [self updateAllViews];
        }
    }

}

-(void)dealloc
{
    [self.vFoot releaseObserver];
    
    if (self.isObserving) {
        [self.myCollectionView removeObserver:self forKeyPath:@"contentSize" context:NULL];
    }
}

@end
