//
//  MDMarkersDistribution.h
//  Naturapass
//
//  Created by Manh on 3/28/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//
typedef void (^callBackMarkers) (UIImage *image);

@interface MDMarkersDistribution : UIView
{
    UIView *pv;
    
}
@property (nonatomic) BOOL hidesArrow;
@property (nonatomic, copy) callBackMarkers CallBackMarkers;

@property (nonatomic, strong) UIView *contentView;
@property (nonatomic,strong) NSDictionary *dicMarker;
- (void)doSetCalloutView;
-(instancetype)initWithDictionary:(NSDictionary*)dicMarker;
@end
