//
//  PublicationBaseVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PublicationBaseVC.h"
#import "NotificationVC.h"
#import "SubNavigationPRE_ANNULER.h"
#import "AlertVC.h"
#import "PublicationVC.h"
#import "ChassesCreate_Step1.h"
@interface PublicationBaseVC ()

@end

@implementation PublicationBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Signaler from Live hunt
    if (self.expectTarget == ISLIVE) {
        UINavigationBar *navBar=self.navigationController.navigationBar;
        [navBar setBarTintColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR) ];
        
        //add sub navigation
        [self addMainNav:@"MainNavSignaler"];
        
        //Set title
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myTitle setText:@"SIGNALER QUOI ?"];
        subview.backgroundColor = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
        
        [self addSubNav:nil];
        self.vContainer.backgroundColor = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
    }
    // Signaler from mur
    else {
        UINavigationBar *navBar=self.navigationController.navigationBar;
        [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
        
        //add sub navigation
        [self addMainNav:@"MainNavMUR"];
        
        //Set title
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myTitle setText:str(strMUR)];
        subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        
        [self addSubNav:@"SubNavigationPRE_ANNULER"];
        
        //coloring btn
        
        SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
        
        UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
        UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
        
        btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
        
        btn2.backgroundColor = UIColorFromRGB(MUR_CANCEL);
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //go to favorite address
    if (self.expectTarget == ISLOUNGE) {
        UINavigationBar *navBar=self.navigationController.navigationBar;
        [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
        
        
        //Set title
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myTitle setText:str(strCHANTIERS)];
        subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        UIButton *btn1 = (UIButton*)[self.subview viewWithTag:START_SUB_NAV_TAG];
        
        btn1.backgroundColor = UIColorFromRGB(CHASSES_BACK);
        
    }
    
}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://HOME
        {
            [self gotoback];
        }
            break;
            
        case 1://Search
        {
            [self doRemoveObservation];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
            break;
            
        default:
            break;
    }
    
}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
        case 1://ANNULER
        {
            
            //ggtt check lai...
            
            
            NSString *str=@"";
            Class Cclass= nil;
            if (self.expectTarget == ISLOUNGE)
            {
                str = str(strAAgenda);
                Cclass= [ChassesCreate_Step1 class];
            }
            else
            {
                str =str(strPublication);
                Cclass= [PublicationVC class];
            }
            NSString *strmessage= @"";
            if ([PublicationOBJ sharedInstance].isEditer) {
                strmessage = [NSString stringWithFormat:str(strEtesVousSurDeVouloiranuilervotresaisie)];
            }
            else
            {
                strmessage = [NSString stringWithFormat:str(strMessage10),str];
            }
            AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@",str(strAAnnuler),str] message:strmessage cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
            
            [vc doBlock:^(NSInteger index, NSString *str) {
                if (index==0) {
                    // NON
                }
                else if(index==1)
                {
                    //OUI
                    NSArray * controllerArray = [[self navigationController] viewControllers];
                    
                    for (int i=0; i < controllerArray.count; i++) {
                        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                            
                            [self.navigationController popToViewController:self.mParent animated:YES];
                            
                            return;
                        }
                    }
                    [self doRemoveObservation];
                    
                    [self.navigationController popToRootViewControllerAnimated:YES];
                }
            }];
            [vc showAlert];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)onNext:(id)sender {
}

@end
