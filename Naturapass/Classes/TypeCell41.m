//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCell41.h"
#import "CommonHelper.h"

@implementation TypeCell41
- (void)awakeFromNib {
    [super awakeFromNib];

    [COMMON listSubviewsOfView:self];
    [[CommonHelper sharedInstance] setRoundedView:self.img1 toDiameter:self.img1.frame.size.height /2];
    _constraint_control_width.constant =0;
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label1.preferredMaxLayoutWidth = CGRectGetWidth(self.label1.frame);
    self.label2.preferredMaxLayoutWidth = CGRectGetWidth(self.label2.frame);

}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [self.label2 setTextColor:color];
}
@end
