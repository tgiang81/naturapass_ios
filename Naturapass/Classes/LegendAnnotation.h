//
//  LegendAnnotation.h
//  Naturapass
//
//  Created by Giang on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MapViewAnnotation.h"

@interface LegendAnnotation :  NSObject <MKAnnotation>

@property (nonatomic, strong) NSDictionary *myDictionary;

- (id)initWithDic:(NSDictionary *)dic;

@end
