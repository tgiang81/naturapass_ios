//
//  Etape9_Agenda_Groupe.m
//  Naturapass
//
//  Created by Giang on 10/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Etape9_Agenda_Groupe.h"
#import "ChassesCreate_Step6.h"
#import "Etape9_Agenda_Groupe_selection.h"

@interface Etape9_Agenda_Groupe ()
{
    IBOutlet UILabel *lbTitle;
    IBOutlet UIButton *btnOui;
    IBOutlet UIButton *btnNon;
}
@end

@implementation Etape9_Agenda_Groupe

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strAjouterCetEvenementDansLagenda);
    [btnOui setTitle:  str(strOUI) forState:UIControlStateNormal];
    [btnNon setTitle:  str(strNON) forState:UIControlStateNormal];
    self.needChangeMessageAlert = YES;

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//insert

-(IBAction)NonAction:(id)sender
{
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self backEditListChasse];
    }
    else
    {
    ChassesCreate_Step6 *viewController1 = [[ChassesCreate_Step6 alloc] initWithNibName:@"ChassesCreate_Step6" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
};

-(IBAction)OUIAction:(id)sender
{
    Etape9_Agenda_Groupe_selection *viewController1 = [[Etape9_Agenda_Groupe_selection alloc] initWithNibName:@"Etape9_Agenda_Groupe_selection" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
};

@end
