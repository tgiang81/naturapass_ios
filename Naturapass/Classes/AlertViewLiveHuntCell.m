//
//  AlertViewLiveHuntCell.m
//  Naturapass
//
//  Created by JoJo on 12/18/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "AlertViewLiveHuntCell.h"

@implementation AlertViewLiveHuntCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgAvatar.layer.masksToBounds = YES;
    self.imgAvatar.layer.cornerRadius= 20;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
