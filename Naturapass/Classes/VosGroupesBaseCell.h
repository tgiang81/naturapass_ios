//
//  VosGroupesBaseCell.h
//  Naturapass
//
//  Created by JoJo on 11/22/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDRadioButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface VosGroupesBaseCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *lbTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgAvatar;
@property (nonatomic, strong) IBOutlet MDRadioButton *btnCheckbox;
@property (nonatomic, strong) IBOutlet UIButton *btnSelected;
@property (nonatomic, strong) IBOutlet UILabel *lbAccess;
@property (nonatomic, strong) IBOutlet UILabel *lbMember;
@property (nonatomic, strong) IBOutlet UIImageView *imgLineBottom;
@end

NS_ASSUME_NONNULL_END
