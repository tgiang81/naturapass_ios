//
//  GroupFromMapCell.h
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupFromMapCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UILabel *lbTitle;
@property(nonatomic,strong) IBOutlet UILabel *lbDescription;
@property(nonatomic,strong) IBOutlet UIImageView *imgIcon;
@property(nonatomic,strong) IBOutlet UIButton *btnEnter;
@end
