//
//  GroupCreateBaseVC.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "GroupCreateOBJ.h"

@interface GroupCreateBaseVC : BaseVC

@property (assign) BOOL needChangeMessageAlert;

@property (nonatomic,assign) GROUP_CREATE_ADD_VIEW myTypeView;

@property (nonatomic,strong) IBOutlet UIButton *btnSuivant;
- (IBAction)onNext:(id)sender;
@end
