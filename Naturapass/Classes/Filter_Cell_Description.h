//
//  Filter_Cell_Description.h
//  Naturapass
//
//  Created by manh on 11/10/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Filter_Cell_Description : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbDescription;

@end
