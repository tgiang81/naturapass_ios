//
//  AmisAddVC.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisBaseVC.h"

@interface AmisAddVC : AmisBaseVC
@property (strong, nonatomic) NSMutableArray *filteredContacts;
@property (nonatomic,retain) NSMutableArray *listEmails;

@end
