//
//  Amis_Demand_Invitation.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Amis_Demand_Invitation.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SubNavigationAMIS.h"
#import "FriendInfoVC.h"

#import "CellKind6.h"
#import "CommonObj.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Amis_Demand_Invitation ()
{
    NSMutableArray          *arrayFriendsList;
    IBOutlet UILabel *lbInvitaion;
    IBOutlet UILabel *lbZeroInviationAmis;

}
@end

@implementation Amis_Demand_Invitation

- (void)viewDidLoad {
    [super viewDidLoad];
    lbInvitaion.text = str(strINVITATIONSAMIS);
    lbZeroInviationAmis.text = str(strZeroInvitaionAmis);
    arrayFriendsList = [[NSMutableArray alloc] init];

    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind6" bundle:nil] forCellReuseIdentifier:identifierSection1];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getWaitingInvitation];

}
-(void)checkEmtry:(int)count
{
    if (count>0) {
        lbZeroInviationAmis.hidden = YES;
        
    }
    else
    {
        lbZeroInviationAmis.hidden = NO;
        
    }
}
-(void) getWaitingInvitation
{
    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    UIButton *btnCheck = (UIButton*)[self.subview viewWithTag: 2*( START_SUB_NAV_TAG + 3)];
    [self.subview badgingBtn:btnCheck count:[CommonObj sharedInstance].nbUserWaiting];

    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];

    [serviceObj getUserWaitingAction];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        [COMMON removeProgressLoading];
        
        if (response) {
            if ([response[@"users"] isKindOfClass: [NSArray class]]) {
                NSArray *arr = response[@"users"];
                [CommonObj sharedInstance].nbUserWaiting = (int)arr.count;
                [self.subview badgingBtn:btnCheck count:[CommonObj sharedInstance].nbUserWaiting];
                
                [arrayFriendsList removeAllObjects];
                [arrayFriendsList addObjectsFromArray:response[@"users"]];
                [self.tableControl reloadData];
                [self checkEmtry: (int)arrayFriendsList.count];

            }
        }
        
        
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayFriendsList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind6 *cell = nil;
    
    cell = (CellKind6 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrayFriendsList[indexPath.row];
    
    NSString *strName=[NSString stringWithFormat:@"%@ %@", dic[@"firstname"],dic[@"lastname"]];
    [cell.label1 setText:strName];
    cell.label1.numberOfLines=1;

    NSString *imgUrl;
    //profilepicture
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }

    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];

    [cell.btnValid addTarget:self action:@selector(addFriend:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnRefuse addTarget:self action:@selector(subFriend:) forControlEvents:UIControlEventTouchUpInside];

    [cell.btnProfile addTarget:self action:@selector(viewProfile:) forControlEvents:UIControlEventTouchUpInside];

    
    cell.btnValid.tag = indexPath.row +1;
    cell.btnRefuse.tag = indexPath.row +2;
    cell.btnProfile.tag = indexPath.row +3;

    //FONT
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


/*
 https://naturapass.e-conception.fr/api/v2/users/205/friendship DELETE
 {"relation":{"mutualFriends":0,"friendship":false}}
 
 https://naturapass.e-conception.fr/api/v2/users/206/friendship PUT
 
 {"relation":{"mutualFriends":0,"friendship":{"state":2,"way":3}}}
 */


-(void) viewProfile :(UIButton*)sender
{
    int idex = (int)sender.tag - 3;
    NSDictionary *dic = arrayFriendsList[idex];
    
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic: dic];
    
    [self pushVC:friend animate:YES expectTarget:ISMUR];

}

-(void) addFriend :(UIButton*)sender
{
    NSDictionary *dic = arrayFriendsList[sender.tag - 1];
    /*
     {
     friendship =     {
     state = 2;
     way = 3;
     };
     }
     
     */
    [COMMON addLoading:self];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj putUserFriendshipConfirmAction:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if ([response isKindOfClass : [NSDictionary class] ]) {
            if ([response[@"relation"][@"friendship"] isKindOfClass: [NSDictionary class]] ) {
                //OK friend
                //Alert user are friend.
                
                [self getWaitingInvitation];
            }
        }
        [self.tableControl reloadData];
    };
    
}

-(void) subFriend :(UIButton*)sender
{
    [COMMON addLoading:self];
    NSDictionary *dic = arrayFriendsList[sender.tag - 2];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj delUserFriendshipConfirmAction:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                [COMMON removeProgressLoading];
        if ([response isKindOfClass : [NSDictionary class] ]) {
            if (![response[@"relation"][@"friendship"] isKindOfClass: [NSDictionary class]]) {
                //OK refuse this friend
                //Alert user are friend.
                
                [self getWaitingInvitation];
            }
        }
        [self.tableControl reloadData];
    };
}


@end
