//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
#import "WebServiceAPI.h"
typedef void (^AlertSearchPersonneCallback)(NSArray *arrPersonne, float count);
@interface AlertSearchPersonneVC : UIView<UITextFieldDelegate>
{
    NSString *strTitle;
    NSString *strDescriptions;
    WebServiceAPI *serviceAPI;
    NSMutableArray * arrDataSelected;
    NSMutableArray * arrData;

}
@property (nonatomic,copy) AlertSearchPersonneCallback callback;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (nonatomic, assign) BOOL shouldBeginEditing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTable_Y;

-(void)doBlock:(AlertSearchPersonneCallback ) cb;
-(instancetype)init;
-(void)reloadData:(NSArray*)arrNodes;
- (void) processLoungesSearchingURL:(NSString *)searchText;
-(void)fnHide:(BOOL)hide;
-(void)addContraintSupview:(UIView*)viewSuper;
@end
