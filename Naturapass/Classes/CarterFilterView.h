//
//  CarterFilterView.h
//  Naturapass
//
//  Created by Manh on 9/23/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "BaseVC.h"
#import "CarterFilterAddView.h"

typedef void (^FilterViewCallback)(NSString *strSearch);
@interface CarterFilterView : UIView

@property (nonatomic,copy) FilterViewCallback callback;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;
@property (weak, nonatomic) IBOutlet UILabel *lbTitleScreen;
@property (weak, nonatomic) IBOutlet UITextField *tfMotCle;
@property (nonatomic,assign)  BOOL showPopoutFilter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintLeading;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintHeaderViewSearch;
@property (weak, nonatomic) IBOutlet UIView *vViewSearch;

@property (weak, nonatomic) IBOutlet UIButton *btnValider;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIView *vContent;
@property (weak, nonatomic) IBOutlet UIView *vValiderAll;
@property (nonatomic,strong) CarterFilterAddView* viewValiderAdd;
@property (nonatomic, strong) NSString *myC_Search_Text;
@property(nonatomic,strong)  UIColor *colorNavigation;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic, strong) BaseVC *parentVC;
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt;
-(void)addContraintSupview:(UIView*)viewSuper;
-(void)hide:(BOOL)hidden;
-(void)setup;
-(void)fnGetDataWithStringSearch:(NSString*)strSearch;
@end
