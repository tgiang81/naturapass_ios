//
//  AgendaFromMap
//  Naturapass
//
//  Created by Giang on 12/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AgendaFromMap.h"
#import "CellKind22.h"
#import "MapLocationVC.h"
#import "AlertMapVC.h"
#import "NSDate+Extensions.h"

#import "GroupEnterMurVC.h"
#import "ChassesParticipeVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+HTML.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface AgendaFromMap ()
{
    NSString                    *numberOption;
    NSString *str_Owner;
    IBOutlet UILabel *lbStartDate;
    IBOutlet UILabel *lbStartTime;
    IBOutlet UILabel *lbEndDate;
    IBOutlet UILabel *lbEndTime;
    IBOutlet UILabel *lbAddress;
    IBOutlet UILabel *lbLat;
    IBOutlet UILabel *lbLong;
    IBOutlet UILabel *lblDebut;
    IBOutlet UILabel *lblFin;
    IBOutlet UILabel *lblRendez;
    IBOutlet UIButton *btnSURNATURA;
    IBOutlet UIButton *btnAUTRESGPS;
    
    __weak IBOutlet UILabel *lbNameHunt;
    __weak IBOutlet UILabel *lbDescription;
    
    __weak IBOutlet UILabel *lbAccess;
    __weak IBOutlet UIImageView *logo;
}
@end

@implementation AgendaFromMap

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavDetailMap"];
    MainNavigationBaseView *subview =  [self getSubMainView];
    //Change background color
    //SUB
    [self addSubNav:nil];
    
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                subview.backgroundColor = UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR);
                
            }else{
                //Change status bar color
                subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            }
            
        }
            break;
        case ISLOUNGE:
        {
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        case ISMUR:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        default:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
    }
    // Do any additional setup after loading the view from its nib.
    lblDebut.text = str(strDEBUTLE);
    lblFin.text = str(strFINLE);
    lblRendez.text = str(strRendezVousA);
    
    logo.layer.cornerRadius = logo.frame.size.height /2;
    logo.layer.masksToBounds = YES;
    logo.layer.borderWidth = 0;
    
    NSString* strPath  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,_dicChassesItem[@"c_photo"]];
    
    [logo sd_setImageWithURL: [NSURL URLWithString: strPath ] placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
    }];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind22" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    //      = _dicChassesItem[@"c_subscriber"];  access type HARD CODE testing
    
    //    @"c_subscribers": [NSNumber numberWithInt: [set_querry intForColumn:@"c_subscribers"]],
    //    @"c_photo":[set_querry stringForColumn:@"c_photo"],
    
    int iSubScribe = [_dicChassesItem[@"c_subscribers"] intValue];
    
    switch (iSubScribe)
    {
        case USER_INVITED:
        {
            [btnSURNATURA setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
            [btnSURNATURA setEnabled:YES];
            [btnSURNATURA addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
        case USER_WAITING_APPROVE:
        {
            [btnSURNATURA setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
            [btnSURNATURA setEnabled:NO];
        }
            break;
            
        case USER_NORMAL:
        case USER_ADMIN:
        {
            [btnSURNATURA setTitle:str(strACCEDER) forState:UIControlStateNormal];
            [btnSURNATURA setEnabled:YES];
            [btnSURNATURA addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
        case USER_REJOINDRE:
        {
            [btnSURNATURA setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
            [btnSURNATURA setEnabled:YES];
            [btnSURNATURA addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
        }
            break;
        default:
            break;
    }
    
    
    //
    numberOption =@"-1";
    
    //
    numberOption = _dicChassesItem[@"c_nb_participant"];
    
    lbNameHunt.text = [_dicChassesItem[@"c_name"] stringByDecodingHTMLEntities];
    
    //
    int iAccess = [_dicChassesItem[@"c_access"] intValue];
    
    
    if (iAccess == 0)
        lbAccess.text=str(strAccessPrivate);
    else if(iAccess == 1)
        lbAccess.text=str(strAccessSemiPrivate);
    else if(iAccess == 2)
        lbAccess.text=str(strAccessPublic);
    
    lbDescription.text = [_dicChassesItem[@"c_description"] stringByDecodingHTMLEntities];
    
    NSTimeInterval timeMeetingDate =  [_dicChassesItem[@"c_start_date"] integerValue];
    NSString *strMeetingDate = [NSDate convertTimeIntervalToNSTring:timeMeetingDate];
    
    NSTimeInterval timeEndDate =  [_dicChassesItem[@"c_end_date"] integerValue];
    NSString *strEndDate = [NSDate convertTimeIntervalToNSTring:timeEndDate];
    
    lbStartDate.text=@"";
    lbStartTime.text=@"";
    lbAddress.text =@"";
    lbLat.text =@"";
    lbLong.text=@"";
    //START DATE
    
    if (strMeetingDate.length>0) {
        lbStartDate.text= [strMeetingDate substringWithRange:NSMakeRange(0, 10)];
        lbStartTime.text= [NSString stringWithFormat:@"à %@",[strMeetingDate substringWithRange:NSMakeRange(11, 5)]];
        
    }
    //END DATE
    if (strEndDate.length>0) {
        lbEndDate.text= [strEndDate substringWithRange:NSMakeRange(0, 10)];
        lbEndTime.text= [NSString stringWithFormat:@"à %@",[strEndDate substringWithRange:NSMakeRange(11, 5)]];
        
    }
    
    //ADDRESS
    lbAddress.text = [_dicChassesItem[@"c_meeting_address"] stringByDecodingHTMLEntities];
    lbLat.text =[ NSString stringWithFormat:@"Lat. %.5f" , [_dicChassesItem[@"c_meeting_lat"] floatValue]];
    lbLong.text = [ NSString stringWithFormat:@"Lng. %.5f" , [_dicChassesItem[@"c_meeting_lon"] floatValue]];
    
}

-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://back
        {
            
            [self.navigationController popViewControllerAnimated:YES];
        }
            break;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
                
            }else{
                //Change status bar color
                [navBar setBarTintColor: UIColorFromRGB(CARTE_MAIN_BAR_COLOR) ];
            }
            
        }
            break;
        case ISLOUNGE:
        {
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_MAIN_BAR_COLOR) ];
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_MAIN_BAR_COLOR) ];
            
        }
            break;
        case ISMUR:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
            
        }
            break;
        default:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
        }
            break;
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    
    NSDateFormatter *outputFormatterBis = [[ASSharedTimeFormatter sharedFormatter] dateFormatterTer];
    
    [ASSharedTimeFormatter checkFormatString: @"yyyy-MM-dd'T'HH:mm:ssZZZZ" forFormatter: inputFormatter];
    
    [ASSharedTimeFormatter checkFormatString: @"dd/MM/yyyy HH:mm" forFormatter: outputFormatterBis];
    
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}

#pragma mark - API

-(IBAction)NATURAAction:(id)sender
{
    //ACCESS page??? or ....that depend on user type
    
    //        NSDictionary *local = (NSDictionary*)dic;
    //
    //        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
    //        viewController1.isSubVC = YES;
    //        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
    //                                      @"longitude":  local[@"longitude"]};
    //
    //        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

-(IBAction)GPSAction:(id)sender
{
    //ROUTE from Current location to AGENDA position.
    
    NSString *strLat =@"";
    NSString *strLng =@"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    double lat = appDelegate.locationManager.location.coordinate.latitude;
    double lng = appDelegate.locationManager.location.coordinate.longitude;
    
    strLat = _dicChassesItem[@"c_meeting_lat"];
    strLng = _dicChassesItem[@"c_meeting_lon"];
    
    AlertMapVC *vc = [[AlertMapVC alloc] initWithTitle:nil message:nil];
    
    [vc doBlock:^(NSInteger index) {
        if (index==0) {
            // Plans
            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
            NSURL *url =[NSURL URLWithString:str];
            
            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                NSLog(@"Can't use applemap://");
            }
        }
        else if(index==1)
        {
            
            
            //Maps
            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
            }
            
        }
        else if(index==2)
        {
            //Waze
            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"waze://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                // Waze is not installed. Launch AppStore to install Waze app
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
            }
        }
    }];
    [vc showInVC:self];
}

//JUMP

//1
-(void)rejoindreCommitAction:(UIButton *)sender{
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    
    
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),_dicChassesItem[@"c_name"]];
    
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}


//2
-(void) gotoGroup:(UIButton*)sender
{
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:_dicChassesItem[@"c_id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
        
    };
}

//3
- (void)cellRejoindre:(UIButton *)sender
{
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    NSString *desc=@"";
    NSString *strTile=@"";
    NSString *strNameGroup=_dicChassesItem[@"c_name"];
    
    int accessValueNumber = [_dicChassesItem[@"c_access"] intValue];
    
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        
        desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
        
        strTile = str(strRejoinAcceptPublic);
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
    }
    
    [COMMON addLoading:self];
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:_dicChassesItem[@"c_id"] ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        [COMMON removeProgressLoading];
        if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
            [self.tableControl reloadData];
            return ;
        }
        NSInteger access= [respone[@"access"] integerValue];
        if (access==NSNotFound) {
            [self.tableControl reloadData];
            return ;
        }
        [UIView animateWithDuration:0.3 animations:^{
        } completion:^(BOOL finished){}];
        
        
        [UIAlertView showWithTitle:strTile message:desc
                 cancelButtonTitle:str(strYES)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    };
}

- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    //get chasse item. set for .dictionaryGroup
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:_dicChassesItem[@"c_id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        int accessValueNumber = [_dicChassesItem[@"c_access"] intValue];
        
        NSString *desc=@"";
        NSString *strNameGroup=_dicChassesItem[@"c_name"];
        
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
            
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
        }
        
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        NSString *strLoungeID = _dicChassesItem[@"c_id"];
        [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            
            [COMMON removeProgressLoading];
            
            if([respone isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
            
            viewController1.dicChassesItem =  [GroupEnterOBJ sharedInstance].dictionaryGroup;
            
            viewController1.isInvitionAttend =YES;
            [viewController1 doCallback:^(NSString *strID) {
                //get chasse item. set for .dictionaryGroup
                
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
            }];
            [self pushVC:viewController1 animate:YES];
        };
        
    };
    
    
    
}

@end
