//
//  MeoteoView.m
//  Naturapass
//
//  Created by Manh on 12/2/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MeoteoView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Extensions.h"

@implementation MeoteoView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)fnDataView:(NSDictionary*)dicWeather
{
 
    NSString *strDate = [NSString stringWithFormat:@"%@",dicWeather[@"dt_txt"]];
    self.lbTime.text = [strDate substringWithRange:NSMakeRange(11, 5)];
    
    self.lbMain.text = dicWeather[@"weather"][0][@"main"];
    float temp = [dicWeather[@"main"][@"temp"] floatValue];
    self.lbTemperature.text =[NSString stringWithFormat:@"%0.1f°",temp];
    self.lbRain.text =[NSString stringWithFormat:@"%@%@", dicWeather[@"main"][@"humidity"],@"%"];
    
    NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"http://openweathermap.org/img/w/%@.png",dicWeather[@"weather"][0][@"icon"]]];
    [self.imgMain sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:nil];
}
@end
