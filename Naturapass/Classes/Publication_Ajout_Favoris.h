//
//  Publication_Ajout_Favoris.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PublicationBaseVC.h"

@interface Publication_Ajout_Favoris : PublicationBaseVC
@property(nonatomic,strong) IBOutlet UITextField *tfNom;
@property(nonatomic,strong) NSDictionary *dicFavo;
@end
