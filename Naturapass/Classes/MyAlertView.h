//
//  MyAlertView.h
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^MyAlertViewCbk)(NSDictionary *dicResult);

@interface MyAlertView : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *strTitle;
    NSArray *arrData;
    NSArray *listCheck;
    
    IBOutlet UITableView *popTable;
    TYPE_CONTROL typeControl;
}
@property (nonatomic,copy) MyAlertViewCbk callback;
@property (nonatomic,strong) IBOutlet UIView *viewBoder;


-(void)doBlock:(MyAlertViewCbk ) cb;

-(void) doShow :(UIViewController*)parent;

-(void)initParams:(NSString*)stitle arrContent:(NSArray*)arrContent arrCheck:(NSArray*)arrCheck withTypeControl:(TYPE_CONTROL)typeC parent:(UIViewController*)pr;


@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBorder;

@end


