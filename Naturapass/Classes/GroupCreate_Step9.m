//
//  GroupCreate_Step9.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step9.h"
#import "GroupCreate_Step3.h"
#import "SubNavigation_PRE_General.h"

@interface GroupCreate_Step9 ()

@end

@implementation GroupCreate_Step9

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.subview removeFromSuperview];
    [self addSubNav:nil];
    self.needChangeMessageAlert = YES;

}

-(IBAction)onNext:(id)sender
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        
        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
            
            [self.navigationController popToViewController:self.mParent animated:YES];
            
            return;
        }
    }
    
    [self doRemoveObservation];

    [self.navigationController popToRootViewControllerAnimated:YES];

}

@end
