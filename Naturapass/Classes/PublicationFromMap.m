//
//  PublicationFromMap.m
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "PublicationFromMap.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MediaCell.h"
#import "AllerMUR.h"
#import "AllerMURGPS.h"
#import "MapLocationVC.h"
#import "FriendInfoVC.h"
#import "MurEditPublicationVC.h"
#import "MurPeopleLikes.h"
#import "AgendaAndGroupCell.h"
#import "AgendaFromMapCell.h"
#import "GroupFromMapCell.h"
#import "GroupDetailCell.h"
#import "AgendaDetailCell.h"
#import "GroupEnterMurVC.h"
#import "NSString+HTML.h"
#import "NSDate+Extensions.h"
#import "AlertMapVC.h"
#import "ChassesParticipeVC.h"
#import "DetailFromMapVC.h"
#import "SignalerTerminez.h"
#import "AlertVC.h"

static NSString *MediaCellIDMur = @"MediaCell";
static NSString *AgendaAndGroupID = @"AgendaAndGroupID";
static NSString *CellAgendaID = @"CellAgendaID";
static NSString *CellGroupID = @"CellGroupID";
static NSString *GroupDetailID = @"GroupDetailID";
static NSString *AgendaDetailID = @"AgendaDetailID";

@interface PublicationFromMap ()
@property (nonatomic,assign) ARROW_TYPE arrowType;

@end

@implementation PublicationFromMap

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavDetailMap"];
    MainNavigationBaseView *subview =  [self getSubMainView];
    //Change background color
    //SUB
    [self addSubNav:nil];
    
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                subview.backgroundColor = UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR);
                
            }else{
                //Change status bar color
                subview.backgroundColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            }
            
        }
            break;
        case ISLOUNGE:
        {
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        case ISMUR:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        default:
        {
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
    }
    //
    [self.tableControl registerNib:[UINib nibWithNibName:@"MediaCell" bundle:nil] forCellReuseIdentifier:MediaCellIDMur];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AgendaAndGroupCell" bundle:nil] forCellReuseIdentifier:AgendaAndGroupID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AgendaFromMapCell" bundle:nil] forCellReuseIdentifier:CellAgendaID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"GroupFromMapCell" bundle:nil] forCellReuseIdentifier:CellGroupID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"GroupDetailCell" bundle:nil] forCellReuseIdentifier:GroupDetailID];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AgendaDetailCell" bundle:nil] forCellReuseIdentifier:AgendaDetailID];

    self.tableControl.estimatedRowHeight = 160;
    widthImage = [UIScreen mainScreen].bounds.size.width * 286/304;
    shareDetail = [[SettingActionView alloc] initSettingActionView];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    switch (self.expectTarget) {
        case ISCARTE:
        {
            if (self.isLiveHuntDetail) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_TINY_BAR_COLOR) ];
                
            }else{
                //Change status bar color
                [navBar setBarTintColor: UIColorFromRGB(CARTE_MAIN_BAR_COLOR) ];
            }
            
        }
            break;
        case ISLOUNGE:
        {
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_MAIN_BAR_COLOR) ];
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_MAIN_BAR_COLOR) ];
            
        }
            break;
        case ISMUR:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
            
        }
            break;
        default:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_MAIN_BAR_COLOR) ];
        }
            break;
    }
    
}
-(void)setDataWithPublication:(NSArray*)arr
{
    publicationArray = [NSMutableArray new];
    arrAgenda = [NSMutableArray new];
    arrGroup = [NSMutableArray new];
    arrSection3 = [NSMutableArray new];

    publicationArray = [arr mutableCopy];
    //Group
    for (NSDictionary *gDic in  publicationArray[0][@"groups"]) {
        BOOL isAdd = YES;
        if ([gDic[@"access"] intValue] == 0) {
            if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                int connected_access = [gDic[@"connected"][@"access"] intValue];
                if (connected_access < 2) {
                    isAdd = NO;
                }
            }
            else
            {
                isAdd = NO;
            }
        }
        if (isAdd) {
            [arrGroup addObject:gDic];
        }
    }
    //Agenda
    for (NSDictionary *gDic in  publicationArray[0][@"hunts"]) {
        BOOL isAdd = YES;
        if ([gDic[@"access"] intValue] == 0) {
            if ([gDic[@"connected"] isKindOfClass:[NSDictionary class]]) {
                int connected_access = [gDic[@"connected"][@"access"] intValue];
                if (connected_access < 2) {
                    isAdd = NO;
                }
            }
            else
            {
                isAdd = NO;
            }
        }
        //check endate
        if ([gDic[@"meeting"] isKindOfClass:[NSDictionary class]]) {
            if ([NSDate compareLocalDateWithInput:gDic[@"meeting"][@"date_end"]] == NSOrderedDescending) {
                isAdd = NO;
            }
        }
        else
        {
            isAdd = NO;
        }
        if (isAdd) {
            [arrAgenda addObject:gDic];
        }
    }

}
#pragma mark - TABLEVIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (arrSection3.count > 0) {
        return 3;
    }
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [publicationArray count];
    }
    else if (section == 2)
    {
        return arrSection3.count;
    }
    else
    {
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[MediaCell class]])
    {
        MediaCell *cell = (MediaCell *)cellTmp;
        
        NSDictionary *dic = [publicationArray objectAtIndex:indexPath.row] ;
        [cell setThemeWithFromScreen:self.isLiveHunt?ISLIVEMAP:self.expectTarget];
        
        if ([publicationArray objectAtIndex:0] != [NSNull null])
        {
            cell.btnTitle.tag=indexPath.row;
            [cell.btnTitle addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
            
            cell.btnProfile.tag=indexPath.row;
            [cell.btnProfile addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *strImage=@"";
            if (dic[@"owner"][@"profilepicture"] != nil) {
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
            }else{
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            }
            NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
            
            [cell.imageProfile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder_avatar"] ];
            cell.contraintHeightImageContent.constant = 0;
            cell.imageContent.contentMode = UIViewContentModeScaleToFill;

            if   ( (dic[@"media"] != nil) &&   [dic[@"media"] isKindOfClass: [ NSDictionary class]] )
            {
                if ([dic[@"media"] isKindOfClass:[NSDictionary class]]) {
                    NSString *strVideo= [dic[@"media"][@"type"] stringValue];
                    
                    //video
                    if ([strVideo isEqualToString:@"101"] ){
                        strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[[[publicationArray objectAtIndex:indexPath.row]
                                                                                     valueForKey:@"media"]
                                                                                    valueForKey:@"path"]];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".qt" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".mp4.mp4" withString:@".jpeg"];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".mp4.jpeg"];
                        
                        cell.imgIcon.hidden = NO;
                        
                    } else {
                        //image
                        cell.imgIcon.hidden = YES;
                        
                        strImage=[NSString stringWithFormat:@"%@%@.jpeg",IMAGE_ROOT_API,dic[@"media"][@"path"]];
                        strImage=[strImage stringByReplacingOccurrencesOfString:@".jpeg.jpeg" withString:@".jpeg"];
                    }
                    
                    url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
                    
//                    __weak MediaCell *weakCell = cell;
//                    __weak typeof(self) weakSelf = self;
                    cell.imageContent.tag = indexPath.row;
                    cell.imageContent.image = nil;
                    cell.imageContent.url= [COMMON characterTrimming:strImage];
                    float heightImg = [self fnResize:CGSizeMake([dic[@"media"][@"size"][@"width"] floatValue], [dic[@"media"][@"size"][@"height"] floatValue]) width:widthImage].height;
//                    if (heightImg > widthImage*9/16) {
//                        heightImg = widthImage*9/16;
//                    }
                    cell.contraintHeightImageContent.constant = heightImg;
                    [cell.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        /*
                        //check if in visible cell => update...
                        MediaCell *checkCell = (MediaCell*)[self.tableControl cellForRowAtIndexPath:indexPath];
                        //ok reload
                        
                        if (image) {
                            UIImage *tempImg = [self fnResizeFixWidth:image :widthImage];
                            
                            weakCell.imageContent.image = tempImg;
                        }
                        if (checkCell && image) {
                            [weakSelf.tableControl reloadData];
                        }
                         */
                    }];
                    
                    
                    
                    [cell.btnTitle addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    
                    [cell.imageContent setOncallback:^(NSInteger index)
                     {
                         int dx = (int)index - COMMENTBUTTONTAG;
                         
                         NSDictionary *dic = [publicationArray objectAtIndex:dx];
                         [self displayCommentVC:dic];
                         
                     }];
                    
                }
            }
            else
            {
                //text
                cell.imgIcon.hidden = YES;
                cell.imageContent.image = nil;
                [cell.imageContent fnRemoveClick];
            }
            
            [cell.location addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.location.tag = indexPath.row + LOCATIONBUTTONTAG;
            //Assign Values in Cell
            [cell assignValues:dic];
            
            cell.btnObs.tag = indexPath.row + 2001;
            [cell setCbClickObs:^(UIButton*theBtn)
             {
                 NSInteger index = (int)theBtn.tag - 2001;
                 NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
                 BOOL readmore = ![dicTmp[@"readobs"] boolValue];
                 [dicTmp setObject:@(readmore) forKey:@"readobs"];
                 [publicationArray replaceObjectAtIndex:index withObject:dicTmp];
                 
                 [self.tableControl beginUpdates];
                 NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
                 [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                 [self.tableControl endUpdates];
             }];
            cell.myContent.lineBreakMode =  NSLineBreakByWordWrapping;
            cell.myContent.numberOfLines = 0;
//            UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture:)];
//            
//            readMoreGesture.numberOfTapsRequired = 1;
//            [cell.myContent addGestureRecognizer:readMoreGesture];
//            cell.myContent.tag = indexPath.row + 1001;
//            if ([dic[@"readmore"] boolValue]) {
//                cell.myContent.lineBreakMode =  NSLineBreakByWordWrapping;
//                cell.myContent.numberOfLines = 0;
//            }
//            else
//            {
//                cell.myContent.lineBreakMode =  NSLineBreakByTruncatingTail;
//                cell.myContent.numberOfLines = 3;
//
//            }
            [cell setCbClickText:^(UIButton*theBtn)
             {
                 int index = (int)theBtn.tag - COMMENTBUTTONTAG;
                 NSDictionary *dic = [publicationArray objectAtIndex:index];
                 [self displayCommentVC:dic];
                 
             }];
        }
        
        [cell.btnTextComment addTarget:self action:@selector(commentViewAction:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnComment addTarget:self action:@selector(commentViewAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnLike addTarget:self action:@selector(likeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        
        //Is me -> edit + Del
        //friend -> Signal
        
        [cell.btnSetting addTarget:self action:@selector(settingAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnSetting.tag =indexPath.row+100;
        
        [cell.btnLikesList addTarget:self action:@selector(displayLikesList:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnLikesList.tag =indexPath.row+110;
        [cell.btnShareDetail addTarget:self action:@selector(shareDetailAction:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnShareDetail.tag =indexPath.row+210;
        cell.btnLike.tag = indexPath.row + LIKETAG;
        cell.btnComment.tag = indexPath.row + COMMENTBUTTONTAG;
        cell.btnTextComment.tag = indexPath.row + COMMENTBUTTONTAG;
        cell.imageContent.tag =indexPath.row + COMMENTBUTTONTAG;
        [cell layoutIfNeeded];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        MediaCell *cell = [tableView dequeueReusableCellWithIdentifier:MediaCellIDMur forIndexPath:indexPath];
        [self configureCell:cell forRowAtIndexPath:indexPath];
        return cell;
    }
    else if (indexPath.section == 2)
    {
        if (_arrowType == ARROW_GROUP) {
            if (arrSection3.count == 1) {
                GroupDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:GroupDetailID forIndexPath:indexPath];
                [self configureGroupDetailCell:cell cellForRowAtIndexPath:indexPath];
                return cell;

            }
            else
            {
                GroupFromMapCell *cell = [tableView dequeueReusableCellWithIdentifier:CellGroupID forIndexPath:indexPath];
                [self configureGroupCell:cell cellForRowAtIndexPath:indexPath];
                return cell;

            }
        }
        else
        {
            if (arrSection3.count == 1) {
                AgendaDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:AgendaDetailID forIndexPath:indexPath];
                [self configureAgendaDetailCell:cell cellForRowAtIndexPath:indexPath];
                return cell;
                
            }
            else
            {

            AgendaFromMapCell *cell = [tableView dequeueReusableCellWithIdentifier:CellAgendaID forIndexPath:indexPath];
            [self configureAgendaCell:cell cellForRowAtIndexPath:indexPath];
            return cell;
            }
        }

    }
    else
    {
        AgendaAndGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:AgendaAndGroupID forIndexPath:indexPath];
        [cell.btnAgenda addTarget:self action:@selector(switchStateAgendaAndGroup:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnAgenda.tag =indexPath.row+500;

        [cell.btnGroup addTarget:self action:@selector(switchStateAgendaAndGroup:) forControlEvents:UIControlEventTouchUpInside];
        cell.btnGroup.tag =indexPath.row+1000;
        cell.imgArrowGroup.image = [UIImage imageNamed:@"detail_arrow_up"];
        cell.imgArrowAgenda.image = [UIImage imageNamed:@"detail_arrow_up"];

        if (_arrowType == ARROW_GROUP) {
            cell.imgArrowGroup.image = [UIImage imageNamed:@"detail_arrow_down"];
            cell.imgArrowAgenda.image = [UIImage imageNamed:@"detail_arrow_up"];

        }
        else if (_arrowType == ARROW_AGENDA)
        {
            cell.imgArrowGroup.image = [UIImage imageNamed:@"detail_arrow_up"];
            cell.imgArrowAgenda.image = [UIImage imageNamed:@"detail_arrow_down"];
            
        }
        if (arrGroup.count == 0) {
            cell.vGroup.hidden= YES;
        }
        else
        {
            cell.vGroup.hidden= NO;

        }
        if (arrAgenda.count == 0) {
            cell.vAgenda.hidden= YES;
        }
        else
        {
            cell.vAgenda.hidden= NO;
            
        }
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];
        return cell;
    }
}
- (void)configureAgendaCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[AgendaFromMapCell class]])
    {
        AgendaFromMapCell *cell = (AgendaFromMapCell *)cellTmp;
        NSMutableArray *arrData =[NSMutableArray new];
        
        arrData =[arrSection3 copy];
        
        if([arrData count]>0){
            //
            NSDictionary *dic = arrData[indexPath.row];
            
            //location
            [cell.btnLocation addTarget:self action:@selector(locationAgendaAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnLocation.tag = indexPath.row + 200;
            
            NSString *loungDateString=@"";
            loungDateString=[NSString stringWithFormat:@"%@",dic[@"meeting"][@"end_date"]];
            NSString * outputString = [self convertDate:loungDateString] ;
            NSString                        *strName;
            strName=dic[@"name"];
            cell.lbTitle.text=strName;
            cell.lbRendez.text=[NSString stringWithFormat:@"%@", outputString];
            //ADDRESS
            NSString*strLocation;
            if(dic[@"meeting"][@"address"]!=nil){
                strLocation=[NSString stringWithFormat:@"%@",dic[@"meeting"][@"address"][@"address"]];
                [cell.lbRendez setText:strLocation];
            }
            else {
                [cell.lbRendez setText: [ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meeting"][@"address"][@"latitude"] floatValue],
                                       [dic[@"meeting"][@"address"][@"longitude"] floatValue]
                                       ] ];
            }
            
            NSString *strText;
            strText=[NSString stringWithFormat:@"%@",dic[@"description"]];
            if ([strText isEqualToString:@"(null)"]) {
                strText=@"";
            }
            //DATE
            NSString * dateStrings = [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_begin"]]];
            if(dateStrings !=nil)
                [cell.lbDebut setText:[NSString stringWithFormat:@"%@",dateStrings]];
            else
                [cell.lbDebut setText:@""];
            
            //END DATE
            NSString *outputStrings= [self convertDate:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_end"]]];
            if(outputStrings !=nil)
                cell.lbFin.text = [NSString stringWithFormat:@"%@", outputStrings];
            else
                cell.lbFin.text = @"";
            
            [cell.lbDescription setText:strText];
            [cell.lbDescription setBackgroundColor:[UIColor clearColor]];
            [cell layoutIfNeeded];
            NSString *strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"access"]];
            if([strAccessGroup isEqualToString:@"(null)"]||strAccessGroup==nil)
                strAccessGroup=[NSString stringWithFormat:@"%@",dic[@"lounge"][@"access"]];
            
            if(strAccessGroup.intValue == ACCESS_PRIVATE)
                cell.lbAccess.text=str(strAccessPrivate);
            else if(strAccessGroup.intValue ==ACCESS_SEMIPRIVATE)
                cell.lbAccess.text=str(strAccessSemiPrivate);
            else if(strAccessGroup.intValue==ACCESS_PUBLIC)
                cell.lbAccess.text=str(strAccessPublic);
            
            cell.imgIcon.image = [UIImage imageNamed:@"detail_icon_agenda_brown"];
            [cell.lbTitle setTextColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            [cell.lbRendez setTextColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            cell.btnEnter.tag = 1000 + indexPath.row;;
            [cell.btnEnter addTarget:self action:@selector(enterDetailAgendaAction:) forControlEvents:UIControlEventTouchUpInside];
            cell.backgroundColor=[UIColor clearColor];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell layoutIfNeeded];

        }
        
    }
}
- (void)configureGroupCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[GroupFromMapCell class]])
    {
        GroupFromMapCell *cell = (GroupFromMapCell *)cellTmp;
        NSDictionary *dic = arrSection3[indexPath.row];
        
        cell.imgIcon.image =  [UIImage imageNamed: @"detail_icon_group_green"];
        
        //FONT
        cell.lbTitle.text = dic[@"name"];
        cell.lbDescription.text = dic[@"description"];
        cell.btnEnter.tag = 1000 + indexPath.row;;
        [cell.btnEnter addTarget:self action:@selector(enterDetailGroupAction:) forControlEvents:UIControlEventTouchUpInside];

        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];

    }
}
- (void)configureGroupDetailCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[GroupDetailCell class]])
    {
        GroupDetailCell *cell = (GroupDetailCell *)cellTmp;
        NSDictionary *dic = arrSection3[indexPath.row];
        //FONT
        cell.lbTitle.text = dic[@"name"];
        cell.lbDescription.text = dic[@"description"];
        int         accessType = [dic[@"access"] intValue];
        if (accessType == 0) {
            cell.lblAccess.text = str(strAccessPrivate);
        } else if (accessType == 1) {
            cell.lblAccess.text = str(strAccessSemiPrivate);
        } else if (accessType == 2) {
            cell.lblAccess.text = str(strAccessPublic);
        }

//        int         nbPending = [dic[@"nbPending"] intValue];
        int         nbSubscribers = [dic[@"nbSubscribers"] intValue];
        if (nbSubscribers ==1) {
            cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@", nbSubscribers,str(strMMembre)];
        }
        else
        {
            cell.lblNbSubcribers.text = [NSString stringWithFormat:@"%d %@s", nbSubscribers,str(strMMembre)];
        }
        //Enter
        cell.btnEnterGroup.tag = 1000 + indexPath.row;;
        [cell.btnEnterGroup addTarget:self action:@selector(enterGroupMurAction:) forControlEvents:UIControlEventTouchUpInside];
        
        NSString    *strPhotoUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strPhotoUrl]];
        __weak GroupDetailCell *weakCell = cell;
        __weak typeof(self) weakSelf = self;

        [cell.imgIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //check if in visible cell => update...
            GroupDetailCell *checkCell = (GroupDetailCell*)[self.tableControl cellForRowAtIndexPath:indexPath];
            //ok reload
            
            if (image) {
                UIImage *tempImg = [self fnResizeFixWidth:image :widthImage];
                
                weakCell.imgIcon.image = tempImg;
            }
            if (checkCell && image) {
                [weakSelf.tableControl reloadData];
            }
        }];

        [cell fnSettingCell:UI_GROUP_MUR_ADMIN];
        
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];

    }
}
- (void)configureAgendaDetailCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cellTmp isKindOfClass:[AgendaDetailCell class]])
    {
        AgendaDetailCell *cell = (AgendaDetailCell *)cellTmp;
        NSDictionary *dic = arrSection3[indexPath.row];
        cell.lblDebut.text = str(strDEBUTLE);
        cell.lblFin.text = str(strFINLE);
        cell.lblRendez.text = str(strRendezVousA);
        
        
        NSString* strPath  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
        
        [cell.logo sd_setImageWithURL: [NSURL URLWithString: strPath ] placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        NSString                    *strAccess;

        if ([dic[@"connected"] isKindOfClass:[NSDictionary class]]) {
            strAccess= [NSString stringWithFormat:@"%@",dic[@"connected"][@"access"]];
        }
        else
        {
            strAccess=@"4";
        }
        NSInteger iSubScribe = 10;
        if (strAccess && ![strAccess isEqualToString:@""]) {
            iSubScribe=[strAccess intValue];
        }
        [cell.btnSURNATURA setColorEnable:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        [cell.btnSURNATURA setColorDisEnable:[UIColor lightGrayColor]];
        
        //refresh
        [cell.btnSURNATURA removeTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSURNATURA removeTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSURNATURA removeTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
        
        switch (iSubScribe)
        {
            case USER_INVITED:
            {
                [cell.btnSURNATURA setTitle:str(strA_VALIDER) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(rejoindreCommitAction:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_WAITING_APPROVE:
            {
                [cell.btnSURNATURA setTitle:str(strEN_ATTENTE) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:NO];
            }
                break;
                
            case USER_NORMAL:
            case USER_ADMIN:
            {
                [cell.btnSURNATURA setTitle:str(strACCEDER) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(gotoGroup:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            case USER_REJOINDRE:
            {
                [cell.btnSURNATURA setTitle:str(strREJOINDRE) forState:UIControlStateNormal];
                [cell.btnSURNATURA setEnabled:YES];
                [cell.btnSURNATURA addTarget:self action:@selector(cellRejoindre:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
            default:
                break;
        }

        
        cell.lbNameHunt.text = [dic[@"name"] stringByDecodingHTMLEntities];
        
        //
        int iAccess = [dic[@"access"] intValue];
        
        
        if (iAccess == 0)
            cell.lbAccess.text=str(strAccessPrivate);
        else if(iAccess == 1)
            cell.lbAccess.text=str(strAccessSemiPrivate);
        else if(iAccess == 2)
            cell.lbAccess.text=str(strAccessPublic);
        
        cell.lbDescription.text = [dic[@"description"] stringByDecodingHTMLEntities];
        
        NSString *strMeetingDate = [NSDate convertAgendaToNSTring:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_begin"]]];
        
        NSString *strEndDate = [NSDate convertAgendaToNSTring:[NSString stringWithFormat:@"%@",dic[@"meeting"][@"date_end"]]];

        cell.lbStartDate.text=@"";
        cell.lbStartTime.text=@"";
        cell.lbAddress.text =@"";
        cell.lbLatLong.text =@"";
        //START DATE
        
        if (strMeetingDate.length>0) {
            cell.lbStartDate.text= [strMeetingDate substringWithRange:NSMakeRange(0, 10)];
            cell.lbStartTime.text= [NSString stringWithFormat:@"à %@",[strMeetingDate substringWithRange:NSMakeRange(11, 5)]];
        }
        //END DATE
        if (strEndDate.length>0) {
            cell.lbEndDate.text= [strEndDate substringWithRange:NSMakeRange(0, 10)];
            cell.lbEndTime.text= [NSString stringWithFormat:@"à %@",[strEndDate substringWithRange:NSMakeRange(11, 5)]];
        }
        //ADDRESS
        cell.lbAddress.text = [dic[@"meeting"][@"address"][@"address"] stringByDecodingHTMLEntities];
        cell.lbLatLong.text =[ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meeting"][@"address"][@"latitude"] floatValue],[dic[@"meeting"][@"address"][@"longitude"] floatValue]];
        cell.backgroundColor=[UIColor clearColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];
        
    }
}
- (IBAction)locationAgendaAction:(UIButton*) sender
{
    int index = (int) sender.tag - 200;
    NSDictionary *myDic = arrSection3[index];
    id dic = myDic[@"meetingAddress"];
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = NO;
        viewController1.isSpecial = YES;
        
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"]};
        
        viewController1.needRemoveSubItem = NO;
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = myDic;
        
        [self pushVC:viewController1 animate:YES expectTarget: ISMUR];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


-(void) fnAller:(NSInteger)tag
{
    
    NSDictionary *dic = [publicationArray objectAtIndex:tag] ;
    
    AllerMUR *vc = [[AllerMUR alloc] initWithData:dic];
    
    [vc doBlock:^(NSInteger index) {
        switch (index) {
            case 0:
            {
                //cancel
            }
                break;
            case 1:
            {
                MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
                mapVC.isSubVC = YES;
                mapVC.simpleDic = dic;
                mapVC.isLiveHuntDetail = YES;
                [self pushVC:mapVC animate:YES expectTarget:ISMUR];
                
                
                
                //to naturapass
                /*
                 MapGlobalVC *mapVC = [[MapGlobalVC alloc]initWithNibName:@"MapGlobalVC" bundle:nil];
                 mapVC.simpleDic = @{@"latitude":dic[@"geolocation"][@"latitude"],
                 @"longitude":dic[@"geolocation"][@"longitude"]};
                 
                 [_parentVC pushVC:mapVC animate:YES expectTarget:ISCARTE];
                 */
                
            }
                break;
            case 2:
            {
                //self add new VIEW.
                NSString *strLat =@"";
                NSString *strLng =@"";
                
                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                double lat = appDelegate.locationManager.location.coordinate.latitude;
                double lng = appDelegate.locationManager.location.coordinate.longitude;
                
                strLat = dic[@"geolocation"][@"latitude"];
                strLng = dic[@"geolocation"][@"longitude"];
                
                AllerMURGPS *vc = [[AllerMURGPS alloc] initWithData:nil];
                
                [vc doBlock:^(NSInteger index) {
                    
                    switch (index) {
                            
                        case 0:
                        {}break;
                            
                        case 1:
                        {
                            //cancel
                            // Plans
                            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
                            NSURL *url =[NSURL URLWithString:str];
                            
                            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                                [[UIApplication sharedApplication] openURL:url];
                            } else {
                                NSLog(@"Can't use applemap://");
                            }
                            
                        }
                            break;
                        case 2:
                        {
                            //Maps
                            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
                            
                            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
                            
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
                            }
                            
                        }
                            break;
                        case 3:
                        {
                            //Waze
                            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
                            if ([[UIApplication sharedApplication] canOpenURL:
                                 [NSURL URLWithString:@"waze://"]]) {
                                [[UIApplication sharedApplication] openURL:
                                 [NSURL URLWithString:str]];
                            } else {
                                // Waze is not installed. Launch AppStore to install Waze app
                                [[UIApplication sharedApplication] openURL:[NSURL
                                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
                            }
                            
                        }
                            break;
                            
                    }
                    
                }];
                [vc showAlert];
                
            }
                break;
            default:
                break;
        }
    }];
    [vc fnColorWithExpectTarget:self.expectTarget];
    [vc showAlert];
    
}

#pragma mark - Resize Image
-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth
{
    float actualHeight = img.size.height;
    float actualWidth = img.size.width;
    
    float imgRatio = actualHeight/actualWidth;
    
    float fixWidth = wWidth;
    
    imgRatio = fixWidth / actualWidth;
    actualHeight = imgRatio * actualHeight;
    actualWidth = fixWidth;
    
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [img drawInRect:rect];
    UIImage *returnImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return returnImg;
}
#pragma mark - Action
-(void)reloadDataItemWithIndex:(NSString*)publicationId
{
    [self getPublictionItem:publicationId];
}

-(void) displayCommentVC: (NSDictionary *)dic
{
    
    CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
    commentVC.commentDic= dic;
    
    [commentVC doCallback:^(NSString *pubicationID) {
        [self reloadDataItemWithIndex:pubicationID];
    }];
    
    [commentVC setCallbackWithDelItem:^(NSDictionary *dicPublication)
     {
         //delete
         if (_callbackWithDelItem) {
             _callbackWithDelItem(dicPublication);
         }
         [self gotoback];
     }];
    // push
    [self pushVC:commentVC animate:YES expectTarget:ISMUR];
    
}
#pragma mark - user profile action

- (void) userProfileAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[[publicationArray objectAtIndex:tag][@"owner"][@"id"] integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    if ([self isKindOfClass:[FriendInfoVC class]]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    
    [friend setFriendDic:[[publicationArray objectAtIndex:tag] objectForKey:@"owner"]];
    [self pushVC:friend animate:YES expectTarget:ISMUR];
}

- (void) userProfileFromTheirNameAction:(id)sender
{
    NSInteger tag = ((UIButton *)sender).tag;
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[[publicationArray objectAtIndex:tag][@"owner"][@"id"] integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    if ([self isKindOfClass:[FriendInfoVC class]]) {
        return;
    }
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic:[[publicationArray objectAtIndex:tag] objectForKey:@"owner"]];
    [self pushVC:friend animate:YES expectTarget:ISMUR];
    
}
- (IBAction)locationButtonAction:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    NSInteger locationtag = [btn tag] - LOCATIONBUTTONTAG;
    
    NSDictionary*dic = [publicationArray objectAtIndex:locationtag] ;
    
    if ([dic[@"geolocation"] isKindOfClass: [NSDictionary class ] ])
    {
        NSDictionary *dLocation = [[publicationArray objectAtIndex:locationtag] valueForKey:@"geolocation"];
        
        if (dLocation != nil)
        {
            MapLocationVC *mapVC=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
            mapVC.isSubVC = YES;
            
            mapVC.simpleDic = dic;
            
            [self pushVC:mapVC animate:YES expectTarget:ISMUR];
            
        }
    }
    
}

#pragma mark - menu
-(IBAction)settingAction:(UIButton *)sender
{
    NSInteger index =[sender tag]-100;
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    // Hide already showing popover
    [cell show];
    
    [cell setCallBackGroup:^(NSInteger ind)
     {
         NSDictionary *dic = [publicationArray objectAtIndex:index] ;
         
         //Editer/supprimer/signaler
         
         if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
             switch (ind) {
                     
                 case 0:
                 {
                     [self editerAction:index];
                     
                 }
                     break;
                 case 1:
                 {
                     NSString *strContent = str(strMessage23);
                     
                     [UIAlertView showWithTitle: str(strMessage24)
                                        message:strContent
                              cancelButtonTitle:str(strOui)
                              otherButtonTitles:@[str(strNon)]
                                       tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                           if (buttonIndex == [alertView cancelButtonIndex]) {
                                               [self deletePublicationAction:index];
                                           }
                                           else
                                           {
                                               
                                           }
                                       }];
                     
                     
                 }
                     break;
                 case 2:
                 {
                     [self fnAller:index];
                     
                 }
                     break;
                 default:
                     break;
             }
             
         }else{
             switch (ind) {
                     
                 case 0:
                 {
                     [self signalerAction:index];
                     
                 }
                     break;
                 case 1:
                 {
                     [self blackListerAction: index];
                 }
                     break;
                 case 2:
                 {
                     [self fnAller:index];
                     
                 }
                     break;
                 default:
                     break;
             }
         }
         
         
     }];
}
#pragma mark - LikeAction

-(IBAction)likeButtonAction:(UIButton *)sender
{
    
    [COMMON addLoadingForView:self.view];
    [sender setSelected:YES];
    NSInteger liketag = [sender tag] - LIKETAG;
    
    if([[[publicationArray objectAtIndex:liketag] valueForKey:@"isUserLike"] integerValue]==1)
    {
        //Current like -> unlike
        
        NSString *pubicationID=[[[publicationArray objectAtIndex:liketag]valueForKey:@"id"] stringValue];
        
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj deletePublicationActionAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
            
//            [self getPublictionItem:pubicationID];
            [self updateLike:liketag withDicLike:response];

        };
    }
    else
    {
        //Current unlike
        
        NSString *pubicationID=[[[publicationArray objectAtIndex:liketag]valueForKey:@"id"] stringValue];
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj postPublicationLikeAction:pubicationID];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            if (!response) {
                return ;
            }
//            [self getPublictionItem:pubicationID];
            [self updateLike:liketag withDicLike:response];
        };
    }
}
-(void)updateLike:(NSInteger)index withDicLike:(NSDictionary*)dicLike
{
    if (dicLike[@"likes"]) {
        NSMutableDictionary *dic = [publicationArray[index] mutableCopy];
        [dic setObject:dicLike[@"likes"] forKey:@"likes"];
        [dic setObject:dicLike[@"likedusers"] forKey:@"likedusers"];
        BOOL isUserLike = ![dic[@"isUserLike"] boolValue];
        [dic setObject:@(isUserLike) forKey:@"isUserLike"];
        [publicationArray replaceObjectAtIndex:index withObject:dic];
        [self.tableControl reloadData];
    }
}
#pragma mark - DeleteAction

-(void)deletePublicationAction:(NSInteger )index
{
    [COMMON addLoadingForView:self.view];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI deletePublicationAction:publicationArray[index][@"id"]];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if (!response) {
            return ;
        }
        NSDictionary *dic = (NSDictionary*)response;
        if ([dic[@"success"] boolValue]) {
            
            //update sqldb
            
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];


                NSString *strQuerry = [NSString stringWithFormat:@"DELETE FROM `tb_carte` WHERE `c_id` IN (%@) AND ( c_user_id=%@ OR c_user_id= %d);", publicationArray[index][@"id"], sender_id, SPECIAL_USER_ID];
                
                NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                
                ASLog(@"%@", tmpStr);
                
                BOOL isOK =   [db  executeUpdate:tmpStr];

            }];
            
            //update in cache Mur pub
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_MUR_SAVE)   ];
            NSArray *arr = [[NSArray arrayWithContentsOfFile:strPath] mutableCopy];
            NSMutableArray *mutArr = [arr mutableCopy];
            for (NSDictionary*mDic in arr) {
                if ([mDic[@"id"] intValue] == [publicationArray[index][@"id"] intValue]) {
                    [mutArr removeObject:mDic];
                    [mutArr writeToFile:strPath atomically:YES];
                    
                    break;
                }
            }
            if (_callbackWithDelItem) {
                _callbackWithDelItem(publicationArray[index]);
            }
            [self gotoback];
            
        }
    };
}

- (IBAction)signalerAction:(NSInteger)iTag {
    
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    
    AlertVC *vc = [[AlertVC alloc] initAlertSignalez];
    
    [vc doBlock:^(NSInteger index, NSString *strContent) {
        
        switch ((int)index) {
                //ACCUEIL -> Homepage
            case 0:
            {
            }
                break;
                
            case 1:
            {
                NSDictionary * postDict = @{@"publication": @{@"signal":@1, @"explanation":str(strThis_publication_has_content_issue)} };
                
                WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                
                [serviceObj postPublicationSignalAction: dic[@"id"] withParametersDic:postDict];
                
                serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                    NSLog(@"%@",response);
                    
                };
            }
                break;
                
            default:
                break;
        }
    }];
    [vc showAlert];


}
-(void)blackListerAction:(NSInteger)iTag
{
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    
    NSString *strContent =[NSString stringWithFormat: str(strVoulez_vous_blacklister),dic[@"owner"][@"fullname"]];
    
    [UIAlertView showWithTitle:str(strBlacklister)
                       message:strContent
             cancelButtonTitle:str(strOUI)
             otherButtonTitles:@[str(strNON)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              [COMMON addLoading:self];
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj postUserLock:@{@"id": dic[@"owner"][@"id"]}];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//                                  if (_callback) {
//                                      _callback(VIEW_ACTION_REFRESH_TOP, publicationArray);
//                                  }
                              };
                          }
                          else
                          {
                              
                          }
                      }];
}
/*
 
 */
-(void)editerAction:(NSInteger)iTag
{
    NSDictionary *dic = [publicationArray objectAtIndex:iTag] ;
    [[PublicationOBJ sharedInstance] fnSetValueForKeyWithPublication:dic];
    [PublicationOBJ sharedInstance].mParentVC =self;
    SignalerTerminez *viewController1 = [[SignalerTerminez alloc] initWithNibName:@"SignalerTerminez" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
}
-(IBAction)commentViewAction:(UIButton *)sender
{
    int index = (int)sender.tag - COMMENTBUTTONTAG;
    NSDictionary *dic = [publicationArray objectAtIndex:index];
    [self displayCommentVC:dic];
}
-(IBAction)displayLikesList:(id)sender
{
    NSInteger index =[sender tag]-110;
    NSDictionary *dic = [publicationArray objectAtIndex:index];
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI = [[WebServiceAPI alloc]init];
    [serviceAPI getPublicationLikesAction:dic[@"id"]];
    serviceAPI.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        
        if ([self fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        NSMutableArray *arrMembers =[NSMutableArray new];
        [arrMembers addObjectsFromArray:[response objectForKey:@"likes"]];
        
        if (arrMembers.count>0) {
            [self showPopupMembers:arrMembers];
        }
    };
    
}
-(void)showPopupMembers:(NSMutableArray*)arrMembers
{
    MurPeopleLikes *viewController1 = [[MurPeopleLikes alloc] initWithNibName:@"MurPeopleLikes" bundle:nil];
    [viewController1 setData:arrMembers];
    
    viewController1.expectTarget =ISMUR;
    [viewController1  setCallBack:^(NSInteger index,NSDictionary *dic)
     {
         FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
         [friend setFriendDic: dic];
         [self pushVC:friend animate:YES expectTarget:ISMUR];
     }];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
    
}
#pragma mark - GET DATA

- (void)getPublictionItem:(NSString*)public_id {
    //request first page
    
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getPublicationsActionItem:public_id];
    __weak BaseVC *wself = self;
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        dispatch_async(dispatch_get_main_queue(), ^{
            [COMMON removeProgressLoading];
            if([wself fnCheckResponse:response]) return;
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            NSMutableArray *publicationArrayItem =[NSMutableArray new];
            
            [publicationArrayItem addObjectsFromArray:@[response[@"publication"]]];
            
            if (publicationArrayItem.count > 0) {
                NSString *idItem =response[@"publication"][@"id"];
                for (int i=0; i<publicationArray.count; i++) {
                    NSString *idPub =[publicationArray objectAtIndex:i][@"id"];
                    
                    if (idItem.intValue== idPub.intValue) {
                        // update
                        [publicationArray replaceObjectAtIndex:i withObject:publicationArrayItem[0]];
                        [self.tableControl reloadData];
                        //                        if (_callback) {
                        //                            _callback(VIEW_ACTION_UPDATE_DATA, publicationArray);
                        //                        }
                        break;
                    }
                }
                
            }
        });
    };
}
-(IBAction)switchStateAgendaAndGroup:(id)sender
{
    NSInteger index = 0;
    if ([sender tag] >= 1000) {
        //group
        index = [sender tag] - 1000;
        if (_arrowType == ARROW_GROUP) {
            _arrowType = ARROW_NONE;
            [arrSection3 removeAllObjects];
        }
        else
        {
            _arrowType = ARROW_GROUP;
            arrSection3 = [arrGroup mutableCopy];

        }
    }
    else
    {
        index = [sender tag] - 500;
        if (_arrowType == ARROW_AGENDA) {
            _arrowType = ARROW_NONE;
            [arrSection3 removeAllObjects];

        }
        else
        {
            _arrowType = ARROW_AGENDA;
            arrSection3 = [arrAgenda mutableCopy];

        }
    }
    [self.tableControl reloadData];
}
-(void)enterGroupMurAction:(id)sender
{
    //Expect target...
    
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = tag - 1000;
    NSDictionary *dic = [arrSection3 objectAtIndex:index];
    GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    viewController1.needRemoveSubItem = NO;
    //
    [[GroupEnterOBJ sharedInstance] resetParams];
    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    [self pushVC:viewController1 animate:YES expectTarget:ISGROUP];
}
#pragma mark - API

-(IBAction)NATURAAction:(id)sender
{
    //ACCESS page??? or ....that depend on user type
    
    //        NSDictionary *local = (NSDictionary*)dic;
    //
    //        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
    //        viewController1.isSubVC = YES;
    //        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
    //                                      @"longitude":  local[@"longitude"]};
    //
    //        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
}

-(IBAction)GPSAction:(id)sender
{
    //ROUTE from Current location to AGENDA position.
    NSDictionary *dic = arrSection3[0];
    NSString *strLat =@"";
    NSString *strLng =@"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    double lat = appDelegate.locationManager.location.coordinate.latitude;
    double lng = appDelegate.locationManager.location.coordinate.longitude;
    strLat = dic[@"meeting"][@"address"][@"latitude"] ;
    strLng = dic[@"meeting"][@"address"][@"longitude"] ;
    
    AlertMapVC *vc = [[AlertMapVC alloc] initWithTitle:nil message:nil];
    
    [vc doBlock:^(NSInteger index) {
        if (index==0) {
            // Plans
            NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"http://maps.apple.com/?q=%@,%@&zoom=8",strLat,strLng];
            NSURL *url =[NSURL URLWithString:str];
            
            if ([[UIApplication sharedApplication] canOpenURL: url]) {
                [[UIApplication sharedApplication] openURL:url];
            } else {
                NSLog(@"Can't use applemap://");
            }
        }
        else if(index==1)
        {
            
            
            //Maps
            NSString *str = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%f,%f&daddr=%@,%@", lat, lng, strLat, strLng];
            
            //            NSString *str  = [NSString stringWithFormat:@"comgooglemaps://?q=%@,%@&views=traffic&zoom=8",strLat,strLng];
            
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"comgooglemaps://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id585027354"]];
            }
            
        }
        else if(index==2)
        {
            //Waze
            NSString *str  = [NSString stringWithFormat:@"waze://?ll=%@,%@&navigate=yes",strLat,strLng];
            if ([[UIApplication sharedApplication] canOpenURL:
                 [NSURL URLWithString:@"waze://"]]) {
                [[UIApplication sharedApplication] openURL:
                 [NSURL URLWithString:str]];
            } else {
                // Waze is not installed. Launch AppStore to install Waze app
                [[UIApplication sharedApplication] openURL:[NSURL
                                                            URLWithString:@"http://itunes.apple.com/us/app/id323229106"]];
            }
        }
    }];
    [vc showInVC:self];
}

//JUMP

//1
-(void)rejoindreCommitAction:(UIButton *)sender{
    NSDictionary *dic = arrSection3[0];
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    
    
    NSString *strMessage = [ NSString stringWithFormat: str(strAcceptInvitationTourChasse),dic[@"name"]];
    
    [UIAlertView showWithTitle:str(strTitle_app) message:strMessage
             cancelButtonTitle:str(strNO)
             otherButtonTitles:@[str(strYES)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          
                          if (buttonIndex == 1) {
                              [self cellRejoindreFromInviter:sender];
                              
                          }
                          else
                          {
                              
                          }
                          
                      }];
}


//2
-(void) gotoGroup:(UIButton*)sender
{
    NSDictionary *dic = arrSection3[0];
    
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
        
    };
}

//3
- (void)cellRejoindre:(UIButton *)sender
{
    NSDictionary *dic = arrSection3[0];
    
    if (![COMMON isReachable])
    {
        //warning
        [KSToastView ks_showToast: str(strUneConnexionInternetEstRequise) duration:2.0f completion: ^{
        }];
        
        return;
    }
    
    NSString *desc=@"";
    NSString *strTile=@"";
    NSString *strNameGroup=dic[@"name"];
    
    int accessValueNumber = [dic[@"access"] intValue];
    
    if (accessValueNumber == ACCESS_PUBLIC)
    {
        
        desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
        
        strTile = str(strRejoinAcceptPublic);
    }
    else if (accessValueNumber == ACCESS_PRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptPrivate);
    }
    else if (accessValueNumber == ACCESS_SEMIPRIVATE)
    {
        //Join_Salon_Text
        desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
        strTile = str(strRejoinAcceptSemi);
    }
    
    [COMMON addLoading:self];
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:dic[@"id"] ];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:respone];
        
        [COMMON removeProgressLoading];
        if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
            [self.tableControl reloadData];
            return ;
        }
        NSInteger access= [respone[@"access"] integerValue];
        if (access==NSNotFound) {
            [self.tableControl reloadData];
            return ;
        }
        [UIView animateWithDuration:0.3 animations:^{
        } completion:^(BOOL finished){}];
        
        
        [UIAlertView showWithTitle:strTile message:desc
                 cancelButtonTitle:str(strYES)
                 otherButtonTitles:nil
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          }];
    };
}

- (void)cellRejoindreFromInviter:(UIButton *)sender
{
    NSDictionary *dic = arrSection3[0];
    
    //get chasse item. set for .dictionaryGroup
    
    //get chasse item. set for .dictionaryGroup
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getItemWithKind:MYCHASSE myid:dic[@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        if (!response) {
            return;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSDictionary *dic = [response valueForKey:@"lounge"];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup=dic;
        
        int accessValueNumber = [dic[@"access"] intValue];
        
        NSString *desc=@"";
        NSString *strNameGroup=dic[@"name"];
        
        NSString *strTile=@"";
        if (accessValueNumber == ACCESS_PUBLIC)
        {
            desc =[ NSString stringWithFormat: str(strRejoinLoungPublic)  ,strNameGroup ];
            
            strTile = str(strRejoinAcceptPublic);
            
        }
        else if (accessValueNumber == ACCESS_PRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptPrivate);
        }
        else if (accessValueNumber == ACCESS_SEMIPRIVATE)
        {
            desc =[NSString stringWithFormat:str(strRejoinUnLoung),strNameGroup];
            strTile = str(strRejoinAcceptSemi);
        }
        
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        NSString *strLoungeID = dic[@"id"];
        [LoungeJoinAction putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];

        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            
            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
            [app updateAllowShowAdd:respone];
            
            [COMMON removeProgressLoading];
            
            if([respone isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
            
            viewController1.dicChassesItem =  [GroupEnterOBJ sharedInstance].dictionaryGroup;
            
            viewController1.isInvitionAttend =YES;
            [viewController1 doCallback:^(NSString *strID) {
                //get chasse item. set for .dictionaryGroup
                
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
            }];
            [self pushVC:viewController1 animate:YES];
        };
        
    };
    
}
//MARK: - ENTER
-(void)enterDetailGroupAction:(id)sender
{
    //Expect target...
    
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = tag - 1000;
    NSDictionary *dic = [arrSection3 objectAtIndex:index];
    DetailFromMapVC *viewcontroller1=[[DetailFromMapVC alloc]initWithNibName:@"DetailFromMapVC" bundle:nil];
    viewcontroller1.arrowType = ARROW_GROUP;
    [viewcontroller1 setDataWithPublication:@[dic]];
    [self pushVC:viewcontroller1 animate:YES expectTarget:ISGROUP];
}
-(void)enterDetailAgendaAction:(id)sender
{
    //Expect target...
    
    NSInteger tag = ((UIButton *)sender).tag;
    NSInteger index = tag - 1000;
    NSDictionary *dic = [arrSection3 objectAtIndex:index];
    DetailFromMapVC *viewcontroller1=[[DetailFromMapVC alloc]initWithNibName:@"DetailFromMapVC" bundle:nil];
    viewcontroller1.arrowType = ARROW_AGENDA;
    [viewcontroller1 setDataWithPublication:@[dic]];
    [self pushVC:viewcontroller1 animate:YES expectTarget:ISLOUNGE];
}
-(void)readMoreDidClickedGesture:(UITapGestureRecognizer*)sender
{
    UIView *myContent = sender.view;
    NSInteger index = myContent.tag - 1001;
    NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
    BOOL readmore = ![dicTmp[@"readmore"] boolValue];
    [dicTmp setObject:@(readmore) forKey:@"readmore"];
    [publicationArray replaceObjectAtIndex:index withObject:dicTmp];
    
    [self.tableControl beginUpdates];
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [self.tableControl reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [self.tableControl endUpdates];
}
//MARK: - Share detail view
-(IBAction)shareDetailAction:(UIButton *)sender
{
    NSInteger index =[sender tag]-210;
    NSMutableDictionary *dicTmp = [publicationArray[index] mutableCopy];
    
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[MediaCell class]]) {
        parent = parent.superview;
    }
    
    MediaCell *cell = (MediaCell *)parent;
    [self showShareDetail:self.tableControl withPointView:cell.imgIconShare withData:dicTmp];
}
-(void)showShareDetail:(UIView*)viewSuper withPointView:(UIView*)pointView withData:(NSDictionary*)dic
{
    [shareDetail fnInputData:dic];
    [shareDetail showAlertWithSuperView:viewSuper withPointView:pointView];
    [shareDetail doBlock:^(NSDictionary * _Nonnull dicResult) {
        
    }];
}
-(void)hideShareDetail
{
    if([shareDetail isDescendantOfView:self.tableControl]) {
        [shareDetail hideAlert];
    }
}
@end
