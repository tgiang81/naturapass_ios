//
//  ColorCollectionCell.h
//  Naturapass
//
//  Created by Manh on 11/18/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PdfCollectionCell : UICollectionViewCell
{
}
@property(nonatomic,strong) IBOutlet DACircularProgressView *progressView;

@property(nonatomic,strong) IBOutlet UIView *vColor;
@property(nonatomic,strong) IBOutlet UIImageView *imgThumb;
@property(nonatomic,strong) IBOutlet UILabel *lbCount;
@property(nonatomic,strong) NSIndexPath *myIndex;

@end
