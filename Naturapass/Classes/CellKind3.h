//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface CellKind3 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UIView *viewControl;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintRight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_image_height;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_control_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_control_height;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_view2_width;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_view2_height;

-(void)fnSettingCell:(int)type;
@end
