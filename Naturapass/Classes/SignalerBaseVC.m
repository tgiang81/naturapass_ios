//
//  SignalerBaseVC.m
//  Naturapass
//
//  Created by JoJo on 9/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "SignalerBaseVC.h"
#import "MapGlobalVC.h"

@interface SignalerBaseVC ()

@end

@implementation SignalerBaseVC

- (void)viewDidLoad {
    [self fnSetColor];

    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addMainNav:@"MainNavSignaler"];
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    //Change background color
    switch (self.expectTarget) {
        case ISLIVE:
        {
            [subview.myTitle setText:@"SIGNALER QUOI ?"];

        }
            break;
        case ISCARTE:
        {
            [subview.myTitle setText:@"PUBLIER QUOI ?"];
            //Change background color
            subview.backgroundColor = _colorNavigation;
            subview.imgClose.image = [[UIImage imageNamed:@"icon_chasses_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            subview.imgClose.tintColor = _colorBackGround;

        }
            break;
        default:
        {
            [subview.myTitle setText:@"POSTER QUOI ?"];
            //Change background color
            subview.backgroundColor = _colorNavigation;
            subview.imgBackground.image = [UIImage imageNamed:@"bg_nav_mur_v31"];
            subview.imgClose.image = [[UIImage imageNamed:@"icon_chasses_close"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            subview.imgClose.tintColor = [UIColor whiteColor];
        }
            break;
    }
    [self addSubNav:nil];
    subview.backgroundColor = _colorNavigation;
    self.vContainer.backgroundColor = _colorBackGround;

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: _colorNavigation];
}
-(void)gobackParent
{
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (UIViewController *controller in controllerArray)
    {
        //Code here.. e.g. print their titles to see the array setup;
        
        if ([controller isKindOfClass:[self.mParent class]] ) {
            if ([controller isKindOfClass: [HomeVC class]]) {
                [self doRemoveObservation];
            }
            
            [self.navigationController popToViewController:controller animated:NO];
            return;
        }
    }
}
-(void)fnSetColor
{
    switch (self.expectTarget) {
        case ISLIVE:
        {
            _colorBackGround = UIColorFromRGB(CHASSES_CREATE_BACKGROUND_COLOR);
            _colorNavigation = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
            _colorCellBackGround = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
            _colorAnnuler = UIColorFromRGB(CHASSES_ANNULER_COLOR);
            _colorAttachBackGround = UIColorFromRGB(CHASSES_CREATE_TABLE_COLOR);
            _colorAttachCellBackGround = UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);

        }
            break;
        case ISCARTE:
        {
            _colorBackGround = UIColorFromRGB(CARTE_BACKGROUND_COLOR);
            _colorNavigation = UIColorFromRGB(CARTE_NAV_COLOR);
            _colorCellBackGround = UIColorFromRGB(CARTE_CELL_BACKGROUND_COLOR);
            _colorAnnuler = UIColorFromRGB(CARTE_ANNULER_COLOR);
            _colorAttachBackGround = UIColorFromRGB(CARTE_CELL_BACKGROUND_COLOR);
            _colorAttachCellBackGround = UIColorFromRGB(CARTE_NAV_COLOR);
            
        }
            break;
        default:
        {
            _colorBackGround = UIColorFromRGB(POSTER_BACKGROUND_COLOR);
            _colorNavigation = UIColorFromRGB(POSTER_NAV_COLOR);
            _colorCellBackGround = UIColorFromRGB(POSTER_CELL_BACKGROUND_COLOR);
            _colorAnnuler = UIColorFromRGB(POSTER_ANNULER_COLOR);
            _colorAttachBackGround = UIColorFromRGB(POSTER_BACKGROUND_COLOR);
            _colorAttachCellBackGround = UIColorFromRGB(POSTER_CELL_BACKGROUND_COLOR);

        }
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://HOME
        {
            [self gotoback];
            
        }
            break;
            
        case 1://
        {
            //go to Live hunt screen
                NSArray * controllerArray = [[self navigationController] viewControllers];
                
                for (UIViewController *controller in controllerArray)
                {
                    //Code here.. e.g. print their titles to see the array setup;
                    
                    if ([controller isKindOfClass:[MapGlobalVC class]] ) {
                        [self.navigationController popToViewController:controller animated:TRUE];
                        return;
                    }
                }
            
            //else
            [self.navigationController popToRootViewControllerAnimated:YES];
                
        }
            break;

        default:
            break;
    }
    
}

@end
