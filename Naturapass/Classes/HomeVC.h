//
//  HomeVC.h
//  Naturapass
//
//  Created by Giang on 7/27/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeVC : UIViewController
@property(nonatomic,assign) BOOL isNotifi;
@property (nonatomic,strong) NSString *typeNotifi;
@property (nonatomic,strong) NSString *IdNotifi;
@property (nonatomic,strong) NSDictionary *dicNotifi;

-(void)gotoScreenWithTypeNotification:(NSString*)type IdNoti:(NSString*)IdNoti dicNotif:(NSDictionary*)dic;
@end
