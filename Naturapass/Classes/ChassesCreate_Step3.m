//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step3.h"

//MAP
#import "Publication_Carte.h"

@interface ChassesCreate_Step3 ()
{
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescription;

}
@end

@implementation ChassesCreate_Step3
#pragma mark -setColor
-(void)fnColorTheme
{
    [self.btnSuivant setBackgroundColor:UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
}
#pragma viewdid
- (void)viewDidLoad {
    [super viewDidLoad];
    [self fnColorTheme];
    lblTitleScreen.text = str(strLieuDeRendezVous);
    lblDescription.text = str(strVousPermet);
    [self.btnSuivant setTitle:  str(strSUIVANT) forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)onNext:(id)sender {
    Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
    viewController1.expectTarget = self.expectTarget;
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    
}
@end
