//
//  Parameter_Notification_Detail.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Parameter_Notification_Detail.h"

static NSString *identifierSection1 = @"MyTableViewCell1";


@interface Parameter_Notification_Detail ()
{
    NSMutableArray * arrData;
    IBOutlet UILabel *lbTitle;
}

@end


@implementation Parameter_Notification_Detail

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.nofiticationType == ISSMARTPHONE) {
        lbTitle.text = str(strNOTIFICATIONS_SMARTPHONE);
    }
    else
    {
        lbTitle.text = str(strNOTIFICATIONS_EMAIL);
        
    }    
    arrData = [NSMutableArray new];

    for (NSDictionary*dic in _arrDataFull) {
        [arrData addObject:@{@"name": dic[@"label"],
                             @"image":@"mur_st_ic_message",
                             @"status": dic[@"wanted"] ,
                             @"id": dic[@"id"]
                             }];
    }
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind7" bundle:nil] forCellReuseIdentifier:identifierSection1];
    self.tableControl.estimatedRowHeight = 66;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind7 *cell = nil;
    
    cell = (CellKind7 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    [cell layoutIfNeeded];
    
    
    
    cell.swControl.tag = indexPath.row + 100;
    //Status
    cell.swControl.transform = CGAffineTransformMakeScale(0.75, 0.75);
    [cell.swControl setOnTintColor:UIColorFromRGB(ON_SWITCH_PARAM)];
    [cell.swControl setOn:[dic[@"status"] boolValue]];
    [cell.swControl addTarget:self action:@selector(switchValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)switchValueChanged:(id)sender
{
    UISwitch *sv = (UISwitch*)sender;
    int index = (int)sv.tag - 100;
    NSDictionary *dic = [arrData[index] mutableCopy];
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    WebServiceAPI *serverAPI =[WebServiceAPI new];
    serverAPI.onComplete = ^(id response, int errCode)
    {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        
        if (response) {
            if ([response[@"success"] boolValue]) {
                //change data
                [dic setValue: @(!([dic[@"status"] boolValue])) forKey: @"status" ];
                [arrData replaceObjectAtIndex:index withObject:dic];
                
            }
        }
        NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:index inSection:0];
        NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
        [self.tableControl reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
    };
    
    if (self.nofiticationType == ISSMARTPHONE) {
        [serverAPI putSmartphoneNotificationSetting:@{@"option": dic[@"id"],
                                                        @"value": [NSNumber numberWithBool:sv.isOn]
                                                        }];
    }
    else
    {
        [serverAPI putEmailNotificationSetting:@{@"option": dic[@"id"],
                                             @"value": [NSNumber numberWithBool:sv.isOn]
                                             }];
    }
    
}


@end
