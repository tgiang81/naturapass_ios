//
//  Publication_ChoixSpecNiv4.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_ChoixSpecNiv4.h"
#import "Publication_ChoixSpecNiv5.h"
#import "Publication_ChoixSpecNiv4_elargi.h"
#import "Publication_Partage.h"
#import "Publication_ChoixSpecNiv2.h"

#import "CellKind2.h"
#import "CellKind4.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

static NSString *identifierHeader = @"MyTableViewCell2";

@interface Publication_ChoixSpecNiv4 ()
{
    NSMutableArray * arrData;
    IBOutlet UILabel *lbTitle;

}
@end

@implementation Publication_ChoixSpecNiv4

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isFromNewSignal) {
        MainNavigationBaseView *subview =  [self getSubMainView];
        [subview.myDesc setText:@"Autres..."];
        lbTitle.text = str(strPrecisez_sur_quoi_porte_votre_observation);
        lbTitle.textColor = [UIColor whiteColor];
    }

    
    arrData = [NSMutableArray new];
    
    for (NSDictionary*dic in self.myDic[@"children"]) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            [arrData addObject:dic];
        }
    }
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind4" bundle:nil] forCellReuseIdentifier:identifierHeader];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == arrData.count)
    {
        CellKind4 *vHeader = (CellKind4 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierHeader];
        [vHeader.label1 setText:str(strPlus_danimaux)];
        [vHeader.label1 setFont:FONT_HELVETICANEUE_MEDIUM(18)];
        
        [vHeader.label2 setText:str(strElargir_la_selection)];
        
        if (self.isFromNewSignal) {
            [vHeader.label1 setTextColor:[UIColor whiteColor]];
            [vHeader.label2 setTextColor:[UIColor whiteColor]];
            vHeader.backgroundColor=UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
        }

        [vHeader setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return vHeader;
    }
    
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    cell.constraint_image_width.constant = cell.constraint_image_height.constant = 0;
    
    //    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    //
    //    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString: imgUrl ] ];
    
    //FONT
    
    if (self.isFromNewSignal) {
        cell.label1.text = dic[@"name"];
        [cell.label1 setTextColor:[UIColor whiteColor]];
        [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
        
        //Arrow
        UIImageView *btn = [UIImageView new];
        [btn setImage: [UIImage imageNamed:@"icon_sign_arrow_right" ]];
        
        btn.contentMode = UIViewContentModeScaleAspectFit;
        
        cell.constraint_control_width.constant = 10;
        cell.constraint_control_height.constant = 20;
        
        btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
        
        cell.constraintRight.constant = 15;
        
        
        [cell.viewControl addSubview:btn];
        
        cell.backgroundColor=UIColorFromRGB(CHASSES_CREATE_NAV_COLOR);
    }else{
        cell.label1.text = dic[@"name"];
        [cell.label1 setTextColor:[UIColor blackColor]];
        [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(18)];
        
        //Arrow
        UIImageView *btn = [UIImageView new];
        [btn setImage: [UIImage imageNamed:@"arrow_icon" ]];
        
        btn.contentMode = UIViewContentModeScaleToFill;
        
        cell.constraint_control_width.constant = 8;
        cell.constraint_control_height.constant = 13;
        
        btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
        
        cell.constraintRight.constant = 15;
        
        
        [cell.viewControl addSubview:btn];
        
        cell.backgroundColor=[UIColor whiteColor];
    }

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == arrData.count)
    {
        
        //PLUS
        
        [self fnEnlarge];
        
        return;
    }
    NSDictionary *dic = arrData[indexPath.row];

    
    NSString *strPath = [FileHelper pathForApplicationDataFile: [NSString stringWithFormat:@"%ld.json",(long)indexPath.row] ];
    [dic writeToFile:strPath atomically:YES];

    ASLog(@"%@",strPath);
//

    //Specific
    
    if ( ( (NSArray*)dic[@"children"]).count > 0 ) {
        
        //has search option
        if ([dic[@"search"] intValue] ==  1)
        {
            Publication_ChoixSpecNiv4 *viewController1 = [[Publication_ChoixSpecNiv4 alloc] initWithNibName:@"Publication_ChoixSpecNiv4" bundle:nil];
            
            viewController1.myDic = dic;
            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];

            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

            
        }else{
            Publication_ChoixSpecNiv2 *viewController1 = [[Publication_ChoixSpecNiv2 alloc] initWithNibName:@"Publication_ChoixSpecNiv2" bundle:nil];
            viewController1.isFromNewSignal = self.isFromNewSignal;

            viewController1.myDic = dic;
            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];

        }
    }else{
        //Show card....
        if ( dic[@"card"] ) {
            //LEAF
            Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
            viewController1.myDic = dic;
            viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
            viewController1.iSpecific =  0;//[self.myDic[@"search"] intValue] ;
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }else{
            
            NSString *strPrecision =[NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
            NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
            if (strPrecision) {
                [dicFavo setValue:strPrecision forKey:@"category_tree"];
            }
            
            NSDictionary *dicObservation = @{
                                             @"specific": [NSNumber numberWithInt: 0 ],
                                             @"category" : dic[@"id"]
                                             };
            
            [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
            [dicFavo setValue:dicObservation forKey:@"observation"];
            if ([self.myDic[@"receivers"] isKindOfClass: [NSArray class]]) {
                [PublicationOBJ sharedInstance].arrReceivers = self.myDic[@"receivers"];
                [dicFavo setValue:dic[@"receivers"] forKey:@"receivers"];

            }
            [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];

            //edit ob
            if ([PublicationOBJ sharedInstance].isEditer) {
                [[PublicationOBJ sharedInstance] modifiPublication:self withType:EDIT_PRECISIONS];
                
            }
            else if ([PublicationOBJ sharedInstance].isEditFavo)
            {
                [self backEditFavo];
            }
            else
            {
                //
                /*
                 GROUP
                 When we publish, the sharing should be directly with the checked group => i think that we should not ask the question (take off question page = directly sharing)
                 => OK, we can  take off question page and select the group for sharing.
                 */
                
                Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
        }
        
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//
//    return 50;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//
//    CellKind4 *vHeader = (CellKind4 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierHeader];
//    vHeader.contentView.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
//
//    [vHeader.label1 setText:@"Plus d'animaux"];
//    [vHeader.label2 setText:@"Elargir la sélection"];
//    [vHeader.btnControl addTarget:self action:@selector(fnEnlarge) forControlEvents:UIControlEventTouchUpInside];
//    vHeader.imageIcon.image  = [UIImage imageNamed:@"ic_add_more"];
//    vHeader.constraint_image_width.constant = vHeader.constraint_image_height.constant = 40;
//
//
//    //Arrow
//    UIImageView *btn = [UIImageView new];
//    [btn setImage: [UIImage imageNamed:@"arrow_icon" ]];
//
//
//    btn.contentMode = UIViewContentModeScaleToFill;
//
//    vHeader.constraint_control_width.constant = 8;
//    vHeader.constraint_control_height.constant = 13;
//
//    btn.frame = CGRectMake(0, 0, vHeader.constraint_control_width.constant, vHeader.constraint_control_height.constant);
//
//
//    vHeader.constraintRight.constant = 15;
//
//    [vHeader.viewControl addSubview:btn];
//
//
//
//
//    return  vHeader;
//
//}

-(void) fnEnlarge
{
    Publication_ChoixSpecNiv4_elargi *viewController1 = [[Publication_ChoixSpecNiv4_elargi alloc] initWithNibName:@"Publication_ChoixSpecNiv4_elargi" bundle:nil];
    viewController1.myDic = self.myDic;
    viewController1.myName = [NSString stringWithFormat:@"%@",self.myName];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}

@end
