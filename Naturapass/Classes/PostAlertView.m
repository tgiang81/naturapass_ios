//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PostAlertView.h"
#import "Define.h"
@implementation PostAlertView
{
    
}

#pragma mark - viewdid
-(void)viewDidLoad
{
    [super viewDidLoad];
    subAlertView.layer.cornerRadius=3.0f;
    subAlertView.layer.masksToBounds=YES;}

#pragma callback
-(void)setCallback:(PostAlertViewCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(PostAlertViewCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark-  action
-(IBAction)fnMessage:(id)sender
{
    if (_callback) {
        [self dismissViewControllerAnimated:YES completion:nil];
        _callback(POST_MESSAGE);
    }
}
#pragma mark - photo/video
-(IBAction)photoButtonAction:(id)sender
{
    if (_callback) {
        [self dismissViewControllerAnimated:YES completion:nil];
        _callback(POST_PHOTO);
    }
}
-(IBAction)videoButtonAction:(id)sender
{
    
    if (_callback) {
        [self dismissViewControllerAnimated:YES completion:nil];
        _callback(POST_VIDEO);
    }
}
-(IBAction)fnClose:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
