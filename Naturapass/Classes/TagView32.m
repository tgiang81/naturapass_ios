//
//  TagView32.m
//  Naturapass
//
//  Created by Giang on 3/2/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "TagView32.h"
#import "ContactObject.h"
#import "MBContactPicker.h"

@interface TagView32 () <MBContactPickerDataSource, MBContactPickerDelegate>
{
    NSInteger _index;
}
@property (nonatomic) NSArray *listAll;
@property (nonatomic) NSArray *selectedContacts;
@property (nonatomic) NSString *strIDSelected;
@property (nonatomic) NSMutableArray *listCheck;

@property (strong, nonatomic) IBOutlet MBContactPicker *contactPickerView;
@property (strong, nonatomic) IBOutlet UITextField *promptTextField;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *contactPickerViewHeightConstraint;
@end

@implementation TagView32


-(void) showViewWithData:(NSArray*)arrContent withArrSelected:(NSArray*)arrSelected withIndex:(NSInteger)index
{
    _index = index;
    _listCheck = [NSMutableArray new];
    NSMutableArray *arrFull = [NSMutableArray new];
    NSMutableArray *arrCheck = [NSMutableArray new];

    for (NSDictionary*aDic in arrContent) {
        MBContactModel *model = [[MBContactModel alloc] init];
        model.contactTitle = aDic[@"name"];
        model.contactValue = aDic;
        [arrFull addObject:model];
        for (NSString *strCheck in arrSelected) {
            if ([aDic[@"id"] intValue] ==[strCheck intValue] ) {
                [arrCheck addObject:model];
                break;
            }
        }
    }
    
    //test
    if (arrCheck.count>0) {
        self.selectedContacts = arrCheck;
        self.listCheck = [NSMutableArray arrayWithArray:arrCheck];
    }
    

    [_contactPickerView fnResetData];

    
    self.listAll = arrFull;

    //update new datadelegate from listAll
    [_contactPickerView reloadData];
    
    //auto show list full
    
    [_contactPickerView autoShow];
    int iCount =0;
    int min = 2;
    int max = 5;
    if (self.listAll.count<min) {
        iCount= min;
    }
    else if (self.listAll.count>max)
    {
        iCount =max;
    }
    else
    {
        iCount = (int)self.listAll.count;
    }
    self.constraintHeightViewBorder.constant = iCount*40+100;
}

-(void)awakeFromNib{
    [super awakeFromNib];

    [self setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [self setBackgroundColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.5]];
    [self.layer setMasksToBounds:YES];
    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.viewBoder.layer setMasksToBounds:YES];
    self.viewBoder.layer.cornerRadius= 5.0;
    self.viewBoder.layer.borderWidth =0.5;
    self.viewBoder.layer.borderColor = [[UIColor lightGrayColor] CGColor];

    self.contactPickerView.delegate = self;
    self.contactPickerView.datasource = self;
    
    self.promptTextField.text = self.contactPickerView.prompt;
    [self.promptTextField addTarget:self action:@selector(promptTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
}

#pragma mark - MBContactPickerDataSource

- (NSArray *)contactModelsForContactPicker:(MBContactPicker*)contactPickerView
{
    return self.listAll;
}

- (NSArray *)selectedContactModelsForContactPicker:(MBContactPicker*)contactPickerView
{
    return self.selectedContacts;
}

#pragma mark - MBContactPickerDelegate

- (void)contactCollectionView:(MBContactCollectionView*)contactCollectionView didSelectContact:(id<MBContactPickerModelProtocol>)model
{

}

- (void)contactCollectionView:(MBContactCollectionView*)contactCollectionView didAddContact:(id<MBContactPickerModelProtocol>)model
{
    [self.listCheck addObject:model];
}

- (void)contactCollectionView:(MBContactCollectionView*)contactCollectionView didRemoveContact:(id<MBContactPickerModelProtocol>)model
{
    MBContactModel *modelValue = (MBContactModel*)model;
    for (int i =0; i<self.listCheck.count; i++) {
        MBContactModel *mod= self.listCheck[i];
        if ([mod.contactValue[@"value"] intValue]== [modelValue.contactValue[@"value"]intValue]) {
            [self.listCheck removeObjectAtIndex:i];
            break;
        }
    }
}

// This delegate method is called to allow the parent view to increase the size of
// the contact picker view to show the search table view
- (void)didShowFilteredContactsForContactPicker:(MBContactPicker*)contactPicker
{
    //    if (self.contactPickerViewHeightConstraint.constant <= contactPicker.currentContentHeight)
    //    {
    //        [UIView animateWithDuration:contactPicker.animationSpeed animations:^{
    //            CGRect pickerRectInWindow = [self.view convertRect:contactPicker.frame fromView:nil];
    //            CGFloat newHeight = self.view.window.bounds.size.height - pickerRectInWindow.origin.y - contactPicker.keyboardHeight;
    //            self.contactPickerViewHeightConstraint.constant = newHeight;
    //            [self.view layoutIfNeeded];
    //        }];
    //    }
}

// This delegate method is called to allow the parent view to decrease the size of
// the contact picker view to hide the search table view
- (void)didHideFilteredContactsForContactPicker:(MBContactPicker*)contactPicker
{
    //    if (self.contactPickerViewHeightConstraint.constant > contactPicker.currentContentHeight)
    //    {
    //        [UIView animateWithDuration:contactPicker.animationSpeed animations:^{
    //            self.contactPickerViewHeightConstraint.constant = contactPicker.currentContentHeight;
    //            [self.view layoutIfNeeded];
    //        }];
    //    }
}

// This delegate method is invoked to allow the parent to increase the size of the
// collectionview that shows which contacts have been selected. To increase or decrease
// the number of rows visible, change the maxVisibleRows property of the MBContactPicker
- (void)contactPicker:(MBContactPicker*)contactPicker didUpdateContentHeightTo:(CGFloat)newHeight
{
    //    self.contactPickerViewHeightConstraint.constant = newHeight;
    //    [UIView animateWithDuration:contactPicker.animationSpeed animations:^{
    //        [self.view layoutIfNeeded];
    //    }];
}

// This delegate method is invoked when the user types a contact that is not in the original
// list provided to the contact picker.  Specifically this is triggered when the user presses
// the 'return' key on the keyboard.
- (void)contactPicker:(MBContactPicker *)contactPicker didEnterCustomText:(NSString *)text
{
    MBContactModel *model = [[MBContactModel alloc] init];
    model.contactTitle = text;
    [self.contactPickerView addToSelectedContacts:model];
}

// This delegate method allows custom filtering by providing a predicate that filters
// MBContactModel or a custom subclass that you have created. If this is not declared
// then the default filtering implementation is used.
- (NSPredicate*) customFilterPredicate:(NSString *)searchString
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"contactTitle BEGINSWITH[cd] %@", searchString];
    return predicate;
}



#pragma callback

-(void)setCallback:(callBackAlert)callback
{
    _CallBack=callback;
}
#pragma callback
-(void)doBlock:(callBackAlert ) cb
{
    self.CallBack = cb;
    
}
-(IBAction)fnCalcel:(id)sender
{
    [self removeFromSuperview];
}

-(IBAction)fnOK:(id)sender
{
    if (self.CallBack) {
        NSMutableArray *arrData = [ NSMutableArray new];
        for (MBContactModel *model in self.listCheck) {
            [arrData addObject:@{@"value":model.contactValue[@"id"],@"text":model.contactValue[@"name"]}];
        }
        self.CallBack(_index,@{@"arrData":arrData});
    }
    [self removeFromSuperview];
}

@end
