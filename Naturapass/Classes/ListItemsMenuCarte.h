//
//  ListItemsMenuCarte.h
//  Naturapass
//
//  Created by giangtu on 1/17/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^callMasterFilter) (MASTER_FILTER_TYPE iType);

@interface ListItemsMenuCarte : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>
{
    IBOutlet UICollectionView *collectionView;

}
@property (nonatomic, copy) callMasterFilter callbackMasterFilter;
-(void) fnResetAll;

@end
