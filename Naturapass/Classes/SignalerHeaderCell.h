//
//  SignalerHeaderCell.h
//  Naturapass
//
//  Created by JoJo on 9/27/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignalerHeaderCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbTitle;

@end
