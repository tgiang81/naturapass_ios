//
//  MapDataDownloader.h
//  TestMap
//
//  Created by Giang on 10/2/15.
//  Copyright © 2015 PHS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "Define.h"

#import "AppCommon.h"

typedef void (^callBackDonePublication) ();
typedef void (^callBackDoneShape) ();
typedef void (^callBackEditShape) ();

@interface MapDataDownloader : NSObject
{
    WebServiceAPI *serviceObj;
    BOOL isGettingTree;
    
}
@property (nonatomic, strong) NSOperationQueue * operationQueue;
//@property (nonatomic, retain) AFHTTPRequestOperation *op;
+ (MapDataDownloader *) sharedInstance;

@property (nonatomic, copy) callBackDonePublication CallBackPublication;
@property (nonatomic, copy) callBackDoneShape CallBackShape;
@property (nonatomic, copy) callBackEditShape CallBackEditShape;

-(void)fnGetSqliteForAllPointsInMap :(NSArray*) arrTypes;
-(void) resetParam;
-(void) doUpdateTableDBVersion;
-(void) doUpdateFavorites;
-(void) doUpdateDogsWeapon;
-(void) fnGetObservationTree;
-(void) doUpdateTree:(NSDictionary*) response;
-(void) doUpdateTableDBAgenda;
@end
