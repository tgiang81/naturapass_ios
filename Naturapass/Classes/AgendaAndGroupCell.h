//
//  AgendaAndGroupCell.h
//  Naturapass
//
//  Created by manh on 11/25/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgendaAndGroupCell : UITableViewCell
@property(nonatomic,strong) IBOutlet UIButton *btnAgenda;
@property(nonatomic,strong) IBOutlet UIImageView *imgArrowAgenda;
@property(nonatomic,strong) IBOutlet UILabel *lbAgenda;
@property(nonatomic,strong) IBOutlet UIView *vGroup;

@property(nonatomic,strong) IBOutlet UIButton *btnGroup;
@property(nonatomic,strong) IBOutlet UIImageView *imgArrowGroup;
@property(nonatomic,strong) IBOutlet UILabel *lbGroup;
@property(nonatomic,strong) IBOutlet UIView *vAgenda;
@end
