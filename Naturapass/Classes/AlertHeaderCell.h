//
//  AlertHeaderCell.h
//  Naturapass
//
//  Created by GiangTT on 12/20/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *text;

@end
