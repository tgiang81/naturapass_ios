//
//  AlertBaseVC.h
//  Naturapass
//
//  Created by Manh on 8/24/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
typedef void (^onDataBack)(NSDictionary*);

@interface AlertBaseVC : UIViewController
@property (nonatomic,strong) IBOutlet UIView *vHeader;
@property (nonatomic,strong) IBOutlet UIView *vContraint;
@property (nonatomic,strong) IBOutlet UIView *vContent;
@property (nonatomic,strong) IBOutlet UIView *vSubContent;
@property (nonatomic,strong) IBOutlet UIButton *btnClose;
@property (nonatomic,strong) IBOutlet UIButton *btnValider;

@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeight;


- (id)initWithNibName;
-(IBAction)closeAction:(id)sender;
-(void)addContraintView:(UIView*)view;
-(void)showInVC:(UIViewController*)vc;
@property (nonatomic, copy) onDataBack myCallback;
@property (nonatomic,assign) ISSCREEN expectTarget;
@end
