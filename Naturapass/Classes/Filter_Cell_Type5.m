//
//  TheProjectCell.m
//  The Projects
//
//  Created by Ahmed Karim on 1/11/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import "Filter_Cell_Type5.h"

@interface Filter_Cell_Type5()
{
    __weak IBOutlet NSLayoutConstraint *contraintPaddingLeft;
}
@property (nonatomic) BOOL isExpanded;

@end

@implementation Filter_Cell_Type5

#pragma mark - Draw controls messages
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.cellLabel.preferredMaxLayoutWidth = CGRectGetWidth(self.cellLabel.frame);
}
- (void)drawRect:(CGRect)rect
{
    NSMutableDictionary *dic = [self.treeNode.nodeObject mutableCopy];
    int level = [dic[@"level"] intValue];
    int indentation = level * 15 + 20;
    contraintPaddingLeft.constant=indentation;
}

- (void)setTheButtonBackgroundImage:(UIImage *)backgroundImage
{
    [self.cellButton setBackgroundImage:backgroundImage forState:UIControlStateNormal];
}

- (IBAction)expand:(id)sender
{
    NSMutableDictionary *dic = [self.treeNode.nodeObject mutableCopy];
    [dic setObject:@(![dic[@"status"] boolValue]) forKey:@"status"];
    self.treeNode.nodeObject = dic;

    [self setSelected:NO];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ProjectTreeNodeButtonClicked" object:self];
}
@end
