//
//  ChatLiveHuntView.m
//  Naturapass
//
//  Created by Manh on 12/15/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ChatLiveHuntView.h"
#import "Naturapass-Swift.h"
#import "Config.h"
#import "FileHelper.h"
#import "AppCommon.h"
#include "CommonObj.h"
#import "LiveHuntOBJ.h"
#import "KSToastView.h"
#import "MessageWaitToSend.h"
#import "GroupEnterOBJ.h"
#import "MapDataDownloader.h"
#import "SVPullToRefresh.h"
#import "ChatLiveHuntObsTextCell.h"
#import "CommonHelper.h"
#import "ServiceHelper.h"

static NSString *identifierLiveCell = @"LiveCellID";
//static NSString *identifierLiveObsCell = @"LiveCellIDObs";
//static NSString *identifierLiveObsTextCell = @"LiveCellIDObsText";

//static int minHeight =44;
//static int maxHeight =33*3;
//static int detal =2;

@implementation ChatLiveHuntView
{
    NSMutableArray *arrDataDiscussions;
    NSArray *arrDataDiscussions_Header;
    SocketIOClient* socket;
    NSString *strUserID;
    BOOL animateHeightChange;
    NSTimeInterval animationDuration;
    __weak IBOutlet NSLayoutConstraint *constraintMessageHeight;
    __weak IBOutlet NSLayoutConstraint *constraintMessageView;
    int                             kbHeight;
    UIBarButtonItem             *doneBarItem;
    UIBarButtonItem             *spaceBarItem;
    UIBarButtonItem             *previousBarItem;
    UIBarButtonItem             *nextBarItem;
    NSDictionary *dicLiveHunt;
    BOOL isFisrtLoad;
    IBOutlet UIImageView *imgSend;
    IBOutlet UIButton *btnIc_message;
    NSInteger indexSelected;
    SocketManager* manager;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startRefreshControl) name: NOTIFY_REFRESH_MES_NEW object:nil];

//    self.view.layer.cornerRadius= 10.0;
//    self.view.layer.borderWidth =0.5;
//    self.view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    indexSelected = -1;
    self.tableControl.estimatedRowHeight = 50;
    dicLiveHunt =[LiveHuntOBJ sharedInstance].dicLiveHunt;
    arrDataDiscussions = [NSMutableArray new];
    [self connectMapLive];
    strUserID = [NSString stringWithFormat:@"%@",[COMMON getUserId]];
    [self.tableControl reloadData];
    [self loadDataCache];
    //    [self getLiveChatWithMore:NO];
    
    [self initRefreshControl];
    isFisrtLoad =YES;
    [self getLiveChatWithMore:NO];

}



#pragma mark - REFERSH CONTROL

-(void) initRefreshControl
{
    __weak ChatLiveHuntView *weakSelf = self;
    [self.tableControl addPullToRefreshWithActionHandler:^{
        [weakSelf insertRowAtTop];
    }];
    [self.tableControl addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
    }];
    self.tableControl.infiniteScrollingView.deltaScrollOffset = 150;
}
-(void)startRefreshControl
{
    [self.tableControl triggerPullToRefresh];
    
}
-(void)moreRefreshControl
{
    [self.tableControl triggerInfiniteScrolling];
    
}
-(void)stopRefreshControl
{
    [self.tableControl.pullToRefreshView stopAnimating];
    [self.tableControl.infiniteScrollingView stopAnimating];
}
//overwrite
- (void)insertRowAtTop {
    [self getLiveChatWithMore:YES];
}

//overwrite
- (void)insertRowAtBottom {
    [self getLiveChatWithMore:NO];
}


#pragma mark -cache
-(void)loadDataCache
{
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(strUserID,LIVE_MAP_CHAT_SAVE)];
    NSDictionary *dicDataCache = [NSDictionary dictionaryWithContentsOfFile:strPath];
    if ([dicDataCache[@"hunt_id"] intValue]== [dicLiveHunt[@"id"] intValue]) {
        NSArray *arrTmp = dicDataCache[@"data"];
        arrDataDiscussions = [arrTmp mutableCopy];
        [self.tableControl reloadData];
        [self scrollToBottomWithIndexAuto:nil];
    }
    
}
-(void)saveCacheWithArr:(NSArray*)arrData
{
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(strUserID,LIVE_MAP_CHAT_SAVE)];
    NSDictionary *dicCache = @{@"hunt_id":dicLiveHunt[@"id"],@"data":arrData};
    // Write array
    [dicCache writeToFile:strPath atomically:YES];
}
-(void)removeItemWithPublication:(NSDictionary*)dicPublication
{
    NSString *publicationID = [dicPublication[@"id"] stringValue];
    //update in cache Mur pub
    
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring(strUserID,LIVE_MAP_CHAT_SAVE)];
    NSDictionary *dicDataCache = [NSDictionary dictionaryWithContentsOfFile:strPath];
    if ([dicDataCache[@"hunt_id"] intValue]== [dicLiveHunt[@"id"] intValue]) {
        NSArray *arrTmp = dicDataCache[@"data"];
        NSMutableArray *mutArr = [arrTmp mutableCopy];
        for (NSDictionary*mDic in mutArr) {
            if ([mDic[@"id"] intValue] == [publicationID intValue]) {
                [mutArr removeObject:mDic];
                NSDictionary *dicCache = @{@"hunt_id":dicLiveHunt[@"id"],@"data":mutArr};
                // Write array
                [dicCache writeToFile:strPath atomically:YES];

                arrDataDiscussions = [mutArr mutableCopy];
                [self.tableControl reloadData];
                break;
            }
        }

    }

}

#pragma mark - API
-(void)getLiveChatWithMore:(BOOL)isMore
{
    if ([COMMON isReachable] == NO)
    {
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        return;
    }

    NSString *countData =@"0";
    if (isMore) {
        countData =[NSString stringWithFormat:@"%d",(int)arrDataDiscussions.count];
    }
    WebServiceAPI *serviceAPI = [WebServiceAPI new];
    [serviceAPI fnGET_LIVE_HUNT_PUBLICATION_DISCUSSION:dicLiveHunt[@"id"] limit:@"20" offset:countData];
    serviceAPI.onComplete = ^(id response, int errCode)
    {
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        if([self fnCheckResponse:response]) return;
        if(errCode == 200 && [response isKindOfClass:[NSArray class]])
        {
            if (!isMore) {
                [arrDataDiscussions removeAllObjects];
                [self.tableControl reloadData];
            }
            NSArray *arrTmp= (NSArray*)response;
            NSMutableArray *arrData = [NSMutableArray arrayWithArray:arrDataDiscussions];
            if (isMore) {

                for (int  i = 0; i<arrTmp.count; i++) {
                    if (arrData.count == 0) {
                        [arrData addObject:arrTmp[i]];
                    }
                    else
                    {
                        [arrData insertObject:arrTmp[i] atIndex:0];
                    }
                }
            }
            else
            {
                if (arrTmp.count>0) {
                    for (int i = (int)(arrTmp.count -1); i>=0; i--) {
                        [arrData addObject:arrTmp[i]];
                    }
                }
            }
            [arrDataDiscussions removeAllObjects];
            [self fnAddHeader:arrData];
            [self.tableControl reloadData];
            
            if (!isMore && arrData.count>0) {
                [self scrollToBottomWithIndexAuto:nil];
            }
        }
        
    };
}
-(BOOL)fnCheckResponse:(NSDictionary*)response
{
    if ([response isKindOfClass:[NSDictionary class]]) {
        if ([response[@"message"] isKindOfClass:[NSString class]]) {
//            [KSToastView ks_showToast: response[@"message"] duration:2.0f completion: ^{
//            }];
            return YES;
        }
    }
    return NO;
}
-(void)fnAddHeader:(NSArray*)array
{
    // neu arrData  == trong thi set phan tu dau tien co head
    //kiem tra phan tu tiep theo neu type giong voi phan tu hien tai thi khong them head nguoc lai thi them head
    //check if it exist...
    
    for (int i= 0; i<array.count; i++)
    {
        NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:array[i]];
        
        BOOL isExist = NO;
        
        for (NSDictionary*mDic in arrDataDiscussions) {
            if ([[mDic[@"id"] stringValue] isEqualToString: [dic[@"id"] stringValue]] ) {
                //exist
                isExist = YES;
                break;
            }
        }
        
        if (isExist) {
            continue;
        }else{
            if (arrDataDiscussions.count>0)
            {
                NSDictionary *dicData = [arrDataDiscussions lastObject];
                NSString *typeDate= dicData[@"type"];
                //            if ([typeDate isEqualToString:PublicationTextType]) {
                //                typeDate = PublicationType;
                //            }
                
                if ([typeDate isEqualToString:dic[@"type"]]) {
                    [dic setValue:@0 forKey:@"head"];
                }
                else
                {
                    [dic setValue:@1 forKey:@"head"];
                }
                
            }
            else
            {
                [dic setValue:@1 forKey:@"head"];
            }
            
            [arrDataDiscussions addObject:dic];

        }
    }
    
    // Path to save array data
    [self saveCacheWithArr:arrDataDiscussions];
    
    
}
-(void) willMoveToParentViewController:(UIViewController *)parent
{
    [super willMoveToParentViewController:parent];
    if (!parent) {
        [self closeSock];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFY_REFRESH_MES_NEW object:nil];
    }
}

#pragma mark - socket
-(void) closeSock
{
    [socket disconnect];
}

-(void) connectMapLive
{
    NSURL* url = [[NSURL alloc] initWithString:LINK_SOCKET];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    socket = manager.defaultSocket;

    [socket onAny:^(SocketAnyEvent * test) {
        
                NSLog(@"EVENT: %@ -- count %d",test.event, (int)test.items.count);
        
    }];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        
        [socket emit:@"npevent-user:connected" with:@[      @{@"usertag":userTag }]];
        //reg lounge
        NSString *loungetag = dicLiveHunt[@"loungetag"] ;
        [socket emit:@"npevent-map-live:join" with:@[loungetag?loungetag:@""]];
        
    } ];
    //reg message
    [socket on:@"npevent-lounge:message" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //        NSLog(@"DEF");
        if (data.count > 0) {

            
            NSError* error = nil;
            id dataInput = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];

            
            NSString *strISOLatin = [[NSString alloc] initWithData:dataInput encoding:NSUTF8StringEncoding];
            NSString *correctString = [NSString stringWithCString:[strISOLatin cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

            NSData *dataUTF8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
            
            id dataOuput = [NSJSONSerialization JSONObjectWithData:dataUTF8 options:0 error:&error];
            
            NSMutableDictionary *dic =[dataOuput[0] mutableCopy];
            [dic setValue:@"message" forKey:@"type"];

            
            /*
            NSString *correctString = [NSString stringWithCString:[dic[@"content"] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

            [dic setValue:@"message" forKey:@"type"];
            if (correctString) {
                [dic setValue:correctString forKey:@"content"];
            }
            NSMutableDictionary *owner = [dic[@"owner"] mutableCopy];
            NSString *fullname = [NSString stringWithCString:[owner[@"fullname"]
                                                              cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            if(fullname)
            {
                [owner setObject:fullname forKey:@"fullname"];
                [dic setObject:owner forKey:@"owner"];
            }
*/
            //            NSLog(@"MESSAGE FUNCTION:  %@", data[0]);
            
//            if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
//                return;
//            }
            
            //check wrong...need verify again...
            
//            if (self.allow_show_chat == NO) {
//
//                BOOL isAdmin = NO;
//
//                if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
//                    if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
//                    {
//                        isAdmin = YES;
//                        //admin
//                    }
//                }
//
//                if (isAdmin == NO) {
//                    return;
//                }
//
//            }
            
            //Message Type
            [self fnAddHeader:@[dic]];
            [self.tableControl reloadData];

            //make appear bubble in real time
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:arrDataDiscussions.count-1 inSection:0];
            // scroll to bottom to get latest
            [self scrollToBottomWithIndexAuto:indexPath];
            if (!_showPopoutChat) {
                [CommonObj sharedInstance].nbLiveChat+=1;
            }
            
        }
    } ];
    
    //reg publication
    [socket on:@"npevent-lounge:publication" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //        NSLog(@"DEF");
        if (data.count > 0) {
            
            
//            if (self.allow_show == NO) {
//
//                BOOL isAdmin = NO;
//
//                if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
//                    if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
//                    {
//                        isAdmin = YES;
//                        //admin
//                    }
//                }
//
//                if (isAdmin == NO) {
//                    return;
//                }
//
//            }
            
            
            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_publication]];
            
            NSError* error = nil;
            id dataInput = [NSJSONSerialization dataWithJSONObject:data options:0 error:&error];
            
            
            NSString *strISOLatin = [[NSString alloc] initWithData:dataInput encoding:NSUTF8StringEncoding];
            NSString *correctString = [NSString stringWithCString:[strISOLatin cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            
            NSData *dataUTF8 = [correctString dataUsingEncoding:NSUTF8StringEncoding];
            
            id dataOuput = [NSJSONSerialization JSONObjectWithData:dataUTF8 options:0 error:&error];
            
            NSMutableDictionary *dic =[dataOuput[0] mutableCopy];
            [dic setValue:@"publication" forKey:@"type"];

            /*
            [dic setValue:@"publication" forKey:@"type"];
            NSString *correctString = [NSString stringWithCString:[dic[@"content"] cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];

            if (correctString) {
                [dic setValue:correctString forKey:@"content"];
            }
            NSMutableArray *arrObservation = [dic[@"observations"] mutableCopy];
            
            if (arrObservation.count > 0)
            {
                NSMutableDictionary *dicObservation = [arrObservation[0] mutableCopy];
                
                NSArray*arrTree = dicObservation[@"tree"];
                NSMutableArray *arrTmp = [NSMutableArray new];
                for (id strTree in arrTree) {
                    NSString *correctObs = [NSString stringWithCString:[strTree
                                                                        cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
                    if (correctObs) {
                        [arrTmp addObject:correctObs];
                    }
                }
                if (arrTmp.count > 0) {
                    [dicObservation setValue:arrTmp forKey:@"tree"];
                    [arrObservation replaceObjectAtIndex:0 withObject:dicObservation];
                    [dic setValue:arrObservation forKey:@"observations"];

                }
            }
            NSMutableDictionary *owner = [dic[@"owner"] mutableCopy];
            NSString *fullname = [NSString stringWithCString:[owner[@"fullname"]
                                                              cStringUsingEncoding:NSISOLatin1StringEncoding] encoding:NSUTF8StringEncoding];
            if(fullname)
            {
                [owner setObject:fullname forKey:@"fullname"];
                [dic setObject:owner forKey:@"owner"];
            }
*/
            //            NSLog(@"PUBLICATION FUNCTION:  %@", data[0]);
            
            //Message Type
            [self fnAddHeader:@[dic]];
            
            [self.tableControl reloadData];

            //make appear bubble in real time
            NSIndexPath *indexPath=[NSIndexPath indexPathForRow:arrDataDiscussions.count-1 inSection:0];
            // scroll to bottom to get latest
            [self scrollToBottomWithIndexAuto:indexPath];

            if (!_showPopoutChat) {
                [CommonObj sharedInstance].nbLiveMur+=1;
            }
            
        }
    } ];
    
    [socket connect];
}

#pragma mark - table delegate

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = nil;
    dic = [arrDataDiscussions objectAtIndex:indexPath.row];
    
    if ([cellTmp isKindOfClass:[ChatLiveHuntBaseCell class]])
    {
        ChatLiveHuntCell *cell = (ChatLiveHuntCell *)cellTmp;
        if (dic) {
            
            [cell assignValues:dic];
        }
        
        [cell setThemeWithFromScreen:ISLIVEMAP];
        
        if ([dic[@"head"] boolValue]) {
            cell.contraintHeightHeader.constant =21;
        }
        else
        {
            cell.contraintHeightHeader.constant =0;
            
        }
        if(indexSelected == indexPath.row)
        {
            cell.imgCicle.image = [[UIImage imageNamed:@"live_cycle_bg"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            cell.imgCicle.tintColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
        }
        else
        {

            cell.imgCicle.image = [UIImage imageNamed:@"live_cycle_bg"];

        }

        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell layoutIfNeeded];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDataDiscussions.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = nil;
    dic = [arrDataDiscussions objectAtIndex:indexPath.row];

    ChatLiveHuntCell *cell = (ChatLiveHuntCell *)[tableView dequeueReusableCellWithIdentifier:identifierLiveCell];
    
//    cell = [self.tableControl dequeueReusableCellWithIdentifier:identifierLiveCell forIndexPath:indexPath];

    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
    
}

//append a cell -> act like button
-(void) fnAutoSelectCellAtIndexPath:(NSIndexPath *) indexPath
{
    indexSelected = indexPath.row;
    [self.tableControl reloadData];
    UITableViewCell *topCell = [self.tableControl cellForRowAtIndexPath: indexPath];
    CGPoint p = [topCell.superview convertPoint:topCell.center toView:self.view];
    
    NSLog(@"row height : %f", p.y);
    
    if (_callbackLiveChatview) {
        
        NSDictionary *dic = arrDataDiscussions[indexPath.row];
        if (_callbackLiveChatview) {
            //Check if it's Chat or Publication
            _callbackLiveChatview(@{@"type":DetailType,
                                    @"data":dic,
                                    @"pos": @(p.y - topCell.bounds.size.height/2),
                                    @"selected": @(1),
                                    @"socket": @(1)
                                    });
            
            
            if ([dic[@"type"] isEqualToString:PublicationType ]) {
                //go detail publication
                //                _callbackLiveChatview(@{@"type":DetailType,@"data":dic});
                //position to show callout !
                
            }else{
                //Chat live
                //  _callbackLiveChatview(@{@"type": ...});
            }
        }
        
    }

}
-(void)unSelectedItem
{
    indexSelected = -1;
    [self.tableControl reloadData];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //cal cell position
    [self fnAutoSelectCellAtIndexPath: indexPath];
}

-(void) scrollToBottomWithIndexAuto:(NSIndexPath*)indexAuto
{
    [self.tableControl layoutIfNeeded];

    NSInteger count = [self tableView:self.tableControl numberOfRowsInSection:0];
    
    if (count > 0) {
        NSInteger lastPos = MAX(0, count-1);
        
        [self.tableControl scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:lastPos inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.tableControl scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:lastPos inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            if (indexAuto) {
                [self fnAutoSelectCellAtIndexPath: indexAuto];
            }
        });
        
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //hide the popup...
    
    _callbackLiveChatview(@{@"type":@"hide"});

}

@end
