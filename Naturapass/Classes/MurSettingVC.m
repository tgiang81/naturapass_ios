//
//  MurSettingVC.m
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurSettingVC.h"
#import "MurParameter_Publication.h"
#import "MurParameter_Notification_Email.h"
#import "MurParameter_Notification_Smartphones.h"
#import "MurParameter_Favorites.h"
#import "MemberBlackListVC.h"
#import "MurParameter_PublicationFav.h"
#import "CellKind9.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface MurSettingVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation MurSettingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strParametresUtilisateurs);
    // Do any additional setup after loading the view from its nib.
    
    NSMutableDictionary *dic1 = [@{@"name": str(strPublication),
                                   @"image":@"mur_st_ic_publication"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strNotifications_email),
                                   @"image":@"mur_st_ic_email"} copy];
    
    NSMutableDictionary *dic3 = [@{@"name":str(strNotifications_smartphones),
                                   @"image":@"mur_st_ic_smartphones"} copy];

    NSMutableDictionary *dic4 = [@{@"name":str(strAdresses_favorites),
                                   @"image":@"mur_ic_add_fav"} copy];
    
    NSMutableDictionary *dic5 = [@{@"name":str(strPublications_favorites),
                                   @"image":@"mur_ic_pub_fav"} copy];
    
    NSMutableDictionary *dic6 = [@{@"name":str(strMembres_blacklister),
                                   @"image":@"ic_blacklist"} copy];

    arrData =  [@[dic1,dic2,dic3,dic4,dic5,dic6] copy];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind9" bundle:nil] forCellReuseIdentifier:identifierSection1];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;

}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind9 *cell = nil;
    
    cell = (CellKind9 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];

    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.arrow setImage: [UIImage imageNamed:@"arrow_cell" ]];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.row) {
        case 0:
        {
            MurParameter_Publication *viewController1 = [[MurParameter_Publication alloc] initWithNibName:@"MurParameter_Publication" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 1:
        {
            MurParameter_Notification_Email *viewController1 = [[MurParameter_Notification_Email alloc] initWithNibName:@"MurParameter_Notification_Email" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 2:
        {
            MurParameter_Notification_Smartphones *viewController1 = [[MurParameter_Notification_Smartphones alloc] initWithNibName:@"MurParameter_Notification_Smartphones" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 3:
        {
            MurParameter_Favorites *viewController1 = [[MurParameter_Favorites alloc] initWithNibName:@"MurParameter_Favorites" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
        }
            break;
            
        case 4:
        {
            MurParameter_PublicationFav *viewController1 = [[MurParameter_PublicationFav alloc] initWithNibName:@"MurParameter_PublicationFav" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
        }
            break;
        case 5:
        {
           MemberBlackListVC  *viewController1 = [[MemberBlackListVC alloc] initWithNibName:@"MemberBlackListVC" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR];
        }
            break;
        default:
            break;
    }

}


@end
