//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

typedef void (^callBackGroup) (NSInteger);
@interface CellKind8 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (strong, nonatomic) IBOutlet UIImageView *rightIcon;

@property (weak, nonatomic) IBOutlet UIImageView *imageLine;
@property (weak, nonatomic) IBOutlet UIButton *btnDel;

@property (nonatomic, copy) callBackGroup CallBackGroup;

@end
