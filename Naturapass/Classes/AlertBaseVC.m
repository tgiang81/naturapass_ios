//
//  AlertBaseVC.m
//  Naturapass
//
//  Created by Manh on 8/24/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertBaseVC.h"
#import "Define.h"
#import "AppCommon.h"
@interface AlertBaseVC ()

@end

@implementation AlertBaseVC

- (id)initWithNibName
{
    self = [super initWithNibName:@"AlertBaseVC" bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [COMMON listSubviewsOfView:self.view];

    [self.vContraint.layer setMasksToBounds:YES];
    self.vContraint.layer.cornerRadius= 3.0f;
    self.vContraint.layer.borderWidth =0;
    [self.vHeader.layer setMasksToBounds:YES];
    self.vHeader.layer.cornerRadius= 5;
    self.vHeader.layer.borderWidth =0;
    [self.vHeader setBackgroundColor:UIColorFromRGB(ON_SWITCH_CARTE)];

}
-(void)addContraintView:(UIView*)view
{
    view.translatesAutoresizingMaskIntoConstraints = NO;

    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeTop
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContent
                               attribute:NSLayoutAttributeTop
                               multiplier:1.0 constant:0] ];

    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeLeading
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContent
                               attribute:NSLayoutAttributeLeading
                               multiplier:1.0 constant:0] ];
    
    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeTrailing
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContent
                               attribute:NSLayoutAttributeTrailing
                               multiplier:1.0 constant:0] ];
    
    [self.view addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeBottom
                               relatedBy:NSLayoutRelationEqual
                               toItem:self.vContent
                               attribute:NSLayoutAttributeBottom
                               multiplier:1.0 constant:0] ];

}

- (NSLayoutConstraint *)pin:(id)item attribute:(NSLayoutAttribute)attribute andView:(UIView*) v
{
    return [NSLayoutConstraint constraintWithItem:v
                                        attribute:attribute
                                        relatedBy:NSLayoutRelationEqual
                                           toItem:item
                                        attribute:attribute
                                       multiplier:1.0
                                         constant:0.0];
}

#pragma mark - set them
-(void)fnSettingCell:(ISSCREEN)type
{
    switch (type) {
        case ISMUR:
        {
            [self fnSetColor: UIColorFromRGB(MUR_TINY_BAR_COLOR)];
            
        }
            break;
        case ISGROUP:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            
        }
            break;
        case ISLOUNGE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        }
            break;
            
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{

    [_btnClose setBackgroundColor:color];
}
#pragma mark -action
-(void)showInVC:(UIViewController*)vc
{
    
    [vc presentViewController:self animated:NO completion:^{
        
    }];
}
-(IBAction)closeAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
