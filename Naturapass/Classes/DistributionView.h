//
//  DistributionView.h
//  Naturapass
//
//  Created by Giang on 6/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "NewMapAnnotationView.h"
#import "ViewFooter.h"
#import "ViewHead.h"
#import "PinView.h"

@class DistributionAnnotation;

@interface DistributionView : NewMapAnnotationView <UITableViewDataSource, UITableViewDelegate , UICollectionViewDataSource, UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout>
{
    BOOL hideTableSection;
    IBOutlet ViewHead *vHead;
    
    UIView *pv;
    
}
//@property (nonatomic, strong) DistributionAnnotation *mapViewAnnotation;
@property (nonatomic, strong) ViewFooter *vFoot;

@property(nonatomic, strong) UIView *calloutView;

- (instancetype) initForDisMapViewAnnotation: (DistributionAnnotation *) imageMapAnnotation
                                NS_DESIGNATED_INITIALIZER;

- (void)doSetCalloutView;
@end
