//
//  Publication_Choix_Partage.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"
#import "PosterBaseVC.h"

@interface Poster_Choix_Partage : PosterBaseVC

@property(nonatomic,assign) BOOL isEditFavo;
@property (strong, nonatomic) IBOutlet UIView *viewBottom;

@end
