//
//  BaseVC.h
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

#import "BaseNavigation.h"
#import "TabVC.h"
#import "TabBottomVC.h"
#import "MainNavigationBaseView.h"
#import "SubNavBaseView.h"
//#import "YiRefreshHeader.h"
//#import "YiRefreshFooter.h"
#import "AppCommon.h"
#import "FileHelper.h"
#import "UIAlertView+Blocks.h"
#import "PublicationOBJ.h"
#import "ASSharedTimeFormatter.h"
#import "GroupEnterOBJ.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Group_Hunt_ListShare.h"
#import "KSToastView.h"
#import "MainNavLiveMap.h"
#import "AZNotification.h"
#import "HomeVC.h"
#import "Flurry.h"
#import "NSString+EMOEmoji.h"
@interface BaseVC : UIViewController
{
//        YiRefreshHeader *refreshHeader;
//        YiRefreshFooter *refreshFooter;
    
//        BaseNavigation *navigation;

    MainNavigationBaseView *subviewCount;
    BOOL allow_add_chat;
    BOOL allow_show_chat;
    
    BOOL allow_add;
    BOOL allow_show;

}
@property (nonatomic, strong) AppDelegate *app;

@property (nonatomic,strong) NSOperationQueue * operationQueue;

//Notifi
@property(nonatomic,assign) BOOL isNotifi;

//Sub nav group detail
@property(nonatomic,assign) BOOL needRemoveSubItem;

@property (nonatomic,strong) NSString *typeNotifi;
@property (nonatomic,strong) NSString *IdNotifi;
-(void)gotoNotifi_leaves;
//COMMON
@property (nonatomic,assign) BOOL isSubVC;
@property (nonatomic,assign)   BOOL isGoin;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) ISNOTIFICATION nofiticationType;

@property(nonatomic,strong) UIViewController *mParent;

//Special val
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic,assign) BOOL isLiveHuntDetail;

//To containt other view...

@property (nonatomic, strong)  IBOutlet SubNavBaseView *subview;
@property (nonatomic, strong)  MainNavLiveMap *navLiveMap;

@property (nonatomic, strong)  IBOutlet UIView *vContainer;

@property (nonatomic, strong)  UIView *vBottom;

//VC
@property (nonatomic, strong)  TabVC *myTabVC;
@property (nonatomic, strong)  TabBottomVC *bottomVC;

@property (nonatomic, strong)  UIToolbar                   *keyboardToolbar;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;

@property (nonatomic, strong) NSDictionary *dicOption;
@property (nonatomic, assign) BOOL showTabBottom;
@property (nonatomic, assign) BOOL murV3;

-(MainNavigationBaseView*) getSubMainView;
-(void) cleanAnyTmpData;

-(void) fnRemoveObserver;
-(void) doRemoveObservation;

-(void) initRefreshControl;
-(void)startRefreshControl;
-(void)stopRefreshControl;
-(void)isRemoveScrollingViewHeight:(BOOL)enable;
-(void)moreRefreshControl;
@property (nonatomic, strong)  IBOutlet NSLayoutConstraint *constraintBottom;
- (NSLayoutConstraint *)pin:(id)item attribute:(NSLayoutAttribute)attribute andView:(UIView*) v;

-(void) addMainNav:(NSString*) nameView;
-(void) addSubNav:(NSString*) nameView;

-(void) onMainNavClick:(UIButton*)btn;
-(void) onSubNavClick:(UIButton*)btn;
-(void) onTabItemClick:(UIButton*)btn;

-(void) clickSubNav_Mur:(int) index;
-(void) clickSubNav_Group:(int) index;
-(void) clickSubNav_Chass:(int) index;

-(void) clickSubNav_Discuss:(int) index;
-(void) clickSubNav_Special:(int) index;

-(void) onSubNavClick_Invite_Chasse:(int) index;

-(void) InitializeKeyboardToolBar;
- (void)resignKeyboard:(id)sender;

-(void) doPushVC:(Class)class iAmParent:(BOOL)isParent;
-(void) doPushVC:(Class)class iAmParent:(BOOL)isParent expectTarget:(ISSCREEN)expectTarget;

-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim;
-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim expectTarget:(ISSCREEN)expectTarget;
-(void) pushVC:(BaseVC*)vc animate:(BOOL)anim expectTarget:(ISSCREEN)expectTarget iAmParent:(BOOL)isParent;
-(void) doPushVCLiveHunt:(Class)class iAmParent:(BOOL)isParent expectTarget:(ISSCREEN)expectTarget;

-(IBAction)gotoback;
-(NSString*)convertDate:(NSString*)date;
-(void) updateStatusNavControls;
-(UIButton* )returnButton;
-(void)setThemeNavSub :(BOOL) forceParent withDicOption:(NSDictionary*)dicOption;
-(void) removeItem:(int) index;
-(void) doRefresh;

-(NSInteger) checkCachePublicationHasData;
-(void)backEditListChasse;
-(void)backEditListGroups;
-(void)backEditListMur;

-(void)fnGetAllNotifications;
-(void) fnGetAllNotifications:(NSInteger)index creen:(ISSCREEN)screen;

-(void) fnDoRefreshData;
- (void)putNotification:(NSString *)strNotificationID;
-(UIImage*) fnResizeFixWidth :(UIImage*)img :(float)wWidth;
-(CGSize) fnResize:(CGSize)size width:(float)wWidth;
-(void)setColorTextNavSubWithIndex:(int)index;
-(void)setThemeTabVC:(UIViewController*)vc;
-(void)backEditFavo;
-(BOOL)fnCheckResponse:(NSDictionary*)response;
-(void)checkAllow;
-(void) fnDoRefreshNotification;

@end
