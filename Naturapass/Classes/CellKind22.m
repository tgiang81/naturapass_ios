//
//  CellKind22.m
//  Naturapass
//
//  Created by Giang on 2/22/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "CellKind22.h"
#import "AppCommon.h"

@implementation CellKind22

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
