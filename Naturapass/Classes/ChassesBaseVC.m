//
//  ChassesBaseVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesBaseVC.h"

#import "ChassesMurVC.h"
#import "ChassesSearchVC.h"
#import "ChassesMurPassVC.h"
#import "NotificationVC.h"
#import "ChassesInvitationAttend.h"
#import "CommonObj.h"

@interface ChassesBaseVC ()

@end

@implementation ChassesBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];

    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    //show tabBottom
    if ([self isKindOfClass:[ChassesMurVC class]] ||
        [self isKindOfClass:[ChassesSearchVC class]] ||
        [self isKindOfClass:[ChassesInvitationAttend class]] ||
        [self isKindOfClass:[ChassesMurPassVC class]] ) {
        self.showTabBottom = YES;
    }
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strCHANTIERS)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    
    [self addSubNav:@"SubNavigationCHASSES"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
    
    [self setThemeNavSub:NO withDicOption:nil];

    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    //Confort
    UIButton *btnCheck = (UIButton*)[self.subview viewWithTag: 2*( START_SUB_NAV_TAG + 4)];
    [self.subview badgingBtn:btnCheck count: (int)[CommonObj sharedInstance].nbChasseInvitation];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];

}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
}

-(void)fnGetAllNotifications
{
    UIImageView *btnAmis =(UIImageView*)[subviewCount viewWithTag:24];
    UIImageView *btnDiscussion =(UIImageView*)[subviewCount viewWithTag:26];
    UIImageView *btnNotification =(UIImageView*)[subviewCount viewWithTag:28];
    
    [subviewCount badgingBtn:btnAmis count:[CommonObj sharedInstance].nbUserWaiting];
    [subviewCount badgingBtn:btnDiscussion count:[CommonObj sharedInstance].nbUnreadMessage];
    [subviewCount badgingBtn:btnNotification count:[CommonObj sharedInstance].nbUnreadNotification];
    
    [self fnGetAllNotifications:4 creen:ISLOUNGE];
    
}
#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];

    [super onSubNavClick:btn];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            NSArray * controllerArray = [[self navigationController] viewControllers];
            
            for (UIViewController *controller in controllerArray)
            {
                //Code here.. e.g. print their titles to see the array setup;
                
                if ([controller isKindOfClass:[self.mParent class]] ) {
                    if ([controller isKindOfClass: [HomeVC class]]) {
                        [self doRemoveObservation];
                    }

                    [self.navigationController popToViewController:controller animated:NO];
                    return;
                }
            }
            [self gotoback];
        }
            break;
            
        case 1://MUR
        {
            if ([self isKindOfClass: [ChassesMurVC class]])
                return;
            
            [self doPushVC:[ChassesMurVC  class] iAmParent:NO];
        }
            break;
            
            
        case 2://TOUT
        {
            if ([self isKindOfClass: [ChassesSearchVC class]])
                return;
            
            [self doPushVC:[ChassesSearchVC  class] iAmParent:NO];
            
        }
            break;
            
        case 3://old
        {
            if ([self isKindOfClass: [ChassesMurPassVC class]])
                return;
            
            [self doPushVC:[ChassesMurPassVC  class] iAmParent:NO];
        }
            
            break;
        case 4://Invitation
        {
            if ([self isKindOfClass: [ChassesInvitationAttend class]])
                return;
            
            [self doPushVC:[ChassesInvitationAttend  class] iAmParent:NO];
        }
        default:
            break;
    }
}



@end
