//
//  GroupesViewCell.m
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "TypeCell12.h"

@implementation TypeCell12
- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

}
- (void)layoutSubviews
{
    [super layoutSubviews];
    self.lbDescription.preferredMaxLayoutWidth = CGRectGetWidth(self.lbDescription.frame);
}
-(void)getParticipationName:(int)participationId
{
    NSString *url =@"";
    switch (participationId) {
        case 0:// Ne partticipe pas
        {
            url =@"ic_not_participate";
            self.lbParticipant.text = @"Ne participe pas";
        }
            break;
        case 1://Participe
        {
            url =@"ic_participate";
            self.lbParticipant.text = @"Participe ";

        }
            break;
        case 2://sais pas
        {
            url =@"ic_pre_participate";
            self.lbParticipant.text = @"Ne Sais pas";

        }
            break;
        default:
            self.lbParticipant.text = @"";
            break;
    }
    [self.imgParticipant setImage:[UIImage imageNamed:url]];
}
-(void)fnSettingCell:(int)type
{
    switch (type) {
        case UI_GROUP_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_MUR_NORMAL:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_GROUP_TOUTE:
        {
            
        }
            break;
        case UI_CHASSES_MUR_ADMIN:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_MUR_NORMAL:
        {
            
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
            
        }
            break;
        case UI_CHASSES_TOUTE:
        {
            
        }
            break;
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
}
@end
