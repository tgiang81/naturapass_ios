//
//  MDMarkersLegend.m
//  Naturapass
//
//  Created by Manh on 3/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDMarkersText.h"
#import "Define.h"

@import QuartzCore;


static CGFloat const kJPSThumbnailAnnotationViewVerticalOffset    = 10.0f;
static CGFloat const kJPSThumbnailAnnotationViewAnimationDuration = 0.25f;

@interface MDMarkersText ()
{
}
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@end

@implementation MDMarkersText
-(instancetype)initWithText:(NSString*)text
{
    self = [super init];
    if (self) {
        //set legend
        self.strText = text;
        [self setupView];
    }
    return self;
}
- (void)doSetCalloutView {
    
    [self setupView];
}
- (void)setLayerProperties {
    UIImage *image = [self imageFromWithView:self];
    if (_CallBackMarkers) {
        _CallBackMarkers(image);
    }

}

- (void)setupView {
    UILabel*tmp = [UILabel new];
    tmp.font = [UIFont boldSystemFontOfSize:12];
    
    tmp.text = self.strText;
    tmp.textColor = [UIColor whiteColor];
    CGSize textSize = [[tmp text] sizeWithAttributes:@{NSFontAttributeName:[tmp font]}];
    
    CGFloat strikeWidth = textSize.width + 5;
    
    self.frame = CGRectMake(0, 0, strikeWidth, kJPSThumbnailAnnotationViewVerticalOffset + 15);
    tmp.frame = self.frame;
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:tmp];

    [self setLayerProperties];
    
}
- (UIImage *)imageFromWithView:(UIView*)vView
{
    UIView *view = vView;
    //draw
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end