//
//  GroupCreate_Step1.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Parameter_Profile_Papers_Change.h"

#import "PhotoSheetVC.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ImageWithCloseButton.h"
#import "AlertVC.h"
#import "VoisChiens_Document_Alert.h"
#import "ChooseDocumentVC.h"
#import "CommonHelper.h"
#import "Define.h"
#import "FileHelper.h"

@interface Parameter_Profile_Papers_Change ()
{
    IBOutlet UILabel *lbNameScreen;
    NSMutableDictionary *postDict;
    NSMutableDictionary *attachment;
    NSData         *dataPhoto;
    
}

@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;
@property (weak, nonatomic)     IBOutlet ImageWithCloseButton *viewPhotoSelected;
@end

@implementation Parameter_Profile_Papers_Change

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialization];
    [self fnSetValueUI];
    if (self.isModifi) {
        [self fnSetDataModifi];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)initialization
{
    [nameTextField setAutocapitalizationType: UITextAutocapitalizationTypeWords];
    [self InitializeKeyboardToolBar];
    [nameTextField setInputAccessoryView:self.keyboardToolbar];
    [descTextView setInputAccessoryView:self.keyboardToolbar];
    //Photo
    self.viewPhotoSelected.hidden=YES;
    [self.viewPhotoSelected setMyCallback: ^(){
        //Close
        self.viewPhotoSelected.hidden=YES;
        self.viewPhotoSelected.imageContent.image = nil;
    }];
}

-(void)fnSetValueUI
{
    //if edit Modification d'un papier
    
    NSString *strKind = nil;
    if (self.isModifi) {
        strKind = str(strModification_dun_papier);
    }else{
        strKind = str(strAjout_dun_papier);
    }
    
    lbNameScreen.text = strKind;
}

-(void)fnSetDataModifi
{
    switch (self.myTypeVview) {
        case TYPE_PAPER:
        {
            //paper[type] = 0 => TYPE_ALL, 1 => TYPE_MEDIA, 2 => TYPE_MEDIA_NAME, 3 => TYPE_MEDIA_TEXT
            switch ([self.dicModifi[@"type"] intValue]) {
                case PAPER_TYPE_ALL:
                {
                    
                }
                    break;
                case PAPER_TYPE_MEDIA:
                {
                    //only edit media
                    viewTitle.hidden = YES;
                    viewDescription.hidden =YES;
                }
                    break;
                case PAPER_TYPE_MEDIA_NAME:
                {
                    //only edit media, name
                    viewDescription.hidden =YES;
                    
                }
                    break;
                case PAPER_TYPE_MEDIA_TEXT:
                {
                    viewTitle.hidden = YES;
                    self.constraintHeightViewTitle.constant = 0;
                }
                    break;
                default:
                    break;
            }
            
            //name
            nameTextField.text = self.dicModifi[@"name"];
            //name
            descTextView.text = self.dicModifi[@"text"];
            
            if([descTextView.text isEqual:@""]||descTextView ==nil)
                descTitleLabel.text= @"Entrer un texte";
            else
                descTitleLabel.text=@"";
            
            //photo
            NSArray *medias =self.dicModifi[@"medias"];
            if (medias.count>0) {
                self.viewPhotoSelected.hidden = NO;
                NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicModifi[@"medias"][medias.count -1][@"path"]]]];
                [self.viewPhotoSelected.imageContent sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                }];
            }
            
        }
            break;
            
        default:
            break;
    }
    
}

#pragma mark - API
#pragma mark - action

-(IBAction)createDogAction:(id)sender
{
    [self fnChangeDog];
}

-(void)fnChangeDog
{
    postDict =[NSMutableDictionary new];
    attachment= [NSMutableDictionary new];
    
    if (dataPhoto != nil) {
        UIImage *image = [UIImage imageWithData:dataPhoto];
        NSData *fileData = UIImageJPEGRepresentation(image, 0.5); //

        [attachment setObject:@{@"name":@"paper[medias][1000][file]",@"kFileName": @"image.png",@"kFileData": fileData} forKey:@"photo"];
    }
    
    [COMMON addLoading:self];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    if (self.isModifi) {
        
        switch ([self.dicModifi[@"type"] intValue]) {
            case PAPER_TYPE_ALL:
            {
                //all edit
                if (nameTextField.text.length>0) {
                    [postDict setValue:nameTextField.text forKey:@"paper[name]"];
                }
                
                if (descTextView.text.length>0) {
                    [postDict setValue:descTextView.text forKey:@"paper[text]"];
                }
                
            }
                break;
                
            case PAPER_TYPE_MEDIA:
            {
                //only edit media
                
            }
                break;
            case PAPER_TYPE_MEDIA_NAME:
            {
                //only edit media, name
                if (nameTextField.text.length>0) {
                    [postDict setValue:nameTextField.text forKey:@"paper[name]"];
                }
            }
                break;
                
            case PAPER_TYPE_MEDIA_TEXT:
            {
                //only edit media, text
                if (descTextView.text.length>0) {
                    [postDict setValue:descTextView.text forKey:@"paper[text]"];
                }

                //to retain default value
                [postDict setValue:self.dicModifi[@"name"] forKey:@"paper[name]"];
                
            }
                break;
            default:
                break;
        }
        
        [serviceObj putProfileWithKindID:self.dicModifi[@"id"] withKind:self.myTypeVview withParams:postDict attachment:attachment!=nil?attachment:NULL];
    }
    else
    {
        if (nameTextField.text.length>0) {
            [postDict setValue:nameTextField.text forKey:@"paper[name]"];
        }
        
        if (descTextView.text.length>0) {
            [postDict setValue:descTextView.text forKey:@"paper[text]"];
        }
        
        [serviceObj postProfileWithParam:postDict attachment:attachment!=nil?attachment:NULL withKind:self.myTypeVview];
    }
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
        [self gotoback];
    };
}

- (IBAction)profileImageOptions:(id)sender{
    [nameTextField resignFirstResponder];
    
    PhotoSheetVC *vc = [[PhotoSheetVC alloc] initWithNibName:@"PhotoSheetVC" bundle:nil];
    vc.expectTarget = self.expectTarget;
    
    [vc setMyCallback:^(NSDictionary*data){
        NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:data[@"image"]];
        UIImage *imgData =[UIImage imageWithData: [NSData dataWithContentsOfFile:photoFile ]]; //
        
        self.viewPhotoSelected.imageContent.image = imgData;
        self.viewPhotoSelected.hidden = NO;
        
        
        dataPhoto = UIImageJPEGRepresentation(imgData, 1.0);
        
    }];
    
    [self presentViewController:vc animated:NO completion:^{
        
    }];
}

#pragma mark - delegate
- (void)textViewDidChange:(UITextView *)textView
{
    if([descTextView.text isEqual:@""]||descTextView ==nil)
        descTitleLabel.text= str(strEntrer_un_texte);
    else
        descTitleLabel.text=@"";
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 100) animated:YES];
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [nameTextField resignFirstResponder];
    return YES;
}
#pragma mark -keyboar tool bar
- (void)resignKeyboard:(id)sender
{
    [nameTextField resignFirstResponder];
    [descTextView resignFirstResponder];
    
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 150) animated:YES];
}
- (void)textViewDidEndEditing:(UITextView *)textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
@end
