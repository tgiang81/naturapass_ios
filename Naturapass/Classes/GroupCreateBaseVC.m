//
//  GroupCreateBaseVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateBaseVC.h"
#import "NotificationVC.h"
#import "SubNavigationPRE_ANNULER.h"
#import "GroupCreate_Step9.h"
#import "AlertVC.h"
#import "GroupMesVC.h"
@interface GroupCreateBaseVC ()

@end

@implementation GroupCreateBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //add sub navigation
    [self addMainNav:@"MainNavMUR"];
    
    
    //Set title
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myTitle setText:str(strGROUPES)];
    
    //Change background color
    subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
    
    
    [self addSubNav:@"SubNavigationPRE_ANNULER"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
    //coloring btn
    
    SubNavigationPRE_ANNULER*okSubView = (SubNavigationPRE_ANNULER*)self.subview;
    
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    UIButton *btn2 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+1];
    
    btn1.backgroundColor = UIColorFromRGB(GROUP_BACK);
    
    btn2.backgroundColor = UIColorFromRGB(GROUP_CANCEL);
}

#pragma SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    [self viewWillDisappear:YES];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
        case 1://ANNULER
        {
            NSString *str=@"groupe";
            NSString *strmessage= @"";
            if ([GroupCreateOBJ sharedInstance].isModifi) {
                strmessage = [NSString stringWithFormat:str(strEtesVousSurDeVouloiranuilervotresaisie)];
            }
            else
            {
                if (self.needChangeMessageAlert) {
                    strmessage = [NSString stringWithFormat:str(strMessage31),str];
                    
                }else{
                    
                    strmessage = [NSString stringWithFormat:str(strMessage10),str];
                }
            }
            AlertVC *vc = [[AlertVC alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@",str(strAAnnuler),str] message:strmessage cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
            
            [vc doBlock:^(NSInteger index , NSString *str) {
                if (index==0) {
                    // NON
                    
                }
                else if(index==1)
                {
                    //OUI
                    if (self.needChangeMessageAlert) {
                        
                        GroupCreate_Step9 *viewController1 = [[GroupCreate_Step9 alloc] initWithNibName:@"GroupCreate_Step9" bundle:nil];
                        [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
                    }else{
                        NSArray * controllerArray = [[self navigationController] viewControllers];
                        
                        for (int i=0; i < controllerArray.count; i++) {
                            if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
                                
                                [self.navigationController popToViewController:self.mParent animated:YES];
                                
                                return;
                            }
                        }
                        [self doRemoveObservation];

                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }
                    
                    
                }
            }];
            [vc showAlert];
        }
            break;
            
        default:
            break;
    }
}

- (IBAction)onNext:(id)sender {
}

@end
