//
//  FriendInfoProfileCell.m
//  Naturapass
//
//  Created by JoJo on 12/17/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import "FriendInfoProfileCell.h"
@implementation FriendInfoProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.viewMain.layer.masksToBounds = YES;
    self.viewMain.layer.cornerRadius= 5;

    //mld test
    [self fnSetTextWithDic:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)fnSetTextWithDic:(NSDictionary*)dic
{
    NSString *strValueSesPapiers = @"Demander I'information";
    NSDictionary *attrs1 = @{ NSForegroundColorAttributeName : [UIColor redColor],NSFontAttributeName: FONT_HELVETICANEUE_Italic(12)};
    NSMutableAttributedString *mSesPapiers = [[NSMutableAttributedString alloc] initWithString:strValueSesPapiers attributes:attrs1];
    NSMutableAttributedString *mFull = [NSMutableAttributedString new];
    [mFull appendAttributedString:mSesPapiers];
    [mFull addAttribute:NSUnderlineStyleAttributeName
                  value:[NSNumber numberWithInt:1]
                  range:(NSRange){0,[mFull length]}];
    self.lbValueSesPapiers.attributedText = mFull;

}
-(void)fnSetColorWithExpectTarget:(ISSCREEN)expTarget
{
    _expectTarget = expTarget;
    _colorAnnuler = UIColorFromRGB(POSTER_ANNULER_COLOR);
    switch (self.expectTarget) {
        case ISLOUNGE:
        {
            _colorCommon = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            _colorCommon = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        default:
        {
            _colorCommon = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
    }
    
    self.imgLine.backgroundColor = _colorAnnuler;
    self.lbTitle.textColor = _colorCommon;
    
    self.lbOuilellChasse.textColor = _colorAnnuler;
    self.lbValueOuilellChasse.textColor = _colorCommon;
    
    self.lbSesPapiers.textColor = _colorAnnuler;
    self.lbValueSesPapiers.textColor = [UIColor redColor]; //_colorCommon;
    
    self.lbSesArmes.textColor = _colorAnnuler;
    self.lbValueSesArmes.textColor = _colorCommon;
    
    self.lbSesChiens.textColor = _colorAnnuler;
    self.lbValueSesChiens.textColor = _colorCommon;
    
    self.lbSesTypesChasse.textColor = _colorAnnuler;
    self.lbValueSesTypesChasse.textColor = _colorCommon;
    
    self.lbSesPaysChasse.textColor = _colorAnnuler;
    self.lbValueSesPaysChasse.textColor = _colorCommon;
    
    self.lbGrade.textColor = _colorAnnuler;
    self.lbVaueGrade.textColor = _colorCommon;
}
@end
