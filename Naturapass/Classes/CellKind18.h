//
//  CellKind18.h
//  Naturapass
//
//  Created by Giang on 10/9/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWProgressView.h"
#import "AppCommon.h"

@interface CellKind18 : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UIImageView *imageLine;

@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UIImageView *rightIcon;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintRightIconWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintRightIconHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftIconWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLeftIconHeight;
@property (nonatomic, strong)  PWProgressView *progressView;

@end
