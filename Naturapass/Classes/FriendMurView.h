//
//  FriendMurView.h
//  Naturapass
//
//  Created by Manh on 6/16/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MediaCell.h"
#import "BaseVC.h"
#import "Define.h"
#import "BaseView.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "SettingActionView.h"

typedef void (^FriendMurViewCallback)(VIEW_ACTION_TYPE type, NSArray *arrData);
@interface FriendMurView : BaseView<UIDocumentInteractionControllerDelegate, IDMPhotoBrowserDelegate>
{
    NSMutableArray                  *publicationArray;
    int widthImage;
    SettingActionView *shareDetail;
    BOOL isOpenPublication;
    NSInteger countFriend;
    NSInteger countFriendCommon;
    BOOL isAddFriendHide;
    ISFRIEND_KIND stateFriend;
    UITapGestureRecognizer *tapDissmiss;
    BOOL isOpenSetting;
    BOOL isDeBlock;

}
@property(nonatomic,strong)  UIColor *colorCommon;
@property(nonatomic,strong)  UIColor *colorAnnuler;
@property (nonatomic, strong) MediaCell *prototypeCell;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,assign) BOOL isLiveHunt;
@property (nonatomic, strong) BaseVC *parentVC;
@property (nonatomic,copy) FriendMurViewCallback callback;
@property (nonatomic, strong) UIDocumentInteractionController *documentInteractionController;
@property (nonatomic, retain) NSDictionary *friendDic;


-(void)addContraintSupview:(UIView*)viewSuper;
-(void)fnSetDataPublication:(NSArray*)arrPublication;
-(void)fnPublicationNew:(NSArray*)arrPublication;
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget;
-(void)getTheFriends;
@end
