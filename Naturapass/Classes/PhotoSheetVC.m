//
//  PhotoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "PhotoSheetVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "AppDelegate.h"
#import "Define.h"
#import "FileHelper.h"
#import "CommonHelper.h"

@interface PhotoSheetVC () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic,strong) IBOutlet UIButton *btnPhoto;
@property (nonatomic,strong) IBOutlet UIButton *btnLibrary;
@property (nonatomic,strong) IBOutlet UIButton *btnClose;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;

@end

@implementation PhotoSheetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnPhoto   setTitle:str(strPrendre_une_photo) forState:UIControlStateNormal];
    [_btnLibrary   setTitle:str(strChoisir_dans_la_bibliotheque) forState:UIControlStateNormal];
    [_btnClose  setTitle:str(strAAnnuler) forState:UIControlStateNormal];
    _lbTitle.text = str(strPublier_une_photo);
    // Do any additional setup after loading the view from its nib.
    
    [self fnSettingCell:self.expectTarget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTakePhoto:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorryNoCamera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
        [altView show];
        return;
    }
    [self getMediaFromSource:UIImagePickerControllerSourceTypeCamera];

}

- (IBAction)onTakeLibrary:(id)sender {
    [self getMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}


- (void)getMediaFromSource:(UIImagePickerControllerSourceType)sourceType
{
    //iPhone
    UIImagePickerController *imagePicker;
    
    NSArray *mediaTypes = [UIImagePickerController
                           availableMediaTypesForSourceType:sourceType];
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.mediaTypes = mediaTypes;
    imagePicker.delegate = (id)self;
    //    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = sourceType;
    
    [self presentViewController:imagePicker animated:NO completion:Nil];

}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}


- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
//    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data


    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    CLLocation * loc = app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        
        
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

#pragma mark  -  PICKER DELEGATE

// FIXME: Rewrite me!

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    // FIXME: USE NSSTRING CONSTANT NOT STRING VALUE: UIImagePickerControllerOriginalImage
    
    if([info objectForKey:@"UIImagePickerControllerOriginalImage"])
    {
        NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
        if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
            UIImage *tmpimage = [info objectForKey:UIImagePickerControllerOriginalImage];
            
            NSString *strName = [CommonHelper  generatorString];

            NSString *photoFile = [[FileHelper documentTempDirectory] stringByAppendingPathComponent:strName];
            NSData *imageData = UIImageJPEGRepresentation(tmpimage,1);
            [imageData writeToFile:photoFile atomically:YES];
            
            NSURL *assetURL = [info objectForKey:UIImagePickerControllerReferenceURL];
            
            
//            assetURL.path;
            
            if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
                
//                [self saveImage:tmpimage withInfo:info];
                
                AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                
                CLLocation * loc = app.locationManager.location;

                
                if (loc && loc.coordinate.longitude && loc.coordinate.latitude) {
                    NSDictionary *retDic = @{@"image":strName,
                                             @"Latitude": [NSNumber numberWithFloat:loc.coordinate.latitude],
                                             @"Longitude": [NSNumber numberWithFloat:loc.coordinate.longitude] };
                    self.myCallback(retDic);
                }else{
                    self.myCallback(@{@"image":strName});
                }
                [self onCancel:nil];

            }else{
                void (^ALAssetsLibraryAssetForURLResultBlock)(ALAsset *) = ^(ALAsset *asset)
                {
                    NSDictionary *metadata = asset.defaultRepresentation.metadata;
//                    ASLog(@"Image Meta Data: %@",metadata);
                    NSNumber *latNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Latitude"];
                    NSNumber *lonNum=[[metadata valueForKey:@"{GPS}"]valueForKey:@"Longitude"];
                    
                    if (latNum && lonNum) {
                        NSDictionary *retDic = @{@"image":strName,
                                                 @"Latitude":latNum,
                                                 @"Longitude":lonNum};
                        self.myCallback(retDic);
                    }else{
                        self.myCallback(@{@"image":strName});
                    }
                    
                    [self onCancel:nil];
                };
                
                
                //        fileData   = [ NSData dataWithContentsOfURL: [NSURL fileURLWithPath: dataDic[strPostAttachmentKey][@"kFileData"] ] ]; //Video URL
                
                
                ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                //            NSURL *imageDataURL=[NSURL URLWithString:@"assets-library" ];
                //            [[UIApplication sharedApplication] openURL:imageDataURL];
                [library assetForURL:assetURL
                         resultBlock:ALAssetsLibraryAssetForURLResultBlock
                        failureBlock:^(NSError *error) {
                        }];
            }

        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [picker dismissViewControllerAnimated:YES completion:nil];

}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [picker dismissViewControllerAnimated: YES completion: NULL];
    
    [self onCancel:nil];
    [self onCancel:nil];
}

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - set them
-(void)fnSettingCell:(ISSCREEN)type
{
    switch (type) {
        case ISMUR:
        {
            [self fnSetColor: UIColorFromRGB(MUR_TINY_BAR_COLOR)];
            
        }
            break;
        case ISGROUP:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            
        }
            break;
        case ISLOUNGE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        }
            break;
        case ISCARTE:
        {
            [self fnSetColor: UIColorFromRGB(CARTE_TINY_BAR_COLOR)];
        }
            break;
        case ISPARAMTRES:
        {
            [self fnSetColor: UIColorFromRGB(PARAM_MAIN_BAR_COLOR)];
        }
            break;
        case ISCREATEAGENDA:
            {
                [self fnSetColor: UIColorFromRGB(CHASSES_CREATE_NAV_COLOR)];
            
            }
        default:
            break;
    }
}
-(void)fnSetColor:(UIColor*)color
{
    [_btnPhoto setBackgroundColor:color];
    [_btnLibrary setBackgroundColor:color];
    [_btnClose setBackgroundColor:color];
}

#pragma mark - rotate
- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
