//
//  GroupCreateOBJ.m
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "LiveHuntOBJ.h"
#import "ASSharedTimeFormatter.h"
#import "Config.h"
@implementation LiveHuntOBJ


static LiveHuntOBJ *sharedInstance = nil;

+ (LiveHuntOBJ *) sharedInstance
{
    static dispatch_once_t once = 0;
    
    dispatch_once(&once, ^{sharedInstance = [[self alloc] init];});
    return sharedInstance;
}

-(void) resetParams
{
    _dicLiveHunt =nil;
}
-(void) resetListHunt
{
    _arrLiveHunt =nil;
}
-(BOOL)checkDateMoreThanCurrent:(NSString*)date
{
    NSDate *now = [NSDate date];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * date_begin= [ inputFormatter dateFromString:loungDateStrings ];
    
    NSDate *d1 = [df dateFromString:(NSString*) [df stringFromDate:date_begin]];
    NSDate *d2 = [df dateFromString:(NSString*) [df stringFromDate:now]];
    if ([d1 compare: d2] == NSOrderedSame) {
        return YES;
    }
    return NO;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    NSInteger hours = (ti / 3600);
    if (hours == 0 && minutes == 0) {
        return [NSString stringWithFormat:@"%2lds", (long)seconds];
    }
    else if (hours == 0) { // minutes
        return [NSString stringWithFormat:@"%2ldmn%2ld ", (long)minutes, (long)seconds];

    }
    else
    {
        return [NSString stringWithFormat:@"%2ldh%2ldmn ", (long)hours, (long)minutes];
    }
}



-(NSMutableAttributedString*)formatLiveHuntTimeWithDateBegin:(NSString*)begin withDateEnd:(NSString*)end
{
    NSString *date_begin =[self convertStringToDate:begin ];
    NSString *date_end =[self convertStringToDate: end];
    
//    NSString *date_begin=@"";
//    NSString *date_end =@"";
//    if (convert_date_begin) {
//        date_begin =  [convert_date_begin substringWithRange:NSMakeRange(11, 5)];
//    }
//    if (convert_date_end) {
//        date_end =  [convert_date_end substringWithRange:NSMakeRange(11, 5)];
//    }
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    [inputFormatter setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [inputFormatter setDateFormat: @"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    
    NSDate *startDate = [inputFormatter dateFromString:begin];
    
    NSMutableAttributedString *hyphen = [[NSMutableAttributedString alloc] initWithString:@"-" ];
    [hyphen addAttribute:NSFontAttributeName
                   value:FONT_HELVETICANEUE(13)
                   range:NSMakeRange(0, hyphen.length)];
    

    
    if ([[NSDate date] compare: startDate] == NSOrderedAscending)
    {
        //going to happen
        NSString *str1 = [NSString stringWithFormat:@"- COMMENCE DANS"];
        
        
        
        NSTimeInterval gdate = [startDate timeIntervalSinceReferenceDate] - [[NSDate date] timeIntervalSinceReferenceDate];
//        NSLog(@"Time Interval %@",[self stringFromTimeInterval:gdate]);
        
        
        
        //duration time = startdate - current
        NSString *mData = [NSString stringWithFormat:@" %@ ",[self stringFromTimeInterval:gdate]];
        
        
        NSMutableAttributedString *m1 = [[NSMutableAttributedString alloc] initWithString:str1];
        [m1 addAttribute:NSFontAttributeName
                   value:FONT_HELVETICANEUE(13)
                   range:NSMakeRange(0, m1.length)];
        
        NSMutableAttributedString *m2 = [[NSMutableAttributedString alloc] initWithString:mData];

        NSMutableAttributedString *attString = [NSMutableAttributedString new];
        
        [attString appendAttributedString:m1];
        [attString appendAttributedString:m2];
        [attString appendAttributedString: hyphen ];

        return attString;
    
    }
    //else
    
    NSString *strBegin = [NSString stringWithFormat:@" DE %@ ",date_begin];
    NSString *strEnd = [NSString stringWithFormat:@"À %@ ",date_end];
    
    
    NSMutableAttributedString *mDe = [[NSMutableAttributedString alloc] initWithString:strBegin];
    NSMutableAttributedString *mA = [[NSMutableAttributedString alloc] initWithString:strEnd];
    [mDe addAttribute:NSFontAttributeName
                value:FONT_HELVETICANEUE(13)
                range:NSMakeRange(1, 3)];
    
    [mA addAttribute:NSFontAttributeName
               value:FONT_HELVETICANEUE(13)
               range:NSMakeRange(0, 1)];
    NSMutableAttributedString *attString = [NSMutableAttributedString new];

    [attString appendAttributedString:hyphen];

    [attString appendAttributedString:mDe];
    [attString appendAttributedString:mA];

    [attString appendAttributedString:hyphen];

    return attString;
}

//Mer 16h15 à Lun 19h15

-(NSString*)convertStringToDate:(NSString*)date
{
    NSDateFormatter *outputFormatter1 = [[NSDateFormatter alloc] init];
    NSDateFormatter *outputFormatter2 = [[NSDateFormatter alloc] init];

    [outputFormatter1 setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    [outputFormatter1 setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [outputFormatter1 setDateFormat: @"EEEE"];

    [outputFormatter2 setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
    [outputFormatter2 setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [outputFormatter2 setDateFormat: @" HH'h'mm"];

    
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    
    NSString * strOut1 = [ outputFormatter1 stringFromDate:inputDates ];
    NSString * strOut2 = [ outputFormatter2 stringFromDate:inputDates ];

    NSMutableString *mutStr = [NSMutableString new];
    if (strOut1) {
        NSString*strTmp = [strOut1 substringToIndex:3];
        
        strTmp = [strTmp stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                              withString:[[strTmp substringToIndex:1] capitalizedString]];
        
                                                                               
        [mutStr appendString:strTmp];
    }
    
    if (strOut2) {
        [mutStr appendString:strOut2];
    }
    
    return mutStr;
}
@end
