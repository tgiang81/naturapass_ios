//
//  SplashVC.h
//  Naturapass
//
//  Created by Giang on 4/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^callBackClose) ();

typedef void (^callBackGoto) ();


@interface SplashVC : UIViewController
{

}
@property (strong, nonatomic) NSString *strImage;
@property (nonatomic, copy) callBackClose CallBackClose;
@property (nonatomic, copy) callBackGoto callBackGoto;

@property (assign) BOOL isGif;

@end
