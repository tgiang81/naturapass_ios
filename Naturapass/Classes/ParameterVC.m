//
//  ParameterVC.m
//  Naturapass
//
//  Created by Giang on 11/3/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ParameterVC.h"


#import "CellKind9.h"
#import "Parameter_Notification_GlobalVC.h"
#import "Parameter_Favorites.h"
#import "Parameter_Publication.h"
#import "Parameter_ProfilVC.h"
#import "Parameter_MesFavoris.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface ParameterVC ()
{
    NSArray * arrData;
    IBOutlet UILabel *lbTitle;
}
@end

@implementation ParameterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strPARAMETRES);
    // Do any additional setup after loading the view from its nib.
    [Flurry logEvent:@"MyProfileListFragment" timed:YES];

    NSMutableDictionary *dic1 = [@{@"name": str(strPProfil),
                                   @"image":@"mur_st_ic_publication"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":str(strGGeneral),
                                   @"image":@"mur_st_ic_email"} copy];
    NSMutableDictionary *dic3 = [@{@"name":str(strNotifications_email),
                                   @"image":@"mur_st_ic_smartphones"} copy];

    NSMutableDictionary *dic4 = [@{@"name":str(strNotifications_smartphones),
                                   @"image":@"mur_st_ic_smartphones"} copy];
    
    NSMutableDictionary *dic5 = [@{@"name":str(strFFavoris),
                                   @"image":@"mur_st_ic_adresses"} copy];
    
    arrData =  [@[dic1,dic2,dic3,dic4,dic5] copy];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind9" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind9 *cell = nil;
    
    cell = (CellKind9 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    cell.imageIcon.image =  [UIImage imageNamed: dic[@"image"]];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.arrow setImage: [UIImage imageNamed:@"arrow_cell" ]];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
        {
            Parameter_ProfilVC *viewController1 = [[Parameter_ProfilVC alloc] initWithNibName:@"Parameter_ProfilVC" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            //Général
        case 1:
        {
            Parameter_Publication *viewController1 = [[Parameter_Publication alloc] initWithNibName:@"Parameter_Publication" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
        case 2:
        {
            Parameter_Notification_GlobalVC *viewController1 = [[Parameter_Notification_GlobalVC alloc] initWithNibName:@"Parameter_Notification_GlobalVC" bundle:nil];
            viewController1.nofiticationType = ISEMAIL;
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 3:
        {
            Parameter_Notification_GlobalVC *viewController1 = [[Parameter_Notification_GlobalVC alloc] initWithNibName:@"Parameter_Notification_GlobalVC" bundle:nil];
            viewController1.nofiticationType = ISSMARTPHONE;
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
        case 4:
        {
            Parameter_MesFavoris *viewController1 = [[Parameter_MesFavoris alloc] initWithNibName:@"Parameter_MesFavoris" bundle:nil];
            [self pushVC:viewController1 animate:YES];
        }
            break;
            
            
        default:
            break;
    }
}


@end
