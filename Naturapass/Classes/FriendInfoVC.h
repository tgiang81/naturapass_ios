//
//  FriendInfoViewController_New.h
//  Naturapass
//
//  Created by Admin on 4/17/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "FriendInfoCustomCell.h"
#import <MediaPlayer/MediaPlayer.h>
//#import "AsyncImageView.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "BaseVC.h"

#import "BaseVC.h"

@interface FriendInfoVC :BaseVC
{
    IBOutlet UITableView            *searchTableView;
    IBOutlet UIView                 *searchBarView;
    IBOutlet UIView                 *searchListView;
    IBOutlet UIView                 *videoView;
    IBOutlet UIView                 *photoView;
    IBOutlet UIView                 *photoListView;
    IBOutlet UIView                 *videoListView;
    IBOutlet UIView                 *zoomImageView;
    IBOutlet UIView                 *commentView;
    IBOutlet UIView                 *commentDetailView;
    IBOutlet UIView                 *commentuserView;
    IBOutlet UIScrollView           *commentScrollView;

    //array
    NSMutableArray                  *publicationArray;
    NSMutableArray                  *sectionImageArray;
    NSMutableArray                  *searchedArray;
    NSMutableArray                  *searchingArray;
    NSMutableArray                  *photoArray;
    NSMutableArray                  *commentArray;
    //NSMutableArray                  *commentAsyncImageArray;


    BOOL                            searching;
    BOOL                            isSearch;

    
    int imageViewTag;
    //NSMutableArray                  *arrayAsyncImage;
    //NSMutableArray                  *arrayDescriptionImage;

    //button
    IBOutlet UIButton               *commentButton;
    IBOutlet UIButton               *deleteButton;
    IBOutlet UIButton               *muteButton;
    IBOutlet UIButton               *envoyerButton;
    IBOutlet UIButton               *messageButton;
    IBOutlet UIButton               *photoButton;
    IBOutlet UIButton               *videoButton;
    IBOutlet UIButton               *takephotoButton;
    IBOutlet UIButton               *photoLibraryButton;
    IBOutlet UIButton               *cancelButton;
    IBOutlet UIButton               *takevideoButton;
    IBOutlet UIButton               *videoLibraryButton;
    IBOutlet UIButton               *videocancelButton;

    //label
    IBOutlet UILabel                *photoLabel;
    IBOutlet UILabel                *videoLabel;
    IBOutlet UILabel                *messageLabel;
    IBOutlet UILabel                *photoTitleLabel;
    IBOutlet UILabel                *videoTitleLabel;
    IBOutlet UILabel                *userNameLabel;
    IBOutlet UILabel                *dateUserLabel;
    IBOutlet UILabel                *descUserLabel;

    UILabel                         *timeLabel;

    //string
    NSString                        *commentString;
    NSString                        *strSharingType;
    NSString                        *strPubicationID;
    NSString                        *textPull;
    NSString                        *textRelease;
    NSString                        *textLoading;
    NSString                        *strlike;
    NSString                        *strUnlike;
    NSString                        *commentpubicationID;
    NSString                        *strId;
    NSString                        *strLatLoungitude;
    NSString                        *strLatitudes;
    NSString                        *strLongitudes;
    NSString                        *strPosting;
    NSString                        *strUSERDETAILS;
    NSString                        *strRemove;
    BOOL                            isOpen;
    BOOL                            isDragging;
    BOOL                            isLoading;
    BOOL                            isLikeComment;
    BOOL                            isSharing;
    BOOL                            isGroup;
    BOOL                            isPostDevice;

    NSInteger                       flagList;
    NSInteger                       flagcount;
    NSInteger                       responseType;
    //textfeld
    IBOutlet UITextField            *searchTextField;
    IBOutlet UITextField            *commentTextField;
    
    UIImagePickerController         *cameraImagePicker;
    NSMutableData                   *responseData;

    UIActionSheet               *actionSheet;
    NSURL                   *videoRecordurl;
    CGSize                  dataSize;
    MBProgressHUD           *progressHUD;

    NSURLConnection         *urlConnection;
    NSMutableURLRequest     *request;
    NSInteger               signalerInteger;
    
    //refresh
    NSUserDefaults      *homeDefaults;
    NSDate              *todayDate;
    NSDate              *todayTime;
    NSIndexPath         *signalerIndex;
    UIView                  *refreshHeaderView;
    UILabel                 *refreshLabel;
    UIImageView             *refreshArrow;
    UIImageView             *refreshbg;
    IBOutlet UIImageView             *imageUser;
    IBOutlet UIImageView             *userViewImage;
    
    IBOutlet UIImageView    *profileImageview;
    IBOutlet UILabel        *mutualFriendLbl;
    IBOutlet UILabel        *fullNameLbl;
    IBOutlet UILabel        *friendLbl;
    
    IBOutlet UIButton       *addFriendBtn;
    IBOutlet UIButton        *chatBtn;
  
    
    UIActivityIndicatorView *refreshSpinner;
    MPMoviePlayerController *playerView;
    
}
@property(nonatomic,retain)NSString *strSharing;
@property(nonatomic,retain)NSString *strTag;
@property(nonatomic,retain)NSString *slideTag;
@property(nonatomic,retain)NSMutableArray   *slideShareArray;
@property(nonatomic,retain)NSString *slideSTR;

@property(nonatomic,retain) NSString *strPush;
@property(nonatomic,strong) NSString *push_ID;
@property(nonatomic,retain) NSString *pushType;
@property (nonatomic,retain)NSString *strPushView;
@property (nonatomic, retain) NSDictionary *friendDic;

- (void) getPublications;

-(void)push_ID:(NSString *)push_ID pushType:(NSString *)pushType strPush:(NSString *)strPush;
 @end

