//
//  AlertStatisticVC.m
//  Naturapass
//
//  Created by giangtu on 1/27/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import "AlertStatisticVC.h"
#import "RecordingDB.h"

@interface AlertStatisticVC ()

@end

@implementation AlertStatisticVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Auto recording
    RecordingDB.sharedInstance.startDate =  [NSDate date];
    [RecordingDB startPedometerUpdates];
    
    //1 time...just 1 hunt was been recored
    NSString * strID = [NSString stringWithFormat:@"%@_recording_status", [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] ];
    
    //PEDOMETER_RECORDING = 0 -> locationManager will record lat/lon
    [[NSUserDefaults standardUserDefaults] setInteger: 0 forKey: strID ];
    [[NSUserDefaults standardUserDefaults]  synchronize];
    
    
    //fetch sum data from DB for this livehunt.
    //first time zero info...
    NSDictionary* inDic = [RecordingDB getSumaryWithAgenda:self.strLivehunt];

    NSNumber *numDistance = inDic[@"distance"];
    NSNumber *numDenivele = inDic[@"positive"];
    NSNumber *numDeniveleNegatif = inDic[@"negative"];
    NSNumber *numVitesseMoyenne = inDic[@"average"];
    
    lbDistance.text = [NSString stringWithFormat:@"%.1f", [numDistance floatValue]];
    lbDenivele.text = [NSString stringWithFormat:@"%.f", [numDenivele floatValue]];
    lbDeniveleNegatif.text = [NSString stringWithFormat:@"%.f", [numDeniveleNegatif floatValue]];
    lbVitesseMoyenne.text = [NSString stringWithFormat:@"%.1f", [numVitesseMoyenne floatValue]];
    
    [btnPedometer addTarget:self action:@selector(fnMeterAndPause:) forControlEvents:UIControlEventTouchUpInside ];
    [btnTerminal addTarget:self action:@selector(fnTerminalRecord:) forControlEvents:UIControlEventTouchUpInside ];
    
    [btnContinue addTarget:self action:@selector(fnContinueRecord:) forControlEvents:UIControlEventTouchUpInside ];

}

-(IBAction)fnMeterAndPause:(id)sender
{
    NSString * strID = [NSString stringWithFormat:@"%@_recording_status", [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] ];

//    _pedometerStatus = PEDOMETER_PAUSE; 1
    [[NSUserDefaults standardUserDefaults] setInteger: 1 forKey: strID ];
    [[NSUserDefaults standardUserDefaults]  synchronize];

    [RecordingDB stopPedometerUpdates];

    //save current data...to DB
    //Store information : from StartDate -> Now (query pedometer to get information)
    [RecordingDB queryPedometerFromDate:  RecordingDB.sharedInstance.startDate ];
    
    float distance = [RecordingDB.sharedInstance.myPedoMeterData.distance floatValue];
    float average = [RecordingDB.sharedInstance.myPedoMeterData.averageActivePace floatValue];
    
    NSDateFormatter *formatter_local = [[NSDateFormatter alloc] init];
    [formatter_local setTimeZone: [NSTimeZone systemTimeZone]];
    [formatter_local setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    NSString *strStartTime = [formatter_local stringFromDate:RecordingDB.sharedInstance.startDate];
    NSString *strEndTime = [formatter_local stringFromDate: [NSDate date]];
    
    [RecordingDB addWithAgenda:self.strLivehunt
                     startTime:strStartTime
                       endTime:strEndTime
                      distance:distance
                       average:average];
}

-(IBAction)fnTerminalRecord:(id)sender
{
    [RecordingDB stopPedometerUpdates];
}

-(IBAction)fnContinueRecord:(id)sender
{
    //do nothing...auto recording...
}

@end
