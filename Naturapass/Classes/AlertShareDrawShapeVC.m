//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertShareDrawShapeVC.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import "ColorCollectionCell.h"
#import "FileHelper.h"
#import "PublicationOBJ.h"
#import "SlideCustomCell.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "Filter_Cell_Type2.h"


static NSString *identifierSection0 = @"MyTableViewCell0";
static NSString *identifierSection2 = @"MyTableViewCell2";
static NSString *identifierSection3 = @"MyTableViewCell3";
static NSString *identifierSection4 = @"MyTableViewCell4";

@implementation AlertShareDrawShapeVC
{
    NSDictionary* currentData;
    
    NSMutableArray              *mesArr;
    
    NSMutableArray              *sharingGroupsArray;
    NSMutableArray              *sharingHuntsArray;
    
    int iSharing;
    NSDictionary * dataInput;

}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertShareDrawShapeVC" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [subAlertView.layer setMasksToBounds:YES];
        subAlertView.layer.cornerRadius= 3;
        subAlertView.layer.borderWidth =0;
        [COMMON listSubviewsOfView:self];
        
        
        // Do any additional setup after loading the view from its nib.
        [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection0];
        
        //Section2
        [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection2];
        
        [self.tableControl registerNib:[UINib nibWithNibName:@"SlideCustomCell" bundle:nil] forCellReuseIdentifier:identifierSection3];
        
        [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection4];
        self.tableControl.estimatedRowHeight = 36;

    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertShareDrawShapeVCCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertShareDrawShapeVCCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlertWithDic:(NSDictionary*)dicInput
{
    dataInput = dicInput;
    
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
///
    
    sharingGroupsArray= [NSMutableArray new];
    sharingHuntsArray= [NSMutableArray new];
    
    //Filter
    mesArr =        [NSMutableArray new];
    iSharing = 0;
    
   // "c_sharing" = 0;
    if (dataInput[@"c_sharing"]) {
        NSString *newString = dataInput[@"c_sharing"];
        iSharing = [newString intValue];
    }
    BOOL blMoi = iSharing == 0 ? TRUE: NO;
    NSDictionary *dic0 = @{@"categoryName":strMoi,
                           @"categoryImage":@"mes2_icon",
                           @"isSelected": [NSNumber numberWithBool:blMoi]
                           } ;

    BOOL blAmis = iSharing == 1 ? TRUE: NO;

    NSDictionary *dic1 = @{@"categoryName":strAAmis,
                           @"categoryImage":@"mes2_icon",
                           @"isSelected": [NSNumber numberWithBool:blAmis]
                           } ;
    BOOL blNatiz = iSharing == 3 ? TRUE: NO;

    NSDictionary *dic2 = @{@"categoryName":str(strTous_les_natiz),
                           @"categoryImage":strIcon_All_Member,
                           @"isSelected": [NSNumber numberWithBool:blNatiz]
                           };
    
    
    
    mesArr = [NSMutableArray arrayWithArray:@[dic0,dic1,dic2] ];
    
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //get current time
        NSDate *date = [NSDate date];
        NSTimeInterval time = [date timeIntervalSince1970];
        //list shared mes chasses
        int current_time = (int)time;
        
        //list shared mes groups
        //GROUP
        
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            BOOL isSelected = NO;
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmp) {
                if ([kDic[@"groupID"] intValue ]== strID) {
                    //selected
                    isSelected = [kDic[@"isSelected"] boolValue];
                    break;
                }
            }
            
            [sharingGroupsArray addObject:@{@"categoryName":  [[set_querry1 stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ,
                                            @"groupID":  [set_querry1 stringForColumn:@"c_id"],
                                            @"isSelected": [NSNumber numberWithBool:isSelected]
                                            }];
            
        }
        
        //AGENDA
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
        
        
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_hunt WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ AND c_end_date >= %d ",sender_id, current_time];
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            
            int strID = [set_querry_hunt intForColumn:@"c_id"];
            BOOL isSelected = NO;
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmpHunt) {
                if ([kDic[@"huntID"] intValue ]== strID) {
                    //selected
                    isSelected = [kDic[@"isSelected"] boolValue];
                    break;
                }
            }
            
            [sharingHuntsArray addObject:@{@"categoryName":  [[set_querry_hunt stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ,
                                           @"huntID":  [set_querry_hunt stringForColumn:@"c_id"],
                                           @"isSelected": [NSNumber numberWithBool:isSelected]
                                           }];
            
        }
    }];
    
    
    //??? DB fail ??? => Get all. hard fix
    if (sharingHuntsArray.count == 0 && sharingGroupsArray.count == 0) {
        
        //GROUPE
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        for (NSDictionary*kDic in arrTmp) {
            ASLog(@"%@",kDic);
            
            [sharingGroupsArray addObject: @{@"categoryName":  [kDic[@"categoryName"] stringByDecodingHTMLEntities] ,
                                             @"groupID":  kDic[@"groupID"],
                                             @"isSelected": [NSNumber numberWithBool:NO]
                                             }];
        }
        
        //AGENDA
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        for (NSDictionary*kDic in arrTmpHunt) {
            
            [sharingHuntsArray addObject:@{@"categoryName":  [kDic[@"categoryName"] stringByDecodingHTMLEntities] ,
                                           @"huntID":  kDic[@"huntID"],
                                           @"isSelected": [NSNumber numberWithBool:NO]
                                           }];
        }
        
    }
    //    "c_groups" = "[552],[650]";
    //set auto select group/hunt if go in from ...
    //set check list group

    if (dataInput[@"c_groups"]) {
        NSArray *c_Group  = [dataInput[@"c_groups"] componentsSeparatedByString:@","];
        for (NSString *oldString in c_Group) {
            NSString *newString = [oldString stringByReplacingOccurrencesOfString: @"[" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString: @"]" withString:@""];
            for (int i = 0; i<sharingGroupsArray.count; i++) {
                NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
                
                if ([newString  intValue] == [dic[@"groupID"]  intValue])
                {
                    [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    [sharingGroupsArray replaceObjectAtIndex:i withObject:dic];
                    break;
                    
                }
            }

        }

    }
    //    "c_hunts" = "[552],[650]";
    //set check list hunt

    if (dataInput[@"c_hunts"]) {
        NSArray *c_Hunt  = [dataInput[@"c_hunts"] componentsSeparatedByString:@","];
        for (NSString *oldString in c_Hunt) {
            NSString *newString = [oldString stringByReplacingOccurrencesOfString: @"[" withString:@""];
            newString = [newString stringByReplacingOccurrencesOfString: @"]" withString:@""];
            for (int i = 0; i<sharingHuntsArray.count; i++) {
                NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
                
                if ([newString  intValue] == [dic[@"huntID"]  intValue])
                {
                    [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    [sharingHuntsArray replaceObjectAtIndex:i withObject:dic];
                    break;
                    
                }
            }
            
        }
        
    }
    
    
    [self.tableControl reloadData];
    

}
-(IBAction)validerAction:(id)sender
{
    [self removeFromSuperview];

    NSMutableDictionary *dic = [NSMutableDictionary new];
    
    int  MDshare =0;
    //amis
    if ([mesArr[1][@"isSelected"]boolValue]) {
        MDshare =1;
    }
    //naturapass member
    else if ([mesArr[2][@"isSelected"]boolValue]) {
        MDshare =3;
    }
    [dic setValue:[NSNumber numberWithInt:MDshare] forKey:@"sharing"];
    
    NSMutableArray *arrGroup =[NSMutableArray new];
    NSMutableArray *arrHunt =[NSMutableArray new];
    
    if (sharingGroupsArray) {
        for (NSDictionary *dic in sharingGroupsArray) {
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                [arrGroup addObject :dic[@"groupID"]];
            }
        }
        [dic setValue:arrGroup forKey:@"groups"];
    }
    
    if (sharingHuntsArray) {
        for (NSDictionary *dic in sharingHuntsArray) {
            NSNumber *num = dic[@"isSelected"];
            if ( [num boolValue]) {
                [arrHunt addObject :dic[@"huntID"]];
            }
        }
        [dic setValue:arrHunt forKey:@"hunts"];
    }
    
    if (_callback) {
        _callback(@"valider",dic);
    }
}
-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(@"dissmiss",nil);
    }
}
-(IBAction)backAction:(id)sender
{
    [self removeFromSuperview];
    if (_callback) {
        _callback(@"back",nil);
    }
}
#pragma mark -  TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //don't show Federation
    
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return mesArr.count;
            break;
        case 1:
            return sharingGroupsArray.count;
            break;
        case 2:
            return sharingHuntsArray.count;
            break;
            
        default:
            return 0;
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return UITableViewAutomaticDimension;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 50;
    }
    else if (section == 1) {
        
        if (sharingGroupsArray.count>0) {
            return 50;
        }
        else
        {
            return 0;
        }
    }
    else if (section == 2) {
        
        if (sharingHuntsArray.count>0) {
            return 50;
        }
        else
        {
            return 0;
        }
    }else{
        return 20;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionView = nil;
    
    if (section==0) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 200, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont boldSystemFontOfSize:16]];
        headerLabel.text= str(strPARTAGER_AVEC);
        [sectionView addSubview:headerLabel];
        
        
    }
    else if (section==1) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 200, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:FONT_HELVETICANEUE(16)];
        headerLabel.text=str(strMesGroupes);
        [sectionView addSubview:headerLabel];
    }else if (section==2) {
        sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 200, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:FONT_HELVETICANEUE(16)];
        headerLabel.text= str(strMesChantier);
        [sectionView addSubview:headerLabel];
    }
    
    
//    [sectionView setBackgroundColor: UIColorFromRGB(TABLE_BACKGROUND_COLOR) ];
    [sectionView setBackgroundColor: [UIColor whiteColor]];

    //Divider on top
    UIImageView *dividerImage =[[UIImageView alloc]initWithFrame:CGRectMake(18, 10, 255, 1)];
    
    [dividerImage setImage:[UIImage imageNamed:@"divider1"]];
    
    [dividerImage setAlpha:1];
    [sectionView addSubview:dividerImage];
    
    return sectionView;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    SlideCustomCell *cell = nil;
    
    if (indexPath.section == 0) {
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
    }else if (indexPath.section == 1) {
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
    }else{
        cell = (SlideCustomCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3 forIndexPath:indexPath];
    }
    
//    [cell setBackgroundColor: UIColorFromRGB(TABLE_BACKGROUND_COLOR) ];
    [cell setBackgroundColor: [UIColor whiteColor]];

    
    if (indexPath.section == 0)
    {
        NSDictionary*dic = [mesArr objectAtIndex:indexPath.row];
        [cell setListLabel:dic[@"categoryName"]];
        [cell setListImages:dic[@"categoryImage"]];
        [cell.tickButton setTag: indexPath.row + 10];
        NSNumber *num = dic[@"isSelected"];
        
        if ( [num boolValue]) {
            [cell.tickButton setSelected:TRUE];
        }else{
            [cell.tickButton setSelected:FALSE];
        }
        
        [cell.tickButton addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (indexPath.section == 1)
    {
        NSDictionary*dic = [sharingGroupsArray objectAtIndex:indexPath.row];
        NSString *strName = [dic[@"categoryName"] emo_emojiString];
        
        //Mes group
        [cell setListLabel:strName];
        [cell setListImages:@"sharechamp"];
        [cell.tickButton setTag: indexPath.row + 100];
        NSNumber *num = dic[@"isSelected"];
        
        if ( [num boolValue]) {
            [cell.tickButton setSelected:TRUE];
        }else{
            [cell.tickButton setSelected:FALSE];
        }
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesGroup:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (indexPath.section == 2)
    {
        NSDictionary*dic = [sharingHuntsArray objectAtIndex:indexPath.row];
        NSString *strName = [dic[@"categoryName"] emo_emojiString];
        
        //Mes hunt
        [cell setListLabel:strName];
        [cell setListImages:@"sharechamp"];
        [cell.tickButton setTag: indexPath.row + 100];
        NSNumber *num = dic[@"isSelected"];
        
        if ( [num boolValue]) {
            [cell.tickButton setSelected:TRUE];
        }else{
            [cell.tickButton setSelected:FALSE];
        }
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesHunts:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
}
-(void)selectTickAction:(id)sender{
    NSInteger index =[sender tag] - 10;
    NSMutableDictionary *dic=[[mesArr objectAtIndex:index] mutableCopy];
    
    for (int i = 0; i < mesArr.count; i++) {
        NSMutableDictionary *dic0 = [mesArr[i] mutableCopy];
        [dic0 setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [mesArr replaceObjectAtIndex:i withObject:dic0];

    }
    //refresh
    [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [mesArr replaceObjectAtIndex:index withObject:dic];
    [self.tableControl reloadData];
}

-(IBAction)selectTickActionMesGroup:(UIButton*)sender
{
    NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    
    if ([sender isSelected])
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingGroupsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}

-(IBAction)selectTickActionMesHunts:(UIButton*)sender
{
    NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    
    if ([sender isSelected])
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingHuntsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}

@end
