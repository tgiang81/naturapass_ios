//
//  NotificationVC.m
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MemberBlackListVC.h"
#import "Define.h"

#import "TypeCellBlacklisterCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ASSharedTimeFormatter.h"

static NSString *identifierSection1 = @"MyTableViewCell1";



@interface MemberBlackListVC ()
{
    NSMutableArray *arrData;
    IBOutlet UILabel *lbTitle;
}
@property (weak, nonatomic) IBOutlet UILabel *warningLb;

@end

@implementation MemberBlackListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMEMBRES_BLACKLISTER);
    // Do any additional setup after loading the view from its nib.
    
    arrData = [NSMutableArray new];

    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCellBlacklisterCell" bundle:nil] forCellReuseIdentifier:identifierSection1];

    _warningLb.text = str(strWarningNoBlocker);

    _warningLb.hidden = YES;
    
    self.tableControl.estimatedRowHeight = 66;
    self.tableControl.rowHeight = UITableViewAutomaticDimension;

    [self initRefreshControl];
    [self startRefreshControl];

    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}
- (void)insertRowAtTop {
    [self getUserLockAction];
}
- (void)insertRowAtBottom {
    [self stopRefreshControl];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getUserLockAction
{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj getUserLock];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        
        [arrData removeAllObjects];
        
        if (!response) {
            [self.tableControl reloadData];
            return;
        }
        
        if ([response[@"locked"] isKindOfClass:[NSArray class]]) {
            arrData = [response[@"locked"] mutableCopy];
            
            [self checkShowWarning];
           /* {
                "id": 160,
                "fullname": "Réal Boubet",
                "usertag": "real-boubet"
            }
            */
        }
        [self.tableControl reloadData];
    };

}

-(void) checkShowWarning
{
    if (arrData.count <= 0) {
        _warningLb.hidden = NO;
    }else{
        _warningLb.hidden = YES;
    }

}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60+6;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TypeCellBlacklisterCell *cell = nil;
    
    cell = (TypeCellBlacklisterCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    //imgage
    NSString * strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];

    strImage = [[strImage stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSURL * url = [[NSURL alloc] initWithString:strImage];
    [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    
    //Format
    NSString *strContent =dic[@"fullname"];
    cell.label1.text= strContent;
    cell.label1.textColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    
    //read
    [cell.btnDel addTarget:self action:@selector(delBlackLister:) forControlEvents:UIControlEventTouchUpInside];
    cell.imgXIcon.hidden=NO;
    cell.btnDel.tag=indexPath.row+100;
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}
-(IBAction)delBlackLister:(id)sender
{
    int index  = (int)[sender tag]-100;
    [UIAlertView showWithTitle:str(strTitle_app)
                       message:str(strDELETE_BLACKLISTER)
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[str(strNon)]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              
                              [COMMON addLoading:self];
                              WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                              [serviceObj deleteUserLock:arrData[index][@"id"]];
                              serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                  [COMMON removeProgressLoading];
                                  if (!response) {
                                      return;
                                  }
                                  if (response[@"success"]) {
                                      [arrData removeObjectAtIndex:index];
                                      [self checkShowWarning];
                                      [self.tableControl reloadData];
                                  }
                              };
                          }
                          else
                          {
                              
                          }
                      }];
    
}

@end
