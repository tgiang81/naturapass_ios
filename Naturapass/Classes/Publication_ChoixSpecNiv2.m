//
//  Publication_ChoixSpecNiv2.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_ChoixSpecNiv2.h"
#import "Publication_ChoixSpecNiv4.h"

#import "SubNavigationPRE_Search.h"

#import "Publication_ChoixSpecNiv5.h"
#import "Publication_Partage.h"
#import "CellKind2.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Publication_ChoixSpecNiv2 ()<UITextFieldDelegate>
{
    NSMutableArray * arrData;
    
    //if search
    UITextField *myTextField;
    
}
@end

@implementation Publication_ChoixSpecNiv2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [NSMutableArray new];
    
    /*
     
     //children = 0, when card exist
     
     "children": {
     
     },
     "card": {
     "id": 1,
     "name": "Indetermine-MâleAdulte-FemelleAdulte-M
     âleJeune-FemelleJeune"
     }
     
     */
    for (NSDictionary*dic in self.myDic[@"children"]) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {
            [arrData addObject:dic];
        }
    }
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    if ([self.myDic[@"search"] intValue] ==  1)
    //    {
    //        //Display search
    //        [self addSubNav:@"SubNavigationPRE_Search"];
    //
    //        SubNavigationPRE_Search *okSubView = (SubNavigationPRE_Search*)self.subview;
    //
    //        UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    //
    //        btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
    //
    //        UITextField *btn2 = (UITextField*)[self.subview viewWithTag:START_SUB_NAV_TAG+1];
    //        btn2.delegate = self;
    //        myTextField = btn2;
    //
    //        [[NSNotificationCenter defaultCenter]  addObserver:self
    //                                                  selector:@selector(textFieldDidChange:)
    //                                                      name:UITextFieldTextDidChangeNotification
    //                                                    object:btn2];
    //
    //        [self textFieldDidChange:nil];
    //    }
    //    else
    //    {
    //
    //    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    cell.constraint_image_width.constant = cell.constraint_image_height.constant = 0;
    
    //    NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    //
    //    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString: imgUrl ] ];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    //Arrow
    UIImageView *btn = [UIImageView new];
    [btn setImage: [UIImage imageNamed:@"arrow_icon" ]];
    
    btn.contentMode = UIViewContentModeScaleToFill;
    
    cell.constraint_control_width.constant = 8;
    cell.constraint_control_height.constant = 13;
    
    btn.frame = CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant);
    
    cell.constraintRight.constant = 15;
    
    
    [cell.viewControl addSubview:btn];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = arrData[indexPath.row];
    
    //Specific
    
    
    
    if ( ( (NSArray*)dic[@"children"]).count > 0 ) {
        
        //has search option
        if ([dic[@"search"] intValue] ==  1)
        {
            Publication_ChoixSpecNiv4 *viewController1 = [[Publication_ChoixSpecNiv4 alloc] initWithNibName:@"Publication_ChoixSpecNiv4" bundle:nil];
            
            viewController1.myDic = dic;
            viewController1.myName =  [NSString stringWithFormat:@"%@/%@",self.myName, dic[@"name"] ];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
        }else{
            Publication_ChoixSpecNiv2 *viewController1 = [[Publication_ChoixSpecNiv2 alloc] initWithNibName:@"Publication_ChoixSpecNiv2" bundle:nil];
            
            viewController1.myDic = dic;
            viewController1.myName =  [NSString stringWithFormat:@"%@/%@",self.myName, dic[@"name"] ];
            
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
    }else{
        //Show card....
        if ( dic[@"card"] ) {
            //LEAF
            Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
            viewController1.myDic = dic;
            viewController1.myName =  [NSString stringWithFormat:@"%@/%@",self.myName, dic[@"name"] ];
            
            viewController1.iSpecific =  0;//[self.myDic[@"search"] intValue] ;
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
        }else{
            
            NSString *strPrecision =[NSString stringWithFormat:@"%@/%@",self.myName,dic[@"name"]];
            NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
            if (strPrecision) {
                [dicFavo setValue:strPrecision forKey:@"category_tree"];
            }
            
            NSDictionary *dicObservation = @{
                                             @"specific": [NSNumber numberWithInt: 0 ],
                                             @"category" : dic[@"id"]
                                             };
            [dicFavo setValue:dicObservation forKey:@"observation"];
            [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
            if ([dic[@"receivers"] isKindOfClass: [NSArray class]]) {
                [PublicationOBJ sharedInstance].arrReceivers = dic[@"receivers"];
                [dicFavo setValue:dic[@"receivers"] forKey:@"receivers"];
                
            }
            [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
            
            //edit ob
            if ([PublicationOBJ sharedInstance].isEditer) {
                
                //                [[PublicationOBJ sharedInstance] modifiObservationWithVC:self];
                [[PublicationOBJ sharedInstance]  modifiPublication:self  withType:EDIT_PRECISIONS];
                
            }
            else if ([PublicationOBJ sharedInstance].isEditFavo)
            {
                [self backEditFavo];
            }
            else
            {
                //
                /*
                 GROUP
                 When we publish, the sharing should be directly with the checked group => i think that we should not ask the question (take off question page = directly sharing)
                 => OK, we can  take off question page and select the group for sharing.
                 */
                
                Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
        }
        
    }
}

#pragma mark - textfield delegate
//- (void)textFieldDidBeginEditing:(UITextField *)textField {
//}
//
//- (void)textFieldDidEndEditing:(UITextField *)textField {
//    //Validate next step
//}
//- (BOOL) textFieldShouldReturn:(UITextField *)textField{
//    [textField resignFirstResponder];
//    return YES;
//}

//- (void)textFieldDidChange:(NSNotification *)info {
// Update the character count

//    NSLog(@"%@",myTextField.text);
//    //do filter...
//
//    if ([myTextField.text isEqualToString:@""]){
//
//        arrFilter = [arrData copy];
//    } else {
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.name contains[cd] %@", myTextField.text];
//        arrFilter = [arrData filteredArrayUsingPredicate:predicate];
//    }
//
//    [self.tableControl reloadData];
//}
@end
