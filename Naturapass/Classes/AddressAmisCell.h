//
//  CellKind1.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MDCheckBox.h"
#import "AppCommon.h"

@interface AddressAmisCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (weak, nonatomic) IBOutlet UILabel *label2;

@property (weak, nonatomic) IBOutlet MDCheckBox *viewControl;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;

@end
