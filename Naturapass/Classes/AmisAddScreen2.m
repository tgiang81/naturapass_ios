//
//  AmisAddScreen2.m
//  Naturapass
//
//  Created by Giang on 9/10/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisAddScreen2.h"
#import "CellKind2.h"
#import "AmisAddContent.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "MDCheckBox.h"
#import "AddressAmisCell.h"

#import "Amis_Demand_Invitation.h"
#import "AmisAddScreen1.h"
#import "AmisSearchVC.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface AmisAddScreen2 ()
{
    NSMutableArray *listFinalEmail;
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescriptionScreen;
    __weak IBOutlet UIButton *btnAjouter;
}

@end

@implementation AmisAddScreen2

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strINVITEZVOSAMIS);
    lblDescriptionScreen.text = str(strAppuyez_sur_le_bouton);
    [btnAjouter setTitle:str(strValider_votre_liste) forState:UIControlStateNormal];
   listFinalEmail =  [NSMutableArray new];
    
    listFinalEmail = [NSMutableArray arrayWithArray:self.listEmails];
    [self.tableControl registerNib:[UINib nibWithNibName:@"AddressAmisCell" bundle:nil] forCellReuseIdentifier:identifierSection1];

    [self.tableControl reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listFinalEmail.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressAmisCell *cell = nil;
    
    cell = (AddressAmisCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = listFinalEmail[indexPath.row];
    
    cell.imageIcon.image = dic[@"image"];
    [cell.imageIcon.layer setMasksToBounds:YES];
    cell.imageIcon.layer.cornerRadius= 30;
    
    cell.label1.text =dic[@"name"];
    cell.label2.text =dic[@"email"];
    
    cell.label1.numberOfLines=1;
    cell.viewControl.tag=indexPath.row+100;
    
    [cell.viewControl setImage: [UIImage imageNamed:@"ic_remove_contact"] forState:UIControlStateNormal];
    [cell.viewControl setSelected:NO];
    cell.viewControl.userInteractionEnabled=NO;

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(IBAction)fnForward:(id)sender
{
    NSMutableArray *mutDic = [NSMutableArray new];
    
    for (NSDictionary*dic in listFinalEmail) {
        [mutDic addObject:dic[@"email"] ];
    }
    
    if (mutDic.count > 0) {
        AmisAddContent *viewController1 = [[AmisAddContent alloc] initWithNibName:@"AmisAddContent" bundle:nil];
        viewController1.listEmails = mutDic;
        [self pushVC:viewController1 animate:YES expectTarget:ISAMIS iAmParent:NO];
        
    }
}

@end
