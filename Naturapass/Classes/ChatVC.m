//
//  ChatVC
//  Naturapass
//
//  Created by Giang on 8/17/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatVC.h"
#import <QuartzCore/QuartzCore.h>
#import "Naturapass-Swift.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "ASSharedTimeFormatter.h"
#import "GroupEnterOBJ.h"

#import "MessageCell.h"
#import "Message.h"
#import "SAMTextView.h"
#import "DiscustionsEntity.h"
#import "MessageWaitToSend.h"
#import "NSDate+Extensions.h"
#import "LiveHuntOBJ.h"
#import "FriendInfoVC.h"
#import "GroupEnterMurVC.h"
#import "AlertVC.h"
#import "CommonHelper.h"
#import "ServiceHelper.h"

static NSString *CellIdentifier = @"cellIdentifier";

static int minHeight =44;
static int maxHeight =33*3;
static int detal =2;

@interface ChatVC () <UIGestureRecognizerDelegate,UITextViewDelegate>
{
    
    SocketIOClient* socket;
    NSMutableArray  *messageArray;
    //    IBOutlet UITextField *txtChat;
    int                             kbHeight;
    __weak IBOutlet NSLayoutConstraint *constraintMessageView;
    __weak IBOutlet NSLayoutConstraint *constraintMessageHeight;
    
    UITapGestureRecognizer *tapGesture;
    IBOutlet UIImageView *imgSend;
    IBOutlet UIButton *btnSilence;
    IBOutlet UIImageView *imgHangout;

    NSMutableArray *arrParticipants;
    BOOL animateHeightChange;
    NSTimeInterval animationDuration;
    NSString*  strID, *messageID;
    BOOL statusSelence;
    SocketManager* manager;
    
}
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;
@property (nonatomic,strong)   IBOutlet SAMTextView  *textView;
@property (nonatomic,strong)   IBOutlet UIView  *viewFooter;

@end

@implementation ChatVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doUpdateCacheChat:) name: UPDATE_CACHE_CHAT object:nil];
    self.lbMessage.text =EMPTY_DISCUSTION;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethod:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myNotificationMethodHide:) name:UIKeyboardDidHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disableChat:) name: DISABLE_CHAT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateRightToChat:) name: UPDATE_RIGHT_CHAT object:nil];
    
    sphBubbledata =[[NSMutableArray alloc]init];
    
    arrParticipants=[[NSMutableArray alloc]init];
    
    tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(handleTapGesture:)];
    messageArray =[[NSMutableArray alloc]init];
    
    [self.tableControl registerClass:[MessageCell class] forCellReuseIdentifier: CellIdentifier];
    [btnSilence setImage:[UIImage imageNamed:@"icon_silence_active"] forState:UIControlStateNormal];
    [btnSilence setImage:[UIImage imageNamed:@"icon_silence_inactive"] forState:UIControlStateSelected];
    btnSilence.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    //Default...Expected Has push...
    statusSelence = YES;
    btnSilence.selected = YES;
    btnSilence.layer.masksToBounds = YES;
    btnSilence.layer.cornerRadius = 4;
    btnSilence.hidden = YES;
    imgHangout.hidden = YES;
    switch (self.expectTarget) {
        case ISMUR:
        {
            
        }
            break;
        case ISGROUP:
        {
            strID = [NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
            messageID =@"id";
            imgHangout.hidden = NO;
            imgHangout.image = [UIImage imageNamed:@"ic_group_hangout"];
        }
            break;
        case ISLOUNGE:
        {
            strID = [NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]];
            messageID =@"id";
            imgHangout.hidden = NO;
            imgHangout.image = [UIImage imageNamed:@"ic_chasse_hangout"];

        }
            break;
            
        case ISDISCUSS:
        {
            strID =self.myConversationID;
            messageID=@"messageId";
            btnSilence.hidden = NO;
        }
            break;
            
        default:
            //HOME
        {
        }
            break;
    }

    [self initRefreshControl];
    [self isRemoveScrollingViewHeight: YES];
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationMUR"];
        }
            break;
        case ISGROUP:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
            if (self.isNotifi) {
                self.myConversationID = self.IdNotifi;
            }
            
        }
            break;
        case ISLOUNGE:
        {
            [self checkAllow];
            if (self.isLiveHunt) {
                UINavigationBar *navBar=self.navigationController.navigationBar;
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
                [self addMainNav:@"MainNavLiveMap"];
                //SUB
                [self addSubNav:@"livechat"];
//                [self setThemeTabVC:self];
                
                if ([LiveHuntOBJ sharedInstance].dicLiveHunt) {
                    [self.navLiveMap.lbHuntName setText:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"name"]];
                    [self.navLiveMap.lbHuntTime setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: [LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_begin"] withDateEnd:[LiveHuntOBJ sharedInstance].dicLiveHunt[@"meeting"][@"date_end"]]];
                }
                if (self.isNotifi) {
                    self.myConversationID = self.IdNotifi;
                    [self gotoNotifi_leaves];
                }
            }
            else
            {
                if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                    if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                    {
                        //admin
                        self.dicOption =@{@"admin":@1};
                        
                    }else{
                        self.dicOption =@{@"admin":@0};
                        
                    }
                    
                }else{
                    self.dicOption =@{@"admin":@0};
                }
                
                //add sub navigation
                [self addMainNav:@"MainNavMUR"];
                
                //Set title
                MainNavigationBaseView *subview =  [self getSubMainView];
                [subview.myTitle setText:str(strCHANTIERS)];
                
                //Change background color
                subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                
                
                [self addSubNav:@"SubNavigationGROUPENTER"];
                if (self.needRemoveSubItem) {
                    [self removeItem:4];
                }
                
                
                if (self.isNotifi) {
                    self.myConversationID = self.IdNotifi;
                    [self gotoNotifi_leaves];
                }
            }
        }
            break;
            
        case ISDISCUSS:
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strDISCUSSIONS)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigation_DISCUSSION"];
            
            // get discussion message
            //            [_textView becomeFirstResponder];
            if (self.isNotifi) {
                self.myConversationID = self.IdNotifi;
                //                [self gotoNotifi_leaves];
            }
            if (_IS_CREATE_SPECIAL) {
                // get discussion message
                [self insertRowAtTop];
                [_textView becomeFirstResponder];
            }

            
            
        }
            break;
            

        default:
            //HOME
        {
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self addSubNav:@"SubNavigationMUR"];
            imgHangout.hidden = NO;
            imgHangout.image = [UIImage imageNamed:@"ic_chasse_hangout"];

        }
            break;
    }
    
    
    if (self.isNotifi)
    {
        
        [self.subview removeFromSuperview];
        [self addSubNav:@"SubNavigationMUR"];
        
        
        self.isNotifi =!self.isNotifi;
    }

    
    switch (self.expectTarget) {
        case ISMUR:
        {
            imgSend.image = [UIImage imageNamed:@"mur_ic_send"];
            btnSilence.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
        }
            break;
        case ISGROUP:
        {
            imgSend.image = [UIImage imageNamed:@"group_ic_send_post"];
            btnSilence.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
        }
            break;
        case ISLOUNGE:
        {
            if (self.isLiveHunt) {
                [imgSend setImage:[[UIImage imageNamed:@"chasse_ic_sent_comment"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
                imgSend.tintColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                
                imgHangout.image = [[UIImage imageNamed:@"ic_chasse_hangout"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
                imgHangout.tintColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);

                btnSilence.backgroundColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                
            }
            else
            {
                imgSend.image = [UIImage imageNamed:@"chasse_ic_sent_comment"];
                btnSilence.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            }
            
        }
            break;
        default:
            //DISCUSSION
        {
            imgSend.image = [UIImage imageNamed:@"discussion_ic_send"];
            btnSilence.backgroundColor = UIColorFromRGB(DISCUSSION_MAIN_BAR_COLOR);
        }
            break;
    }
    [self addTextView];
    
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter]  removeObserver:self];
    
    [socket disconnect];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [Flurry logEvent:@"discussion.DiscussionFragment" timed:YES];
    
    NSDictionary *dic = nil;
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
        }
            break;
        case ISGROUP:
        {
            
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            [self addSubNav:@"SubNavigationGROUPENTER"];
            if (self.isNotifi) {
            }
            else
            {
                [self loadCacheWithID:[NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]]];
                [self  getGroupMessageOffsetAction:LOAD_CURRENT];
                [self connectGroup];
            }
            
        }
            break;
        case ISLOUNGE:
        {
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }
            
            if (self.isLiveHunt) {
                [navBar setBarTintColor: UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR) ];
            }
            else
            {
                [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            }
            
            
            if (self.isNotifi) {
                self.isNotifi=!self.isNotifi;
                
            }
            else
            {
                [self loadCacheWithID:[NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]]];
                [self getChassOffsetMessage:LOAD_CURRENT];
                [self connectChasse];
            }
            
            
        }
            break;
            
        case ISDISCUSS:
        {
            [self setThemeNavSub:NO withDicOption:nil];
            
            [navBar setBarTintColor: UIColorFromRGB(DISCUSSION_TINY_BAR_COLOR) ];
            if (_IS_CREATE_SPECIAL) {
                //check in cache: if have in cache then load cache, not then get api
                BOOL isCache = NO;
                NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_CHATLISTE_SAVE)];
                NSDictionary *dicTmp = [NSDictionary dictionaryWithContentsOfFile:strPath];
                if (dicTmp.count > 0) {
                    //List message
                    if (dicTmp && [dicTmp[@"messages"] isKindOfClass: [NSArray class]]) {
                        NSArray *arr = dicTmp[@"messages"];
                        isCache = [self checkConversationID:arr];
                    }
                }
                if (!isCache) {
                    [COMMON addLoading:self];
                    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                    
                    [serviceObj fnGET_ALL_CONVERSATIONS:@"1000" offset:0 limit_hunt:@"0" offset_hunt:0 limit_group:@"0" offset_group:0];
                    __weak typeof(self) wself = self;
                    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                        [COMMON removeProgressLoading];
                        [self stopRefreshControl];
                        if([wself fnCheckResponse:response]) return;
                        if (!response) {
                            return ;
                        }
                        if([response isKindOfClass: [NSArray class] ]) {
                            return ;
                        }
                        //List message
                        if (response && [response[@"messages"] isKindOfClass: [NSArray class]]) {
                            NSArray *arr = response[@"messages"];
                            [self checkConversationID:arr];
                        }
                    };
                }

            }
            else
            {
                //setup connect socket
                if (self.isNotifi) {
                    self.isNotifi=!self.isNotifi;
                    [self gotoNotifi_leaves];
                    [self connectDiscussion];
                }
                else
                {
                    [self loadCacheWithID:self.myConversationID];
                    [self getMessageForConversation:self.myConversationID withstate:LOAD_CURRENT];
                    [self connectDiscussion];
                }
            }
            

        }
            break;
        default:
            //HOME
        {
        }
            break;
    }
    [self setThemeNavSub:NO withDicOption:dic];
    UIButton *btn = [self returnButton];
    
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
}

#pragma MAIN - NAV EVENT

-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map

    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            [self gotoback];
        }
            break;
            
        case 1:
        {
            [self gotoback];
            
        }
        break;
            
        default:
            break;
    }
}

-(BOOL)checkConversationID:(NSArray*)arr
{
    for (NSDictionary*dic in arr) {
        NSArray *arrTmp = dic[@"conversation"][@"participants"];

        if ([self checkListFriend:arrTmp arrFriend:self.arrFriendDiscussion]) {
            self.myConversationID = dic[@"conversation"][@"id"];
            [self loadCacheWithID:self.myConversationID];
            [self getMessageForConversation:self.myConversationID withstate:LOAD_CURRENT];
            [self connectDiscussion];
            return  YES;

        }
    }
    return NO;
}
-(BOOL)checkListFriend:(NSArray*)arrPart arrFriend:(NSArray*)arrFriend
{
    int count = 0;
    
    if (arrPart.count == arrFriend.count && arrFriend.count > 0) {
        for (NSDictionary *dic1 in arrPart) {
            for (NSDictionary *dic2 in arrFriend) {
                if ([dic1[@"id"] intValue] == [dic2[@"id"] intValue]) {
                    count++;
                }
            }
        }
    }
    
    if (count > 0 && arrPart.count == count) {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(void) disableChat:(NSNotification*)notif
{
    if (self.expectTarget == ISLOUNGE)
    {
        allow_add_chat=  NO;
        [self addTextView];
        
        NSDate *datetime = (NSDate*)[notif object];
        //Display
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
        NSString *timeStr = [self convertDate:[formatter stringFromDate:datetime]];
        
        for (int i = 0 ; i<sphBubbledata.count; i++) {
            Message *msg = sphBubbledata[i];
            if ([msg.date isEqualToString:timeStr])
            {
                [sphBubbledata removeObjectAtIndex:i];
                [self.tableControl reloadData];
            }
        }
    }
}

-(void) updateRightToChat:(NSNotification*)notif
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //refresh param...
        [self checkAllow];
        
        //refresh UI
        if (self.expectTarget == ISLOUNGE)
        {
            if (!allow_show_chat == YES) {
                //jum chasses wall
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
                return ;
            }
            
            [self addTextView];
        }
    });
    
    
}

#pragma mark - socket

-(void) connectGroup
{
    NSURL* url = [[NSURL alloc] initWithString:LINK_SOCKET];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    socket = manager.defaultSocket;

    [socket onAny:^(SocketAnyEvent * test) {
        
        //        NSLog(@"EVENT: %@ -- count %d",test.event, (int)test.items.count);
        
        if ([test.event isEqualToString: @"npevent-group:message"]) {
            //Got message
            if (test.items.count > 0) {
                //                NSLog(@"MESSAGE:  %@", test.items[0]);
            }
        }
    }];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        
        [socket emit:@"npevent-user:connected" with:@[      @{@"usertag":userTag }]];
        //reg lounge
        NSString *grouptag =[GroupEnterOBJ sharedInstance].dictionaryGroup[@"grouptag"] ;
        
        //avoid crash-> check ""
        [socket emit:@"npevent-group:join" with:@[grouptag?grouptag:@""]];
        
    } ];
    //reg message
    [socket on:@"npevent-group:message" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //        NSLog(@"DEF");
        if (data.count > 0) {
            //            NSLog(@"MESSAGE FUNCTION:  %@", data[0]);
            NSDictionary *dic = (NSDictionary*)data[0];
            
            if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                return;
            }
            
            NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            
            [self adddMediaBubbledata:MessageSenderSomeone mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[@"id"] name:dic[@"owner"][@"fullname"] owner_ID:dic[@"owner"][@"id"]];
            [self.tableControl reloadData];
            
            [self scrollToBottom];
            [self saveCacheWithIDWithArrData:@[dic]];
        }
    } ];
    
    [socket connect];
}

-(void) connectChasse
{
    NSURL* url = [[NSURL alloc] initWithString:LINK_SOCKET];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    socket = manager.defaultSocket;

    [socket onAny:^(SocketAnyEvent * test) {
        
        //        NSLog(@"EVENT: %@ -- count %d",test.event, (int)test.items.count);
        
        if ([test.event isEqualToString: @"npevent-lounge:message"]) {
            //Got message
            if (test.items.count > 0) {
                //                NSLog(@"MESSAGE:  %@", test.items[0]);
            }
        }
    }];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        
        [socket emit:@"npevent-user:connected" with:@[      @{@"usertag":userTag }]];
        //reg lounge
        NSString *loungetag =[GroupEnterOBJ sharedInstance].dictionaryGroup[@"loungetag"] ;
        [socket emit:@"npevent-lounge:join" with:@[loungetag?loungetag:@""]];
        
    } ];
    //reg message
    [socket on:@"npevent-lounge:message" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //        NSLog(@"DEF");
        if (data.count > 0) {
            NSDictionary *dic = (NSDictionary*)data[0];
            
            //            NSLog(@"MESSAGE FUNCTION:  %@", data[0]);
            
            NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            
            if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                return;
            }
            
            //if admin = 0 and allow_show = 0
            
            if (allow_show_chat == NO) {
                
                BOOL isAdmin = NO;
                
                if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                    if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                    {
                        isAdmin = YES;
                        //admin
                    }
                }
                
                if (isAdmin == NO) {
                    return;
                }
                
            }
            
            [self adddMediaBubbledata:MessageSenderSomeone mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[@"id"] name:dic[@"owner"][@"fullname"] owner_ID:dic[@"owner"][@"id"]];
            
            [self.tableControl reloadData];
            
            [self scrollToBottom];
            [self saveCacheWithIDWithArrData:@[dic]];
        }
    } ];
    
    [socket connect];
}

-(void) connectDiscussion
{
    NSURL* url = [[NSURL alloc] initWithString:LINK_SOCKET];
    manager = [[SocketManager alloc] initWithSocketURL:url config:@{@"log": @YES, @"forcePolling": @YES}];
    socket = manager.defaultSocket;

    [socket onAny:^(SocketAnyEvent * test) {
        
        //        NSLog(@"EVENT: %@ -- count 1%d",test.event, (int)test.items.count);
        
        if ([test.event isEqualToString: @"npevent-chat-message:incoming"]) {
            //Got message
            if (test.items.count > 0) {
                //                NSLog(@"MESSAGE:  %@", test.items[0]);
            }
        }
    }];
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        
        [socket emit:@"npevent-user/connected" with:@[      @{@"usertag":userTag }]];
        
        [socket emit:@"npevent-user:connected" with:@[      @{@"usertag":userTag }]];
        
        //avoid received message from other conversation
        [socket emit:@"npevent-chat-message:join" with:@[      @{@"conversation": [NSString stringWithFormat:@"conversation%@",self.myConversationID] }]];
        
    } ];
    //reg message
    [socket on:@"npevent-chat-message:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //        NSLog(@"DEF");
        if (data.count > 0) {
            //            NSLog(@"MESSAGE FUNCTION:  %@", data[0]);
            NSDictionary *dic = (NSDictionary*)data[0];
            if ([dic[@"conversation"][@"id"] intValue] == [self.myConversationID intValue]) {
                if ( [dic[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                    return;
                }
                NSString *imgUrl = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
                
                [self adddMediaBubbledata:MessageSenderSomeone mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[@"id"] name:dic[@"owner"][@"fullname"] owner_ID:dic[@"owner"][@"id"]];
                [self.tableControl reloadData];
                
                [self scrollToBottom];
                [self saveCacheWithIDWithArrData:@[dic]];
            }
        }
    } ];
    
    [socket connect];
}

#pragma mark - ADD CELL
-(void)adddMediaBubbledata:(MessageSender)mediaType  mediaPath:(NSString*)mediaPath mtime:(NSString*)messageTime thumb:(NSString*)thumbUrl  downloadstatus:(NSString*)downloadstatus sendingStatus:(NSString*)sendingStatus msg_ID:(NSString*)msgID name:(NSString*)name owner_ID:(NSString*)owner_ID
{
    mediaPath = [mediaPath isKindOfClass:[NSString class]]?mediaPath:@"";
    
    Message *message = [[Message alloc] init];
    
    //emoji
    message.text = [mediaPath emo_emojiString];
    message.sender =  mediaType;
    message.date = [self convertDate:messageTime];
    message.imgAvatar =thumbUrl;
    message.name =name;
    message.chat_id = msgID;
    message.ownerID = owner_ID;
    BOOL exist =NO;
    for (int i = 0 ; i<sphBubbledata.count; i++) {
        Message *msg = sphBubbledata[i];
        if ([message.chat_id intValue]>0) {
            if ([msg.chat_id intValue] == [message.chat_id intValue]) {
                [sphBubbledata replaceObjectAtIndex:i withObject:message];
                exist =YES;
                break;
            }
        }
        
    }
    if (!exist) {
        [sphBubbledata addObject:message];
    }
}

-(void)insertTopMediaBubbledata:(MessageSender)mediaType  mediaPath:(NSString*)mediaPath mtime:(NSString*)messageTime thumb:(NSString*)thumbUrl  downloadstatus:(NSString*)downloadstatus sendingStatus:(NSString*)sendingStatus msg_ID:(NSString*)msgID name:(NSString*)name owner_ID:(NSString*)owner_ID
{
    mediaPath = [mediaPath isKindOfClass:[NSString class]]?mediaPath:@"";
    Message *message = [[Message alloc] init];
    
    message.text = [mediaPath emo_emojiString];
    message.sender =  mediaType;
    message.date = [self convertDate:messageTime];
    message.imgAvatar =thumbUrl;
    message.name =name;
    message.chat_id = msgID;
    message.ownerID = owner_ID;
    BOOL exist =NO;
    for (int i = 0 ; i<sphBubbledata.count; i++) {
        Message *msg = sphBubbledata[i];
        if ([message.chat_id intValue]>0) {
            if ([msg.chat_id intValue] == [message.chat_id intValue]) {
                [sphBubbledata replaceObjectAtIndex:i withObject:message];
                exist =YES;
                break;
            }
        }
        
    }
    if (!exist) {
        [sphBubbledata insertObject:message atIndex:0];
    }
}

#pragma mark - CONVERTDATE
-(NSString*)convertDate:(NSString*)date
{
    NSDateFormatter *outputFormatterBis = [[NSDateFormatter alloc] init];
    
//    [outputFormatterBis setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
//    [outputFormatterBis setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [outputFormatterBis setTimeZone:[NSTimeZone systemTimeZone]];
    
    [outputFormatterBis setDateFormat: @"d MMM H:mm"];
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    return outputStrings;
}

-(NSDate*)convertDateToDate:(NSString*)date
{
    NSDateFormatter *outputFormatterBis = [[NSDateFormatter alloc] init];
    
//    [outputFormatterBis setLocale: [NSLocale localeWithLocaleIdentifier: @"fr_FR"]];
//    [outputFormatterBis setTimeZone: [NSTimeZone timeZoneWithName: @"Europe/Paris"]];
    [outputFormatterBis setTimeZone:[NSTimeZone systemTimeZone]];

    [outputFormatterBis setDateFormat: @"yyyy-MM-dd"];
    NSDateFormatter *inputFormatter = [[ASSharedTimeFormatter sharedFormatter] inputFormatter];
    NSString *loungDateStrings=[NSString stringWithFormat:@"%@",date];
    NSDate * inputDates = [ inputFormatter dateFromString:loungDateStrings ];
    NSString * outputStrings = [ outputFormatterBis stringFromDate:inputDates ];
    NSDate * returnDate = [ outputFormatterBis dateFromString:outputStrings ];
    
    return returnDate;
}

#pragma mark - TABLE DELEGATE

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Message *data = (Message*)[sphBubbledata objectAtIndex:indexPath.row];
    if(data.sender == MessageSenderMyself)
        return data.heigh+10;
    return data.heigh + 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Message *data = (Message*)[sphBubbledata objectAtIndex:indexPath.row];
    
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!cell)
    {
        cell = [[MessageCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSString *bg_chat=@"";
    NSString *arrow_chat=@"";
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            bg_chat=@"MD_group_bg_my_chat";
            arrow_chat = @"MD_group_bg_my_chat_arrow";
            
        }
            break;
        case ISGROUP:
        {
            bg_chat=@"MD_group_bg_my_chat";
            arrow_chat = @"MD_group_bg_my_chat_arrow";
        }
            break;
        case ISLOUNGE:
        {
            if (self.isLiveHunt) {
                bg_chat=@"MD_live_bg_my_chat";
                arrow_chat = @"MD_live_bg_my_chat_arrow";
                
            }
            else
            {
                bg_chat=@"MD_chasse_my_chat_bg";
                arrow_chat = @"MD_chasse_my_chat_bg_arrow";
            }
        }
            break;
        case ISDISCUSS:
        {
            bg_chat=@"MD_discussion_bg_my_chat";
            arrow_chat = @"MD_discussion_bg_my_chat_arrow";
        }
            break;
            
        default:
            break;
    }
    cell.bg_chat =bg_chat;
    cell.arrow_chat =arrow_chat;
    
    cell.message = data;
    
    cell.btnInfo.tag=indexPath.row;
    [cell.btnInfo addTarget:self action:@selector(userProfileFromTheirNameAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return sphBubbledata.count;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self deleteMsgAction:(int)indexPath.row];
}
- (void) userProfileFromTheirNameAction:(id)sender
{
    
    NSInteger tag = ((UIButton *)sender).tag;
    Message *data = (Message*)[sphBubbledata objectAtIndex:tag];
    if (data.sender == MessageSenderMyself) {
        return;
    }
    
    NSString *my_id=[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSInteger owner_id =[data.ownerID integerValue];
    if (my_id.integerValue==owner_id) {
        return;
    }
    
    WebServiceAPI* serviceAPI = [WebServiceAPI new];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        if ([response[@"user"] isKindOfClass: [NSDictionary class]]) {
            
            FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
            [friend setFriendDic: response[@"user"]];
            friend.expectTarget = ISMUR;
            [self pushVC:friend animate:YES expectTarget:ISMUR];
            
        }
    };
    //mld_
    [serviceAPI getFriendAction:data.ownerID];
}
-(void)deleteMsgAction:(int)index
{
    Message *data = (Message*)[sphBubbledata objectAtIndex:index];
    if (data.sender == MessageSenderSomeone) {
        return;
    }
    NSString *msg_id=data.chat_id;
    if([msg_id intValue]==0) return;
    [UIAlertView showWithTitle:str(strTitle_app)
                       message:str(strVouloirSupprimerCeMessage)
             cancelButtonTitle:str(strOui)
             otherButtonTitles:@[strNon]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == [alertView cancelButtonIndex]) {
                              
                              
                              [COMMON addLoading:self];
                              
                              WebServiceAPI *serverAPI =[WebServiceAPI new];
                              serverAPI.onComplete = ^(id response, int errCode)
                              {
                                  [COMMON removeProgressLoading];
                                  if (response[@"success"]) {
                                      
                                      //update db:
                                      
                                      //                                      if (![DiscustionsEntity checkExitsWith:strID messageId:[dic[messageID] intValue] expectTarget:self.expectTarget myID:myID]) {
                                      //                                      cusObj.disID = [NSNumber numberWithInt: [strID intValue]];
                                      //                                      cusObj.disMessageID = [NSNumber numberWithInt: [dic[messageID] intValue] ];
                                      
                                      int myID =[[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];
                                      
                                      NSPredicate *ind=  [NSPredicate predicateWithFormat: @"disID=%@ && disMessageID = %@ &&  disType =%d && disMyID =%d",strID ,msg_id,self.expectTarget, myID];
                                      
                                      DiscustionsEntity* objDel = [DiscustionsEntity MR_findFirstWithPredicate:ind];
                                      if (objDel) {
                                          [objDel MR_deleteEntity];
                                      }
                                      
                                      [sphBubbledata removeObjectAtIndex:index];
                                      [self.tableControl reloadData];
                                  }
                              };
                              
                              switch (self.expectTarget) {
                                      
                                  case ISGROUP:
                                  {
                                      [serverAPI fnDELETE_MESSAGE_GROUP_CHAT:msg_id];
                                  }
                                      break;
                                      
                                  case ISLOUNGE:
                                  {
                                      [serverAPI fnDELETE_MESSAGE_HUNT_CHAT:msg_id];
                                  }
                                      break;
                                      
                                  case ISDISCUSS:
                                  {
                                      [serverAPI fnDELETE_GENERAL_CHAT_MESSAGE:msg_id];
                                  }
                                      break;
                                      
                                      
                                  default:
                                      break;
                              }
                              
                          }
                      }];
}
#pragma mark SUB - NAV EVENT

-(void) onSubNavClick:(UIButton *)btn{
    
    [socket disconnect];
    
    switch (self.expectTarget)
    {
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISDISCUSS:
        {
            [self clickSubNav_Discuss: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
            
        default:
            [self gotoback];
            break;
    }
}

-(void)insertRowAtTop
{
    switch (self.expectTarget) {
        case ISHOME:
        {
        }
            break;
            
        case ISGROUP:
        {
            if (![COMMON isReachable]) {
                [self loadCacheWithID:[NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]]];
            }
            else
            {
                [self getGroupMessageOffsetAction:LOAD_PREVOUS];
            }
        }
            break;
            
        case ISLOUNGE:
        {
            if (![COMMON isReachable]) {
                [self loadCacheWithID:[NSString stringWithFormat:@"%@",[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"]]];
            }
            else
            {
                [self getChassOffsetMessage:LOAD_PREVOUS];
            }
        }
            break;
            
        case ISDISCUSS:
        {
            if (![COMMON isReachable]) {
                [self loadCacheWithID:self.myConversationID];
            }
            else
            {
                [self getMessageForConversation:self.myConversationID withstate:LOAD_PREVOUS];
            }
        }
            break;
            
        default:
        {
            [self stopRefreshControl];
            
        }
            break;
    }
}

- (void)insertRowAtBottom {
    ASLog(@"bottom.....");
    if (self.expectTarget== ISGROUP) {
        [self  getGroupMessageOffsetAction:LOAD_CURRENT];
    }
    else if (self.expectTarget == ISLOUNGE)
    {
        [self getChassOffsetMessage:LOAD_CURRENT];
    }
    else if(self.expectTarget == ISDISCUSS) {
        [self getMessageForConversation:self.myConversationID withstate:LOAD_CURRENT];
    }
    else
    {
        [self stopRefreshControl];
    }
}


#pragma mark - GET MSG

-(void) parserData:(NSArray*) tempmessageArray  withstate:(STATE_LOAD)state
{
    [self saveCacheWithIDWithArrData:tempmessageArray];
    
    if (tempmessageArray.count > 0)
    {
        if (state == LOAD_CURRENT) {
            [sphBubbledata removeAllObjects];
            [arrParticipants removeAllObjects];
        }
        
        for (NSDictionary*dic in tempmessageArray) {
            
            NSString *imgUrl = nil;
            
            int ownerID=0;
            NSString *strUserTag  = @"";
            
            
            
            if (dic[@"owner"][@"ownerId"] != nil) {
                ownerID=[dic[@"owner"][@"ownerId"] intValue];
                strUserTag =dic[@"owner"][@"usertag"];
                
                NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"self.id == %@", dic[@"owner"][@"ownerId"]];
                NSArray *theFilteredArray = [arrParticipants filteredArrayUsingPredicate:aPredicate];
                
                if ([theFilteredArray count]){
                }else{
                    
                    [ arrParticipants addObject:@{@"id":dic[@"owner"][@"ownerId"],
                                                  @"text":dic[@"owner"][@"fullname"],
                                                  @"usertag":dic[@"owner"][@"usertag"]
                                                  }];
                }
                
                
            }else{
                
                ownerID=[dic[@"owner"][@"id"] intValue];
                strUserTag =dic[@"owner"][@"usertag"];
                
                NSPredicate *aPredicate = [NSPredicate predicateWithFormat:@"self.id == %@", dic[@"owner"][@"ownerId"]];
                NSArray *theFilteredArray = [arrParticipants filteredArrayUsingPredicate:aPredicate];
                
                if ([theFilteredArray count]){
                }else{
                    [ arrParticipants addObject:@{@"id":dic[@"owner"][@"id"],
                                                  @"text":dic[@"owner"][@"fullname"],
                                                  @"usertag":dic[@"owner"][@"usertag"]
                                                  }];                }
            }
            
            /*
             [arrUsers addObject:@{@"id":dic[@"id"],
             @"text":dic[@"fullname"],
             @"usertag":dic[@"usertag"]
             }];
             */
            
            if (dic[@"owner"][@"profilepicture"] != nil) {
                imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
            }else{
                imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            }
            
            
            //            NSLog(@" [CHAT] %@", dic);
            
            //Me -- Other
            if ( ownerID == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                if (state == LOAD_MORE) {
                    [self adddMediaBubbledata:MessageSenderMyself mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[messageID] name:dic[@"owner"][@"fullname"] owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
                    
                }
                else
                {
                    [self insertTopMediaBubbledata:MessageSenderMyself mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[messageID] name:dic[@"owner"][@"fullname"] owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
                }
                
            }else
            {
                //Other
                if (state == LOAD_MORE) {
                    [self adddMediaBubbledata:MessageSenderSomeone mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: dic[messageID] name:dic[@"owner"][@"fullname"] owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
                    
                }
                else
                {
                    [self insertTopMediaBubbledata:MessageSenderSomeone mediaPath:dic[@"content"] mtime:dic[@"created"] thumb:imgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID:dic[messageID] name:dic[@"owner"][@"fullname"] owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
                }
            }
        }
        
        [self.tableControl reloadData];
        if ((state == LOAD_MORE) || (state == LOAD_CURRENT) ) {
            [self scrollToBottom];
        }
    }else{
        //        ==0
        if (state == LOAD_CURRENT) {
            [sphBubbledata removeAllObjects];
            [arrParticipants removeAllObjects];
            
            [DiscustionsEntity MR_deleteAllMatchingPredicate: [NSPredicate predicateWithFormat:@"disType=%d && disID=%@",self.expectTarget ,strID ]  ];
            
        }
        
    }
}

-(void) getMessageForConversation:(NSString*) strConversationID withstate:(STATE_LOAD)state
{
    NSString *strLoadedCount  =@"0";
    NSString *strLimit = @"0";
    switch (state) {
        case LOAD_PREVOUS:
        {
            strLoadedCount =[NSString stringWithFormat:@"%lu",(unsigned long)sphBubbledata.count];
            strLimit = @"20";
        }
            break;
        case LOAD_CURRENT:
        {
            strLoadedCount= @"0";
            strLimit = @"20";
        }
            break;
        case LOAD_MORE:
        {
            strLoadedCount= @"0";
            strLimit = @"1";
        }
            break;
        default:
            break;
    }
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnGET_CONVERSATIONS_MESSAGE:strConversationID limit:strLimit offset:strLoadedCount];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        if (!response) {
            return ;
        }
        if([response isKindOfClass:[NSDictionary class]])
        {
            if (response[@"notify"]) {
                statusSelence = [response[@"notify"] boolValue];
            }
            else
            {
                statusSelence = YES;
            }
            btnSilence.selected = statusSelence;
            [self cacheSilence:@{[NSString stringWithFormat:@"%@",self.myConversationID]: [NSNumber numberWithBool:statusSelence]}];
            [self parserData:[response objectForKey:@"messages"] withstate:state];
        }
    };
    
}
#pragma mark -group
-(void)getGroupMessageOffsetAction:(STATE_LOAD)state
{
    NSString *strLoadedCount  =@"0";
    NSString *strLimit = @"0";
    switch (state) {
        case LOAD_PREVOUS:
        {
            strLoadedCount =[NSString stringWithFormat:@"%lu",(unsigned long)sphBubbledata.count];
            strLimit = @"20";
        }
            break;
        case LOAD_CURRENT:
        {
            strLoadedCount= @"0";
            strLimit = @"20";
        }
            break;
        case LOAD_MORE:
        {
            strLoadedCount= @"0";
            strLimit = @"1";
        }
            break;
        default:
            break;
    }
    __weak typeof(self) wself = self;
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getMessageOffsetActionWithMykind:(self.expectTarget== ISLOUNGE? MYCHASSE: MYGROUP) mykind_id:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] limit:strLimit offset:strLoadedCount];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self stopRefreshControl];
        if([wself fnCheckResponse:response]) return;
        
        if (!response) {
            return ;
        }
        if([response isKindOfClass:[NSDictionary class]])
        {
            NSArray *tempmessageArray=[response objectForKey:@"messages"];
            
            if (tempmessageArray.count > 0)
            {
                [self parserData:[response objectForKey:@"messages"] withstate:state];
            }else{
                //refresh on top
                if (state == LOAD_CURRENT) {
                    [sphBubbledata removeAllObjects];
                    [arrParticipants removeAllObjects];
                    
                    [DiscustionsEntity MR_deleteAllMatchingPredicate: [NSPredicate predicateWithFormat:@"disType=%d && disID=%@",self.expectTarget ,strID ]  ];
                    
                }
            }
        }
    };
}

#pragma mark - chasse
- (void) getChassOffsetMessage:(STATE_LOAD)state
{
    NSString *strLoadedCount  =@"0";
    NSString *strLimit = @"0";
    switch (state) {
        case LOAD_PREVOUS:
        {
            strLoadedCount =[NSString stringWithFormat:@"%lu",(unsigned long)sphBubbledata.count];
            strLimit = @"20";
        }
            break;
        case LOAD_CURRENT:
        {
            strLoadedCount= @"0";
            strLimit = @"20";
        }
            break;
        case LOAD_MORE:
        {
            strLoadedCount= @"0";
            strLimit = @"1";
        }
            break;
        default:
            break;
    }
    __weak typeof(self) wself = self;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getLoungeMessageOffset:[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] limit:@"20" offset:strLoadedCount];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        [self stopRefreshControl];
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            
            NSArray *arr= (NSArray*)response;
            
            if (arr.count>0) {
            }
            return ;
        }
        
        if([response isKindOfClass:[NSDictionary class]])
        {
            NSArray *tempmessageArray=[response objectForKey:@"messages"];
            
            if (tempmessageArray.count > 0)
            {
                [self parserData:[response objectForKey:@"messages"] withstate:state];
            }else{
                //refresh on top
                if (state == LOAD_CURRENT) {
                    [sphBubbledata removeAllObjects];
                    [arrParticipants removeAllObjects];
                    
                    [DiscustionsEntity MR_deleteAllMatchingPredicate: [NSPredicate predicateWithFormat:@"disType=%d && disID=%@",self.expectTarget ,strID ]  ];
                    
                }
            }
        }
        
        [self.tableControl reloadData];
        
        if (sphBubbledata.count<=0 && self.expectTarget== ISLOUNGE && state == LOAD_CURRENT) {
            self.lbMessage.hidden=NO;
        }
        else
        {
            self.lbMessage.hidden=YES;
        }
    };
}

//#pragma mark - DELETE
//-(void)deleteMessageAction:(NSString*)message_id index:(NSInteger)index
//{
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//
//    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
//    if (self.expectTarget==ISGROUP) {
//        [serviceObj deleteMessageWithKind:MYGROUP mykind_id:message_id];
//    }
//    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
//        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//
//        if (!response ||[response isKindOfClass: [NSArray class] ]) {
//            return ;
//        }
//        if (response[@"success"]) {
//            [sphBubbledata removeObjectAtIndex:index];
//            [self.tableControl reloadData];
//            [self scrollToBottom];
//
//        }
//    };
//
//}

#pragma mark - POST MSG

/*
 {
 message =     {
 content = ga;
 conversation =         {
 id = 17;
 participants =             (
 {
 firstname = Tuyen;
 fullname = "Tuyen Dang Ngoc";
 id = 161;
 lastname = "Dang Ngoc";
 usertag = "tuyen-dang";
 }
 );
 updated = "2015-09-21T12:39:53+0200";
 };
 created = "2015-09-21T12:39:53+0200";
 messageId = 354;
 owner =         {
 firstname = Manh;
 fullname = "Manh ManhManh";
 lastname = ManhManh;
 ownerId = 160;
 profilepicture = "/uploads/users/images/thumb/3699cad9e4240bf3508bad7958dca8f1d0fcec34.jpeg";
 usertag = "manh-manhmanh";
 };
 updated = "2015-09-21T12:39:53+0200";
 userId = 160;
 };
 }
 
 */

-(void) fnCreateConversation
{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    
    NSDictionary *dicPost = @{@"message":@{@"content":_textView.text,@"pendingParticipants":self.arrFriendDiscussion }};
    
    
    [serviceObj fnPOST_CREATE_DISCUSSION:dicPost];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        
        //reset
        [self.textView setText:@""];
        
        [COMMON removeProgressLoading];
        [self stopRefreshControl];
        
        //List message
        if ([response isKindOfClass:[NSDictionary class]]) {
            if (response && [response[@"message"] isKindOfClass: [NSDictionary class]]) {
                self.IS_CREATE_SPECIAL = NO;
                self.myConversationID = response[@"message"][@"conversation"][@"id"];
                
                //Have conversation -> register socket
                [self connectDiscussion];
                
                [self getMessageForConversation:self.myConversationID withstate:LOAD_MORE];
            }
        }
    };
}

//separate function sending message chat


-(IBAction) resignTextView{
    //Post discussion maker
    //Create DISCUSSION with participals: at list 1.
    
    //
    if ([_textView.text length]<1) {
        
    }
    else
    {
        if (!_textView.text) {
            return;
        }
        
        //if from DISCUSSION ->
        switch (self.expectTarget)
        {
                
            case ISDISCUSS:
            {
                if (_IS_CREATE_SPECIAL) {
                    [self fnCreateConversation];
                    return;

                }
                
                NSString *trimmedString = [_textView.text stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSDate *date=[NSDate date];

                NSString *strMessage = trimmedString;
                
                if ([COMMON isReachable] == NO)
                {
                    [KSToastView ks_showToast: NSLocalizedString(kWarnChatMessage, @"") duration:2.0f completion: ^{
                    }];
                    
                }
                
                //push in cache
                
                NSMutableArray *tmpArr = [NSMutableArray new];
                
                for (NSDictionary*dicT in arrParticipants) {
                    [tmpArr addObject:dicT[@"id"]];
                }
                NSString * joinStr = [tmpArr componentsJoinedByString:@","];
                
                //empty checking...
                
                if ([COMMON isReachable]) {
                    [[ServiceHelper sharedInstance] postingDiscussionMessageONLINE:@{
                                                                                     @"idgroup": [NSString stringWithFormat:@"%d",[self.myConversationID intValue]],
                                                                                     @"message":strMessage,
                                                                                     @"date":date,
                                                                                     @"targetScreen": [NSNumber numberWithInt:self.expectTarget],
                                                                                     @"joinStr": joinStr
                                                                                     }];
                }else{
                    [[ServiceHelper sharedInstance] prepareSendingMessage:@{
                                                                            @"idgroup": [NSString stringWithFormat:@"%d",[self.myConversationID intValue]],
                                                                            @"message":strMessage,
                                                                            @"date":date,
                                                                            @"targetScreen": [NSNumber numberWithInt:self.expectTarget],
                                                                            @"joinStr": joinStr
                                                                            }];
                }
                
                

                
                
                
                
                //Display
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
                //                    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT+2"];
                //                    [formatter setTimeZone:gmt];
                
                NSString *timeView = [formatter stringFromDate:date];
                
                [self adddMediaBubbledata:MessageSenderMyself mediaPath:strMessage mtime:timeView thumb:@""  downloadstatus:@"" sendingStatus:kSent msg_ID: @"" name:@"My full name" owner_ID:[[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] stringValue]];
                
                [self.tableControl reloadData];
                [self scrollToBottom];
                
                //clean
                _textView.text=@"";
                constraintMessageHeight.constant =minHeight;
                
                return;
                
            }
                break;
                
            default: // CHAT GROUP/CHASSES
            {
                
                NSString *trimmedString = [_textView.text stringByTrimmingCharactersInSet:
                                           [NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                NSString *strMessage = trimmedString;
                
                if ([COMMON isReachable] == NO)
                {
                    [KSToastView ks_showToast: NSLocalizedString(kWarnChatMessage, @"") duration:2.0f completion: ^{
                    }];
                    
                }
                
                {

                    //unmicode convert.
                    NSString *valueUnicode = [strMessage emo_UnicodeEmojiString];
                    
                    NSDate *date=[NSDate date];
   
                    
                    
                    [[ServiceHelper sharedInstance] prepareSendingMessage:@{
                                                                            @"idgroup":  [[GroupEnterOBJ sharedInstance].dictionaryGroup[@"id"] stringValue],
                                                                            @"message":valueUnicode,
                                                                            @"date":date,
                                                                            @"targetScreen": [NSNumber numberWithInt:self.expectTarget],
                                                                            @"joinStr": @""
                                                                            }];

                    //Display
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
                    //                    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT+2"];
                    //                    [formatter setTimeZone:gmt];
                    
                    NSString *timeStr = [formatter stringFromDate:date];
                    
                    [self adddMediaBubbledata:MessageSenderMyself mediaPath:strMessage mtime:timeStr thumb:@""  downloadstatus:@"" sendingStatus:kSent msg_ID: @"" name:@"My full name" owner_ID: [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] stringValue]];
                    [self.tableControl reloadData];
                    [self scrollToBottom];
                    
                    //clean
                    _textView.text=@"";
                    constraintMessageHeight.constant =minHeight;
                    
                    if (sphBubbledata.count<=0 && self.expectTarget== ISLOUNGE) {
                        self.lbMessage.hidden=NO;
                    }
                    else
                    {
                        self.lbMessage.hidden=YES;
                    }
                    
                    return;
                }
                //ONLINE ->post directly
                
            }
                break;
        }
        
        _textView.text=@"";
        constraintMessageHeight.constant =minHeight;
    }
    
}

-(void) scrollToBottom
{
    [self.tableControl layoutIfNeeded];
    NSInteger count = [self tableView:self.tableControl numberOfRowsInSection:0];
    if (count > 0) {
        NSInteger lastPos = MAX(0, count-1);
        [self.tableControl scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:lastPos inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.tableControl scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:lastPos inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        });
        
    }
}

#pragma mark - keyboard
- (void)myNotificationMethod:(NSNotification*)notification
{
    [self.view addGestureRecognizer:tapGesture];
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    kbHeight = CGRectGetHeight(keyboardFrameBeginRect);
    if (self.showTabBottom) {
        kbHeight = kbHeight-44;
    }
    
    NSLog(@"kbHeight: %d", kbHeight);
    constraintMessageView.constant = kbHeight;
    //
    [self scrollToBottom];
    
}
- (void)myNotificationMethodHide:(NSNotification*)notification
{
    [self.view removeGestureRecognizer:tapGesture];
    
    constraintMessageView.constant = 0;
}
#pragma mark - delegate textfield
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //constraintMessageView.constant = kbHeight;
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //    constraintMessageView.constant = 0;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //    [self resignTextView];
    return YES;
}
#pragma mark - tap
- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture {
    [_textView resignFirstResponder];
}

#pragma mark ----
-(void)gotoNotifi_leaves
{
    [self loadCacheWithID:self.myConversationID];
    
    if(self.expectTarget == ISDISCUSS) {
        
        //        [self getMessageForConversation:self.myConversationID withstate:LOAD_CURRENT];
    }else if (self.expectTarget== ISGROUP) {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)   ];
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        if (arrTmp.count > 0) {
            
            for (NSDictionary*dic in arrTmp) {
                
                if ([dic[@"id"] intValue] == [self.myConversationID intValue]) {
                    
                    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                    [self moreRefreshControl];
                    [self connectGroup];
                    
                    break;
                }
            }
        }
    }
    else if (self.expectTarget == ISLOUNGE)
    {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE)   ];
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        if (arrTmp.count > 0) {
            
            for (NSDictionary*dic in arrTmp) {
                
                if ([dic[@"huntID"] intValue] == [self.myConversationID intValue]) {
                    
                    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic[@"hunt"];
                    [self moreRefreshControl];
                    [self connectChasse];
                    break;
                }
            }
        }
        
    }
}

-(void)addTextView
{
    _textView.placeholder =str(strMessage25);
    
    if (self.expectTarget == ISLOUNGE)
    {
        if (!allow_add_chat) {
            _textView.placeholder = str(strMessages_limites_aux_administrateurs);
            _viewFooter.userInteractionEnabled = NO;
            [imgSend setImage:[UIImage imageNamed:@"live_ic_send"]];
            btnSilence.backgroundColor = [UIColor lightGrayColor];
        }
        else
        {
            _viewFooter.userInteractionEnabled = YES;
            
            if (self.isLiveHunt) {
                [imgSend setImage:[[UIImage imageNamed:@"chasse_ic_sent_comment"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
                imgSend.tintColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                
                btnSilence.backgroundColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
            }
            else
            {
                imgSend.image = [UIImage imageNamed:@"chasse_ic_sent_comment"];
                btnSilence.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            }
        }
    }
    animateHeightChange=YES;
    animationDuration = 0.1f;
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self refreshHeight];
}

-(void)refreshHeight
{
    NSInteger newSizeH = [self measureHeight];
    if (newSizeH < minHeight || !_textView.hasText) {
        newSizeH = minHeight; //not smalles than minHeight
    }
    else if (maxHeight && newSizeH > maxHeight) {
        newSizeH = maxHeight; // not taller than maxHeight
    }
    if (_textView.frame.size.height != newSizeH)
    {
        if (newSizeH <= maxHeight)
        {
            [UIView animateWithDuration:animationDuration
                                  delay:0
                                options:(UIViewAnimationOptionAllowUserInteraction|
                                         UIViewAnimationOptionBeginFromCurrentState)
                             animations:^(void) {
                                 [self resizeTextView:newSizeH];
                             }
                             completion:^(BOOL finished) {
                                 [self resizeTextView:newSizeH];
                                 
                             }];
        }
    }
    
}

- (CGFloat)measureHeight
{
    return self.textView.contentSize.height;
}

-(void)resizeTextView:(NSInteger)newSizeH
{
    constraintMessageHeight.constant = newSizeH+detal;
}

#pragma mark - cache
-(void)saveCacheWithIDWithArrData:(NSArray*)arr
{
    int myID =[[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];
    NSArray *tempmessageArray = arr;
    
    if (tempmessageArray.count > 0 && messageID.length>0)
    {
        
        for (NSDictionary*dic in tempmessageArray) {
            NSString *imgUrl = nil;
            
            int ownerID=0;
            NSString *strUserTag  = @"";
            if (dic[@"owner"][@"ownerId"] != nil) {
                ownerID=[dic[@"owner"][@"ownerId"] intValue];
                strUserTag =dic[@"owner"][@"usertag"];
                
            }else{
                ownerID=[dic[@"owner"][@"id"] intValue];
                strUserTag =dic[@"owner"][@"usertag"];
            }
            
            if (dic[@"owner"][@"profilepicture"] != nil) {
                imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"profilepicture"]];
            }else{
                imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"owner"][@"photo"]];
            }
            
            //cache database
            if (![DiscustionsEntity checkExitsWith:strID messageId:[dic[messageID] intValue] expectTarget:self.expectTarget myID:myID]) {
                DiscustionsEntity*cusObj = nil;
                cusObj = [DiscustionsEntity MR_createEntity];
                cusObj.disID = [NSNumber numberWithInt: [strID intValue]];
                cusObj.disMessageID = [NSNumber numberWithInt: [dic[messageID] intValue] ];
                cusObj.disCreateTime = dic[@"created"];
                cusObj.disContent = [dic[@"content"] isKindOfClass:[NSString class]]?dic[@"content"]:@"";
                cusObj.disOwnerID=[NSNumber numberWithInt: ownerID ];
                cusObj.disFullName =dic[@"owner"][@"fullname"];
                cusObj.disImgUrl =imgUrl;
                cusObj.disUserTag =strUserTag;
                cusObj.disType = [NSNumber numberWithInt:self.expectTarget];
                cusObj.disMyID = [NSNumber numberWithInt:myID];
            }
            
        }
        
        
        //Refactor data, delete...
        NSArray *arrSort=  [DiscustionsEntity MR_findAllSortedBy:@"disCreateTime" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"disID=%d &&  disType =%d && disMyID =%d",[strID intValue],self.expectTarget,myID]];
        
        if (arrSort.count>MAX_MESSAGE) {
            DiscustionsEntity *first =arrSort[0];
            int countDate =0;
            for (DiscustionsEntity *cusObj in arrSort) {
                NSString *firt=[NSString stringWithFormat:@"%@",first.disCreateTime];
                NSString *second=[NSString stringWithFormat:@"%@",cusObj.disCreateTime];
                NSDate *d1 = [self convertDateToDate:firt];
                NSDate *d2 = [self convertDateToDate:second];
                
                if (countDate>=2) {
                    //xoa ban ghi
                    [DiscustionsEntity MR_deleteAllMatchingPredicate:[NSPredicate predicateWithFormat:@"disID=%d",[cusObj.disID intValue]]];
                }
                else
                {
                    if ([d2 compare: d1]==NSOrderedAscending)
                    {
                        countDate++;
                    }
                }
                /*
                 2015-12-03T05:40:30+01:00
                 2015-12-03T05:40:28+01:00
                 2015-12-03T05:40:26+01:00
                 2015-12-03T05:40:25+01:00
                 2015-12-03T05:40:23+01:00
                 2015-12-03T05:40:21+01:00
                 */
            }
        }
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        
        [localContext MR_saveToPersistentStoreAndWait];
    }
}

-(void)loadCacheWithID:(NSString*)mstrID
{
    if (self.expectTarget == ISDISCUSS) {
        [self loadCacheSilenceWithConversationID:mstrID];
    }
    int myID =[[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue];
    NSArray *arrDis=  [DiscustionsEntity MR_findAllSortedBy:@"disCreateTime" ascending:NO withPredicate:[NSPredicate predicateWithFormat:@"disID=%d &&  disType =%d && disMyID =%d",[mstrID intValue],self.expectTarget,myID]];
    
    if (arrDis.count>0) {
        [sphBubbledata removeAllObjects];
        [arrParticipants removeAllObjects];
        
        for (DiscustionsEntity *cusObj in arrDis) {
            
            int ownerID =[cusObj.disOwnerID intValue];
            NSString *strContent = cusObj.disContent;
            NSString *strCreate = cusObj.disCreateTime;
            NSString *strImgUrl = cusObj.disImgUrl;
            NSString *strFullName = cusObj.disFullName;
            NSString *strUserTag  = cusObj.disUserTag;
            NSString *strdisMessageID = [cusObj.disMessageID stringValue];
            
            NSArray *tmpArr = [arrParticipants copy];
            
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"self.id = %@", [NSString stringWithFormat:@"%d",ownerID]];
            
            
            NSArray *theFilteredArray = [tmpArr filteredArrayUsingPredicate:pred];
            
            if ([theFilteredArray count]){
            }else{
                [ arrParticipants addObject:@{@"id":[NSString stringWithFormat:@"%d",ownerID],
                                              @"text":strFullName,
                                              @"usertag":strUserTag
                                              }];
            }
            
            //Me -- Other
            if ( ownerID == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                
                [self insertTopMediaBubbledata:MessageSenderMyself mediaPath:strContent mtime:strCreate thumb:strImgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: strdisMessageID name:strFullName owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
                
            }else
            {
                //Other
                [self insertTopMediaBubbledata:MessageSenderSomeone mediaPath:strContent mtime:strCreate thumb:strImgUrl  downloadstatus:@"" sendingStatus:kSent msg_ID: strdisMessageID name:strFullName owner_ID:[NSString stringWithFormat:@"%d",ownerID]];
            }
        }
        [self.tableControl reloadData];
        [self scrollToBottom];
    }
    
}

-(void) doUpdateCacheChat:(NSNotification*)notif
{
    NSDictionary*dic = [notif object];
    [self saveCacheWithIDWithArrData:@[dic[@"message"]]];
}
//MARK: - SILENCE
-(IBAction)silenceAction:(id)sender
{
    if (statusSelence) {
        AlertVC *vc = [[AlertVC alloc] initWithTitle:nil message:str(strSilenceAlert) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
        
        [vc doBlock:^(NSInteger index, NSString *str) {
            if (index==0) {
                // NON
            }
            else if(index==1)
            {
                //OUI
                [self putSilence:self.myConversationID];
            }
        }];
        [vc showAlert];

    }
    else
    {
        [self putSilence:self.myConversationID];
    }
    
}
-(void)putSilence:(NSString*)discussionID
{
    if (discussionID) {
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
        [COMMON addLoading:self];

        WebServiceAPI *serverAPI =[WebServiceAPI new];
        serverAPI.onComplete = ^(id response, int errCode)
        {
            [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
            [COMMON removeProgressLoading];

            if (response) {
                if ([response[@"success"] boolValue]) {
                    statusSelence = !statusSelence;
                    btnSilence.selected = statusSelence;

                    [self cacheSilence:@{[NSString stringWithFormat:@"%@",discussionID]: [NSNumber numberWithBool:statusSelence]}];
                    
                }
            }
        };
        
        /*
         notify = 0 là không nhận được push
         notify = 1 là nhận được push
         không có trường push là nhận được push ạ
         */
        
        [serverAPI putGroupSmartphoneNotificationSetting:@{@"option": @"chat.new_message",
                                                           @"value": [NSNumber numberWithBool:!statusSelence],
                                                           @"group_id": discussionID
                                                           }];
    }
}
-(void)cacheSilence:(NSDictionary*)dicSilence
{
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_DISCUSSION_SILENCE_SAVE)   ];
    NSDictionary *dicTmp = [NSDictionary dictionaryWithContentsOfFile:strPath];
    NSMutableDictionary *mdicCache = [NSMutableDictionary new];
    if (mdicCache) {
        [mdicCache addEntriesFromDictionary:dicTmp];
    }
    //[@"conversationID":@"status silence"];
    if (dicSilence) {
        [mdicCache addEntriesFromDictionary:dicSilence];
    }
    // Write array
    [mdicCache writeToFile:strPath atomically:YES];

}
-(void)loadCacheSilenceWithConversationID:(NSString*)discussionID
{
    if (discussionID) {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],FILE_DISCUSSION_SILENCE_SAVE)   ];
        // Write array
        NSDictionary *dicSelence = [NSDictionary dictionaryWithContentsOfFile:strPath];
        if (dicSelence[discussionID]) {
            statusSelence = [dicSelence[discussionID] boolValue];
        }
        btnSilence.selected = statusSelence;
    }

}
@end
