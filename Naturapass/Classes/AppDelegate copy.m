//
//  AppDelegate.m
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import "AppDelegate.h"

#import "AppCommon.h"
#import "ServiceHelper.h"
#import "FileHelper.h"
#import "UserSettingsHelper.h"
#include "ErrorViewController.h"
#import "NSDate+Extensions.h"
#import "Define.h"
#import "UIAlertView+Blocks.h"
#import "AZNotification.h"
#import "AZNotificationView.h"
#import "ASLocationManager.h"
#import "HomeVC.h"
#import "MDLoginVC.h"
#import "CommentDetailVC.h"
#import "HNKGooglePlacesAutocompleteQuery.h"

#import "Group_Hunt_ListShare.h"
#import "iRate.h"
#import "FMDB.h"
#import "FileHelper.h"
#import "DatabaseManager.h"
#import "MapDataDownloader.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import <GoogleMaps/GoogleMaps.h>
#import "DatabaseManager.h"
#import "GroupEnterMurVC.h"
#import "Flurry.h"
#import "ChassesParticipeVC.h"
#import "UIImage+animatedGIF.h"

static int val_onTick = 5;
@interface AppDelegate () <CLLocationManagerDelegate>
{
    dispatch_queue_t backgroundQueue;
    NSOperationQueue * operationQueue;
    CLLocationManager *locationManager;
    UIApplication *app;
    NSTimer* timer;
    NSDictionary *tmpDicEventSpecial;
}

@end

static NSString *const kHNKDemoGooglePlacesAutocompleteApiKey = @"AIzaSyC21pAQ94efbsY_uNpUjej9PjmHJcCAsnk";

@implementation AppDelegate

- (void)initialize
{
    //overriding the default iRate strings
    [iRate sharedInstance].appStoreID = 883300083;
    [iRate sharedInstance].messageTitle =  @"Naturapass";
    [iRate sharedInstance].message = @"Merci de donner votre avis sur Naturapass sur Appstore.";
    [iRate sharedInstance].cancelButtonLabel = @"Je ne veux pas";
    [iRate sharedInstance].remindButtonLabel = @"Plus tard";
    [iRate sharedInstance].rateButtonLabel = @"Oui";
}


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    
    [locationManager stopUpdatingLocation];
    
    //    if ([COMMON isLoggedIn]) {
    //        if ([newLocation distanceFromLocation: oldLocation]>1) {
    //
    //            dispatch_async(backgroundQueue, ^(void) {
    //                CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //
    //                [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
    //                 {
    //                     if (error == nil && [placemarks count] > 0)
    //                     {
    //                         CLPlacemark *placemark = [placemarks lastObject];
    //                         NSString *poststrAddress=@"";
    //                         NSString *strCountry=@"";
    //
    //                         if(placemark.locality !=nil){
    //                             poststrAddress=placemark.locality;
    //                             strCountry=placemark.country;
    //
    //                         }
    //
    //                         NSString *strLat=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    //                         NSString *strLong=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    //                         NSString *strAltitude=[NSString stringWithFormat:@"%f",newLocation.altitude];
    //
    //                         NSDictionary*  locationInfo =@{geolocation:@{currentlatitude:strLat,currentlongitude:strLong,currentAltitude:strAltitude,myaddress:poststrAddress,countryName:strCountry}};
    //
    //                         WebServiceAPI *serviceAPI =[WebServiceAPI new];
    //                         [serviceAPI postUpdateUserGeolocationAction: locationInfo];
    //                         serviceAPI.onComplete =^(NSDictionary *response, int errCode)
    //                         {
    //                             ASLog(@"did update location ok");
    //                         };
    //                     }
    //                 }];
    //            });
    //
    //        }
    //    }
    
}
//run background task
-(void)runBackgroundTask: (int) time{
    //check if application is in background mode
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateBackground && [COMMON isLoggedIn]) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            timer = [NSTimer scheduledTimerWithTimeInterval:time target:self selector:@selector(startTrackingBg) userInfo:nil repeats:NO];
            [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
            [[NSRunLoop currentRunLoop] run];
        });
    }
    else
    {
        [self stopBackgroundTask];
    }
}


//starts with background task
-(void)startTrackingBg{
    //write background time remaining
    //    ASLog(@"backgroundTimeRemaining: %.0f", [[UIApplication sharedApplication] backgroundTimeRemaining]);
    //start updating location
    [locationManager startUpdatingLocation];
    [self runBackgroundTask:val_onTick];
}
-(void)stopBackgroundTask
{
    [timer invalidate];
    timer = nil;
}

-(void)appDidBecomeActiveNotif:(NSNotification*)notif
{
    //ggtt
    //check join special event?
//    [self doCheckSpecialEventSuggestion];
    
    //change locationManager status after time
    //    if (timer==nil) {
    //        [self runBackgroundTask:val_onTick];
    //    }
    
}
-(void)appWillResignActiveNotif:(NSNotification*)notif
{
    //change locationManager status after time
    //    if (timer==nil) {
    //        [self runBackgroundTask:val_onTick];
    //    }
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    [_db close];
    
    [socket close];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    [localContext MR_saveToPersistentStoreAndWait];
    
}


- (BOOL) application: (UIApplication *) application willFinishLaunchingWithOptions: (NSDictionary *) launchOptions
{
    // check & download interstice json data
    [self checkAndDownloadIntersticeJsonData];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //init param
    operationQueue = [NSOperationQueue new];
    backgroundQueue = dispatch_queue_create("com.naturapass.locationuserupdate", 0);
    
    //init location manager
    locationManager =[[CLLocationManager alloc] init];
    locationManager.delegate=self;
    //    [locationManager setDesiredAccuracy: kCLLocationAccuracyNearestTenMeters];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    if ([locationManager respondsToSelector: @selector(requestWhenInUseAuthorization)])
        [locationManager requestWhenInUseAuthorization];
    
    //create UIBackgroundTaskIdentifier and create tackground task, which starts after time
//    app = [UIApplication sharedApplication];
//    __block UIBackgroundTaskIdentifier bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
//        [app endBackgroundTask:bgTask];
//        bgTask = UIBackgroundTaskInvalid;
//    }];
    
    //change locationManager status after time
    //    [self runBackgroundTask:val_onTick];
    
    //
    [GMSServices provideAPIKey:GOOGLE_PLACE_API];
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession: FLURRY_API_KEY];
    
    //FB
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor whiteColor]];
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    //    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    [HNKGooglePlacesAutocompleteQuery setupSharedQueryWithAPIKey:kHNKDemoGooglePlacesAutocompleteApiKey];
    
    
    if ([[self window] respondsToSelector: @selector(setTintColor:)])
    {
        [[self window] setTintColor: COLOR_SALON_TAB_SELECT];
        
        [[UISwitch appearance] setOnTintColor: COLOR_SALON_TAB_SELECT];
        
        [[UIRefreshControl appearance] setTintColor: COLOR_SALON_TAB_SELECT];
    }
    
    if ([UITableView instancesRespondToSelector: @selector(setSeparatorInset:)])
    {
        [[UITableView appearance] setSeparatorInset: UIEdgeInsetsZero];
    }
    
    //User must enable in Setting...
    if ([application respondsToSelector: @selector(registerForRemoteNotifications)])
    {
        //ASLog(@"ios 8 push");
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes: UIUserNotificationTypeAlert | UIUserNotificationTypeSound
                                                                                 categories: nil];
        
        [[UIApplication sharedApplication] registerUserNotificationSettings: settings];
        
        [COMMON setPushAreEnabled: [[UIApplication sharedApplication] isRegisteredForRemoteNotifications]];
        //
        //		ASLog(@"can push: %@", [[UIApplication sharedApplication] isRegisteredForRemoteNotifications] ? @"YES" : @"NO");
        //
        //		ASLog(@"other push: %@", [COMMON pushAreEnabled] ? @"YES" : @"NO");
    }
    
    
    [[NSUserDefaults standardUserDefaults]setValue:@"GROUPS" forKey:@"GROUPSTYPES"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"DataNatura"];
    [self queueFMDatabase];
    //Init database
    
    if ([COMMON isLoggedIn]) {
        [self doLogin];
        
        if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey])
        {
            NSDictionary *userInfo = [launchOptions objectForKey: UIApplicationLaunchOptionsRemoteNotificationKey];
            [self handleRemoteNotification:application userInfo:userInfo];
        }
        else {
            [self showSplashView];
        }
    }else{
        //Display Login page
        [self gotoLogin];
    }
    [self.window makeKeyAndVisible];
    return YES;
}

-(void)gotoLogin
{
    Class kclass =[MDLoginVC class];
    MDLoginVC *vc =[[MDLoginVC alloc] initWithNibName:NSStringFromClass(kclass) bundle:nil];
    self.loginVC =(MDLoginVC*)vc;
    self.window.rootViewController =vc;
}

-(void)loadLoginView {
    //Offline -> can not logout.
    
    if ([COMMON isReachable]) {
        
        //Confirm
        
        [UIAlertView showWithTitle:@"DÉCONNEXION" message:@"Êtes-vous sûr de vouloir vous déconnecter de NaturaPass?"
                 cancelButtonTitle:@"ANNULER"
                 otherButtonTitles:@[@"VALIDER"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              
                              if (buttonIndex == 1) {
                                  [self deleteUserDeviceAction];
                                  [COMMON doLogout];
                                  MDLoginVC *loginVC = [[MDLoginVC alloc] initWithNibName:@"MDLoginVC" bundle:nil];
                                  self.window.rootViewController = loginVC;
                              }
                              
                          }];
        
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Naturapass"
                                                            message: NSLocalizedString(@"NETWORK", @"")
                                                           delegate: nil
                                                  cancelButtonTitle: @"OK"
                                                  otherButtonTitles: nil];
        
        [alertView show];
    }
}

-(void) forceLogout
{
    [COMMON doLogout];
    MDLoginVC *loginVC = [[MDLoginVC alloc] initWithNibName:@"MDLoginVC" bundle:nil];
    self.window.rootViewController = loginVC;
    
}

-(void)deleteUserDeviceAction{
    
    NSString *strDeviceToken= [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICETOKEN"];
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI deleteUserDeviceAction:@"ios" identifier:strDeviceToken];
    serviceAPI.onComplete= ^(id response, int errCode)
    {};
}

- (void) doLogin{
    
    if (![COMMON isReachable]) {
        return;
    }
    NSString    *strUser      = [COMMON keyChainUserID];
    NSString    *strPassword  = [COMMON keyChainPasswordID];
    NSString *strDeviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICETOKEN"];
    
    ASLog(@"token: %@", strDeviceToken);
    
    if (!strDeviceToken || ![strDeviceToken length])
        ASLog(@"NIL TOKEN");
    
    if(strUser != NULL && strPassword !=NULL)
    {
        
        WebServiceAPI *serviceAPI =[WebServiceAPI new];
        if (strDeviceToken)
        {
            [serviceAPI getRequest: REAL_LOGIN(strUser, strPassword, strDeviceToken)];
        }
        else
        {
            [serviceAPI getRequest: REAL_LOGIN_NO_PUSH(strUser, strPassword)];
        }
        
        serviceAPI.onComplete =^(id response, int errCode)
        {
            NSMutableDictionary *userDictionary = [response valueForKey:@"user"];
            if ([userDictionary isKindOfClass:[NSDictionary class]] && userDictionary != NULL) {
                NSString *user_id   = [userDictionary valueForKey:@"id"];
                NSString *user_sid  = [userDictionary valueForKey:@"sid"];
                if (user_id != NULL && user_sid != NULL) {
                    [[NSUserDefaults standardUserDefaults] setValue:user_id forKey:@"sender_id"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue: userDictionary[@"usertag"] forKey:@"user_tag"];
                    
                    [[NSUserDefaults standardUserDefaults] setValue:user_sid forKeyPath:@"PHPSESSID"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                else
                {
                }
            }
            else
            {
                //Offline???
                
            }
        };
        
    } else {
    }
}


-(void)showSplashView {
    
    [splashView removeFromSuperview];
    
    if ([self shouldShowSecondSplashGameFair]) {
//        [self showSecondSplashAds];
        [self showSecondSplashGameFair];
    } else {
        [self gotoApplication];
    }
}


- (void)hideSplashView
{
    [splashView removeFromSuperview];
    
    if ([self shouldShowSecondSplashGameFair]) {
//        [self showSecondSplashAds];
        [self showSecondSplashGameFair];

    } else {
        [self gotoApplication];
    }
}

- (void) gotoApplication
{    //change locationManager status after time
    //    if (timer==nil) {
    //        [self runBackgroundTask:val_onTick];
    //    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"WAKEUP"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    
    //Register Location Notify
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidBecomeActiveNotif:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillResignActiveNotif:) name:UIApplicationWillResignActiveNotification object:nil];
    //    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    // Initialize Reachability
    Reachability *reachability = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reachability.reachableBlock = ^(Reachability *reachability) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ServiceHelper sharedInstance] startSyncingPostingData];
        });
        
        NSLog(@"Network is reachable.");
    };
    
    reachability.unreachableBlock = ^(Reachability *reachability) {
        NSLog(@"Network is unreachable.");
        dispatch_async(dispatch_get_main_queue(), ^{
            [[ServiceHelper sharedInstance] resetStatus];
        });
    };
    
    // Start Monitoring
    [reachability startNotifier];
    
    [[UIApplication sharedApplication] setStatusBarHidden: NO];
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    
    [self.window setRootViewController:self.navigationController ];
    
    //Init socket listening notification
    socket = [[SocketIOClient alloc] initWithSocketURL:LINK_SOCKET opts:nil];
    
    
    [socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        
        NSString *userTag = [[NSUserDefaults standardUserDefaults] valueForKey: @"user_tag"];
        
        [socket emit:@"npevent-user/connected" withItems:@[      @{@"usertag":userTag }]];
        
        [socket emit:@"npevent-user:connected" withItems:@[      @{@"usertag":userTag }]];
    } ];
    
    [socket on:@"npevent-notification:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //lounge.join.invited
        //group.join.invited
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket on:@"npevent-invitation:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //    type = "user.friendship.asked";
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket on:@"npevent-chat-message:incoming" callback:^(NSArray* data, SocketAckEmitter* ack) {
        //reg user
        //lounge.join.invited
        //group.join.invited
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REQUEST_UPDATING object:nil];
        
        //        NSLog(@"%@",data);
        
    } ];
    
    [socket connect];
    
    [self fnDoThread1];
    
    //RATING
    [self initialize];
    
    //Get gamefaire event...
    
    [self doCheckSpecialEventSuggestion];
}

#define ID_EVENT_SPECIAL @"586"

//ggtt
-(void) doCheckSpecialEventSuggestion
{
    //join public event
    dispatch_async(backgroundQueue, ^(void) {
        //if user infor is nul or wating status => POST / PUT to join... then go to detail. to get mur detail.
        
        WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
        
        [LoungeJoinAction fnGET_HUNT_INFORMATION: ID_EVENT_SPECIAL];
        
        LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
        {
            
            //            [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
            //
            //            [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
            
            //fail....
            if (respone[@"hunt"]) {
                
                tmpDicEventSpecial = respone[@"hunt"];
                
                if ([respone[@"connected"] isKindOfClass: [NSDictionary class]]) {
                    NSInteger access= [respone[@"connected"][@"connected"] integerValue];
                    if (access == USER_WAITING_APPROVE) { //wait
                        //put access...
                        [self doAskToJoinEventSpecial: YES];
                        return;
                    }
                }
                
            }
            
            [self doAskToJoinEventSpecial:NO];
        };
        
    });
}

-(void) doAskToJoinEventSpecial :(BOOL) isPut
{
    
    [UIAlertView showWithTitle:@"NATURAPASS" message:@"Would you like to join GameFair event?"
             cancelButtonTitle:@"ANNULER"
             otherButtonTitles:@[@"VALIDER"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == 1) {
                              
                              if (isPut) {
                                  [self doJoinSpecialEventPut];
                              }else{
                                  [self doJoinSpecialEventPost];
                              }
                          }
                      }];
}

-(void) doJoinSpecialEventPut
{
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:ID_EVENT_SPECIAL];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
        
        if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
            return ;
        }
        NSInteger access= [respone[@"access"] integerValue];
        if (access==NSNotFound) {
            return ;
        }
        
        if (access == 2) {
            //jump
            [self enterChasesWall];
            
        }
    };
    
}

-(void) doJoinSpecialEventPost
{
    WebServiceAPI *LoungeJoinAction = [[WebServiceAPI alloc]init];
    [LoungeJoinAction postLoungeJoinAction:ID_EVENT_SPECIAL];
    LoungeJoinAction.onComplete =^(NSDictionary *respone, int code)
    {
        [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
        [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[ kSQL_hunt]];
        
        if ([respone[@"access"] isKindOfClass:[NSArray class]]) {
            return ;
        }
        NSInteger access= [respone[@"access"] integerValue];
        if (access==NSNotFound) {
            return ;
        }
        if (access == 2) {
            //jump
            [self enterChasesWall];

        }
    };
    
}

//response[@"lounge"]
-(void)enterChasesWall
{

    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
//    GroupEnterMurVC *vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
//    vc.expectTarget =ISLOUNGE;
//    vc.mParent = viewController1;
    
//    [[GroupEnterOBJ sharedInstance] resetParams];
//    [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
    
    ChassesParticipeVC  *vc=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
    vc.dicChassesItem = tmpDicEventSpecial;
    vc.isInvitionAttend =YES;
    [vc doCallback:^(NSString *strID) {
        
        GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = tmpDicEventSpecial;
        [vc pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
    }];

    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:NO completion:^ {
        ASLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

-(void) fnDoThread1
{
    //get small
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_favorite, kSQL_address, kSQL_hunt, kSQL_group, kSQL_breed, kSQL_type, kSQL_brand, kSQL_calibre]];
    [[MapDataDownloader sharedInstance] setCallBack:^(){
        //get big
        [self fnDoThread2];
    }];
}

-(void) fnDoThread2
{
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_publication, kSQL_shape]];
    [MapDataDownloader sharedInstance].CallBack = nil;
}
/*
 check tag versions: to update structure.
 finish...
 run sqlite tag.
 
 
 the process is :
 you can get for e.g all elements, but we need to start executing "versions" and not "sqlite" to update structure before adding or updating elements.
 So check if "versions" exist. if true, for each version element exec all sqlite. if no error insert version in tb_db_version table, and exec other version if we have.
 When all version are updated, exec all "sqlite"
 
 {
 sqlite =     (
 );
 versions =     (
 {
 sqlite =             (
 "ALTER TABLE `tb_shape` ADD `c_lat_center` INTEGER",
 "ALTER TABLE `tb_shape` ADD `c_lng_center` INTEGER",
 ""
 );
 version = 20160222122811;
 }
 );
 }
 
 */

//

-(void)gotoScreenWithTypeNotification:(NSString*)type IdNoti:(NSString*)IdNoti dicNotif:(NSDictionary*)dic;
{
    [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
    [[MapDataDownloader sharedInstance] fnGetSqliteForAllPointsInMap:@[kSQL_group, kSQL_hunt]];
    
    [[UIApplication sharedApplication] setStatusBarHidden: NO];
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    [viewController1  gotoScreenWithTypeNotification:type IdNoti:IdNoti dicNotif:dic];
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    
    self.window.rootViewController = self.navigationController;
    
}

//NSURL *url = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"gif"];
//self.dataImageView.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];


- (void) showSecondSplashGameFair
{
    if (secondSplashAdsView == nil) {
        secondSplashAdsView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        secondSplashAdsView.contentMode = UIViewContentModeScaleAspectFit;
        [[self window] addSubview:secondSplashAdsView];
    }
    
    if (btnClose == nil) {
        // close button
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenBound.size.width;
        btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClose setFrame:CGRectMake(screenWidth - 75, 20, 55, 26)];
        [btnClose setAlpha:1.0f];
        [btnClose addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [btnClose setTitleColor:[UIColor colorWithWhite:0.9 alpha:0.9] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnClose setTitle:NSLocalizedString(@"Close", @"") forState:UIControlStateNormal];
        [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:11.0f]];
        [btnClose setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
        btnClose.layer.cornerRadius = 3.0f;
        btnClose.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.9].CGColor;
        btnClose.layer.borderWidth = 1.0f;
        btnClose.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [[self window] addSubview:btnClose];
    }
    
//    secondSplashAdsView.image = [UIImage animatedImageNamed:@"Interstice-Game-Fair-Animee" duration:1];

    UIImage *imvSplashAds = [UIImage imageWithContentsOfFile:[FileHelper pathForSecondSplashFile]];
    secondSplashAdsView.image = imvSplashAds;

    
    // update display time
    [UserSettingsHelper setIntersticeDisplayTime:[NSDate currentGMTZeroTime]];
    
//user must click button close
}


- (void) showSecondSplashAds
{
    if (secondSplashAdsView == nil) {
        secondSplashAdsView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        secondSplashAdsView.contentMode = UIViewContentModeScaleAspectFit;
        [[self window] addSubview:secondSplashAdsView];
    }
    
    if (btnClose == nil) {
        // close button
        CGRect screenBound = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenBound.size.width;
        btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnClose setFrame:CGRectMake(screenWidth - 75, 20, 55, 26)];
        [btnClose setAlpha:1.0f];
        [btnClose addTarget:self action:@selector(closeButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [btnClose setTitleColor:[UIColor colorWithWhite:0.9 alpha:0.9] forState:UIControlStateNormal|UIControlStateHighlighted];
        [btnClose setTitle:NSLocalizedString(@"Close", @"") forState:UIControlStateNormal];
        [btnClose.titleLabel setFont:[UIFont boldSystemFontOfSize:11.0f]];
        [btnClose setBackgroundColor:[UIColor colorWithWhite:0.1 alpha:0.5]];
        btnClose.layer.cornerRadius = 3.0f;
        btnClose.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:0.9].CGColor;
        btnClose.layer.borderWidth = 1.0f;
        btnClose.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        [[self window] addSubview:btnClose];
    }
    
    UIImage *imvSplashAds = [UIImage imageWithContentsOfFile:[FileHelper pathForSecondSplashFile]];
    secondSplashAdsView.image = imvSplashAds;
    
    // update display time
    [UserSettingsHelper setIntersticeDisplayTime:[NSDate currentGMTZeroTime]];
    
    [self performSelector: @selector(hideSecondSplashAds) withObject:nil afterDelay:3];
}


#pragma mark - ERROR
static BOOL isError503Showing;

-(void)showErrorViewController
{
    if (isError503Showing) {
        return;
    }
    isError503Showing = YES;
    
    ErrorViewController *errorVC=[[ErrorViewController alloc]initWithNibName:@"ErrorViewController" bundle:nil];
    errorVC.view.tag=10000;
    [errorVC.gotoHome addTarget:self action:@selector(hideErrorViewController:) forControlEvents:UIControlEventTouchUpInside];
    
    errorVC.view.frame = self.window.frame;
    
    [[self window]addSubview:errorVC.view];
    
    UIView *v = errorVC.view;
    
    [self.window addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"V:|-0-[v]-0-|"
                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(v)]];
    
    [self.window addConstraints:[NSLayoutConstraint
                                 constraintsWithVisualFormat:@"H:|-0-[v]-0-|"
                                 options:NSLayoutFormatDirectionLeadingToTrailing
                                 metrics:nil
                                 views:NSDictionaryOfVariableBindings(v)]];
    
}
-(IBAction)hideErrorViewController:(id)sender
{
    isError503Showing = NO;
    
    UIView *view= [[self window] viewWithTag:10000];
    [view removeFromSuperview];
}

#pragma mark - SPLASH ADS
- (void) hideSecondSplashAds
{
    if (btnClose) {
        [btnClose removeFromSuperview];
        btnClose = nil;
    }
    if (secondSplashAdsView) {
        [secondSplashAdsView removeFromSuperview];
        secondSplashAdsView = nil;
        
        [self gotoApplication];
    }
}
- (void) closeButtonPressed:(id)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideSecondSplashAds) object:nil];
    [self performSelector: @selector(hideSecondSplashAds) withObject:nil afterDelay:0.2];
}

- (BOOL) shouldShowSecondSplashGameFair
{
    return YES;
    
    
//    
//    BOOL isShow = NO;
//    NSDictionary *dictionary = [UserSettingsHelper getIntersticeJsonData];
//    if (dictionary) {
//        NSNumber *enable = [dictionary objectForKey:@"enable"];
//        if ([enable boolValue] && [FileHelper isfileExisting:[FileHelper pathForSecondSplashFile]]) {
//            isShow = YES;
//        }
//    }
//    
//    if (isShow) {
//        NSString *lastLoad = [UserSettingsHelper getIntersticeDisplayTime];
//        if (lastLoad) {
//            NSDate *lastDate = [NSDate GMTZeroDateForString:lastLoad];
//            int days = [NSDate numberOfDaysFromDate:lastDate ToDate:[NSDate date]];
//            if (days >= 7) {
//                isShow = YES;
//            } else {
//                isShow = NO;
//            }
//        } else {
//            isShow = YES;
//        }
//    }
//    
//    return isShow;

}
- (BOOL) shouldShowSecondSplash
{
    BOOL isShow = NO;
    NSDictionary *dictionary = [UserSettingsHelper getIntersticeJsonData];
    if (dictionary) {
        NSNumber *enable = [dictionary objectForKey:@"enable"];
        if ([enable boolValue] && [FileHelper isfileExisting:[FileHelper pathForSecondSplashFile]]) {
            isShow = YES;
        }
    }
    
    if (isShow) {
        NSString *lastLoad = [UserSettingsHelper getIntersticeDisplayTime];
        if (lastLoad) {
            NSDate *lastDate = [NSDate GMTZeroDateForString:lastLoad];
            int days = [NSDate numberOfDaysFromDate:lastDate ToDate:[NSDate date]];
            if (days >= 7) {
                isShow = YES;
            } else {
                isShow = NO;
            }
        } else {
            isShow = YES;
        }
    }
    
    return isShow;
}

- (void) checkAndDownloadIntersticeJsonData
{
    // check and load story every 1 day
    BOOL isLoad = YES;
    NSString *lastLoad = [UserSettingsHelper getFetchIntersticeTime];
    if (lastLoad) {
        NSDate *lastDate = [NSDate GMTZeroDateForString:lastLoad];
        int days = [NSDate numberOfDaysFromDate:lastDate ToDate:[NSDate date]];
        if (days < 1) {
            isLoad = NO;
        }
    }
    if (isLoad) {
        [[ServiceHelper sharedInstance] getIntersticeAction];
    }
}

-(void)applicationDidBecomeActive:(UIApplication *)application
{
    if ([COMMON isReachable]) {
        //        [[ServiceHelper sharedInstance] startSyncingPostingData];
    }
    
}

#pragma mark - REGISTER PUSH
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    
    //NSLog(@"token: %@", devToken);
    
    NSString *deviceToken = [[[[devToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
    ASLog(@"devicetoken_app = %@",deviceToken);
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"DEVICETOKEN"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [COMMON setPushAreEnabled: YES];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    
    ASLog(@"did fail to register: %@", error);
    
    [COMMON setPushAreEnabled: NO];
}

- (void)application: (UIApplication *) application didReceiveRemoteNotification: (NSDictionary *) userInfo
{
    ASLog(@"did receive push: %@", userInfo);
    
    
    [self ProcessingNotification_REMOTE:userInfo withApp:application ];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSDictionary*userInfo = notification.userInfo;
    
    if (application.applicationState != UIApplicationStateActive)
    {
        //Wakeup app
        [self processRemoteNotificationWithUserInfo: userInfo];
        return;
    }
}

-(void) ProcessingNotification_REMOTE :(NSDictionary*) userInfoTmp withApp:(UIApplication *)application
{
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:userInfoTmp];
    
    //crash cause this???
    //    NSString* strNotificationID = [userInfo[@"notification_id"] stringValue];
    //
    //    //default notificationID
    //    if (!strNotificationID || strNotificationID == (id)[NSNull null] || strNotificationID.length == 0 )
    //    {
    //        [userInfo setValue: @"10000" forKey:@"notification_id"];
    //    }
    //
    // if app inactive then when select banner -> link processremote
    
    
    if (application.applicationState != UIApplicationStateActive)
    {
        
        //Wakeup app
        [self processRemoteNotificationWithUserInfo: userInfo];
        return;
    }
    else
    {
        [AZNotification showNotificationWithTitle:userInfo[@"aps"][@"alert"] controller:self.window.rootViewController notificationType:AZNotificationTypeMessage];
        
        //make crash!!!!
        [self cacheLocalNotify:userInfo];
    }
    
}

-(void) cacheLocalNotify :(NSDictionary*)userInfo
{
    NSMutableDictionary*mutDic = [userInfo mutableCopy];
    
    //continue
    //Local notification
    if ([userInfo[@"notification_id"] isKindOfClass: [NSNull class]]) {
        [mutDic setValue:@"" forKey:@"notification_id"];
    }
    
    if ([userInfo[@"object_id"] isKindOfClass: [NSNull class]]) {
        [mutDic setValue:@"" forKey:@"object_id"];
    }
    
    
    NSString *strAlert =mutDic[@"aps"][@"alert"];
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    
    localNotification.userInfo = mutDic;
    
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    
    localNotification.alertBody = strAlert;
    
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:2];
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void) processRemoteNotificationWithUserInfo: (NSDictionary *) userInfo
{
    if ([COMMON isLoggedIn])
    {
        
        NSLog(@"[GGTT] handleRemoteNotification");
        
        [self handleRemoteNotification: nil userInfo: userInfo];
    }
}

-(void) handleRemoteNotification:(UIApplication *)application userInfo:(NSDictionary *)userInfo {
    /*
     {
     aps =     {
     alert = "Dua Dua : 138";
     badge = 1;
     sound = default;
     };
     element = message;
     "notification_id" = "<null>";
     "object_id" = 93;
     projectID = 541284815128;
     type = "chat.new_message";
     }
     */
    ASLog(@"userinfo: %@", userInfo);
    if ([COMMON isLoggedIn]) {
        [self putNotification:userInfo];
        [self gotoScreenWithTypeNotification:userInfo[@"type"] IdNoti:userInfo[@"object_id"] dicNotif:userInfo];
        
    }
}
- (void)putNotification:(NSDictionary*)userInfo{
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        //reset list
    };
    NSString *strNotificationID = @"";
    
    if ([userInfo[@"notification_id"] isKindOfClass: [NSString class] ]) {
        strNotificationID =userInfo[@"notification_id"];
        [serviceObj putUserReadNotificationAction:strNotificationID];
        
    }else if ([userInfo[@"notification_id"] isKindOfClass: [NSNumber class] ]) {
        
        strNotificationID = [userInfo[@"notification_id"]  stringValue];
        [serviceObj putUserReadNotificationAction:strNotificationID];
    }
}

#pragma mark -get count notification

-(void) showErrMsg :(NSString*)strMsg
{
    [AZNotification showNotificationWithTitle:strMsg controller:self.window.rootViewController notificationType:AZNotificationTypeError];
    
}
-(void) showPostMsgSuccess :(NSString*)strMsg
{
    [AZNotification showNotificationWithTitle:strMsg controller:self.window.rootViewController notificationType:AZNotificationTypeMessage];
    
}

//application switchs back from background
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [socket connect];
    application.applicationIconBadgeNumber = 0;
    
}
//starts when application switchs into backghround
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [socket close];
}

#pragma mark - FB
//pdf
-(void)handleDocumentOpenURL:(NSURL*)url
{
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *filename = [[url path] lastPathComponent];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,filename];
        [urlData writeToFile:filePath atomically:YES];
    }
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    NSLog(@"%@",[url scheme]);
    
    if ([[url scheme] isEqualToString:@"fb746119168733563"]) {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
    }
    else if([[url scheme] isEqualToString:@"naturapass"])
    {
        //goto hunt
        if ([COMMON isLoggedIn]) {
            [self doLogin];
            //goto hunt
            [self enterChasesWallFromWeb];
            
        }else{
            //Display Login page
            [self gotoLogin];
        }
        return YES;
    }
    else
    {
        [self handleDocumentOpenURL:url];
        return YES;
    }
}

-(void)enterChasesWallFromWeb
{
    
    HomeVC *viewController1 = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    
    GroupEnterMurVC *vc = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
    vc.expectTarget =ISLOUNGE;
    vc.mParent = viewController1;
    
    //    [[GroupEnterOBJ sharedInstance] resetParams];
    //    [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
    
    self.navigationController = [[MainNavigationController alloc] initWithRootViewController:viewController1];
    [(MainNavigationController *)self.navigationController pushViewController:vc animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
    [self.window setRootViewController:self.navigationController ];
}

#pragma mark
-(void) queueFMDatabase
{
    //if not exist table....
    //create
    //        INSERT INTO `tb_shape`(`c_id`,`c_owner_id`,`c_type`,`c_title`,`c_description`,`c_sharing`,`c_groups`,`c_hunts`,`c_friend`,`c_member`,`c_data`,`c_updated`,`c_nw_lat`,`c_nw_lng`,`c_se_lat`,`c_se_lng`,`c_user_id`)
    
    //TABLE SHAPE
    
    
    
    //c_allow_add using to tick on group Edit...show shareview in creating publication
    //c_allow_show using to tick on group Edit
    
    NSArray *arrSqlites = @[@"CREATE TABLE IF NOT EXISTS  `tb_shape` (`c_id`    INTEGER,\
                            `c_owner_id`    INTEGER,\
                            `c_type`    TEXT,\
                            `c_title`    TEXT,\
                            `c_description`    TEXT,\
                            `c_sharing`    INTEGER,\
                            `c_groups`    TEXT,\
                            `c_hunts`    TEXT,\
                            `c_friend`    INTEGER,\
                            `c_member`    INTEGER,\
                            `c_data`   TEXT,\
                            `c_updated`    INTEGER,\
                            `c_ne_lat`    REAL,\
                            `c_ne_lng`    REAL,\
                            `c_sw_lat`    REAL,\
                            `c_sw_lng`    REAL,\
                            `c_user_id`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id));",
                            
                            //table group
                            @"CREATE TABLE IF NOT EXISTS   `tb_group` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_description`    TEXT,\
                            `c_access`    INTEGER,\
                            `c_admin`    INTEGER,\
                            `c_nb_member`    INTEGER,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            `c_allow_add`    INTEGER,\
                            `c_allow_show`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE  IF NOT EXISTS  `tb_hunt` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_description`    TEXT,\
                            `c_access`    INTEGER,\
                            `c_admin`    INTEGER,\
                            `c_nb_participant`    INTEGER,\
                            `c_start_date`    INTEGER,\
                            `c_end_date`    INTEGER,\
                            `c_meeting_address`    TEXT,\
                            `c_meeting_lat`    REAL,\
                            `c_meeting_lon`    REAL,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE  IF NOT EXISTS `tb_favorite` (\
                            `c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_legend`    TEXT,\
                            `c_color`    INTEGER,\
                            `c_category`    INTEGER,\
                            `c_tree`    TEXT,\
                            `c_card`    INTEGER,\
                            `c_animal`    INTEGER,\
                            `c_specific`    INTEGER,\
                            `c_sharing`    INTEGER,\
                            `c_groups`    TEXT,\
                            `c_hunts`    TEXT,\
                            `c_attchments`    TEXT,\
                            `c_user_id`    INTEGER,\
                            `c_updated`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_favorite_address` (\
                            `c_id`    INTEGER,\
                            `c_title`    TEXT,\
                            `c_favorite`    TEXT,\
                            `c_lat`    REAL,\
                            `c_lon`    REAL,\
                            `c_user_id`    INTEGER,\
                            PRIMARY KEY(c_id,c_user_id)\
                            );",
                            
                            //table tb_db_version
                            @"CREATE TABLE IF NOT EXISTS  `tb_db_version` (`version`    INTEGER);",
                            //c_address
                            //c_cp + c_city
                            /*
                             
                             "INSERT INTO `tb_distributor`(`c_id`,`c_name`,`c_address`,`c_cp,`c_city`,`c_tel`,`c_email`,`c_lat`,`c_lon`,`c_brands`,`c_updated`,`c_logo`) VALUES ('641','ACL ARMURERIE','RUE RABELAIS LA MELIGRETTE','37220','CROUZILLES','02 47 58 30 60','acl-loches@orange.fr','47.1247564','0.4531836','[{\"name\":\"Browning\",\"partner\":1,\"logo\":\"\\/uploads\\/brands\\/images\\/resize\\/bbcf161328cb1fb7768434fee8936b350e67bfe2.jpeg\"}]','1433238042','');",
                             
                             */
                            @"CREATE TABLE IF NOT EXISTS  `tb_distributor` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            `c_address`    TEXT,\
                            `c_cp`    TEXT,\
                            `c_city`    TEXT,\
                            `c_tel`    TEXT,\
                            `c_email`    TEXT,\
                            `c_lat`    REAL,\
                            `c_lon`    REAL,\
                            `c_brands`    TEXT,\
                            `c_updated`    INTEGER,\
                            `c_logo`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_dog_breed` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_dog_type` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_weapon_brand` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );",
                            
                            @"CREATE TABLE IF NOT EXISTS `tb_weapon_calibre` (`c_id`    INTEGER,\
                            `c_name`    TEXT,\
                            PRIMARY KEY(c_id)\
                            );"
                            ];
    //table distributor
    for (NSString*strQuerry in arrSqlites) {
        @try
        {
            [[[DatabaseManager sharedManager] myDB] beginTransaction];
            [[[DatabaseManager sharedManager] myDB] executeUpdate:strQuerry];
            [[[DatabaseManager sharedManager] myDB] commit];
        }
        @catch (NSException *exception)
        {
            [[[DatabaseManager sharedManager] myDB] rollback];
        }
        
    }
    
    /*ADD 1 columns  c_default */
    
    
    if (![[[DatabaseManager sharedManager] myDB] columnExists:@"c_default" inTableWithName:@"tb_favorite"])
    {
        arrSqlites = @[@"ALTER TABLE tb_favorite ADD COLUMN c_default INTEGER"];
        
        @try
        {
            [[[DatabaseManager sharedManager] myDB] beginTransaction];
            for (NSString*strQuerry in arrSqlites) {
                
                [[[DatabaseManager sharedManager] myDB] executeUpdate:strQuerry];
            }
            [[[DatabaseManager sharedManager] myDB] commit];
        }
        @catch (NSException *exception)
        {
            [[[DatabaseManager sharedManager] myDB] rollback];
        }
        // I tried to set the result of this query in "BOOL Success" and print the results and I got YES
    }
    
    /*ADD 2 columns  c_lat_center + c_lng_center */
    
    if (![[[DatabaseManager sharedManager] myDB] columnExists:@"c_lat_center" inTableWithName:@"tb_shape"])
    {
        arrSqlites = @[@"ALTER TABLE tb_shape ADD COLUMN c_lat_center REAL",
                       @"ALTER TABLE tb_shape ADD COLUMN c_lng_center REAL" ];
        
        @try
        {
            [[[DatabaseManager sharedManager] myDB] beginTransaction];
            for (NSString*strQuerry in arrSqlites) {
                
                [[[DatabaseManager sharedManager] myDB] executeUpdate:strQuerry];
            }
            [[[DatabaseManager sharedManager] myDB] commit];
        }
        @catch (NSException *exception)
        {
            [[[DatabaseManager sharedManager] myDB] rollback];
        }
        // I tried to set the result of this query in "BOOL Success" and print the results and I got YES
    }
    
}

@end
