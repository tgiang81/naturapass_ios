//
//  GroupCreate_Step9.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisFelicitations.h"
@interface AmisFelicitations ()
{
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescriptionScreen;
    __weak IBOutlet UIButton *btnSuivant;

}
@end

@implementation AmisFelicitations

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strMessage14);
    lblDescriptionScreen.text = str(strLenvoi_de_vos_invitations);
    [btnSuivant setTitle:str(strTermine) forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
}

-(IBAction)onNext:(id)sender
{
    
    NSArray * controllerArray = [[self navigationController] viewControllers];
    
    for (int i=0; i < controllerArray.count; i++) {
        if ([controllerArray[i] isKindOfClass: [self.mParent class]]) {
            
            [self.navigationController popToViewController:self.mParent animated:YES];
            
            return;
        }
    }
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
