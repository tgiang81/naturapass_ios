//
//  CellKind2.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CellKind9.h"
#import "Config.h"
#import "CommonHelper.h"
#import "Define.h"

@implementation CellKind9

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [[CommonHelper sharedInstance] setRoundedView:self.imageIcon toDiameter:self.imageIcon.frame.size.width/2];

    
    [self.label1 setTextColor:[UIColor blackColor]];
    [self.label1 setFont:FONT_HELVETICANEUE(15)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    self.label1.preferredMaxLayoutWidth = CGRectGetWidth(self.label1.frame);
}

@end
