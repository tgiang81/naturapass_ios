//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
#import "MDCheckBox.h"
#import "AppCommon.h"

@interface TypeCell41: UITableViewCell
//view
@property (weak, nonatomic) IBOutlet UIView * view1;

//image
@property (nonatomic,retain) IBOutlet UIImageView       *img1;

//label
@property (nonatomic,retain) IBOutlet UILabel           *label1;
@property (nonatomic,retain) IBOutlet UILabel           *label2;
//button
@property (nonatomic,retain) IBOutlet MDCheckBox           *checkbox;
//funtion
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraint_control_width;

-(void)fnSettingCell:(int)type;
@end
