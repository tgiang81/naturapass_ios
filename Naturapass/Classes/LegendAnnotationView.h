//
//  LegendAnnotationView.h
//  Naturapass
//
//  Created by Giang on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "NewMapAnnotationView.h"
#import "LegendAnnotation.h"

@interface LegendAnnotationView : MKAnnotationView
{
    UIView *pv;

}
@property (nonatomic, strong) LegendAnnotation *mapViewAnnotation;

- (id)initWithAnnotation:(id<MKAnnotation>)annotation;

@end
