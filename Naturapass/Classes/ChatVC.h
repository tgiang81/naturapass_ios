//
//  ChassesChatVC.h
//  Naturapass
//
//  Created by Giang on 8/17/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "BaseVC.h"

@interface ChatVC : BaseVC <UITextFieldDelegate,UINavigationControllerDelegate>
{
    NSMutableArray *sphBubbledata;

}
//@property (weak, nonatomic) IBOutlet UITableView *sphChatTable;

@property (strong, nonatomic) NSArray *arrFriendDiscussion;

@property (strong, nonatomic) NSString*myConversationID;
@property (strong, nonatomic) NSDictionary*dicConversation;
@property (assign, nonatomic) BOOL IS_CREATE_SPECIAL;

@end
