//
//  MDMarkersLegend.h
//  Naturapass
//
//  Created by Manh on 3/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^callBackMarkers) (UIImage *image);
@interface MDMarkersLegend : UIView
@property (nonatomic,assign) BOOL isResize;
@property (nonatomic,strong) NSDictionary *dicMarker;
@property (nonatomic, copy) callBackMarkers CallBackMarkers;
-(instancetype)initWithDictionary:(NSDictionary*)dicMarker withResize:(BOOL)resize;
- (void)doSetCalloutView;
@end
