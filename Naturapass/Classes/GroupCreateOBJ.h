//
//  GroupCreateOBJ.h
//  Naturapass
//
//  Created by Giang on 8/7/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Define.h"
#import "BaseVC.h"
@interface GroupCreateOBJ : NSObject
+ (GroupCreateOBJ *) sharedInstance;


@property (nonatomic, strong) NSString *group_id;
@property (nonatomic, strong) NSData *imgData;
@property (nonatomic, strong) NSString *strName;
@property (nonatomic, strong) NSString *strComment;
@property (nonatomic, assign) ACCESS_KIND accessKind;
@property (nonatomic, assign) BOOL     isModifi;

@property (nonatomic, strong) NSArray *listHuntAdmin;

@property (nonatomic, assign) BOOL     allow_add;
@property (nonatomic, assign) BOOL     allow_show;

@property (nonatomic, assign) BOOL     allow_chat_add;
@property (nonatomic, assign) BOOL     allow_chat_show;

-(void) resetParams;
-(void)modifiGroupWithVC:(UIViewController*)vc;
@end
