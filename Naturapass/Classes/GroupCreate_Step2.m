//
//  GroupCreate_Step2.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreate_Step2.h"
#import "GroupCreate_Step9.h"
#import "GroupCreate_Step3.h"

#import "GroupCreateOBJ.h"
#import "MapDataDownloader.h"
#import "Define.h"
#import "GroupCreateCell_Step2.h"
#import "UIAlertView+Blocks.h"
#import "GroupMesVC.h"
static NSString *GroupCreateCellStep2 =@"GroupCreateCell_Step2";
static NSString *GroupesCreateCellID =@"GroupesCreateCellID";

@interface GroupCreate_Step2 ()<UITableViewDataSource,UITableViewDelegate>
{
    NSDictionary *_postDict;
    NSDictionary *_attachment;
    NSMutableArray *_arrAccess;
    IBOutlet UILabel *lbTitle;
}
@property (nonatomic, strong) GroupCreateCell_Step2 *prototypeCell;


@end

@implementation GroupCreate_Step2

- (GroupCreateCell_Step2 *)prototypeCell

{
    
    if (!_prototypeCell)
        
    {
        
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:GroupesCreateCellID];
        
    }
    
    return _prototypeCell;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strTypeDeGroupe);
    [self initialization];
    // Do any additional setup after loading the view from its nib.
}
-(void)initialization
{
    [self.tableControl registerNib:[UINib nibWithNibName:GroupCreateCellStep2 bundle:nil] forCellReuseIdentifier:GroupesCreateCellID];
    
    _arrAccess = [NSMutableArray arrayWithObjects:str(strMessageAccessPrivate),
                 str(strMessageAccessSemiPrivate),
                  str(strMessageAccessPublication),
                  nil];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setDataPostDict:(NSDictionary*)postdict Attachment:(NSDictionary*)attachment
{
    _postDict = postdict;
    _attachment =attachment;
    
}
#pragma mark - TableView datasource & delegate
- (void)configureCell:(UITableViewCell *)cellTmp cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GroupCreateCell_Step2 *cell = (GroupCreateCell_Step2 *)cellTmp;
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch (indexPath.row) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"ic_private_access"];
            cell.lblTitle.text =str(strPRIVATE);
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"ic_private_access"];
            cell.lblTitle.text =str(strSEMIPRIVATE);
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
            cell.imgViewAvatar.image=  [UIImage imageNamed:@"public_access"];
            cell.lblTitle.text =str(strPUBLIC);

        }
            break;
        default:
            break;
    }
    cell.lblDescription.text =_arrAccess[indexPath.row];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAccess.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupCreateCell_Step2 *cell = (GroupCreateCell_Step2 *)[tableView dequeueReusableCellWithIdentifier:GroupesCreateCellID];
    [self configureCell:cell cellForRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    
    [self configureCell:self.prototypeCell cellForRowAtIndexPath:indexPath];
    
    
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableControl deselectRowAtIndexPath:indexPath animated:NO];
        [self postGroupAction:indexPath.row];
    
}
-(void)postGroupAction:(NSInteger)index
{
    
    ACCESS_KIND   m_accessKind = ACCESS_PRIVATE;
    switch (index) {
        case 0:
        {
            m_accessKind = ACCESS_PRIVATE;
        }
            break;
        case 1:
        {
            m_accessKind = ACCESS_SEMIPRIVATE;
        }
            break;
        case 2:
        {
            m_accessKind = ACCESS_PUBLIC;
        }
            break;
        default:
            break;
    }
    
    /*
     group[name] = "Battue dimanche"
     1064:      *      group[allow_add] = [0 => ADMIN, 1 => ALL_MEMBERS]
     1065:      *      group[allow_show] = [0 => ADMIN, 1 => ALL_MEMBERS]
     1066:      *      group[description] = "Une bonne battue entre amis"
     1067:      *      group[access] = [0 => Privé, 1 => Semi-privé, 2 => Public]
     1068:      *      group[photo][file] = Données de photo
     */
    [GroupCreateOBJ sharedInstance].accessKind =m_accessKind;
    GroupCreateOBJ *obj =[GroupCreateOBJ sharedInstance];
    
    NSDictionary *postDict =[NSDictionary new];
    NSDictionary * attachment;

    postDict = @{
                 @"group[name]":obj.strName,
                 @"group[description]":obj.strComment,
                 @"group[access]":[NSString stringWithFormat:@"%u",obj.accessKind],
                 @"group[allow_add]":  [NSNumber numberWithBool:obj.allow_add]  ,
                 @"group[allow_show]": [NSNumber numberWithBool:obj.allow_show],
                 @"group[allow_add_chat]":  [NSNumber numberWithBool:obj.allow_chat_add]  ,
                 @"group[allow_show_chat]": [NSNumber numberWithBool:obj.allow_chat_show]

                 };
    
    
    
    
    NSData *fileData;
    if (obj.imgData != nil) {
        UIImage *image = [UIImage imageWithData:obj.imgData];
        fileData = UIImageJPEGRepresentation(image, 0.5); //
        attachment = @{@"kFileName": @"image.png",
                       @"kFileData": fileData};

    }
    //update group
    if ([GroupCreateOBJ sharedInstance].isModifi) {
        [[GroupCreateOBJ sharedInstance] modifiGroupWithVC:self];
    }
    //create new
    else
    {
        [COMMON addLoading:self];
        WebServiceAPI *serviceAPI =[WebServiceAPI new];
        [serviceAPI postGroupAction:postDict withAttachmentMediaDic:attachment];
        serviceAPI.onComplete =^(id response, int errCode)
        {
            
            /*
             {
             "group_id" = 417;
             }
             */
            [COMMON removeProgressLoading];
            if (response[@"group"] != nil) {
                [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFY_REFRESH_GROUP object: nil userInfo: nil];

                [[Group_Hunt_ListShare sharedInstance] fnGetListGroup_Hunts];
                AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
                [app updateAllowShowAdd:response];

                
                NSDictionary *dicResp = response;
                
                [GroupCreateOBJ sharedInstance].group_id = dicResp[@"group_id"];
                //Step9...3
                GroupCreate_Step3 *viewController1 = [[GroupCreate_Step3 alloc] initWithNibName:@"GroupCreate_Step3" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISGROUP iAmParent:NO];
            }
            
        };
    }
}
@end
