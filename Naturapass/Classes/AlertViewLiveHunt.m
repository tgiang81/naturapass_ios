//
//  UploadProgress.m
//  Naturapass
//
//  Created by GS-Soft on 9/27/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "AlertViewLiveHunt.h"
#import "CellKind12.h"
#import "AppCommon.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"
#import "Define.h"
#import "LiveHuntOBJ.h"
#import "AlertViewLiveHuntCell.h"
@interface AlertViewLiveHunt ()
{
    IBOutlet UITableView *tableControl;
    UIColor *color;
    UIImage *imgAdd;
    NSString *sender_id;
}
@end

static NSString *identifier = @"AlertViewLiveHuntCellID";

@implementation AlertViewLiveHunt
- (instancetype)initListLiveHunt {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertViewLiveHunt" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        subAlertView.layer.masksToBounds = YES;
        subAlertView.layer.cornerRadius = 10.0;
        [tableControl registerNib:[UINib nibWithNibName:@"AlertViewLiveHuntCell" bundle:nil] forCellReuseIdentifier:identifier];
        tableControl.estimatedRowHeight = 80;

    }
    return self;
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [LiveHuntOBJ sharedInstance].arrLiveHunt.count;
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   AlertViewLiveHuntCell *cell = (AlertViewLiveHuntCell *)[tableControl dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    NSDictionary *dic = [LiveHuntOBJ sharedInstance].arrLiveHunt[indexPath.row];
    NSString *strName = [dic[@"name"] emo_emojiString];
    
    cell.lbTitle.text = strName;
    [cell.lbTime  setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: dic[@"meeting"][@"date_begin"] withDateEnd:dic[@"meeting"][@"date_end"]]];
    NSString *strImage;
    if (dic[@"profilepicture"] != nil) {
        strImage=dic[@"profilepicture"];
    }else{
        strImage=dic[@"photo"];
    }
    if(![strImage containsString:IMAGE_ROOT_API])
    {
        strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,strImage];
    }
    NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
    
    [cell.imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"profile"] ];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dic = [[LiveHuntOBJ sharedInstance].arrLiveHunt[indexPath.row] copy];
    [self removeFromSuperview];
    if (_callbackLive) {
        _callbackLive(indexPath.row, dic);
    }
}

@end
