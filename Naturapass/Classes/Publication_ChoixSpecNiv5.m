//
//  Publication_ChoixSpecNiv5.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_ChoixSpecNiv5.h"
#import "Publication_Partage.h"
#import "CellKind5.h"
#import "CellKind5_1.h"
#import "Card_Cell_Type0.h"
#import "Card_Cell_Type1.h"
#import "Card_Cell_Type10.h"
#import "Card_Cell_Type20.h"
#import "Card_Cell_Type30.h"
#import "Card_Cell_Type40.h"
#import "Card_Cell_Type32.h"
#import "Card_Cell_Type31.h"
#import "Card_Cell_Type_Base.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "AlertPublicationMultipleSelect.h"
#import "MDPopover.h"
#import "MMNumberKeyboard.h"
#import "MyAlertView.h"

#import "TagView31.h"
#import "TagView32.h"


static NSString *identifierSection0 = @"MyTableViewCell0";
static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection10 = @"MyTableViewCell10";
static NSString *identifierSection20 = @"MyTableViewCell20";
static NSString *identifierSection30 = @"MyTableViewCell30";
static NSString *identifierSection40 = @"MyTableViewCell40";
static NSString *identifierSection32 = @"MyTableViewCell32";
static NSString *identifierSection31 = @"MyTableViewCell31";

static NSString *identifier1 = @"MyTableViewCell1";
static NSString *identifier2 = @"MyTableViewCell2";
static NSString *Card_Cell = @"Card_Cell_Type_Base";

@interface Publication_ChoixSpecNiv5 () <UITextFieldDelegate,MMNumberKeyboardDelegate >
{
    NSMutableArray * arrData;
    
    UITextField *txtAnimal;
    IBOutlet UIButton *btnSuivatnt;
    int indexEdit;
    MDPopover *popup;
    NSDateFormatter *formatter;
    NSIndexPath *indexPathTextCell;
    
    
}
//Tag32


- (IBAction)resignFirstResponder:(id)sender;
- (IBAction)takeFirstResponder:(id)sender;
- (IBAction)enabledSwitched:(id)sender;
- (IBAction)completeDuplicatesSwitched:(id)sender;



@property (nonatomic, strong) Card_Cell_Type_Base *prototypeCell;
@property (strong, nonatomic)   IBOutlet TPKeyboardAvoidingScrollView *scrollviewKeyboard;

@end

@implementation Publication_ChoixSpecNiv5

/*
 {"observation":{"specific":0,"attachments":[{"label":1,"value":2},{"label":2,"value":1},{"label":3,"value":1},{"label":4,"value":1},{"label":5,"value":3}],"receivers":[],"category":128}}
 
 //Specific 0 - 1 if get Card from Search...
 
 */

/*
 type = 20 => date input (11/02/2016)
 type = 21 => hour input (09:00)
 type = 22 => date + hour input (11/02/2016 09:00)
 */

#pragma mark - COMMON functions

- (void) dateUpdated:(UIDatePicker *)datePicker {
    
    [self updateDataDate];
    
}

-(void) updateDataDate
{
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:    [formatter setDateFormat:@"dd/MM/YYYY"];
            break;
        case 21:    [formatter setDateFormat:@"HH:mm"];
            break;
        case 22:    [formatter setDateFormat:@"dd/MM/YYYY HH:mm"];
            break;
    }
    
    NSString *strTest = [formatter stringFromDate:self.datePicker.date];
    ASLog(@"%@",strTest);
    //update text value for text in cell.
    Card_Cell_Type_Base *cell = [self.tableControl cellForRowAtIndexPath:indexPathTextCell];
    
    cell.txtInput.text=strTest;
    
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[indexPathTextCell.row]];
    [dicMul removeObjectForKey:@"value"];
    [dicMul setObject:strTest forKey:@"value"];
    
    [dicMul removeObjectForKey:@"text"];
    [dicMul setObject:strTest forKey:@"text"];
    [arrData replaceObjectAtIndex:indexPathTextCell.row withObject:dicMul];
    
}

/*
 - (IBAction)buttonDone:(id)sender {
 [self.view endEditing:YES];
 
 }
 - (void)resignKeyboard:(id)sender
 {
 [self.view endEditing:YES];
 [self updateDataDate];
 }
 */
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    //tick the selected index...
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[Card_Cell_Type_Base class]]) {
        parent = parent.superview;
    }
    
    Card_Cell_Type_Base *cell = (Card_Cell_Type_Base *)parent;
    
    indexPathTextCell = [self.tableControl indexPathForCell:cell];
    
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeDate];
            [self updateDataDate];
        }
            break;
        case 21:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeTime];
            [self updateDataDate];
        }
            break;
        case 22:
        {
            [self.datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
            [self updateDataDate];
        }
            break;
    }
    
    return true;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.datePicker.backgroundColor = [UIColor whiteColor];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    [self.viewPickerBackground removeFromSuperview];
    
    [self.datePicker addTarget:self action:@selector(dateUpdated:) forControlEvents:UIControlEventValueChanged];
    
    
    formatter = [[NSDateFormatter alloc] init];
    
    indexEdit =-1;
    arrData = [NSMutableArray new];
    if (self.isEditFavo) {
        NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
        arrData =dicFavo[@"attachments"];
        if (self.nodeItemAttachment) {
            for (int i=0; i<arrData.count; i++) {
                if ([arrData[i][@"label"]intValue]==[self.nodeItemAttachment[@"label"]intValue]) {
                    indexEdit =i;
                    break;
                }
            }
        }
    }
    else
    {
        if (self.iSpecific == 0)
        {
            NSArray *listCards = [[PublicationOBJ sharedInstance] getCache_Cards];
            
            /*
             ok, but on other level, if we have empty card, we have not empty page, we have the question or sharing
             so this is the same on the first level, if empty card, show share Question
             */
            
            if (!listCards) {
                Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            }
            
            for (NSDictionary*dicCard in listCards) {
                if ([self.myDic[@"card"][@"id"] intValue] == [dicCard[@"id"] intValue]) {
                    //Edit...Card UP - DOWN
                    
                    self.myCard = dicCard;
                    break;
                }
            }
            
            
        }else{
            //get from parent
            
        }
        
        //    NSLog(@"%@",self.myDic);
        
        for (int  i =0; i<[self.myCard[@"labels"] count]; i++) {
            NSDictionary *dic = [self.myCard[@"labels"][i] copy];
            NSDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:dic];
            if ([dicMul[@"type"] intValue]==10) {
                [dicMul setValue:@"0" forKey:@"value"];
                [dicMul setValue:@"0" forKey:@"text"];
                
            }
            if ([dicMul[@"type"] intValue]==11) {
                [dicMul setValue:@"0.0" forKey:@"value"];
                [dicMul setValue:@"0.0" forKey:@"text"];
                
            }
            [arrData addObject:dicMul];
        }
        //if modifi publication then fill data card
        
        if (_modifiAttachment) {
            [self fillDataAttachment];
        }
    }
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind5" bundle:nil] forCellReuseIdentifier:identifier1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind5_1" bundle:nil] forCellReuseIdentifier:identifier2];
    
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type0" bundle:nil] forCellReuseIdentifier:identifierSection0];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type1" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type10" bundle:nil] forCellReuseIdentifier:identifierSection10];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type20" bundle:nil] forCellReuseIdentifier:identifierSection20];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type30" bundle:nil] forCellReuseIdentifier:identifierSection30];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type40" bundle:nil] forCellReuseIdentifier:identifierSection40];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type32" bundle:nil] forCellReuseIdentifier:identifierSection32];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Card_Cell_Type31" bundle:nil] forCellReuseIdentifier:identifierSection31];
    self.tableControl.estimatedRowHeight = 80;
    //    popup= [[MDPopover alloc] initWithInView:self.view];
    [self InitializeKeyboardToolBar];
    
    if ([PublicationOBJ sharedInstance].isEditer) {
        [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    
}
-(void)fillDataAttachment
{
    NSArray *arrOb = [PublicationOBJ sharedInstance].dicEditer[@"observations"];
    if (arrOb.count > 0) {
        NSArray *modifiAttachment = arrOb[0][@"attachments"];
        if (modifiAttachment > 0) {
            
            for (int index = 0; index < arrData.count; index++) {
                NSMutableDictionary *dicMul =[NSMutableDictionary dictionaryWithDictionary:arrData[index]];
                for (NSDictionary *itemAttachment in modifiAttachment) {
                    if ([itemAttachment[@"label"] isEqualToString: dicMul[@"name"]] ) {
                        switch ([dicMul[@"type"] intValue]) {
                            case 0:
                            case 1:
                            case 10:
                            case 11:
                            case 20:
                            case 21:
                            case 22:
                            {
                                if (itemAttachment[@"value"]) {
                                    [dicMul setObject:itemAttachment[@"value"] forKey:@"value"];
                                    [dicMul setObject:itemAttachment[@"value"] forKey:@"text"];
                                    
                                    [arrData replaceObjectAtIndex:index withObject:dicMul];
                                }
                            }
                                break;
                                
                            case 30:
                            case 31:
                            case 32:
                            case 40:
                            case 50:
                            {
                                NSArray *arrValues = itemAttachment[@"values"];
                                
                                if (arrValues.count > 0) {
                                    NSArray *arrContent = dicMul[@"contents"];
                                    NSMutableArray *arrResultValue = [NSMutableArray new];
                                    NSMutableArray *arrResultText = [NSMutableArray new];
                                    
                                    for (NSDictionary *dicContent in arrContent) {
                                        for (NSString *strTmp in arrValues) {
                                            if ([strTmp isEqualToString:dicContent[@"name"]]) {
                                                [arrResultText addObject:strTmp];
                                                [arrResultValue addObject:dicContent[@"id"]];
                                            }
                                        }
                                    }
                                    if(arrResultValue.count > 0)
                                    {
                                        NSString *strText= [arrResultText componentsJoinedByString:@", "];
                                        [dicMul setObject:strText forKey:@"text"];
                                        if ([dicMul[@"type"] intValue] == 30) {
                                            [dicMul setObject:arrResultValue[0] forKey:@"value"];
                                        }
                                        else
                                        {
                                            [dicMul setObject:arrResultValue forKey:@"value"];
                                        }
                                        [arrData replaceObjectAtIndex:index withObject:dicMul];
                                    }
                                    
                                }
                                
                            }
                                break;
                                
                            default:
                            {
                            }
                                break;
                        }
                        
                    }
                }
            }
            
        }
    }
}

/*END pop tags field*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    NSDictionary *dic = arrData[indexPathTextCell.row];
    
    switch ([dic[@"type"] intValue]) {
        case 20:
        case 21:
        case 22:
        {
            [self updateDataDate];
        }
            break;
    }
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
    
}
- (void)textViewDidEndEditing:(UITextView *)m_textView {
    [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

-(void) showDateView:(UIButton *)sender
{
    
    //Card_Cell_Type20
    
    UIView *parent = [sender superview];
    while (parent && ![parent isKindOfClass:[Card_Cell_Type20 class]]) {
        parent = parent.superview;
    }
    
    Card_Cell_Type20 *cell = (Card_Cell_Type20 *)parent;
    
    [cell.txtInput becomeFirstResponder];
}

-(Card_Cell_Type_Base*)prototypeCellWithIndexPath:(NSIndexPath*)indexPath
{
    NSInteger index =indexPath.row;
    NSDictionary *dic = arrData[index];
    Card_Cell_Type_Base *cell = self.prototypeCell;
    
    switch ([dic[@"type"] intValue]) {
            
            //type =0
        case 0:
        {
            cell = (Card_Cell_Type0 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0];
        }
            break;
            //type 1
        case 1:
        {
            cell = (Card_Cell_Type1 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
        }
            break;
            //type =10
        case 10:
        {
            
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10];
        }
            break;
            //type =11
        case 11:
        {
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10];
        }
            break;
            //type =20
        case 20:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            //type =21
        case 21:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            //not design
            //type =22
        case 22:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20];
        }
            break;
            
            //type =30
        case 30:
        {
            cell = (Card_Cell_Type30 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection30];
        }
            break;
            //type =30
        case 31:
        {
            cell = (Card_Cell_Type31 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection31];
        }
            break;
            //type =32
        case 32:
        {
            cell = (Card_Cell_Type32 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection32];
        }
            break;
            //type =40
            
        case 40:
        case 50:
            
        {
            cell = (Card_Cell_Type40 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection40];
        }
            break;
        default:
        {
        }
            break;
    }
    return cell;
    
}
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([cellTmp isKindOfClass:[Card_Cell_Type_Base class]])
    {
        NSInteger index =indexPath.row;
        NSDictionary *dic = arrData[index];
        
        Card_Cell_Type_Base *cell = (Card_Cell_Type_Base *)cellTmp;
        switch ([dic[@"type"] intValue]) {
                
                //type =0
            case 0:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"pen-icon"];
                cell.txtInput.delegate = self;
                [cell.txtInput setInputAccessoryView:self.keyboardToolbar];
                cell.txtInput.text=dic[@"text"];
                
            }
                break;
                //type 1
            case 1:
            {
                [cell.tvDescription setInputAccessoryView:self.keyboardToolbar];
                cell.tvDescription.text=dic[@"text"];
                [cell setCallBackEditing:^(NSInteger index)
                 {
                     [self.scrollviewKeyboard setContentOffset:CGPointMake(0, 0) animated:YES];
                 }];
            }
                break;
                //type =10
            case 10:
            {
                cell.typenumber = NUMBER_INT;
                [cell.txtInputNumber setInputAccessoryView:self.keyboardToolbar];
                cell.txtInputNumber.keyboardType = UIKeyboardTypeNumberPad;
                cell.txtInputNumber.text=dic[@"text"];
                cell.txtInputNumber.delegate = self;
                
            }
                break;
                //type =11
            case 11:
            {
                
                //val
                cell.typenumber = NUMBER_FLOAT;
                [cell.txtInputNumber setInputAccessoryView:self.keyboardToolbar];
                //                cell.txtInputNumber.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
                cell.txtInputNumber.text=dic[@"text"];
                cell.txtInputNumber.delegate = self;
                
                ///
                // Create and configure the keyboard.
                MMNumberKeyboard *keyboard = [[MMNumberKeyboard alloc] initWithFrame:CGRectZero];
                keyboard.allowsDecimalPoint = YES;
                keyboard.delegate = self;
                cell.txtInputNumber.inputView = keyboard;
                
            }
                break;
                //type =20
            case 20:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"ic_mes_chasse_inactive"];
                cell.txtInput.delegate =self;
                
                cell.txtInput.inputView = self.viewPickerBackground;
                
                cell.txtInput.text=dic[@"text"];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //not design
                //type =21
            case 21:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"ic_mes_chasse_inactive"];
                cell.txtInput.delegate =self;
                cell.txtInput.text=dic[@"text"];
                
                cell.txtInput.inputView = self.viewPickerBackground;
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //not design
                //type =22
            case 22:
            {
                cell.imgIconText.image =[UIImage imageNamed:@"ic_mes_chasse_inactive"];
                cell.txtInput.delegate =self;
                
                cell.txtInput.inputView = self.viewPickerBackground;
                
                cell.txtInput.text=dic[@"text"];
                
                [cell.btnShowMultipSelect addTarget:self action:@selector(showDateView:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                
                //type =30
            case 30:
            {
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=str(strChoisir_une_fiche);
                }
                cell.btnShowMultipSelect.tag=indexPath.row+10;
                //                [cell.btnShowMultipSelect addTarget:self action:@selector(showMultipSelect:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showSelectOne:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //type =30
            case 31:
            {
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=@"";
                }
                cell.btnShowMultipSelect.tag=indexPath.row+200;
                //                [cell.btnShowMultipSelect addTarget:self action:@selector(showSelectOne:) forControlEvents:UIControlEventTouchUpInside];
                [cell.btnShowMultipSelect addTarget:self action:@selector(showMultipSelect:) forControlEvents:UIControlEventTouchUpInside];
                
            }
                break;
                //type =32
            case 32:
            {
                cell.btnShowMultipSelect.tag=indexPath.row+100;
                if (dic[@"text"]) {
                    cell.lbDescription.text=[NSString stringWithFormat:@"%@",dic[@"text"]];
                }
                else
                {
                    cell.lbDescription.text=@"";
                }
                [cell.btnShowMultipSelect addTarget:self action:@selector(showPoupMultipleSelect:) forControlEvents:UIControlEventTouchUpInside];
            }
                break;
                //type =40
                
            case 40:
            case 50:
                
            {
                NSArray *arrContent =arrData[index][@"contents"];
                NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
                NSMutableArray *arrCheck = [NSMutableArray new];
                if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
                    arrCheck = [NSMutableArray arrayWithArray:dicMul[@"value"]];
                }
                [cell fnSetData:arrContent arrCheck:arrCheck withTypeControl:([dic[@"type"] intValue] == 40)?CONTROL_CHECKBOX:CONTROL_RADIO];
                
                [cell doBlock:^(NSDictionary *dicResult) {
                    
                    NSMutableArray *listSelect = [NSMutableArray new];
                    NSMutableArray *listSelectText = [NSMutableArray new];
                    
                    listSelect = [NSMutableArray arrayWithArray:dicResult[@"value"]];
                    [dicMul removeObjectForKey:@"value"];
                    [dicMul setObject:listSelect forKey:@"value"];
                    
                    listSelectText = [NSMutableArray arrayWithArray:dicResult[@"text"]];
                    NSString *strText= [listSelectText componentsJoinedByString:@", "];
                    [dicMul removeObjectForKey:@"text"];
                    [dicMul setObject:strText forKey:@"text"];
                    
                    [arrData replaceObjectAtIndex:index withObject:dicMul];
                    [self.tableControl reloadData];
                    
                }];
            }
                break;
            default:
            {
            }
                break;
        }
        cell.index= indexPath.row;
        [cell setCallBackValue:^(NSInteger index, NSDictionary *value)
         {
             NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
             
             [dicMul removeObjectForKey:@"value"];
             [dicMul setObject:value[@"value"] forKey:@"value"];
             
             [dicMul removeObjectForKey:@"text"];
             [dicMul setObject:value[@"value"] forKey:@"text"];
             
             [arrData replaceObjectAtIndex:index withObject:dicMul];
         }];
        
        
        cell.lbTitle.text = dic[@"name"];
        
        [cell layoutIfNeeded];
        
        if (self.isEditFavo && indexEdit ==indexPath.row) {
            cell.lbTitle.textColor = [UIColor redColor];
        }
        else{
            cell.lbTitle.textColor = [UIColor blackColor];
        }
        
        cell.backgroundColor=[UIColor whiteColor];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Card_Cell_Type_Base *cell = (Card_Cell_Type_Base*)[self fnGetUITableViewCellWithIndexPath:indexPath];
    if (cell) {
        [self configureCell:cell forRowAtIndexPath:indexPath];
    }
    return cell;
}

-(UITableViewCell*)fnGetUITableViewCellWithIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index =indexPath.row;
    NSDictionary *dic = arrData[index];
    Card_Cell_Type_Base *cell = nil;
    
    switch ([dic[@"type"] intValue]) {
            
            //type =0
        case 0:
        {
            cell = (Card_Cell_Type0 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
        }
            break;
            //type 1
        case 1:
        {
            cell = (Card_Cell_Type1 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
        }
            break;
            //type =10
        case 10:
        {
            
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10 forIndexPath:indexPath];
        }
            break;
            //type =11
        case 11:
        {
            cell = (Card_Cell_Type10 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection10 forIndexPath:indexPath];
        }
            break;
            //type =20
        case 20:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            //type =21
        case 21:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            //not design
            //type =22
        case 22:
        {
            cell = (Card_Cell_Type20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection20 forIndexPath:indexPath];
        }
            break;
            
            //type =30
        case 30:
        {
            cell = (Card_Cell_Type30 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection30 forIndexPath:indexPath];
        }
            break;
            //type =30
        case 31:
        {
            cell = (Card_Cell_Type31 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection31 forIndexPath:indexPath];
        }
            break;
            //type =32
        case 32:
        {
            cell = (Card_Cell_Type32 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection32 forIndexPath:indexPath];
        }
            break;
            //type =40
            
        case 40:
        case 50:
            
        {
            cell = (Card_Cell_Type40 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection40 forIndexPath:indexPath];
        }
            break;
        default:
        {
        }
            break;
    }
    return cell;
}

//"receivers":[] Is federation...will responsed inside category list.

/*
 {"observation":{"specific":0,"attachments":[{"label":1,"value":2},{"label":2,"value":1},{"label":3,"value":1},{"label":4,"value":1},{"label":5,"value":3}],"receivers":[],"category":128}}
 */

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)onNext:(id)sender {
    //Upload server
    
    if (self.isEditFavo) {
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        [dicFavo setValue:arrData forKey:@"attachments"];
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
        [self gotoback];
    }
    else
    {
        NSMutableArray *arrAttach =  [NSMutableArray new];
        for (int i=0 ; i < arrData.count; i++)
        {
            if (![self isValidate:arrData[i]])
            {
                return;
            }
            NSString *str =@"";
            NSString *strText=@"";
            
            //Type32
            /*
             id = 389;
             name = Calibre;
             required = 0;
             text = "222, 260 Rem, 243 win, 6,5X57R, 6,5X57, 6,5X68S, 270 wsm, 270 win";
             type = 32;
             value =     (
             273,
             275,
             274,
             277,
             276,
             279,
             281,
             280
             );
             */
            /*
             
             {
             "observation": {
             "specific": 0,
             "animal": null,
             "attachments": [{
             "label": 381,
             "value": 252
             }, {
             
             "label": 387,
             "value": 265
             }, {
             "label": 387,
             "value": 266
             }, {
             "label": 387,
             "value": 267
             }, {
             "label": 387,
             "value": 268
             }, {
             "label": 389,
             "value": 276
             }, {
             "label": 389,
             "value": 280
             }],
             "receivers": [],
             "category": 144
             }
             }
             */
            if ([arrData[i][@"value"] isKindOfClass:[NSArray class]]) {
                
                NSArray *arrValue = arrData[i][@"value"];
                NSString *itemText = arrData[i][@"text"];
                NSArray *arrText = [itemText componentsSeparatedByString:@","];
                
                for (int j =0; j<arrValue.count;j++) {
                    id itemValue = arrValue[j];
                    if (itemValue!=nil) {
                        str=[[NSString stringWithFormat:@"%@",itemValue] emo_UnicodeEmojiString];
                        
                        
                    }
                    if (j<arrText.count) {
                        if (arrText[j]!=nil) {
                            strText=[NSString stringWithFormat:@"%@",arrText[j]];
                        }
                    }
                    [arrAttach addObject:@{
                                           @"label": self.myCard[@"labels"][i][@"id"],
                                           @"value": str,
                                           //for viewing
                                           @"text": strText,
                                           @"name":self.myCard[@"labels"][i][@"name"]
                                           }];
                    
                }
            }
            else//if Type == value
            /*{
             id = 391;
             name = "Distance du tir (m)";
             required = 0;
             text = 2;
             type = 10;
             value = 2;
             }
             */
            {
                if (arrData[i][@"value"]!=nil) {
                    //to UNICODE
                    str = [[NSString stringWithFormat:@"%@",arrData[i][@"value"]] emo_UnicodeEmojiString];
                    strText = [[NSString stringWithFormat:@"%@",arrData[i][@"text"]] emo_UnicodeEmojiString];
                    
                }
                [arrAttach addObject:@{
                                       @"label": self.myCard[@"labels"][i][@"id"],
                                       @"value": str,
                                       @"text": strText,
                                       @"name":self.myCard[@"labels"][i][@"name"]
                                       }];
                
            }
        }
        //ID  ...
        NSDictionary *dicObservation = nil;
        
        if (self.iSpecific == 1)
        {
            if (arrAttach.count>0) {
                dicObservation = @{
                                   @"specific": [NSNumber numberWithInt: self.iSpecific ],
                                   @"animal": self.id_animal,
                                   @"attachments" : arrAttach ,
                                   @"category" : self.myDic[@"id"]
                                   };
            }
            else
            {
                dicObservation = @{
                                   @"specific": [NSNumber numberWithInt: self.iSpecific ],
                                   @"animal": self.id_animal,
                                   @"category" : self.myDic[@"id"]
                                   };
            }
            
        }else{
            if (arrAttach.count>0) {
                dicObservation = @{
                                   @"specific": [NSNumber numberWithInt: self.iSpecific ],
                                   @"attachments" : arrAttach ,
                                   @"category" : self.myDic[@"id"]
                                   };
            }
            else
            {
                dicObservation = @{
                                   @"specific": [NSNumber numberWithInt: self.iSpecific ],
                                   @"category" : self.myDic[@"id"]
                                   };
            }
            
        }
        NSString *strPrecision =[NSString stringWithFormat:@"%@",self.myName];
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        if (strPrecision) {
            [dicFavo setValue:strPrecision forKey:@"category_tree"];
        }
        [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
        [dicFavo setValue:dicObservation forKey:@"observation"];
        [dicFavo setValue:arrAttach forKey:@"attachments"];//hien thi
        
        [dicFavo setValue:self.myDic[@"card"][@"id"] forKey:@"card_id"];
        
        if ([self.myDic[@"receivers"] isKindOfClass: [NSArray class]]) {
            [PublicationOBJ sharedInstance].arrReceivers = self.myDic[@"receivers"];
            [dicFavo setValue:self.myDic[@"receivers"] forKey:@"receivers"];
            
        }
        
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
        //edit ob
        if ([PublicationOBJ sharedInstance].isEditer) {
            [[PublicationOBJ sharedInstance] modifiPublication:self withType:EDIT_PRECISIONS];
        }
        else if ([PublicationOBJ sharedInstance].isEditFavo)
        {
            [self backEditFavo];
        }
        else
        {
            
            Publication_Partage *viewController1 = [[Publication_Partage alloc] initWithNibName:@"Publication_Partage" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
    }
}
- (BOOL)isValidate:(NSDictionary *)dic{
    
    NSString *str=nil;
    if ([dic[@"required"] boolValue]) {
        if ([dic[@"type"] intValue]==10) {
            if ([dic[@"value"] intValue]<=0) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
        else
        {
            if (dic[@"value"]== nil) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
    }
    else
    {
        if ([dic[@"type"] intValue]==10) {
            if ( dic[@"value"]== nil ) {
                str =dic[@"name"];
            }
            else
            {
                return YES;
            }
        }
    }
    if (str) {
        UIAlertView   *alert = [[UIAlertView alloc] initWithTitle:@""
                                                          message:[NSString stringWithFormat:str(strVeuillez_remplir_le_champs),str]
                                                         delegate:self
                                                cancelButtonTitle:str(strOK)
                                                otherButtonTitles:nil];
        [alert show];
        
        return NO;
    }
    else
    {
        return YES;
    }
}

-(IBAction)showMultipSelect:(id)sender
{
    NSInteger index =[sender tag]-200;
    NSArray *arrContent =arrData[index][@"contents"];
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    NSMutableArray *arrCheck = [NSMutableArray new];
    if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
        arrCheck = [NSMutableArray arrayWithArray:dicMul[@"value"]];
    }
    
    
    
    MyAlertView *ttest = [[[NSBundle mainBundle] loadNibNamed:@"MyAlertView" owner:self options:nil] objectAtIndex:0] ;
    
    
    [ttest  initParams:@"abc" arrContent:arrContent arrCheck:arrCheck withTypeControl:CONTROL_CHECKBOX parent:self];
    //
    [ttest doBlock:^(NSDictionary *dicResult) {
        
        NSMutableArray *listSelect = [NSMutableArray new];
        listSelect = [NSMutableArray arrayWithArray:dicResult[@"value"]];
        [dicMul removeObjectForKey:@"value"];
        [dicMul setObject:listSelect forKey:@"value"];
        
        NSMutableArray *listSelectText = [NSMutableArray new];
        listSelectText = [NSMutableArray arrayWithArray:dicResult[@"text"]];
        NSString *strText= [listSelectText componentsJoinedByString:@", "];
        [dicMul removeObjectForKey:@"text"];
        [dicMul setObject:strText forKey:@"text"];
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
        
    }];
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
}

#pragma mark - TYPE32

-(IBAction)showPoupMultipleSelect:(id)sender
{
    NSInteger index =[sender tag]-100;
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    
    NSArray *arrContent =dicMul[@"contents"];
    
    NSArray*arrCheck = nil;
    
    if ([dicMul[@"value"] isKindOfClass:[NSArray class]]) {
        arrCheck =  dicMul[@"value"];
    }
    
    TagView32 *ttest = [[[NSBundle mainBundle] loadNibNamed:@"TagView32" owner:self options:nil] objectAtIndex:0] ;
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                     
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    [ttest doBlock:^(NSInteger index,NSDictionary *dicResult) {
        NSMutableArray *listSelect = [NSMutableArray new];
        NSMutableArray *listSelectText = [NSMutableArray new];
        for (NSDictionary *dicData in dicResult[@"arrData"]) {
            [listSelect addObject:dicData[@"value"]];
            [listSelectText addObject:dicData[@"text"]];
            
        }
        [dicMul removeObjectForKey:@"value"];
        [dicMul setObject:listSelect forKey:@"value"];
        
        NSString *strText= [listSelectText componentsJoinedByString:@", "];
        [dicMul removeObjectForKey:@"text"];
        [dicMul setObject:strText forKey:@"text"];
        
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
    }];
    [ttest showViewWithData:arrContent withArrSelected:arrCheck withIndex:index];
}

/****************/
//Tag31

//[{"label":76,"value":2}]

-(IBAction)showSelectOne:(id)sender
{
    TagView31 *ttest = [[[NSBundle mainBundle] loadNibNamed:@"TagView31" owner:self options:nil] objectAtIndex:0] ;
    
    [self.vContainer addSubview:ttest];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[ttest]-(0)-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    [self.vContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[ttest]|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:NSDictionaryOfVariableBindings(ttest)]];
    
    NSInteger index =[sender tag]-10;
    NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:arrData[index]];
    
    [ttest showViewWithData:dicMul[@"contents"] withstrSelected:dicMul[@"value"] withIndex:index];
    [ttest doBlock:^(NSInteger index,NSDictionary *dicResult) {
        [dicMul removeObjectForKey:@"value"];
        [dicMul removeObjectForKey:@"text"];
        
        if (!([dicResult[@"isRemove"] boolValue] ==YES)) {
            [dicMul setObject:dicResult[@"value"] forKey:@"value"];
            [dicMul setObject:dicResult[@"text"] forKey:@"text"];
        }
        
        
        [arrData replaceObjectAtIndex:index withObject:dicMul];
        [self.tableControl reloadData];
        
    }];
}

@end
