//
//  PublicAlertView.m
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AlertListMarker.h"
#import "Define.h"
#import "AppCommon.h"
#import "AppDelegate.h"
#import "CommonHelper.h"
#import "AlertListMarkerCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSDate+Extensions.h"
#import "AlertHeaderCell.h"
#import "UIImage+Utils.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@implementation AlertListMarker
{
}
-(instancetype)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"AlertListMarker" owner:self options:nil] objectAtIndex:0] ;
    
    if (self) {
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertHeaderCell" bundle:nil] forCellReuseIdentifier:@"AlertHeaderCell"];
        
        [self.tableControl registerNib:[UINib nibWithNibName:@"AlertListMarkerCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
        self.tableControl.estimatedRowHeight = 70;
        [self.tableControl.layer setMasksToBounds:YES];
        self.tableControl.layer.cornerRadius= 10;
        self.tableControl.layer.borderWidth =0;
    }
    return self;
}
-(void)awakeFromNib{
    [super awakeFromNib];
    
}

#pragma callback
-(void)setCallback:(AlertListMarkerCallback)callback
{
    _callback=callback;
}

-(void)doBlock:(AlertListMarkerCallback ) cb
{
    self.callback = cb;
    
}
#pragma mark -action
-(void)showAlert
{
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [view setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.2]];
    
    view.frame = app.window.frame;
    
    [[app window]addSubview:view];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    
    [app.window addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:NSDictionaryOfVariableBindings(view)]];
    [self.tableControl reloadData];
}

-(IBAction)dissmiss:(id)sender
{
    [self removeFromSuperview];
}
#pragma mark Search Helpers

#pragma mark UITableViewDataSource

//Header
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 70;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    AlertHeaderCell *cell = (AlertHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:@"AlertHeaderCell"];
    cell.text.text=@"Choisissez l'information que vous voulez afficher";
    cell.backgroundColor =UIColorFromRGB(MAIN_COLOR);
    return  cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrMarker.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AlertListMarkerCell *cell = nil;
    
    cell = (AlertListMarkerCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dicMarker = self.arrMarker[indexPath.row];
    NSString * strImage = nil;
    NSString * photoDefault = nil;
    
    cell.imageIcon.backgroundColor = [UIColor clearColor];
    switch ([dicMarker[@"c_marker"] intValue]) {
        case  MARKER_AGENDA:
        {
            cell.lbTitle.text = dicMarker[@"c_name"];
            NSTimeInterval timeMeetingDate =  [dicMarker[@"c_start_date"] integerValue];
            NSString *strMeetingDate = [NSDate convertTimeInterval:timeMeetingDate withFormat:@"dd-MMM"];
            
            NSTimeInterval timeEndDate =  [dicMarker[@"c_end_date"] integerValue];
            NSString *strEndDate = [NSDate convertTimeInterval:timeEndDate withFormat:@"dd-MMM"];
            cell.lbDescription.text  = [NSString stringWithFormat:@"%@ - %@", strMeetingDate,strEndDate];
            [cell.lbDescription setTextColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
            
            photoDefault = @"ic_agenda_point";
            
            UIImage *mask = [UIImage imageNamed:@"mask.png"];

            UIImage *maskedImage = [UIImage maskImage:[UIImage imageNamed: photoDefault] withMask:mask];

            cell.imageIcon.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            cell.imageIcon.image = maskedImage ;
        }
            break;
            
        case MARKER_PUBLICATION:
        {
            NSTimeInterval timer =  [dicMarker[@"c_created"] integerValue];
            NSString *strCreated = [NSDate convertTimeIntervalToNSTring:timer];
            cell.lbTitle.text = strCreated;
            //First name + lastname
            
            cell.lbDescription.text = dicMarker[@"c_owner_name"];
            [cell.lbDescription setTextColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            
            if ([dicMarker[@"owner"] isKindOfClass: [NSDictionary class] ]) {
                //normal dic
                if ( [dicMarker[@"owner"][@"id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                    //My publication
                    photoDefault = @"green_circle_map_icon_photo";
                }else{
                    //Other one
                    photoDefault = @"green_carre_map_icon_photo";
                }
                
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicMarker[@"mobile_markers"][@"picto"]];
                
            }else{
                if ( [dicMarker[@"c_owner_id"] intValue] == [[[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"] intValue]) {
                    //My publication
                    photoDefault = @"green_circle_map_icon_photo";
                }else{
                    //Other one
                    photoDefault = @"green_carre_map_icon_photo";
                }
                
                strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dicMarker[@"c_marker_picto"]];
            }
            NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
            [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:photoDefault] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
            
        }
    }
    
    //FONT
    [cell.lbTitle setTextColor:[UIColor blackColor]];
    [cell.lbTitle setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.lbDescription setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.imageLine.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dissmiss:nil];
    NSDictionary *dicMarker = self.arrMarker[indexPath.row];
    if (_callback) {
        _callback(dicMarker);
    }
}

@end
