//
//  Publication_Favoris_System.m
//  Naturapass
//
//  Created by Manh on 1/13/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Publication_Favoris_System_Edit.h"

#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "TreeViewNode.h"
#import "PublicationFavorisSystemCell.h"
#import "PublicationOBJ.h"
#import "OHAttributedLabel.h"
#import "FavorisSystemBaseCell.h"
#import "PublicationFavorisSystemColorCell.h"
#import "Publication_Ajout_Favoris.h"
#import "Publication_Legende_Carte.h"
#import "Publication_Color.h"
#import "Publication_Favoris_System_Edit_Feche.h"
#import "Publication_Choix_Partage.h"
#import "Publication_Niv6_Validation.h"
#import "Publication_ChoixSpecNiv1.h"
static NSString *favorisCell = @"FavorisCell";
static NSString *favorisColorCell = @"FavorisColorCell";

@interface Publication_Favoris_System_Edit ()
{
    NSMutableArray *nodes;
    IBOutlet UIButton *btnOnNext;
    int isOnNext;
    NSDictionary *myCard;
    NSDictionary *typeCard;
    NSMutableArray *arrAttach;
    BOOL isNoCard;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    
}
@end

@implementation Publication_Favoris_System_Edit

- (void)viewDidLoad {
    [super viewDidLoad];
    [btnOnNext setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    lbDescription1.text = str(strFAVORIS);
    lbDescription2.text = str(strRetrouvez_ici_toutes_vos_informations);
    
    //    [btnOnNext setBackgroundColor:[UIColor grayColor]];
    [btnOnNext setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
    [self.tableControl registerNib:[UINib nibWithNibName:@"PublicationFavorisSystemCell" bundle:nil] forCellReuseIdentifier:favorisCell];
    [self.tableControl registerNib:[UINib nibWithNibName:@"PublicationFavorisSystemColorCell" bundle:nil] forCellReuseIdentifier:favorisColorCell];
    //
    nodes =[NSMutableArray new];
    //
    if ([[PublicationOBJ sharedInstance].dicFavoris[@"card_id"] isEqualToString:@""]) {
        isNoCard=YES;
    }
    else
    {
        [self fnGetCard];
    }
    
}

-(void)fnGetCard
{
    NSArray *listCards = [[PublicationOBJ sharedInstance] getCache_Cards];
    NSDictionary *dicCardsSpecific = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
    NSMutableArray *listFullCard = [NSMutableArray arrayWithArray:listCards];
    [listFullCard addObject:dicCardsSpecific];
    //update  arrReceivers
    NSMutableDictionary *updatedicFovoris = [[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    
    NSMutableArray *updatearrReceivers = [updatedicFovoris[@"receivers"] mutableCopy];
    
    int iCount = (int) updatearrReceivers.count;
    for (int i= 0; i< iCount; i++) {
        
        NSMutableDictionary *dic= [updatearrReceivers[i] mutableCopy];
        
        [dic setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        
        [updatearrReceivers replaceObjectAtIndex:i withObject:dic];
    }
    
    if (updatearrReceivers) {
        [updatedicFovoris setObject:updatearrReceivers forKey:@"receivers"];
        [PublicationOBJ sharedInstance].dicFavoris = updatedicFovoris;
    }
    
    NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
    for (NSDictionary*dicCard in listFullCard) {
        if ([dicFavo[@"card_id"] intValue] == [dicCard[@"id"] intValue]) {
            //Edit...Card UP - DOWN
            myCard = dicCard;
            break;
        }
    }
    NSArray *arrAttactment =dicFavo[@"attachments"];
    arrAttach = [NSMutableArray new];
    
    for (NSDictionary *dicAtt in arrAttactment) {
        for (NSDictionary *dicCard in myCard[@"labels"]) {
            if ([[NSString stringWithFormat:@"%@",dicAtt[@"label"]] isEqualToString: [NSString stringWithFormat:@"%@",dicCard[@"name"]]]) {
                
                NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:dicCard];
                NSMutableDictionary *value= [NSMutableDictionary dictionaryWithDictionary:dicAtt];
                
                switch ([dicMul[@"type"] intValue]) {
                    case 30:
                    {
                        
                        /*
                         {
                         text = "select type 1";
                         value = 1;
                         }
                         */
                        
                        for (NSDictionary *dicContent in dicMul[@"contents"]) {
                            if (value[@"values"]) {
                                NSString *strValue =[NSString stringWithFormat:@"%@",value[@"values"][0]];
                                NSString *str =[NSString stringWithFormat:@"%@",dicContent[@"name"]];
                                if ([strValue isEqualToString:str]) {
                                    [dicMul removeObjectForKey:@"value"];
                                    [dicMul setObject:dicContent[@"id"] forKey:@"value"];
                                    [dicMul removeObjectForKey:@"text"];
                                    [dicMul setObject:dicContent[@"name"] forKey:@"text"];
                                    break;
                                    
                                }
                                
                            }
                        }
                    }
                        break;
                    case 31:
                    {
                        NSMutableArray *arrText = [NSMutableArray new];
                        NSMutableArray *arrValue = [NSMutableArray new];
                        for (NSString *strV in value[@"values"]) {
                            for (NSDictionary *dicContent in dicMul[@"contents"]) {
                                NSString *strValue =[NSString stringWithFormat:@"%@",strV];
                                NSString *str =[NSString stringWithFormat:@"%@",dicContent[@"name"]];
                                if ([strValue isEqualToString:str]) {
                                    [arrValue addObject:dicContent[@"id"]];
                                    [arrText addObject:dicContent[@"name"]];
                                    break;
                                }
                            }
                        }
                        
                        [dicMul removeObjectForKey:@"value"];
                        [dicMul setObject:arrValue forKey:@"value"];
                        
                        NSString *strText= [arrText componentsJoinedByString:@","];
                        
                        [dicMul setObject:strText forKey:@"text"];
                    }
                        break;
                    case 32:
                    case 40:
                    case 50:
                    {
                        /*
                         {
                         text =     (
                         "checbox 1"
                         );
                         value =     (
                         13
                         );
                         }
                         */
                        NSMutableArray *arrText = [NSMutableArray new];
                        NSMutableArray *arrValue = [NSMutableArray new];
                        for (NSString *strV in value[@"values"]) {
                            for (NSDictionary *dicContent in dicMul[@"contents"]) {
                                NSString *strValue =[NSString stringWithFormat:@"%@",strV];
                                NSString *str =[NSString stringWithFormat:@"%@",dicContent[@"name"]];
                                if ([strValue isEqualToString:str]) {
                                    [arrValue addObject:dicContent[@"id"]];
                                    [arrText addObject:dicContent[@"name"]];
                                    break;
                                }
                            }
                        }
                        [dicMul removeObjectForKey:@"value"];
                        [dicMul setObject:arrValue forKey:@"value"];
                        
                        [dicMul removeObjectForKey:@"text"];
                        [dicMul setObject:arrText forKey:@"text"];
                    }
                        break;
                    default:
                    {
                        [dicMul removeObjectForKey:@"value"];
                        [dicMul setObject:value[@"value"] forKey:@"value"];
                        
                        [dicMul removeObjectForKey:@"text"];
                        [dicMul setObject:value[@"value"] forKey:@"text"];
                        
                        
                    }
                        break;
                }
                [arrAttach addObject:dicMul];
                break;
            }
        }
        
    }
}
-(void)fnGetAttachmentInfo
{
    NSArray *listCards = [[PublicationOBJ sharedInstance] getCache_Cards];
    NSDictionary *dicCardsSpecific = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
    NSMutableArray *listFullCard = [NSMutableArray arrayWithArray:listCards];
    [listFullCard addObject:dicCardsSpecific];
    //update  arrReceivers
    NSMutableDictionary *updatedicFovoris = [[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    NSMutableArray *updatearrReceivers = updatedicFovoris[@"receivers"];
    for (int i= 0; i< updatearrReceivers.count; i++) {
        NSMutableDictionary *dic=[[updatearrReceivers objectAtIndex:i] mutableCopy];
        [dic setValue:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
        [updatearrReceivers replaceObjectAtIndex:i withObject:dic];
    }
    if (updatearrReceivers) {
        [updatedicFovoris setObject:updatearrReceivers forKey:@"receivers"];
        [PublicationOBJ sharedInstance].dicFavoris = updatedicFovoris;
    }
    
    NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
    for (NSDictionary*dicCard in listFullCard) {
        if ([dicFavo[@"card_id"] intValue] == [dicCard[@"id"] intValue]) {
            //Edit...Card UP - DOWN
            myCard = dicCard;
            break;
        }
    }
    NSArray *arrAttactment =dicFavo[@"attachments"];
    arrAttach = [NSMutableArray new];
    for (NSDictionary *dicCard in myCard[@"labels"]) {
        NSMutableDictionary *dicMul= [NSMutableDictionary dictionaryWithDictionary:dicCard];
        BOOL isNoExit =NO;
        for (NSDictionary *dicAtt in arrAttactment) {
            if ([dicAtt[@"label"] intValue] ==[dicCard[@"id"] intValue]) {
                
                NSMutableDictionary *value= [NSMutableDictionary dictionaryWithDictionary:dicAtt];
                
                [dicMul removeObjectForKey:@"value"];
                [dicMul setObject:value[@"value"] forKey:@"value"];
                
                [dicMul removeObjectForKey:@"text"];
                [dicMul setObject:value[@"value"] forKey:@"text"];
                [arrAttach addObject:dicMul];
                isNoExit =YES;
                break;
            }
        }
        if (isNoExit == NO) {
            [dicMul removeObjectForKey:@"value"];
            [dicMul setObject:@"" forKey:@"value"];
            
            [dicMul removeObjectForKey:@"text"];
            [dicMul setObject:@"" forKey:@"text"];
            [arrAttach addObject:dicMul];
        }
        
    }
}
-(void)refreshCard
{
    NSMutableDictionary *dicFavo =[[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    if (isNoCard) {
        [self fnGetAttachmentInfo];
        
        if ([dicFavo[@"observation"] isKindOfClass:[NSDictionary class]]) {
            [dicFavo setObject:dicFavo[@"observation"][@"category"] forKey:@"category_id"];
            [dicFavo setObject:dicFavo[@"observation"][@"specific"] forKey:@"specific"];
            [dicFavo setValue:dicFavo[@"observation"][@"animal"] forKey:@"animal"];
            [PublicationOBJ sharedInstance].dicFavoris= dicFavo;
        }
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [PublicationOBJ sharedInstance].isEditFavo = NO;
    [nodes removeAllObjects];
    
    NSMutableDictionary *dicFavo =[[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    //legend
    if (dicFavo[@"legend"]) {
        TreeViewNode *treeLegend = [[TreeViewNode alloc]init];
        treeLegend.nodeLevel = 0;
        NSString *strLegend = dicFavo[@"legend"];
        if (!(strLegend.length>0)) {
            treeLegend.isEmtry=YES;
        }
        treeLegend.nodeObject = strLegend;
        treeLegend.nodeTitle =str(strLegende);
        treeLegend.isExpanded = NO;
        treeLegend.typeCell = FAV_LEGENDE;
        treeLegend.isEditItem =YES;
        [nodes addObject:treeLegend];
    }
    if (dicFavo[@"color"]) {
        //publication color
        TreeViewNode *treeColor = [[TreeViewNode alloc]init];
        treeColor.nodeLevel = 0;
        NSString *strColor = nil;
        
        if ([dicFavo[@"color"] isKindOfClass:[NSDictionary class]]) {
            strColor =dicFavo[@"color"][@"color"];
            if (!(strColor.length>0)) {
                treeColor.isEmtry=YES;
            }
        }
        else
        {
            treeColor.isEmtry=YES;
        }
        treeColor.nodeObject = strColor;
        treeColor.nodeTitle = str(strCouleur);
        treeColor.isExpanded = NO;
        treeColor.typeCell = FAV_COULEUR;
        treeColor.isEditItem =YES;
        [nodes addObject:treeColor];
    }
    if (dicFavo[@"category_tree"]) {
        //publication precistions
        TreeViewNode *treePrecistions = [[TreeViewNode alloc]init];
        treePrecistions.nodeLevel = 0;
        NSString *strPrecistions = dicFavo[@"category_tree"];
        if (!(strPrecistions.length>0)) {
            treePrecistions.isEmtry=YES;
        }
        if (isNoCard) {
            treePrecistions.isEditItem =YES;
        }
        else
        {
            treePrecistions.isEditItem =NO;
        }
        treePrecistions.nodeObject =strPrecistions;
        treePrecistions.nodeTitle = str(strPrecistions);
        treePrecistions.isExpanded = NO;
        treePrecistions.typeCell = FAV_PRECISIONS;
        [nodes addObject:treePrecistions];
    }
    
    NSArray *attachments = arrAttach;
    if (attachments.count>0) {
        //observation
        TreeViewNode *treeOb = [[TreeViewNode alloc]init];
        treeOb.nodeLevel = 0;
        treeOb.nodeObject = nil;
        treeOb.isEmtry=NO;
        treeOb.nodeTitle = str(strFiche);
        treeOb.isExpanded = YES;
        treeOb.typeCell = FAV_FICHE;
        treeOb.isEditItem =YES;
        treeOb.nodeChildren =[NSMutableArray new];
        //observation child
        for (int i =0 ; i<attachments.count; i++) {
            NSDictionary *dicAtt = attachments[i];
            TreeViewNode *treeAtt = [[TreeViewNode alloc]init];
            treeAtt.nodeLevel = 1;
            NSString *strAtt  = nil;
            if ([dicAtt[@"text"] isKindOfClass:[NSArray class]]) {
                strAtt= [dicAtt[@"text"] componentsJoinedByString:@","];
            }
            else
            {
                strAtt  = dicAtt[@"text"];
            }
            if (!(strAtt.length>0)) {
                treeAtt.isEmtry=YES;
            }
            treeAtt.nodeObject = strAtt;
            treeAtt.nodeTitle = dicAtt[@"name"];
            
            NSDictionary *dicFull = @{@"keyfull": dicAtt, @"index": [NSNumber numberWithInt:i] };
            
            treeAtt.nodeItemAttachment = dicFull;
            treeAtt.isExpanded = NO;
            treeAtt.typeCell = FAV_FICHE_CHILD;
            treeAtt.isEditItem =YES;
            treeAtt.parent = treeOb;
            [treeOb.nodeChildren addObject:treeAtt];
            
        }
        [nodes addObject:treeOb];
    }
    NSString *strpartage =@"";
    if (dicFavo[@"sharing"]) {
        switch ([dicFavo[@"sharing"][@"share"] intValue]) {
            case 0:
            {
                strpartage =str(strMoi);
            }
                break;
            case 1:
            {
                strpartage =str(strAAmis);
            }
                break;
            case 3:
            {
                strpartage =str(strTous_les_natiz);
            }
                break;
            default:
                break;
        }
    }
    
    
    NSArray *arrGroup =dicFavo[@"groups"];
    if (arrGroup.count>0) {
        NSMutableArray *arrGroupName =  [NSMutableArray new];
        NSString*strGroupName = nil;
        for (NSDictionary*dicGroup in arrGroup) {
            if (dicGroup[@"name"]) {
                [arrGroupName addObject:dicGroup[@"name"]];
            }
        }
        strGroupName = [arrGroupName componentsJoinedByString:@", "];
        
        strpartage = [NSString stringWithFormat:@"%@\n\n%@: %@",strpartage,str(strGroupes),strGroupName];
    }
    
    NSArray *arrHunt =dicFavo[@"hunts"];
    if (arrHunt.count>0) {
        NSMutableArray *arrChassesName =  [NSMutableArray new];
        NSString*strChassesName = nil;
        for (NSDictionary*dicHunt in arrHunt) {
            if (dicHunt[@"name"]) {
                [arrChassesName addObject:dicHunt[@"name"]];
            }
        }
        strChassesName = [arrChassesName componentsJoinedByString:@", "];
        
        strpartage = [NSString stringWithFormat:@"%@\n\n%@: %@",strpartage,str(strAAgenda),strChassesName];
    }
    //Personn
    NSArray *arrUser =dicFavo[@"sharingUsers"];
    if (arrUser.count>0) {
        NSMutableArray *arrUserName =  [NSMutableArray new];
        NSString*strUserName = nil;
        for (NSDictionary*dicUser in arrUser) {
            if (dicUser[@"name"]) {
                [arrUserName addObject:dicUser[@"name"]];
            }
        }
        strUserName = [arrUserName componentsJoinedByString:@", "];
        
        strpartage = [NSString stringWithFormat:@"%@\n\n%@: %@",strpartage,str(@"Personnes"),strUserName];
    }
    NSArray *arrReceivers =dicFavo[@"receivers"];
    if (arrReceivers.count>0) {
        NSMutableArray *arrReceiversName =  [NSMutableArray new];
        NSString*strReceiversName = nil;
        for (NSDictionary*dicReceiver in arrReceivers) {
            if ([dicReceiver[@"isSelected"] boolValue] ==YES) {
                if (dicReceiver[@"name"]) {
                    [arrReceiversName addObject:dicReceiver[@"name"]];
                }
            }
        }
        strReceiversName = [arrReceiversName componentsJoinedByString:@", "];
        if (strReceiversName.length>0) {
            strpartage = [NSString stringWithFormat:@"%@\n\n%@: %@",strpartage,str(strSentinelle),strReceiversName];
        }
    }
    //publication precistions
    TreeViewNode *treePartage = [[TreeViewNode alloc]init];
    treePartage.nodeLevel = 0;
    if (strpartage.length==0) {
        treePartage.isEmtry =YES;
    }
    treePartage.nodeObject = strpartage;
    treePartage.nodeTitle = str(strPartage);
    treePartage.isExpanded = NO;
    treePartage.typeCell = FAV_PARTAGE;
    treePartage.isEditItem =YES;
    [nodes addObject:treePartage];
    [self fillDisplayArray];
    [self.tableControl reloadData];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}
#pragma mark - Messages to fill the tree nodes and the display array

- (void)checkFromParent:(TreeViewNode *)parent withStatusNode:(int)statusNode
{
    parent.statusNode = statusNode;
    NSArray *childrenArray = parent.nodeChildren;
    for (TreeViewNode *node in childrenArray) {
        node.statusNode = statusNode;
        //de quy
        if (node.nodeChildren) {
            [self checkFromParent:node withStatusNode:statusNode];
        }
        
    }
}

- (void)checkFromChild:(TreeViewNode *)childrenArray
{
    TreeViewNode *parent = childrenArray.parent;
    if (!parent) {
        return;
    }
    int countCheck =0;
    int countUnCheck =0;
    for (TreeViewNode *treenode in parent.nodeChildren) {
        if (treenode.statusNode == IS_CHECK) {
            countCheck++;
        }
        if (treenode.statusNode == UN_CHECK) {
            countUnCheck++;
        }
    }
    
    if (countCheck == parent.nodeChildren.count) {
        parent.statusNode = IS_CHECK;
    }
    else if ((countCheck>0 && countCheck< parent.nodeChildren.count) || (countUnCheck>0 && countUnCheck < parent.nodeChildren.count))
    {
        parent.statusNode = UN_CHECK;
    }
    else
    {
        parent.statusNode = NON_CHECK;
        
    }
    //de quy
    if (parent.parent) {
        [self checkFromChild:parent];
    }
    
}
//This function is used to fill the array that is actually displayed on the table view
- (void)fillDisplayArray
{
    isOnNext=0;
    self.displayArray = [[NSMutableArray alloc]init];
    for (TreeViewNode *node in nodes) {
        if (node.isEmtry) {
            isOnNext+=1;
        }
        [self.displayArray addObject:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
            
            int index  =(int)self.displayArray.count;
            if(index>0)
            {
                TreeViewNode *nodeLeave = self.displayArray[index-1];
                nodeLeave.lineBottom =YES;
                [self.displayArray replaceObjectAtIndex:index-1 withObject:nodeLeave];
            }
        }
    }
    //set
    //    if (isOnNext>0) {
    //        [btnOnNext setBackgroundColor:[UIColor grayColor]];
    //    }
    //    else
    //    {
    //        [btnOnNext setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
    //
    //    }
}
//This function is used to add the children of the expanded node to the display array
- (void)fillNodeWithChildrenArray:(NSArray *)childrenArray
{
    for (TreeViewNode *node in childrenArray) {
        if (node.isEmtry) {
            isOnNext+=1;
        }
        [self.displayArray addObject:node];
        if (node.isExpanded) {
            [self fillNodeWithChildrenArray:node.nodeChildren];
        }
    }
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.displayArray.count;
}
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    
    if (node.typeCell==FAV_COULEUR) {
        PublicationFavorisSystemColorCell *cell = (PublicationFavorisSystemColorCell*)[tableView dequeueReusableCellWithIdentifier:favorisColorCell];
        if (node.isEmtry) {
            cell.contentView.backgroundColor = UIColorFromRGB(FAVO_ACTIVE_COLOR);
        }
        else
        {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        if (node.nodeObject) {
            NSString *strColor = node.nodeObject;
            unsigned result = 0;
            NSScanner *scanner = [NSScanner scannerWithString:strColor];
            
            [scanner setScanLocation:0]; // bypass '#' character
            [scanner scanHexInt:&result];
            cell.imgColor.backgroundColor = UIColorFromRGB(result);
            cell.imgColor.hidden =NO;
        }
        else
        {
            cell.imgColor.hidden =YES;
        }
        cell.treeNode = node;
        //
        if (node.isEditItem) {
            [cell.imgArrow setImage:[UIImage imageNamed:@"favo_arrow_right"]];
            
        }
        else
        {
            [cell.imgArrow setImage:nil];
        }
        NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
        UIColor *color = [UIColor blackColor]; // select needed color
        if (node.nodeTitle) {
            NSString *title = (NSString*)node.nodeTitle;
            
            NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
            
            NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
            [mLabel appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@": "]];
            [mLabel addAttribute:NSFontAttributeName
                           value:FONT_HELVETICANEUE_MEDIUM(15)
                           range:NSMakeRange(0, title.length+1)];
            //Add label
            [mAttach appendAttributedString:mLabel];
        }
        cell.btnCheckBox.hidden=YES;
        cell.btnSelect.hidden =YES;
        cell.selectCellButton.hidden = YES;
        cell.isEditType =YES;
        [cell.cellLabel setAttributedText:mAttach];
        cell.lineBottom.hidden = NO;
        [cell setNeedsDisplay];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
        
    }
    else
    {
        PublicationFavorisSystemCell *cell = (PublicationFavorisSystemCell*)[tableView dequeueReusableCellWithIdentifier:favorisCell];
        if (node.isEmtry) {
            cell.contentView.backgroundColor = UIColorFromRGB(FAVO_ACTIVE_COLOR);
        }
        else
        {
            cell.contentView.backgroundColor = [UIColor whiteColor];
        }
        if (node.isEditItem) {
            [cell.imgArrow setImage:[UIImage imageNamed:@"favo_arrow_right"]];
        }
        else
        {
            [cell.imgArrow setImage:nil];
        }
        cell.treeNode = node;
        //
        NSMutableAttributedString *mAttach = [NSMutableAttributedString new];
        UIColor *color = [UIColor blackColor]; // select needed color
        if (node.nodeTitle) {
            NSString *title = (NSString*)node.nodeTitle;
            
            NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
            
            NSMutableAttributedString *mLabel = [[NSMutableAttributedString alloc] initWithString:title attributes:attrs];
            if (title.length > 0) {
                [mLabel appendAttributedString: [ NSMutableAttributedString attributedStringWithString:@": "]];
                [mLabel addAttribute:NSFontAttributeName
                               value:FONT_HELVETICANEUE_MEDIUM(15)
                               range:NSMakeRange(0, title.length+1)];
                
            }
            //Add label
            [mAttach appendAttributedString:mLabel];
        }
        if (node.nodeObject) {
            //value
            if(node.typeCell == FAV_FICHE_CHILD)
            {
                color =  UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            }
            else
            {
                color = [UIColor blackColor];
            }
            
            NSDictionary *attrsVal = @{ NSForegroundColorAttributeName : color };
            
            NSMutableAttributedString *mValue = [[NSMutableAttributedString alloc] initWithString:node.nodeObject attributes:attrsVal];
            
            [mAttach appendAttributedString:mValue];
        }
        cell.btnCheckBox.hidden=YES;
        cell.btnSelect.hidden =YES;
        cell.selectCellButton.hidden = YES;
        cell.isEditType =YES;
        //
        [cell.cellLabel setAttributedText:mAttach];
        cell.lineBottom.hidden = NO;
        [cell setNeedsDisplay];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
        
    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TreeViewNode *node = [self.displayArray objectAtIndex:indexPath.row];
    if (node.isEditItem) {
        switch (node.typeCell) {
                //edit Legend
            case FAV_LEGENDE:
            {
                Publication_Legende_Carte *viewController1 = [[Publication_Legende_Carte alloc] initWithNibName:@"Publication_Legende_Carte" bundle:nil];
                viewController1.isEditFavo =YES;
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                
            }
                break;
                //edit color
            case FAV_COULEUR:
            {
                
                Publication_Color *viewController1 = [[Publication_Color alloc] initWithNibName:@"Publication_Color" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                viewController1.isEditFavo =YES;
                
            }
                break;
                //edit obsevation
            case FAV_PRECISIONS:
            {
                Publication_ChoixSpecNiv1 *viewController1 = [[Publication_ChoixSpecNiv1 alloc] initWithNibName:@"Publication_ChoixSpecNiv1" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                [PublicationOBJ sharedInstance].isEditFavo = YES;
            }
                break;
                //edit fiche
            case FAV_FICHE:
            {
                Publication_Favoris_System_Edit_Feche *viewController1 = [[Publication_Favoris_System_Edit_Feche alloc] initWithNibName:@"Publication_Favoris_System_Edit_Feche" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                viewController1.isEditFavo =YES;
                viewController1.nodeItemAttachment = @{@"isFull":@1,@"attachment":arrAttach};
                [viewController1 doBlock:^(int index,NSDictionary *dicResult) {
                    NSArray *arrAttachment= (NSArray*)dicResult[@"attachment"];
                    arrAttach= [arrAttachment mutableCopy];
                }];
                
            }
                break;
                //edit attatchment
            case FAV_FICHE_CHILD:
            {
                Publication_Favoris_System_Edit_Feche *viewController1 = [[Publication_Favoris_System_Edit_Feche alloc] initWithNibName:@"Publication_Favoris_System_Edit_Feche" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                viewController1.nodeItemAttachment = (NSDictionary*) node.nodeItemAttachment [@"keyfull"];
                viewController1.isEditFavo =YES;
                [viewController1 doBlock:^(int index,NSDictionary *dicResult) {
                    NSDictionary *dic = (NSDictionary*)dicResult[@"attachment"][0];
                    int iIndex = [ node.nodeItemAttachment[@"index"] intValue];
                    ASLog(@"SELECT:: %d",iIndex);
                    [arrAttach replaceObjectAtIndex:iIndex withObject:dic];
                }];
                
            }
                break;
            case FAV_PARTAGE:
            {
                NSDictionary *dicFavo =[PublicationOBJ sharedInstance].dicFavoris;
                NSNumber *MDshare = (NSNumber*)dicFavo[@"sharing"][@"share"];
                
                [PublicationOBJ sharedInstance].iShare = [MDshare intValue];
                [PublicationOBJ sharedInstance].arrHunt = dicFavo[@"hunts"];
                [PublicationOBJ sharedInstance].arrGroup = dicFavo[@"groups"];
                [PublicationOBJ sharedInstance].arrsharingUsers = dicFavo[@"sharingUsers"];
                [PublicationOBJ sharedInstance].arrReceivers = dicFavo[@"receivers"];//@[@{@"id":@4,@"name":@"charnoz-sur-ain"}];
                Publication_Choix_Partage *viewController1 = [[Publication_Choix_Partage alloc] initWithNibName:@"Publication_Choix_Partage" bundle:nil];
                [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
                viewController1.isEditFavo =YES;
            }
                break;
                
            default:
                break;
        }
    }
}
-(IBAction)onNext:(id)sender
{
    //check emtry
    //    if (isOnNext>0) {
    //        return;
    //    }
    NSMutableDictionary *dicFavo =[[PublicationOBJ sharedInstance].dicFavoris mutableCopy];
    //upload
    //legend
    [PublicationOBJ sharedInstance].legend =dicFavo[@"legend"];
    //color
    if ([dicFavo[@"color"] isKindOfClass:[NSDictionary class]]) {
        [PublicationOBJ sharedInstance].publicationcolor=dicFavo[@"color"][@"id"];
    }
    //arrAttachments
    NSArray *arrData =arrAttach;
    //observation
    NSMutableArray *MarrAttach =  [NSMutableArray new];
    
    for (int i=0 ; i < arrData.count; i++)
    {
        NSString *str =@"";
        if ([arrData[i][@"value"] isKindOfClass:[NSArray class]]) {
            
            NSArray *arrValue = arrData[i][@"value"];
            for (id itemValue in arrValue) {
                if (itemValue!=nil) {
                    str=[[NSString stringWithFormat:@"%@",itemValue] emo_UnicodeEmojiString];
                    
                }
                [MarrAttach addObject:@{@"label": arrData[i][@"id"],
                                        @"value": str
                                        } ];
            }
        }
        else
        {
            if (arrData[i][@"value"]!=nil) {
                str=[[NSString stringWithFormat:@"%@",arrData[i][@"value"]] emo_UnicodeEmojiString];
                
            }
            [MarrAttach addObject: @{@"label": arrData[i][@"id"],
                                     @"value": str
                                     }];
        }
        
    }
    //ID  ...
    NSMutableDictionary *dicObservation = [NSMutableDictionary new];
    
    
    
    if (dicFavo[@"category_id"]) {
        
        if (MarrAttach.count>0) {
            [dicObservation setValue:MarrAttach forKey:@"attachments"];
        }
        
        [dicObservation setValue:dicFavo[@"category_id"] forKey:@"category"];
        
        if (dicFavo[@"specific"]) {
            [dicObservation setValue:dicFavo[@"specific"] forKey:@"specific"];
            
        }
        if (dicFavo[@"animal"]) {
            [dicObservation setValue:dicFavo[@"animal"] forKey:@"animal"];
            
        }
        [PublicationOBJ sharedInstance].observation = [dicObservation mutableCopy];
        [dicFavo setObject:dicObservation forKey:@"observation"];
        
    }
    if (arrAttach.count>0) {
        [dicFavo setObject:arrAttach forKey:@"attachments"];
        
    }
    
    [PublicationOBJ sharedInstance].arrReceivers = dicFavo[@"receivers"];
    //
    NSMutableArray  *arrReceivers=[[NSMutableArray alloc]init];
    
    if ([PublicationOBJ sharedInstance].arrReceivers) {
        [arrReceivers addObjectsFromArray:[PublicationOBJ sharedInstance].arrReceivers];
    }
    
    NSMutableDictionary *dicUpLoad =[NSMutableDictionary new];
    
    //Get data group from FAV selected:  eg: FULL dic group. {}
    
    NSMutableArray *arrFavGroups = [NSMutableArray arrayWithArray: dicFavo[@"groups"]];
    
    NSMutableArray *arrFavHunt = [NSMutableArray arrayWithArray: dicFavo[@"hunts"]];
    
    
    
    NSMutableArray *sharingGroupsArray = [NSMutableArray new];
    NSMutableArray *sharingHuntsArray = [NSMutableArray new];
    
    
    NSNumber *MDshare = (NSNumber*)dicFavo[@"sharing"][@"share"];
    if (arrFavGroups.count>0) {
        
        for (int i = 0; i<arrFavGroups.count; i++) {
            NSDictionary *dicGroup = arrFavGroups[i];
            
            [sharingGroupsArray addObject:@{@"groupID":dicGroup[@"id"],
                                            @"categoryName":dicGroup[@"name"],
                                            @"isSelected":[NSNumber numberWithBool:TRUE]}];
        }
        
        [dicUpLoad setValue:sharingGroupsArray forKey:@"sharingGroupsArray"];
        [dicFavo setObject:dicUpLoad[@"sharingGroupsArray"] forKey:@"sharingGroupsArray"];
        
    }
    
    
    if (arrFavHunt.count>0) {
        for (int i = 0; i<arrFavHunt.count; i++) {
            NSDictionary *dicHunt = arrFavHunt[i];
            
            [sharingHuntsArray addObject:@{@"huntID":dicHunt[@"id"],
                                           @"categoryName":dicHunt[@"name"],
                                           @"isSelected":[NSNumber numberWithBool:TRUE]}];
        }
        
        [dicUpLoad setValue:sharingHuntsArray forKey:@"sharingHuntsArray"];
        [dicFavo setObject:dicUpLoad[@"sharingHuntsArray"] forKey:@"sharingHuntsArray"];
    }
    //user
    NSMutableArray *arrFavUsers = [NSMutableArray arrayWithArray: dicFavo[@"sharingUsers"]];
    
    NSMutableArray *arrTmp = [NSMutableArray new];
    for (NSDictionary *dicPersonne in arrFavUsers) {
        [arrTmp addObject:dicPersonne[@"id"]];
    }
    NSString *strUser = @"";
    if (arrTmp.count > 0) {
        strUser = [arrTmp componentsJoinedByString:@","];
        [dicUpLoad setValue:strUser forKey:@"sharingUsers"];
    }
    //
    if (arrReceivers.count>0) {
        [dicUpLoad setValue:arrReceivers forKey:@"receivers"];
    }
    if (MDshare) {
        [dicUpLoad setValue:MDshare forKey:@"iSharing"];
        
    }
    else
    {
        [dicUpLoad setValue:@0 forKey:@"iSharing"];
        
    }
    
    [dicFavo setObject:dicUpLoad[@"iSharing"] forKey:@"iSharing"];
    [PublicationOBJ sharedInstance].dicFavoris =dicFavo;
    
    [[PublicationOBJ sharedInstance] uploadData:dicUpLoad];
    
    
    
    Publication_Niv6_Validation *viewController1 = [[Publication_Niv6_Validation alloc] initWithNibName:@"Publication_Niv6_Validation" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}
@end
