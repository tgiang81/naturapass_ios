//
//  MurVC.h
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "MurBaseVC.h"
#import "CarterFilterView.h"
#import "ShortcutScreenVC.h"
@interface MurVC : MurBaseVC
{
    IBOutlet UISwitch *jtSwitch;
    __weak IBOutlet UILabel *lbFilter1;
    __weak IBOutlet UILabel *lbFilter2;

}


@property(nonatomic,strong) NSString *strMesGroupFilter;
@property(nonatomic,strong) NSString *strMesHuntFilter;
@property (nonatomic,strong) IBOutlet UILabel *warning;
@property (weak, nonatomic) IBOutlet UIButton *btnShortCut;
@property (weak, nonatomic) IBOutlet UIView *vViewFilter;
@property(nonatomic,strong) NSString *strFilterType;
@property(nonatomic,strong) NSString *strFilterMoi;
@property(nonatomic,strong) NSString *strFilterAmis;
@property(nonatomic,strong) NSString *strFilterPersonne;
@property(nonatomic,strong) NSString *strFilterDebut;
@property(nonatomic,strong) NSString *strFilterFin;
@property (nonatomic, strong) NSString *myC_Search_Text;
@property (nonatomic,retain) NSArray *arrCategoriesFilter;

@property (nonatomic,strong) CarterFilterView* viewFilter;
@property (nonatomic,strong) ShortcutScreenVC* viewShortCut;

@end
