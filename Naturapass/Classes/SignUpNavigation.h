//
//  BaseNavigation.h
//  NaturapassMetro
//
//  Created by Giang on 7/22/15.
//  Copyright (c) 2015 PHS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignUpNavigation : UIViewController
@property (nonatomic, strong) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIView *viewNavigation1;
@property (nonatomic, strong) IBOutlet UIButton *btnBack;

@end
