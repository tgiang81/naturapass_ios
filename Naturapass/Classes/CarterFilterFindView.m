//
//  CarterFilterFindView.m
//  Naturapass
//
//  Created by Manh on 10/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "CarterFilterFindView.h"
#import "Filter_Cell_Type1.h"
#import "Filter_Cell_Type2.h"
#import "TreeViewNode.h"
#import "DatabaseManager.h"
#import "NSDate+Extensions.h"
#import "NSString+HTML.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";
//header cell
static NSString *identifierHeader3 = @"identifierHeader3";
@implementation CarterFilterFindView

#pragma mark - INIT

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self instance];
    }
    return self;
}
-(instancetype)initWithEVC:(BaseVC*)vc expectTarget:(ISSCREEN)expectTarget isLiveHunt:(BOOL)isLiveHunt;
{
    self = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] objectAtIndex:0] ;
    if (self) {
        _expectTarget = expectTarget;
        _parentVC = vc;
        _isLiveHunt =isLiveHunt;
        [self instance];
    }
    return self;
}

-(void)instance
{
    [self.btnCancel setTitle:str(strAnuler) forState:UIControlStateNormal];
    [self.btnValider setTitle:str(strValider) forState:UIControlStateNormal];
    switch (_expectTarget) {
        case ISMUR:
        {
            self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            [self.btnSearch setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
        }
            break;
        case ISGROUP:
        {
            self.colorNavigation = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            [self.btnSearch setBackgroundColor:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];

        }
            break;
        case ISLOUNGE:
        {
            if (_isLiveHunt) {
                self.colorNavigation = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                self.lbTitleScreen.textColor = UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR);
                [self.btnSearch setBackgroundColor:UIColorFromRGB(LIVE_MAP_MAIN_BAR_COLOR)];
            }
            else
            {
                self.colorNavigation = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.btnValider setBackgroundColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
                [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
                self.lbTitleScreen.textColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
                [self.btnSearch setBackgroundColor:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
            }
            
        }
            break;
        default:
        {
            self.colorNavigation = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.btnValider setBackgroundColor:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
            [self.btnCancel setBackgroundColor:UIColorFromRGB(MUR_CANCEL)];
            self.lbTitleScreen.textColor = UIColorFromRGB(CARTE_MAIN_BAR_COLOR);
            [self.btnSearch setBackgroundColor:UIColorFromRGB(CARTE_MAIN_BAR_COLOR)];
            
        }
            break;
    }
    [self.btnValider setTitle:str(strValider) forState:UIControlStateNormal];
    [self.btnCancel setTitle:str(strAnuler) forState:UIControlStateNormal];
    
    //
    [self.tfMotCle.layer setMasksToBounds:YES];
    self.tfMotCle.layer.cornerRadius= 4.0;
    self.tfMotCle.layer.borderWidth =0.5;
    self.tfMotCle.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type1" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection2];
    //header
    [self.tableControl registerNib:[UINib nibWithNibName:@"HeaderSearchPersonnesCell" bundle:nil] forCellReuseIdentifier:identifierHeader3];
    
    self.tableControl.estimatedRowHeight = 66;
    arrData = [NSMutableArray new];
    arrDataFull = [NSMutableArray new];
    arrDataSelected = [NSMutableArray new];
    
    [self.tfMotCle addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    
    __weak typeof(self) weakVC = self;
    serviceAPI = [WebServiceAPI new];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        [weakVC performSelectorOnMainThread:@selector(reloadNewData:) withObject:response waitUntilDone:YES];
        
    };
    
}
-(void)addContraintSupview:(UIView*)viewSuper
{
    UIView *view = self;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    view.frame = viewSuper.frame;
    
    [viewSuper addSubview:view];
    
    [viewSuper addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeLeading
                               relatedBy:NSLayoutRelationEqual
                               toItem:viewSuper
                               attribute:NSLayoutAttributeLeading
                               multiplier:1.0 constant:0]];
    [viewSuper addConstraint: [NSLayoutConstraint
                               constraintWithItem:view attribute:NSLayoutAttributeTrailing
                               relatedBy:NSLayoutRelationEqual
                               toItem:viewSuper
                               attribute:NSLayoutAttributeTrailing
                               multiplier:1.0 constant:0] ];
    [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view]-(0)-|"
                                                                      options:0
                                                                      metrics:nil
                                                                        views:NSDictionaryOfVariableBindings(view)]];
    
}
-(void)hide:(BOOL)hidden
{
    [self endEditing:YES];
    if (hidden) {
        _contraintLeading.constant = 375;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"HIDE");
             self.hidden = hidden;
         }];
    }
    else
    {
        self.hidden = hidden;
        _contraintLeading.constant = 70;
        [UIView animateWithDuration:0.5
                              delay:0.1
                            options: 0
                         animations:^
         {
             [self layoutIfNeeded]; // Called on parent view
         }
                         completion:^(BOOL finished)
         {
             
             NSLog(@"SHOW");
         }];
    }
    
}
-(void)setup
{
    self.hidden = YES;
    _contraintLeading.constant = 375;
}
-(void)buttonValiderWithHide:(BOOL)hide
{
    if (hide) {
        _contraintHeghtValider.constant = 0;
        _btnValider.hidden = YES;
    }
    else
    {
        _contraintHeghtValider.constant = 40;
        _btnValider.hidden = NO;
    }
}

- (void) GetUsersSearchAction:(NSString*)searchString
{
    if (searchString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];
        
    }
    else
    {
        [COMMON removeProgressLoading];
        [COMMON addLoadingForView:self];
        [serviceAPI getUsersSearchAction:searchString];
    }
    
    
}
-(void)reloadNewData:(id) response
{
    [self endEditing:YES];
    [arrDataFull removeAllObjects];
    for (NSDictionary*kDic in [response objectForKey:@"users"]) {
        BOOL isCheck = NO;
        for (NSDictionary *dicSelected in arrDataSelected) {
            if ([dicSelected[@"id"] intValue] == [kDic[@"id"] intValue]) {
                isCheck = YES;
            }
        }
        NSMutableDictionary *dicGG = [@{@"id":kDic[@"id"],
                                        @"name":kDic[@"fullname"],
                                        @"status":@(isCheck),
                                        @"type": @(6)} copy];
        [arrDataFull addObject:dicGG];
    }
    arrData =[arrDataFull mutableCopy];
    [self.tableControl reloadData];
    
}
//MARK: -DATA
-(void)fnTreeNode:(TreeViewNode*)node
{
    _node = node;
    [arrDataFull removeAllObjects];
    [arrData removeAllObjects];
    [arrDataSelected removeAllObjects];
    switch ([_node.nodeObject[@"filter_type"] intValue] ) {
        case FILTER_GROUP:
        {
            _lbTitleScreen.text = @"Rechercher des groups";
            _tfMotCle.placeholder = @"Groups";
            [self buttonValiderWithHide: NO];

        }
            break;
        case FILTER_AGENDA:
        {
            _lbTitleScreen.text = @"Rechercher des agendas";
            _tfMotCle.placeholder = @"Agendas";
            [self buttonValiderWithHide: NO];

        }
            break;
        case FILTER_PERSION:
        {
            _lbTitleScreen.text = @"Rechercher des personnes";
            _tfMotCle.placeholder = @"Personnes";
            [self buttonValiderWithHide: NO];

        }
            break;
        default:
            break;
    }
    for (TreeViewNode *dicChild in node.nodeChildren) {
        if ([dicChild.nodeObject[@"status"] boolValue]) {
            [arrDataSelected addObject:dicChild.nodeObject];
        }
    }
    
    //get list all
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //list shared mes groups
        NSMutableArray *arrFull = [NSMutableArray new];
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        switch ([_node.nodeObject[@"filter_type"] intValue] ) {
            case FILTER_GROUP:
            {
                NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE (c_admin=1 OR c_allow_show=1) AND c_user_id=%@ ",sender_id];
                
                FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
                
                
                while ([set_querry1 next])
                {
                    
                    int strID = [set_querry1 intForColumn:@"c_id"];
                    
                    //compare to get defaul
                    for (NSDictionary*kDic in arrTmp) {
                        if ([kDic[@"groupID"] intValue ]== strID) {
                            BOOL isCheck = NO;
                            for (NSDictionary *dicSelected in arrDataSelected) {
                                if ([dicSelected[@"id"] intValue] == strID) {
                                    isCheck = YES;
                                    break;
                                }
                            }
                            NSMutableDictionary *dicGG = [@{@"id":kDic[@"groupID"],
                                                            @"name":kDic[@"categoryName"],
                                                            @"type": @(6),
                                                            @"status":@(isCheck)} copy];
                            [arrFull addObject:dicGG];
                            break;
                        }
                    }
                    
                }
                
            }
                break;
            case FILTER_AGENDA:
            {
                //list shared mes chasses
                
                //                strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
                //
                //                NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
                
                //tb_hunt only return new mes agenda
                //tb_agenda return Old + New agenda + public + Semi-private Agenda
                //Show Agenda for filtering points. on Map/Wall
                //2-3 Normal or Admin user.
                
                //Don't show Agenda that expired!
                //get current time
                NSDate *date = [NSDate date];
                NSTimeInterval time = [date timeIntervalSince1970];
                //list shared mes chasses
                int current_time = (int)time;

                NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_hunt WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ AND c_end_date >= %d ",sender_id, current_time];

                FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
                
                
                while ([set_querry_hunt next])
                {
                    
                    int intID = [set_querry_hunt intForColumn:@"c_id"];
                    NSString* mID = [set_querry_hunt stringForColumn:@"c_id"];
                    
                    NSString* nameAgenda = [[set_querry_hunt stringForColumn:@"c_name"] stringByDecodingHTMLEntities];
                    
                    BOOL isCheck = NO;
                    for (NSDictionary *dicSelected in arrDataSelected) {
                        if ([dicSelected[@"id"] intValue] == intID) {
                            isCheck = YES;
                            break;
                        }
                    }
                    
                    NSMutableDictionary *dicGG = [@{@"id":mID,
                                                    @"name":nameAgenda,
                                                    @"type": @(6),
                                                    @"status":@(isCheck)} copy];
                    [arrFull addObject:dicGG];
                    
                }
                
            }
                break;
            case FILTER_PERSION:
            {
                arrFull = [arrDataSelected mutableCopy];
            }
                break;
            default:
                break;
        }
        [self reloadData:arrFull];
    }];
    
    
    
}
-(void)reloadData:(NSArray*)arrNodes
{
    
    [arrDataFull removeAllObjects];
    [arrDataFull addObjectsFromArray:arrNodes];
    [arrData removeAllObjects];
    [arrData addObjectsFromArray:arrNodes];
    [self.tableControl reloadData];
}

- (void) processLoungesSearchingURL:(NSString *)searchText
{
    if (([_node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION)) {
        if (searchText.length > 0) {
            [self GetUsersSearchAction:searchText];
        }
        else
        {
            arrData =[arrDataSelected mutableCopy];
            [self.tableControl reloadData];
        }
        
    }
    else
    {
        if (searchText.length > 0) {
            NSString *predicateString = [NSString stringWithFormat:@"name contains[c] '%@'", searchText];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateString];
            arrData =[[arrDataFull filteredArrayUsingPredicate:predicate] mutableCopy];
        }
        else
        {
            arrData =[arrDataFull mutableCopy];
        }
        [self.tableControl reloadData];
    }
}
-(void)textChanged:(UITextField *)textField
{
    if (!([_node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION)) {
        [self processLoungesSearchingURL: textField.text];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self endEditing:YES];
    if ([_node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION) {
        [self processLoungesSearchingURL:textField.text];
    }
    return YES;
    
}
//MARK: - CALLBACK
-(void)setCallback:(CarterFilterFindViewCallback)callback
{
    _callback = callback;
}
//MARK: - ACTION
-(IBAction)searchAction:(id)sender
{
    [self endEditing:YES];
    [self processLoungesSearchingURL:self.tfMotCle.text];
}
-(IBAction)bacButtonAction:(id)sender
{
    [self hide:YES];
}
-(IBAction)validerAction:(id)sender
{
    NSMutableArray *arrChild = [ NSMutableArray new];
    if([_node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION)
    {
        arrDataFull = arrDataSelected;
        
    }
    for (NSDictionary *dic in arrDataFull) {
        if ([dic[@"status"] boolValue]) {
            NSMutableDictionary *dicTmp = [dic mutableCopy];
            [dicTmp setObject:@(6) forKey:@"type"];
            TreeViewNode *nodeChildQUI6 = [TreeViewNode new];
            nodeChildQUI6.nodeObject = dicTmp;
            
            nodeChildQUI6.parent = _node;
            [arrChild addObject:nodeChildQUI6];
        }
    }
    if (!_node.nodeChildren) {
        _node.nodeChildren = [NSMutableArray new];
    }
    _node.nodeChildren = arrChild;
    //callback
    if (_callback) {
        _callback(_node);
    }
    
    [self hide:YES];
}
-(IBAction)switchValueChanged:(id)sender
{
    [self hide:YES];
    
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic = arrData[indexPath.row];
    Filter_Cell_Type2 *cell = (Filter_Cell_Type2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
    cell.lbTitle.text = dic[@"name"];
    cell.imgArrow.hidden = YES;
    cell.accessoryType = [dic[@"status"] boolValue] ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    //Status
    cell.backgroundColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //select
    NSMutableDictionary *dicTmp = [arrData[indexPath.row] mutableCopy];
    [dicTmp setObject:@(![dicTmp[@"status"] boolValue]) forKey:@"status"];
    [arrData replaceObjectAtIndex:indexPath.row withObject:dicTmp];
    
    for (int i = 0; i < arrDataFull.count; i++) {
        NSDictionary *dic = arrDataFull[i];
        if ([dic[@"id"] intValue] == [dicTmp[@"id"] intValue]) {
            [arrDataFull replaceObjectAtIndex:i withObject:dicTmp];
            break;
        }
    }
    
    
    if([_node.nodeObject[@"filter_type"] intValue] == FILTER_PERSION)
    {
        BOOL isCheck = NO;
        for (int i = 0; i < arrDataSelected.count; i++) {
            NSDictionary *dicSelected = arrDataSelected[i];
            if ([dicSelected[@"id"] intValue] == [dicTmp[@"id"] intValue]) {
                isCheck = YES;
                if ([dicTmp[@"status"] boolValue]) {
                    [arrDataSelected replaceObjectAtIndex:i withObject:dicTmp];
                }
                else
                {
                    [arrDataSelected removeObjectAtIndex:i];
                }
                break;
            }
        }
        if (!isCheck && [dicTmp[@"status"] boolValue]) {
            [arrDataSelected addObject:dicTmp];
        }
        [self validerAction:nil];
    }
    [self.tableControl reloadData];
    
}

@end
