//
//  MultipleChooseMediaCell.h
//  Naturapass
//
//  Created by manh on 12/27/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultipleChooseMediaCell : UITableViewCell
@property (weak, nonatomic)     IBOutlet UIImageView *imgBackGroundNamePdf;
@property (weak, nonatomic)     IBOutlet UIImageView *imgButtonClosePdf;
@property (weak, nonatomic)     IBOutlet UILabel *lbNamePdf;
@property (weak, nonatomic)     IBOutlet UIButton *btnClose;

@end
