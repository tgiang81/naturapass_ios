//
//  ChatNouveauVC.m
//  Naturapass
//
//  Created by Giang on 9/21/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChatNouveauVC.h"
#import "GroupCreateCell_Step4.h"
#import "ChatVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString *GROUPCREATECELLSTEP4 = @"GroupCreateCell_Step4";
static NSString *CELLID4 = @"cellid4";

#import "SubNavigationPRE_Search.h"

@interface ChatNouveauVC () <UITextFieldDelegate>
{
    UITextField *myTextField;
    NSMutableArray *arrFriend, *arrCommon;
    NSMutableArray *searchedArray, * selectedUsers;
    WebServiceAPI *serviceAPI;

    UITapGestureRecognizer *tapGesture;
    IBOutlet UIButton *btnSuivant;
}
@end

@implementation ChatNouveauVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [btnSuivant setTitle: str(strSUIVANT) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
    [self.subview removeFromSuperview];
    
    
    [self addSubNav:@"SubNavigationPRE_Search"];
    
    SubNavigationPRE_Search *okSubView = (SubNavigationPRE_Search*)self.subview;
    
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    
    btn1.backgroundColor = UIColorFromRGB(DISCUSSION_BACK);
    
    UITextField *btn2 = (UITextField*)[self.subview viewWithTag:START_SUB_NAV_TAG+1];
    btn2.delegate = self;
    myTextField = btn2;
    btn2.placeholder =str(strRecherchezQuelquun);
    btn2.autocorrectionType = UITextAutocorrectionTypeNo;
    
    UIButton *btn3 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+2];
    [btn3 addTarget:self action:@selector(fnSearch) forControlEvents:UIControlEventTouchUpInside];

    [[NSNotificationCenter defaultCenter]  addObserver:self
                                              selector:@selector(textFieldDidChange:)
                                                  name:UITextFieldTextDidChangeNotification
                                                object:btn2];
    
    UIImageView *icon_search = (UIImageView*)[okSubView viewWithTag:1001];
    [icon_search setImage:[UIImage imageNamed:@"discussion_ic_search"]];
    
    [self textFieldDidChange:nil];

    arrFriend = [NSMutableArray new];
    arrCommon = [NSMutableArray new];
    selectedUsers = [NSMutableArray new];
    searchedArray = [NSMutableArray new];
    [self.tableControl registerNib:[UINib nibWithNibName:GROUPCREATECELLSTEP4 bundle:nil] forCellReuseIdentifier:CELLID4];
    
    [COMMON addLoading:self];
    
    WebServiceAPI *friend_serviceAPI =[WebServiceAPI new];
    [friend_serviceAPI getTheFriends:YES withID:nil];
    __weak typeof(self) wself = self;
    
    friend_serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response]) return ;
        if (response!=nil) {
            if ([response isKindOfClass: [NSDictionary class]]) {
                
                NSArray *arrTmp = response[@"friends"] ;
                
                NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                             ascending:YES];
                //
                NSArray *results = [arrTmp sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
                
                
                for (NSDictionary*dic in results) {
                    
                    NSMutableDictionary *mutDic = [NSMutableDictionary dictionaryWithDictionary:dic];
                    [mutDic setValue: [NSNumber numberWithBool:NO]  forKey:@"status"];
                    
                    //the same
                    [arrFriend addObject:mutDic];
                    arrCommon = [arrFriend mutableCopy];
                }
                [self.tableControl reloadData];
            }
        }
        
    };
    
    //API Search
    serviceAPI = [WebServiceAPI new];
    
    __weak ChatNouveauVC *weakVC = self;
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        [weakVC performSelectorOnMainThread:@selector(reloadNewData:) withObject:response waitUntilDone:YES];
        
    };

}
-(void)reloadNewData:(id) response
{
    [searchedArray removeAllObjects];
    [searchedArray addObjectsFromArray:[response objectForKey:@"users"]];
    arrCommon = [searchedArray mutableCopy];
    [self.tableControl reloadData];
    
}
#pragma mark -API
- (void) GetUsersSearchAction:(NSString*)searchString
{
    if (searchString.length < VAR_MINIMUM_LETTRES) {
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];
        
    }
    else
    {
        [COMMON removeProgressLoading];
        [COMMON addLoading:self];
        [serviceAPI getUsersSearchAction:searchString];
    }
    
    
}
/*
 {
 friends =     (
 {
 courtesy = 1;
 firstname = Nicolas;
 fullname = "Nicolas Mendez";
 id = 3;
 lastname = Mendez;
 parameters =             {
 friend = 0;
 };
 photo = "/uploads/users/images/thumb/47c5b13dddc8c6e79df796beec5ceeb4677412d3.jpeg";
 relation =             {
 friendship =                 {
 state = 2;
 way = 2;
 };
 mutualFriends = 0;
 };
 usertag = "nicolas-mendez";
 }
 
 {
 term = "ba dua";
 total = 1;
 users =     (
 {
 firstname = "D\U01b0a";
 friendship = 0;
 fullname = "D\U01b0a Ba";
 id = 4880;
 lastname = Ba;
 mutualFriends = 0;
 profilepicture = "/uploads/users/images/thumb/f74c1043a0b39b1b4028e70054620c04ce7fb2e8.jpeg";
 state = 0;
 usertag = "dua-ba";
 }
 );
 }
 */
-(void) onSubNavClick:(UIButton *)btn{
//    [self viewWillDisappear:YES];
    
//    [super onSubNavClick:btn];
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://BACK
        {
            
            [self gotoback];
            
        }
            break;
            
        case 1:
        {
        }
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self addGesture];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //Validate next step
    [self removeGesture];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self fnSearch];
    return YES;
}

- (void)textFieldDidChange:(NSNotification *)info {
    if ([myTextField.text isEqualToString:@""]){
        [arrCommon removeAllObjects];
        arrCommon = [arrFriend mutableCopy];
        [self.tableControl reloadData];
    }
}
-(void)fnSearch
{
    if ([myTextField.text isEqualToString:@""]){
        [arrCommon removeAllObjects];
        arrCommon = [arrFriend mutableCopy];
        [self.tableControl reloadData];
    } else {
        NSString *strsearchValues=myTextField.text;
        strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        myTextField.text=strsearchValues;
        [self GetUsersSearchAction:myTextField.text];
    }
    
    [myTextField resignFirstResponder];
}
-(BOOL) checkInList :(NSDictionary*)dic
{
    BOOL isExist = NO;
    
    for (NSDictionary*mDic in selectedUsers) {
        if ([mDic[@"id"] intValue] == [dic[@"id"] intValue]) {
            isExist = YES;
            break;
        }
    }
    
    return isExist;
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrCommon count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:CELLID4 forIndexPath:indexPath];
    
    NSDictionary * dic = arrCommon[indexPath.row];
    
    [cell.lblTitle setText:dic[@"fullname"]];
    
    NSString *imgUrl = nil;
    
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [cell.imgViewAvatar sd_setImageWithURL:  [NSURL URLWithString:imgUrl  ]];
    
    if ([self checkInList:dic] == YES) {
        [cell.btnInvite setSelected:YES];
    }else{
        [cell.btnInvite setSelected:NO];
    }
    [cell.btnInviteOver addTarget:self action:@selector(addParticipal:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnInviteOver.tag = indexPath.row;
    
    [cell.btnInvite setTypeCheckBox:UI_DISCUSSION];
    return  cell;
}

#pragma mark - Action
-(IBAction)addParticipal:(UIButton *)sender{
    
    NSMutableDictionary * dic = [arrCommon[sender.tag] mutableCopy];
    //Add or remove...
    if ([self checkInList:dic] == YES) {
        
        [selectedUsers removeObject:dic];
        
        //remove
    }else{
        //Check user in the list? selected, If not add
        [selectedUsers addObject:dic];
    }
    
    [self.tableControl reloadData];
}

-(IBAction) fnSuivant
{
    [self.view endEditing:YES];
    if (selectedUsers.count == 0) {
        return;
    }
    ChatVC *viewController1 = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        viewController1.arrFriendDiscussion = selectedUsers;
        viewController1.expectTarget = ISDISCUSS;
        viewController1.IS_CREATE_SPECIAL = YES;
        viewController1.mParent = self.mParent;
    
    [(MainNavigationController *)self.navigationController pushViewController:viewController1 animated:NO completion:^ {
        NSLog(@"COMPLETED");
    }];
}
-(void)addGesture
{
    tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                        action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:tapGesture];


}
- (void)handleTapGesture:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    [self removeGesture];
}
-(void)removeGesture
{
    [self.view removeGestureRecognizer:tapGesture];
}
@end
