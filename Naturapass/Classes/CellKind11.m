//
//  CellKind1.m
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CellKind11.h"

@implementation CellKind11

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    [COMMON listSubviewsOfView:self];

    [self.imageIcon.layer setMasksToBounds:YES];
    self.imageIcon.layer.cornerRadius= 30;
    self.imageIcon.layer.borderWidth =0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
