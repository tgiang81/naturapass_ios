//
//  MurEditPublicationVC.m
//  Naturapass
//
//  Created by Giang on 11/6/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "MurEditPublicationVC.h"
#import "CellKind20.h"
#import "PublicationVC.h"
#import "Publication_Carte.h"
#import "Publication_Legende_Carte.h"
#import "Publication_ChoixSpecNiv1.h"
#import "Publication_Choix_Partage.h"
#import "Publication_Color.h"
#import "Publication_Favoris_System.h"
#import "Publication_ChoixSpecNiv5.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface MurEditPublicationVC ()
{
    NSMutableArray * arrData;
    IBOutlet UILabel *lbTitle;
    NSDictionary *childCategory;
    NSArray *modifiAttachment;
    
}
@end

@implementation MurEditPublicationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strMODIFIER_VOTRE_PUBLICATION);
    UINavigationBar *navBar=self.navigationController.navigationBar;
    switch (self.expectTarget) {
        case ISMUR:
        case ISCARTE:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            //Change status bar color
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strMUR)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            
            [self addSubNav:@"SubNavigationMUR"];
            
            
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
        }
            break;
        case ISLOUNGE:
        {
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    self.dicOption =@{@"admin":@1};
                    
                }else{
                    self.dicOption =@{@"admin":@0};
                    
                }
                
            }else{
                self.dicOption =@{@"admin":@0};
            }
            
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCHANTIERS)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
            
            
            if (self.needRemoveSubItem) {
                [self removeItem:4];
            }
            
        }
            break;
        default:
            break;
    }
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind20" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    UINavigationBar *navBar=self.navigationController.navigationBar;
    NSDictionary *dic = nil;
    
    switch (self.expectTarget) {
        case ISMUR:
        case ISCARTE:
        {
            //Change status bar color
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            
        }
            break;
        case ISLOUNGE:
        {
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            //Non admin
            if ([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"] isKindOfClass: [NSDictionary class]]) {
                if([[GroupEnterOBJ sharedInstance].dictionaryGroup[@"connected"][@"access"]isEqual:@3])
                {
                    //admin
                    dic =@{@"admin":@1};
                    
                }else{
                    dic =@{@"admin":@0};
                    
                }
                
            }else{
                dic =@{@"admin":@0};
            }
            
        }
            break;
        default:
            break;
    }
    
    [self setThemeNavSub:NO withDicOption:dic];
    
    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    [self setColorTextNavSubWithIndex:((TEXT_START_SUB_NAV_TAG-START_SUB_NAV_TAG)*2+ (int)btn.tag)];
    
    
    // Do any additional setup after loading the view from its nib.
    arrData = [NSMutableArray new];
    NSMutableDictionary *dic1 = [@{@"name": @"Texte",
                                   @"type":@"0"} copy];
    NSMutableDictionary *dic2 = [@{@"name":@"Géolocalisation",
                                   @"type":@"1"} copy];
    NSMutableDictionary *dic3 = [@{@"name":@"Légende",
                                   @"type":@"2"} copy];
    NSMutableDictionary *dic4 = [@{@"name":@"Précisions",
                                   @"type":@"3"} copy];
    NSMutableDictionary *dic5 = [@{@"name":@"Partage",
                                   @"type":@"4"} copy];
    NSMutableDictionary *dic6 = [@{@"name":@"Couleur",
                                   @"type":@"5"} copy];
    NSMutableDictionary *dic7 = [@{@"name":@"Observation",
                                   @"type":@"6"} copy];


    [arrData addObject:dic1];
    [arrData addObject:dic2];
    if ([PublicationOBJ sharedInstance].latitude || [PublicationOBJ sharedInstance].longtitude) {
        [arrData addObject:dic3];
        [arrData addObject:dic6];
    }
    //check observation and attachment -> goto 5
    childCategory = nil;
    NSArray *arrOb = [PublicationOBJ sharedInstance].dicEditer[@"observations"];
    if (arrOb.count>0) {
        //get data with categrogy_id
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        for (NSDictionary*nDic in [PublicationOBJ sharedInstance].treeCategory) {
            [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
            if (childCategory != nil) {
                break;
            }
        }

        if (childCategory == nil) {
            // neu duyet trong tree default không có thì sẽ duyệt trong các tree còn lại
            BOOL isFinish = false;
            for (NSString *strKey in treeDic.allKeys) {
                if (![strKey isEqualToString:@"default"]) {
                    NSArray *arrTreeCategory = treeDic[strKey];
                    for (NSDictionary*nDic in arrTreeCategory) {
                        [self getDataTree:nDic withCategoryID:[arrOb[0][@"category_id"] intValue]];
                        if (childCategory != nil) {
                            isFinish =TRUE;
                            break;
                        }
                    }
                }
                if (isFinish == TRUE) {
                    break;
                }
            }
        }
        //check if group exist ->
        if (![[PublicationOBJ sharedInstance] checkShowCategory:childCategory[@"groups"]]) {
           childCategory = nil;
        }
        
        if (childCategory != nil) {
            if (childCategory[@"card"] || [childCategory[@"search"] boolValue] ) {
                //show modife form
                [arrData addObject:dic7];
            }
        }
    }

    [arrData addObject:dic4];
    [arrData addObject:dic5];
    [self.tableControl reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)checkAttachment
{
    if ([PublicationOBJ sharedInstance].latitude.length>0 && [PublicationOBJ sharedInstance].longtitude.length>0 && [COMMON isReachable])
    {
        //But bad network?
        
        [COMMON addLoading:self];
        WebServiceAPI *serviceObj = [WebServiceAPI new];
        [serviceObj getCategoriesGeolocated:[PublicationOBJ sharedInstance].latitude strLng:[PublicationOBJ sharedInstance].longtitude];
        
        serviceObj.onComplete = ^(NSDictionary*response1, int errCode){
            //Has Data
            if (![response1[@"model"] isKindOfClass: [NSNull class]])
            {
                
                if ([response1[@"model"] isKindOfClass: [NSString class]])
                {
                    if ([response1[@"model"] isEqualToString:@"default"])
                    {
                        [COMMON removeProgressLoading];
                        
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                        
                        [self doNext];
                        // get tree/card from tree cache default
                    }
                    else
                    {
                        // Receiver_1....                                     "model": "receiver_7"
                        NSString *strReceiver = response1[@"model"];
                        
                        //check in the cache
                        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                        
                        if (treeDic[strReceiver] != [NSNull class] && treeDic[strReceiver])
                        {
                            [COMMON removeProgressLoading];
                            
                            [PublicationOBJ sharedInstance].treeCategory = treeDic[strReceiver];
                            [self doNext];
                        }
                        else //doesn't exist...
                        {
                            strReceiver = [strReceiver stringByReplacingOccurrencesOfString:@"_" withString:@"="];
                            
                            //receiver_7
                            
                            WebServiceAPI *serviceGetTree =[WebServiceAPI new];
                            serviceGetTree.onComplete =^(id response2, int errCode){
                                [COMMON removeProgressLoading];
                                
                                if ([response2 isKindOfClass: [NSDictionary class]]) {
                                    
                                    //if not contained -> request get new tree... then merge result to the cache
                                    [[PublicationOBJ sharedInstance] doMergeTreeWithNew:response2];
                                    //Get needed tree
                                    
                                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                                    
                                    [PublicationOBJ sharedInstance].treeCategory = treeDic[response1[@"model"]];
                                    [self doNext];
                                }
                                
                            };
                            
                            [serviceGetTree fnGET_CATEGORIES_BY_RECEIVER:strReceiver];
                            
                        }
                    }
                }
                else if ([response1[@"model"] isKindOfClass: [NSDictionary  class]])
                {
                    //a real tree. => merge..
                    [COMMON removeProgressLoading];
                    
                }
                //A TREE
                else if ([response1[@"tree"] isKindOfClass: [NSArray  class]])
                {
                    [COMMON removeProgressLoading];
                    [PublicationOBJ sharedInstance].treeCategory = response1[@"tree"];
                    [self doNext];
                    
                    return;
                }else{
                    [COMMON removeProgressLoading];
                    //Offline
                    NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                    
                    [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                    [self doNext];
                    
                }
            }else{
                [COMMON removeProgressLoading];
                
                //Offline
                NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
                
                [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
                [self doNext];
                
            }
            
        };
        
    }else{
        NSDictionary*treeDic = [[PublicationOBJ sharedInstance] getCache_Tree];
        
        [PublicationOBJ sharedInstance].treeCategory = treeDic[@"default"];
        [self doNext];
        
    }
}
-(void)doNext
{

}
-(void) getDataTree:(NSDictionary*) trDic withCategoryID:(int)categoryID
{
    
    if ([trDic[@"id"] intValue] == categoryID ) {
        childCategory = trDic;
        return;
    }else{
        
        for (NSDictionary*nDic in trDic[@"children"]) {
            [self getDataTree:nDic withCategoryID:categoryID];
        }
    }
}

#pragma SUB - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    
    switch (self.expectTarget) {
            //special
        case ISCARTE:
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        default:
            break;
    }
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind20 *cell = nil;
    
    cell = (CellKind20 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    
    //FONT
    cell.name.text = dic[@"name"];
    [cell.name setTextColor:[UIColor blackColor]];
    [cell.name setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    Publication_Favoris_System *viewController1 = [[Publication_Favoris_System alloc] initWithNibName:@"Publication_Favoris_System" bundle:nil];
//    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
//    return;
//    
    
    NSDictionary *dic = arrData[indexPath.row];

    switch ([dic[@"type"] intValue]) {
        case EDIT_TEXT:
        {
            PublicationVC *viewController1 = [[PublicationVC alloc] initWithNibName:@"PublicationVC" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
        }
            break;
            
        case EDIT_CARTE:
        {
            Publication_Carte *viewController1 = [[Publication_Carte alloc] initWithNibName:@"Publication_Carte" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
            break;
            
        case EDIT_LEGEND:
        {
            
            Publication_Legende_Carte *viewController1 = [[Publication_Legende_Carte alloc] initWithNibName:@"Publication_Legende_Carte" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            
        }
            break;
            
        case EDIT_PRECISIONS:
        {
            
            Publication_ChoixSpecNiv1 *viewController1 = [[Publication_ChoixSpecNiv1 alloc] initWithNibName:@"Publication_ChoixSpecNiv1" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
            break;
        case EDIT_PARTAGE:
        {
            Publication_Choix_Partage *viewController1 = [[Publication_Choix_Partage alloc] initWithNibName:@"Publication_Choix_Partage" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
            break;
        case EDIT_COLOR:
        {
            Publication_Color *viewController1 = [[Publication_Color alloc] initWithNibName:@"Publication_Color" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
        }
            break;
        case EDIT_ATTACHMENT:
        {
            Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
            viewController1.modifiAttachment = YES;
            viewController1.myDic = childCategory;
            viewController1.iSpecific =  [childCategory[@"search"] intValue] ;
            if ([childCategory[@"search"] boolValue] && [[PublicationOBJ sharedInstance] getCacheSpecific_Cards].count > 0) {
                viewController1.myCard = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
                viewController1.id_animal = [childCategory[@"id"] stringValue];
            }
            [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
            //fill data ....

        }
            break;
        default:
            break;
    }
    
}


@end
