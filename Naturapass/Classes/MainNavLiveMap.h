//
//  MainNavMUR.h
//  Naturapass
//
//  Created by Giang on 7/28/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

//Main NAV for MUR...GROUP...

#import "MainNavigationBaseView.h"
@interface MainNavLiveMap : MainNavigationBaseView

@property (weak, nonatomic) IBOutlet UILabel *lbHuntName;
@property (weak, nonatomic) IBOutlet UILabel *lbHuntTime;

@end
