//
//  SignalerBaseVC.h
//  Naturapass
//
//  Created by JoJo on 9/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@interface SignalerBaseVC : BaseVC

@property(nonatomic,strong)  UIColor *colorBackGround;
@property(nonatomic,strong)  UIColor *colorNavigation;
@property(nonatomic,strong)  UIColor *colorCellBackGround;
@property(nonatomic,strong)  UIColor *colorAnnuler;
@property(nonatomic,strong)  UIColor *colorAttachBackGround;
@property(nonatomic,strong)  UIColor *colorAttachCellBackGround;

-(void)gobackParent;
@end
