//
//  AmisListVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisListVC.h"
#import "CellKind2.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface AmisListVC ()
{
    NSMutableArray *arrFriendList, *searchedArray;
    IBOutlet UILabel*lbAmisCount;
    IBOutlet UISearchBar                *toussearchBar;
}

@property (nonatomic, assign) BOOL shouldBeginEditing;

@end

@implementation AmisListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrFriendList = [NSMutableArray new];
    searchedArray = [NSMutableArray new];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    [self initRefreshControl];
    
    toussearchBar.placeholder = str(strRechercher);
    
    //load cache
    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],MY_FRIEND_SAVE) ];
    NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
    if (arrTmp.count > 0) {
        [arrFriendList  addObjectsFromArray:arrTmp];

        [lbAmisCount setText: [NSString stringWithFormat: @"%@ (%lu)",str(strAMIS), (unsigned long)arrFriendList.count ]];

        [self.tableControl reloadData];
    }else{
        [self startRefreshControl];
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [toussearchBar setTintColor:UIColorFromRGB(AMIS_MAIN_BAR_COLOR)];
}

- (void)insertRowAtTop {
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj getUserFriendsAction];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        
        if ([wself fnCheckResponse:response]) return ;
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        [arrFriendList removeAllObjects];
        
        NSArray *arrTmp = response[@"friends"] ;
        
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"fullname"
                                                                     ascending:YES];
        //
        NSArray *results = [arrTmp sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
        
        //save
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],MY_FRIEND_SAVE)   ];
        [arrFriendList addObjectsFromArray:results];
        
        [arrFriendList writeToFile:strPath atomically:YES];
        
        [lbAmisCount setText: [NSString stringWithFormat: @"%@ (%lu)",str(strAMIS), (unsigned long)arrFriendList.count ]];
        
        
        [self.tableControl reloadData];
    };
    
}

- (void)insertRowAtBottom {
    [self stopRefreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.shouldBeginEditing) {
        return searchedArray.count;
    }
    
    return arrFriendList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind2 *cell = nil;
    
    cell = (CellKind2 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    NSDictionary *dic = nil;
    
    if (self.shouldBeginEditing) {
        dic = searchedArray[indexPath.row];
        
    }else{
        dic = arrFriendList[indexPath.row];
    }
    
    NSString *imgUrl = nil;
    
    
    if (dic[@"profilepicture"] != nil) {
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"profilepicture"]];
    }else{
        imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    }
    
    [cell.imageIcon sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
    
    cell.label1.text =dic[@"fullname"];
    
    //set fram image
    cell.constraint_image_width.constant = 40;
    cell.constraint_image_height.constant = 40;
    cell.label1.numberOfLines=1;
    
    cell.constraint_control_width.constant = 0;
    cell.constraintRight.constant = 0;
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //Goto Profile
    NSDictionary *dic = nil;
    
    if (self.shouldBeginEditing) {
        dic = searchedArray[indexPath.row];
        
    }else{
        dic = arrFriendList[indexPath.row];
    }
    
    
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic: dic];
    
    [self pushVC:friend animate:YES expectTarget:ISMUR];
}

-(void)killTextFocus {
    
    [toussearchBar resignFirstResponder];
}

- (void) processLoungesSearchingURL:(NSString *)searchText
{
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    toussearchBar.text=strsearchValues;
    
    [searchedArray removeAllObjects];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"fullname CONTAINS[c] %@", toussearchBar.text];
    NSArray *result = [arrFriendList filteredArrayUsingPredicate:predicate];
    
    searchedArray = [result mutableCopy];
    
    [self.tableControl reloadData];
    
}

#pragma mark- searBar


- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:0.07];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.shouldBeginEditing = NO;
    //return full...
    
    [self.searchDisplayController setActive:NO];
    [self.tableControl reloadData];
}

//do nothing...because stroking...searching
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    
    //reload table
    [searchedArray removeAllObjects];
    [self.tableControl reloadData];
}

@end
