//
//  ColorCollectionCell.h
//  Naturapass
//
//  Created by Manh on 11/18/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorCollectionCell : UICollectionViewCell
@property(nonatomic,strong) IBOutlet UIView *vColor;
@property(nonatomic,strong) IBOutlet UIImageView *imgCheck;

@end
