//
//  MyAlertView.h
//  Naturapass
//
//  Created by Giang on 2/29/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"

typedef void (^MyAlertViewCbk)(NSInteger index);

@interface VoisChiens_Document_Modifi_Alert : UIView <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *popTable;
}
@property (nonatomic,copy) MyAlertViewCbk callback;
@property (nonatomic,strong) IBOutlet UIView *viewBoder;

-(void)doBlock:(MyAlertViewCbk ) cb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBorder;
-(void)fnSetTitleWithArray:(NSArray*)arr;

@end


