//
//  CellKind23.h
//  Naturapass
//
//  Created by Giang on 3/7/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

typedef void (^callBackGroup) (NSInteger);
@interface CellKind23 : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label1;

@property (strong, nonatomic) IBOutlet UIImageView *rightIcon;
@property (strong, nonatomic) IBOutlet UIImageView *imgLine;

@property (weak, nonatomic) IBOutlet UIButton *btnDel;

@property (nonatomic, copy) callBackGroup CallBackGroup;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintRightIconWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintRightIconHeight;

@end
