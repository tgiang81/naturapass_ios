//
//  GroupCreate_Step1.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateBaseVC.h"

@interface GroupCreate_Step1 : GroupCreateBaseVC
{        
        //textfield
        IBOutlet UITextField    *nameTextField;
        IBOutlet UITextView     *descTextView;
        
        //label
        IBOutlet UILabel        *descTitleLabel;
        
        IBOutlet UIView             *viewActionsheetView;
        IBOutlet UILabel            *labelChoosePhoto;
        IBOutlet UIButton           *btnAvatar;
        IBOutlet UIButton           *btnCamera;
        IBOutlet UIButton           *btnLibrary;
        IBOutlet UIButton           *btnCancel;
        UIImage                     *userImage;
        
        

}
@end
