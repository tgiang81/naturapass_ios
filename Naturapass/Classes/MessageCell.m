//
//  MessageCell.m
//  Whatsapp
//
//  Created by Rafael Castro on 7/23/15.
//  Copyright (c) 2015 HummingBird. All rights reserved.
//

#import "MessageCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "NSString+EMOEmoji.h"
static int sizeAvarta =50;
static int deltalY =15;
static int spaceIconDelete =0;
static int arrow_chat_width =10;

@interface MessageCell ()<UITextViewDelegate>
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UIImageView *bubbleImage;
@property (strong, nonatomic) UIImageView *arrowImage;

@property (strong, nonatomic) UIImageView *statusIcon;
@property (strong, nonatomic) UIButton *warningButton;

@property (strong, nonatomic) UIImageView *avatarYou;
@property (strong, nonatomic) UILabel *nameLabel;
//@property (strong, nonatomic) UIImageView *deleteImage;


@end


@implementation MessageCell

-(CGFloat)height
{
    return _bubbleImage.frame.size.height;
}
-(void)updateMessageStatus
{
    [self buildCell];
    //Animate Transition
    _statusIcon.alpha = 0;
    [UIView animateWithDuration:.5 animations:^{
        _statusIcon.alpha = 1;
    }];
}

#pragma mark -

-(id)init
{
    self = [super init];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self commonInit];
    }
    return self;
}
-(void)commonInit
{
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    _textView = [[UITextView alloc] init];
    _textView.editable = NO;
    _textView.dataDetectorTypes = UIDataDetectorTypeLink;
    _textView.delegate = self;
    _bubbleImage = [[UIImageView alloc] init];
    _timeLabel = [[UILabel alloc] init];
    _statusIcon = [[UIImageView alloc] init];
    _avatarYou = [[UIImageView alloc] init];
    _nameLabel = [[UILabel alloc] init];

    
    _warningButton = [[UIButton alloc] init];
    _warningButton.hidden = YES;
    
    _btnInfo = [[UIButton alloc] init];
    _btnInfo.hidden = YES;

//    _deleteImage = [[UIImageView alloc] init];
    _arrowImage = [[UIImageView alloc] init];

    [self.contentView addSubview:_bubbleImage];
    [self.contentView addSubview:_textView];
    [self.contentView addSubview:_timeLabel];
    [self.contentView addSubview:_statusIcon];
    [self.contentView addSubview:_warningButton];
    [self.contentView addSubview:_avatarYou];
    [self.contentView addSubview:_btnInfo];
    [self.contentView addSubview:_nameLabel];
//    [self.contentView addSubview:_deleteImage];
    [self.contentView addSubview:_arrowImage];

    
}
-(void)prepareForReuse
{
    [super prepareForReuse];
    
    _textView.text = @"";
    _timeLabel.text = @"";
    _statusIcon.image = nil;
    _bubbleImage.image = nil;
    _warningButton.hidden = YES;
//    _deleteImage.image = nil;
    _arrowImage.image = nil;
    _nameLabel.text = @"";
    _avatarYou.image=nil;
    _btnInfo.hidden = YES;

}
-(void)setMessage:(Message *)message
{
    _message = message;
    [self buildCell];
    
    message.heigh = self.height;
}
-(void)buildCell
{
    [self setTextView];
    [self setTimeLabel];
    [self setBubble];
    
    [self addStatusIcon];
    [self setStatusIcon];
    
    //    [self setFailedButton];
    [self fbAvatarYou];
    [self setNameLabel];
    [self setNeedsLayout];
}

#pragma mark - TextView

-(void)setTextView
{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    CGFloat min_witdh =100;
    _textView.frame = CGRectMake(0, 0, max_witdh, MAXFLOAT);
    _textView.font = [UIFont fontWithName:@"Helvetica" size:17.0];
    _textView.backgroundColor = [UIColor clearColor];
    _textView.userInteractionEnabled = YES;
    
    NSString *emojiString = [_message.text emo_emojiString];
    NSString *trimmedString = [emojiString stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];

    
    _textView.text = trimmedString;
    [_textView sizeToFit];
    
    CGFloat textView_x;
    CGFloat textView_y;
    CGFloat textView_w = _textView.frame.size.width;
    CGFloat textView_h = _textView.frame.size.height;
    UIViewAutoresizing autoresizing;
    if (textView_w<min_witdh) {
        textView_w=min_witdh;
    }
    if (_message.sender == MessageSenderMyself)
    {
        _textView.textColor =[UIColor whiteColor];
        textView_x = self.contentView.frame.size.width - textView_w-15;
        textView_y = -deltalY;
        autoresizing = UIViewAutoresizingFlexibleLeftMargin;
    }
    else
    {
        _textView.textColor =[UIColor blackColor];
        textView_x = sizeAvarta+20;
        textView_y = -1;
        autoresizing = UIViewAutoresizingFlexibleRightMargin;
    }
    
    _textView.autoresizingMask = autoresizing;
    _textView.frame = CGRectMake(textView_x, textView_y+deltalY, textView_w, textView_h);
}
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    // Do whatever you want here
    NSLog(@"%@", URL); // URL is an instance of NSURL of the tapped link
    return YES; // Return NO if you don't want iOS to open the link
}

#pragma mark - TimeLabel

-(void)setTimeLabel
{
    _timeLabel.frame = CGRectMake(0, 0, 200, 14);
    _timeLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    _timeLabel.userInteractionEnabled = NO;
    _timeLabel.alpha = 0.7;
    _timeLabel.textAlignment = NSTextAlignmentLeft;
    
    CGFloat time_x;
    CGFloat time_y = _textView.frame.size.height-5;

    if (_message.sender == MessageSenderMyself)
    {
        _timeLabel.textColor = [UIColor whiteColor];
        time_y -= (deltalY - 5);
    }
    else
    {
        _timeLabel.textColor = [UIColor blackColor];

    }
    //Set Text to Label
    self.timeLabel.text = _message.date;
    
    //Set position
 
        time_x = _textView.frame.origin.x + 5;
    
    _timeLabel.frame = CGRectMake(time_x,
                                  time_y+deltalY ,
                                  _timeLabel.frame.size.width,
                                  _timeLabel.frame.size.height);
    
    _timeLabel.autoresizingMask = _textView.autoresizingMask;
}
-(BOOL)isSingleLineCase
{
    CGFloat delta_x = _message.sender == MessageSenderMyself?65.0:44.0;
    
    CGFloat textView_height = _textView.frame.size.height;
    CGFloat textView_width = _textView.frame.size.width;
    CGFloat view_width = self.contentView.frame.size.width;
    
    //Single Line Case
    return (textView_height <= 45 && textView_width + delta_x <= 0.8*view_width)?YES:NO;
}

#pragma mark - Bubble

- (void)setBubble
{

    //Margins to Bubble
    CGFloat marginLeft = 5;
    CGFloat marginRight = 2;
    
    //Bubble positionsa
    CGFloat bubble_x;
    CGFloat bubble_y = 0;
    CGFloat bubble_width;
    CGFloat bubble_height = MIN(_textView.frame.size.height + 15,
                                _timeLabel.frame.origin.y + _timeLabel.frame.size.height + 15);
    
    if (_message.sender == MessageSenderMyself)
    {
        
        bubble_x = _textView.frame.origin.x -marginLeft-spaceIconDelete;
        bubble_y -= (deltalY - 5) ;
        
        _bubbleImage.image = [[UIImage imageNamed:self.bg_chat]
                              stretchableImageWithLeftCapWidth:15 topCapHeight:14];
        
        
        bubble_width = self.contentView.frame.size.width - bubble_x - marginRight -arrow_chat_width ;
        //button close
//        _deleteImage.frame =CGRectMake(bubble_x + 10, (deltalY - 5), 12, 15);
//        _deleteImage.image = [UIImage imageNamed:@"ic_supprimer_white"];
        //arrow
        _arrowImage.frame =CGRectMake(bubble_x + bubble_width, bubble_y+deltalY+bubble_height -arrow_chat_width, arrow_chat_width, arrow_chat_width);
        _arrowImage.image = [UIImage imageNamed:self.arrow_chat];

    }
    else
    {
        bubble_x = marginRight;
        
        _bubbleImage.image = [[UIImage imageNamed:@"MD_friend_chat_bg"]
                              stretchableImageWithLeftCapWidth:21 topCapHeight:14];
        
        bubble_width = _textView.frame.origin.x + _textView.frame.size.width + marginLeft-sizeAvarta - arrow_chat_width;
        bubble_x+=sizeAvarta+arrow_chat_width -2;
        //button close
//        _deleteImage.image =nil;
        //arrow
        _arrowImage.frame =CGRectMake(sizeAvarta , bubble_y+deltalY-1, arrow_chat_width, arrow_chat_width);
        _arrowImage.image = [UIImage imageNamed:@"MD_friend_chat_bg_arrow"];

    }
    
    _bubbleImage.frame = CGRectMake(bubble_x, bubble_y+deltalY, bubble_width, bubble_height);
    _bubbleImage.autoresizingMask = _textView.autoresizingMask;
}

#pragma mark - StatusIcon

-(void)addStatusIcon
{
    CGRect time_frame = _timeLabel.frame;
    CGRect status_frame = CGRectMake(0, 0, 15, 14);
    status_frame.origin.x = time_frame.origin.x + time_frame.size.width + 5;
    status_frame.origin.y = time_frame.origin.y;
    _statusIcon.frame = status_frame;
    _statusIcon.contentMode = UIViewContentModeLeft;
    _statusIcon.autoresizingMask = _textView.autoresizingMask;
}
-(void)setStatusIcon
{
    if (self.message.status == MessageStatusSending)
        _statusIcon.image = [UIImage imageNamed:@"status_sending"];
    else if (self.message.status == MessageStatusSent)
        _statusIcon.image = [UIImage imageNamed:@"status_sent"];
    else if (self.message.status == MessageStatusReceived)
        _statusIcon.image = [UIImage imageNamed:@"status_notified"];
    else if (self.message.status == MessageStatusRead)
        _statusIcon.image = [UIImage imageNamed:@"status_read"];
    if (self.message.status == MessageStatusFailed)
        _statusIcon.image = nil;
    
    _statusIcon.hidden = _message.sender == MessageSenderSomeone;
}

#pragma mark - Failed Case

//
// This delta is how much TextView
// and Bubble should shit left
//
-(NSInteger)fail_delta
{
    return 60;
}
-(BOOL)isStatusFailedCase
{
    return self.message.status == MessageStatusFailed;
}
-(void)fbAvatarYou
{
    NSInteger b_size = sizeAvarta;
    CGRect frame = CGRectMake(5 ,
                              deltalY,
                              b_size,
                              b_size);
    
    _avatarYou.frame = frame;
    _avatarYou.hidden = (_message.sender == MessageSenderMyself);
    NSURL * url = [[NSURL alloc] initWithString:_message.imgAvatar];
    [_avatarYou sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_avatar"]];
        _avatarYou.layer.masksToBounds = YES;
        _avatarYou.layer.cornerRadius = frame.size.height/2;
    //add button
    _btnInfo.hidden = NO;
    [_btnInfo setFrame: frame];
}
-(void)setNameLabel
{
    CGFloat max_witdh = 0.7*self.contentView.frame.size.width;
    _nameLabel.frame = CGRectMake(5, 10, max_witdh, MAXFLOAT);
    _nameLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
    _nameLabel.backgroundColor = [UIColor clearColor];
    _nameLabel.userInteractionEnabled = NO;
    _nameLabel.textColor =[UIColor blackColor];
    _nameLabel.text = _message.name;
    [_nameLabel sizeToFit];
    _nameLabel.hidden = (_message.sender == MessageSenderMyself);

}
@end
