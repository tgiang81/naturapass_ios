//
//  Publication_Partage.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_Partage.h"
#import "Publication_Choix_Partage.h"
#import "Publication_Niv6_Validation.h"
#import "ServiceHelper.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreLocation/CoreLocation.h>

@interface Publication_Partage ()
{
    NSDictionary* currentData;
    IBOutlet UILabel *lbDescription1;
    IBOutlet UILabel *lbDescription2;
    IBOutlet UIButton *btnNON;
    IBOutlet UIButton *btnOUI;


}
@end

@implementation Publication_Partage

- (void)viewDidLoad {
    [super viewDidLoad];
    lbDescription1.text = str(strVoulezvous_partager_cette_publication);
    lbDescription2.text = str(strVous_garderez_cette_publication_pour);
    [btnNON setTitle:str(strNON)  forState:UIControlStateNormal];
    [btnOUI setTitle:str(strOUI)  forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)selecNON:(id)sender {
    if ([PublicationOBJ sharedInstance].isEditer) {
    }
    else
    {
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        
        [dicFavo setValue:[NSNumber numberWithInt:0] forKey:@"iSharing"];
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
        
        [[PublicationOBJ sharedInstance] uploadData:@{@"sharingGroupsArray":[NSArray new],@"iSharing":[NSNumber numberWithInt:0]}];
        
        Publication_Niv6_Validation *viewController1 = [[Publication_Niv6_Validation alloc] initWithNibName:@"Publication_Niv6_Validation" bundle:nil];
        [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    }

}

- (IBAction)onNext:(id)sender {
    Publication_Choix_Partage *viewController1 = [[Publication_Choix_Partage alloc] initWithNibName:@"Publication_Choix_Partage" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    
}
@end
