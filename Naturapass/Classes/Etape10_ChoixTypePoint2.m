//
//  Etape10_ChoixTypePoint2.m
//  Naturapass
//
//  Created by Giang on 10/5/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "Etape10_ChoixTypePoint2.h"
#import "SubNavigationPRE_Search.h"
#import "ChassesCreate_Step11.h"
#import "DatabaseManager.h"

#import "CellKind14.h"

@interface Etape10_ChoixTypePoint2 ()
{
    NSMutableArray * arrDataSelection, *arrData;
    IBOutlet UIButton *suivant;
    IBOutlet UILabel *lbTitle;
    IBOutlet UILabel *lbDescription;
    NSInteger countCheck;
}
@end

@implementation Etape10_ChoixTypePoint2



static NSString *identifierSection = @"MyTableViewCell1";

- (void)viewDidLoad {
    [super viewDidLoad];
    lbTitle.text = str(strTypeDePointGeo);
    lbDescription.text = str(strVeuilezSelectionnerPointGeo);
    self.needChangeMessageAlert = YES;

    arrDataSelection = [NSMutableArray new];
    arrData = [NSMutableArray new];
    /*
     
     //children = 0, when card exist
     
     "children": {
     
     },
     "card": {
     "id": 1,
     "name": "Indetermine-MâleAdulte-FemelleAdulte-M
     âleJeune-FemelleJeune"
     }
     
     */
    for (NSDictionary*dic in self.myDic[@"children"]) {
        if ([[PublicationOBJ sharedInstance] checkShowCategory:dic[@"groups"]]) {

        [arrData addObject:dic];
        
        [arrDataSelection addObject:@{@"id":dic[@"id"],
                                      @"name":dic[@"name"],
                                      @"status":[NSNumber numberWithBool:NO]
                                      }];
        }
        
    }
    
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind14" bundle:nil] forCellReuseIdentifier:identifierSection];
    suivant.backgroundColor = UIColorFromRGB(UI_CHASSES_MUR_ADMIN);
    if ([ChassesCreateOBJ sharedInstance].isModifi) {
        [self.btnSuivant setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    [self statusButtonSuivant];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrDataSelection.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
    
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind14 *cell = nil;
    
    cell = (CellKind14 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection forIndexPath:indexPath];
    
    NSDictionary *dic = arrDataSelection[indexPath.row];
    NSDictionary *dicFull = arrData[indexPath.row];
    //Mes group
    
    [cell.slideTitleLabel setText :dic[@"name"]];
    
    if ( ((NSArray*)dicFull[@"children"]).count > 0) {
        cell.imgArrow.hidden = NO;
    }else{
        cell.imgArrow.hidden = YES;
    }
    [cell.btnOver addTarget:self action:@selector(fnCheckBox:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnOver.tag = indexPath.row;
    
    NSNumber *num = dic[@"status"];
    
    if ( [num boolValue] ) {
        [cell.checkbox setSelected:TRUE];
        
    }else{
        [cell.checkbox setSelected:FALSE];
        
    }
    [cell.checkbox setTypeCheckBox:UI_CHASSES_MUR_NORMAL];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void) fnCheckBox:(UIButton*)btn
{
    [self selectedRowWithIndex:btn.tag];
}
-(void)selectedRowWithIndex:(NSInteger)index
{
    NSDictionary *dic=[arrDataSelection[index] mutableCopy];
    
    if ([dic[@"status"] boolValue] == YES)
    {
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"status"];
        countCheck--;
    }
    else
    {
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        countCheck++;
    }
    [arrDataSelection replaceObjectAtIndex:index withObject:dic];
    
    NSRange range = NSMakeRange(0,1);
    NSIndexSet *sections = [NSIndexSet indexSetWithIndexesInRange:range];
    [self.tableControl reloadSections:sections withRowAnimation:UITableViewRowAnimationNone];
    [self statusButtonSuivant];
}
-(void)statusButtonSuivant
{
    if (countCheck >0) {
        self.btnSuivant.backgroundColor = UIColorFromRGB(CHASSES_BACK);
    }
    else
    {
        self.btnSuivant.backgroundColor = [UIColor lightGrayColor];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //    if (indexPath.section == 0) {
    NSDictionary *dic = arrData[indexPath.row];
    
    //Don't care search == 1
    if ( ( (NSArray*)dic[@"children"]).count > 0 ) {
        
        Etape10_ChoixTypePoint2 *viewController1 = [[Etape10_ChoixTypePoint2 alloc] initWithNibName:@"Etape10_ChoixTypePoint2" bundle:nil];
        
        viewController1.myDic = dic;
        [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
    }
    else
    {
        [self selectedRowWithIndex:indexPath.row];
    }
}

-(IBAction)onNext:(id)sender
{

    NSMutableArray *arrTmp = [NSMutableArray new];
    
    for (NSDictionary*dic in arrDataSelection) {
        if ([dic[@"status"] boolValue] == YES) {
            [arrTmp addObject:dic[@"id"]];
        }
    }
    if (arrTmp.count == 0) {
        return;
    }
    //allow next if select atleast 1
    [COMMON addLoading:self];
    //Request
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnPOST_JOIN_GROUP_TO_HUNT:@{@"groups":@[[ChassesCreateOBJ sharedInstance].group_id_involve],
                                            @"categories": arrTmp
                                            }
                                 loungeID:[ChassesCreateOBJ sharedInstance].strID];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [COMMON removeProgressLoading];
//        NSLog(@"%@", response);
        if (response[@"success"]) {
            //
            [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
                NSArray *arrSqls = response[@"sqlite"] ;
                
                for (NSString*strQuerry in arrSqls) {
                    //correct API
                    NSString *tmpStr = [strQuerry stringByReplacingOccurrencesOfString:@",," withString:@"NULL"];
                    
                    
                    ASLog(@"%@", tmpStr);
                    
                    BOOL isOK =   [db  executeUpdate:tmpStr];

                }
            }];

            if ([ChassesCreateOBJ sharedInstance].isModifi) {
                [self backEditListChasse];
            }
            else
            {
            ChassesCreate_Step11 *viewController1 = [[ChassesCreate_Step11 alloc] initWithNibName:@"ChassesCreate_Step11" bundle:nil];
            [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
            }
            
        }
    };
    
}

@end

