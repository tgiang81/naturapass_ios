//
//  AmisSearchVC.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisSearchVC.h"

#import "CellKind2.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "FriendInfoVC.h"

#import "CommonHelper.h"

static NSString *identifierSection1 = @"MyTableViewCell1";
@interface AmisSearchVC ()<UISearchBarDelegate>
{
    NSMutableArray                  *searchedArray;
    IBOutlet UISearchBar                *toussearchBar;
    
    
    
}
@property (nonatomic, assign) BOOL shouldBeginEditing;

@end

@implementation AmisSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [toussearchBar setTintColor:UIColorFromRGB(AMIS_TINY_BAR_COLOR)];
    toussearchBar.placeholder = str(strRechercher);
    searchedArray= [NSMutableArray new];
    
    // Do any additional setup after loading the view from its nib.
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind2" bundle:nil] forCellReuseIdentifier:identifierSection1];
    [self.tableControl setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark- searBar
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    
    [self GetUsersSearchAction:toussearchBar.text];
}
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    [self performSelector:@selector(processLoungesSearchingURL:) withObject:searchText afterDelay:1.0];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    
    self.shouldBeginEditing = NO;
    [self.searchDisplayController setActive:NO];
    [self.tableControl reloadData];
}
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    
    NSString *strsearchValues=toussearchBar.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    toussearchBar.text=strsearchValues;
    [self GetUsersSearchAction:toussearchBar.text];
    [theSearchBar resignFirstResponder];
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [searchBar setShowsCancelButton:YES animated:YES];
    self.shouldBeginEditing = YES;
    [searchedArray removeAllObjects];
    [self.tableControl reloadData];
}

#pragma mark -API
- (void) GetUsersSearchAction:(NSString*)searchString
{
    if (searchString.length < VAR_MINIMUM_LETTRES) {
//        [UIAlertView showWithTitle:@"NaturaPass"
//                           message:MINIMUM_LETTRES
//                 cancelButtonTitle:@"Oui"
//                 otherButtonTitles:nil
//                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
//                          }];
        [KSToastView ks_showToast:MINIMUM_LETTRES duration:2.0f completion: ^{
        }];
    }
    else
    {
        [Flurry logEvent:@"AmisSearchFragment" timed:YES];

        [COMMON addLoading: self];
        
        WebServiceAPI *serviceAPI =[ WebServiceAPI new];
        [serviceAPI getUsersSearchAction:searchString];
        serviceAPI.onComplete =^(id response, int errCode)
        {
            [COMMON removeProgressLoading];
            
            [searchedArray removeAllObjects];
            [searchedArray addObjectsFromArray:[response objectForKey:@"users"]];
            [self.tableControl reloadData];
            [COMMON removeProgressLoading];
        };
    }

}
-(IBAction)friendButtonAction:(UIButton *)sender {
    [self.searchDisplayController.searchBar isFirstResponder];
    [COMMON addLoading:self];

    NSMutableDictionary *mulDic =[NSMutableDictionary dictionaryWithDictionary:searchedArray[[sender tag]]];
    
    NSString *strId=searchedArray[[sender tag]][@"id"];
    
    WebServiceAPI *serviceAPI =[ WebServiceAPI new];
    
    [serviceAPI postUserFriendShipAction:strId];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [COMMON removeProgressLoading];
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
        alert.tag = 50;
        [alert show];
        
        if (mulDic[@"state"]!=nil) {
            [mulDic removeObjectForKey:@"state"];
        }
        [mulDic setValue:@(1) forKey:@"state"];
        [mulDic setValue:@{@"state":@(1),@"way":@(2)} forKey:@"friendship"];
        [searchedArray replaceObjectAtIndex:[sender tag] withObject:mulDic];
        [self.tableControl reloadData];
    };
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [searchedArray count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 60;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind2 *cell = nil;
    
    if (indexPath.row <= searchedArray.count) {
        if (tableView == self.tableControl) {
            cell = (CellKind2 *)[tableView dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
            
            NSDictionary *dic = searchedArray[indexPath.row];
            //IMAGE
            if (searchedArray[indexPath.row][@"profilepicture"]) {
                NSString *strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,searchedArray[indexPath.row][@"profilepicture"]];
                NSURL * url = [NSURL URLWithString:[COMMON characterTrimming:strImage]];
                [cell.imageIcon sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"UserList"]];
                //border img
                
                [[CommonHelper sharedInstance] setRoundedView:cell.imageIcon toDiameter:cell.imageIcon.frame.size.height /2];
            }
            //FONT
            cell.label1.text = dic[@"fullname"];
            [cell.label1 setTextColor:[UIColor blackColor]];
            [cell.label1 setNumberOfLines:1];
            //BUTTON
            cell.constraint_control_width.constant = 20;
            cell.constraintRight.constant = 10;
            cell.constraint_control_height.constant = 20;
            cell.constraint_image_width.constant =50;
            cell.constraint_image_height.constant =50;
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, cell.constraint_control_width.constant, cell.constraint_control_height.constant)] ;
            btn.tag =indexPath.row;
            if([dic[@"state"]integerValue] ==0 || [dic[@"state"]integerValue] == 3)
            {
                [btn setImage:[UIImage imageNamed:@"search_amis_ic_add_amis" ] forState:UIControlStateNormal];
                [btn addTarget:self action:@selector(friendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                cell.viewControl.hidden=NO;
                [cell.viewControl addSubview:btn];
            }
            else
            {
                cell.viewControl.hidden=YES;
            }
            cell.backgroundColor=[UIColor whiteColor];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
    }
    
    
    return cell;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FriendInfoVC *friend=[[FriendInfoVC alloc]initWithNibName:@"FriendInfoVC" bundle:nil];
    [friend setFriendDic:searchedArray[indexPath.row]];
    
    [self pushVC:friend animate:YES expectTarget:ISMUR];
}


@end
