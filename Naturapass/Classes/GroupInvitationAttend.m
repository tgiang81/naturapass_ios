//
//  GroupInvitationAttend.m
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "GroupInvitationAttend.h"
#import "CellKind15.h"
#import "CommonHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "GroupEnterMurVC.h"
static NSString *GroupesCellID =@"GroupesViewCellID";


@interface GroupInvitationAttend ()
{
    //    NSMutableArray  *groupsActionArray;
    NSMutableArray  *arrGroupsIsAdmin;
    NSMutableArray  *arrGroupsIsNormal;
    
    NSArray  *arrCategories;
    IBOutlet UILabel *lbZeroInvitaionGroup;
}
@property (nonatomic, strong) CellKind15 *prototypeCell;

@end

@implementation GroupInvitationAttend

- (void)viewDidLoad {
    [super viewDidLoad];
    lbZeroInvitaionGroup.text = str(strZeroInvitaionGroup);
    lbZeroInvitaionGroup.hidden = YES;
    [self initRefreshControl];
    [self initialization];
    arrCategories = @[str(strPersonnesEnAttenteDeVotreValidation), str(strVosDemandesDaccesEnAttent)];
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],@"GROUP_INVITATION_SAVE")   ];
        NSDictionary *dicTmp = [NSDictionary dictionaryWithContentsOfFile:strPath];
        if (dicTmp.count > 0) {
            if (dicTmp[@"validations"]) {
                //code
                arrGroupsIsAdmin = [dicTmp[@"validations"] mutableCopy];

            }
            if (dicTmp[@"access"]) {
                //code
                arrGroupsIsNormal = [dicTmp[@"access"] mutableCopy];
                
            }
            
            [self checkEmtry:(int)(arrGroupsIsAdmin.count + arrGroupsIsNormal.count)];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            [self.tableControl reloadData];
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startRefreshControl];
    });

    

}

-(void)checkEmtry:(int)count
{
    if (count>0) {
        lbZeroInvitaionGroup.hidden = YES;

    }
    else
    {
        lbZeroInvitaionGroup.hidden = NO;

    }
}

-(void)initialization
{
    // init tableviewcell
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind15" bundle:nil] forCellReuseIdentifier:GroupesCellID];
    self.tableControl.estimatedRowHeight = 100;
    //init array
    //    groupsActionArray=[[NSMutableArray alloc]init];
    arrGroupsIsAdmin=[[NSMutableArray alloc]init];
    arrGroupsIsNormal=[[NSMutableArray alloc]init];
}

#pragma mark - WebServiceAPI

- (void)insertRowAtTop {
    [self refreshGroupInvition];
}

- (void)insertRowAtBottom {
    [self refreshGroupInvition];
}

-(void)refreshGroupInvition
{
    //request first page
//    if ( ![COMMON isReachable] )
//    {
//        [self stopRefreshControl];
//        [self checkEmtry:(int)(arrGroupsIsAdmin.count + arrGroupsIsNormal.count)];
//        return;
//    }
    
    //    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [serviceObj fnV2_GET_GROUP_INVITATION];
    
    __weak GroupInvitationAttend *weakSelf = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        [arrGroupsIsAdmin removeAllObjects];
        [arrGroupsIsNormal removeAllObjects];
        
        if (response[@"validations"]) {
            //code
//            for (NSDictionary *dic in response[@"validations"]) {
//                [arrGroupsIsAdmin addObject:dic[@"group"]];
            arrGroupsIsAdmin = [response[@"validations"] mutableCopy];

//            }
        }
        if (response[@"access"]) {
            //code
            arrGroupsIsNormal = [response[@"access"] mutableCopy];
            
        }
        //cache
            NSArray*arrGroups = [response mutableCopy];
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],@"GROUP_INVITATION_SAVE")   ];
            [arrGroups writeToFile:strPath atomically:YES];
        [weakSelf.tableControl reloadData];
        [self checkEmtry:(int)(arrGroupsIsAdmin.count + arrGroupsIsNormal.count)];
    };
}
- (CellKind15 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:GroupesCellID];
    }
    return _prototypeCell;
}

#pragma mark - TableView datasource & delegate
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellKind15 *cell=(CellKind15 *) cellTmp;
    [self setDataForGroupCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

/*
 {"groups":[
 
 {"id":304,
 "owner":
 {"id":219,"firstname":"B1","lastname":"B1","usertag":"b1-b1","profilepicture"
 :"/img/interface/default-avatar.jpg"},
 
 "access":1,
 "name":"a11",
 "grouptag":"a11","description":"a11","nbSubscribers"
 :1,"nbAdmins":1,"nbPending":0,"photo":"https://naturapass.e-conception.fr/img/interface/default-media
 .jpg",
 
 "connected":
 {"user":
 {"id":160,"fullname":"Manh ManhManh","firstname":"Manh","lastname":"ManhManh"
 ,"usertag":"manh-manhmanh","profilepicture":"/uploads/users/images/thumb/3699cad9e4240bf3508bad7958dca8f1d0fcec34
 .jpeg"},
 "mailable":true,"access":0}
 },
 
 */

/*
 UIImageView *imgProfile;
 UILabel *nameGroup;
 UILabel *accessType;
 UILabel *numberMember;
 UILabel *contentGroup;
 UIButton *btnValid;
 UIButton *btnRefuse;
 */

-(void) setDataForGroupCell:(CellKind15*) cell withIndex:(NSIndexPath *)indexPath
{
    
    NSDictionary *dic = [NSDictionary new];
    BOOL isAdmin = NO;
    
    if (indexPath.section == 0) {
        isAdmin = YES;
        dic = arrGroupsIsAdmin[indexPath.row][@"group"];
        
        [cell.nameGroup setText: arrGroupsIsAdmin[indexPath.row][@"user"][@"fullname"]];
        cell.contentGroup.text = @"";
        NSString *strName = [dic[@"name"] emo_emojiString];

        cell.lbNameGroupAdmin.text = strName;

    }
    else
    {
        isAdmin = NO;
        dic = arrGroupsIsNormal[indexPath.row];
        NSString *strName = [dic[@"name"] emo_emojiString];

        [cell.nameGroup setText: strName];
        NSString *strDescriptione = [dic[@"description"] emo_emojiString];

        cell.contentGroup.text = strDescriptione;
        cell.lbNameGroupAdmin.text = @"";

    }
    
    /*
     acess = 0: current user is invited
     1: request to access and pending to be validate by admin
     2: normal user
     3: admin
     */
    
    
    
    //    NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    [cell.imgProfile sd_setImageWithURL:  [NSURL URLWithString:dic[@"photo"]]];
    
    
    //type groups
    if ([dic[@"access"] intValue] == 0) {
        cell.accessType.text = str(strAccessPrivate);
    } else if ([dic[@"access"] intValue] == 1) {
        cell.accessType.text = str(strAccessSemiPrivate);
    } else if ([dic[@"access"] intValue] == 2) {
        cell.accessType.text = str(strAccessPublic);
    }
    
    
    if ([dic[@"nbSubscribers"] intValue] ==1) {
        cell.numberMember.text = [NSString stringWithFormat:@"%@ %@", dic[@"nbSubscribers"],str(strMMembre)];
        
    }
    else
    {
        cell.numberMember.text = [NSString stringWithFormat:@"%@ %@s", dic[@"nbSubscribers"],str(strMMembre)];
    }
    
    if (isAdmin) {
        [cell.btnValid addTarget:self action:@selector(adminAcceptJoin:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRefuse addTarget:self action:@selector(adminCancelJoin:) forControlEvents:UIControlEventTouchUpInside];

    }else{
        [cell.btnValid addTarget:self action:@selector(acceptJoin:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRefuse addTarget:self action:@selector(cancelJoin:) forControlEvents:UIControlEventTouchUpInside];
    
    }
    
    cell.btnValid.tag  = indexPath.row + 1;
    cell.btnRefuse.tag = indexPath.row + 2;

//    if (([dic[@"access"] intValue] == 3) /*i am admin...i can accept-reject */||  ( [dic[@"connected"][@"access"] intValue] == 0) /*they invite me and wait for my action*/) {
    if(isAdmin){

        //i am admin...
        //accept - reject
        //not yet join -> can accept/cancel
        
        cell.btnValid.hidden = NO;
        cell.btnRefuse.hidden = NO;
        
        [cell.btnValid setTitle:str(strValider) forState:UIControlStateNormal];
        [cell.btnRefuse setTitle:str(strRefuser) forState:UIControlStateNormal];
        
        
    }
    else // i am normal user...i search and asked for joinning, but i can reject
    {

//                    access = 0; duoc moi, 1: minh chu dong join...
        if ([dic[@"connected"][@"access"] intValue] == 0) {
            cell.btnValid.hidden = NO;
            cell.btnRefuse.hidden = NO;
            
            [cell.btnValid setTitle:str(strValider) forState:UIControlStateNormal];
            [cell.btnRefuse setTitle:str(strRefuser) forState:UIControlStateNormal];

        }else{
            //i request join -> only can cancel annuler
            cell.btnValid.hidden = YES;
            cell.btnRefuse.hidden = NO;
            [cell.btnRefuse setTitle:str(strAnuler) forState:UIControlStateNormal];
        }
    }
    [cell.lbTime setText:@""];
    [cell layoutIfNeeded];
}

-(void) fnDoRefreshData
{
    [self insertRowAtTop];
}

 -(void) adminAcceptJoin:(UIButton*)btn
{
    int index = 0;
    NSMutableArray *arrTmp = [NSMutableArray new];

    arrTmp = [arrGroupsIsAdmin mutableCopy];
    index = (int) btn.tag-1;
    
    NSDictionary *dic = [arrTmp objectAtIndex:index];
    
    
    //if i am admin...
    //    access = 1;
    
    
    //    https://naturapass.e-conception.fr/api/v1/groups/299/users/192/join
    //put res {"access":1}
    //https://naturapass.e-conception.fr/api/v1/groups/160/joins/304
    
    //    [COMMON addLoading:self];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    NSLog(@"start...");
    

    [serviceObj putJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE mykind_id:dic[@"group"][@"id"] strUserID:dic[@"user"][@"id"] ];

    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        NSLog(@"stop....");
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        
        [self fnGetAllNotifications:3 creen:ISGROUP];
        
        if (!response) {
            return ;
        }
        if ([response[@"subscriber"] isKindOfClass:[NSDictionary class]]) {
            [self startRefreshControl];
            //go in?
        }
        
    };

}

 -(void) adminCancelJoin:(UIButton*)btn
{
    int index = 0;
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    arrTmp = [arrGroupsIsAdmin mutableCopy];
    index = (int) btn.tag-2;
    
    NSDictionary *dic = [arrTmp objectAtIndex:index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    
    //i am admin
    [serviceObj deleteJoinWithKind:(self.expectTarget==ISGROUP)?MYGROUP:MYCHASSE UserID:dic[@"user"][@"id"] mykind_id:dic[@"group"][@"id"]];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [self fnGetAllNotifications:3 creen:ISGROUP];
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        if ([response[@"success"] intValue] == 1) {
            [self startRefreshControl];
            
            //go in?
        }
    };

}

-(void) acceptJoin:(UIButton*)btn
{
    int index = 0;
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    arrTmp = [arrGroupsIsNormal mutableCopy];
    index = (int) btn.tag-1;
    NSDictionary *dic = [arrTmp objectAtIndex:index];
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];

    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSString *strLoungeID = dic[@"id"];
    [serviceObj putJoinWithKind:MYGROUP mykind_id:strLoungeID strUserID:UserId ];
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [COMMON removeProgressLoading];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:response];

        [self fnGetAllNotifications:3 creen:ISGROUP];
        
        if (!response) {
            return ;
        }
        if ([response[@"success"] intValue] == 1) {
            
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];
                //
                [[GroupEnterOBJ sharedInstance] resetParams];
                [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                
                [self pushVC:viewController1 animate:YES];
        }
    };
}

-(void) cancelJoin:(UIButton*)btn
{
    int index = 0;
    NSMutableArray *arrTmp = [NSMutableArray new];
    
    arrTmp = [arrGroupsIsNormal mutableCopy];
    index = (int) btn.tag-2;
    
    NSDictionary *dic = [arrTmp objectAtIndex:index];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    NSString *sender_id = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    [serviceObj unsubscribeGroupAction:sender_id :dic[@"id"]];    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [self fnGetAllNotifications:3 creen:ISGROUP];
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        if ([response[@"success"] intValue] == 1) {
            [self startRefreshControl];
            
            //go in?
        }
    };
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if ((section==0 && arrGroupsIsAdmin.count) || (section==1 && arrGroupsIsNormal.count>0)) {
        return 50;
    }
    return 0.0001;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ((section==0 && arrGroupsIsAdmin.count) || (section==1 && arrGroupsIsNormal.count>0)) {
        UIView* sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
        sectionView.backgroundColor= UIColorFromRGB(MAIN_COLOR);
        UILabel *headerLabel=[[UILabel alloc]initWithFrame:CGRectMake(12, 15, 320, 29)];
        [headerLabel setTextColor:[UIColor blackColor]];
        headerLabel.numberOfLines=2;
        [headerLabel setBackgroundColor:[UIColor clearColor]];
        [headerLabel setFont:[UIFont boldSystemFontOfSize:12]];
        
        headerLabel.text= arrCategories[section];
        
        [sectionView addSubview:headerLabel];
        
        return  sectionView;
    }
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return arrGroupsIsAdmin.count;
    }
    else
    {
        return arrGroupsIsNormal.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind15 *cell = (CellKind15 *)[tableView dequeueReusableCellWithIdentifier:GroupesCellID forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

@end
