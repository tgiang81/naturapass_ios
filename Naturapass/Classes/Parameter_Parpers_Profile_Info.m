//
//  Parameter_VoisChiens_Info.m
//  Naturapass
//
//  Created by Manh on 3/15/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_Parpers_Profile_Info.h"
#import "PdfCollectionCell.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "CommonHelper.h"
#import "TypeCell_PaperProfile_Info.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_Parpers_Profile_Info () <IDMPhotoBrowserDelegate>
{
    IBOutlet UILabel *lbName;
    IBOutlet UILabel *lbText;
    IBOutlet UILabel *lbNameScreen;

//    UIImageView *imgAvatar;
    IBOutlet UIButton    *btnValider;

    __weak IBOutlet UILabel *lbTitle;
    
    NSArray * arrData;

}
@property (nonatomic, strong) TypeCell_PaperProfile_Info *prototypeCell;

@end

@implementation Parameter_Parpers_Profile_Info

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"TypeCell_PaperProfile_Info" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    self.tableControl.rowHeight = UITableViewAutomaticDimension;
    self.tableControl.estimatedRowHeight = 44.0; // set to whatever your "average" cell height is
//    imgAvatar = [UIImageView new];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self fnSetDataInfo];
}

/*
 deletable = 1;
 id = 903;
 medias =     (
 );
 
 name = b1;
 text = "mo ta";
 title = Description;
 type = 0;
 */

-(void)fnSetDataInfo
{
    if (self.dicInfo) {
        //name
        lbName.text = [self.dicInfo[@"name"] uppercaseString];

        lbTitle.text = [self.dicInfo[@"title"] uppercaseString];
        
        //description
        lbText.text =self.dicInfo[@"text"];
        //photo
//        NSArray *medias =self.dicInfo[@"medias"];
//        
//        if (medias.count>0)
//        {
//            NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"medias"][medias.count -1][@"path"]]]];
//            
//            [imgAvatar sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                
//                if (image) {
//                    if (image.size.width > CGRectGetWidth(imgAvatar.bounds)) {
//                        
//                        UIImage *tempImg = [self fnResizeFixWidth:image :imgAvatar.bounds.size.width];
//                        imgAvatar.image = tempImg;
//                        [self.tableControl reloadData];
//                        
//                    }else{
//                        imgAvatar.image = image;
//                        [self.tableControl reloadData];
//                        
//                    }
//                }else{
//                    //image nil...or load when offline
//                    imgAvatar.image = nil;
//                    [self.tableControl reloadData];
//                    
//                }
//                
//            }];
//        }
//        else
//        {
//            
//        }
    }
    
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) showPhoto
{
    NSArray *medias =self.dicInfo[@"medias"];
    
    if (medias.count > 0)
    {
        IDMPhoto *aphoto = nil;
        NSArray *medias =self.dicInfo[@"medias"];
        
        NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"medias"][medias.count -1][@"path"]]];
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];
        
        NSURL * urlImage = [NSURL URLWithString:strImage];
        
        
        aphoto = [IDMPhoto photoWithURL:urlImage];
        // Create and setup browser
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:@[aphoto]];
        browser.delegate = self;
        
        // Show
        [self presentViewController:browser animated:YES completion:nil];

    }
}

#pragma mark - TABLEVIEW

- (TypeCell_PaperProfile_Info *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
    }
    return _prototypeCell;
}



//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TypeCell_PaperProfile_Info *cell = [tableView dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height+10;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (void)configureCell:(TypeCell_PaperProfile_Info *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.name.text = [self.dicInfo[@"name"] uppercaseString];
    cell.title.text = [self.dicInfo[@"title"] uppercaseString];
    cell.text.text = self.dicInfo[@"text"];
    
    [cell.btnImage addTarget:self action:@selector(showPhoto) forControlEvents:UIControlEventTouchUpInside];

    NSArray *medias =self.dicInfo[@"medias"];
    
    if (medias.count > 0)
    {
        NSString *strImage = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,[COMMON characterTrimming:self.dicInfo[@"medias"][medias.count -1][@"path"]]];
        
        strImage=[strImage stringByReplacingOccurrencesOfString:@"resize" withString:@"original"];

        NSURL * url = [NSURL URLWithString:strImage];
        __weak typeof(self) weakSelf = self;

        [cell.indicator startAnimating];
        cell.indicator.hidden = NO;
        
        [cell.image sd_setImageWithURL:url placeholderImage: [UIImage imageNamed:@"placeholder_photo"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {

            if (image) {
                if (image.size.width > CGRectGetWidth( self.view.frame )) {
                    
                    UIImage *tempImg = [self fnResizeFixWidth:image :self.view.frame.size.width];
                    cell.image.image = tempImg;
                    
                }else{
                    cell.image.image = image;
                    
                }
                
                    [weakSelf.tableControl beginUpdates];
                    [weakSelf.tableControl endUpdates];

            }else{
                //image nil...or load when offline
                cell.image.image = nil;
                
            }
            [cell.indicator stopAnimating];
            cell.indicator.hidden = YES;

        }];
    }
    else
    {
        cell.indicator.hidden = YES;
    }
    
    [cell layoutIfNeeded];

    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
   
}

- (IBAction)fnBack:(id)sender {
    [self gotoback];
}

@end
