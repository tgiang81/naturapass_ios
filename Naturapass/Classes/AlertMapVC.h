//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
typedef void (^AlertMapVCCallback)(NSInteger index);
@interface AlertMapVC : UIViewController
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
    IBOutlet UIImageView *bgImage;
}
@property (nonatomic,copy) AlertMapVCCallback callback;
-(void)doBlock:(AlertMapVCCallback ) cb;
-(void)showInVC:(UIViewController*)vc;
-(instancetype)initWithTitle:(NSString*)stitle message:(NSString*)smessage;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;
@property (nonatomic,strong) IBOutlet UILabel *lbDescriptions;

@end
