//
//  TreeViewNode.h
//  The Projects
//
//  Created by Ahmed Karim on 1/12/13.
//  Copyright (c) 2013 Ahmed Karim. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TreeViewNode : NSObject

@property (nonatomic) NSUInteger nodeLevel;
@property (nonatomic) NSUInteger nodeID;
@property (nonatomic) BOOL isExpanded;
@property (nonatomic) BOOL isEditItem;
@property (nonatomic) BOOL isEmtry;

@property (nonatomic) BOOL isSelected;
@property (nonatomic, strong) id nodeObject;
@property (nonatomic, strong) id nodeTitle;
@property (nonatomic, strong) id nodeItemAttachment;

@property (nonatomic, strong) NSMutableArray *nodeChildren;
@property (nonatomic) int statusNode;//trang thai node
@property (nonatomic) BOOL lineBottom;
@property (nonatomic) int typeCell;
@property (nonatomic, retain) TreeViewNode *parent;
@end
