//
//  AlertViewLiveHuntCell.h
//  Naturapass
//
//  Created by JoJo on 12/18/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AlertViewLiveHuntCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgCircleAvatar;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbTime;
@property (weak, nonatomic) IBOutlet UIImageView *imgLine;

@end

NS_ASSUME_NONNULL_END
