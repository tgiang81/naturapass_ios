//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
#import "KSToastView.h"
#import "Define.h"
typedef void (^AlertListMarkerCallback)(NSDictionary *dicUserData);
@interface AlertListMarker : UIView<UITextFieldDelegate>
{
    IBOutlet UIView *subAlertView;
    NSString *strTitle;
    NSString *strDescriptions;
}
@property (strong, nonatomic)  NSMutableArray *arrMarker;
@property (nonatomic,assign) ISSCREEN expectTarget;
@property (nonatomic,copy) AlertListMarkerCallback callback;
@property (strong, nonatomic) IBOutlet UITableView *tableControl;

-(void)doBlock:(AlertListMarkerCallback ) cb;
-(void)showAlert;
-(instancetype)init;
@end
