//
//  ChassesCreate_Step2.m
//  Naturapass
//
//  Created by Manh on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ChassesCreate_Step7.h"
#import "ChassesCreate_Step8.h"
#import "TypeCell17.h"
#import "Etape10_ChoixTypePoint1.h"
#import "DatabaseManager.h"
static NSString *typecell41 =@"TypeCell17";
static NSString *typecell41ID =@"TypeCell17ID";
@interface ChassesCreate_Step7 ()
{
    NSMutableArray *arrData;
}
@property(nonatomic,strong) IBOutlet UILabel *label1;
@property(nonatomic,strong) IBOutlet UILabel *label2;
@property(nonatomic,strong) IBOutlet UILabel *label3;
@property(nonatomic,strong) IBOutlet UILabel *labelWarning;

@end

@implementation ChassesCreate_Step7
#pragma mark -setColor
-(void)fnColorTheme
{
    _label1.text =str(strIMPORTER_DES_POINTS_GEOLOCALISES);
    _label2.text =str(strImporter_point_de_geolocalisation);
    _label3.text =str(strGroupes);
    
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self fnColorTheme];
    [self initialization];
    self.needChangeMessageAlert = YES;

}

//membres ~ number subscriber.
-(void)initialization
{

    [self.tableControl registerNib:[UINib nibWithNibName:typecell41 bundle:nil] forCellReuseIdentifier:typecell41ID];
    arrData = [NSMutableArray new];
    /*
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE c_admin=1 AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            
            //compare to get defaul
            for (NSDictionary*dic in arrTmp) {
                if ([dic[@"groupID"] intValue ]== strID) {
                    [arrData addObject:@{@"id":dic[@"group"][@"id"],
                                         @"name":dic[@"group"][@"name"],
                                         @"photo":dic[@"group"][@"photo"],
                                         @"nbSubscribers": dic[@"group"][@"nbSubscribers"],
                                         @"status":[NSNumber numberWithBool:NO]
                                         }];
                    break;
                }
            }
        }
    }];
     */
    [COMMON addLoadingForView:self.view];
    WebServiceAPI *service = [WebServiceAPI new];
    [service getAllGroupsAdmin];
    service.onComplete = ^(NSDictionary *response, int errCode)
    {
        [COMMON removeProgressLoading];
        //compare to get defaul
        NSArray *arrTmp = response[@"groups"];
        for (NSDictionary*dic in arrTmp) {
            [arrData addObject:@{@"id":dic[@"id"],
                                 @"name":dic[@"name"],
                                 @"photo":dic[@"photo"],
                                 @"nbSubscribers": dic[@"nbSubscribers"],
                                 @"status":[NSNumber numberWithBool:NO]
                                 }];
        }
        if (arrData.count ==0 && [COMMON isReachable]) {
            self.labelWarning.hidden = NO;
        }else{
            self.labelWarning.hidden = YES;
        }
        [self.tableControl reloadData];
    };

}

#pragma mark - TableView datasource & delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TypeCell17 *cell = (TypeCell17 *)[tableView dequeueReusableCellWithIdentifier:typecell41ID];
    
    //photo
    NSDictionary *dic = arrData[indexPath.row];
    
    NSString* imgUrl  = [NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];

    [cell.img1 sd_setImageWithURL:  [NSURL URLWithString:imgUrl]];
    
    
    NSString*strMembres = nil;
    
    cell.label1.text = dic[@"name"];
    if ([dic[@"nbSubscribers"] intValue] > 1) {
        strMembres = [NSString stringWithFormat:@"%d %@s",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
        
    }else{
        strMembres = [NSString stringWithFormat:@"%d %@",[dic[@"nbSubscribers"] intValue],str(strMMembre)];
    }
    
    cell.label2.text = strMembres;
    [cell fnSettingCell:UI_CHASSES_MUR_NORMAL];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = arrData[indexPath.row];
    
    [ChassesCreateOBJ sharedInstance].group_id_involve = dic[@"id"];
    
    
    Etape10_ChoixTypePoint1 *viewController1 = [[Etape10_ChoixTypePoint1 alloc] initWithNibName:@"Etape10_ChoixTypePoint1" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE iAmParent:NO];
}

@end
