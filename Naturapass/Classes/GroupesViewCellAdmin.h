//
//  GroupesViewCell.h
//  Naturapass
//
//  Created by Admin on 5/12/15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GroupesViewBaseCell.h"
#import "MLKMenuPopover.h"
@interface GroupesViewCellAdmin : GroupesViewBaseCell <MLKMenuPopoverDelegate>
{

}
@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;
@property (nonatomic,strong) IBOutlet UIView *viewPopup;

@end
