//
//  Publication_Choix_Partage.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Signaler_Choix_Partage.h"
#import "Publication_Niv6_Validation.h"
#import "ServiceHelper.h"

#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreLocation/CoreLocation.h>

#import "SignalerCheckBoxCell.h"
#import "MurParameter_Publication.h"
#import "MurVC.h"
#import "DatabaseManager.h"
#import "NSString+HTML.h"
#import "AlertSearchPersonneVC.h"
#import "Filter_Cell_Type2.h"
#import "SignalerPeopleCell.h"
#import "SignalerHeaderCell.h"
#import "ChassesCreatePersonneCell.h"
#import "AlertSuggestPersoneView.h"
static NSString *identifierSection0 = @"MyTableViewCell0";
static NSString *identifierSection1 = @"MyTableViewCell1";
static NSString *identifierSection2 = @"MyTableViewCell2";
static NSString *identifierSection3 = @"MyTableViewCell3";
static NSString *identifierSection4 = @"MyTableViewCell4";
static NSString *identifierSection5 = @"MyTableViewCell5";
static NSString *identifierHeader   = @"identifierHeader";
static NSString *identifierPersonne = @"identifierPersonne";

//static int  kHeightCell = 48;
//static int  kMaxNumberCell = 5;

@interface Signaler_Choix_Partage ()<UIScrollViewDelegate, UITextFieldDelegate>
{
    NSDictionary* currentData;
    
    NSMutableArray              *mesArr;
    
    NSMutableArray              *sharingGroupsArray;
    NSMutableArray              *sharingHuntsArray;
    NSMutableArray              *arrReceivers;
    NSMutableArray              *arrPersonne;
    NSArray                     *arrHeader;

    int iSharing;
    BOOL isOpenSearch;
    //    NSDictionary *dicFederer;
//    IBOutlet UIButton *suivantBtn;
//    IBOutlet UIButton *btnMonPartage;
//    AlertSearchPersonneVC *vcSearch;
    AlertSuggestPersoneView *suggestViewPersonne;

    NSString *strSearch;
    IBOutlet UIButton *btnSuivatnt;
    IBOutlet UIButton *btnAnnuler;
}
@property (strong, nonatomic)  NSString *strPersonne;

@end

@implementation Signaler_Choix_Partage

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    MainNavigationBaseView *subview =  [self getSubMainView];
    [subview.myDesc setText:@"Options de partage..."];
    
//    [btnMonPartage setTitle:str(strGERER_MON_PARTAGE) forState:UIControlStateNormal];
//    [suivantBtn setTitle:str(strSUIVANT) forState:UIControlStateNormal];
    
    self.viewBottom.backgroundColor = self.colorBackGround;
    
    [btnSuivatnt.layer setMasksToBounds:YES];
    btnSuivatnt.layer.cornerRadius= 25.0;
    [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal];
    btnSuivatnt.backgroundColor = self.colorNavigation;

    [btnAnnuler.layer setMasksToBounds:YES];
    btnAnnuler.layer.cornerRadius= 20.0;
    btnAnnuler.backgroundColor = self.colorAnnuler;

    // Do any additional setup after loading the view from its nib.
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerCheckBoxCell" bundle:nil] forCellReuseIdentifier:identifierSection0];
    
    //Section1
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerCheckBoxCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    //Section2
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerCheckBoxCell" bundle:nil] forCellReuseIdentifier:identifierSection2];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerCheckBoxCell" bundle:nil] forCellReuseIdentifier:identifierSection3];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"Filter_Cell_Type2" bundle:nil] forCellReuseIdentifier:identifierSection4];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerPeopleCell" bundle:nil] forCellReuseIdentifier:identifierSection5];
    [self.tableControl registerNib:[UINib nibWithNibName:@"SignalerHeaderCell" bundle:nil] forCellReuseIdentifier:identifierHeader];
    [self.tableControl registerNib:[UINib nibWithNibName:@"ChassesCreatePersonneCell" bundle:nil] forCellReuseIdentifier:identifierPersonne];
    self.tableControl.estimatedRowHeight = 60;
    self.tableControl.estimatedSectionHeaderHeight = 40;
    self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self InitializeKeyboardToolBar];
    _imgCheckBoxActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:self.colorNavigation colorBorder:[UIColor whiteColor] isRadio:NO];
    _imgCheckBoxInActive = [UIImage imageCheckBoxWithBackground:[UIColor whiteColor] colorActive:[UIColor whiteColor] colorBorder:[UIColor whiteColor] isRadio:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    suggestViewPersonne = [[AlertSuggestPersoneView alloc] initSuggestView];

    arrHeader = @[@{@"name":str(strPARTAGER_AVEC)},
                  @{@"name":str(strSentinelle)},
                  @{@"name":str(strMesGroupes)},
                  @{@"name":str(strMesChantier)}];
    
    sharingGroupsArray= [NSMutableArray new];
    sharingHuntsArray= [NSMutableArray new];
    
    //Filter
    mesArr =        [NSMutableArray new];
    arrReceivers=  [NSMutableArray new];
    arrPersonne = [NSMutableArray new];
    
    if ([PublicationOBJ sharedInstance].arrReceivers) {
        [arrReceivers addObjectsFromArray:[PublicationOBJ sharedInstance].arrReceivers];
    }
    
    if (!self.isEditFavo) {
        for (int i= 0; i< arrReceivers.count; i++) {
            NSMutableDictionary *dic=[[arrReceivers objectAtIndex:i] mutableCopy];
            [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
            [arrReceivers replaceObjectAtIndex:i withObject:dic];
        }
    }
    
    //If go from group/hunt => don't auto select amis/Member Natura
    iSharing = 0;
    
    if ([PublicationOBJ sharedInstance].groupID != nil || [PublicationOBJ sharedInstance].huntID != nil) {
        iSharing = 0;
    }
    
    //1
    NSDictionary *dic1 = @{@"categoryName":strAAmis,
                           @"categoryImage":@"mes2_icon",
                           @"isSelected": [NSNumber numberWithBool:NO]
                           } ;
    
    //3
    NSDictionary *dic2 = @{@"categoryName":str(strTous_les_natiz),
                           @"categoryImage":strIcon_All_Member,
                           @"isSelected": [NSNumber numberWithBool:NO]
                           };
    
    NSDictionary *dic3 = @{@"categoryName":str(strOtherPeople),
                           @"categoryImage":@"ic_my_group_inactive",
                           @"isSelected": [NSNumber numberWithBool:NO]
                           };
    
    
    mesArr = [NSMutableArray arrayWithArray:@[dic1,dic2,dic3] ];
    
    
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        //get current time
        NSDate *date = [NSDate date];
        NSTimeInterval time = [date timeIntervalSince1970];
        //list shared mes chasses
        int current_time = (int)time;
        
        //list shared mes groups
        //GROUP
        
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSString *strQuerry = [NSString stringWithFormat:@" SELECT * FROM tb_group WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ ",sender_id];
        
        FMResultSet *set_querry1 = [db  executeQuery:strQuerry];
        
        
        while ([set_querry1 next])
        {
            
            int strID = [set_querry1 intForColumn:@"c_id"];
            BOOL isSelected = NO;
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmp) {
                if ([kDic[@"groupID"] intValue ]== strID) {
                    //selected
                    isSelected = [kDic[@"isSelected"] boolValue];
                    break;
                }
            }
            
            [sharingGroupsArray addObject:@{@"categoryName":  [[set_querry1 stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ,
                                            @"groupID":  [set_querry1 stringForColumn:@"c_id"],
                                            @"isSelected": [NSNumber numberWithBool:isSelected]
                                            }];
            
        }
        
        //AGENDA
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
        
        
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        NSString *strQuerry_hunt = [NSString stringWithFormat:@" SELECT * FROM tb_hunt WHERE (c_admin=1 OR c_allow_add=1) AND c_user_id=%@ AND c_end_date >= %d ",sender_id, current_time];
        
        FMResultSet *set_querry_hunt = [db  executeQuery:strQuerry_hunt];
        
        
        while ([set_querry_hunt next])
        {
            
            int strID = [set_querry_hunt intForColumn:@"c_id"];
            BOOL isSelected = NO;
            
            //compare to get defaul
            for (NSDictionary*kDic in arrTmpHunt) {
                if ([kDic[@"huntID"] intValue ]== strID) {
                    //selected
                    isSelected = [kDic[@"isSelected"] boolValue];
                    break;
                }
            }
            
            [sharingHuntsArray addObject:@{@"categoryName":  [[set_querry_hunt stringForColumn:@"c_name"] stringByDecodingHTMLEntities] ,
                                           @"huntID":  [set_querry_hunt stringForColumn:@"c_id"],
                                           @"isSelected": [NSNumber numberWithBool:isSelected]
                                           }];
            
        }
    }];
    
    //??? DB fail ??? => Get all. hard fix
    if (sharingHuntsArray.count == 0 && sharingGroupsArray.count == 0) {
        
        //GROUPE
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],SHARE_MES_GROUP_SAVE)  ];
        
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        for (NSDictionary*kDic in arrTmp) {
            ASLog(@"%@",kDic);
            
            [sharingGroupsArray addObject: @{@"categoryName":  [kDic[@"categoryName"] stringByDecodingHTMLEntities] ,
                                             @"groupID":  kDic[@"groupID"],
                                             @"isSelected": [NSNumber numberWithBool:NO]
                                             }];
        }
        
        //AGENDA
        strPath = [FileHelper pathForApplicationDataFile:  concatstring([COMMON getUserId],SHARE_MES_HUNT_SAVE) ];
        NSArray *arrTmpHunt = [NSArray arrayWithContentsOfFile:strPath];
        
        for (NSDictionary*kDic in arrTmpHunt) {
            
            [sharingHuntsArray addObject:@{@"categoryName":  [kDic[@"categoryName"] stringByDecodingHTMLEntities] ,
                                           @"huntID":  kDic[@"huntID"],
                                           @"isSelected": [NSNumber numberWithBool:NO]
                                           }];
        }
        
    }
    
    //
    if ([PublicationOBJ sharedInstance].isEditer) {
        [self fnSetValueIsEditer];
        [btnSuivatnt setTitle:str(strValider) forState:UIControlStateNormal ];
    }
    else
    {
        
        if (self.isEditFavo) {
            [self fnSetValueIsEditer];
        }
        else
        {
            //                if ([COMMON isReachable]) {
            //                    [self getUserShare];
            //                }
            //set auto select group/hunt if go in from ...
            //set check list group
            for (int i = 0; i<sharingGroupsArray.count; i++) {
                NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
                
                if ([[PublicationOBJ sharedInstance].groupID  intValue] == [dic[@"groupID"]  intValue])
                {
                    [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    [sharingGroupsArray replaceObjectAtIndex:i withObject:dic];
                    break;
                    
                }
            }
            
            //set check list hunt
            for (int i = 0; i<sharingHuntsArray.count; i++) {
                NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
                
                if ([[PublicationOBJ sharedInstance].huntID  intValue] == [dic[@"huntID"]  intValue])
                {
                    [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                    [sharingHuntsArray replaceObjectAtIndex:i withObject:dic];
                    break;
                    
                }
            }
            NSDictionary *dicPatarge = [[PublicationOBJ sharedInstance].dicPatarge copy];
            if(dicPatarge[@"sharingGroupsArray"])
            {
                sharingGroupsArray = [dicPatarge[@"sharingGroupsArray"] mutableCopy];
            }
            if(dicPatarge[@"sharingHuntsArray"])
            {
                sharingHuntsArray = [dicPatarge[@"sharingHuntsArray"] mutableCopy];
            }
            if(dicPatarge[@"receivers"])
            {
                arrReceivers = [dicPatarge[@"receivers"] mutableCopy];
            }
            if(dicPatarge[@"personne"])
            {
                arrPersonne = [dicPatarge[@"personne"] mutableCopy];
            }
            if(dicPatarge[@"iSharing"])
            {
                iSharing =[dicPatarge[@"iSharing"] intValue];
                //Amis
                NSMutableDictionary *dic1 = [mesArr[0] mutableCopy];
                [dic1 setObject:[NSNumber numberWithBool:iSharing==1] forKey:@"isSelected"];
                [mesArr replaceObjectAtIndex:0 withObject:dic1];
                //natiz
                NSMutableDictionary *dic2 = [mesArr[1] mutableCopy];
                [dic2 setObject:[NSNumber numberWithBool:iSharing==3] forKey:@"isSelected"];
                [mesArr replaceObjectAtIndex:1 withObject:dic2];
                

            }
            
            //? Other person save ?
            NSMutableDictionary *dic3 = [mesArr[2] mutableCopy];
            
            if (dicPatarge[@"isOpenSearch"]) {
                isOpenSearch = [dicPatarge[@"isOpenSearch"]  boolValue];
                [dic3 setObject: dicPatarge[@"isOpenSearch"] forKey:@"isSelected"];
                [mesArr replaceObjectAtIndex:2 withObject:dic3];
            }

            [self.tableControl reloadData];
            
        }
        
    }
    
//    [self searchPersonne];
    
}

-(void)fnSetValueIsEditer
{
    NSString *strId =@"id";
    // set value ishare
    iSharing =[PublicationOBJ sharedInstance].iShare;
    
    //Amis
    NSMutableDictionary *dic1 = [mesArr[0] mutableCopy];
    [dic1 setObject:[NSNumber numberWithBool:iSharing==1] forKey:@"isSelected"];
    [mesArr replaceObjectAtIndex:0 withObject:dic1];
    //natiz
    NSMutableDictionary *dic2 = [mesArr[1] mutableCopy];
    [dic2 setObject:[NSNumber numberWithBool:iSharing==3] forKey:@"isSelected"];
    [mesArr replaceObjectAtIndex:1 withObject:dic2];
    
    //    if (self.isEditFavo) {
    //        strId =@"groupID";
    //    }
    //set check list group
    for (NSDictionary *dicGroup in [PublicationOBJ sharedInstance].arrGroup) {
        for (int i = 0; i<sharingGroupsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:i] mutableCopy];
            
            if ([dicGroup[strId]  intValue] == [dic[@"groupID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingGroupsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //    if (self.isEditFavo) {
    //        strId =@"huntID";
    //    }
    //set check list hunt
    for (NSDictionary *dicHunt in [PublicationOBJ sharedInstance].arrHunt) {
        for (int i = 0; i<sharingHuntsArray.count; i++) {
            NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:i] mutableCopy];
            
            if ([dicHunt[strId]  intValue] == [dic[@"huntID"]  intValue])
            {
                [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
                [sharingHuntsArray replaceObjectAtIndex:i withObject:dic];
                break;
                
            }
        }
    }
    //set check list hunt
    [arrPersonne removeAllObjects];
    for (NSDictionary *dicUser in [PublicationOBJ sharedInstance].arrsharingUsers) {
        NSMutableDictionary *dic=[dicUser mutableCopy];
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"status"];
        [arrPersonne addObject:dic];
    }
    if (arrPersonne.count > 0) {
        NSMutableDictionary *dic=[[mesArr objectAtIndex:2] mutableCopy];
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
//        [vcSearch reloadData:nil];
//        [self checkHeightTableWithCount:0];
//        [self fnSetPosittionSearch];
        [mesArr replaceObjectAtIndex:2 withObject:dic];
        
    }
    
    [self.tableControl reloadData];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -  API
-(void)getUserShare
{
    //    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:TRUE];
    //
    //    WebServiceAPI *serverAPI =[WebServiceAPI new];
    //    [serverAPI getUserParam];
    //    serverAPI.onComplete = ^(id response, int errCode)
    //    {
    //        //170:      *      "friend": [0 => OFF, 1 => ON]
    //        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
    //
    //        if (response) {
    //            [[NSUserDefaults standardUserDefaults]setValue: [NSNumber numberWithInt:[response[@"sharing"] intValue] ] forKey:@"CONFIGPARTAGE"];
    //            [[NSUserDefaults standardUserDefaults]synchronize];
    //
    //            iSharing =[response[@"sharing"] intValue];
    //
    //            if ([PublicationOBJ sharedInstance].groupID != nil || [PublicationOBJ sharedInstance].huntID != nil) {
    //                iSharing = 0;
    //            }
    //
    //            NSMutableDictionary *dic1 = [@{@"categoryName":str(strAAmis),
    //                                           @"categoryImage":@"mes2_icon",
    //                                           @"isSelected": [NSNumber numberWithBool:iSharing==1]
    //                                           } copy];
    //
    //            NSMutableDictionary *dic2 = [@{@"categoryName":str(strTous_les_natiz),
    //                                           @"categoryImage":strIcon_All_Member,
    //                                           @"isSelected": [NSNumber numberWithBool:iSharing==3]
    //
    //                                           } copy];
    //
    //            mesArr = [NSMutableArray arrayWithArray:[@[dic1,dic2] copy]];
    //            [self.tableControl reloadData];
    //        }
    //    };
}
#pragma mark -  TableView Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //don't show Federation
    
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            if(isOpenSearch)
                {
                    return (mesArr.count + 1);

                }
            else
                {
                    return mesArr.count;

                }
            break;
        case 1:
            return arrReceivers.count;
            break;
        case 2:
            return sharingGroupsArray.count;
            break;
        case 3:
            return sharingHuntsArray.count;
            break;
            
        default:
            return 0;
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 50;
    }else if (section == 1) {
        if (arrReceivers.count>0) {
            return 50;
        }
        else
        {
            return 0;
        }
    }
    else if (section == 2) {
        
        if (sharingGroupsArray.count>0) {
            return 50;
        }
        else
        {
            return 0;
        }
    }
    else if (section == 3) {
        
        if (sharingHuntsArray.count>0) {
            return 50;
        }
        else
        {
            return 0;
        }
    }else{
        return 20;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dicData = arrHeader[section];
    SignalerHeaderCell *cell = (SignalerHeaderCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierHeader];
    cell.lbTitle.text = dicData[@"name"];
    cell.backgroundColor = self.colorCellBackGround;
    return cell;
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 3) {
        ChassesCreatePersonneCell *cell = (ChassesCreatePersonneCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierPersonne forIndexPath:indexPath];
        //Status
        cell.backgroundColor = self.colorCellBackGround;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell addPersone:[arrPersonne copy]];
        [cell doBlockTokenDelete:^(NSInteger index) {
            NSDictionary *dicTmp = [arrPersonne[index] copy];
            
            [arrPersonne removeObjectAtIndex:index];
            [cell addPersone:[arrPersonne copy]];
            [self.tableControl reloadData];
            //                    [self updateSuggestView:cell.tfInput];
            [suggestViewPersonne removeItemSelected:dicTmp];
        }];
        cell.tfInput.delegate = self;
        [cell.tfInput addTarget:self action:@selector(textChange:) forControlEvents:UIControlEventEditingChanged];
        cell.tfInput.text = self.strPersonne;
        [cell.tfInput setInputAccessoryView:self.keyboardToolbar];
        cell.tfInput.clearButtonMode = UITextFieldViewModeWhileEditing;
        cell.imageLeftTF.image = [UIImage imageNamed:@"icon_chasses_search_user"];
        return cell;
    }

    SignalerCheckBoxCell *cell = nil;
    
    if (indexPath.section == 0) {
        cell = (SignalerCheckBoxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection0 forIndexPath:indexPath];
    }else if (indexPath.section == 1) {
        cell = (SignalerCheckBoxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    }else if (indexPath.section == 2) {
        cell = (SignalerCheckBoxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection2 forIndexPath:indexPath];
    }else{
        cell = (SignalerCheckBoxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection3 forIndexPath:indexPath];
    }
    
    
    if (indexPath.section == 0)
    {
        NSDictionary*dic = [mesArr objectAtIndex:indexPath.row];
        [cell setListLabel:dic[@"categoryName"]];
        [cell setListImages:dic[@"categoryImage"]];
        [cell.tickButton setTag: indexPath.row + 10];
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            cell.imgCheckBox.image = self.imgCheckBoxActive;
        }else{
            cell.imgCheckBox.image = self.imgCheckBoxInActive;

        }
        
        [cell.tickButton addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (indexPath.section == 1)
    {
        NSDictionary*dic = [arrReceivers objectAtIndex:indexPath.row];
        
        
        [cell setListLabel:dic[@"name"]];
        [cell setListImages:@"ic_federation"];
        [cell.tickButton setTag: indexPath.row + 100];
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            cell.imgCheckBox.image = self.imgCheckBoxActive;
        }else{
            cell.imgCheckBox.image = self.imgCheckBoxInActive;
            
        }
        
        [cell.tickButton addTarget:self action:@selector(selectTickActionFederation:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    else if (indexPath.section == 2)
    {
        NSDictionary*dic = [sharingGroupsArray objectAtIndex:indexPath.row];
        NSString *strName = [dic[@"categoryName"] emo_emojiString];
        
        //Mes group
        [cell setListLabel:strName];
        [cell setListImages:@"sharechamp"];
        [cell.tickButton setTag: indexPath.row + 100];
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            cell.imgCheckBox.image = self.imgCheckBoxActive;
        }else{
            cell.imgCheckBox.image = self.imgCheckBoxInActive;
            
        }
        
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesGroup:) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (indexPath.section == 3)
    {
        NSDictionary*dic = [sharingHuntsArray objectAtIndex:indexPath.row];
        NSString *strName = [dic[@"categoryName"] emo_emojiString];
        
        NSNumber *num = dic[@"isSelected"];
        if ( [num boolValue]) {
            cell.imgCheckBox.image = self.imgCheckBoxActive;
        }else{
            cell.imgCheckBox.image = self.imgCheckBoxInActive;
            
        }
        //Mes hunt
        [cell setListLabel:strName];
        [cell setListImages:@"sharechamp"];
        [cell.tickButton setTag: indexPath.row + 100];
        [cell.tickButton addTarget:self action:@selector(selectTickActionMesHunts:) forControlEvents:UIControlEventTouchUpInside];
    }

    cell.slideTitleLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = self.colorCellBackGround;

    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    [self fnSetPosittionSearch];
}
-(void)selectTickAction:(id)sender{
    NSInteger index =[sender tag] - 10;
    NSMutableDictionary *dic=[[mesArr objectAtIndex:index] mutableCopy];
    BOOL isSelected = [dic[@"isSelected"] boolValue];


    if (index == 2 ) {
        isOpenSearch = !isSelected;
        [arrPersonne removeAllObjects];
        
        [dic setValue:[NSNumber numberWithBool:isOpenSearch] forKey:@"isSelected"];
        [mesArr replaceObjectAtIndex:index withObject:dic];
        [self.tableControl reloadData];
        /*
        [arrPersonne removeAllObjects];
        strSearch = @"";
        if ([sender isSelected])
        {
            [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
            [vcSearch fnHide:YES];
            [vcSearch reloadData:nil];
        }
        else
        {
            [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
            [vcSearch reloadData:nil];
        }
        
        [self checkHeightTableWithCount:0];
        [self fnSetPosittionSearch];
        [mesArr replaceObjectAtIndex:index withObject:dic];
        [self.tableControl reloadData];
         */
        
    }
    else if (index > 2)
        {
        }
    else
    {
        if (isSelected)
            [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
        else
        {
            //reset selection...
            //Amis
            NSMutableDictionary *dic1 = [mesArr[0] mutableCopy];
            [dic1 setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            [mesArr replaceObjectAtIndex:0 withObject:dic1];
            //natiz
            NSMutableDictionary *dic2 = [mesArr[1] mutableCopy];
            [dic2 setObject:[NSNumber numberWithBool:NO] forKey:@"isSelected"];
            [mesArr replaceObjectAtIndex:1 withObject:dic2];
            
            //set for one.
            
            //refresh
            [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
        }
        
        [mesArr replaceObjectAtIndex:index withObject:dic];
        [self.tableControl reloadData];
    }
}

-(IBAction)selectTickActionMesGroup:(UIButton*)sender
{
    NSMutableDictionary *dic=[[sharingGroupsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    BOOL isSelected = [dic[@"isSelected"] boolValue];

    if (isSelected)
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingGroupsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}

-(IBAction)selectTickActionMesHunts:(UIButton*)sender
{
    NSMutableDictionary *dic=[[sharingHuntsArray objectAtIndex:[sender tag] - 100] mutableCopy];
    BOOL isSelected = [dic[@"isSelected"] boolValue];

    if (isSelected)
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [sharingHuntsArray replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}



-(void)selectTickActionFederation:(UIButton *) sender
{
    NSMutableDictionary *dic=[[arrReceivers objectAtIndex:[sender tag] - 100] mutableCopy];
    BOOL isSelected = [dic[@"isSelected"] boolValue];

    if (isSelected)
        [dic setValue:[NSNumber numberWithBool:FALSE] forKey:@"isSelected"];
    else
        [dic setValue:[NSNumber numberWithBool:TRUE] forKey:@"isSelected"];
    
    [arrReceivers replaceObjectAtIndex:[sender tag]-100 withObject:dic];
    
    [self.tableControl reloadData];
}
-(IBAction)annulerAction:(id)sender
{
    [self gotoback];
}
- (IBAction)onNext:(id)sender {
    ///"receivers":[{"receiver":2},{"receiver":3}]
    
    int  MDshare =0;
    //amis
    if ([mesArr[0][@"isSelected"]boolValue]) {
        MDshare =1;
    }
    
    //naturapass member
    else if ([mesArr[1][@"isSelected"]boolValue]) {
        MDshare =3;
    }
    if (self.isEditFavo) {
        //favo
        NSMutableDictionary *dicFavo = [NSMutableDictionary dictionaryWithDictionary:[PublicationOBJ sharedInstance].dicFavoris];
        NSMutableArray *arrGroup =[NSMutableArray new];
        
        if (sharingGroupsArray) {
            for (NSDictionary *dic in sharingGroupsArray) {
                NSNumber *num = dic[@"isSelected"];
                if ( [num boolValue]) {
                    [arrGroup addObject:@{@"id":dic[@"groupID"],@"name":dic[@"categoryName"]}];
                }
            }
            [dicFavo setValue:arrGroup forKey:@"groups"];
        }
        NSMutableArray *arrHunt =[NSMutableArray new];
        if (sharingHuntsArray) {
            for (NSDictionary *dic in sharingHuntsArray) {
                NSNumber *num = dic[@"isSelected"];
                if ( [num boolValue]) {
                    [arrHunt addObject:@{@"id":dic[@"huntID"],@"name":dic[@"categoryName"]}];
                    
                    
                }
            }
            [dicFavo setValue:arrHunt forKey:@"hunts"];
        }
        NSMutableArray *arrUser =[NSMutableArray new];
        if (arrPersonne) {
            for (NSDictionary *dic in arrPersonne) {
//                NSNumber *num = dic[@"status"];
//                if ( [num boolValue]) {
                    [arrUser addObject:dic];
                    
//                }
            }
            [dicFavo setValue:arrUser forKey:@"sharingUsers"];
        }
        [dicFavo setValue:[NSNumber numberWithInt:MDshare] forKey:@"iSharing"];
        [dicFavo setValue:@{@"share":[NSNumber numberWithInt:MDshare]} forKey:@"sharing"];
        [dicFavo setValue:arrReceivers forKey:@"receivers"];
        [PublicationOBJ sharedInstance].dicFavoris = [dicFavo mutableCopy];
        
        [self gotoback];
        
    }
    else
    {
            NSMutableArray *arrTmp = [NSMutableArray new];
            for (NSDictionary *dicPersonne in arrPersonne) {
                [arrTmp addObject:dicPersonne[@"id"]];
            }
            NSString *strUser = @"";
            if (arrTmp.count > 0) {
                strUser = [arrTmp componentsJoinedByString:@","];
            }
        
        //save sharing option: Amis/Natiz/Other
            NSMutableDictionary *dicPatarge = [@{@"iSharing":[NSNumber numberWithInt:MDshare]} mutableCopy];
        
            [dicPatarge setObject:[NSNumber numberWithBool:isOpenSearch] forKey:@"isOpenSearch"];
        
            if (sharingGroupsArray) {
                [dicPatarge setObject:sharingGroupsArray forKey:@"sharingGroupsArray"];
            }
            if (sharingHuntsArray) {
                [dicPatarge setObject:sharingHuntsArray forKey:@"sharingHuntsArray"];
            }
            if (arrReceivers) {
                [dicPatarge setObject:arrReceivers forKey:@"receivers"];
            }
            if (arrPersonne) {
                [dicPatarge setObject:arrPersonne forKey:@"personne"];
            }
            if (strUser) {
                [dicPatarge setObject:strUser forKey:@"sharingUser"];
            }
        [PublicationOBJ sharedInstance].dicPatarge = dicPatarge;
        [self gotoback];
    }
}

-(IBAction)fnMonPartage:(id)sender
{
    MurParameter_Publication *viewController1 = [[MurParameter_Publication alloc] initWithNibName:@"MurParameter_Publication" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:self.expectTarget iAmParent:YES];

}

//MARK: - Search Personne
-(void)createSuggestViewWithSuperView:(UIView*)view withRect:(CGRect)rect withIndexPath:(NSIndexPath*)indexPath
{
    __weak typeof(self) wself = self;

    [suggestViewPersonne fnDataInput:self.strPersonne withArrSelected:arrPersonne];
    [suggestViewPersonne doBlock:^(NSArray *arrResult) {
        [wself resignKeyboard:nil];
        
        [arrPersonne removeAllObjects];
        
        if(arrResult)
        {
            //check if it available
//            NSDictionary *dicSelected = arrResult[0];
//            
//            for (NSDictionary*dic in arrPersonne) {
//                if ([dic[@"id"] intValue] == [dicSelected[@"id"] intValue]) {
//                    //exist => return
//                    return;
//                }
//            }
            //else
            
            [arrPersonne addObjectsFromArray:arrResult];
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableControl reloadData];
        });
    }];
    [suggestViewPersonne showAlertWithSuperView:view withRect:rect];
}
- (void)keyboardWillHide:(NSNotification*)notification {
    [self removeSuggestView];
}
-(void)removeSuggestView
{
    if([suggestViewPersonne isDescendantOfView:self.tableControl]) {
        [suggestViewPersonne closeAction:nil];
    }
}
- (void)resignKeyboard:(id)sender
{
    [self.view endEditing:YES];
    [self removeSuggestView];
    
}
-(IBAction)textChange:(id)sender
{
    UITextField *searchText = (UITextField*)sender;
    NSString *strsearchValues= searchText.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    searchText.text=strsearchValues;
    [self processLoungesSearchingURL:searchText.text];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSString *strsearchValues= textField.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    textField.text=strsearchValues;
    [self processLoungesSearchingURL:strsearchValues];
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIView *parent = [textField superview];
    while (parent && ![parent isKindOfClass:[ChassesCreateBaseCell class]]) {
        parent = parent.superview;
    }
    
    ChassesCreateBaseCell *cell = (ChassesCreateBaseCell *)parent;
    
    NSIndexPath *indexPath = [self.tableControl indexPathForCell:cell];
    
    CGRect myRect = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect rectTf = cell.viewInput.frame;
    
    CGRect rectSubView = CGRectMake(myRect.origin.x + rectTf.origin.x, myRect.origin.y + rectTf.origin.y + rectTf.size.height - 10, rectTf.size.width, 200);
    [self createSuggestViewWithSuperView:self.tableControl withRect:rectSubView withIndexPath:indexPath];
    [self.tableControl bringSubviewToFront:cell.viewInput];
    
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //    [self removeSuggestView];
}
- (void) processLoungesSearchingURL:(NSString *)searchText
{
    self.strPersonne = searchText;
    [suggestViewPersonne processLoungesSearchingURL:searchText];
}
/*
-(void)searchPersonne
{
    __weak typeof(self) wself = self;
    vcSearch = [[AlertSearchPersonneVC alloc] init];
    [vcSearch doBlock:^(NSArray *arr, float count) {
        if (strSearch.length < VAR_MINIMUM_LETTRES)
        {
            vcSearch.hidden = YES;
        }
        else
        {
            vcSearch.hidden = NO;
            
        }
        
        if (count >=0) {
            if (count == 0) {
                vcSearch.hidden = YES;
            }
            [self checkHeightTableWithCount:count];
        }
        else
        {
            [wself.view endEditing:YES];
            //enable request
            for (NSDictionary *dic in arr) {
                BOOL isExit = false;
                for (NSDictionary *dicPersonne in arrPersonne) {
                    if ([dic[@"id"] intValue] == [dicPersonne[@"id"] intValue]) {
                        isExit = true;
                        break;
                    }
                }
                if (!isExit) {
                    [arrPersonne addObject:dic];
                }
            }
            //natiz
            NSMutableDictionary *dic2 = [mesArr[2] mutableCopy];
            [dic2 setObject:[NSNumber numberWithBool:arrPersonne.count >0? YES: NO] forKey:@"isSelected"];
            [mesArr replaceObjectAtIndex:2 withObject:dic2];
            [wself.tableControl reloadData];
            vcSearch.hidden = YES;
        }
    }];
    [vcSearch fnHide:YES];
    [vcSearch addContraintSupview:self.tableControl];
}
-(void)fnSetPosittionSearch
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:2 inSection:0];
    //
    CGRect cellRectInTable = [self.tableControl rectForRowAtIndexPath:indexPath];
    CGRect cellInSuperview = [self.tableControl convertRect:cellRectInTable toView:[self.tableControl superview]];
    // NSLog(@"x: %f y: %f",cellInSuperview.origin.x,cellInSuperview.origin.y);
    vcSearch.constraintTable_Y.constant = cellInSuperview.origin.y + 116;
}

-(void)checkHeightTableWithCount:(int)count
{
    if (count< kMaxNumberCell) {
        vcSearch.constraintHeightTable.constant = count*kHeightCell;
    }
    else
    {
        vcSearch.constraintHeightTable.constant = kMaxNumberCell*kHeightCell;
    }
}
-(void) showKeyBoardSearch:(UIButton*)sender
{
    UIView *view = sender.superview;
    while (view && ![view isKindOfClass:[UITableViewCell self]]) view = view.superview;
    SignalerPeopleCell *cell = (SignalerPeopleCell *)view;
    
    [cell.tfSearch becomeFirstResponder];
}

-(IBAction)searchPersonneAction:(id)sender
{
    if (strSearch.length >= VAR_MINIMUM_LETTRES)
    {
        [self fnSetPosittionSearch];
        [vcSearch reloadData:arrPersonne];
        [vcSearch processLoungesSearchingURL:strSearch];
    }
    else
    {
        vcSearch.hidden = YES;
    }
}
-(void)textChanged:(UITextField *)textField
{
    strSearch = textField.text;
    [self searchPersonneAction: nil];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self searchPersonneAction: nil];
    [self.view endEditing:YES];
    return YES;
}

-(IBAction)removeItemChoose:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    int index = (int)btn.tag - 300 - 3;
    [arrPersonne removeObjectAtIndex:index];
    [self.tableControl reloadData];
}
*/
@end
