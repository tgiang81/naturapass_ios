//
//  PosterBaseVC.h
//  Naturapass
//
//  Created by JoJo on 9/7/18.
//  Copyright © 2018 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
@interface PosterBaseVC : BaseVC
@property(nonatomic,strong)  UIColor *colorBackGround;
@property(nonatomic,strong)  UIColor *colorNavigation;
@property(nonatomic,strong)  UIColor *colorCellBackGround;
@property(nonatomic,strong)  UIColor *colorAnnuler;
-(void)gobackParent;
@end
