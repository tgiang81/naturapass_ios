//
//  AlertSelectListNaturalive.m
//  Naturapass
//
//  Created by giangtu on 1/20/19.
//  Copyright © 2019 Appsolute. All rights reserved.
//

#import "AlertSelectListNaturalive.h"
#import "FileHelper.h"
#import "Define.h"
#import "AppCommon.h"
#import "NSString+EMOEmoji.h"
#import "CellSelectListnaturalive.h"
#import "LiveHuntOBJ.h"

@interface AlertSelectListNaturalive ()
{
    
    __weak IBOutlet UITableView *tbData;
    NSArray*arrData;
}
@end

static NSString *identifierCellHome = @"CellHome";

@implementation AlertSelectListNaturalive

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [tbData registerNib:[UINib nibWithNibName:@"CellSelectListnaturalive" bundle:nil] forCellReuseIdentifier:identifierCellHome];

    // Do any additional setup after loading the view from its nib.
    NSString *strPath = [ FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],LIVE_HUNT_HOME_SAVE) ];
    
    arrData = [NSArray arrayWithContentsOfFile:strPath];

    //Sorting array via date...recent date at first.
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"meeting.date_begin"  ascending:YES];
    arrData = [arrData sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];

    [tbData reloadData];
}

#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellSelectListnaturalive *cell = (CellSelectListnaturalive *)[tbData dequeueReusableCellWithIdentifier:identifierCellHome];
    
    NSDictionary *dic = arrData[indexPath.row];
    NSString *strName = [dic[@"name"] emo_emojiString];
    
    cell.name.text = strName;
    [cell.mDate  setAttributedText:[[LiveHuntOBJ sharedInstance] formatLiveHuntTimeWithDateBegin: dic[@"meeting"][@"date_begin"] withDateEnd:dic[@"meeting"][@"date_end"]]];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}

@end
