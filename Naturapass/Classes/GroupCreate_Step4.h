//
//  GroupCreate_Step4.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateBaseVC.h"

@interface GroupCreate_Step4 : GroupCreateBaseVC
@property(nonatomic,strong) NSMutableArray *arrSubcriber;
@end
