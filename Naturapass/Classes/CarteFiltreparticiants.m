//
//  PhotoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "CarteFiltreparticiants.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "MDCheckBox.h"
#import "GroupCreateCell_Step4.h"
static NSString *cell2 =@"GroupCreateCell_Step4";
static NSString *cellID2 =@"cellID8";


@interface CarteFiltreparticiants () <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSArray * arrData;
    __weak IBOutlet UILabel *lblDesctiption;
    __weak IBOutlet UIButton *btnClose;
    __weak IBOutlet UIButton *btnValider;

}
@property (nonatomic,strong) IBOutlet UITableView *tableControl;
@property (nonatomic,strong) IBOutlet UISwitch *btnFilterControl;

@end

@implementation CarteFiltreparticiants
#pragma mark - init
-(id)initFromParent
{
    self = [super initWithNibName];
    if (self) {
        // Custom initialization
    }
    return self;
    
}
#pragma mark - viewdid
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addSubContent:@"CarteFiltreparticiants"];
    [btnClose setTitle:str(strAnuler)forState:UIControlStateNormal];
    [btnValider setTitle:str(strValider) forState:UIControlStateNormal];

    lblDesctiption.text = str(strSelectionLesParticipants);
    [self.lbTitle setText:str(strFilterParTypeDeContenu)];
    
    [self.tableControl registerNib:[UINib nibWithNibName:cell2 bundle:nil] forCellReuseIdentifier:cellID2];
    [self updateUI];
    //
    
    NSMutableDictionary *dic1 = [@{@"name":@"Roger Marin",
                                   @"image":@"cart_photo"} copy];
    
    NSMutableDictionary *dic2 = [@{@"name":@"Bertrand Nedved",
                                   @"image":@"cart_photo"} copy];
    NSMutableDictionary *dic3 = [@{@"name":@"Michel Dupond",
                                   @"image":@"cart_photo"} copy];
    arrData =  [@[dic1,dic2,dic3] copy];
}
-(void) updateUI
{
    NSDictionary   *filterDic = [[NSUserDefaults standardUserDefaults] objectForKey : [NSString stringWithFormat:@"OBJECT_FILTER_%@",[COMMON getUserId]]];
    [_btnFilterControl setOnTintColor:UIColorFromRGB(ON_SWITCH_CARTE)];
    _btnFilterControl.transform = CGAffineTransformMakeScale(0.75, 0.75);

    //Load option
    BOOL bFilterON;
    if ([filterDic[@"bFilterON"] boolValue] ) {
        bFilterON = YES;
    }
    else
    {
        bFilterON = NO;
    }
    [_btnFilterControl setOn:bFilterON];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
}
-(void) addSubContent:(NSString*) nameView
{
    NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:nameView owner:self options:nil];
    self.vSubContent = (UIView*)[nibArray objectAtIndex:0];
    [self.vContent addSubview:self.vSubContent];
    [self addContraintView:self.vSubContent];
    self.constraintHeight.constant =self.vSubContent.frame.size.height;
    
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dic = arrData[indexPath.row];

    GroupCreateCell_Step4 *cell = (GroupCreateCell_Step4 *)[self.tableControl dequeueReusableCellWithIdentifier:cellID2 forIndexPath:indexPath];
    [cell.btnInvite setTypeCheckBox:UI_GROUP_MUR_ADMIN];
    
    [cell.lblTitle setText:dic[@"name"]];

    
    cell.imgViewAvatar.image=[UIImage imageNamed: dic[@"image"]] ;
    cell.btnInviteOver.tag = indexPath.row;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return  cell;
}
@end
