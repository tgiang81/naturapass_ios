//
//  Parameter_Notification_Detail.h
//  Naturapass
//
//  Created by Giang on 8/3/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "ParameterBaseVC.h"
#import "CellKind7.h"


@interface Parameter_Notification_Detail : ParameterBaseVC
@property(nonatomic,strong) NSArray *arrDataFull;
@end
