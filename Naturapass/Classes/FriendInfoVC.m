//
//  FriendInfoViewController_New.h
//  Naturapass
//

#import "FriendInfoVC.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "Config.h"
#import "AppCommon.h"
#import "WebServiceAPI.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "ErrorViewController.h"
#import "ServiceHelper.h"
#import "ASSharedTimeFormatter.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "Define.h"
#import "CommentDetailVC.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "MediaCell+Color.h"

#import "MurVC.h"
#import "GroupEnterMurVC.h"

#import "ListAMIS.h"
#import "ListAMIS_COMMON.h"
#import "MurEditPublicationVC.h"
#import "ChatVC.h"
#import "MapLocationVC.h"
#import "MurPeopleLikes.h"
#import <IDMPhotoBrowser/IDMPhotoBrowser.h>
#import "FriendMurView.h"
#define LIKETAG             1000
#define UNLIKETAG           2000
#define COMMENTBUTTONTAG    3000
#define LOCATIONBUTTONTAG   4000


#define PUBLICATION_RESPONSE    0
#define LIKE_RESPONSE           1
#define UNLIKE_RESPONSE         2
#define COMMENT_RESPONSE        3
#define PUBLICATION_OFFSET_RESPONSE     4
#define SEARCH_COUNT                    5
#define FRIENDS_COUNT                   6
#define SIGNAL_COUNT                    7
#define DELETEACTION                    8
#define SIGNAL_RESPONSE                 9
#define GETGROUPRESPONSE                10
#define POSTDEVICE                      11
#define SLIDETYPE                       12

static NSString *MDMediaCell = @"MediaCell";
static NSString *MDMediaCellID = @"MediaCellID";

@interface FriendInfoVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate,WebServiceAPIDelegate,CLLocationManagerDelegate,IDMPhotoBrowserDelegate>{
    
    BOOL loadedResponse;
    
    BOOL typeOfAPILoaded;
    
    NSInteger selectedSection;
    
    UIView                  *refreshFooterView;
    UILabel                 *refreshFooterLabel;
    UILabel                 *timeFooterLabel;
    UIImageView             *refreshFooterArrow;
    UIActivityIndicatorView *refreshFooterSpinner;
    NSString                 *textFooterLoading;
    NSString                 *textFooterPull;
    NSString                 *textFooterRelease;
    BOOL                     isFooterDragging;
    //check refreshHeader or refreshFooter
    BOOL isFooter;
    NSMutableArray          *searchAsyncImageArray;
    CLLocationManager       *locationManager;
    NSTimer                 *progressTimer;
    
    __weak IBOutlet UIImageView *image1;
    
    __weak IBOutlet UIImageView *image2;
    __weak IBOutlet UIButton *btnAjouter;
    __weak IBOutlet UIButton *btnMessage;
    __weak IBOutlet UIImageView *imgAjouter;
    __weak IBOutlet UIButton *btnListAmis;
    FriendMurView                         *murView;

}
@property (nonatomic, strong) MediaCell *prototypeCell;
@property (nonatomic,strong)   IBOutlet UILabel  *lbMessage;
@property (nonatomic,strong)   IBOutlet UIView      *viewContent;

@end

@implementation FriendInfoVC
@synthesize strSharing;
@synthesize strTag;
@synthesize slideShareArray;
@synthesize slideSTR;

@synthesize strPush;
@synthesize push_ID;
@synthesize pushType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.vContainer.backgroundColor = UIColorFromRGB(NEW_MUR_BACKGROUND_COLOR);
    friendLbl.hidden = YES;
    image1.hidden =YES;
    mutualFriendLbl.hidden = YES;
    image2.hidden = YES;
    btnListAmis.hidden = YES;

    // neu chua la ban thi hien thi button mau xanh
    //neu la ban roi thi cho phep xoa la ban hien thi nut mau do
    // friendship =0 chua la ban
    // friendship =1  cho xac nhan la ban
    //frienship =2 da la ban
    
    /*
     relation =     {
     friendship = 0;
     mutualFriends = 0;
     };
     
     */
    __weak typeof(self) wself = self;
    //add view mur
    /*
     [murView fnSetDataPublication:publicGroupArray];
     [murView stopRefreshControl];
     [murView startRefreshControl];
     [murView fnSetDataPublication:publicGroupArray];
     
     */
    murView = [[FriendMurView alloc] initWithEVC:self expectTarget:self.expectTarget];
    murView.isLiveHunt = self.isLiveHunt;
    murView.friendDic = [_friendDic copy];
    [murView setCallback:^(VIEW_ACTION_TYPE type, NSArray *arrData)
     {
         switch (type) {
             case VIEW_ACTION_REFRESH_TOP:
             {
                 [wself insertRowAtTop];
             }
                 break;
             case VIEW_ACTION_REFRESH_BOTTOM:
             {
                 [wself insertRowAtBottom];
             }
                 break;
             case VIEW_ACTION_UPDATE_DATA:
             {
                 [wself updateDataPublication:arrData];
             }
                 break;
             default:
                 break;
         }
     }];
    [murView addContraintSupview:self.viewContent];
        
    //INIT
    searchedArray = [[NSMutableArray alloc] init];
    commentArray = [[NSMutableArray alloc]init];
    publicationArray =[[NSMutableArray alloc]init];
    
    isPostDevice=YES;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            
            // Do any additional setup after loading the view.
            [self addMainNav:@"MainNavSignaler"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:@"PROFIL"];
            [subview.myDesc setText:@""];
            subview.imgClose.hidden = TRUE;
            subview.btnClose.hidden = TRUE;
            subview.imgBackground.image = [UIImage imageNamed:@"mur_top_bar_bg"];
            //Change status bar color
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            //Change background color
            subview.backgroundColor = UIColorFromRGB(MUR_MAIN_BAR_COLOR);
            self.showTabBottom = YES;
            self.murV3 = YES;
            
            [self addSubNav:nil];
        }
            break;
        case ISGROUP:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strGROUPES)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(GROUP_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
        }
            break;
        case ISLOUNGE:
        {
            UINavigationBar *navBar=self.navigationController.navigationBar;
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
            
            //add sub navigation
            [self addMainNav:@"MainNavMUR"];
            
            //Set title
            MainNavigationBaseView *subview =  [self getSubMainView];
            [subview.myTitle setText:str(strCHANTIERS)];
            
            //Change background color
            subview.backgroundColor = UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
            
            
            [self addSubNav:@"SubNavigationGROUPENTER"];
        }
            break;
        default:
            break;
    }
    
    if ([self isKindOfClass:[FriendInfoVC class]]
        ) {
        self.showTabBottom = YES;
    }
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            image1.image = [UIImage imageNamed:@"admin"];
            image2.image = [UIImage imageNamed:@"admin"];
            
            [self fnTheme:UIColorFromRGB(MUR_MAIN_BAR_COLOR)];
        }
            break;
        case ISGROUP:
        {
            image1.image = [UIImage imageNamed:@"amis_groupe"];
            image2.image = [UIImage imageNamed:@"amis_groupe"];
            [self fnTheme:UIColorFromRGB(GROUP_MAIN_BAR_COLOR)];
        }
            break;
        case ISLOUNGE:
        {
            image1.image = [UIImage imageNamed:@"amis_chasse"];
            image2.image = [UIImage imageNamed:@"amis_chasse"];
            [self fnTheme:UIColorFromRGB(CHASSES_MAIN_BAR_COLOR)];
        }
            break;
            
        default:
            break;
    }
    
    if (self.isNotifi) {
        [self.subview removeFromSuperview];
        [self addSubNav:@"SubNavigationMUR"];
        
        
        self.isNotifi =!self.isNotifi;
    }

    
    [self setThemeNavSub:NO withDicOption:nil];
    
    UIButton *btn = [self returnButton];
    [btn setSelected:YES];
    
    //if alreadi friend...-> hidden btnAjoute
    [murView getTheFriends];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([COMMON isLoggedIn]) {
        //Check Webservice Loaded
        
        if(!loadedResponse) {
            if([COMMON isLoggedIn])
                [murView startRefreshControl];
        } else {
            // reload data when base on the filter
            [self performSelector:@selector(getPublications) withObject:nil afterDelay:1];
        }
    }
    
    UINavigationBar *navBar=self.navigationController.navigationBar;
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            [navBar setBarTintColor: UIColorFromRGB(MUR_TINY_BAR_COLOR) ];
            
        }
            break;
        case ISGROUP:
        {
            [navBar setBarTintColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR) ];
            
        }
            break;
        case ISLOUNGE:
        {
            [navBar setBarTintColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR) ];
        }
            break;
        default:
            break;
    }
}
-(void) onMainNavClick:(UIButton*)btn
{
    //Kill map
    
    switch (btn.tag -START_SUB_NAV_TAG) {
        case 0://back
        {
            [self gotoback];
        }
            break;
        default:
            break;
            
    }
}
#pragma SUB - NAV EVENT

-(void)  onSubNavClick:(UIButton *)btn{
    
    switch (self.expectTarget) {
        case ISMUR:
        {
            [self clickSubNav_Mur: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISGROUP:
        {
            [self clickSubNav_Group: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        case ISLOUNGE:
        {
            [self clickSubNav_Chass: (int)btn.tag - START_SUB_NAV_TAG];
        }
            break;
        default:
            break;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [commentView setHidden:YES];
    strPosting=@"";
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}

-(void)fnTheme:(UIColor*)color
{
    //    [btnAjouter setBackgroundColor:color];
    [btnMessage setBackgroundColor:color];
    [friendLbl setTextColor:color];
    [mutualFriendLbl setTextColor:color];
}
#pragma mark - GET MUR
-(void)checkEmpty:(NSMutableArray*)arr
{
    if (arr.count>0) {
        self.lbMessage.hidden=YES;
        _lbMessage.text=@"";
        
    }
    else
    {
        if ([_friendDic isKindOfClass:[NSDictionary class]]) {
            _lbMessage.text=[NSString stringWithFormat:EMPTY_FRIENDINFO,_friendDic [@"firstname"]];
        }
        self.lbMessage.hidden=NO;
    }
}

//overwrite
- (void)insertRowAtTop {
    [self getPublications];
}

//overwrite
- (void)insertRowAtBottom {
    WebServiceAPI *serviceAPI =[ WebServiceAPI new];
    
    [serviceAPI getPublicationsUserAction:mur_limit forUserId:(NSString *)_friendDic [@"id"] withOffset:[NSString stringWithFormat:@"%ld",(long)publicationArray.count]];
    __weak typeof(self) wself = self;
    
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [murView stopRefreshControl];
        strPosting=@"";
        loadedResponse = YES;
        typeOfAPILoaded = NO;
        [COMMON removeProgressLoading];
        if ([wself fnCheckResponse:response])
        {
            [self checkEmpty:publicationArray];
            return ;
        }
        NSArray *offsetArray = [response valueForKey:@"publications"];
        if([offsetArray count]){
            for (int i = 0; i<[offsetArray count]; i++) {
                [publicationArray addObject:[offsetArray objectAtIndex:i]];
            }
            [murView fnSetDataPublication:publicationArray];
        }
        [self checkEmpty:publicationArray];

    };
}
-(void)getPublications
{
    __weak typeof(self) wself = self;
    [COMMON addLoading:self];
    WebServiceAPI *serviceAPI =[ WebServiceAPI new];
    [serviceAPI getPublicationsUserAction:mur_limit forUserId:(NSString *)_friendDic [@"id"] withOffset:@"0"];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        [murView stopRefreshControl];
        [COMMON removeProgressLoading];
        if([strRemove isEqualToString:@"REMOVE"])
            [publicationArray removeAllObjects];
        
        if ([wself fnCheckResponse:response])
        {
            [self checkEmpty:publicationArray];
            return ;
        }
        NSArray *offsetArrays = response [@"publications"];
        for (int i = 0; i<[offsetArrays count]; i++) {
            
            NSDictionary *singleMesDic = [offsetArrays objectAtIndex:i];
            NSArray *temppublicationArr = [publicationArray mutableCopy];
            NSUInteger mesObjIndex = [temppublicationArr indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop){
                NSInteger singleMesId = [singleMesDic [@"id"] integerValue];
                NSInteger compareMesId = [obj[@"id"] integerValue];
                return (singleMesId==compareMesId);
            }];
            
            if(mesObjIndex<publicationArray.count){
                [publicationArray replaceObjectAtIndex:mesObjIndex withObject:singleMesDic];
            }
            else
                [publicationArray insertObject:singleMesDic atIndex:0];
            
            if([strPosting isEqualToString:@"POSTING"])
                
                publicationArray= [response[@"publications"]mutableCopy];
            
            publicationArray = [[publicationArray sortedArrayUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"created" ascending:NO], nil]] mutableCopy];
            [murView fnSetDataPublication:publicationArray];
            
        }
        [self checkEmpty:publicationArray];
        [murView fnSetDataPublication:publicationArray];
    };
    
}
-(void)updateDataPublication:(NSArray*)arrData
{
    publicationArray = [arrData mutableCopy];
    [self checkEmpty:publicationArray];
    
}
#pragma mark  -  FRIEND ACTION
-(void)getTheFriends
{
    //update counter
    //COMMUN
    __weak typeof(self) wself = self;
    WebServiceAPI *serviceAPI =[WebServiceAPI new];
    [serviceAPI getTheFriends:YES withID:_friendDic [@"id"]];
    serviceAPI.onComplete =^(id response, int errCode)
    {
        
        if ([wself fnCheckResponse:response]) return ;
        if (response!=nil) {
            if ([response isKindOfClass: [NSDictionary class]]) {
                NSArray *localArr = (NSArray*) response[@"friends"];
                if(localArr.count == 0)
                {
                    friendLbl.hidden = YES;
                    image1.hidden =YES;
                }else{
                    friendLbl.hidden = NO;
                    image1.hidden =NO;
                    NSString *strAA = @"";
                    if (localArr.count >1) {
                        strAA = str(strAmis);
                    }
                    else
                    {
                        strAA = str(strAmi);
                        
                    }
                    [friendLbl setText: [NSString stringWithFormat:@"%lu %@",(unsigned long)localArr.count,strAA ] ];
                }
            }
        }
        
    };
    
    //COMMUN
    WebServiceAPI *serviceAPI2 =[WebServiceAPI new];
    
    [serviceAPI2 getTheFriends:NO withID:_friendDic [@"id"]];
    serviceAPI2.onComplete =^(id response, int errCode)
    {
        mutualFriendLbl.hidden = NO;
        image2.hidden =NO;
        
        if ([wself fnCheckResponse:response]) return ;
        if (response!=nil) {
            if ([response isKindOfClass: [NSDictionary class]]) {
                NSArray *localArr = (NSArray*) response[@"friends"];
                NSString *strAA = @"";
                if (localArr.count >1) {
                    strAA = str(strAmis);
                }
                else
                {
                    strAA = str(strAmi);
                    
                }
                [mutualFriendLbl setText: [NSString stringWithFormat:@"%lu %@ en commun",(unsigned long)localArr.count,strAA ] ];
            }
        }
        
    };
    
}

- (IBAction)addFriendButtonAction:(id)sender
{
    if (!btnAjouter.isSelected) {
        [COMMON addLoading:self];
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        [serviceObj postUserFriendShipAction:(NSString *)_friendDic [@"id"]];
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            [COMMON removeProgressLoading];
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strREQUESTSENT) delegate:self cancelButtonTitle:nil otherButtonTitles:str(strOK), nil];
            alert.tag = 50;
            [alert show];
            if ([response[@"relation"] isKindOfClass:[NSDictionary class]]) {
                [self checkAjouterButton:YES];
                [self getTheFriends];
            }
            
        };
    }
    else
    {
        //Etes-vous sûr de vouloir supprimer xxx de votre liste d'amis?
        NSString *strMessage = [NSString stringWithFormat:str(strSupprimer_de_votre_damis),_friendDic [@"fullname"]];
        [UIAlertView showWithTitle:str(strTitle_app)
                           message:strMessage
                 cancelButtonTitle:str(strNon)
                 otherButtonTitles:@[str(strOui)]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex == [alertView cancelButtonIndex]) {
                              }
                              else
                              {
                                  [COMMON addLoading:self];
                                  WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                                  [serviceObj deleteUserFriendShipAction:(NSString *)_friendDic [@"id"]];
                                  serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                                      [COMMON removeProgressLoading];
                                      if ([response[@"relation"] isKindOfClass:[NSDictionary class]] || response[@"success"]) {
                                          [self checkAjouterButton:NO];
                                          [self getTheFriends];
                                      }
                                  };
                              }
                          }];
        
    }
    
}
-(void)checkAjouterButton:(BOOL)isSelected
{
    btnAjouter.selected =isSelected;
    if (isSelected) {
        btnAjouter.backgroundColor =UIColorFromRGB(MUR_CANCEL);
        imgAjouter.image = [UIImage imageNamed:@"delete-friend-button-icon"];
        [btnAjouter setTitle:str(strSUPPRIMER) forState:UIControlStateNormal];
        
    }
    else
    {
        switch (self.expectTarget) {
            case ISMUR:
            {
                btnAjouter.backgroundColor =UIColorFromRGB(MUR_MAIN_BAR_COLOR);

            }
                break;
            case ISLOUNGE:
            {
                btnAjouter.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);

            }
                break;
            case ISGROUP:
            {
                btnAjouter.backgroundColor =UIColorFromRGB(GROUP_MAIN_BAR_COLOR);

            }
                break;
            default:
                break;
        }
        imgAjouter.image = [UIImage imageNamed:@"group_icon1"];
        [btnAjouter setTitle:str(strAJOUTER) forState:UIControlStateNormal];
        
        
    }
}
/*
 courtesy = 1;
 firstname = My;
 fullname = "My Teater";
 id = 109;
 lastname = Teater;
 photo = "/uploads/users/images/thumb/a353c2fa7bfa754a699d979d7b9a6f8366b2a5e8.jpeg";
 relation =     {
 friendship =         {
 state = 2;
 way = 2;
 };
 mutualFriends = 1;
 };
 usertag = "my-teater";
 */

- (IBAction)chatButtonAction:(id)sender
{
    if (_friendDic[@"usertag"]) {
        NSMutableArray *arrUsers = [NSMutableArray new];
        
        [arrUsers addObject:@{@"id":_friendDic[@"id"],
                              @"text":_friendDic[@"fullname"],
                              @"usertag":_friendDic[@"usertag"]
                              }];
        ChatVC *viewController1 = [[ChatVC alloc] initWithNibName:@"ChatVC" bundle:nil];
        viewController1.arrFriendDiscussion = arrUsers;
        viewController1.IS_CREATE_SPECIAL = YES;
        [self pushVC:viewController1 animate:NO expectTarget:ISDISCUSS];
        
    }
}


-(IBAction)fnGotoListAmis:(id)sender
{
    ListAMIS *viewController1 = [[ListAMIS alloc] initWithNibName:@"ListAMIS" bundle:nil];
    viewController1.myDic = _friendDic ;
    [self pushVC:viewController1 animate:NO];
}

-(IBAction)fnGotoListCommonAmis:(id)sender
{
    ListAMIS_COMMON *viewController1 = [[ListAMIS_COMMON alloc] initWithNibName:@"ListAMIS_COMMON" bundle:nil];
    
    viewController1.myDic = _friendDic;
    
    [self pushVC:viewController1 animate:NO];
}

/*
 full link is the same process like publication. you have the link with "/resize/...", and to have full link, you have to replace "/resize/..." by "/original/..."(edited)
 */

- (IBAction)fnShowZoomerPhoto:(id)sender {
    
    NSString *strImage = nil;
    
    if (_friendDic[@"profilepicture"] != nil) {
        strImage = [NSString stringWithFormat:@"%@",_friendDic [@"profilepicture"]];
    }else{
        strImage = [NSString stringWithFormat:@"%@",_friendDic [@"photo"]];
    }
    strImage=[strImage stringByReplacingOccurrencesOfString:@"thumb" withString:@"original"];
    
    strImage=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,strImage];
    NSURL *urlImage = [NSURL URLWithString:strImage];
    
    IDMPhoto *aphoto = nil;
    
    if ([COMMON isReachable]) {
        aphoto = [IDMPhoto photoWithURL:urlImage];
    }
    else
    {
        aphoto = [IDMPhoto photoWithImage:profileImageview.image];
    }
    
    NSMutableArray *photos = [NSMutableArray new];
    
    [photos addObject:aphoto];
    // Create and setup browser
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
    browser.delegate = self;
    
    // Show
    [self presentViewController:browser animated:YES completion:nil];
    
}

- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser willDismissAtPageIndex:(NSUInteger)index
{
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSNumber *value = [NSNumber numberWithInt:UIDeviceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    //NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

@end
