//
//  CellKind2.h
//  Naturapass
//
//  Created by Giang on 8/6/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

typedef void (^callBackGroup) (NSInteger);

@interface CellKind9 : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UIImageView *imageLine;
@property (strong, nonatomic) IBOutlet UIImageView *arrow;

@property (nonatomic, copy) callBackGroup CallBackGroup;

@end
