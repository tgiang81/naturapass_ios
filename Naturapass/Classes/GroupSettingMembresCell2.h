//
//  ConfigurationMessageCustomCell.h
//  Naturapass
//
//  Created by ocsdeveloper9 on 2/27/14.
//  Copyright (c) 2014 OCSMobi - 11. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppCommon.h"

@interface GroupSettingMembresCell2 : UITableViewCell
{

}
@property (nonatomic,retain) IBOutlet UILabel *lbTitle;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar;
@property (nonatomic,retain) IBOutlet UIButton *btnValider;
@property (nonatomic,retain) IBOutlet UIButton *btnRefuser;

@end
