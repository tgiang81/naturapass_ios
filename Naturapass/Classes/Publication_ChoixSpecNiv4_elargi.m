//
//  Publication_ChoixSpecNiv4_elargi.m
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "Publication_ChoixSpecNiv4_elargi.h"
#import "Publication_ChoixSpecNiv5.h"
#import "SubNavigationPRE_Search.h"
#import "CellKind23.h"
#import "AnimalsEntity.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Publication_ChoixSpecNiv4_elargi () <UITextFieldDelegate>
{
    NSMutableArray * arrData;
    
    UITextField *myTextField;
    
    __weak IBOutlet UILabel *lbWarning;
    
}

@property (nonatomic, strong) CellKind23 *prototypeCell;

@end

@implementation Publication_ChoixSpecNiv4_elargi

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addSubNav:@"SubNavigationPRE_Search"];
    
    SubNavigationPRE_Search *okSubView = (SubNavigationPRE_Search*)self.subview;
    
    UIButton *btn1 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG];
    
    btn1.backgroundColor = UIColorFromRGB(MUR_BACK);
    
    UITextField *btn2 = (UITextField*)[self.subview viewWithTag:START_SUB_NAV_TAG+1];
    btn2.delegate = self;
    myTextField = btn2;
    btn2.autocorrectionType = UITextAutocorrectionTypeNo;
    
    UIButton *btn3 = (UIButton*)[okSubView viewWithTag:START_SUB_NAV_TAG+2];
    [btn3 addTarget:self action:@selector(fnSearch) forControlEvents:UIControlEventTouchUpInside];
    arrData = [NSMutableArray new];
    
    lbWarning.text = @"";
    [lbWarning setNeedsDisplay];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onNext:(id)sender {
    Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
}

- (CellKind23 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:identifierSection1];
        
    }
    
    return _prototypeCell;
    
}

- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellKind23 *cell=(CellKind23 *) cellTmp;
    [self setDataForGroupCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

-(void) setDataForGroupCell:(CellKind23*) cell withIndex:(NSIndexPath *)indexPath
{
    AnimalsEntity *dic = arrData[indexPath.row];
    
    //FONT
    cell.label1.text = dic.name;
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(15)];
    
    [cell.rightIcon setImage: [UIImage imageNamed:@"favo_arrow_right" ]];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    [cell layoutIfNeeded];
}
#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind23 *cell = (CellKind23 *)[tableView dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    float height;
    
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    height = size.height+1;
    
    return height;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AnimalsEntity *dic = arrData[indexPath.row];
    
    //specific
    Publication_ChoixSpecNiv5 *viewController1 = [[Publication_ChoixSpecNiv5 alloc] initWithNibName:@"Publication_ChoixSpecNiv5" bundle:nil];
    viewController1.iSpecific = 1;
    viewController1.animalNameSpecific =    dic.name;
    viewController1.id_animal = [dic.myid stringValue];
    viewController1.myName = [NSString stringWithFormat:@"%@/%@",self.myName,dic.name];
    
    //he has specific card ID
    //specific only 1
    
    if ([[PublicationOBJ sharedInstance] getCacheSpecific_Cards].count > 0) {
        viewController1.myCard = [[PublicationOBJ sharedInstance] getCacheSpecific_Cards][0];
    }
    
    viewController1.myDic = self.myDic;
    [self pushVC:viewController1 animate:YES expectTarget:ISMUR iAmParent:NO];
    
}

#pragma mark - textfield delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    //Validate next step
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self fnSearch];
    return YES;
}
-(void)fnSearch
{
    if ([myTextField.text isEqualToString:@""]){
        
        //        arrFilter = [arrData copy];
    }
    else
    {
        //        NSArray *ab= [AnimalsEntity MR_findAll];
        NSString *strSearch = myTextField.text;
        NSString *code = [strSearch substringFromIndex: [strSearch length] - 1];
        
        if (strSearch.length > 4 && [code isEqualToString:@"s"]) {
            
            strSearch = [strSearch substringToIndex:[strSearch length] - 1];
        }
        NSArray *arrTmp = [AnimalsEntity MR_findAllWithPredicate:[NSPredicate predicateWithFormat:@"name contains[cd] %@",strSearch]];
        
        if (arrTmp.count <= 0) {
            lbWarning.text = str(strIl_ny_a_pas_danimaux_dans_notre_base);
        }else{
            lbWarning.text = strEMPTY;
        }
        [lbWarning setNeedsDisplay];
        
        [arrData removeAllObjects];
        [arrData addObjectsFromArray:arrTmp];
        [self.tableControl reloadData];
    }
    
    [myTextField resignFirstResponder];
}
@end
