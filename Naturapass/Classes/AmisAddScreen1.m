//
//  AmisAddScreen1.m
//  Naturapass
//
//  Created by Giang on 8/26/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "AmisAddScreen1.h"
#import "AddressAmisCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

#import "AKTagsInputView.h"
#import "MDCheckBox.h"
#import "AmisAddVC.h"
static NSString *identifierSection1 = @"MyTableViewCell1";

@interface AmisAddScreen1 ()
{
    BOOL allowSearch;
    
    __weak IBOutlet UIButton *btnShowAddr;
    __weak IBOutlet UIButton *btnAjouter;
    __weak IBOutlet MDCheckBox *btnSelectAll;
    __weak IBOutlet UILabel *lblListName;
    //
    __weak IBOutlet UILabel *lblTitleScreen;
    __weak IBOutlet UILabel *lblDescriptionScreen;
    __weak IBOutlet UILabel *lblSelectToutLeMon;




}

@property (strong, nonatomic) IBOutlet UISearchBar *barSearch;

@property (strong, nonatomic) IBOutlet UIView *viewContent;

@property (strong, nonatomic) IBOutlet UIView *tokenInputView;
@property (strong, nonatomic) NSMutableArray *contactsCurrent;
@property (strong, nonatomic) NSMutableArray *contacts;
@property (strong, nonatomic) NSMutableArray *filteredContacts;
@property (strong, nonatomic) NSMutableArray *selectedContacts;
@property (nonatomic, assign) BOOL shouldBeginEditing;
@property (nonatomic, assign) BOOL saveStatusSelectAll;
@property (nonatomic, assign) BOOL saveStatusAjouter;
@property (nonatomic, strong) NSString *saveStatusPeoples;
@end


@implementation AmisAddScreen1

- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitleScreen.text = str(strINVITEZVOSAMIS);
    lblSelectToutLeMon.text = str(strSelectionnezToutLeMonde);
    lblDescriptionScreen.text = str(strDescriptionAmisAddScreen);
    [btnAjouter setTitle:str(strValider_votre_liste) forState:UIControlStateNormal];
    // Do any additional setup after loading the view from its nib.
    self.contacts = [NSMutableArray new];
    self.contactsCurrent = [NSMutableArray new];
    self.filteredContacts = [NSMutableArray new];
    //[_tagsInputView.selectedTags componentsJoinedByString:@", "]
    
//    AKTagsInputView * mtokenInputView = [[AKTagsInputView alloc] initWithFrame:self.tokenInputView.frame];
//    
//    mtokenInputView.backgroundColor = [UIColor grayColor];
//    
//    mtokenInputView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    mtokenInputView.lookupTags = nil;//@[@""];
//    mtokenInputView.selectedTags = [NSMutableArray arrayWithArray:@[@"some"]];
//    mtokenInputView.enableTagsLookup = NO;
//    
//    [self.viewContent addSubview: mtokenInputView ];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"AddressAmisCell" bundle:nil] forCellReuseIdentifier:identifierSection1];
    
    allowSearch = NO;
    
    [self.contacts removeAllObjects];
    [self.contactsCurrent removeAllObjects];
    [self.filteredContacts removeAllObjects];

    self.selectedContacts = [NSMutableArray arrayWithCapacity:self.contacts.count];
    
    APAddressBook *addressBook = [[APAddressBook alloc] init];
    addressBook.fieldsMask = APContactFieldFirstName | APContactFieldLastName | APContactFieldEmails | APContactFieldPhoto |APContactFieldThumbnail;
    addressBook.sortDescriptors = @[
                                    [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES],
                                    [NSSortDescriptor sortDescriptorWithKey:@"lastName" ascending:YES]];
    
    [addressBook loadContacts:^(NSArray *contacts, NSError *error)
     {
         if (!error) {
             for (APContact *contact in contacts)
             {
                 NSArray *emails = contact.emails;
                 for (NSString *anEmail in emails) {
                     
                     if (![self.contacts containsObject:anEmail])
                     {
                         NSString *firstName =contact.firstName?contact.firstName:@"";
                         NSString *lastName =contact.lastName?contact.lastName:@"";
                         NSString *Email=anEmail?anEmail:@"";
                         [self.contacts addObject: @{@"name": [NSString stringWithFormat:@"%@ %@",firstName , lastName ],
                                                     @"email": Email,
                                                     @"desc":[[NSString stringWithFormat:@"%@ %@ %@",contact.firstName , contact.lastName, Email ] uppercaseString],
                                                     @"check":@"0",
                                                     @"image":   contact.apphoto!= nil ? contact.apphoto : [UIImage imageNamed:@"img_cam" ]
                                                     }];

                     }
                 }
             }
             self.contactsCurrent = [NSMutableArray arrayWithArray:[self.contacts copy]];
             [self fnShowAddressList:nil];
         }
     }];
    [btnSelectAll setTypeCheckBox:UI_CARTE];
    btnSelectAll.userInteractionEnabled =NO;
    
    [btnAjouter setBackgroundImage:[UIImage imageNamed:@"ajouter-suivant-backgroundbutton-color"] forState:UIControlStateSelected];
    [btnAjouter setBackgroundImage: [UIImage imageNamed:@"ajouter-suivant-backgroundbutton-Gray"] forState:UIControlStateNormal];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)selectTickAction:(id)sender
{
    NSInteger index =[sender tag] -100;
    UIButton *btn= (UIButton*)sender;
    btn.selected =!btn.selected;
    NSMutableArray *arrData =[NSMutableArray new];
    if (self.shouldBeginEditing) {
        arrData = [NSMutableArray arrayWithArray:self.filteredContacts];
    }
    else
    {
        arrData = [NSMutableArray arrayWithArray:self.contactsCurrent];

        
    }
    
    if (arrData.count>=index) {
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:[arrData objectAtIndex:index]];
        //update
        [dic removeObjectForKey:@"check"];
        if (btn.selected) {
            [dic setValue:@"1" forKey:@"check"];
            
        }
        else
        {
            [dic setValue:@"0" forKey:@"check"];
            
        }
        [arrData replaceObjectAtIndex:index withObject:dic];
        if (self.shouldBeginEditing) {
            [self.filteredContacts removeAllObjects];
            self.filteredContacts= [NSMutableArray arrayWithArray:arrData];
            for (int i =0; i<self.contactsCurrent.count; i++) {
                NSString *strValue=self.contactsCurrent[i][@"email"];
                if ([dic[@"email"] isEqualToString:strValue]) {
                    [self.contactsCurrent replaceObjectAtIndex:i withObject:dic];
                    break;
                }
            }
        }
        else
        {
           [self.contactsCurrent removeAllObjects];
            self.contactsCurrent= [NSMutableArray arrayWithArray:arrData];

            
        }
        [self.tableControl reloadData];
    }
//    if (!self.shouldBeginEditing) {
        [self checkStatus:arrData];
//    }
    
}
-(void)checkStatus:(NSMutableArray*)arrData
{
    //mld
    arrData = [self.contactsCurrent copy];

    int countCheck =0;
    NSString *strMail;
    for (int i =0; i<[arrData count]; i++) {
        NSDictionary *dic =[arrData objectAtIndex:i];
        //update
        if ([dic[@"check"] boolValue]) {
            countCheck++;
            if (strMail==nil) {
                strMail =dic[@"email"];
            }
        }
    }
    if (countCheck<[arrData count]) {
        btnSelectAll.selected =NO;
    }
    else
    {
        btnSelectAll.selected =YES;
    }
    
    if (countCheck > 1) {
        lblListName.text = [NSString stringWithFormat:@"%@...+ %d %@  ",strMail,countCheck-1,str(strOther_peoples)];
        btnAjouter.selected=YES;
    }else if(countCheck == 1)
    {
        lblListName.text = [NSString stringWithFormat:@"%@",strMail];
        btnAjouter.selected=YES;
        
    }
    else
    {
        lblListName.text=@"";
        btnAjouter.selected=NO;
        
    }
    if (self.barSearch.text.length>0 &&[self validateEmailWithString:self.barSearch.text]) {
        btnAjouter.selected =YES;
    }
}

-(void) validateEnableNext
{

}


#pragma mark - TABLEVIEW
//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.shouldBeginEditing) {
        return self.filteredContacts.count;
    }
    else
    {
        return self.contactsCurrent.count;

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AddressAmisCell *cell = nil;
    
    cell = (AddressAmisCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    NSDictionary *dic =[NSDictionary new];
    if (self.shouldBeginEditing) {
        dic = self.filteredContacts[indexPath.row];
    }
    else
    {
        dic = self.contactsCurrent[indexPath.row];
        
    }
    
    cell.imageIcon.image = dic[@"image"];
    [cell.imageIcon.layer setMasksToBounds:YES];
    cell.imageIcon.layer.cornerRadius= 30;
    
    cell.label1.text =dic[@"name"];
    cell.label2.text =dic[@"email"];

    cell.label1.numberOfLines=1;
    cell.btnCheck.tag=indexPath.row+100;
    
    [cell.viewControl setSelected:[dic[@"check"] boolValue]];
    [cell.btnCheck addTarget:self action:@selector(selectTickAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.viewControl setTypeCheckBox:UI_CARTE];
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell layoutIfNeeded];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void) searchText
{

    
    NSString *strsearchValues=self.barSearch.text;
    strsearchValues= [strsearchValues stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    self.barSearch.text=strsearchValues;
    
    if ([strsearchValues isEqualToString:@""]){
        
        self.filteredContacts = [NSMutableArray arrayWithArray:[self.contactsCurrent copy]];
    } else {        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.desc contains[cd] %@", [strsearchValues uppercaseString]];
        self.filteredContacts = [NSMutableArray arrayWithArray:[self.contactsCurrent filteredArrayUsingPredicate:predicate]];
    }
    [self.tableControl reloadData];

    //filter list
    [self checkStatus:self.filteredContacts];
    
}

#pragma mark - SEARCH BAR
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    if (self.shouldBeginEditing) {
        [theSearchBar setShowsCancelButton:YES animated:YES];
    }
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self searchText];

}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.text=@"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    //take all list
    self.shouldBeginEditing = NO;
    self.constraint_control_height.constant=32;
    self.vHeader.hidden=NO;
    [self checkStatus:self.contactsCurrent];
    [self.tableControl reloadData];
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar {
    [theSearchBar resignFirstResponder];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{

    self.shouldBeginEditing = YES;
    [self.filteredContacts removeAllObjects];
    self.constraint_control_height.constant=0;
    self.vHeader.hidden=YES;

    [self checkStatus:self.filteredContacts];
    [self.tableControl reloadData];
    return YES;
}
#pragma mark - button click

-(IBAction)fnShowAddressList:(id)sender
{
    btnSelectAll.userInteractionEnabled=YES;
    btnAjouter.hidden=NO;
    //Address
    [self searchText];
}

-(IBAction)selectAll:(id)sender
{
    UIButton *btn =(UIButton*)sender;
    btn.selected =!btn.selected;
    NSMutableArray *arrData =[NSMutableArray new];
        arrData = [NSMutableArray arrayWithArray:self.contactsCurrent];

    for (int i =0; i<[arrData count]; i++) {
        NSMutableDictionary *dic =[NSMutableDictionary dictionaryWithDictionary:[arrData objectAtIndex:i]];
        //update
        [dic removeObjectForKey:@"check"];
        if (btn.selected) {
            [dic setValue:@"1" forKey:@"check"];

        }
        else
        {
            [dic setValue:@"0" forKey:@"check"];

        }
        
        [arrData replaceObjectAtIndex:i withObject:dic];

    }

        [self.contactsCurrent removeAllObjects];
        self.contactsCurrent= [NSMutableArray arrayWithArray:arrData];

    [self.tableControl reloadData];
    
    [self checkStatus:self.contactsCurrent];
}
-(IBAction)fnAjouteAction:(id)sender
{
    if (!btnAjouter.selected) {
        return;
    }

    NSMutableArray *arrData =[NSMutableArray new];
    arrData = [NSMutableArray arrayWithArray:self.contactsCurrent];
    NSMutableArray *listContacts =[NSMutableArray new];
    
    if (self.barSearch.text.length>0 &&[self validateEmailWithString:self.barSearch.text]) {
      NSDictionary *dicSearch =  @{
          @"name": @"",
          @"email": self.barSearch.text,
          @"desc":@"",
          @"check":@"1",
          @"image":[UIImage imageNamed:@"img_cam" ]};
        [listContacts addObject:dicSearch];
    }
    for (NSDictionary *dic in arrData) {
        if ([dic[@"check"] boolValue]) {
            [listContacts addObject:dic];
        }
    }
    AmisAddVC *viewController1 = [[AmisAddVC alloc] initWithNibName:@"AmisAddVC" bundle:nil];
    viewController1.filteredContacts =listContacts;
    [self pushVC:viewController1 animate:TRUE];
}
- (BOOL)validateEmailWithString:(NSString*)emailText
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailText];
}
@end
