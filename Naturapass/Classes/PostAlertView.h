//
//  PublicAlertView.h
//  Naturapass
//
//  Created by Manh on 6/4/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//
typedef void (^PostAlertViewCallback)(NSInteger index);
@interface PostAlertView : UIViewController
{
    IBOutlet UIView *subAlertView;
}
@property (nonatomic,copy) PostAlertViewCallback callback;
-(void)doBlock:(PostAlertViewCallback ) cb;
@end
