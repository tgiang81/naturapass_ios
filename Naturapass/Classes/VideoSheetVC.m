//
//  VideoSheetVC.m
//  Naturapass
//
//  Created by Giang on 7/31/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "VideoSheetVC.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <ImageIO/ImageIO.h>
#import "Define.h"
#import "AppDelegate.h"

@interface VideoSheetVC ()  <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic,strong) IBOutlet UIButton *btnPhoto;
@property (nonatomic,strong) IBOutlet UIButton *btnLibrary;
@property (nonatomic,strong) IBOutlet UIButton *btnClose;
@property (nonatomic,strong) IBOutlet UILabel *lbTitle;


@end

@implementation VideoSheetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_btnPhoto   setTitle:str(strFilmer_une_video) forState:UIControlStateNormal];
    [_btnLibrary   setTitle:str(strChoisir_dans_la_bibliotheque) forState:UIControlStateNormal];
    [_btnClose  setTitle:str(strAAnnuler) forState:UIControlStateNormal];
    _lbTitle.text = str(strPOSTVIDEO);
    
    // Do any additional setup after loading the view from its nib.
    [self fnSettingCell:self.expectTarget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTakeVideo:(id)sender {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *altView = [[UIAlertView alloc]initWithTitle:str(strTitle_app) message:str(strSorryNoCamera) delegate:nil cancelButtonTitle:str(strOK) otherButtonTitles:nil, nil];
        [altView show];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)onTakeLibrary:(id)sender {
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum])
    {
        UIImagePickerController * picker    = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (NSDictionary *) gpsDictionaryForLocation:(CLLocation *)location
{
    CLLocationDegrees exifLatitude  = location.coordinate.latitude;
    CLLocationDegrees exifLongitude = location.coordinate.longitude;
    
    NSString * latRef;
    NSString * longRef;
    if (exifLatitude < 0.0) {
        exifLatitude = exifLatitude * -1.0f;
        latRef = @"S";
    } else {
        latRef = @"N";
    }
    
    if (exifLongitude < 0.0) {
        exifLongitude = exifLongitude * -1.0f;
        longRef = @"W";
    } else {
        longRef = @"E";
    }
    
    NSMutableDictionary *locDict = [[NSMutableDictionary alloc] init];
    
    [locDict setObject:location.timestamp forKey:(NSString*)kCGImagePropertyGPSTimeStamp];
    [locDict setObject:latRef forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLatitude] forKey:(NSString *)kCGImagePropertyGPSLatitude];
    [locDict setObject:longRef forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
    [locDict setObject:[NSNumber numberWithFloat:exifLongitude] forKey:(NSString *)kCGImagePropertyGPSLongitude];
    [locDict setObject:[NSNumber numberWithFloat:location.horizontalAccuracy] forKey:(NSString*)kCGImagePropertyGPSDOP];
    [locDict setObject:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
    
    return locDict ;
    
}


- (void) saveImage:(UIImage *)imageToSave withInfo:(NSDictionary *)info
{
    // Get the assets library
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Get the image metadata (EXIF & TIFF)
    NSMutableDictionary * imageMetadata = [[info objectForKey:UIImagePickerControllerMediaMetadata] mutableCopy];
    
    ASLog(@"image metadata: %@", imageMetadata);
    
    // add GPS data
    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    CLLocation * loc = app.locationManager.location;
    
    //[[CLLocation alloc] initWithLatitude:[[self deviceLatitude] doubleValue] longitude:[[self devicelongitude] doubleValue]]; // need a location here
    if ( loc ) {
        [imageMetadata setObject:[self gpsDictionaryForLocation:loc] forKey:(NSString*)kCGImagePropertyGPSDictionary];
    }
    
    ALAssetsLibraryWriteImageCompletionBlock imageWriteCompletionBlock =
    ^(NSURL *newURL, NSError *error) {
        if (error) {
            NSLog( @"Error writing image with metadata to Photo Library: %@", error );
        } else {
            NSLog( @"Wrote image %@ with metadata %@ to Photo Library",newURL,imageMetadata);
        }
    };
    
    // Save the new image to the Camera Roll
    [library writeImageToSavedPhotosAlbum:[imageToSave CGImage]
                                 metadata:imageMetadata
                          completionBlock:imageWriteCompletionBlock];
}

#pragma mark  -  PICKER DELEGATE

// FIXME: Rewrite me!

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    if (kUTTypeMovie)
    {
//         NSURL *videoRecordurl = info[UIImagePickerControllerMediaURL];
//         ALAssetsLibrary *assetVideo = [[ALAssetsLibrary alloc]init];
        
             NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];

             if (CFStringCompare ((__bridge CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
                 NSURL *videoUrl=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
                 NSString *moviePath = [videoUrl path];
                 
                 if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum (moviePath)) {
                     
                     self.myCallback(@{@"videourl":moviePath});

                 }
             }

        [self onCancel:nil];

//             [assetVideo writeVideoAtPathToSavedPhotosAlbum:videoRecordurl completionBlock:^(NSURL *assetURL, NSError *error){
//         }];
    }
    
    
    [self onCancel:nil];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
    [picker dismissViewControllerAnimated: YES completion: NULL];

    [self onCancel:nil];
    [self onCancel:nil];

}

- (IBAction)onCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - set them
#pragma mark - set them
-(void)fnSettingCell:(ISSCREEN)type
{
    switch (type) {
        case ISMUR:
        {
            [self fnSetColor: UIColorFromRGB(MUR_TINY_BAR_COLOR)];
            
        }
            break;
        case ISGROUP:
        {
            [self fnSetColor: UIColorFromRGB(GROUP_TINY_BAR_COLOR)];
            
        }
            break;
        case ISLOUNGE:
        {
            [self fnSetColor: UIColorFromRGB(CHASSES_TINY_BAR_COLOR)];
        }
            break;
            
        default:
            break;
    }
}

-(void)fnSetColor:(UIColor*)color
{
    [_btnPhoto setBackgroundColor:color];
    [_btnLibrary setBackgroundColor:color];
    [_btnClose setBackgroundColor:color];
}
@end
