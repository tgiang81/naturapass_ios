//
//  ChassesInvitationAttend.m
//  Naturapass
//
//  Created by Giang on 10/7/15.
//  Copyright © 2015 Appsolute. All rights reserved.
//

#import "ChassesInvitationAttend.h"

#import "CellKind16.h"
#import "CommonHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ChassesParticipeVC.h"
#import "GroupEnterMurVC.h"
#import "MapLocationVC.h"

static NSString *GroupesCellID =@"GroupesViewCellID";


@interface ChassesInvitationAttend ()
{
    NSMutableArray  *groupsActionArray;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UILabel *lblDescription1;

}
@property (nonatomic, strong) CellKind16 *prototypeCell;


@end

@implementation ChassesInvitationAttend

- (CellKind16 *)prototypeCell
{
    if (!_prototypeCell)
    {
        _prototypeCell = [self.tableControl dequeueReusableCellWithIdentifier:GroupesCellID];
        
    }
    
    return _prototypeCell;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    lblTitle.text = str(strListeDeVosInvitations);
    lblDescription1.text = str(strVousNavezNiDemande);
    [self initRefreshControl];
    [self initialization];
    
        NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],@"CHASSE_INVITATION_SAVE")   ];
        NSArray *arrTmp = [NSArray arrayWithContentsOfFile:strPath];
        
        if (arrTmp.count > 0) {
            [groupsActionArray  addObjectsFromArray:arrTmp];
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            if (groupsActionArray.count>0) {
                self.lbInvitation.hidden=YES;
                self.lbInvitation.text = @"";
                [self.lbInvitation setNeedsDisplay];
            }
            else
            {
                self.lbInvitation.hidden=NO;
                self.lbInvitation.text = str(strVous_navez_ni_demande_lacces);
                [self.lbInvitation setNeedsDisplay];

            }
            [self.tableControl reloadData];
        });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startRefreshControl];
    });
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}


-(void)initialization
{
    // init tableviewcell
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind16" bundle:nil] forCellReuseIdentifier:GroupesCellID];
    self.tableControl.estimatedRowHeight = 100;
    //init array
    groupsActionArray=[[NSMutableArray alloc]init];
    
}
#pragma mark - WebServiceAPI

- (void)insertRowAtTop {
    //request first page
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        
        return;
    }


        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj fnGET_HUNT_PENDING_INVITATION:@{@"limit":groups_limit,
                                                    @"offset":@"0"}];
        
        
        __weak ChassesInvitationAttend *weakSelf = self;
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            dispatch_async( dispatch_get_main_queue(), ^{
                [self stopRefreshControl];
                [COMMON removeProgressLoading];
                
                if (!response) {
                    return ;
                }
                
                if([response isKindOfClass: [NSArray class] ]) {
                    return ;
                }
                
                NSArray*arrGroups = response[@"lounges"];
                if ((int)arrGroups.count >= 0) {
                    
                    [groupsActionArray removeAllObjects];
                    [groupsActionArray addObjectsFromArray:response[@"lounges"] ];
                    
                    NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],@"CHASSE_INVITATION_SAVE")   ];
                    [groupsActionArray writeToFile:strPath atomically:YES];
                    if (groupsActionArray.count>0) {
                        self.lbInvitation.hidden=YES;
                        self.lbInvitation.text = @"";
                        [self.lbInvitation setNeedsDisplay];
                    }
                    else
                    {
                        self.lbInvitation.hidden=NO;
                        self.lbInvitation.text = str(strNoChantierInvitation);
                        [self.lbInvitation setNeedsDisplay];
                        
                    }
                    
                    [weakSelf.tableControl reloadData];
                }

            });
            
        };
}

- (void)insertRowAtBottom {
    if ( ![COMMON isReachable] ) {
        [self stopRefreshControl];
        
        return;
    }
    [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:YES];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    [serviceObj fnGET_HUNT_PENDING_INVITATION:@{@"limit":groups_limit,
                                                 @"offset":[NSString stringWithFormat:@"%ld",(long)groupsActionArray.count]}];
    
    __weak ChassesInvitationAttend *weakSelf = self;
    
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [[UIApplication sharedApplication]setNetworkActivityIndicatorVisible:NO];
        [self stopRefreshControl];
        if (!response) {
            return ;
        }
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        NSArray*arrGroups = response[@"lounges"];
        
        if (arrGroups.count > 0) {
            [groupsActionArray addObjectsFromArray:response[@"lounges"] ];
            
            NSString *strPath = [FileHelper pathForApplicationDataFile:concatstring([COMMON getUserId],@"CHASSE_INVITATION_SAVE")   ];
            [groupsActionArray writeToFile:strPath atomically:YES];
            if (groupsActionArray.count>0) {
                self.lbInvitation.hidden=YES;
            }
            else
            {
                self.lbInvitation.hidden=NO;
                
            }
            [weakSelf.tableControl reloadData];
        }
        
    };
}

#pragma mark - TableView datasource & delegate
- (void)configureCell:(UITableViewCell *)cellTmp forRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellKind16 *cell=(CellKind16 *) cellTmp;
    [self setDataForGroupCell:cell withIndex:(NSIndexPath *)indexPath ];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
}

-(void) setDataForGroupCell:(CellKind16*) cell withIndex:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [groupsActionArray objectAtIndex:indexPath.row];
    /*
     acess = 0: current user is invited
     1: request to access and pending to be validate by admin
     2: normal user
     3: admin
     */
    
    NSString *strName = [dic[@"name"] emo_emojiString];

    [cell.nameHunt setText: strName];
    
    //    NSString* imgUrl=[NSString stringWithFormat:@"%@%@",IMAGE_ROOT_API,dic[@"photo"]];
    [cell.imgProfile sd_setImageWithURL:  [NSURL URLWithString:dic[@"photo"]]];
    
    
    if ([dic[@"access"] intValue] == 0) {
        cell.accessType.text = str(strAccessPrivate);
    } else if ([dic[@"access"] intValue] == 1) {
        cell.accessType.text = str(strAccessSemiPrivate);
    } else if ([dic[@"access"] intValue] == 2) {
        cell.accessType.text = str(strAccessPublic);
    }
    NSString *strDescription = [dic[@"description"] emo_emojiString];

    cell.contentHunt.text = strDescription;
    
    [cell.btnRefuse addTarget:self action:@selector(cancelJoin:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnRefuse.tag = 100+ indexPath.row;
    cell.btnValid.tag = 1000+ indexPath.row;
    
    [cell.btnLocation addTarget:self action:@selector(locationButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnLocation.tag = indexPath.row + 200;

    if ( [dic[@"connected"][@"access"] intValue] == 0) {
        //not yet join -> can accept/cancel
        [cell.btnValid addTarget:self action:@selector(acceptJoin:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnValid.hidden = NO;
        cell.btnRefuse.hidden = NO;
        
    }else if ( [dic[@"connected"][@"access"] intValue] == 1)
    {
        //i request join -> only can cancel
        cell.btnValid.hidden = YES;
        cell.btnRefuse.hidden = NO;
    }
    
    cell.startDate.text =  [self convertDate: dic[@"meetingDate"]];
    cell.endDate.text = [self convertDate: dic[@"endDate"]];
    
    //ADDRESS
    NSString*strLocation;
    if(dic[@"meetingAddress"][@"address"]!=nil){
        strLocation=[NSString stringWithFormat:@"%@",dic[@"meetingAddress"][@"address"]];
        [cell.location setText:strLocation];
    }
    else {
        [cell.location setText: [ NSString stringWithFormat:@"Lat. %.5f Lng. %.5f" , [dic[@"meetingAddress"][@"latitude"] floatValue],
                               [dic[@"meetingAddress"][@"longitude"] floatValue]
                               ] ];
    }
    
    cell.btnValid.backgroundColor =UIColorFromRGB(CHASSES_MAIN_BAR_COLOR);
    [cell layoutIfNeeded];
}

-(void) fnDoRefreshData
{
    [self insertRowAtTop];
}

-(void) acceptJoin:(UIButton*)btn
{
    NSDictionary *dic = [groupsActionArray objectAtIndex:btn.tag - 1000];

    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    
    NSString *UserId = [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
    NSString *strLoungeID = dic[@"id"];
    [serviceObj putJoinWithKind:MYCHASSE mykind_id:strLoungeID strUserID:UserId ];

    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:response];

        [self fnGetAllNotifications:4 creen:ISLOUNGE];
        
        [COMMON removeProgressLoading];
        
        if (!response) {
            return ;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        if ([response[@"success"] intValue] == 1) {
            
            
            //go in? participant...
            ChassesParticipeVC  *viewController1=[[ChassesParticipeVC alloc]initWithNibName:@"ChassesParticipeVC" bundle:nil];
            viewController1.dicChassesItem =dic;
            viewController1.isInvitionAttend =YES;
            [viewController1 doCallback:^(NSString *strID) {
                GroupEnterMurVC *viewController1 = [[GroupEnterMurVC alloc] initWithNibName:@"GroupEnterMurVC" bundle:nil];

                [[GroupEnterOBJ sharedInstance] resetParams];
                [GroupEnterOBJ sharedInstance].dictionaryGroup = dic;
                [self pushVC:viewController1 animate:YES expectTarget:ISLOUNGE];
            }];
            [self pushVC:viewController1 animate:YES];
            
        }
    };
}

-(void) cancelJoin:(UIButton*)btn
{
    //delete {"success":true}
    //https://naturapass.e-conception.fr/api/v1/groups/160/joins/304
    NSDictionary *dic = [groupsActionArray objectAtIndex:btn.tag - 100];
    
    WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
    
    [COMMON addLoading:self];
    [serviceObj deleteLoungeJoinAction:dic[@"id"]];
    serviceObj.onComplete = ^(NSDictionary*response, int errCode){
        [self stopRefreshControl];
        [self fnGetAllNotifications:4 creen:ISLOUNGE];
        [COMMON removeProgressLoading];
        AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [app updateAllowShowAdd:response];

        if (!response) {
            return ;
        }
        
        if([response isKindOfClass: [NSArray class] ]) {
            return ;
        }
        
        if ([response[@"success"] intValue] == 1) {
            [self startRefreshControl];
            
            //go in?
        }
    };
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return groupsActionArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CellKind16 *cell = (CellKind16 *)[tableView dequeueReusableCellWithIdentifier:GroupesCellID forIndexPath:indexPath];
    [self configureCell:cell forRowAtIndexPath:indexPath];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewAutomaticDimension;
    
}
- (IBAction)locationButtonAction:(UIButton*) sender
{
    int index = (int) sender.tag - 200;
    NSDictionary *myDic = groupsActionArray[index];

    id dic = myDic[@"meetingAddress"];
    
    if ([dic isKindOfClass: [NSDictionary class]]) {
        NSDictionary *local = (NSDictionary*)dic;
        
        MapLocationVC *viewController1=[[MapLocationVC alloc]initWithNibName:@"MapLocationVC" bundle:nil];
        viewController1.isSubVC = NO;
        viewController1.isSpecial = YES;
        
        viewController1.simpleDic = @{@"latitude":  local[@"latitude"],
                                      @"longitude":  local[@"longitude"]};
        
        viewController1.needRemoveSubItem = NO;        
        [[GroupEnterOBJ sharedInstance] resetParams];
        [GroupEnterOBJ sharedInstance].dictionaryGroup = myDic;
        
        [self pushVC:viewController1 animate:YES expectTarget: ISLOUNGE];
    }
    
    /*
     meetingAddress =     {
     address = "San Francisco";
     latitude = "37.785834";
     longitude = "-122.406417";
     };
     */
}

@end
