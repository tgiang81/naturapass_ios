//
//  GroupCreate_Step3.h
//  Naturapass
//
//  Created by Giang on 7/29/15.
//  Copyright (c) 2015 Appsolute. All rights reserved.
//

#import "GroupCreateBaseVC.h"

@interface GroupCreate_Step3 : GroupCreateBaseVC
@property(nonatomic,strong) NSString *group_id;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar1;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar2;
@property (nonatomic, strong) IBOutlet UIImageView *imgViewAvatar3;
@property (nonatomic,strong) NSMutableArray *arrSubcriber;
@property (nonatomic,assign) BOOL fromSetting;
@end
