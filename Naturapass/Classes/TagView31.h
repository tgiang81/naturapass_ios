//
//  TagView31.h
//  Naturapass
//
//  Created by Giang on 3/2/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void (^callBackAlert) (NSInteger index, NSDictionary *dic);

@interface TagView31 : UIView
{
    
}
@property (nonatomic,strong) IBOutlet UIView *viewBoder;

-(void) showViewWithData:(NSArray*)arrContent withstrSelected:(NSString*)strSelected withIndex:(NSInteger)index;

@property (nonatomic, copy) callBackAlert CallBack;
-(void)doBlock:(callBackAlert ) cb;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewBorder;

@end
