//
//  Parameter_CommonSTEP1.m
//  Naturapass
//
//  Created by Giang on 3/11/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "Parameter_CommonSTEP1.h"
#import "Parameter_CommonSTEP2.h"
#import "CellKind23.h"
#import "Config.h"
#import "AlertVC.h"

static NSString *identifierSection1 = @"MyTableViewCell1";

@interface Parameter_CommonSTEP1 ()
{
    NSMutableArray * arrData;
    __weak IBOutlet UILabel *label_title;
    
    IBOutlet UIView *vFooter_1;
}

@end

@implementation Parameter_CommonSTEP1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [NSMutableArray new];
    
    [self.tableControl registerNib:[UINib nibWithNibName:@"CellKind23" bundle:nil] forCellReuseIdentifier:identifierSection1];
    self.tableControl.estimatedRowHeight = 80;
    self.tableControl.rowHeight = UITableViewAutomaticDimension;

}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fnGetData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) fnGetData
{
    if ([COMMON isReachable]) {
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [COMMON addLoading:self];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            
            [COMMON removeProgressLoading];
            
            if (!response) {
                return ;
            }
            
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            
            switch (self.myTypeVview) {
                case CITY:
                {
                    if([response[@"cities"] isKindOfClass: [NSArray class]]){
                        [arrData removeAllObjects];
                        [arrData addObjectsFromArray:response[@"cities"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case COUNTRY:
                {
                    if([response[@"countries"] isKindOfClass: [NSArray class]]){
                        [arrData removeAllObjects];
                        [arrData addObjectsFromArray:response[@"countries"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                    
                }
                    break;
                case HUNT_PRACTICED:
                {
                    if([response[@"practiced"] isKindOfClass: [NSArray class]]){
                        [arrData removeAllObjects];
                        [arrData addObjectsFromArray:response[@"practiced"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                case HUNT_LIKED:
                {
                    if([response[@"liked"] isKindOfClass: [NSArray class]]){
                        [arrData removeAllObjects];
                        [arrData addObjectsFromArray:response[@"liked"]];
                        
                        [self.tableControl reloadData];
                        
                    }
                }
                    break;
                    
                default:
                    break;
            }
            
        };
        
        switch (self.myTypeVview) {
            case CITY:
            {
                //set title
                
                NSMutableAttributedString *attString = [NSMutableAttributedString new];
                
                
                NSMutableAttributedString *nName = [[NSMutableAttributedString alloc] initWithString:str(strOU_CHASSEZ_VOUS)];
                
                [nName addAttribute:NSFontAttributeName
                              value:[UIFont boldSystemFontOfSize:15.0f]
                              range:NSMakeRange(0, nName.length)];
                
                
                [attString appendAttributedString:nName];
                
                //title
                NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:str(strPreciser_au_mieux_votre_profil_vous_permettra)];
                
                [title addAttribute:NSFontAttributeName
                              value:[UIFont systemFontOfSize:13.0f]
                              range:NSMakeRange(0, title.length)];
                
                
                [attString appendAttributedString:title];
                
                [label_title setAttributedText:attString];
                
                
                [serviceObj fnGET_HUNT_CITY_LOCATION];
            }
                break;
            case COUNTRY:
            {
                NSMutableAttributedString *attString = [NSMutableAttributedString new];
                
                
                NSMutableAttributedString *nName = [[NSMutableAttributedString alloc] initWithString:str(strDans_quels_pays_chassez)];
                
                [nName addAttribute:NSFontAttributeName
                              value:[UIFont boldSystemFontOfSize:15.0f]
                              range:NSMakeRange(0, nName.length)];
                
                
                [attString appendAttributedString:nName];
                
                [label_title setAttributedText:attString];

                
                
                [serviceObj fnGET_HUNT_COUNTRY_LOCATION];
                
            }
                break;
            case HUNT_PRACTICED:
            {
                NSMutableAttributedString *attString = [NSMutableAttributedString new];
                
                
                NSMutableAttributedString *nName = [[NSMutableAttributedString alloc] initWithString:str(strQuel_type_de_chasse_pratiquez)];
                
                [nName addAttribute:NSFontAttributeName
                              value:[UIFont boldSystemFontOfSize:15.0f]
                              range:NSMakeRange(0, nName.length)];
                
                
                [attString appendAttributedString:nName];
                
                [label_title setAttributedText:attString];
                

                [serviceObj fnGET_HUNT_PRACTICE];
                
            }
                break;
            case HUNT_LIKED:
            {
                NSMutableAttributedString *attString = [NSMutableAttributedString new]; // Quel type de chasse aimeriez-vous pratiquer
                
                
                NSMutableAttributedString *nName = [[NSMutableAttributedString alloc] initWithString:str(strQuel_type_de_chasse_aimeriez)];
                
                [nName addAttribute:NSFontAttributeName
                              value:[UIFont boldSystemFontOfSize:15.0f]
                              range:NSMakeRange(0, nName.length)];
                
                
                [attString appendAttributedString:nName];
                
                [label_title setAttributedText:attString];
                

                [serviceObj fnGET_HUNT_LIKED];
                
            }
                break;
            default:
                break;
        }
        
    }
    
}
/*
 -(void) fnGET_HUNT_PRACTICE;
 - (void) fnADD_HUNT_PRACTICE:(NSString *)huntTypeID;
 - (void) fnDEL_HUNT_PRACTICE:(NSString *)huntTypeID;
 
 
 -(void) fnGET_HUNT_LIKED;
 - (void) fnADD_HUNT_LIKED:(NSString *)huntTypeID;
 - (void) fnDEL_HUNT_LIKED:(NSString *)huntTypeID;
 */


#pragma mark - TABLEVIEW

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (self.myTypeVview == CITY) {
        
        return 58;
    }else{
        return 0;
    }
}

- ( UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (self.myTypeVview == CITY) {
        
        return vFooter_1;
    }else{
        return nil;
    }
    
}


//section Mes...Mes_groupes
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrData.count;
}


//    You should be using a different reuseIdentifier for each of the two sections, since they are fundamentally differently styled cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CellKind23 *cell = nil;
    
    cell = (CellKind23 *)[self.tableControl dequeueReusableCellWithIdentifier:identifierSection1 forIndexPath:indexPath];
    
    NSDictionary *dic = arrData[indexPath.row];
    
    //FONT
    cell.label1.text = dic[@"name"];
    [cell.label1 setTextColor:[UIColor blackColor]];
    [cell.label1 setFont:FONT_HELVETICANEUE_MEDIUM(17)];
    
    [cell.rightIcon setImage: [UIImage imageNamed:@"mur_st_ic_delete" ]];
    [cell.btnDel addTarget:self action:@selector(fnDel:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnDel.tag = indexPath.row;
    
    [cell layoutIfNeeded];
    
    
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

-(void) fnDel:(UIButton*)btn
{
    if ([COMMON isReachable]) {
        switch (self.myTypeVview) {
            case CITY:
            {
                
                
                AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strEMPTY) message:str(strVouloir_supprimer_cette_ville) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
                
                [vc doBlock:^(NSInteger index, NSString *str) {
                    if (index==0) {
                        // NON
                    }
                    else if(index==1)
                    {
                        //OUI
                        NSDictionary*dic = arrData[btn.tag];
                        
                        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                        
                        [COMMON addLoading:self];
                        
                        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                            
                            [COMMON removeProgressLoading];
                            
                            if (!response) {
                                return ;
                            }
                            
                            if([response isKindOfClass: [NSArray class] ]) {
                                return ;
                            }
                            
                            //If success deleted
                            [arrData removeObjectAtIndex:btn.tag];
                            
                            [self.tableControl reloadData];
                            
                        };
                        
                        [serviceObj fnDEL_HUNT_CITY_LOCATION:dic[@"id"]];
                    }
                }];
                [vc showAlert];
                

                
            }
                break;
            case COUNTRY:
            {
                
                AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strEMPTY) message:str(strSupprimer_ce_pays_de_vos_pays_de_chasse) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
                
                [vc doBlock:^(NSInteger index, NSString *str) {
                    if (index==0) {
                        // NON
                    }
                    else if(index==1)
                    {
                        //OUI
                        NSDictionary*dic = arrData[btn.tag];
                        
                        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                        
                        [COMMON addLoading:self];
                        
                        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                            
                            [COMMON removeProgressLoading];
                            
                            if (!response) {
                                return ;
                            }
                            
                            if([response isKindOfClass: [NSArray class] ]) {
                                return ;
                            }
                            
                            //If success deleted
                            [arrData removeObjectAtIndex:btn.tag];
                            
                            [self.tableControl reloadData];
                            
                        };
                        
                        [serviceObj fnDEL_HUNT_COUNTRY_LOCATION:dic[@"id"]];
                        
                    }
                }];
                [vc showAlert];
                

                
                
            }
                break;
            case HUNT_PRACTICED:
            {
                
                
                AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strEMPTY) message:str(strChasse_de_vos_pratiques) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
                
                [vc doBlock:^(NSInteger index, NSString *str) {
                    if (index==0) {
                        // NON
                    }
                    else if(index==1)
                    {
                        //OUI
                        NSDictionary*dic = arrData[btn.tag];
                        
                        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                        
                        [COMMON addLoading:self];
                        
                        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                            
                            [COMMON removeProgressLoading];
                            
                            if (!response) {
                                return ;
                            }
                            
                            if([response isKindOfClass: [NSArray class] ]) {
                                return ;
                            }
                            
                            //If success deleted
                            [arrData removeObjectAtIndex:btn.tag];
                            
                            [self.tableControl reloadData];
                            
                        };
                        
                        [serviceObj fnDEL_HUNT_PRACTICE:dic[@"id"]];
                    }
                }];
                [vc showAlert];
                
            }
                break;
            case HUNT_LIKED:
            {
                
                
                AlertVC *vc = [[AlertVC alloc] initWithTitle:str(strEMPTY) message:str(strVouloir_supprimer_ce_type_de_chasse) cancelButtonTitle:str(strNON) otherButtonTitles:str(strOUI)];
                
                [vc doBlock:^(NSInteger index, NSString *str) {
                    if (index==0) {
                        // NON
                    }
                    else if(index==1)
                    {
                        //OUI
                        NSDictionary*dic = arrData[btn.tag];
                        
                        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
                        
                        [COMMON addLoading:self];
                        
                        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
                            
                            [COMMON removeProgressLoading];
                            
                            if (!response) {
                                return ;
                            }
                            
                            if([response isKindOfClass: [NSArray class] ]) {
                                return ;
                            }
                            
                            //If success deleted
                            [arrData removeObjectAtIndex:btn.tag];
                            
                            [self.tableControl reloadData];
                            
                        };
                        
                        [serviceObj fnDEL_HUNT_LIKED:dic[@"id"]];
                        
                    }
                }];
                [vc showAlert];
                

            }
                break;
            default:
                break;
        }
        
    }
    else
    {
        
    }
    
}

- (IBAction)fnAdd:(id)sender {
    Parameter_CommonSTEP2 *viewController1 = [[Parameter_CommonSTEP2 alloc] initWithNibName:@"Parameter_CommonSTEP2" bundle:nil];
    viewController1.myTypeVview = self.myTypeVview;
    
    [self pushVC:viewController1 animate:YES];
}


@end

