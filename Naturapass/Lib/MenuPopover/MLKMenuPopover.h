//
//  MLKMenuPopover.h
//  MLKMenuPopover
//
//  Created by NagaMalleswar on 20/11/14.
//  Copyright (c) 2014 NagaMalleswar. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^callBackDismiss) (NSInteger);

@class MLKMenuPopover;

@protocol MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex;

@end

@interface MLKMenuPopover : UIView <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,assign) id<MLKMenuPopoverDelegate> menuPopoverDelegate;
@property(nonatomic,retain) NSArray *menuItems;


- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)menuItems color:(UIColor*)color;
- (void)showInView:(UIView *)view;
- (void)dismissMenuPopover;
- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;
-(void)fnColorTheme:(UIColor*)color;
- (void)showInView:(UIView *)view offset:(CGRect)offset;
@property (nonatomic, copy) callBackDismiss CallBackdDismiss;

@end
