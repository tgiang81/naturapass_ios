//
//  MDPopover.m
//  Naturapass
//
//  Created by Manh on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDPopover.h"
#import "PublicationControlCheckboxCell.h"
static NSString *identifierCard = @"CardCheckboxCellID";
static NSString *tableCell = @"PublicationControlCheckboxCell";
#define ZERO                    0.0f
#define ONE                     1.0f
#define ANIMATION_DURATION      0.5f
@implementation MDPopover

- (id)initWithInView:(UIView*)inView
{
    CGRect frame = CGRectZero;

    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self.layer setMasksToBounds:YES];
        self.layer.cornerRadius= 4;
        self.layer.borderWidth =1;
        [inView addSubview:self];
        _inView=inView;
        self.tableControl =[[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self.tableControl registerNib:[UINib nibWithNibName:tableCell bundle:nil] forCellReuseIdentifier:identifierCard];
        self.tableControl.dataSource = self;
        self.tableControl.delegate = self;
        self.tableControl.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.tableControl];
        tapGesture =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                            action:@selector(handleTapGesture:)];

    }
    return self;
}

-(void)fnSetData:(NSArray*)arrContent withTypeControl:(TYPE_CONTROL)typeC
{
    arrData= arrContent;
    typeControl =typeC;
    arrMultiple= [NSMutableArray arrayWithArray:arrData];
    [self.tableControl reloadData];
}
-(void)showPopupPresentPoint:(UIView*)viewPoint withData:(NSArray*)arrContent withTypeControl:(TYPE_CONTROL)typeC
{
    _viewPoint=viewPoint;
    CGPoint targetRelativeOrigin    = [_viewPoint.superview convertPoint:_viewPoint.frame.origin toView:_inView];
    CGRect rect = self.frame;
    rect.origin.y= targetRelativeOrigin.y+_viewPoint.frame.size.height;
    rect.origin.x =targetRelativeOrigin.x;
    rect.size.width= _viewPoint.frame.size.width;
    int count =0;
    if (arrContent.count>4) {
        count= 4;
    }
    else
    {
        count= arrContent.count;
    }
    rect.size.height= count*40;
    self.frame= rect;
    self.tableControl.frame =CGRectMake(0, 0, rect.size.width, rect.size.height);
    self.backgroundColor = [UIColor redColor];
    [_inView addGestureRecognizer:tapGesture];
    
    self.alpha = ZERO;
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.alpha = ONE;
                     }
                     completion:^(BOOL finished) {}];
    //
    [self fnSetData:arrContent withTypeControl:typeC];
}
-(void)hidenPopup
{
    self.alpha = ONE;
    [UIView animateWithDuration:ANIMATION_DURATION
                     animations:^{
                         self.alpha = ZERO;
                     }
                     completion:^(BOOL finished) {}];
    [_inView removeGestureRecognizer:tapGesture];
}
#pragma mark - tap
- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture {
    [self hidenPopup];
}
#pragma mark -  TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrMultiple count];
}

- (CGFloat) tableView: (UITableView *) tableView heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
    return 40;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PublicationControlCheckboxCell *cell = nil;
    
    cell = (PublicationControlCheckboxCell *)[self.tableControl dequeueReusableCellWithIdentifier:identifierCard forIndexPath:indexPath];
    NSDictionary *dic = arrMultiple[indexPath.row];
    cell.lbTittle.text = dic[@"name"];
    [cell layoutIfNeeded];
    cell.backgroundColor=[UIColor whiteColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
@end
