//
//  MDPopover.h
//  Naturapass
//
//  Created by Manh on 2/19/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Define.h"
@interface MDPopover : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrData;
    TYPE_CONTROL typeControl;
    NSMutableArray *arrMultiple;
    UIView *_inView;
    UIView *_viewPoint;
    UITapGestureRecognizer *tapGesture;
}
@property (nonatomic,strong) UITableView *tableControl;
- (id)initWithInView:(UIView*)inView;
-(void)showPopupPresentPoint:(UIView*)viewPoint withData:(NSArray*)arrData withTypeControl:(TYPE_CONTROL)typeC;
-(void)fnSetData:(NSArray*)arrContent withTypeControl:(TYPE_CONTROL)typeC;
-(void)hidenPopup;
@end
