//
//  AugmentedRealityController.m
//  AR Kit
//
//  Modified by Niels W Hansen on 5/25/12.
//  Modified by Ed Rackham (a1phanumeric) 2013
//

#import "AugmentedRealityController.h"
#import "ARCoordinate.h"
#import "ARGeoCoordinate.h"
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import "Define.h"
#import "DatabaseManager.h"
#import "MarkerView.h"
#import "AppDelegate.h"
#define kFilteringFactor 0.05
#define degreesToRadian(x) (M_PI * (x) / 180.0)
#define radianToDegrees(x) ((x) * 180.0/M_PI)
#define M_2PI 2.0 * M_PI
#define BOX_WIDTH 150
#define BOX_HEIGHT 100
#define BOX_GAP 10
#define ADJUST_BY 30
#define DISTANCE_FILTER 2.0
#define HEADING_FILTER 1.0
#define INTERVAL_UPDATE 0.75
#define SCALE_FACTOR 1.0
#define HEADING_NOT_SET -1.0
#define DEGREE_TO_UPDATE 1


@interface AugmentedRealityController (Private)
- (void) updateCenterCoordinate;
- (void) startListening;
//- (void) currentDeviceOrientation;

- (double) findDeltaOfRadianCenter:(double*)centerAzimuth coordinateAzimuth:(double)pointAzimuth betweenNorth:(BOOL*) isBetweenNorth;
- (CGPoint) pointForCoordinate:(ARCoordinate *)coordinate;
- (BOOL) shouldDisplayCoordinate:(ARCoordinate *)coordinate;

@end

@implementation AugmentedRealityController

@synthesize locationManager;
@synthesize accelerometerManager;
@synthesize displayView;
@synthesize cameraView;
@synthesize rootViewController;
@synthesize centerCoordinate;
@synthesize scaleViewsBasedOnDistance;
@synthesize rotateViewsBasedOnPerspective;
@synthesize maximumScaleDistance;
@synthesize minimumScaleFactor;
@synthesize maximumRotationAngle;
@synthesize centerLocation;
@synthesize coordinates;
@synthesize debugMode;
@synthesize captureSession;
@synthesize previewLayer;
@synthesize delegate;
@synthesize markerDelegate;

- (id)initWithViewController:(UIViewController *)vc withDelgate:(id<ARDelegate>) aDelegate markerDel:(id<ARMarkerDelegate>)markDel {
    
    if (!(self = [super init]))
		return nil;
    
    [self setDelegate:aDelegate];
    
    [self setMarkerDelegate: markDel];
    
    _myDIC_MarkerPublication = [NSMutableDictionary new];
    latestHeading   = HEADING_NOT_SET;
    prevHeading     = HEADING_NOT_SET;
    
	[self setRootViewController: vc];
    [self setMaximumScaleDistance: 0.0];
	[self setMinimumScaleFactor: SCALE_FACTOR];
	[self setScaleViewsBasedOnDistance: YES];
    
	[self setRotateViewsBasedOnPerspective: NO];
    
    [self setOnlyShowItemsWithinRadarRange:YES];
    
	[self setMaximumRotationAngle: M_PI / 6.0];
    [self setCoordinates:[NSMutableArray array]];
//    [self currentDeviceOrientation];
	
	 screenRect = [[UIScreen mainScreen] bounds];
    
//    if (cameraOrientation == UIDeviceOrientationLandscapeLeft || cameraOrientation == UIDeviceOrientationLandscapeRight) {
        screenRect.size.width  = [[UIScreen mainScreen] bounds].size.height;
        screenRect.size.height = [[UIScreen mainScreen] bounds].size.width;
//    }
    
	UIView *camView = [[UIView alloc] initWithFrame:screenRect];
    UIView *displayV= [[UIView alloc] initWithFrame:screenRect];
    
    [displayV setAutoresizesSubviews:YES];
    [camView setAutoresizesSubviews:YES];
    
    camView.autoresizingMask    = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    displayV.autoresizingMask   = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
	degreeRange = 15;
    
    
	[vc setView:displayV];
    [[vc view] insertSubview:camView atIndex:0];
    

#if !TARGET_IPHONE_SIMULATOR
    
    AVCaptureSession *avCaptureSession = [[AVCaptureSession alloc] init];
    AVCaptureDevice *videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error = nil;
    
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoCaptureDevice error:&error];
    
    if (videoInput) {
        [avCaptureSession addInput:videoInput];
    }
    else {
        // Handle the failure.
    }
    
    AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:avCaptureSession];

    [[camView layer] setMasksToBounds:NO];

    [newCaptureVideoPreviewLayer setFrame:[camView bounds]];
    
    [newCaptureVideoPreviewLayer.connection setVideoOrientation:AVCaptureVideoOrientationLandscapeRight|AVCaptureVideoOrientationLandscapeLeft];

    
//    if ([newCaptureVideoPreviewLayer.connection isVideoOrientationSupported]) {
//        [newCaptureVideoPreviewLayer.connection setVideoOrientation:cameraOrientation];
//    }
    
    [newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    [[camView layer] insertSublayer:newCaptureVideoPreviewLayer below:[[[camView layer] sublayers] objectAtIndex:0]];
    
    [self setPreviewLayer:newCaptureVideoPreviewLayer];
    
    [avCaptureSession setSessionPreset:AVCaptureSessionPresetHigh];
    [avCaptureSession startRunning];
    
    [self setCaptureSession:avCaptureSession];
#endif

    //mld
//    CLLocation *newCenter = [[CLLocation alloc] initWithLatitude:21.0241884 longitude:105.1826885]; //TODO: We should get the latest heading here.
//	
//	[self setCenterLocation: newCenter];
	
//	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:)
//                                                 name: UIDeviceOrientationDidChangeNotification object:nil];
//    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];	
    
	
	[self startListening];
    [self setCameraView:camView];
    [self setDisplayView:displayV];
    
    
  	return self;
}

- (BOOL)shouldAutorotate{
    return YES;
}

- (void)setShowsRadar:(BOOL)showsRadar{
    _showsRadar = showsRadar;
    [_radarEllipView          removeFromSuperview];
    [_radarView          removeFromSuperview];
    [_radarViewPort      removeFromSuperview];
    [radarNorthLabel    removeFromSuperview];
    
    _radarEllipView       = nil;
    _radarView       = nil;
    _radarViewPort   = nil;
    radarNorthLabel = nil;
    
    if(_showsRadar){
        
        CGRect displayFrame = [[[self rootViewController] view] frame];
        float RADIUS_A = screenRect.size.width;
        float RADIUS_B = 150;
        _radarEllipView       = [[Radar_Ellipse alloc] initWithFrame:CGRectMake(-RADIUS_A/2, screenRect.size.height - RADIUS_B, RADIUS_A*2, RADIUS_B*2)];
        _radarEllipView.RADIUS_A = RADIUS_A;
        _radarEllipView.RADIUS_B = RADIUS_B;
        
        _radarView       = [[Radar alloc] initWithFrame:CGRectMake(20, 40, RADIUS*2, RADIUS*2)];
//        _radarViewPort   = [[RadarViewPortView alloc] initWithFrame:CGRectMake(displayFrame.size.width - RADIUS*2, 2, RADIUS*2, RADIUS*2)];
//        _radarView.center = CGPointMake(CGRectGetWidth(displayFrame)/2, CGRectGetHeight(displayFrame)/2);
//        _radarViewPort.center = CGPointMake(CGRectGetWidth(displayFrame)/2, CGRectGetHeight(displayFrame)/2);
        
        radarNorthLabel = [[UILabel alloc] initWithFrame:CGRectMake(RADIUS-10, 15, 60, 30)];
        radarNorthLabel.backgroundColor = [UIColor clearColor];
        radarNorthLabel.textColor = [UIColor whiteColor];
        radarNorthLabel.font = [UIFont boldSystemFontOfSize:10.0];
        radarNorthLabel.textAlignment = NSTextAlignmentCenter;
//        radarNorthLabel.text = @"N";
        radarNorthLabel.alpha = 0.8;
        
        _radarEllipView.autoresizingMask         = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;

        _radarView.autoresizingMask         = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
//        _radarViewPort.autoresizingMask     = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
//        radarNorthLabel.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin;
        //vong tron radar
        [self.displayView addSubview:_radarEllipView];
        [self.displayView addSubview:_radarView];
        //goc rada
//        [self.displayView addSubview:_radarViewPort];
        //text hien thi huong N
        [self.displayView addSubview:radarNorthLabel];
    }
}

-(void)unloadAV {
    [self stopListening];
    locationManager.delegate = nil;

    [captureSession stopRunning];
    AVCaptureInput* input = [captureSession.inputs objectAtIndex:0];
    [captureSession removeInput:input];
    [[self previewLayer] removeFromSuperlayer];
    [self setCaptureSession:nil];
    [self setPreviewLayer:nil];	
}

- (void)dealloc {
    [self unloadAV];
//	[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
    [UIAccelerometer sharedAccelerometer].delegate = nil;
}

#pragma mark -	
#pragma mark Location Manager methods
- (void)startListening {
	
	// start our heading readings and our accelerometer readings.
	if (![self locationManager]) {
		CLLocationManager *newLocationManager = [[CLLocationManager alloc] init];

        [newLocationManager setHeadingFilter: HEADING_FILTER];
        [newLocationManager setDistanceFilter:DISTANCE_FILTER];
		[newLocationManager setDesiredAccuracy: kCLLocationAccuracyBest];
        if ([newLocationManager respondsToSelector: @selector(requestWhenInUseAuthorization)])
            [newLocationManager requestWhenInUseAuthorization];
		[newLocationManager startUpdatingHeading];
//		[newLocationManager startUpdatingLocation];
        
		[newLocationManager setDelegate: self];
        
        [self setLocationManager: newLocationManager];
	}
			
	if (![self accelerometerManager]) {
		[self setAccelerometerManager: [UIAccelerometer sharedAccelerometer]];
		[[self accelerometerManager] setUpdateInterval: INTERVAL_UPDATE];
		[[self accelerometerManager] setDelegate: self];
	}
	
	if (![self centerCoordinate]) 
		[self setCenterCoordinate:[ARCoordinate coordinateWithRadialDistance:1.0 inclination:0 azimuth:0]];
}

- (void)stopListening {
//    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
   
    if ([self locationManager]) {
       [[self locationManager] setDelegate: nil];
    }
    
    if ([self accelerometerManager]) {
       [[self accelerometerManager] setDelegate: nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {

    latestHeading = degreesToRadian(newHeading.magneticHeading);
    
    //Let's only update the Center Coordinate when we have adjusted by more than X degrees
    if (fabs(latestHeading-prevHeading) >= degreesToRadian(DEGREE_TO_UPDATE) || prevHeading == HEADING_NOT_SET) {
        prevHeading = latestHeading;
        [self updateCenterCoordinate];
        [[self delegate] didUpdateHeading:newHeading];
    }
    
    
    if(_showsRadar){
        int gradToRotate = newHeading.magneticHeading;
        if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeLeft) {
            gradToRotate += 90;
        }
        if([[UIDevice currentDevice] orientation] == UIDeviceOrientationLandscapeRight){
            gradToRotate -= 90;
        }
        if (gradToRotate < 0) {
            gradToRotate = 360 + gradToRotate;
        }
        float value = gradToRotate;
        if (value>360) {
            value= 360-value;
        }
        NSString *str=@"";
        if(value >= 0 && value < 23)
        {
            str = [NSString stringWithFormat:@"%0.f° N",value];
        }
        else if(value >=23 && value < 68)
        {
            str = [NSString stringWithFormat:@"%0.f° NE",value];
        }
        else if(value >=68 && value < 113)
        {
            str = [NSString stringWithFormat:@"%0.f° E",value];
        }
        else if(value >=113 && value < 185)
        {
            str = [NSString stringWithFormat:@"%0.f° SE",value];
        }
        else if(value >=185 && value < 203)
        {
            str = [NSString stringWithFormat:@"%0.f° S",value];
        }
        else if(value >=203 && value < 249)
        {
            str = [NSString stringWithFormat:@"%0.f° SE",value];
        }
        else if(value >=249 && value < 293)
        {
            str = [NSString stringWithFormat:@"%0.f° W",value];
        }
        else if(value >=293 && value < 350)
        {
            str = [NSString stringWithFormat:@"%0.f° NW",value];
        }
        else if(value >=350 && value <= 360)
        {
            str = [NSString stringWithFormat:@"%0.f° N",value];
        }
        radarNorthLabel.text = str;
        _radarView.referenceAngle = gradToRotate;

        gocThamChieu = gradToRotate;
        
        [_radarView setNeedsDisplay];
        _radarEllipView.referenceAngle = gradToRotate;
        [_radarEllipView setNeedsDisplay];

    }
}

- (BOOL)locationManagerShouldDisplayHeadingCalibration:(CLLocationManager *)manager {
	return YES;
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    [self setCenterLocation:newLocation];
//    [[self delegate] didUpdateLocation:newLocation];
//    
//}
-(void)coordinateBoundsWithLocation:(CLLocationCoordinate2D)coordinate withDistance:(CLLocationDistance)distance
{
    MKCoordinateRegion region  =  MKCoordinateRegionMakeWithDistance(coordinate, distance, distance);
    CLLocationDegrees northEastLatitude = coordinate.latitude + region.span.latitudeDelta;
    CLLocationDegrees northEastLongitude = coordinate.longitude + region.span.longitudeDelta;
    
    CLLocationDegrees southWestLatitude = coordinate.latitude - region.span.latitudeDelta;
    CLLocationDegrees southWestLongitude = coordinate.longitude - region.span.longitudeDelta;
    
    northEastFilter = CLLocationCoordinate2DMake(northEastLatitude, northEastLongitude);
    southWestFilter = CLLocationCoordinate2DMake(southWestLatitude, southWestLongitude);
}

- (void)updateCenterCoordinate {
	
	double adjustment = 0;

    switch (cameraOrientation) {
        case UIDeviceOrientationLandscapeLeft:
            adjustment = degreesToRadian(270); 
            break;
        case UIDeviceOrientationLandscapeRight:    
            adjustment = degreesToRadian(90);
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            adjustment = degreesToRadian(180);
            break;
        default:
            adjustment = 0;
            break;
    }
	
	[[self centerCoordinate] setAzimuth: latestHeading - adjustment];
	[self updateLocations];
}

- (void)setCenterLocation:(CLLocation *)newLocation {
	centerLocation = newLocation;
	
	for (ARGeoCoordinate *geoLocation in [self coordinates]) {
		
		if ([geoLocation isKindOfClass:[ARGeoCoordinate class]]) {
			[geoLocation calibrateUsingOrigin:centerLocation];
			
            if(_onlyShowItemsWithinRadarRange){
                if(([geoLocation radialDistance] / 1000) > _radarRange){
                    continue;
                }
            }
            
			if ([geoLocation radialDistance] > [self maximumScaleDistance]) 
				[self setMaximumScaleDistance:[geoLocation radialDistance]];
		}
	}
}

- (void)accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
	
	switch (cameraOrientation) {
		case UIDeviceOrientationLandscapeLeft:
			viewAngle = atan2(acceleration.x, acceleration.z);
			break;
		case UIDeviceOrientationLandscapeRight:
			viewAngle = atan2(-acceleration.x, acceleration.z);
			break;
		case UIDeviceOrientationPortrait:
			viewAngle = atan2(acceleration.y, acceleration.z);
			break;
		case UIDeviceOrientationPortraitUpsideDown:
			viewAngle = atan2(-acceleration.y, acceleration.z);
			break;	
		default:
			break;
	}
}

#pragma mark -	
#pragma mark Coordinate methods

- (void)addCoordinate:(ARGeoCoordinate *)coordinate {
        [coordinate calibrateUsingOrigin:centerLocation];

	[[self coordinates] addObject:coordinate];
    [_myDIC_MarkerPublication setObject:coordinate forKey:coordinate.dicPublication[@"c_id"]];

	if ([coordinate radialDistance] > [self maximumScaleDistance]) 
		[self setMaximumScaleDistance: [coordinate radialDistance]];
}

- (void)removeCoordinate:(ARGeoCoordinate *)coordinate {
	[[self coordinates] removeObject:coordinate];
    [_myDIC_MarkerPublication removeObjectForKey:coordinate.dicPublication[@"c_id"]];

}

- (void)removeCoordinates:(NSArray *)coordinateArray {	
	
	for (ARGeoCoordinate *coordinateToRemove in coordinateArray) {
		NSUInteger indexToRemove = [[self coordinates] indexOfObject:coordinateToRemove];
		
		//TODO: Error checking in here.
		[[self coordinates] removeObjectAtIndex:indexToRemove];
        [_myDIC_MarkerPublication removeObjectForKey:coordinateToRemove.dicPublication[@"c_id"]];

	}
}
-(void)removeAllCoordinate
{
    [[self coordinates] removeAllObjects];
    [_myDIC_MarkerPublication removeAllObjects];

}
#pragma mark -
#pragma mark Location methods

-(double) findDeltaOfRadianCenter:(double*)centerAzimuth coordinateAzimuth:(double)pointAzimuth betweenNorth:(BOOL*) isBetweenNorth {
    
    if (*centerAzimuth < 0.0)
        *centerAzimuth = M_2PI + *centerAzimuth;
    
    if (*centerAzimuth > M_2PI)
        *centerAzimuth = *centerAzimuth - M_2PI;
    
    double deltaAzimuth = ABS(pointAzimuth - *centerAzimuth);
    *isBetweenNorth		= NO;
    
    // If values are on either side of the Azimuth of North we need to adjust it.  Only check the degree range
    if (*centerAzimuth < degreesToRadian(degreeRange) && pointAzimuth > degreesToRadian(360-degreeRange)) {
        deltaAzimuth	= (*centerAzimuth + (M_2PI - pointAzimuth));
        *isBetweenNorth = YES;
    }
    else if (pointAzimuth < degreesToRadian(degreeRange) && *centerAzimuth > degreesToRadian(360-degreeRange)) {
        deltaAzimuth	= (pointAzimuth + (M_2PI - *centerAzimuth));
        *isBetweenNorth = YES;
    }
    
    return deltaAzimuth;
}

- (BOOL)shouldDisplayCoordinate:(ARCoordinate *)coordinate {
    
    double currentAzimuth = [[self centerCoordinate] azimuth];
    double pointAzimuth	  = [coordinate azimuth];
    BOOL isBetweenNorth	  = NO;
    double deltaAzimuth	  = [self findDeltaOfRadianCenter: &currentAzimuth coordinateAzimuth:pointAzimuth betweenNorth:&isBetweenNorth];
    BOOL result			  = NO;
    
    //  NSLog(@"Current %f, Item %f, delta %f, range %f",currentAzimuth,pointAzimuth,deltaAzimith,degreesToRadian([self degreeRange]));
    
    if (deltaAzimuth <= degreesToRadian(degreeRange)){
        result = YES;
    }
    
    // Limit results to only those within radar range (if set)
    if(_onlyShowItemsWithinRadarRange){
        if(([coordinate radialDistance] / 1000) > _radarRange){
            result = NO;
        }
    }
    
    return result;
}

- (CGPoint)pointForCoordinate:(ARCoordinate *)coordinate {
    
    CGPoint point;
    CGRect realityBounds	= [[self displayView] bounds];
    double currentAzimuth	= [[self centerCoordinate] azimuth];
    double pointAzimuth		= [coordinate azimuth];
    BOOL isBetweenNorth		= NO;
    double deltaAzimith		= [self findDeltaOfRadianCenter: &currentAzimuth coordinateAzimuth:pointAzimuth betweenNorth:&isBetweenNorth];
    
    if ((pointAzimuth > currentAzimuth && !isBetweenNorth) ||
        (currentAzimuth > degreesToRadian(360- degreeRange) && pointAzimuth < degreesToRadian(degreeRange))) {
        point.x = (realityBounds.size.width / 2) + ((deltaAzimith / degreesToRadian(1)) * ADJUST_BY);  // Right side of Azimuth
    }
    else
        point.x = (realityBounds.size.width / 2) - ((deltaAzimith / degreesToRadian(1)) * ADJUST_BY);	// Left side of Azimuth
    
    point.y = (realityBounds.size.height / 2) + (radianToDegrees(M_PI_2 + viewAngle)  * 2.0);
    
    return point;
}
-(CGPoint)caculator:(ARCoordinate *)poi withRadius: (float)radius
{
    CGPoint point;
    float _range = radius *1;
    float scale = _range / RADIUS_2;
    
    double dazimuth =0;
    if (radians(gocThamChieu) -poi.azimuth>0) {
        dazimuth= M_PI_2 + ABS(radians(gocThamChieu) -poi.azimuth);
    }
    else
    {
        dazimuth= M_PI_2-ABS(radians(gocThamChieu) -poi.azimuth);
    }
    
    if (dazimuth>2*M_PI) {
        dazimuth = dazimuth - 2*M_PI;
    }
    if (dazimuth<0) {
        dazimuth = 2*M_PI +dazimuth;
    }
    float x, y;
    //case1: azimiut is in the 1 quadrant of the radar
    if (dazimuth >= 0 && dazimuth <= M_PI / 2) {
        x = RADIUS_2 + cosf(dazimuth) * (poi.radialDistance / scale);
        y = RADIUS_2 - sinf(dazimuth) * (poi.radialDistance / scale);
    } else if (dazimuth > M_PI / 2 && dazimuth <= M_PI) {
        //case2: azimiut is in the 2 quadrant of the radar
        x = RADIUS_2 - cosf(M_PI-dazimuth)* (poi.radialDistance / scale);
        y = RADIUS_2 - sinf(M_PI-dazimuth) * (poi.radialDistance / scale);
    } else if (dazimuth > M_PI && dazimuth <= (3 * M_PI / 2)) {
        //case3: azimiut is in the 3 quadrant of the radar
        x = RADIUS_2 - sinf((3 * M_PI / 2) - dazimuth) * (poi.radialDistance / scale);
        y = RADIUS_2 + cosf((3 * M_PI / 2) - dazimuth) * (poi.radialDistance / scale);
    } else if(dazimuth > (3 * M_PI / 2) && dazimuth <= (2 * M_PI)) {
        //case4: azimiut is in the 4 quadrant of the radar
        x = RADIUS_2 + cosf(2*M_PI-dazimuth) * (poi.radialDistance / scale);
        y = RADIUS_2 + sinf(2*M_PI-dazimuth) * (poi.radialDistance / scale);
    }
    
    else {
        x = RADIUS_2;
        y = RADIUS_2;
    }
    point.x = x;
    point.y = x;
    return point;
}
- (void)updateLocations {
    
    NSMutableArray *radarPointValues = [[NSMutableArray alloc] initWithCapacity:[self.coordinates count]];
    
    for (ARGeoCoordinate *item in [self coordinates]) {
        
//        UIView *markerView = [item displayView];
        
//        if ([self shouldDisplayCoordinate:item]) {
        
//            CGPoint loc = [self caculator:item withRadius:_radarRange];
//            [markerView setFrame:CGRectMake(160 -loc.x, 160 - loc.y, 100, 100)];
//            [markerView setNeedsDisplay];

//            CGFloat scaleFactor = SCALE_FACTOR;
//            
//            if ([self scaleViewsBasedOnDistance]) {
//                scaleFactor = scaleFactor - [self minimumScaleFactor]*([item radialDistance] / [self maximumScaleDistance]);
//            }
//            
//            float width	 = [markerView bounds].size.width  * scaleFactor;
//            float height = [markerView bounds].size.height * scaleFactor;
            
//            [markerView setFrame:CGRectMake(loc.x - width / 2.0, loc.y, width, height)];
//            [markerView setNeedsDisplay];
            
//            CATransform3D transform = CATransform3DIdentity;
//            
//            // Set the scale if it needs it. Scale the perspective transform if we have one.
//            if ([self scaleViewsBasedOnDistance])
//                transform = CATransform3DScale(transform, scaleFactor, scaleFactor, scaleFactor);
//            
//            if ([self rotateViewsBasedOnPerspective]) {
//                transform.m34 = 1.0 / 300.0;
                /*
                 double itemAzimuth		= [item azimuth];
                 double centerAzimuth	= [[self centerCoordinate] azimuth];
                 
                 if (itemAzimuth - centerAzimuth > M_PI)
                 centerAzimuth += M_2PI;
                 
                 if (itemAzimuth - centerAzimuth < -M_PI)
                 itemAzimuth  += M_2PI;
                 */
                //		double angleDifference	= itemAzimuth - centerAzimuth;
                //		transform				= CATransform3DRotate(transform, [self maximumRotationAngle] * angleDifference / 0.3696f , 0, 1, 0);
//            }
//            [[markerView layer] setTransform:transform];
            
            //if marker is not already set then insert it
//            if (!([markerView superview])) {
//                [[self displayView] insertSubview:markerView atIndex:1];
//            }
//        }else {
//            if([markerView superview]){
//                [markerView removeFromSuperview];
//            }
//        }
        
        [radarPointValues addObject:item];
        
    }
    
    if(_showsRadar){
        _radarView.pois      = radarPointValues;
        _radarView.radius    = _radarRange;
        [_radarView setNeedsDisplay];
        //
        _radarEllipView.pois      = radarPointValues;
        _radarEllipView.radius    = _radarRange;
        [_radarEllipView setNeedsDisplay];
    }
}

- (NSComparisonResult)LocationSortClosestFirst:(ARCoordinate *)s1 secondCoord:(ARCoordinate*)s2{
    
	if ([s1 radialDistance] < [s2 radialDistance]) 
		return NSOrderedAscending;
	else if ([s1 radialDistance] > [s2 radialDistance]) 
		return NSOrderedDescending;
	else 
		return NSOrderedSame;
}

#pragma mark -	
#pragma mark Device Orientation

//- (void)currentDeviceOrientation {
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//    
//	if (orientation != UIDeviceOrientationUnknown && orientation != UIDeviceOrientationFaceUp && orientation != UIDeviceOrientationFaceDown) {
//		switch (orientation) {
//            case UIDeviceOrientationLandscapeLeft:
//                cameraOrientation = AVCaptureVideoOrientationLandscapeRight;
//                break;
//            case UIDeviceOrientationLandscapeRight:
//                cameraOrientation = AVCaptureVideoOrientationLandscapeLeft;
//                break;
//            case UIDeviceOrientationPortraitUpsideDown:
//                cameraOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
//                break;
//            case UIDeviceOrientationPortrait:
//                cameraOrientation = AVCaptureVideoOrientationPortrait;
//                break;
//            default:
//                break;
//        }
//    }
//}

//- (void)deviceOrientationDidChange:(NSNotification *)notification {
//	
//	prevHeading = HEADING_NOT_SET; 
//    
//    [self currentDeviceOrientation];
//	
//    [[self previewLayer].connection setVideoOrientation:cameraOrientation];
//    
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//	
//    CGRect newFrame = [[UIScreen mainScreen] bounds];
//    
//    switch (orientation) {
//        case UIDeviceOrientationLandscapeLeft:
//        case UIDeviceOrientationLandscapeRight:
//            newFrame.size.width     = [[UIScreen mainScreen] applicationFrame].size.height;
//            newFrame.size.height    = [[UIScreen mainScreen] applicationFrame].size.width;
//            break;
//        case UIDeviceOrientationPortraitUpsideDown:
//            break;
//        default:
//            break;
//    }
//    
//    [previewLayer setFrame:[self.cameraView bounds]];
//    
//    if ([previewLayer.connection isVideoOrientationSupported]) {
//        [previewLayer.connection setVideoOrientation:cameraOrientation];
//    }
//    
//    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
//    
//    //Last but not least we need to move the radar if we are displaying one
////    if(_radarViewPort && _radarView)
////    {
////        [radarNorthLabel setFrame:CGRectMake(newFrame.size.width - 37, 2, 10, 10)];
////        [_radarView setFrame:CGRectMake(newFrame.size.width - 63, 2, 61, 61)];
////        [_radarViewPort setFrame:CGRectMake(newFrame.size.width - 63, 2, 61, 61)];
////    }
//}

//MARK: -
-(void)refreshData
{
    __weak typeof(self) wself = self;
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setCallback:^(CLLocation *newLocation)
     {
         [self setCenterLocation:newLocation];
         [[self delegate] didUpdateLocation:newLocation];
         [wself coordinateBoundsWithLocation:newLocation.coordinate withDistance:self.radarRange];
         [wself doPublication];
     }];
    [self setCenterLocation:appDelegate.locationManager.location];
    [[self delegate] didUpdateLocation:appDelegate.locationManager.location];

    [self coordinateBoundsWithLocation:appDelegate.locationManager.location.coordinate withDistance:_radarRange];
    [self doPublication];
}

-(void) doPublication{
    
    _strFilter = [_strFilter stringByReplacingOccurrencesOfString:@"[southWestLat]" withString:[NSString stringWithFormat:@"%f",southWestFilter.latitude]];
    _strFilter = [_strFilter stringByReplacingOccurrencesOfString:@"[northEastLat]" withString:[NSString stringWithFormat:@"%f",northEastFilter.latitude]];
    _strFilter = [_strFilter stringByReplacingOccurrencesOfString:@"[southWestLon]" withString:[NSString stringWithFormat:@"%f",southWestFilter.longitude]];
    _strFilter = [_strFilter stringByReplacingOccurrencesOfString:@"[northEastLon]" withString:[NSString stringWithFormat:@"%f",northEastFilter.longitude]];

    NSMutableArray *arrFilter = [NSMutableArray new];
    
    
    //points in visible view from Database
    [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db) {
        FMResultSet *set_querry = [db  executeQuery:_strFilter];
        
        
        while ([set_querry next])
        {
            //marker not exist...add new...
            if (self.myDIC_MarkerPublication[[set_querry stringForColumn:@"c_id"]] == nil) {
                [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                        @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                        @"c_legend":[set_querry stringForColumn:@"c_legend"],
                                        @"c_marker_picto":[set_querry stringForColumn:@"c_marker_picto"],
                                        @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                        @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                        @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                        @"c_marker":@(MARKER_PUBLICATION),
                                        }];
                
            }else{
                //exist... check update???
                //if update...remove...add to arrFilter again...!!!!! think it new.
                
                ARGeoCoordinate *tempCoordinate =  self.myDIC_MarkerPublication[[set_querry stringForColumn:@"c_id"]] ;
                
                if ([set_querry longForColumn :@"c_updated"] != [tempCoordinate.dicPublication[@"c_updated"] floatValue]){

                    
                    //need to be updated
                    [self removeCoordinate:tempCoordinate];
                    
                    //add new
                    [arrFilter addObject:@{ @"c_id":[set_querry stringForColumn:@"c_id"],
                                            @"c_owner_id":[set_querry stringForColumn:@"c_owner_id"],
                                            @"c_legend":[set_querry stringForColumn:@"c_legend"],
                                            @"c_marker_picto":[set_querry stringForColumn:@"c_marker_picto"],
                                            @"c_updated":  [NSNumber numberWithLong:  [set_querry longForColumn :@"c_updated"]],
                                            @"c_lat":[set_querry stringForColumn:@"c_lat"],
                                            @"c_lon":[set_querry stringForColumn:@"c_lon"],
                                            @"c_marker":@(MARKER_PUBLICATION),
                                            }];
                    
                }                
            }

        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
            //add new
            if (arrFilter.count > 0) {
                NSLog(@"aff .... count : %lu",(unsigned long)arrFilter.count);
                [self performSelector:@selector(TimerTick:) withObject:arrFilter];
            }
        });
     
    }];
}
-(void)TimerTick:(NSMutableArray *)arrIn
{

    if (arrIn.count> 0) {
        NSMutableArray *mutArr = [arrIn mutableCopy];
        
        NSDictionary *dic =mutArr[0];
        //add coordinate
        CLLocation       *tempLocation = [[CLLocation alloc] initWithLatitude:[dic[@"c_lat"] floatValue] longitude:[dic[@"c_lon"] floatValue]];
        ARGeoCoordinate *tempCoordinate = [ARGeoCoordinate coordinateWithLocation:tempLocation dicPublication:dic];

        MarkerView *cv = [[MarkerView alloc] initForCoordinate:tempCoordinate withDelgate:self allowsCallout:YES];
        [tempCoordinate setDisplayView:cv];
        [self addCoordinate:tempCoordinate];
        
        [mutArr removeObjectAtIndex:0];
        
        [self performSelector:@selector(TimerTick:) withObject:mutArr afterDelay:0.01];
        
        
    }
}

- (void)didTapMarker:(ARGeoCoordinate *)coordinate {
    NSLog(@"delegate worked click on %@", [coordinate title]);
    //go detail of this marker
    
//    __block  NSMutableDictionary *dicMul = [NSMutableDictionary dictionaryWithDictionary:_coordinateInfo.dicPublication];

    [markerDelegate didTapMarker:coordinate];
    
}


@end
