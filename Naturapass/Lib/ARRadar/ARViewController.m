//
//  ARViewController.m
//  ARKitDemo
//
//  Modified by Niels W Hansen on 12/31/11.
//  Modified by Ed Rackham (a1phanumeric) 2013
//

#import "ARViewController.h"
#import "AugmentedRealityController.h"
#import "GEOLocations.h"
#import "MarkerView.h"
#import "DatabaseManager.h"
#import "AppDelegate.h"

#import "CommentDetailVC.h"

@implementation ARViewController{
    AugmentedRealityController *_agController;
}

@synthesize delegate;

- (id)initWithDelegate:(id<ARLocationDelegate>)aDelegate{
	
	[self setDelegate:aDelegate];
	
	if (!(self = [super init])){
		return nil;
	}
    
	[self setWantsFullScreenLayout:NO];
    
    // Defaults
    _debugMode                      = NO;
    _scaleViewsBasedOnDistance      = YES;
    _minimumScaleFactor             = 0.5;
    _rotateViewsBasedOnPerspective  = YES;
    _showsRadar                     = YES;
    _showsCloseButton               = YES;
    _radarRange                     = 50.0;
    _onlyShowItemsWithinRadarRange  = NO;
    
    // Create ARC
    _agController = [[AugmentedRealityController alloc] initWithViewController:self withDelgate:self markerDel: self];
	
    [_agController setShowsRadar:_showsRadar];
    [_agController setRadarRange:_radarRange];
	[_agController setScaleViewsBasedOnDistance:_scaleViewsBasedOnDistance];
	[_agController setMinimumScaleFactor:_minimumScaleFactor];
	[_agController setRotateViewsBasedOnPerspective:_rotateViewsBasedOnPerspective];
    [_agController setOnlyShowItemsWithinRadarRange:_onlyShowItemsWithinRadarRange];
    [self refreshData];
    
    [self.view setAutoresizesSubviews:YES];
    
    
 	return self;
}
-(void)refreshData
{
    [_agController removeAllCoordinate];
//    GEOLocations *locations = [[GEOLocations alloc] initWithDelegate:delegate];
//    
//    if([[locations returnLocations] count] > 0){
//        for (ARGeoCoordinate *coordinate in [locations returnLocations]){
//            MarkerView *cv = [[MarkerView alloc] initForCoordinate:coordinate withDelgate:self allowsCallout:YES];
//            [coordinate setDisplayView:cv];
//            //            [coordinate calibrateUsingOrigin:coordinate.geoLocation];
//            [_agController addCoordinate:coordinate];
//            
//        }
//    }
}
- (void)closeButtonClicked:(id)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setCallback:nil];
    [_agController unloadAV];
    
    _agController = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

//    if(_showsCloseButton == YES) {
        UIButton *closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, 45, 45)];
        
        [closeBtn setImage: [UIImage imageNamed:@"ic_radar_close"] forState:UIControlStateNormal];
    
    
        [closeBtn addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [[self view] addSubview:closeBtn];
//    }
}


- (void)didUpdateHeading:(CLHeading *)newHeading {
    //NSLog(@"Heading Updated");
}
- (void)didUpdateLocation:(CLLocation *)newLocation {
    //NSLog(@"Location Updated");
}
- (void)didUpdateOrientation:(UIDeviceOrientation)orientation {
   /*NSLog(@"Orientation Updated");
    
    if(orientation == UIDeviceOrientationPortrait)
        NSLog(@"Portrait");
    */
}

#pragma mark - Custom Setters
- (void)setDebugMode:(BOOL)debugMode{
    _debugMode = debugMode;
    [_agController setDebugMode:_debugMode];
}

- (void)setShowsRadar:(BOOL)showsRadar{
    _showsRadar = showsRadar;
    [_agController setShowsRadar:_showsRadar];
}

- (void)setScaleViewsBasedOnDistance:(BOOL)scaleViewsBasedOnDistance{
    _scaleViewsBasedOnDistance = scaleViewsBasedOnDistance;
    [_agController setScaleViewsBasedOnDistance:_scaleViewsBasedOnDistance];
}

- (void)setMinimumScaleFactor:(float)minimumScaleFactor{
    _minimumScaleFactor = minimumScaleFactor;
    [_agController setMinimumScaleFactor:_minimumScaleFactor];
}

- (void)setRotateViewsBasedOnPerspective:(BOOL)rotateViewsBasedOnPerspective{
    _rotateViewsBasedOnPerspective = rotateViewsBasedOnPerspective;
    [_agController setRotateViewsBasedOnPerspective:_rotateViewsBasedOnPerspective];
}

- (void)setRadarPointColour:(UIColor *)radarPointColour{
    _radarPointColour = radarPointColour;
    [_agController.radarView setPointColour:_radarPointColour];
}

- (void)setRadarBackgroundColour:(UIColor *)radarBackgroundColour{
    _radarBackgroundColour = radarBackgroundColour;
    [_agController.radarView setRadarBackgroundColour:_radarBackgroundColour];
}

- (void)setRadarViewportColour:(UIColor *)radarViewportColour{
    _radarViewportColour = radarViewportColour;
    [_agController.radarViewPort setViewportColour:_radarViewportColour];
}

- (void)setRadarRange:(float)radarRange{
    _radarRange = radarRange;
    [_agController setRadarRange:_radarRange];
}

- (void)setOnlyShowItemsWithinRadarRange:(BOOL)onlyShowItemsWithinRadarRange{
    _onlyShowItemsWithinRadarRange = onlyShowItemsWithinRadarRange;
    [_agController setOnlyShowItemsWithinRadarRange:_onlyShowItemsWithinRadarRange];
}

-(void)setStrFilter:(NSString *)strFilter
{
    _strFilter = strFilter;
    _agController.strFilter = _strFilter;
    [_agController refreshData];
}
#pragma mark - View Cleanup
- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
	_agController = nil;
}

- (BOOL)shouldAutorotate
{
    return YES;
}
- (UIInterfaceOrientationMask) supportedInterfaceOrientations
{
    if (_agController == nil) {
        return UIInterfaceOrientationMaskPortrait;

    }else{
        return UIInterfaceOrientationMaskLandscape;
    }
}

//MARK: - PUBLICATION

- (void)didTapMarker:(ARGeoCoordinate *)coordinate {
    NSLog(@"delegate worked click on %@", [coordinate title]);
    NSDictionary *dic = coordinate.dicPublication;
    
    NSLog(@"%@", dic);
    [self gotoPublicationDidTapMarker:dic];
//    [delegate locationClicked:coordinate];
}

-(void)gotoPublicationDidTapMarker:(NSDictionary *)dicMark
{
    //if online -> get item's data
//    __weak typeof(self) wself = s1elf;
    if ([COMMON isReachable]) {
        
        [COMMON addLoading:self];
        
        WebServiceAPI *serviceObj = [[WebServiceAPI alloc]init];
        
        [serviceObj getPublicationsActionItem: dicMark[@"c_id"]];
        
        serviceObj.onComplete = ^(NSDictionary*response, int errCode){
            dispatch_async(dispatch_get_main_queue(), ^{
                [COMMON removeProgressLoading];
                
            });
            
            if (!response) {
                return ;
            }
            if([response isKindOfClass: [NSArray class] ]) {
                return ;
            }
            if (response[@"publication"]) {
                
                //success...go detail.
                dispatch_async(dispatch_get_main_queue(), ^{

                    //reload header
                    CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
                    commentVC.commentDic = response[@"publication"];
                    commentVC.mParent = self;
                    commentVC.expectTarget = ISMUR;
//                    if (self.isLiveHunt) {
                        commentVC.isLiveHuntDetail = YES;
//                    }
                    // push
                    [commentVC setCallbackWithDelItem:^(NSDictionary *dicPublication)
                     {
                         
                     }];
                    
                    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:commentVC];
                    
                    
                    [self presentViewController:nav animated:YES completion:^{
                    }];
                    
                });
                
            }
        };
        
    }
    else
    {
        //Show comment offline mode
        
        //querry from db to get text + geo + name owner.
        
        //marker.userData[@"c_id"]
        NSString *sender_id= [[NSUserDefaults standardUserDefaults] valueForKey:@"sender_id"];
        
        NSMutableString *mutStr = [NSMutableString new];
        
        [mutStr setString:@"SELECT DISTINCT * FROM tb_carte "];
        [mutStr appendString: [NSString stringWithFormat:@" WHERE c_id = %@ ",dicMark[@"c_id"]] ];
        [mutStr appendString:[ NSString stringWithFormat:@" AND ( c_user_id=%@ OR c_user_id= %d)  ", sender_id, SPECIAL_USER_ID]];
        
        
        [[DatabaseManager sharedManager].queue inDatabase:^(FMDatabase *db)
         {
             FMResultSet *set_querry = [db  executeQuery:mutStr];
             
             NSString *mOwnerName = @"Test";
             
             NSString *mContent = @"Test";
             NSString *mLat =@"";
             NSString *mLon =@"";
             int iShare = 0;
             
             while ([set_querry next])
             {
                 mOwnerName = CHECKSTRING([set_querry stringForColumn:@"c_owner_name"]);
                 
                 mContent = CHECKSTRING([set_querry stringForColumn:@"c_text"]);
                 
                 mLat = CHECKSTRING([set_querry stringForColumn:@"c_lat"]);
                 mLon = CHECKSTRING([set_querry stringForColumn:@"c_lon"]);
                 iShare = [set_querry intForColumn:@"c_sharing"];
                 
             }
             // Nil param???
             //if any error => don't go
             
             dispatch_async( dispatch_get_main_queue(), ^{
                 CommentDetailVC *commentVC=[[CommentDetailVC alloc]initWithNibName:@"CommentDetailVC" bundle:nil];
                 
                 commentVC.commentDic = @{
                                          @"owner": @{@"firstname": mOwnerName},
                                          @"content" : mContent,
                                          @"geolocation":@{@"latitude":mLat,
                                                           @"longitude" :mLon,
                                                           },
                                          @"sharing": @{@"share": [NSNumber numberWithInt: iShare ]}
                                          };
                 commentVC.mParent = self;
                 commentVC.expectTarget = ISMUR;
                 commentVC.isLiveHuntDetail = YES;
                 // push
                 
                 UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:commentVC];
                 
                 
                 [self presentViewController:nav animated:YES completion:^{
                 }];
                 
             });
             
             
         }];
    }
}

@end
