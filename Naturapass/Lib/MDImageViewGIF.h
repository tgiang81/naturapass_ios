//
//  MDImageViewGIF.h
//  Naturapass
//
//  Created by manh on 11/18/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^MDImageViewGIFCompletionBlock) (NSInteger buttonIndex);

@interface MDImageViewGIF : UIImageView
@property (copy, nonatomic,) MDImageViewGIFCompletionBlock didDismissBlock;
-(void)addButtonTitles:(NSArray *)otherButtonTitles
              tapBlock:(MDImageViewGIFCompletionBlock)tapBlock;
@end
