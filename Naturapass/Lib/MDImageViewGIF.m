//
//  MDImageViewGIF.m
//  Naturapass
//
//  Created by manh on 11/18/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "MDImageViewGIF.h"

@implementation MDImageViewGIF

- (void)awakeFromNib {
    [super awakeFromNib];
}
-(void)addButtonTitles:(NSArray *)otherButtonTitles
                     tapBlock:(MDImageViewGIFCompletionBlock)tapBlock {
    UIView *viewSuper = self;
    NSMutableString *strContraintH = [NSMutableString new];
    NSMutableDictionary *dic = [NSMutableDictionary new];
    for (int i = 0; i < otherButtonTitles.count; i++) {
        UIButton *view = [[UIButton alloc] init];
        [viewSuper addSubview:view];
        [view addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        view.tag = i;
        view.backgroundColor = [UIColor clearColor];
        view.translatesAutoresizingMaskIntoConstraints = NO;
        if (i == 0) {
            [strContraintH appendFormat:@"H:|-0-"];
            [strContraintH appendFormat:@"%@",[NSString stringWithFormat:@"[btn%d]-0-",i]];
        }
        else if (i > 0)
        {
            [strContraintH appendFormat:@"%@",[NSString stringWithFormat:@"[btn%d)(==btn%d)]-0-",i,i-1]];

        }
        if (i == (otherButtonTitles.count - 1)) {
            [strContraintH appendFormat:@"|"];
        }
        [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view(44)]-(0)-|"
                                                                          options:0
                                                                          metrics:nil
                                                                            views:NSDictionaryOfVariableBindings(view)]];
        
        [dic setObject:view forKey:[NSString stringWithFormat:@"btn%d",i]];
    }
    if (strContraintH.length > 0) {
        [viewSuper addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strContraintH
                                                                          options:0
                                                                          metrics:nil
                                   
                                                                            views:dic]];
    }
    if (tapBlock) {
        [self setDidDismissBlock:tapBlock];
    }
}
-(void)setDidDismissBlock:(MDImageViewGIFCompletionBlock)didDismissBlock
{
    _didDismissBlock = didDismissBlock;
}
-(IBAction)clickAction:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (_didDismissBlock) {
        _didDismissBlock(btn.tag);
    }
}
@end
