//
//  ShareViewController.m
//  shareExtentions
//
//  Created by JoJo on 7/28/16.
//  Copyright © 2016 Appsolute. All rights reserved.
//

#import "ShareViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
@interface ShareViewController ()

@end

@implementation ShareViewController

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    return YES;
}

- (void)didSelectPost {
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    NSExtensionItem *content = self.extensionContext.inputItems[0];
    NSString *contentImage = [NSString stringWithFormat:@"%@",kUTTypeImage];
    NSString *contentMovie = [NSString stringWithFormat:@"%@",kUTTypeMovie];

    NSArray *contents = content.attachments;

    for (NSItemProvider *attachment in contents) {
        //image
        if ([attachment hasItemConformingToTypeIdentifier:contentImage]) {
            [attachment loadItemForTypeIdentifier:contentImage options:nil completionHandler:^(NSURL *url, NSError *error) {
                [self saveShareExtentionsWithURL:url withType:contentImage];
            }];
        }
        //movie
        if ([attachment hasItemConformingToTypeIdentifier:contentMovie]) {
            [attachment loadItemForTypeIdentifier:contentMovie options:nil completionHandler:^(NSURL *url, NSError *error) {
                [self saveShareExtentionsWithURL:url withType:contentMovie];
            }];
        }

    }
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
}
//Save an image to user defaults
-(void)saveShareExtentionsWithURL:(NSURL*)url withType:(NSString*)type
{
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        //save data
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *filename = [[url path] lastPathComponent];
        NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,filename];
        [urlData writeToFile:filePath atomically:YES];
        
        //save info userdefaults
        NSMutableDictionary *dicShare = [NSMutableDictionary new];
        NSUserDefaults *userData = [[NSUserDefaults alloc] initWithSuiteName:@"group.fr.e-conception.naturapass"];
        
        NSDictionary *dicExtentions = [userData objectForKey:@"share_extension"];
        if (dicExtentions != nil) {
            [dicShare addEntriesFromDictionary:dicExtentions];
        }
        
        [dicShare setValue:@{@"content": self.contentText, @"type": type, @"url": filePath} forKey:filePath];
        [userData setObject:dicShare forKey:@"share_extension"];
        [userData synchronize];

    }
}
-(void)handleDocumentOpenURL:(NSURL*)url
{
    NSData *urlData = [NSData dataWithContentsOfURL:url];
    if ( urlData )
    {
        NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString  *documentsDirectory = [paths objectAtIndex:0];
        
        NSString *filename = [[url path] lastPathComponent];
        NSString  *filePath = [NSString stringWithFormat:@"%@/shareExtentions/%@", documentsDirectory,filename];
        [urlData writeToFile:filePath atomically:YES];
    }
}

- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    return @[];
}

@end
